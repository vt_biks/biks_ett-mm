/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import com.google.gwt.core.client.Duration;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.dom.client.LoadEvent;
import com.google.gwt.event.dom.client.LoadHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.logical.shared.AttachEvent;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.UrlBuilder;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.HasRpcToken;
import com.google.gwt.user.client.rpc.RpcTokenException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.rpc.XsrfToken;
import com.google.gwt.user.client.rpc.XsrfTokenService;
import com.google.gwt.user.client.rpc.XsrfTokenServiceAsync;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import pl.bssg.metadatapump.common.ConnectionParametersJdbcBean;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.client.bikpages.IBikPage;
import pl.fovis.client.bikpages.IBikPageForAppAdmin;
import pl.fovis.client.bikpages.IBikTreePage;
import pl.fovis.client.bikpages.IMyBIKSPage;
import pl.fovis.client.bikpages.IMyBIKSPage.ReachableMyBIKSTabCode;
import pl.fovis.client.bikpages.SearchPage;
import pl.fovis.client.bikwidgets.BikIcon;
import pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog;
import pl.fovis.client.dialogs.AttrHintDialog;
import pl.fovis.client.dialogs.EntityDetailsPaneDialog;
import pl.fovis.client.dialogs.HelpDialog;
import pl.fovis.client.dialogs.ViewJoinedAttributeLinkedDialog;
import pl.fovis.client.dialogs.WaitingForLoginAgainDialog;
import pl.fovis.client.dialogs.lisa.ILisaBeansWrapperCreator;
import static pl.fovis.client.entitydetailswidgets.AttributeWidget.RENDER_AS_HTML_VAL_PREFIX;
import static pl.fovis.client.entitydetailswidgets.AttributeWidget.RENDER_AS_HTML_VAL_SUFFIX;
import static pl.fovis.client.entitydetailswidgets.AttributeWidget.getHTMLInnerText;
import pl.fovis.client.nodeactions.NodeActionAddAnyNodeForKind;
import pl.fovis.client.nodeactions.NodeActionContext;
import pl.fovis.client.treeandlist.IBean4TreeExtractor;
import pl.fovis.client.treeandlist.TreeNodeBean4TreeExtractor;
import pl.fovis.common.AppPropBean;
import pl.fovis.common.AppPropsBeanEx;
import pl.fovis.common.AttrHintBean;
import pl.fovis.common.AttrSearchableBean;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.AttributeHyperlinkBean;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.BIKRightRoles;
import pl.fovis.common.ChangedRankingBean;
import pl.fovis.common.ChildrenBean;
import pl.fovis.common.CustomRightRoleBean;
import pl.fovis.common.DisabledUserApplyForAccountMsgsBean;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.HelpBean;
import pl.fovis.common.JoinedObjAttributeLinkedBean;
import pl.fovis.common.LoggedUserOnClientVsServerStatus;
import pl.fovis.common.MenuExtender;
import pl.fovis.common.MenuExtender.MenuBottomLevel;
import pl.fovis.common.MenuNodeBean;
import pl.fovis.common.NodeKind4TreeKind;
import pl.fovis.common.NodeKindBean;
import pl.fovis.common.RightRoleBean;
import pl.fovis.common.StartupDataBean;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.SystemUserGroupBean;
import pl.fovis.common.TreeBean;
import pl.fovis.common.TreeIconBean;
import pl.fovis.common.TreeKindBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.mltx.MultixConstants;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.ClientDiagMsgs;
import pl.fovis.foxygwtcommons.DoNothingAsyngCallback;
import pl.fovis.foxygwtcommons.FoxyClientSingletons;
import static pl.fovis.foxygwtcommons.FoxyClientSingletons.doNothingCallback;
import pl.fovis.foxygwtcommons.FoxyRequestCallback;
import pl.fovis.foxygwtcommons.FoxyRpcRequestBuilder;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.IResponseProcessingHook;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import static pl.fovis.foxygwtcommons.StandardAsyncCallback.showOnFailureError;
import pl.fovis.foxygwtcommons.StatusBarNotificationAnimator;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import pl.fovis.foxygwtcommons.dialogs.Dialogs;
import pl.fovis.foxygwtcommons.treeandlist.ITreeGridForAction;
import pl.fovis.foxygwtcommons.treeandlist.ITreeRowAndBeanStorage;
import pl.rmalinowski.gwt2swf.client.ui.SWFWidget;
import pl.rmalinowski.gwt2swf.client.utils.PlayerVersion;
import pl.rmalinowski.gwt2swf.client.utils.SWFObjectUtil;
import pl.trzy0.foxy.commons.FoxyCommonConsts;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IContinuationWithReturn;
import simplelib.IParametrizedContinuation;
import simplelib.IRegExpMatcher;
import simplelib.IRegExpPattern;
import simplelib.Pair;
import simplelib.SimpleDateUtils;
import simplelib.SimpleHtmlSanitizer;
import simplelib.i18nsupport.I18nMessage;

/**
 *
 * @author wezyr
 */
//i18n-default: no
public class BIKClientSingletons extends FoxyClientSingletons {

    public static final String DIAG_MSG_KIND$MISSING_I18N_VAR = "missingI18nVar";

    public static final String ATTRIBUTE_PATTERN = "<([a-zA-Z0-9 \t\n]*[" + "id+class+width" /* I18N:  */ + "])[ \t\n]*=([a-zA-Z0-9_\\/]*)([ \t\n>])";
    public static final IRegExpPattern HTML_URL_PATTERN = BaseUtils.getRegExpMatcherFactory().compile(ATTRIBUTE_PATTERN, true, true);
    public static final int ORIGINAL_WINDOW_CLIENTHEIGHT = 800;//800
    public static final int ORIGINAL_MAINBODY_DIV_HEIGHT = 674;// 665 //675
    public static final int MY_FUCKING_IE7_HACK_HEIGHT = ORIGINAL_MAINBODY_DIV_HEIGHT - 22; //25
    public static final int ORIGINAL_MAIN_CONTAINER_HEIGHT = ORIGINAL_MAINBODY_DIV_HEIGHT; // nic nie odejmujemy, bo brak paddingów itp.
    public static final int ORIGINAL_TREE_AND_FILTER_HEIGHT = ORIGINAL_MAINBODY_DIV_HEIGHT - 24;  // odejmujemy tylko 3-ci poziom menu, pozostałe są poza mainBody 27
    public static final int ORIGINAL_TREE_GRID_PANEL_HEIGHT = ORIGINAL_TREE_AND_FILTER_HEIGHT - 32; // 32px zajmuje pasek filtrowania pod drzewkiem 32
    public static final int ORIGINAL_SELECT_ROLE_IN_DIALOG_SCROLL_HEIGHT = ORIGINAL_TREE_AND_FILTER_HEIGHT - 251;//156 171 /*- 22; */// 16px zajmuje nagłówek "wybierz rolę", razem z nagłówkiem ma to zajmować tyle co drzewko z filtrowaniem
    public static final int ORIGINAL_TREE_GRID_WIDGET_HEIGHT = ORIGINAL_TREE_GRID_PANEL_HEIGHT; // brak paddingów -> nic nie odejmujemy
    public static final int ORIGINAL_EDP_BODY = 282;
    public static final int ORIGINAL_EDP_ADD_ASSOCIATION = 28;
    public final static int SCROLL_PANEL_SPACING = 60;
//    public static final int ORIGINAL_GLOSSARY_HEIGHT = ORIGINAL_MAINBODY_DIV_HEIGHT - 270;
    private static final GUIResizer guiResizer = new GUIResizer(ORIGINAL_WINDOW_CLIENTHEIGHT);
    private static InlineLabel appVersionInfoLbl;

//    private static final int oldBodyHeight = 590;
//    public static final String TAB_ID_GLOSSARY = "Glossary" /* I18N:  */;
//    public static final String TAB_ID_MYBIK = "MyBIK";
    //---
    //ww: jest metoda getDoNothingCallback
//    private static final AsyncCallback<Void> doNothing = new StandardAsyncCallback<Void>() {
//        public void onSuccess(Void result) {
//            //no-op
//        }
//    };
    private static List<CustomRightRoleBean> customRightRoles;
    public static Integer customRightRoleRegularUserId;
    private static Map<String, Integer> nodeActionCodeToIdMap;
    private static Map<Integer, Set<Integer>> customRightRolesActionsInTreeRoots;
    private static Map<Integer, CustomRightRoleBean> customRightRoleByIdMap;
    private static String ssoUserName;
    private static String developerModeStr;
    private static String enableAttributeUsagePageStr;
    private static boolean useSimpleMsgForUnloggedUser;
    private static boolean suggestSimilarNodes;
    public static Integer requestId = null;
    private static boolean isMultiDomainMode;
    public static String visScript = "";
    private static List<SystemUserGroupBean> allRoles;

    public static <T> AsyncCallback<T> getDoNothingCallback() {
//        return (AsyncCallback<T>) doNothing;
        return DoNothingAsyngCallback.getDoNothingCallback();
    }
    //i18n-default:
    protected static IParametrizedContinuation<Map<String, String>> savePropsCont = new IParametrizedContinuation<Map<String, String>>() {
        public void doIt(Map<String, String> modifiedProps) {
            BIKClientSingletons.getService().setEditableAppProps(modifiedProps, new AsyncCallback<List<AppPropBean>>() {
                public void onFailure(Throwable caught) {
                    Window.alert("error" /* I18N: no */ + "!");
                }

                public void onSuccess(List<AppPropBean> result) {
                    BIKClientSingletons.setupAppProps(result, isMultiXMode);

                    Window.alert("saved! current messages and kinds - cleared" /* I18N: no */ + "...");
                }
            });
        }
    };
//    protected static FoxyRpcRequestBuilder rpcReqBuilder = new FoxyRpcRequestBuilder();
    // słabo jest, trzeba użyć tej metody!!! http://stackoverflow.com/a/10912078/209507
    protected static VersionInfoServiceAsync versionInfoService = null; //GWT.create(VersionInfoService.class);
    private static BOXIServiceAsync service = null;
    private static LisaServiceAsync lisaService = null;
    private static DQMServiceAsync dqmService = null;
    private static FtsServiceAsync ftsService = null;
    private static MultiKSServiceAsync multiXService = null;
    public final static ILisaBeansWrapperCreator beanCreator = GWT.create(ILisaBeansWrapperCreator.class);
    //private static boolean developerMode;
//    private static UserBean loggedUser;
    private static SystemUserBean loggedUser;
    private static boolean userExistButDisabled;
    private static List<NodeKindBean> nodeKinds;
    private static List<TreeKindBean> treeKinds;
    private static Map<Pair<String, String>, String> treeIcons;
    private static List<TreeBean> trees;
    private static List<TreeBean> allTrees;
    private static Map<String, TreeKindBean> treeKindsByCode;
    private static Set<String> treeKindsAsSet;
    private static Map<String, String> treeKindCaptions;
//    private static HTML infoLabel;
//    private static HTML errorLabel;
    //private static int newBodyHeight;
    private static final EDPForms forms = new EDPForms();
    private static ITabSelector<Integer> tabSelector;
    private static TextBox globalSearchTb = null;
    private static Map<Integer, NodeKindBean> nodeKindsMap;
    private static Map<String, NodeKindBean> nodeKindsByCodeMap;
    private static Set<String> treeCodesWithTreeKind;
    private static Map<Integer, TreeBean> treesMap;
    private static Map<Integer, TreeBean> allTreesMap;
    private static Map<String, TreeBean> allTreesByCodeMap;
    private static Map<String, TreeBean> treeByCodeMap;
    protected static List<String> treeNotHiddenKind;
    private static List<MenuNodeBean> menuNodes;
    private static Map<String, String> treeCodeToMenuPath;
//    private static List<BikRoleForNodeBean> bikRolesForNode;
    //protected static List<AppPropBean> appPropBeans;
    public static AppPropsBeanEx appPropsBean;
    private static boolean isMultiXMode;
    private static boolean disableGuestMode;
    protected static Set<TextBox> additionalSearchPagePhraseTbs = new HashSet<TextBox>();
    private static Set<String> inactiveTabs;
    private static Map<Integer, List<AttributeBean>> systemAttribute;
    private static Map<Integer, List<AttributeBean>> allAttributesForNodeKindId;
    private static Map<String, Map<String, String>> mssqlExProps;
    private static Map<String, String> attrNameTranslations;
    private static Map<String, String> attrCatNameTranslations;
    private static Map<String, AttrHintBean> attrHintsMap;
    private static Map<Integer, String> openReportLink;
    private static Set<String> reportTreeCodes;
//    protected static List<AttributeBean> attrDefAndCategory;
    protected static PushButton hintBtn;
    protected static Map<Integer, List<Integer>> attrDict;
    protected static List<AttrSearchableBean> attrsSearchable;
    public static ChangedRankingBean changedRanking;
//    private static long lastRankingChangeTimeStamp;
    private static String welcomeMsgForUser;
    private static String welcomeMsgForVersion;
    private static String biksDatabaseName;
    private static Map<String, RightRoleBean> allRightRolesByCode;
    private static boolean isUserCreatorOfThisDb;
    private static List<String> availableConnectors;
//    public static IContinuation performOnLoginLogoutCont = new IContinuation() {
//        public void doIt() {
//            getTabSelector().refreshAfterLoginLogout();
//        }
//    };
    protected static String cancellableRequestTicketPrefix = BaseUtils.generateRandomToken(8);
    protected static int cancellableRequestTicketNumber = 0;
    protected static Map<Integer, Set<String>> hiddenAttrNamesByNodeKindId = new HashMap<Integer, Set<String>>();
    protected static List<String> adDomains;
    protected static boolean isRegisterNewNodeActionsEnabled;
    protected static String treeSelectorForRegularUserCrr;
    protected static boolean showGrantsInTestConnection;
    protected static boolean isDevModeEx;
    protected static int treeTabAutoRefreshLevel;
    protected static boolean isDescriptionInJoinedPanelVisible;
    protected static ViewType viewType;
    public static boolean canSendMailViaServer;
    protected static Map<String, ConnectionParametersJdbcBean> jdbcSourceTypes = new HashMap<String, ConnectionParametersJdbcBean>();

    static {
        BaseActionOrCancelDialog.USE_ON_DIALOG_NOTIFICATIONS = true;

        ClientDiagMsgs.setSavePropsCont(savePropsCont);

        FoxyClientSingletons.setNotificationDisabler(new IContinuationWithReturn<Boolean>() {

            @Override
            public Boolean doIt() {
                return badLoginDialog != null || showingWaitingForLoginAgainDialog;
            }
        });

//        setRootNotificationHandler(new IFoxyNotificationHandler() {
//            public void showInfo(String msg) {
//                showAppMessage(msg, infoLabel, false);
//            }
//
//            public void showWarning(String err) {
//                showAppMessage(err, errorLabel, true);
//            }
//
//            public boolean isInfoModal() {
//                return false;
//            }
//
//            public boolean isWarningModal() {
//                return false;
//            }
//        });
    }

    public static List<String> getLanguages() {
        return BaseUtils.splitBySep(appPropsBean.languages.toLowerCase(), ",", true);
    }

    public static void setNotificationLabels(HTML infoLabel, HTML errorLabel) {
        setRootNotificationHandler(new StatusBarNotificationAnimator(infoLabel, errorLabel));
//        BIKClientSingletons.infoLabel = infoLabel;
//        BIKClientSingletons.errorLabel = infoLabel;
    }

//    public static void setErrorLabel(HTML errorLabel) {
//        BIKClientSingletons.errorLabel = errorLabel;
//    }
    public static String getOpenReportLink(int treeId) {
        return BIKClientSingletons.openReportLink.get(treeId);
    }

    public static void setOpenReportLink(String openReportLink, Integer treeId) {
        BIKClientSingletons.openReportLink.put(treeId, openReportLink);
    }

    public static List<AttributeBean> getAttributesForKind(Integer nodeKindId) {
        return systemAttribute.get(nodeKindId);
    }

    public static List<AttributeBean> getAllAttributesForKind(Integer nodeKindId) {
        return allAttributesForNodeKindId.get(nodeKindId);
    }

    protected static boolean isAttrHiddenForKind(int nodeKindId, String attrName) {
        Set<String> hiddenNames = hiddenAttrNamesByNodeKindId.get(nodeKindId);
        return BaseUtils.collectionContainsFix(hiddenNames, attrName);
    }

    public static String getTranslatedAttributeNameOrNullForHidden(int nodeKindId, String defName) {
        if (isAttrHiddenForKind(nodeKindId, defName)) {
            return null;
        }

        return getTranslatedAttributeName(defName);
//        List<AttributeBean> attributesForKind = BIKClientSingletons.getAttributesForKind(dstNodeKindId);
//        if (attributesForKind == null) {
//            return BaseUtils.safeToStringDef(defName, "");
//        }
//        for (AttributeBean aB : attributesForKind) {
//            if (aB.atrName.equals(defName)) {
//                if (aB.isVisible == 0) {
//                    return null;
//                }
//                return aB.attrDisplayName;
//            }
//        }
//        return BaseUtils.safeToStringDef(defName, "");
    }

//    protected static void fixAsyncService(Object asyncService) {
//        ((ServiceDefTarget) asyncService).setRpcRequestBuilder(FoxyRpcRequestBuilder.getInstance());
//    }
    public static VersionInfoServiceAsync getVersionInfoService() {
        if (versionInfoService == null) {
//            versionInfoService = GWT.create(VersionInfoService.class);
//            fixAsyncService(versionInfoService);
            versionInfoService = GWT.create(VersionInfoService.class);
            FoxyRpcRequestBuilder.fixAsyncService(versionInfoService);
        }

        return versionInfoService;
    }

    public static void initializeServices(final AsyncCallback endCallback) {
        if (service == null) {
            XsrfTokenServiceAsync xsrf = GWT.create(XsrfTokenService.class);
            ((ServiceDefTarget) xsrf).setServiceEntryPoint(GWT.getModuleBaseURL() + "xsrf");
            xsrf.getNewXsrfToken(new AsyncCallback<XsrfToken>() {

                public void onSuccess(XsrfToken token) {
                    service = GWT.create(BOXIService.class);
                    FoxyRpcRequestBuilder.fixAsyncService(service);
                    ((HasRpcToken) service).setRpcToken(token);
                    endCallback.onSuccess(null);
                }

                public void onFailure(Throwable caught) {
                    try {
                        service = null;
                        throw caught;
                    } catch (RpcTokenException e) {
                        // Can be thrown for several reasons:
                        //   - duplicate session cookie, which may be a sign of a cookie
                        //     overwrite attack
                        //   - XSRF token cannot be generated because session cookie isn't
                        //     present
                    } catch (Throwable e) {
                        // unexpected
                    }
                }
            });
        }
    }

    public static BOXIServiceAsync getService() {
        // słabo jest, trzeba użyć tej metody!!! http://stackoverflow.com/a/10912078/209507
        // na wszystkich serwisach niestety!
        if (service == null) {
            service = GWT.create(BOXIService.class);
            FoxyRpcRequestBuilder.fixAsyncService(service);
        }
        return service;
    }

    public static LisaServiceAsync getLisaService() {
        if (lisaService == null) {
            lisaService = GWT.create(LisaService.class);
            FoxyRpcRequestBuilder.fixAsyncService(lisaService);
        }
        return lisaService;
    }

    public static DQMServiceAsync getDQMService() {
        if (dqmService == null) {
            dqmService = GWT.create(DQMService.class);
            FoxyRpcRequestBuilder.fixAsyncService(dqmService);
        }
        return dqmService;
    }

    public static FtsServiceAsync getFtsService() {
        if (ftsService == null) {
            ftsService = GWT.create(FtsService.class);
            FoxyRpcRequestBuilder.fixAsyncService(ftsService);
        }
        return ftsService;
    }

    public static MultiKSServiceAsync getMultiXService() {
        if (multiXService == null) {
            multiXService = GWT.create(MultiKSService.class);
            FoxyRpcRequestBuilder.fixAsyncService(multiXService);
        }

        return multiXService;
    }

    public static void performUserLogout(/*final IContinuation cont*/) {
        AsyncCallback<Void> myCallBack = new StandardAsyncCallback<Void>("communitaction error in logout" /* I18N: no:err-fail-in */) {
            @Override
            public void onSuccess(Void result) {
                if (isMultiXMode) {
                    History.newItem("", false);
                }
                loggedUser = null;
                //cont.doIt();
                getTabSelector().refreshAfterLoginLogout();
            }
        };

        FoxyRpcRequestBuilder.setLoggedUserName(null);

        getService().performUserLogout(myCallBack);
    }

//    public static void performUserLogin(String login, String pwd, String domain, final IParametrizedContinuation<Pair<String, Boolean>> cont) {
//
//        AsyncCallback<Pair<String, Boolean>> myCallBack = new StandardAsyncCallback<Pair<String, Boolean>>("communitaction error in login" /* I18N: no:err-fail-in */) {
//            @Override
//            public void onSuccess(Pair<String, Boolean> result) {
//                //loggedUser = result;
//                cont.doIt(result);
////                GWTUtils.executeAfter(cont, 5000);
//            }
//        };
//
//        FoxyRpcRequestBuilder.setLoggedUserName(login);
//        getService().performUserLogin(login, pwd, domain, myCallBack);
//    }
//    public static void performUserLoginBySSOLoginAndDomain(String login, String pwd, String domain, final IContinuation cont) {
//
//        AsyncCallback<SystemUserBean> myCallBack = new StandardAsyncCallback<SystemUserBean>("communitaction error in login" /* I18N: no:err-fail-in */) {
//            @Override
//            public void onSuccess(SystemUserBean result) {
//                loggedUser = result;
//                cont.doIt();
//            }
//        };
//
//        FoxyRpcRequestBuilder.setLoggedUserName(login);
//        getService().performUserLoginBySSOLoginAndDomain(login, pwd, domain, myCallBack);
//    }
    public static boolean isUserLoggedIn() {
        return loggedUser != null;
    }

    public static boolean userExistButDisabled() {
        return userExistButDisabled;
    }

    public static String getLoggedUserLoginName() {
        return isUserLoggedIn() ? loggedUser.loginName : null;//loggedUser.name : null;
    }

    public static String getLoggedUserName() {
        return isUserLoggedIn() ? (BaseUtils.isStrEmptyOrWhiteSpace(loggedUser.userName) ? loggedUser.loginName : loggedUser.userName) : null;
    }

    public static Integer getLoggedUserId() {
        return isUserLoggedIn() ? loggedUser.id : null;
    }

    public static String getLoggedUserAvatar() {
        return isUserLoggedIn() ? loggedUser.avatar : null;
    }

    public static void setLoggedUserAvatar(String avatar) {
        if (isUserLoggedIn()) {
            loggedUser.avatar = avatar;
        }
    }

    public static boolean isBikUserLoggedIn() {
        return isUserLoggedIn(); //&& loggedUser.userId != null;
    }

    /**
     * @return pg- zwraca, czy jest zalogowany jako użytkownik społecznościowy
     */
    public static boolean isSystemUserBikUserLoggedIn() {
        return isUserLoggedIn() && loggedUser.userId != null;
    }

    public static Integer getLoggedBikUserId() {
        return isUserLoggedIn() ? loggedUser.userId : null;
    }

    public static boolean isBranchAuthorLoggedIn(String branchId) {
        for (String authorBranchId : loggedUser.userAuthorBranchIds) {
            if (branchId.startsWith(authorBranchId)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isBranchCreatorLoggedIn(String branchId) {
        for (String creatorBranchId : loggedUser.userCreatorBranchIds) {
            if (branchId.startsWith(creatorBranchId)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isBranchNonPublicUserLoggedIn(String branchId) {
        for (String nonPublicUserBranchId : loggedUser.nonPublicUserBranchIds) {
            if (branchId.startsWith(nonPublicUserBranchId)) {
                return true;
            }
        }
        return false;
    }

    //ww-authors: zmieniam na isEffectiveExpert - czyli jest ekspertem lub
    // adminem (sys lub app)
    public static boolean isEffectiveExpertLoggedIn() {
        //ww->authors
//        return isUserLoggedIn() && BIKCenterUtils.userHasRightRoleWithCode(loggedUser, "Expert" /* I18N: no */);
        return BIKRightRoles.isSysUserEffectiveExpert(loggedUser);
    }

    public static boolean isEffectiveExpertLoggedInEx(String actionCode) {

        return BIKClientSingletons.isNodeActionEnabledByCustomRightRoles(actionCode, new ICheckEnabledByDesign() {

            @Override
            public boolean isEnabledByDesign() {
                return BIKClientSingletons.isEffectiveExpertLoggedIn();
            }
        });
    }

    public static boolean isAdminOrExpertOrAuthorOrCreator(String actionCode, Integer treeId) {
        final Integer treeIdOfCurrentTab = (treeId != null ? treeId : getTreeIdOfCurrentTab());
        return isNodeActionEnabledByCustomRightRoles(actionCode, treeIdOfCurrentTab, true, null, new ICheckEnabledByDesign() {

            @Override
            public boolean isEnabledByDesign() {

                return isSysAdminLoggedIn() || isAppAdminLoggedIn() || isEffectiveExpertLoggedIn() || isLoggedUserAuthorOfAllTree(treeIdOfCurrentTab)
                        || isLoggedUserCreatorOfAllTree(treeIdOfCurrentTab);
            }
        });
    }

    public static boolean isNodeAuthorOrEditorLoggedIn(EntityDetailsDataBean data) {
        boolean isAuthotOfTheNode = false;
        Integer bikUserId = getLoggedBikUserId();
        if (data != null && data.author != null && bikUserId != null && bikUserId == data.author.authorId) {
            isAuthotOfTheNode = true;
        }

        return data != null && (isAuthotOfTheNode || isEditorLoggedIn(data));
    }

    public static boolean isEditorLoggedIn(EntityDetailsDataBean data) {
        return data != null && data.isEditor;
    }

    public static boolean checkNonPublicUserRights(int treeId, String branchIds) {
        return isLoggedUserIsNonPublicUserInTree(treeId) || isBranchNonPublicUserLoggedIn(branchIds);
    }

    public static boolean checkAuthorRights(int treeId, String branchIds) {
        return isLoggedUserAuthorOfTree(treeId) || isBranchAuthorLoggedIn(branchIds);
    }

    public static boolean checkCreatorRights(int treeId, String branchIds) {
        return isLoggedUserCreatorOfTree(treeId) || isBranchCreatorLoggedIn(branchIds);
    }

    public static SystemUserBean getLoggedUserBean() {
        return loggedUser;
    }

    public static boolean isLoggedUserAuthorOfTree(int treeID) {
        if (isUserLoggedIn() && loggedUser.userAuthorTreeIDs != null && loggedUser.userAuthorTreeIDs.contains(treeID)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isLoggedUserAuthorOfAllTree(int treeID) {
        if (isUserLoggedIn() && loggedUser.userAuthorAllTreeIDs != null && loggedUser.userAuthorAllTreeIDs.contains(treeID)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isLoggedUserCreatorOfTree(int treeID) {
        if (isUserLoggedIn() && loggedUser.userCreatorTreeIDs != null && loggedUser.userCreatorTreeIDs.contains(treeID)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isLoggedUserCreatorOfAllTree(int treeID) {
        if (isUserLoggedIn() && loggedUser.userCreatorAllTreeIDs != null && loggedUser.userCreatorAllTreeIDs.contains(treeID)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isLoggedUserIsNonPublicUserInTree(int treeID) {
        if (isUserLoggedIn() && loggedUser.nonPublicUserTreeIDs != null && loggedUser.nonPublicUserTreeIDs.contains(treeID)) {
            return true;
        } else {
            return false;
        }
    }

//    public static void setLoggedUserBean(UserBean user) {
//        loggedUser = user;
//    }
    public static List<NodeKindBean> getNodeKinds() {
        return nodeKinds;
    }

//    public static List<BikRoleForNodeBean> getBikRolesForNode() {
//        return bikRolesForNode;
//    }
//    private static void showAppMessage(String info, final HTML label, boolean isError) {
//        String diagMsgKind = ClientDiagMsgs.APP_MSG_KIND + ":" + (isError ? "error" : "info");
//        if (ClientDiagMsgs.isShowDiagEnabled(diagMsgKind)) {
//            ClientDiagMsgs.showDiagMsg(diagMsgKind, info);
//        }
//
//        StatusBarNotificationAnimator.showAppMessage(info, label, isError);
//
////        Animation ani = getInfoWidgetAnimation(label);
////
////        if (ani != null) {
////            ani.cancel();
////        }
////
////        label.setText(info);
////        label.setVisible(true);
////        label.getElement().getStyle().setOpacity(1);
////
////        ani = new Animation() {
////            @Override
////            protected void onUpdate(double progress) {
////                label.getElement().getStyle().setOpacity(1 - progress);
////                if (progress == 1) {
////                    label.setVisible(false);
////                    setInfoWidgetAnimation(label, null);
////                }
////            }
////        };
////
////        setInfoWidgetAnimation(label, ani);
////
////        ani.run(2000, Duration.currentTimeMillis() + 3000);
//    }
    public static void setTabSelector(ITabSelector<Integer> tabSelector) {
        BIKClientSingletons.tabSelector = tabSelector;
    }

    public static ITabSelector<Integer> getTabSelector() {
        return tabSelector;
    }

    public static EDPForms getEDPForms() {
        return forms;
    }

    public static GUIResizer getGUIResizer() {
        return guiResizer;
    }

    public static void registerResizeSensitiveWidget(final IResizeSensitiveWidget rsw,
            final int originalHeight) {
        guiResizer.registerResizeSensitiveWidget(rsw, originalHeight);
    }

    public static void registerResizeSensitiveWidgetByHeight(IsWidget w,
            final int originalHeight) {
        guiResizer.registerResizeSensitiveWidgetByHeight(w, originalHeight);
    }

    public static RootPanel getMainBodyRootPanel() {
        return RootPanel.get("mainBody");
    }

    public static void addOrRemoveAdditionalSearchPagePhraseTb(boolean doAdd, TextBox tb) {
        //searchPagePhraseTb = tb;
        if (doAdd) {
            additionalSearchPagePhraseTbs.add(tb);
        } else {
            additionalSearchPagePhraseTbs.remove(tb);
        }
    }

    public static void clearGlobalSearchTb() {
        globalSearchTb.setText("");
    }

    public static void setGlobalSearchTb(String txt) {
        globalSearchTb.setText(txt);
    }

    public static TextBox getGlobalSearchTextBox() {
        if (globalSearchTb == null) {
            Element editSearchEle = GWTUtils.getElementById("editSearchTop");
            globalSearchTb = TextBox.wrap(editSearchEle);

            globalSearchTb.addKeyUpHandler(new KeyUpHandler() {
                public void onKeyUp(KeyUpEvent event) {
                    for (TextBox tb : additionalSearchPagePhraseTbs) {
                        tb.setText(globalSearchTb.getText());
                    }
                }
            });
        }
        return globalSearchTb;
    }

    public static NodeKindBean getNodeKindById(Integer nodeKindId) {
        return nodeKindsMap.get(nodeKindId);
    }

    public static String getNodeKindCodeById(int nodeKindId) {
        return getNodeKindById(nodeKindId).code;
    }
    protected static Map<String, AppPropBean> appPropMap = new HashMap<String, AppPropBean>();

    public static String getAppPropVal(String propName) {
        AppPropBean apb = appPropMap.get(propName);
        return apb == null ? null : apb.val;
    }

    public static boolean isAppPropEditable(String propName) {
        AppPropBean apb = appPropMap.get(propName);
        return apb == null ? false : apb.isEditable > 0;
    }

    public static boolean isMultiXMode() {
        return isMultiXMode;
    }

    public static void setupAppProps(List<AppPropBean> appPropBeans, boolean isMultiXMode) {
        //BIKClientSingletons.appPropBeans = appPropBeans;
        BIKClientSingletons.isMultiXMode = isMultiXMode;
        BIKClientSingletons.appPropsBean = BIKCenterUtils.readAppPropBeans(appPropBeans);

        appPropMap.clear();
        for (AppPropBean apb : appPropBeans) {
            appPropMap.put(apb.name, apb);
        }

        if (!BaseUtils.isStrEmptyOrWhiteSpace(developerModeStr)) {
            String paramName = developerModeStr.trim();

            isDevModeEx = BaseUtils.safeEquals(Window.Location.getParameter(paramName), paramName);
        } else {
            isDevModeEx = false;
        }

//        ClientDiagMsgs.setClientDiagMsgsEnabled(appPropsBean.developerMode);
        ClientDiagMsgs.setClientDiagMsgsEnabled(isDeveloperMode());
        // allowedShowDiagKinds =
        ClientDiagMsgs.setAllowedShowDiagKinds(
                //BaseUtils.splitUniqueBySep(sdb.allowedShowDiagKinds, ",", true)
                appPropsBean.allowedShowDiagKindSet);
        // disallowedShowDiagKinds =
        ClientDiagMsgs.setDisallowedShowDiagKinds(appPropsBean.disallowedShowDiagKindSet);
        ClientDiagMsgs.clearDiagMsgs();
        availableConnectors = BaseUtils.splitBySep(appPropsBean.availableConnectors, ",", true);
    }

    public static Collection<TreeBean> getTrees() {
        return treesMap.values();
    }

    public static Collection<TreeBean> getAllTrees() {
        return allTrees;
    }

    public static NodeKindBean getNodeKindBeanByCode(String code) {
        return nodeKindsByCodeMap.get(code);
    }

    public static Map<String, NodeKindBean> getNodeKindsByCodeMap() {
        return nodeKindsByCodeMap;
    }

    //public static boolean ignoreDifferentUserOnce;
    public static void markToIgnoreDifferentUserInNextCall() {
        FoxyRequestCallback.setOptCallContextForHookInNextCall(true);
    }

    public static void setStartupData(StartupDataBean sdb) {

        IBiksDynamicI18nMessages dm = GWT.create(IBiksDynamicI18nMessages.class);

        if (sdb.i18nTranslations != null) {

            for (Entry<String, String> e : sdb.i18nTranslations.entrySet()) {
                String varName = e.getKey();

                I18nMessage msg = dm.find(varName);

                if (msg == null) {
                    if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$MISSING_I18N_VAR)) {
                        ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$MISSING_I18N_VAR, "no such var: [" + varName + "]");
                    }
                    continue;
                }

                msg.override(e.getValue());
            }
        }

        //ignoreDifferentUserOnce = true;
        developerModeStr = sdb.developerModeStr;
        canSendMailViaServer = sdb.canSendMails;
        useSimpleMsgForUnloggedUser = sdb.useSimpleMsgForUnloggedUser;

        setupAppProps(sdb.appProps, sdb.isMultiMode);

        disableGuestMode = sdb.disableGuestMode;
        loggedUser = sdb.loggedUser;

        FoxyRpcRequestBuilder.setLoggedUserName(loggedUser == null ? null : loggedUser.loginName);

        userExistButDisabled = sdb.userExistButDisabled;
        ssoUserName = sdb.ssoUserName;
        isMultiDomainMode = sdb.isMultiDomainMode;
        //developerMode = sdb.developerMode;
        nodeKindsMap = BaseUtils.makeBeanMap(sdb.nodeKinds);
        adDomains = BaseUtils.splitBySep(sdb.adDomains, ",");

        nodeKindsByCodeMap = new LinkedHashMap<String, NodeKindBean>();
        for (NodeKindBean nkb : sdb.nodeKinds) {
            nodeKindsByCodeMap.put(nkb.code, nkb);
        }

        for (ConnectionParametersJdbcBean jdbcConnectionType : sdb.jdbcConnectionTypes) {
            jdbcSourceTypes.put(jdbcConnectionType.jdbcSourceName, jdbcConnectionType);
        }
        List<TreeBean> treesAccessibleByGrants = new ArrayList<TreeBean>();

        for (TreeBean tb : sdb.trees) {
            if (tb.isAccessibleByGrants) {
                treesAccessibleByGrants.add(tb);
            }
        }

        allTrees = sdb.trees;
        sdb.trees = treesAccessibleByGrants;

        treeTabAutoRefreshLevel = BaseUtils.tryParseInt(getAppPropVal("treeTabAutoRefreshLevel"));

        treesMap = BaseUtils.makeBeanMap(sdb.trees);
        allTreesMap = BaseUtils.makeBeanMap(allTrees);
        allTreesByCodeMap = new HashMap<String, TreeBean>();
        for (TreeBean tb : allTrees) {
            allTreesByCodeMap.put(tb.code, tb);
        }

        nodeKinds = sdb.nodeKinds;
        treeKinds = sdb.treeKinds;
        trees = sdb.trees;
        treeKindsAsSet = new HashSet<String>();
        treeKindCaptions = new LinkedHashMap<String, String>();
        treeKindsByCode = new HashMap<String, TreeKindBean>();
        treeIcons = new HashMap<Pair<String, String>, String>();
        for (TreeIconBean treeIconBean : sdb.treeIcons) {
            treeIcons.put(new Pair<String, String>(treeIconBean.treeCode, treeIconBean.nodeKindCode), treeIconBean.icon);
        }
        // statyczne wrzucenie tree kindów
        //i18n-default: wtf?
        treeKindCaptions.put(BIKGWTConstants.TREE_KIND_GLOSSARY, I18n.glosariusz_wtf.get() /* I18N:  */);
        treeKindCaptions.put(BIKGWTConstants.TREE_KIND_DICTIONARY, I18n.slownik_wtf.get() /* I18N:  */);
        //treeKindCaptions.put(BIKGWTConstants.TREE_KIND_DICTIONARY_DWH, I18n.slownik_dwh_wtf.get() /* I18N:  */);
        treeKindCaptions.put(BIKGWTConstants.TREE_KIND_USERS, I18n.uzytkownicy_wtf.get() /* I18N:  */);
        treeKindCaptions.put(BIKGWTConstants.TREE_KIND_DOCUMENTS, I18n.dokumentacja_wtf.get() /* I18N:  */);
        treeKindCaptions.put(BIKGWTConstants.TREE_KIND_BLOGS, I18n.blogi_wtf.get() /* I18N:  */);
        if (sdb.isConfluenceActive) {
            treeKindCaptions.put(BIKGWTConstants.TREE_KIND_CONFLUENCE, I18n.confluenceName.get() /* I18N:  */);
        }
        //i18n-default:
        for (TreeKindBean treeKindBean : treeKinds) {
            treeKindsAsSet.add(treeKindBean.code);
            treeKindCaptions.put(treeKindBean.code, treeKindBean.caption);
            treeKindsByCode.put(treeKindBean.code, treeKindBean);
        }
//        bikRolesForNode = sdb.bikRolesForNode;
        menuNodes = sdb.menuNodes;

//        MenuNodeBean extraMnb = new MenuNodeBean(1000000001, "aqq", null, "#xx", 1000000001);
//        menuNodes.add(extraMnb);
        treeCodeToMenuPath = createTreeCodeToMenuPathMap(getExtendedMenuNodes(
                menuNodes, treesMap.values(),
                nodeKindsByCodeMap,
                true, MenuBottomLevel.BikPages, false, false));
        inactiveTabs = sdb.inactiveTabs;
        systemAttribute = sdb.systemAttribute;
        allAttributesForNodeKindId = sdb.allAttributesForNodeKindId;
        mssqlExProps = sdb.mssqlExProps;

        attrNameTranslations = sdb.translationsGeneric.get(BIKConstants.TRANSLATION_KIND_ADEF);
        attrCatNameTranslations = sdb.translationsGeneric.get(BIKConstants.TRANSLATION_KIND_ADCAT);
        //sdb.attrNameTranslations;

        openReportLink = sdb.openReportLink;
        reportTreeCodes = sdb.reportTreeCodes;

        for (Entry<Integer, List<AttributeBean>> nkAts : systemAttribute.entrySet()) {
            Set<String> hiddenNames = new HashSet<String>();
            for (AttributeBean ab : nkAts.getValue()) {
                if (ab.isVisible == 0) {
                    hiddenNames.add(ab.atrName);
                }
            }
            hiddenAttrNamesByNodeKindId.put(nkAts.getKey(), hiddenNames);
        }

        attrHintsMap = new LinkedHashMap<String, AttrHintBean>();
        for (AttrHintBean attr : sdb.attrHints) {
            attr.hint = fixAttrHintValue(attr.hint);
            attrHintsMap.put(attr.name, attr);
        }
//        attrDefAndCategory = sdb.attrDefAndCategory;
        //VisualValueHelper.init();
        //                BaseUtils.projectToMap(sdb.trees, new Projector<TreeBean, Integer>() {
        //
        //            public Integer project(TreeBean val) {
        //                return val.id;
        //            }
        //        });
        HTML diagMsgsPanel = HTML.wrap(GWTUtils.getElementById("diagMsgsPanel"));
        //GWTUtils.setStyleAttribute(diagMsgsPanel, "marginRight", "30px");

        diagMsgsPanel.setVisible(isDeveloperMode());
        if (isDeveloperMode()) {
            diagMsgsPanel.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    ClientDiagMsgs.showDiagMsgsInDialog();
                }
            });
        }
        treeNotHiddenKind = new ArrayList<String>();
        treeCodesWithTreeKind = new HashSet<String>();

        treeByCodeMap = BaseUtils.makeHashMapOfSize(sdb.trees.size());

        for (TreeBean tb : sdb.trees) {
            treeByCodeMap.put(tb.code, tb);

            treeNotHiddenKind.add(tb.treeKind);
            if (tb.treeKind.equals(BIKGWTConstants.TREE_KIND_USERS)) {
                treeCodesWithTreeKind.add(tb.code);
            }
        }
        if (treeKinds.size() > 0) {
            treeNotHiddenKind.add(BIKGWTConstants.TREE_KIND_DYNAMIC_TREE);
        }

        attrDict = sdb.attrsDict;
//        attrsSearchable = sdb.attrsSearchable;
        changedRanking = sdb.changedRanking;
//        lastRankingChangeTimeStamp = sdb.changedRanking.timeStamp;
        welcomeMsgForUser = sdb.welcomeMsgForUser;
        welcomeMsgForVersion = sdb.welcomeMsgForVersion;
        biksDatabaseName = sdb.biksDatabaseName;
        allRightRolesByCode = sdb.allRightRolesByCode;
        isUserCreatorOfThisDb = sdb.isUserCreatorOfThisDb;

        customRightRoles = sdb.customRightRoles;
        customRightRoleRegularUserId = null;
        if (customRightRoles != null) {
            for (CustomRightRoleBean crrb : customRightRoles) {
                if (BaseUtils.safeEquals(crrb.code, "RegularUser")) {
                    customRightRoleRegularUserId = crrb.id;
                    break;
                }
            }
        }
        allRoles = sdb.allRoles;
        customRightRoleByIdMap = BaseUtils.makeBeanMap(customRightRoles);

        nodeActionCodeToIdMap = sdb.nodeActionCodeToIdMap;
        customRightRolesActionsInTreeRoots = sdb.customRightRolesActionsInTreeRoots;

        isRegisterNewNodeActionsEnabled = BIKCenterUtils.getAppPropValAsBool(getAppPropVal("registerNewNodeActions"));

        treeSelectorForRegularUserCrr = sdb.treeSelectorForRegularUserCrr;

        showGrantsInTestConnection = sdb.showGrantsInTestConnection;

        suggestSimilarNodes = BIKCenterUtils.getAppPropValAsBool(getAppPropVal("suggestSimilarNodes"));
    }

    public static boolean suggestSimilarNodes() {
        return suggestSimilarNodes;
    }

    public static String getTreeSelectorForRegularUserCrr() {
        return treeSelectorForRegularUserCrr;
    }

    public static void setLogoOverLay() {
        if (!BaseUtils.isStrEmptyOrWhiteSpace(appPropsBean.logoOverlay)) {
            boolean needChange = false;

            StringBuilder sb = new StringBuilder("<a href=\"#myBIK\"><img src=\"images/bik_logo_1n.png\" alt=\"BIKS\" height=\"40px\" border=\"0\"/>");
            try {
                List<String> overlays = BaseUtils.splitBySep(appPropsBean.logoOverlay, ";");
                for (String overlay : overlays) {
                    if (BaseUtils.isStrEmptyOrWhiteSpace(overlay)) {
                        continue;
                    }
                    Date now = new Date();
                    int nowYear = SimpleDateUtils.getYear(now);
                    int nowMonth = SimpleDateUtils.getMonth(now);
                    int nowDay = SimpleDateUtils.getDay(now);
                    List<String> items = BaseUtils.splitBySep(overlay, "|");
                    //
                    String from = items.get(1);
                    Pair<String, String> fromPair = BaseUtils.splitString(from, ".");
                    int fromMonth = Integer.parseInt(fromPair.v1);
                    int fromDay = Integer.parseInt(fromPair.v2);
                    String to = items.get(2);
                    Pair<String, String> toPair = BaseUtils.splitString(to, ".");
                    int toMonth = Integer.parseInt(toPair.v1);
                    int toDay = Integer.parseInt(toPair.v2);
                    if (fromMonth > nowMonth || (fromMonth == nowMonth && fromDay > nowDay)) {
                        nowYear--;
                    }
                    Date fromDate = SimpleDateUtils.newDate(nowYear, fromMonth, fromDay);
                    if (toMonth < fromMonth || (toMonth == fromMonth && toDay < fromDay)) {
                        nowYear++;
                    }
                    Date toDate = SimpleDateUtils.newDate(nowYear, toMonth, toDay);
                    if (now.after(fromDate) && now.before(toDate)) {
                        sb.append("<img src='").append(items.get(0)).append("' style=\"margin-left: ").append(items.get(3)).append("px; margin-bottom: ").append(items.get(4)).append("px\" border=\"0\" class=\"treeIconP\"/>");
                        needChange = true;
                    }
                }
                sb.append("</a>");
//                Window.alert("string = " + sb.toString());
            } catch (Exception e) {
                needChange = false; // zla konfiguracja - nie podmieniamy loga
            }
            if (needChange) {
                RootPanel logoPanel = RootPanel.get("logoBox4NaqNaq");
                logoPanel.clear(true);
                logoPanel.add(new InlineHTML(sb.toString()));
            }
        }
    }

    public static TreeBean getTreeBeanById(Integer treeId) {
        return allTreesMap.get(treeId);
    }

    public static TreeBean getTreeBeanByCode(String treeCode) {
        return treeByCodeMap.get(treeCode);
    }

    public static boolean isCurrentTabTree() {
        ITabSelector<Integer> ts = getTabSelector();
        if (ts == null || ts.getCurrentTab() == null || ts.getCurrentTab().getId() == null) {
            return false;
        }
        return getTreeBeanByCode(ts.getCurrentTab().getId()) != null;
    }

    public static String getTreeCodeById(Integer treeId) {
//        TreeBean tree = treesMap.get(treeId);
        TreeBean tree = getTreeBeanById(treeId);
        return tree == null ? null : tree.code;
    }

    public static boolean mustShowSSOLoginDialog() {
        return appPropsBean != null && appPropsBean.showSSOLoginDialog;
    }

    public static boolean disableSSOForLoginOnDialog() {
        return appPropsBean != null && appPropsBean.disableSSOForLogin;
    }

    public static boolean isDeveloperMode() {
        return appPropsBean != null && appPropsBean.developerMode || isDevModeEx;
    }

    public static boolean isInUltimateEditMode() {
        return isDemoVersion() && isDeveloperMode()
                && isSysAdminLoggedInEx(BIKConstants.ACTION_IS_IN_ULTIMATE_EDIT_MODE);
    }

    public static String getDBVersion() {
        return appPropsBean.bik_ver;
    }

    public static List<MenuNodeBean> getMenuNodes() {
        return menuNodes;
    }

    public static List<MenuNodeBean> getExtendedMenuNodes(
            List<MenuNodeBean> menuNodes, Collection<TreeBean> trees,
            Map<String, NodeKindBean> nodeKindsByCodeMap,
            boolean expandDynamicTrees,
            MenuBottomLevel keepAsLeaves, boolean addUltimateRootNode,
            boolean hideReduntantGroups) {
        return MenuExtender.extend(menuNodes, trees, nodeKindsByCodeMap,
                expandDynamicTrees, keepAsLeaves, addUltimateRootNode, hideReduntantGroups);
    }

    //ww: od wersji v1.1.9.14 odpowiednie drzewka są ukryte i już - we właściwym
    // miejscu kodu jest rozpoznawanie aby nie pokazywać elementów GUI dotyczących
    // niewidocznych drzewek
//    public static boolean isHideDictionaryRootTab() {
//        return false; // appPropsBean.hideDictionaryRootTab;
//    }
    public static boolean isDemoVersion() {
        return isMultiXMode || appPropsBean.isDemoVersion;
    }

    public static boolean isIconHomeInMenu() {
        return appPropsBean.isIconHomeInMenu;
    }

    public static boolean isDisableRankingPage() {
        return appPropsBean != null ? appPropsBean.disableRankingPage : true;
    }

    public static boolean isDQMRunTestAsFunction() {
        return appPropsBean.dqmRunTestAsFunction;
    }

    public static boolean isRenderAttributesAsHTMLMode() {
        return appPropsBean.renderAttributesAsHTML;
    }

    public static boolean mustHaveSocialUser() {
        return appPropsBean.mustHaveSocialUser;
    }

    public static boolean allowDuplicateJoinedObjs() {
        return appPropsBean.allowDuplicateJoinedObjs;
    }

    public static boolean showExportJasperReports() {
        return appPropsBean.showExportJasperReports;
    }

    public static boolean useProgramerEditMode() {
        return appPropsBean.useProgramerEditMode;
    }

    public static boolean showAddChildNodeBtn() {
        return appPropsBean.showAddChildNodeBtn;
    }

    public static boolean showAttributesBeforeChildrenExtradataWidget() {
        return appPropsBean.showAttributesBeforeChildrenExtradataWidget;
    }

    public static Pair<Integer, Integer> customDialogPercentSize() {
        String customDialogPercentSize = appPropsBean.customDialogPercentSize;
        if (BaseUtils.isStrEmptyOrWhiteSpace(customDialogPercentSize)) {
            return null;
        }
        Pair<String, String> dialogWidhtHehigt = BaseUtils.splitString(customDialogPercentSize, ";");
        int width = BaseUtils.tryParseInt(dialogWidhtHehigt.v1);
        int height = BaseUtils.isStrEmpty(dialogWidhtHehigt.v2) ? width : BaseUtils.tryParseInt(dialogWidhtHehigt.v2);

        return new Pair<Integer, Integer>(width, height);

    }

    public static boolean notExpandingJoinedObjsFooter() {
        return appPropsBean.notExpandingJoinedObjsFooter;
    }

    public static boolean isAlternativeJoinedPanelViewEnabled() {
        return appPropsBean.isAlternativeJoinedPanelViewEnabled;
    }

//    public static boolean isAllowDuplicateJoinedObjs() {
//        return appPropsBean.allowDuplicateJoinedObjs;
//    }
    public static String getNodeKindCaptionByCode(String kindCode) {
        NodeKindBean nkb = getNodeKindBeanByCode(kindCode);
        return nkb != null ? nkb.caption : kindCode;
    }

    public static String getNodeKindIconNameByCode(String kindCode, String treeCode) {
        NodeKindBean nkb = getNodeKindBeanByCode(kindCode);
        String baseIcon = nkb != null ? nkb.iconName : "report" /* i18n: no */;
        if (treeCode == null) {
            return baseIcon;
        }
        String optTreeIcon = getTreeOptIcon(treeCode, kindCode);
        return optTreeIcon != null ? optTreeIcon : baseIcon;
    }

    public static String getHtmlForNodeKindIconWithText(String kindCode, String treeCode, String txt) {
        return "<" + "img style='vertical-align:text-top' border='0' src='images" /* I18N: no */ + "/"
                + BIKClientSingletons.getNodeKindIconNameByCode(kindCode, treeCode) + "." + "gif' width='16' " + "height" /* I18N: no */ + "='16' />" //&nbsp;
                + " <span>" + BaseUtils.encodeForHTMLTag(txt) + "</span>";
    }

//    public static List<String> availableConnectors() {
//        return availableConnectors;
//    }
    public static boolean isConnectorAvailable(String connectorName) {
        return availableConnectors.contains(connectorName);
    }

    public static String mergeWithSepExAndTrim(String valueOpt) {
        List<String> attrOpt = new ArrayList<String>();
        for (String ao : BaseUtils.splitBySep(valueOpt, ",")) {
            ao = ao.trim();
            if (!BaseUtils.isStrEmptyOrWhiteSpace(ao) && !attrOpt.contains(ao)) {
                attrOpt.add(ao);
            }
        }
        return BaseUtils.mergeWithSepEx(attrOpt, ",");
    }

    public static PushButton createFollowButtonEx(final String nodeTreeCode, final int nodeId,
            boolean widgetsActive, String btnStyle) {
        final PushButton followBtn = new PushButton(new Image("images/link_arrows.gif" /* I18N:  */));
        followBtn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                BIKClientSingletons.getTabSelector().submitNewTabSelection(nodeTreeCode, nodeId, true);
            }
        });
//        followBtn.setEnabled(widgetsActive);
        followBtn.setVisible(widgetsActive);
        followBtn.addStyleName(btnStyle);
        followBtn.setTitle(I18n.przejdz.get() /* I18N:  */);
        return followBtn;
    }

    public static PushButton createFollowButton(final String nodeTreeCode, final int nodeId,
            boolean widgetsActive) {
        return createFollowButtonEx(nodeTreeCode, nodeId, widgetsActive, "followBtn");
    }

    public static PushButton createLookupButton(final String nodeTreeCode, final int nodeId,
            final Integer linkingParentId, boolean widgetsActive) {
        PushButton lookupBtn = new PushButton(new Image("images/loupe.gif" /* I18N:  */));
        lookupBtn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {

                EntityDetailsPaneDialog edpd = new EntityDetailsPaneDialog();
                edpd.buildAndShowDialog(nodeId, linkingParentId, nodeTreeCode);
//                edpd.fetchData();
            }
        });

        lookupBtn.setTitle(I18n.podglad.get() /* I18N:  */);
        lookupBtn.setEnabled(widgetsActive);
        lookupBtn.addStyleName("loupeBtn"/*
         * "zeroBtn loupe"
         */);
        return lookupBtn;
    }

    public static PushButton createDeleteButton(final ChildrenBean child, final String eventName) {
        PushButton button = new PushButton(new Image("images/trash_gray.gif" /* I18N: no */));
        button.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

                getService().getBikEntityDetailsData(child.nodeId, null, false, new StandardAsyncCallback<EntityDetailsDataBean>() {

                    public void onSuccess(final EntityDetailsDataBean resultEddb) {
                        HTML element = new HTML(
                                I18n.czyNaPewnoUsunacZdarzenie.get() /* I18N:  */ + ": <div class='Delete-Element'><b>" + eventName + "</b></div>");
                        element.setStyleName("komunikat" /* I18N: no:css-class */);
                        new ConfirmDialog().buildAndShowDialog(element, I18n.usun.get() /* I18N:  */, new IContinuation() {
                            @Override
                            public void doIt() {
                                getService().deleteTreeNode(resultEddb.node, new StandardAsyncCallback<List<String>>("Failure on" /* I18N: no */ + eventName + " deleteNode") {
                                    @Override
                                    public void onSuccess(List<String> result) {
                                        showInfo(I18n.usunietoZdarzenie.get() /* I18N:  */);
                                        getTabSelector().invalidateNodeData(resultEddb.node.parentNodeId);
                                    }
                                });
                            }
                        });
                    }
                });

            }
        });
        button.setStyleName("Attr" /* I18N: no */ + "-trashBtn");
//        button.setVisible(true);
        return button;
    }

    public static BikCustomButton createLabelLookupButton(String caption, final String nodeTreeCode, final int nodeId,
            final Integer linkingParentId, boolean widgetsActive) {
        return createHtmlLabelLookupButton(BaseUtils.encodeForHTMLTag(caption), nodeTreeCode, nodeId, linkingParentId, widgetsActive, null);
    }

    public static BikCustomButton createHtmlLabelLookupButton(String captionHtml, final String nodeTreeCode, final int nodeId,
            final Integer linkingParentId, boolean widgetsActive, final IContinuation refreshContinuation) {
        return createHtmlLabelLookupButton(captionHtml, nodeTreeCode, nodeId, linkingParentId, widgetsActive, refreshContinuation, true);
    }

    public static BikCustomButton createHtmlLabelLookupButton(String captionHtml, final String nodeTreeCode, final int nodeId,
            final Integer linkingParentId, boolean widgetsActive, final IContinuation refreshContinuation, final boolean addClickHandler) {
        BikCustomButton labelBtn = new BikCustomButton(htmlEntitiesDecode(captionHtml), "Blogs" /* I18N: no */ + "-blogTitle",
                new IContinuation() {
            public void doIt() {
                if (addClickHandler) {
                    EntityDetailsPaneDialog edpd = new EntityDetailsPaneDialog();
                    edpd.setRefreshContinuation(refreshContinuation);
                    edpd.buildAndShowDialog(nodeId, linkingParentId, nodeTreeCode);
                }
            }
        });
        labelBtn.setEnabled(widgetsActive);
        return labelBtn;
    }

    public static BikCustomButton createAtrributeLookupButton(String captionHtml, final Integer id, final String nameFrom, final String nameTo,
            boolean widgetsActive, final boolean addClickHandler) {
        BikCustomButton labelBtn = new BikCustomButton(captionHtml, "Blogs" /* I18N: no */ + "-blogTitle",
                new IContinuation() {
            public void doIt() {
                if (addClickHandler) {
                    getService().getJoinedAttributesLinked(id, new StandardAsyncCallback<List<JoinedObjAttributeLinkedBean>>() {

                        @Override
                        public void onSuccess(List<JoinedObjAttributeLinkedBean> result) {
                            new ViewJoinedAttributeLinkedDialog().buildAndShowDialog(nameFrom, nameTo, result);

                        }
                    });

                }
            }
        });
        labelBtn.setEnabled(widgetsActive);
        return labelBtn;
    }

    public static Image createIconForNodeKind(String nodeKindCode, String treeCode, String optCustomHint) {
        Image img = BikIcon.createIcon("images" /* I18N: no */ + "/" + BIKClientSingletons.getNodeKindIconNameByCode(nodeKindCode, treeCode) + "." + "gif" /* I18N:  */, "RelatedObj-ObjIcon");
        if (optCustomHint == null) {
            optCustomHint = getNodeKindCaptionByCode(nodeKindCode);
        }
        img.setTitle(optCustomHint);
        return img;
    }

    public static Map<String, AttrHintBean>/*
             * List<AttrHintBean>
             */ getAttrHints() {
        return attrHintsMap;
    }

    public static String getAttrHint(String name) {
        AttrHintBean ahb = attrHintsMap.get(name);
        return ahb != null ? ahb.hint : null;
    }

    public static boolean helpHasHint(String helpKey) { //[Misia] chwilowo zaslepka
        return true;
    }

    public static boolean attrHasHint(String name) {
        return getAttrHint(name) != null;
//        for (AttrHintBean a : attrHints) {
//            if (a.name.equals(name) && a.hint != null && !a.hint.trim().equals("")) {
//                return true;
//            }
//        }
//        return false;
    }

    public static String fixAttrHintValue(String hint) {
        //ww: tak powinno być
        if (BaseUtils.isHTMLTextEmpty(hint)) {
            hint = null;
        }
        //ww: a nie tak
//        if (hint != null) {
//            if (hint.trim().equals("<br>") || BaseUtils.isStrEmptyOrWhiteSpace(hint)) {
//                hint = null;
//            }
//        }
        return hint;
    }

    public static void updateAttrHint(String name, String hint) {
        attrHintsMap.get(name).hint = fixAttrHintValue(hint);
    }

    public static void addNewAttrToHintsMap(String name) {
        attrHintsMap.put(name, new AttrHintBean());
    }

    public static PushButton createHelpHintButton(final String helpKey, final String caption) {
        hintBtn = new PushButton(new Image("images/hint.gif" /* I18N:  */));
        hintBtn.setTitle(I18n.pomoc.get() /* I18N:  */);
        hintBtn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {

                BIKClientSingletons.getService().getHelpDescr(helpKey, null, new StandardAsyncCallback<HelpBean>() {
                    public void onSuccess(HelpBean result) {
                        new HelpDialog().buildAndShowDialog(result, helpKey, caption);
                    }
                });
            }
        });
        hintBtn.addStyleName("followBtn");
        return hintBtn;
    }

    public static Panel createHintedHTML(final String hintName, String text) {

        HorizontalPanel wrapper = new HorizontalPanel();
        HTML html = new HTML("<b>" + hintName + ":</b> ");
        wrapper.add(html);
        if (!BIKClientSingletons.hideHelpForAttr()) {
            Widget btn = createHintButton(hintName);

            wrapper.add(btn);

            setupDisplayOnHover(btn, new IContinuationWithReturn<Boolean>() {
                @Override
                public Boolean doIt() {
                    return attrHasHint(hintName);
                }
            });

            if (text != null) {
                wrapper.add(new HTML(text));
            }
        }
        wrapper.setStyleName("gp-html");
        return wrapper;
    }

    public static PushButton createHintButton(final String name) {

        hintBtn = new PushButton(new Image("images/hint.gif" /* I18N:  */));
        hintBtn.setTitle(I18n.wyjasnienie.get() /* I18N:  */);
        //hintBtn.setVisible(false);
        hintBtn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
//                Window.alert(getAttrHint(name));
                AttrHintDialog edpd = new AttrHintDialog();
                edpd.buildAndShowDialog(name, getAttrHint(name));
            }
        });
//        followBtn.setEnabled(widgetsActive);
        hintBtn.setVisible(attrHasHint(name));
        hintBtn.addStyleName("followBtn");
        return hintBtn;
    }

    public static boolean isSysAdminLoggedIn() {
//        return (isUserLoggedIn() && BIKCenterUtils.userHasRightRoleWithCode(getLoggedUserBean(), "Administrator" /* I18N: no */));
        return BIKRightRoles.isSysUserSysAdmin(loggedUser);
//                BaseUtils.safeEquals(getLoggedUserBean().kindName, "Administrator"));
    }

    public static boolean isSysAdminLoggedInEx(final String actionCode) {

        return isNodeActionEnabledByCustomRightRoles(actionCode, new ICheckEnabledByDesign() {

            @Override
            public boolean isEnabledByDesign() {
                return isSysAdminLoggedIn();
            }
        });
    }

    public static boolean isAppAdminLoggedIn() {
        return BIKRightRoles.isSysUserEffectiveAppAdmin(loggedUser);
    }

    public static boolean isAppAdminLoggedIn(String actionCode) {

        return isNodeActionEnabledByCustomRightRoles(actionCode, new ICheckEnabledByDesign() {

            @Override
            public boolean isEnabledByDesign() {
                return isAppAdminLoggedIn();//BIKRightRoles.isSysUserEffectiveAppAdmin(loggedUser);
            }
        });
        // return BIKRightRoles.isSysUserEffectiveAppAdmin(loggedUser);
    }

    public static String createLink(String caption, String href) {
        return createLinkEx(caption, href, null);
    }

    public static String createLinkEx(String caption, String href, String optClientFileName) {
        return "<a href='" + fixFileResourceUrlEx(BaseUtils.encodeForHTMLTag(href), optClientFileName) + "' target='_blank'>" + caption + "</a>";
    }

    public static <WHATEVER> boolean isBikPageEnabled(IBikPage<WHATEVER> bikPage) {

        String pageTabId = bikPage.getId();

        if (inactiveTabs.contains(pageTabId)) {
            return false;
        }

        boolean isAdminPage = pageTabId.startsWith("admin:"/* I18N: no */);
//        return !isAdminPage || isSysAdminLoggedIn() || isAppAdminLoggedIn() && bikPage instanceof IBikPageForAppAdmin;
        return !isAdminPage || isSysOrAppAdminLoggedInEx(pageTabId, bikPage);
    }

    protected static <WHATEVER> boolean isSysOrAppAdminLoggedInEx(String pageTabId, final IBikPage<WHATEVER> bikPage) {

        return isNodeActionEnabledByCustomRightRoles("#" + pageTabId, new ICheckEnabledByDesign() {

            @Override
            public boolean isEnabledByDesign() {
                return isSysAdminLoggedIn() || isAppAdminLoggedIn() && bikPage instanceof IBikPageForAppAdmin;
            }
        });

    }

    public static void replaceHrefInnerLinksFromDescrToInlineLabel(Map<Integer, Pair<String, String>> innerLinks, HTMLPanel descrTmp) {
        for (Entry<Integer, Pair<String, String>> entry : innerLinks.entrySet()) {
            final Pair<String, String> url = entry.getValue();
            replaceHrefFromDescrToInlineLabelToHideDialog(url.v2, entry.getKey(), descrTmp);
        }
    }

    //do linków w tekscie, aby po kliknięciu zamykało się okienko
    public static void replaceHrefFromDescrToInlineLabelToHideDialog(final String tabId, final int nodeId, HTMLPanel descr) {
        Element elem = descr.getElementById(nodeId + "$\"");
        if (elem != null) {
            InlineLabel inlineLbl = new InlineLabel(elem.getInnerHTML());
            inlineLbl.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    BaseActionOrCancelDialog.hideAllDialogs();
                    BIKClientSingletons.getTabSelector().submitNewTabSelection(tabId, nodeId, true);
                }
            });
            descr.addAndReplaceElement(inlineLbl, elem);
            inlineLbl.setStyleName("inlineLbl");
        }
    }
//    public static List<AttributeBean> getAttrDefAndCategory() {
//        return attrDefAndCategory;
//    }

    public static Map<Integer, List<Integer>> getAttributeDict() {
        return attrDict;
    }

    public static List<Integer> getProperTreeIdsWithoutOneTree(final int treeId) {
        return BIKCenterUtils.getProperTreeIdsWithoutOneTree(getTrees(), treeId);
    }

    public static List<Integer> getProperTreeIdsWithTreeKind(final String treeKind) {
        if (isTreeKindDynamic(treeKind)) {
            return getProperDynamicTreeIds();
        }
        return BIKCenterUtils.getProperTreeIdsWithTreeKind(getTrees(), treeKind);
    }

    public static List<Integer> getProperTreeIdsWithTreeKinds(Set<String> treeKinds) {
        List<Integer> treeIds = new ArrayList<Integer>();
        for (String treeKind : treeKinds) {
            treeIds.addAll(getProperTreeIdsWithTreeKind(treeKind));
        }
        return treeIds;
    }

    public static boolean isTreeKindDynamic(Set<String> treeKinds) {
        for (String treeKind : treeKinds) {
            if (isTreeKindDynamic(treeKind)) {
                return true;
            }
        }
        return false;
    }

    public static List<Integer> getProperDynamicTreeIds() {
        List<Integer> res = new ArrayList<Integer>();
        for (TreeBean treeBean : getTrees()) {
            if (treeKindsAsSet.contains(treeBean.treeKind/*code*/)) {
                res.add(treeBean.id);
            }
        }
        return res;
    }

    public static Set<String> getProperTreeCodesWithTreeKind(String treeKind) {
        return treeCodesWithTreeKind;
    }

    public static String getCancellableRequestTicket() {
        return cancellableRequestTicketPrefix + "_" + (cancellableRequestTicketNumber++);
    }

    public static boolean cancelRequestIfRunning(Request request, String ticket) {
        boolean isRunning = request != null;
        if (isRunning) {
            BIKClientSingletons.cancelServiceRequest(ticket);
            request.cancel();
        }
        return isRunning;
    }

    public static void cancelServiceRequest(String ticket) {
        BIKClientSingletons.getService().cancelDBJob(ticket, new StandardAsyncCallback<Boolean>() {
            public void onSuccess(Boolean result) {
                // no-op
            }
        });
    }

    public static Map<String, String> getAttrNameTranslations() {
        return attrNameTranslations;
    }

    public static String getTranslatedAttributeName(String defName) {
        return BaseUtils.nullToDef(attrNameTranslations.get(defName), defName);
    }

    public static String getTranslatedAttrCategoryName(String defName) {
        return BaseUtils.nullToDef(attrCatNameTranslations.get(defName), defName);
    }

    public static String makeUrlToDatabase(String dbName) {
        return makeUrlToDatabase(dbName, false);
    }

    public static String makeUrlToDatabase(String dbName, boolean doTrimHash) {
        UrlBuilder newUrl = Window.Location.createUrlBuilder();

        if (BaseUtils.isStrEmptyOrWhiteSpace(dbName)) {
            newUrl.removeParameter(MultixConstants.DB_NAME_PARAM);
        } else {
            newUrl.setParameter(MultixConstants.DB_NAME_PARAM, dbName);
        }
        if (doTrimHash) {
            newUrl.setHash("");
        }
        return newUrl.buildString();
    }

    public static String sanitizeForConfluenceContent(String properDescr) {
        if (properDescr == null || properDescr.trim().isEmpty()) {
            return properDescr;
        }
        StringBuilder sb = new StringBuilder();
        // krok 1: usuwanie <!CDATA[
        int startIndex = 0;
        int foundIndex = 0;
        int endIndex = 0;
        while (foundIndex != -1) {
            foundIndex = properDescr.indexOf("<![CDATA[", startIndex);
            if (foundIndex == -1) {
                break;
            }
            endIndex = properDescr.indexOf("]]>", startIndex + 9);
            sb.append(properDescr.substring(startIndex, foundIndex));
            sb.append(properDescr.substring(foundIndex + 9, endIndex));
            startIndex = endIndex + 3;
        }
        sb.append(properDescr.substring(startIndex, properDescr.length()));
        // krok 2: wstawiac linki do attachment/page/blog-post/space/url

        // krok 3: zamiana ac-image na img src
        // krok 4: zamiana ac-link na a href
        return sb.toString();
    }

    public static void callSearchBtn(TextBox searchTb, Widget optSearchBtn) {
        final IContinuation searchIC = new IContinuation() {
            public void doIt() {
//                BIKClientSingletons.getTabSelector().submitNewTabSelection(
//                        NewSearchBikPage.NEW_SEARCH_TAB_ID, null, true);
                BIKClientSingletons.getTabSelector().submitNewTabSelection(
                        SearchPage.SEARCH_PAGE_TAB_ID, null, true);
            }
        };
        GWTUtils.addEnterKeyHandler(searchTb, searchIC);

        if (optSearchBtn != null) {
            GWTUtils.addOnClickHandler(optSearchBtn, searchIC);
        }
    }

    private static Map<String, String> createTreeCodeToMenuPathMap(List<MenuNodeBean> menuNodes) {
        Map<String, String> map = new HashMap<String, String>();
        Map<Integer, MenuNodeBean> idToBeanMap = new HashMap<Integer, MenuNodeBean>();
        List<MenuNodeBean> listOfTreesInMenu = new ArrayList<MenuNodeBean>();
        for (MenuNodeBean menuNode : menuNodes) {
            idToBeanMap.put(menuNode.id, menuNode);
            if (menuNode.code != null && menuNode.code.startsWith("$") && menuNode.code.length() > 1) {
                listOfTreesInMenu.add(menuNode);
            }
        }
        for (MenuNodeBean treeInMenuBean : listOfTreesInMenu) {
            StringBuilder sb = new StringBuilder();
            createPathForTreeCode(treeInMenuBean, idToBeanMap, sb);
            String treeCodeWithSymbol = treeInMenuBean.code;
            map.put(treeCodeWithSymbol.substring(1), sb.toString());
        }
        return map;
    }

    private static void createPathForTreeCode(MenuNodeBean treeInMenuBean, Map<Integer, MenuNodeBean> idToBeanMap, StringBuilder sb) {
        if (treeInMenuBean == null) {
            return;
        }
        if (treeInMenuBean.parentNodeId != null) {
            createPathForTreeCode(idToBeanMap.get(treeInMenuBean.parentNodeId), idToBeanMap, sb);
        }
        sb.append(treeInMenuBean.name).append(FoxyCommonConsts.RAQUO_STR_SPACED);
    }

    public static List<NodeKindBean> getUploadableChildrenNodeKinds(String childrenNodes) {
        if (BaseUtils.isAnyStringEmptyOrWhiteSpace(childrenNodes)) {
            return null;
        }
        List<NodeKindBean> kindsToReturn = new ArrayList<NodeKindBean>();
        List<String> nodeKindCodes = BaseUtils.splitBySep(childrenNodes, ",", true);
        for (String nodeKindCode : nodeKindCodes) {
            kindsToReturn.add(getNodeKindBeanByCode(nodeKindCode));
        }
        return kindsToReturn;
    }

    public static DisabledUserApplyForAccountMsgsBean getDisabledUserApplyForAccountMsgs() {
        return appPropsBean.disabledUserApplyForAccountMsgsBean;
//        DisabledUserApplyForAccountMsgsBean res = new DisabledUserApplyForAccountMsgsBean();
//        res.headerMsg = "Możesz złożyć wniosek o dostęp do systemu BIKS dla poniższych ról:";
//        res.availableOptions = "Użytkownik DQM|Admin DQM";
//        res.footerMsg = "Kliknij ten link: <a href='mailto:Admin@Of@BIKS'>Wyślij wniosek o dostęp</a>, a następnie wyślij przygotowanego maila";
//        res.mailTo = "pmielanczuk@bssg.pl";
//        res.mailTitle = "Wniosek o dostęp do systemu BIKS dla %{ssoUserName}%";
//        res.mailBody = "Proszę o dostęp do systemu BIKS, dla ról:\r\n\r\n%{options1}%\r\n\r\nMój login domenowy to: %{ssoUserName}%.\r\n";
//        return res;
    }

    public static boolean isEnableAttributeUsagePage() {
        return appPropsBean.enableAttributeUsagePage;
    }

    public static boolean isBIAdminEnabled() {
        return availableConnectors.contains(PumpConstants.SOURCE_NAME_BIAdmin);
    }

    static boolean isEnableNodeHistory() {
        return appPropsBean.enableNodeHistory;
    }

    public static List<TreeBean> getDynamicTrees(List<TreeBean> allTrees) {
        List<TreeBean> dynamicTrees = new ArrayList<TreeBean>();
        for (TreeBean tb : allTrees) {
            String code = tb.treeKind;
            if (BIKClientSingletons.getTreeKindCaptionsMap().containsKey(code) && tb.isBuiltIn == 0) {
                dynamicTrees.add(tb);
            }
        }
        return dynamicTrees;
    }

    public static void callService2DeleteTmpFiles(Set<String> tmpFileName2Delete) {
        if (!BaseUtils.isCollectionEmpty(tmpFileName2Delete)) {
            BIKClientSingletons.getService().deleteTmpFiles(tmpFileName2Delete, new StandardAsyncCallback<Void>() {

                @Override
                public void onSuccess(Void result) {
                }

                @Override
                public void onFailure(Throwable caught) {
                    BIKClientSingletons.showWarning("Nie można skasować temporalnych plików");
                    super.onFailure(caught);
                }
            });
        }
    }

    public static native final boolean isVisInjected() /*-{
     if (!(typeof $wnd.vis === "undefined") && !(null===$wnd.vis)) {
     return true;
     }
     return false;
     }-*/;

    public static Widget createEditChildAttributeButton(final int nodeId, boolean isVisible, ClickHandler onClickHandler) {
        Image imgGray = new Image("images/edit_gray.png" /* I18N: no */);
        imgGray.getElement().setAttribute("width", "12");
        Image imgYellow = new Image("images/edit_yellow.png" /* I18N: no */);
        imgYellow.getElement().setAttribute("width", "12");
        final PushButton editButton = new PushButton(imgGray, imgYellow, onClickHandler);
        editButton.setStyleName("Attr" /* I18N: no */ + "-editBtn Attr-Inline");
//        editButton.getElement().setAttribute("width", "12");
        editButton.setVisible(isVisible);
        editButton.setTitle(I18n.edytuj.get() /* I18N:  */);

        return editButton;

    }

    public static Widget createAddChildButton(final IBikTreePage bikPage, String caption, final int nodeId) {
        final Image img = new Image("images/edit_2add.png" /* I18N:  */);
        img.setStyleName("imgButton");
        img.setTitle(caption);
        img.setVisible(false);
        getService().getBikEntityDetailsData(nodeId, null, true, new StandardAsyncCallback<EntityDetailsDataBean>() {

            @Override
            public void onSuccess(EntityDetailsDataBean data) {
                final NodeActionAddAnyNodeForKind act = new NodeActionAddAnyNodeForKind();
                act.setup(new NodeActionContext(data.node, bikPage, data));
                if (BIKClientSingletons.isEffectiveExpertLoggedIn()
                        || BIKClientSingletons.isBranchAuthorLoggedIn(data.node.branchIds) || BIKClientSingletons.isLoggedUserAuthorOfTree(data.node.treeId)
                        || BIKClientSingletons.isBranchCreatorLoggedIn(data.node.branchIds) || BIKClientSingletons.isLoggedUserCreatorOfTree(data.node.treeId)
                        || BIKClientSingletons.isSysAdminLoggedIn()) {
                    img.setVisible(true);
                    img.addClickHandler(new ClickHandler() {

                        @Override
                        public void onClick(ClickEvent event) {
                            act.execute();
                        }
                    });
                }
            }
        });

        return img;
    }

    public static Widget createAttributeView(String attrType, String atrLinValue, boolean displayAsNumber) {
        return createAttributeView(attrType, atrLinValue, displayAsNumber, false, null, null);
    }

    public static Widget createAttributeView(String attrType, String atrLinValue, boolean displayAsNumber, boolean isFromChildren, Integer childeNodeId, ClickHandler onClickHandler) {
        if (atrLinValue == null) {
            atrLinValue = "";
        }
        Label w = new Label();
        FlowPanel fp = null;
        boolean handleAsHtml = false;

        // tf: a może nie tylko dla niewyliczanych?
        if (BIKClientSingletons.isRenderAttributesAsHTMLMode()/*AttributeType.calculation.name().equals(attrType)*/) {
            String trimmedVal = atrLinValue != null ? BaseUtils.trimAndCompactSpaces(atrLinValue) : "";
            if (BaseUtils.strHasPrefix(trimmedVal, RENDER_AS_HTML_VAL_PREFIX, false) && BaseUtils.strHasSuffix(trimmedVal, RENDER_AS_HTML_VAL_SUFFIX, false)) {
                handleAsHtml = true;
            }
        }

        if (handleAsHtml) {
            String htmlInner = getHTMLInnerText(atrLinValue);

            if (displayAsNumber) {
                htmlInner = BaseUtils.chunkHTMLWithSepSpace(atrLinValue);
            }
            w = new HTML(htmlInner);
        } else if ((AddOrEditAdminAttributeWithTypeDialog.AttributeType.shortText.name().equals(attrType))
                || (AddOrEditAdminAttributeWithTypeDialog.AttributeType.combobox.name().equals(attrType))
                || (AddOrEditAdminAttributeWithTypeDialog.AttributeType.comboBooleanBox.name().equals(attrType))
                //                        || (AttributeType.calculation.name().equals(attrType))
                || (AddOrEditAdminAttributeWithTypeDialog.AttributeType.checkbox.name().equals(attrType))
                || (AddOrEditAdminAttributeWithTypeDialog.AttributeType.data.name().equals(attrType))
                || (AddOrEditAdminAttributeWithTypeDialog.AttributeType.datetime.name().equals(attrType))//|| (AttributeType.ansiiText.name().equals(attrType))
                ) {
            if (displayAsNumber) {
                atrLinValue = BaseUtils.chunkStringWithSepSpace(atrLinValue);
            }
            w = new Label(atrLinValue);
        } else if ((AddOrEditAdminAttributeWithTypeDialog.AttributeType.ansiiText.name().equals(attrType))) {
//            w= new SafeHtm
            w = new HTML(SimpleHtmlSanitizer.htmlEncodeTag(atrLinValue).replace("\n", "<br>"));
//            w = new Label(atrLinValue.replace("\n", "<br>"));
        } else if ((AddOrEditAdminAttributeWithTypeDialog.AttributeType.sql.name().equals(attrType))) {
            w = new HTML(I18n.wykonalnySkrypt.get());
        } else if ((AddOrEditAdminAttributeWithTypeDialog.AttributeType.longText.name().equals(attrType))
                || (AddOrEditAdminAttributeWithTypeDialog.AttributeType.calculation.name().equals(attrType))) //                            || (AttributeType.data.name().equals(attrType))
        {
//                    w = new HTML(BIKClientSingletons.replaceDescrAndSanitize(atrLinValue, widgetsActive ? "" : "$", null, nodeId, getNodeTreeCode()));
            w = new HTML(atrLinValue);
        } else if (AddOrEditAdminAttributeWithTypeDialog.AttributeType.hyperlinkInMulti.name().equals(attrType)
                || AddOrEditAdminAttributeWithTypeDialog.AttributeType.hyperlinkInMono.name().equals(attrType)
                || AddOrEditAdminAttributeWithTypeDialog.AttributeType.selectOneJoinedObject.name().equals(attrType)
                || AddOrEditAdminAttributeWithTypeDialog.AttributeType.permissions.name().equals(attrType)) {
//            String value = "";
//            String sep = "";
            if (atrLinValue.contains("|") || isFromChildren) {
                final List<String> links = BaseUtils.splitBySep(atrLinValue, "|");

                if (isFromChildren) {
                    fp = new FlowPanel();
//                    fp.add(createEditChildAttributeButton(childeNodeId, onClickHandler));
                    boolean isFirst = true;
                    for (String link : links) {
                        if (!BaseUtils.isStrEmptyOrWhiteSpace(link)) {
                            final AttributeHyperlinkBean abh = getAttributeHyperlinkBeanFromValue(link);
                            String caption = (isFirst ? "" : ", ") + abh.caption;
                            isFirst = false;
                            BikCustomButton bb = new BikCustomButton(caption, "attributeChildren", new IContinuation() {

                                @Override
                                public void doIt() {

                                    final Integer nodeId = abh.nodeId;
                                    BIKClientSingletons.getTabSelector().submitNewTabSelection(abh.treeCode, nodeId, true);

                                    BIKClientSingletons.getService().isDeletedNode(nodeId, new StandardAsyncCallback<Integer>() {

                                        @Override
                                        public void onSuccess(Integer result) {
                                            if (result != null && result != 1) {
                                                BIKClientSingletons.getTabSelector().submitNewTabSelection(abh.treeCode, nodeId, true);
                                            } else {
                                                Dialogs.alert(I18n.obiektUsuniety.format(abh.caption), null, null);
                                            }

                                        }
                                    });

                                }
                            });
                            fp.add(bb);
                        }
                    }
                    return fp;
                } else {
                    w = new Label(getHyperlinkAttribute(atrLinValue));
                }

            } else if (!BaseUtils.isStrEmptyOrWhiteSpace(atrLinValue)) {
                final List<String> encodedValues = BaseUtils.splitBySep(atrLinValue, ",");
                final String nodeIdLink = encodedValues.get(0);
                String caption = BaseUtils.substringFromEnd(atrLinValue, atrLinValue.length() - atrLinValue.indexOf(",") - 1);
//                String caption = encodedValues.get(encodedValues.size() - 1);
                String title = null;
                if (caption.indexOf(" » ") >= 0) {
                    title = caption;
                    caption = caption.substring(caption.lastIndexOf(" » ") + 3);
                }
                final String deletedCaption = caption;
                final List<String> link = BaseUtils.splitBySep(nodeIdLink, "_");
                if (link.size() == 2 && onClickHandler == null) {
                    w = new BikCustomButton(caption, "Attr-link", null);
                    onClickHandler = new ClickHandler() {

                        @Override
                        public void onClick(ClickEvent event) {
                            Integer nodeId = Integer.parseInt(link.get(0));
                            BIKClientSingletons.getService().isDeletedNode(nodeId, new StandardAsyncCallback<Integer>() {

                                @Override
                                public void onSuccess(Integer result) {
                                    if (result != null && result != 1) {
                                        BIKClientSingletons.getTabSelector().submitNewTabSelection(link.get(1), Integer.parseInt(link.get(0)), true);
                                    } else {
                                        Dialogs.alert(I18n.obiektUsuniety.format(deletedCaption), null, null);
                                    }

                                }
                            });

                        }
                    };
                } else {
                    w = new Label(caption);
                }

                if (title != null) {
                    w.setTitle(title);
                }
            }

        } else if (AddOrEditAdminAttributeWithTypeDialog.AttributeType.hyperlinkOut.name().equals(attrType)) {
            w = new HTML(BIKCenterUtils.createUrl(atrLinValue));
        }

        if (onClickHandler != null) {
            w.addClickHandler(onClickHandler);
        }
        return w;
    }

    public static String getHyperlinkAttribute(String atrLinValue) {
        String value = "";
        String sep = "";
        if (atrLinValue.contains("|")) {
            final List<String> links = BaseUtils.splitBySep(atrLinValue, "|");
            for (String link : links) {
                if (!BaseUtils.isStrEmptyOrWhiteSpace(link)) {
                    String caption = BaseUtils.substringFromEnd(link, link.length() - link.indexOf(",") - 1);
                    value = value + sep + caption;
                    sep = ", ";
                }
            }

        } else if (!BaseUtils.isStrEmptyOrWhiteSpace(atrLinValue)) {
            value = BaseUtils.substringFromEnd(atrLinValue, atrLinValue.length() - atrLinValue.indexOf(",") - 1);
        }
        return value;
    }

    public static void parseDynamicNodeDesc(final int nodeId, final Widget descr) {
        Set<String> reports = BaseUtils.extractReportTags(descr.getElement().getInnerHTML());
        for (final String reportTag : reports) {
//            final String report = reportTag.substring(0, reportTag.indexOf('('));
//            List<String> params = BaseUtils.splitBySep(reportTag.substring(reportTag.indexOf('(') + 1, reportTag.lastIndexOf(')')), ",");
//            String params = reportTag.substring(reportTag.indexOf('(') + 1, reportTag.lastIndexOf(')'));

            final String reportHtml = BaseUtils.REPORT_TAG_PREFIX + reportTag + BaseUtils.REPORT_TAG_SUFFIX;
            final String hiddenReport = "<div class=\"hidden\">" + reportHtml + "</div>";
            descr.getElement().setInnerHTML(descr.getElement().getInnerHTML().replace(reportHtml, hiddenReport));
            BIKClientSingletons.getService().compileReport(nodeId, reportTag, new StandardAsyncCallback<String>() {

                @Override
                public void onFailure(Throwable caught) {
                    showOnFailureError(I18n.nieMoznaWczytacRaportu.get() + ": " + reportTag, caught);
                }

                @Override
                public void onSuccess(String result) {
                    String txt = descr.getElement().getInnerHTML().replace(hiddenReport, result);
                    descr.getElement().setInnerHTML(txt);
                }
            });
        }
    }

    public static Grid createSelectIconView(Collection<String> icons, String selectedIcon) {
        int COL_NUM = 10;

        Grid grid = new Grid(icons.size() / COL_NUM + 1, COL_NUM * 2);
        grid.setCellSpacing(8);
        int row = 0;
        int col = 0;
        for (final String icon : icons) {
            final RadioButton rb = new RadioButton("icons" /* I18N: no */);
            rb.setTitle(icon);
            Image img = new Image(BIKGWTConstants.ICON_DIR_PREFIX + icon + BIKGWTConstants.ICON_DIR_POSTFIX);
            img.addClickHandler(new ClickHandler() {

                public void onClick(ClickEvent event) {
                    rb.setValue(true);
                }
            });
            grid.setWidget(row, col++, rb);
            grid.setWidget(row, col++, img);

            if (col == COL_NUM * 2) {
                row++;
                col = 0;
            }
            if (icon.equals(selectedIcon)) {
                rb.setValue(true);
            }
        }
        return grid;
    }

    public static String getSelectedIconFromView(Grid grid) {
        for (int i = 0; i < grid.getRowCount(); i++) {
            for (int j = 0; j < grid.getCellCount(i); j++) {
                if (grid.getWidget(i, j) instanceof RadioButton) {
                    RadioButton rb = (RadioButton) grid.getWidget(i, j);
                    if (rb.getValue()) {
                        return rb.getTitle();
                    }
                }
            }
        }
        return null;
    }

    public static boolean canAddToMenuConf(TreeNodeBean node) {
        if (BIKConstants.TREE_KIND_META_BIKS_MENU_CONF.equalsIgnoreCase(node.treeKind)) {
            if (node.objId.equals("#" + SearchPage.SEARCH_PAGE_TAB_ID)
                    || node.objId.equals("#" + BIKGWTConstants.TAB_ID_MYBIKS)) {
                return false;
            }
            List<String> ids = BaseUtils.splitBySep(node.branchIds, "|");
            return ids.size() < 5;
        }
        return true;
    }

    public static boolean isLoggedUserHasPrivilege2PublicAttr(TreeNodeBean node) {
        return isAppAdminLoggedIn() || isEffectiveExpertLoggedIn()
                || isLoggedUserAuthorOfTree(node.treeId)
                || isLoggedUserCreatorOfTree(node.treeId)
                || isLoggedUserIsNonPublicUserInTree(node.treeId)
                || isBranchAuthorLoggedIn(node.branchIds)
                || isBranchCreatorLoggedIn(node.branchIds)
                || isBranchNonPublicUserLoggedIn(node.branchIds);
    }

    public static List<AttributeBean> filtrPublicAttributes(TreeNodeBean node, List<AttributeBean> attributes) {
        if (isLoggedUserHasPrivilege2PublicAttr(node)) {
            return attributes;
        }
        List<AttributeBean> res = new ArrayList<AttributeBean>();
        for (AttributeBean bean : attributes) {
            if (bean.isPublic == 0) {
                //check visible
            } else {
                res.add(bean);
            }
        }
        return res;
    }

    private static class DisplayOnHoverHandlers implements MouseOverHandler, MouseOutHandler {

        private boolean canShow;
        private Widget w;
        private IContinuationWithReturn<Boolean> optCanShowCont;
        //private int opacity; // 100 = fully opaque
        //private Animation ani;
        //private boolean currAniOnShow = false;

        DisplayOnHoverHandlers(Widget w, IContinuationWithReturn<Boolean> optCanShowCont) {
            this.w = w;
            this.optCanShowCont = optCanShowCont;
        }

        public void onMouseOver(MouseOverEvent event) {
//            if (ani != null) {
//                if (currAniOnShow) {
//                    return; // no-op
//                }
//            }

            canShow = optCanShowCont == null || optCanShowCont.doIt();
            if (canShow) {
//                if (ani != null) {
//                  ani.cancel();
//                  ani = null;
//                }
//
//                ani = new Animation() {
//
//                    @Override
//                    protected void onUpdate(double progress) {
//                        opacity = (int)(progress * 100);
//                        w.getElement().getStyle().setOpacity(progress);
//                    }
//                };

                //w.setVisible(true);
                setWidgetVisibleByOpacity(w, true);

            }
        }

        public void onMouseOut(MouseOutEvent event) {
            if (canShow) {
                //w.setVisible(false);
                setWidgetVisibleByOpacity(w, false);
            }
        }
    }

    public static void setWidgetVisibleByOpacity(Widget w, boolean visible) {
        w.getElement().getStyle().setOpacity(visible ? 1 : 0);
    }

    public static void setupDisplayOnHover(final Widget w, Widget hoveringWidget,
            final IContinuationWithReturn<Boolean> optCanShowCont) {
        DisplayOnHoverHandlers handlers = new DisplayOnHoverHandlers(w, optCanShowCont);

        GWTUtils.addOnHoverHandlers(hoveringWidget, handlers, handlers);
        //w.setVisible(false);
        setWidgetVisibleByOpacity(w, false);
    }

    public static void setupDisplayOnHover(Widget w, IContinuationWithReturn<Boolean> optCanShowCont) {
        setupDisplayOnHover(w, w.getParent(), optCanShowCont);
    }

    public static void setupDisplayOnHover(Widget w) {
        setupDisplayOnHover(w, null);
    }

    //ww: zamiast tego jest metoda isEffectiveExpertLoggedIn
//    public static boolean isAdminOrExpertLoggedIn() {
//        return loggedUser != null && (isEffectiveExpertLoggedIn() || isSysAdminLoggedIn());
//    }
    public static <BEAN, IDT> boolean areChildrenCustomSorted(ITreeRowAndBeanStorage<IDT, Map<String, Object>, BEAN> storage, IDT nodeId, IBean4TreeExtractor<BEAN, IDT> extr) {
        Collection<Map<String, Object>> siblingRows = storage.getChildRows(storage.getRowById(nodeId));
        if (siblingRows != null) {
            for (Map<String, Object> row : siblingRows) {
                BEAN tnb = storage.getBeanById(storage.getTreeBroker().getNodeId(row));
                if (extr.extractVisualOrder(tnb) != 0) {
                    return true;
                }
            }
        }
        return false;
    }

    public static int getMaxVisualOrderOfChildren(ITreeRowAndBeanStorage<Integer, Map<String, Object>, TreeNodeBean> storage, Integer nodeId) {
        Collection<Map<String, Object>> siblingRows = storage.getChildRows(storage.getRowById(nodeId));
        int max = -1;

        if (siblingRows != null) {

            for (Map<String, Object> row : siblingRows) {
                TreeNodeBean tnb = storage.getBeanById(storage.getTreeBroker().getNodeId(row));
                int curr = tnb.visualOrder;

                if (curr > max) {
                    max = curr;
                }
            }
        }

        return max;
    }

    public static String replaceDescrAndSanitize(String properDescr, String exToViewHTMLInFullScreenDialog, Map<Integer, Pair<String, String>> innerLinks, int nodeId, String nodeTreeCode) {//String dla FullScrena,
        if (properDescr != null) {
//            properDescr = properDescr.replace(GWT.getHostPageBaseURL(), "");
            String tmp = BIKCenterUtils.replaceContentsFromHtmlTreeCode(
                    !BaseUtils.isMapEmpty(innerLinks) ? properDescr + "<h2 id=\"zobacz\" name=\"naglowek\">" /* i18n: no */ + I18n.zobaczTez.get() /* I18N:  */ + "</h2>" : properDescr,
                    nodeId, nodeTreeCode, exToViewHTMLInFullScreenDialog);
            tmp = fixBadRichtextAttribute(tmp);
//            tmp = com.google.gwt.safehtml.shared.SimpleHtmlSanitizer.sanitizeHtml(tmp).asString();
            tmp = SimpleHtmlSanitizer.sanitize(tmp);
            return tmp;
        }
        return "";
    }

    protected static String fixBadRichtextAttribute(String toFixHTML) {
//        IRegExpPattern htmlUrlPattern =
//                BaseUtils.getRegExpMatcherFactory().compile(ATTRIBUTE_PATTERN, true, true);
        IRegExpMatcher htmlUrlMatcher = HTML_URL_PATTERN.matcher(toFixHTML);
        StringBuilder buffer = new StringBuilder();

        int index = 0;
        while (htmlUrlMatcher.find()) {
            buffer.append(toFixHTML.substring(index, htmlUrlMatcher.start()));
            buffer.append("<");
            buffer.append(htmlUrlMatcher.group(1)).append("=\"").append(htmlUrlMatcher.group(2)).append("\"").append(htmlUrlMatcher.group(3));
            index = htmlUrlMatcher.end();
        }
        if (index < toFixHTML.length()) {
            buffer.append(toFixHTML.substring(index));
        }
        return buffer.toString();
    }

    public static Widget createLinkAsAnchor(String html, final String treeCode, final int nodeId) {
        HTML link = new HTML(html);
        link.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                BIKClientSingletons.getTabSelector().submitNewTabSelection(treeCode, nodeId, true);
            }
        });
        link.setStyleName("linkPointerAndColor");
        return link;
    }

    public static boolean isTreeHidden(final String treeKind) {
        if (treeKind != null && treeKind.equals(BIKGWTConstants.TREE_KIND_DYNAMIC_TREE)) {
            for (TreeBean treeBean : trees) {
                if (treeKindsAsSet.contains(treeBean.treeKind)) {
                    return false;
                }
            }
            return true;
        }
        return !treeNotHiddenKind.contains(treeKind);
    }

    public static boolean isStarredSystemUser(Integer userId) {
        return changedRanking.starredSystemUser.contains(userId);
    }

    public static boolean isStarredBIKUser(Integer userId) {
        return changedRanking.starredBikUser.contains(userId);
    }

    public static boolean isStarredUserNode(Integer userId) {
        return changedRanking.starredUserNode.contains(userId);
    }

    public static long getLastRankingChangeTimeStamp() {
        return changedRanking.timeStamp;
    }

    public static void setChangedRanking(ChangedRankingBean crb) {
//        lastRankingChangeTimeStamp = crb.timeStamp;
        changedRanking = crb;
    }

    public static boolean isFlashPlayerInstalledAndVersionIsValid() {
        PlayerVersion minPlayerVersion = new PlayerVersion(9, 0, 0);
        return SWFObjectUtil.isFlashPlayerInstalled() && SWFObjectUtil.isVersionIsValid(minPlayerVersion);
    }

    public static Widget getMovie(String movie, int width, int height) {

        if (!isFlashPlayerInstalledAndVersionIsValid()) {
            return new HTML(I18n.doObejrzenFilmuPotrzebnJestFlash.get() /* I18N:  */ + "</a>");
        }

        SWFWidget swfWidget = new SWFWidget(//"fsb?get=player_flv_maxi.swf"
                FoxyClientSingletons.getFullFileUploadUrl("player_flv_maxi." + "swf" /* I18N:  */), width + "px", height + "px");
        swfWidget.addFlashVar("flv" /* I18N: no */, //"fsb?get=" + movie + ".flv"
                FoxyClientSingletons.getFullFileUploadUrl(movie + "." + "flv" /* I18N:  */));
        swfWidget.addFlashVar("width" /* I18N: no */, width + "");
        swfWidget.addFlashVar("height" /* I18N: no */, height + "");
        swfWidget.addFlashVar("margin" /* I18N: no */, "1");
        swfWidget.addFlashVar("showvolume" /* I18N: no */, "1");
        swfWidget.addFlashVar("autoplay" /* I18N: no */, "0");
        swfWidget.addFlashVar("showfullscreen" /* I18N: no */, "1");

        swfWidget.addFlashVar("playercolor" /* I18N: no */, "094F8B");
        swfWidget.addParam("wmode" /* I18N: no */, "transparent" /* I18N: no */);
        swfWidget.addParam("autoplay" /* I18N: no */, "false" /* I18N: no */);
        swfWidget.addParam("allowfullscreen" /* I18N: no */, "true" /* I18N: no */);
        swfWidget.addFlashVar("showtitleandstartimage" /* I18N: no */, "0");

        return swfWidget;
    }

    public static int getStarredUserCount() {
        return appPropsBean.starredUserCount;
    }

    public static String getWelcomeMsgForUser() {
        return welcomeMsgForUser;
    }

    public static String getWelcomeMsgForVersion() {
        return welcomeMsgForVersion;
    }

    public static String getBiksDatabaseName() {
        return biksDatabaseName;
    }

    public static ClickHandler createClickHandlerForShowMyBIKSTab(final ReachableMyBIKSTabCode myBIKSTabCode) {
        return new ClickHandler() {
            public void onClick(ClickEvent event) {
                IBikPage<Integer> page = tabSelector.getTabById(BIKGWTConstants.TAB_ID_MYBIKS);
                if (page == null) {
                    return;
                }
                IMyBIKSPage myBIKSPage = (IMyBIKSPage) page;
                myBIKSPage.showTabByCode(myBIKSTabCode);
            }
        };
    }

    public static boolean getSearchPageClickTreeNodeToSearch() {
        return appPropsBean.searchPageClickTreeNodeToSearch;
    }

    public static void setupAdditionalSearchTextBoxEvents(final TextBox searchTb, Widget optSearchBtn) {
        searchTb.addAttachHandler(new AttachEvent.Handler() {
            public void onAttachOrDetach(AttachEvent event) {
                BIKClientSingletons.addOrRemoveAdditionalSearchPagePhraseTb(event.isAttached(), searchTb);
                searchTb.setText(BIKClientSingletons.getGlobalSearchTextBox().getText());
            }
        });
        searchTb.addKeyUpHandler(new KeyUpHandler() {
            public void onKeyUp(KeyUpEvent event) {
                BIKClientSingletons.getGlobalSearchTextBox().setText(searchTb.getText());
            }
        });

        callSearchBtn(searchTb, optSearchBtn);
    }

    public static void setBrowserTitle() {
        Document document = Document.get();
        if (document != null) {
            document.setTitle(BIKGWTConstants.BROWSER_TITLE);
        }
    }

//    public static List<RightRoleBean> getListOfAllRightRoles() {
//        return allRightRoles;
//    }
    public static Map<String, RightRoleBean> getMapOfAllRightRolesByCode() {
        return allRightRolesByCode;
    }

    public static Map<Integer, CustomRightRoleBean> getMapOfAllRightRolesByCodeCrr() {
        return customRightRoleByIdMap;
    }

    public static void refreshAndRedisplayDataUsersTree(boolean isRefresh) {
        if (isRefresh) {
            BIKClientSingletons.invalidateTabData(BIKGWTConstants.TREE_CODE_USER_ROLES);
        }
    }

    //ww: url może być względny - do uploadowanego pliku lub bezwzględny do jakiegoś
    // zasobu w internecie
    public static String fixFileResourceUrl(String url) {
        return fixFileResourceUrlEx(url, null);
    }

    public static String fixFileResourceUrlEx(String url, String clientFileName) {
        return FoxyClientSingletons.getFixedFileResourceUrlEx(url, clientFileName);
    }

    public static String fixAvatarUrl(String avatarUrl, String defaultAvatarUrl) {
        if (avatarUrl != null) {
            return fixFileResourceUrl(avatarUrl);
//            if (avatarUrl.startsWith("file_")) {
//                return FoxyFileUpload.getWebAppUrlPrefixForUploadedFile("fsb?get=") + avatarUrl;
//            }
//            return avatarUrl;
        }
        return defaultAvatarUrl;
    }

    public static Image createIconImgByCode(String iconCode) {
        return createIconImg("images" /* I18N: no */ + "/" + iconCode + "." + "gif" /* I18N:  */, "16px", "16px", null);
    }

    public static Image createIconImg(String icon, String width, String height, String optHint) {
        return createIconImgEx(icon, width, height, optHint, "RelatedObj-ObjIcon");
    }

    public static Image createIconImgEx(String icon, String width, String height, String optHint, String style) {
        Image iconImg = new Image(icon);
        iconImg.setSize(width, height);
        if (style != null) {
            iconImg.setStyleName(style);
        }
        if (optHint != null) {
            iconImg.setTitle(optHint);
        }
        return iconImg;
    }

    public static Image createAvatarImage(String url, final int maxWidth, final int maxHeight, String defaultIcon, String optStyle, String optHint) {
        if (url == null && defaultIcon == null) {
            return null;
        }
        url = fixAvatarUrl(url, defaultIcon);
        final Image img = new Image(url);
        final Image img2 = new Image(url);
        if (maxWidth > 0 && maxHeight > 0) {
            img.setSize(maxWidth + "px", maxHeight + "px");
        }
        img2.addLoadHandler(new LoadHandler() {
            public void onLoad(LoadEvent event) {
                img2.setVisible(true);
                Pair<Integer, Integer> scaledDims = BaseUtils.getScaledDims(img2.getWidth(), img2.getHeight(), -maxWidth, -maxHeight);
                img.setSize(scaledDims.v1 + "px", scaledDims.v2 + "px");
                RootPanel.get().remove(img2);
            }
        });
        RootPanel.get().add(img2);
        img2.setVisible(false);
        if (optStyle != null) {
            img.setStyleName(optStyle);
        }
        if (optHint != null) {
            img.setTitle(optHint);
        }
        return img;
    }

    public static Widget createActionButton(String actionIconUrl, String hint, ClickHandler ch) {
        PushButton btn = new PushButton(new Image("images" /* I18N: no */ + "/" + actionIconUrl + "." + "gif" /* I18N: no */));
        btn.addStyleName("followBtn");
        btn.setTitle(hint);
        btn.addClickHandler(ch);
        return btn;
    }

    public static boolean isTreeKindDynamic(String treeKind) {
        if (treeKind.equals(BIKGWTConstants.TREE_KIND_DYNAMIC_TREE)) {
            return true;
        }
        for (TreeKindBean treeKindBean : treeKinds) {
            if (treeKind.equals(treeKindBean.code)) {
                return true;
            }
        }
        return false;
    }

    public static boolean allowTreeKindLinking(String treeKind) {
        for (TreeKindBean treeKindBean : treeKinds) {
            if (treeKind.equals(treeKindBean.code) && treeKindBean.allowLinking == 1) {
                return true;
            }
        }
        return false;
    }

    public static String getDynamicTreeKindForTreeKind(String treeKind) {
        if (isTreeKindDynamic(treeKind)) {
            return BIKGWTConstants.TREE_KIND_DYNAMIC_TREE;
        }
        return treeKind;
    }

    public static Map<String, String> getTreeKindCaptionsMap() {
        return treeKindCaptions;
    }

    public static TreeKindBean getTreeKindBeanByCode(String code) {
        return treeKindsByCode.get(code);
    }

    public static void updateTreeKindMaps(TreeKindBean bean) {
        treeKindCaptions.put(bean.code, bean.caption);
        treeKindsByCode.put(bean.code, bean);
        treeKindsAsSet.add(bean.code);
        if (treeKinds.contains(bean)) { // sprawdza czy code już istnieje
            treeKinds.remove(bean);
        }
        treeKinds.add(bean);
    }

    public static String getLeafNameForTreeKind(String treeKind) {
        TreeKindBean bean = getTreeKindBeanByCode(treeKind);
        return getNodeKindById(bean.leafNodeKindId).caption;
    }

    public static boolean isTreeBOReport(String treeCode) {
        return reportTreeCodes.contains(treeCode);
    }

    public static boolean isUserCreatorOfThisDb() {
        return isUserCreatorOfThisDb;
    }

    public static void showAppAndDBVersions(String appVersion, String dbVersion) {
        if (appVersionInfoLbl == null) {
            Element appVersionInfoEle = GWTUtils.getElementById("appVersionInfo");
            appVersionInfoLbl = InlineLabel.wrap(appVersionInfoEle);
        }

        appVersionInfoLbl.setText((appVersion != null ? ("appVer: " + "VT " /* I18N: no */ + appVersion) : "") + (dbVersion != null ? (", dbVer: " + "VT " /* I18N: no */ + dbVersion) : ""));
    }

    public static void reloadWithSetDb(String dbName) {

        // jeżeli baza się nie zgadza - trzeba obciąć to, co w hashu
        if (!isMultiXMode) {
            dbName = null;
        } else {
            dbName = BaseUtils.emptyStringOrWhiteSpaceToNull(dbName);
        }

        String currentDbNameFromUrl = BaseUtils.emptyStringOrWhiteSpaceToNull(getCurrentDbNameFromUrl());

        boolean doTrimHash = !BaseUtils.safeEquals(dbName, currentDbNameFromUrl);

        String currentUrl = Window.Location.createUrlBuilder().buildString();
        String newUrl = makeUrlToDatabase(dbName, doTrimHash);

        if (BaseUtils.safeEquals(currentUrl, newUrl)) {
            Window.Location.reload();
        } else {
            Window.Location.replace(newUrl);
        }

//        if (dbName != null) {
////            String newUrl = UrlParser.addParamToUrl(Window.Location.getHref(), MultixConstants.DB_NAME_PARAM, dbName);
////            String oldUrl = Window.Location.getHref();
//
////            Window.alert("Old url: " + oldUrl + ", new url: " + newUrl + ", old.host=" + Window.Location.getHost()
////                    + ", old.path=" + Window.Location.getPath() + ", old.port=" + Window.Location.getPort()
////                    + ", old.queryStr=" + Window.Location.getQueryString() + ", old.hash=" + Window.Location.getHash()
////                    + ", new.dbName=" + dbName + ", old.dbName=" + currentDbNameFromUrl);
//            if (/*newUrl.equals(oldUrl)*/BaseUtils.safeEquals(dbName, currentDbNameFromUrl)) {
//                Window.Location.reload();
//            } else {
//                String newUrl = makeUrlToDatabase(dbName, doTrimHash);
//                Window.Location.replace(newUrl);
//            }
//        } else {
//            Window.Location.reload();
//        }
    }

    public static String getCurrentDbNameFromUrl() {
        return Window.Location.getParameter(MultixConstants.DB_NAME_PARAM);
    }

    public static void setupRpcRequestBuilderParams() {
        setupRpcRequestBuilderOneParam(MultixConstants.DB_NAME_PARAM);
        setupRpcRequestBuilderOneParam(LocaleInfo.getLocaleQueryParam(),
                FoxyCommonConsts.REQUEST_PARAM_NAME_FOR_LOCALE_NAME);
//        FoxyRpcRequestBuilder.setReqExtraParam(BIKConstants.FULL_HREF_REQ_CTX_PARAM_NAME,
//                Window.Location.getHref());
    }

    public static void checkLisaUnassignedCategory() {

//        getLisaService().checkLisaUnassignedCategory(new AsyncCallback<Boolean>() {
//            public void onFailure(Throwable caught) {
//                //nic nie robimy
//            }
//
//            public void onSuccess(Boolean result) {
//                if (result) {
//                    showInfo(I18n.noweElementyDoSkategoryzowania.get());
//                }
//            }
//        });
    }

    public static String getTranslatedMssqlExProp(String nodeKind, String prop) {
        Map<String, String> map = mssqlExProps.get(nodeKind);
        if (map == null || !map.containsKey(prop)) {
            return prop;
        }
        return map.get(prop);
    }

    public static String getTreeOptIcon(String treeCode, String nodeKindCode) {
        return treeIcons.get(new Pair<String, String>(treeCode, nodeKindCode));
    }

    public static Map<Pair<String, String>, String> getTreeIcons() {
        return treeIcons;
    }

    public static boolean isDocumentTreeVisible() {
        for (TreeBean treeBean : trees) {
            if (treeBean.code.equals(BIKConstants.TREE_CODE_DOCUMENTS)) {
                return true;
            }
        }
        return false;
    }

    public static List<String> getAdDomains() {
        return adDomains;
    }

    public static List<AttrSearchableBean> getAttrsSearchable() {
//        return attrsSearchable;
        throw new UnsupportedOperationException("Nie uzywamy ze wzgledu Lucene");
    }

    public static boolean isDisableGuestMode() {
        return disableGuestMode;
    }

    public static boolean isMyObjectsTabVisible() {
//        AppPropBean roles = appPropMap.get(BIKConstants.APP_PROP_MY_OBJECTS_ROLES);
        String myObjectsInRole = appPropsBean.myObjectsInRole;
//        AppPropBean trees = appPropMap.get(BIKConstants.APP_PROP_MY_OBJECTS_TREE_CODE);
//        AppPropBean nodeKinds = appPropMap.get(BIKConstants.APP_PROP_MY_OBJECTS_NODE_KINDS);
        return !BaseUtils.isStrEmptyOrWhiteSpace(myObjectsInRole); //&& (trees != null && !BaseUtils.isStrEmptyOrWhiteSpace(trees.val) || nodeKinds != null && !BaseUtils.isStrEmptyOrWhiteSpace(nodeKinds.val));
    }

    public static List<String> getMyObjectsAttributeNames() {
        String myObjectsAttributes = appPropsBean.myObjectsAttributes;
        return !BaseUtils.isStrEmptyOrWhiteSpace(myObjectsAttributes) ? BaseUtils.splitBySep(myObjectsAttributes, "|", true) : new ArrayList<String>();
    }

    public static boolean isCustomTreeModeEnable(String treeCode) {
        TreeKindBean tkb = treeKindsByCode.get(treeCode);
        return tkb != null && !BaseUtils.isCollectionEmpty(tkb.additionalNodeKindsId) || appPropsBean.enableCustomTreeMode;
    }

    public static List<NodeKindBean> getNodeKindsForTreeKind(TreeNodeBean node) {
        List<NodeKindBean> list = new ArrayList<NodeKindBean>();
        if (!isTreeKindDynamic(node.treeKind) || !canAddToMenuConf(node)) {
            return list;
        }
        TreeKindBean tkb = treeKindsByCode.get(node.treeKind);
        Set<Integer> allNodeKinds = new HashSet<Integer>();
        Set<Integer> branchesNodeKind = new HashSet<Integer>();
//        allNodeKinds.add(tkb.branchNodeKindId);
//        list.add(getNodeKindById(tkb.branchNodeKindId));
//        list.get(list.size() - 1).isBranch = true;
//        list.get(list.size() - 1).isLeaf = false;
        if (tkb.additionalNodeKindsId != null) {
            for (NodeKind4TreeKind addNKB : tkb.additionalNodeKindsId) {
                if (addNKB.dstNodeKindId != null && addNKB.srcNodeKindId != null
                        && !allNodeKinds.contains(addNKB.dstNodeKindId) && addNKB.srcNodeKindId == node.nodeKindId
                        && BIKConstants.CHILD.equalsIgnoreCase(addNKB.relationType)) {
                    allNodeKinds.add(addNKB.dstNodeKindId);
                    branchesNodeKind.add(addNKB.srcNodeKindId);
                    list.add(getNodeKindById(addNKB.dstNodeKindId));
//                    list.get(list.size() - 1).isBranch = addNKB.isBranch;
//                    list.get(list.size() - 1).isLeaf = addNKB.isLeaf;
                }
            }
        }
        for (NodeKindBean nkb : list) {
            if (branchesNodeKind.contains(nkb.id)) {
                nkb.isBranch = true;
            }
            nkb.isLeaf = !nkb.isBranch;
        }
//        if (!allNodeKinds.contains(tkb.leafNodeKindId)) {
//            allNodeKinds.add(tkb.leafNodeKindId);
//            list.add(getNodeKindById(tkb.leafNodeKindId));
//            list.get(list.size() - 1).isBranch = false;
//            list.get(list.size() - 1).isLeaf = true;
//        }
        return list;
    }

    public static List<NodeKindBean> getNodeKindsForTreeKindHasLink(String treeKindCode, int srcNodeKindId) {
        List<NodeKindBean> list = new ArrayList<NodeKindBean>();
        if (!isTreeKindDynamic(treeKindCode)) {
            return list;
        }
        TreeKindBean tkb = treeKindsByCode.get(treeKindCode);
        Set<Integer> allNodeKinds = new HashSet<Integer>();
        Set<Integer> branchesNodeKind = new HashSet<Integer>();
        if (tkb.additionalNodeKindsId != null) {
            for (NodeKind4TreeKind addNKB : tkb.additionalNodeKindsId) {
                if (addNKB.dstNodeKindId != null && addNKB.srcNodeKindId != null
                        && !allNodeKinds.contains(addNKB.dstNodeKindId) && addNKB.srcNodeKindId == srcNodeKindId
                        && BIKConstants.LINKING.equalsIgnoreCase(addNKB.relationType)) {
                    allNodeKinds.add(addNKB.dstNodeKindId);
                    branchesNodeKind.add(addNKB.srcNodeKindId);
                    list.add(getNodeKindById(addNKB.dstNodeKindId));

                }
            }
        }
        for (NodeKindBean nkb : list) {
            if (branchesNodeKind.contains(nkb.id)) {
                nkb.isBranch = true;
            }
            nkb.isLeaf = !nkb.isBranch;
        }
        return list;
    }

    public static String getMenuPathForTreeId(int treeId) {
        return treeCodeToMenuPath.get(getTreeCodeById(treeId));
    }

    public static String getMenuPathForTreeCode(String treeCode) {
        return treeCodeToMenuPath.get(treeCode);
    }

    public static boolean isShowFullPathInJoinedObjsPane() {
        return appPropsBean.showFullPathInJoinedObjsPane;
    }

    public static boolean isShowAnticipatedObjLink() {
        return appPropsBean.showAnticipatedObjLink;
    }

    public static boolean isShowAddNodeButtonOnHyperlinkDialog() {
        return appPropsBean.showAddNodeButtonOnHyperlinkDialog;
    }

    public static boolean hideSocialUserInfoOnAdminUsersTab() {
        return appPropsBean.hideSocialUserInfoOnAdminUsersTab;
    }

    protected static Set<String> nodeActionCodesToRegister = new LinkedHashSet<String>();
    protected static Set<String> nodeActionCodesAlreadyRegistered = new LinkedHashSet<String>();
    protected static double lastNodeActionCodesRegisterMillis = -1;

    public static void registerOptNodeActionCodes(Collection<String> codes) {
        if (codes == null || !isRegisterNewNodeActionsEnabled) {
            return;
        }
        //showInfo("registerOptNodeActionCodes: codes=" + codes + ", nodeActionCodesToRegister=" + nodeActionCodesToRegister + ", nodeActionCodesAlreadyRegistered=" + nodeActionCodesAlreadyRegistered);

        Set<String> candidates = new LinkedHashSet<String>();
        candidates.addAll(codes);
        candidates.removeAll(nodeActionCodeToIdMap.keySet());
        candidates.removeAll(nodeActionCodesAlreadyRegistered);
        candidates.removeAll(nodeActionCodesToRegister);
        if (candidates.isEmpty()) {
            return;
        }

        nodeActionCodesToRegister.addAll(candidates);

        if (lastNodeActionCodesRegisterMillis < 0) {
            Timer t = new Timer() {

                @Override
                public void run() {
                    double ctm = Duration.currentTimeMillis();

                    if (ctm - lastNodeActionCodesRegisterMillis > 900) {
                        //showInfo("registerOptNodeActionCodes: timer will register, nodeActionCodesToRegister=" + nodeActionCodesToRegister);
                        nodeActionCodesAlreadyRegistered.addAll(nodeActionCodesToRegister);
                        getService().registerNodeActionCodes(nodeActionCodesToRegister, doNothingCallback);
                        nodeActionCodesToRegister.clear();
                        lastNodeActionCodesRegisterMillis = -1;
                    } else {
                        //showInfo("registerOptNodeActionCodes: timer reschedule");
                        schedule(1000);
                    }
                }
            };

            t.schedule(1000);
        }

        lastNodeActionCodesRegisterMillis = Duration.currentTimeMillis();
    }

    public static List<CustomRightRoleBean> getCustomRightRoles() {
        return customRightRoles;
    }

    public static List<SystemUserGroupBean> getAllRoles() {
        return allRoles;
    }

    protected static Map<String, Set<String>> treeKindToTreeCodesMap = null;

    public static Set<String> getTreeCodesByTreeKind(String treeKind) {
        if (treeKindToTreeCodesMap == null) {
            initTreeKindToTreeCodesMap();
        }

        return treeKindToTreeCodesMap.get(treeKind);
    }

    protected static void initTreeKindToTreeCodesMap() {
        treeKindToTreeCodesMap = new HashMap<String, Set<String>>();
        for (TreeBean tb : trees) {
            String tk = tb.treeKind;
            Set<String> codes = treeKindToTreeCodesMap.get(tk);
            if (codes == null) {
                codes = new HashSet<String>();
                treeKindToTreeCodesMap.put(tk, codes);
            }
            codes.add(tb.code);
        }
    }

    public static Set<String> treeSelectorToTreeCodes(String treeSelector) {
        Set<String> items = BaseUtils.splitUniqueBySep(treeSelector, ",", true);
        Set<String> res = new LinkedHashSet<String>();
        if (items != null) {
            for (String item : items) {
                if (BaseUtils.strHasPrefix(item, "@", false)) {
                    Set<String> codes = getTreeCodesByTreeKind(item.substring(1));
                    if (codes != null) {
                        res.addAll(codes);
                    }
                } else {
                    res.add(item);
                }
            }
        }
        return res;
    }

    protected static Set<String> customRightRolesTreeCodes = null;

    public static Set<String> getCustomRightRolesTreeCodes() {

        if (customRightRolesTreeCodes == null) {

            if (isAppAdminLoggedIn()) {
                customRightRolesTreeCodes = new HashSet<String>();
            } else {
                String customRightRolesTreeSelector = getAppPropVal("customRightRolesTreeSelector");
                customRightRolesTreeCodes = treeSelectorToTreeCodes(customRightRolesTreeSelector);
                if (isEffectiveExpertLoggedIn()) {
                    Set<String> forRegUser = treeSelectorToTreeCodes(treeSelectorForRegularUserCrr);
                    customRightRolesTreeCodes.removeAll(forRegUser);
                }
            }
        }

        return customRightRolesTreeCodes;
    }

    public static boolean isCustomRightRolesTreeByCode(String treeCode) {
        return getCustomRightRolesTreeCodes().contains(treeCode);
    }

    public static Integer getNodeActionIdByCode(String code) {
        return nodeActionCodeToIdMap.get(code);
    }

    public static boolean isNodeActionGrantedInTreeRoot(int treeId, int actionId) {
        Set<Integer> actionIds = customRightRolesActionsInTreeRoots.get(treeId);
        return BaseUtils.collectionContainsFix(actionIds, actionId);
    }

    public static int getTreeOfTreesId() {
        TreeBean tb = allTreesByCodeMap.get(BIKConstants.TREE_CODE_TREEOFTREES);
        return tb.id;
    }

    //rola autor aktualnosci z # treeoftrees w selectora selectormode=0
    public static boolean isNodeActionEnabledByCustomRightRoles(String actionCode, ICheckEnabledByDesign cebd) {
        return isNodeActionEnabledByCustomRightRoles(actionCode, getTreeOfTreesId(), null, cebd);
    }

    public static boolean isNodeActionEnabledByCustomRightRoles(String actionCode, EntityDetailsDataBean reqNodeData,
            ICheckEnabledByDesign cebd) {
        return isNodeActionEnabledByCustomRightRoles(actionCode, reqNodeData.node.treeId, reqNodeData, cebd);
    }

    public static boolean isNodeActionEnabledByCustomRightRoles(String actionCode, int treeId, EntityDetailsDataBean optNodeData,
            ICheckEnabledByDesign cebd) {
        return isNodeActionEnabledByCustomRightRoles(actionCode, treeId, false, optNodeData, cebd);
    }

    //isAuthorAndCreatorGetAllTree- paramert czy mamy brac wszystkie drzewa do ktłórych ma dostęp(np tylko do jednoego noda ma) czy tylko te co ma dostęp do całosci
    public static boolean isNodeActionEnabledByCustomRightRoles(String actionCode, int treeId, boolean isAuthorAndCreatorGetAllTree, EntityDetailsDataBean optNodeData,
            ICheckEnabledByDesign cebd) {

        if (actionCode == null) {
            return cebd.isEnabledByDesign();
        }

        registerOptNodeActionCodes(BaseUtils.paramsAsSet(actionCode));

//        if (actionCode.equals("NewNestableParent")) {
//            Window.alert("isNodeActionEnabledByCustomRightRoles: NewNestableParent");
//        }
        boolean treeUserRights = false;
        if (!isAuthorAndCreatorGetAllTree) {
            treeUserRights = isLoggedUserAuthorOfTree(treeId) || isLoggedUserCreatorOfTree(treeId);
        } else {
            treeUserRights = isLoggedUserAuthorOfAllTree(treeId) || isLoggedUserCreatorOfAllTree(treeId);
        }
        boolean branchUserRights = optNodeData != null && (!BaseUtils.isStrEmptyOrWhiteSpace(optNodeData.node.branchIds) && (isBranchAuthorLoggedIn(optNodeData.node.branchIds)
                || isBranchCreatorLoggedIn(optNodeData.node.branchIds)));
        String treeCode = getTreeCodeById(treeId);

        if ((!isCustomRightRolesTreeByCode(treeCode)
                || (loggedUser != null && treeUserRights))
                || ((BIKConstants.ACTION_ADD_CUSTOM_TREE_NODE.equals(actionCode) || BIKConstants.ACTION_ADD_ANY_NODE.equals(actionCode)
                || BIKConstants.ACTION_ADD_EVENT.equals(actionCode))
                && branchUserRights
                && !(loggedUser != null && treeUserRights))) {
            return cebd.isEnabledByDesign();
        }

//        if (!isAppAdminLoggedIn()) {
        //sprawdzamy czy użytkownik ma dostępny action id w bik_node_action
        Integer nodeRightsActionId = BIKClientSingletons.getNodeActionIdByCode(actionCode);
        if (nodeRightsActionId == null) {
            return false;
        }

        if (optNodeData != null) {
            if (!optNodeData.grantedNodeRightActionIds.contains(nodeRightsActionId)) {
                return false;
            }
        } else {
            if (!BIKClientSingletons.isNodeActionGrantedInTreeRoot(treeId, nodeRightsActionId)) {
                return false;
            }
        }
        Set<String> oldRoles = loggedUser == null ? null : loggedUser.userRightRoleCodes;
        try {
            if (loggedUser != null) {
                loggedUser.userRightRoleCodes = BIKRightRoles.getSysAdminRightRoles();
            }
            return cebd.isEnabledByDesign();
        } finally {
            if (loggedUser != null) {
                loggedUser.userRightRoleCodes = oldRoles;
            }
        }
    }

    public static CustomRightRoleBean getCustomRightRoleBeanById(int crrId) {
        return customRightRoleByIdMap.get(crrId);
    }

    public static String getSsoUserName() {
        return ssoUserName;
    }

    public static boolean showGrantsInTestConnection() {
        return showGrantsInTestConnection;
    }

    public static boolean isRegularUserTree(TreeBean tb) {
        return tb != null && !BaseUtils.safeEqualsAny(tb.code, "DQM", "DQMDokumentacja");
    }

    public static boolean showingWaitingForLoginAgainDialog = false;

    public static Integer getTreeIdOfCurrentTab() {
        Integer res = null;

        IBikPage<Integer> currTab = BIKClientSingletons.getTabSelector().getCurrentTab();

        if (currTab != null) {
            String tabId = currTab.getId();
            TreeBean tb = BIKClientSingletons.getTreeBeanByCode(tabId);
            if (tb != null) {
                res = tb.id;
            }
        }

        return res;
    }

    public static boolean areChildrenCustomSorted(final ITreeRowAndBeanStorage<Integer, Map<String, Object>, TreeNodeBean> storage,
            Integer parentNodeId) {

        return BIKClientSingletons.areChildrenCustomSorted(storage, parentNodeId, new TreeNodeBean4TreeExtractor());
    }

    public static void sortChildrenWithDefaultOrder(final ITreeRowAndBeanStorage<Integer, Map<String, Object>, TreeNodeBean> storage,
            ITreeGridForAction<Integer> treeGrid,
            Integer parentNodeId) {

        treeGrid.sortChildNodes(parentNodeId, new Comparator<Map<String, Object>>() {
            @Override
            public int compare(Map<String, Object> o1, Map<String, Object> o2) {

                TreeNodeBean n1 = storage.getBeanById(storage.getTreeBroker().getNodeId(o1));
                TreeNodeBean n2 = storage.getBeanById(storage.getTreeBroker().getNodeId(o2));

                //ww: foldery przed niefolderami
                int folderCmpRes = -1 * Boolean.valueOf(n1.isFolder).compareTo(n2.isFolder);

                if (folderCmpRes != 0) {
                    return folderCmpRes;
                }

                return String.CASE_INSENSITIVE_ORDER.compare(n1.name, n2.name);
//                int res = n1.visualOrder - n2.visualOrder;
//
//                return res != 0 ? res : String.CASE_INSENSITIVE_ORDER.compare(n1.name, n2.name);
            }
        });
    }

    public static void maybeSortChildrenWithDefaultOrder(ITreeRowAndBeanStorage<Integer, Map<String, Object>, TreeNodeBean> storage,
            ITreeGridForAction<Integer> treeGrid, Integer parentNodeId) {
        if (!areChildrenCustomSorted(storage, parentNodeId)) {
            sortChildrenWithDefaultOrder(storage, treeGrid, parentNodeId);
        }
    }

    public static int getTreeTabAutoRefreshLevel() {
        return treeTabAutoRefreshLevel;
    }

    protected static Set<Integer> invalidatedTreeIds = new HashSet<Integer>();

    public static void invalidateTabData(String tabId) {

        TreeBean tb = allTreesByCodeMap.get(tabId);

        Integer treeId = tb == null ? null : tb.id;

        if (treeId != null) {
            invalidatedTreeIds.add(treeId);
        }

        BIKClientSingletons.getTabSelector()./**/invalidateTabData(tabId);
    }

    protected static void maybeSendInvalidatedTreeIdsToServer(Long serverTimeStamp) {
        if (!invalidatedTreeIds.isEmpty()) {
            Set<Integer> copy = new HashSet<Integer>(invalidatedTreeIds);
            invalidatedTreeIds.clear();
            getService().registerChangeInTrees(serverTimeStamp, copy, doNothingCallback);
        }
    }
    public static ConfirmDialog badLoginDialog = null;

    public static boolean useSimpleMsgForUnloggedUser() {
        return useSimpleMsgForUnloggedUser;
    }

    public static boolean userDisabledWillReload = false;

    // reakcja na możliwego złego usera
    protected static boolean maybeReactToBadUser(boolean ignoreDifferentUserOnce) {

        String serviceExtraResponseData = FoxyRequestCallback.serviceExtraResponseData;

        LoggedUserOnClientVsServerStatus luocvss;

        String ordinalStr = BaseUtils.dropPrefixOrGiveNull(serviceExtraResponseData, ":");

        if (ordinalStr == null) {
            luocvss = LoggedUserOnClientVsServerStatus.SameUser;
        } else {

            Integer ordinal = BaseUtils.tryParseInteger(ordinalStr);

            // nie wiadamo co to za status, więc zwracamy true, gdy
            // już pokazujemy stosowne dialogi
            if (ordinal == null) {
                return badLoginDialog != null || showingWaitingForLoginAgainDialog;
            }

            luocvss = LoggedUserOnClientVsServerStatus.values()[ordinal];
        }

        if (luocvss == LoggedUserOnClientVsServerStatus.DisabledUser) {
            if (userDisabledWillReload) {
                return true;
            }

            userDisabledWillReload = true;

            Window.alert(I18n.uzytkownikWylaczonyNastapiWylogowanie.get());
            performUserLogout(/*performOnLoginLogoutCont*/);

            return true;
        }

        if (luocvss == LoggedUserOnClientVsServerStatus.SameUser) {

            // poprawiło się - czyścimy dialogi, jeżeli jakieś pokazywaliśmy
            if (badLoginDialog != null) {
                badLoginDialog.hideDialog();
                badLoginDialog = null;
                invalidateAllTabs();
            }
//            if (showingWaitingForLoginAgainDialog) {
//                WaitingForLoginAgainDialog.loginChangedToGood();
//            }
            return false;
        }

        if (showingWaitingForLoginAgainDialog) {
            return true;
        }

        // jeżeli doszliśmy aż tutaj, to luocvss == UnloggedUser albo DifferentUser
        // i trzeba pokazać / zaktualizować badLoginDialog
        // ale jest jeden przypadek, w którym tego nie robimy
        if (ignoreDifferentUserOnce && luocvss == LoggedUserOnClientVsServerStatus.DifferentUser) {
            return false;
        }

        final boolean userUnlogged = luocvss == LoggedUserOnClientVsServerStatus.UnloggedUser;
        String heading = userUnlogged
                ? I18n.jestesWylogowany.get() : I18n.jestesZalogowanyJakoInny.get();
//        final boolean useSimpleMsgForUnloggedUser = useSimpleMsgForUnloggedUser();
        final String dialogBody = "<b>" + heading + ".</b><br/><br/>" + (useSimpleMsgForUnloggedUser ? I18n.abyKontynuowacZalogujSieSimple.get() : I18n.abyKontynuowacZalogujSie.get());

        if (badLoginDialog != null) {

            badLoginDialog.updateBodyTextAndOrTitle(dialogBody, heading);
        } else {

            badLoginDialog = Dialogs.confirmHtml(dialogBody, heading, useSimpleMsgForUnloggedUser ? null : I18n.przelogujSieNaAktZakl.get(),
                    useSimpleMsgForUnloggedUser ? "OK" : I18n.zalogujSieNaInnejZakl.get(), new IParametrizedContinuation<Boolean>() {

                @Override
                public void doIt(Boolean param) {

                    badLoginDialog = null;

                    if (!useSimpleMsgForUnloggedUser) {

                        boolean dialogsAreOpened = BaseActionOrCancelDialog.isShowingDialog();

                        if (dialogsAreOpened && param) {
                            param = Window.confirm(I18n.czyNaPewnoMozeszUtracicPrace.get());
                        }
                    }

                    if (useSimpleMsgForUnloggedUser || param) {
                        BIKClientSingletons.performUserLogout(/*BIKClientSingletons.performOnLoginLogoutCont*/);
                    } else {
                        UrlBuilder newUrl = Window.Location.createUrlBuilder();
                        newUrl.setHash("");
                        Window.open(newUrl.buildString(), "_blank", "");
                        new WaitingForLoginAgainDialog().buildAndShowDialog(userUnlogged);
                    }
                }
            });
        }

        return true;
    }

    //    public static Long serverTimeStampOfInvalidateTreesFromOtherInstances;
    protected static void maybeInvalidateTreesFromOtherInstances() {
        final String serviceExtraResponseData = FoxyRequestCallback.serviceExtraResponseData;

//        if (maybeReactToBadUser(serviceExtraResponseData)) {
//            return;
//        }
        if (BaseUtils.isStrEmptyOrWhiteSpace(serviceExtraResponseData)
                || BaseUtils.strHasPrefix(serviceExtraResponseData, ":", false)) {
            return;
        }

        String treeIdsStr = serviceExtraResponseData;

        List<String> treeIdsStrs = BaseUtils.splitBySep(treeIdsStr, ",", true);

        for (String treeIdStr : treeIdsStrs) {
            Integer treeId = BaseUtils.tryParseInteger(treeIdStr);
            if (treeId == null) {
                continue;
            }

            String treeCode = getTreeCodeById(treeId);
            if (treeCode != null) {
                // tu specjalnie tak - bo bez invalidowania dla innych instancji
                BIKClientSingletons.getTabSelector().invalidateTabData(treeCode);
            }
        }
    }

    public static void setupRequestCallbackForInitedGui() {

        FoxyRequestCallback.setResponseProcessingHook(new IResponseProcessingHook() {

            @Override
            public void afterResponseProcessing(boolean isError, Long foxyRequestTimingAfterCommitTimeStamp) {
                try {
                    maybeSendInvalidatedTreeIdsToServer(foxyRequestTimingAfterCommitTimeStamp);

                    maybeInvalidateTreesFromOtherInstances();
                } finally {
                    //ignoreDifferentUserOnce = false;
                }
            }

            @Override
            public boolean isProcessingResponseRedundant(Object optCallContextForHook) {
                maybeReactToBadUser(optCallContextForHook != null && (Boolean) optCallContextForHook);
                return false;
            }
        });

//        FoxyRequestCallback.setResponseProcessingHook(new IParametrizedContinuation<Pair<Boolean, Long>>() {
//
//            @Override
//            public void doIt(Pair<Boolean, Long> param) {
//
//                try {
//                    maybeSendInvalidatedTreeIdsToServer(param.v2);
//
//                    maybeInvalidateTreesFromOtherInstances();
//                } finally {
//                    ignoreDifferentUserOnce = false;
//                }
//            }
//        });
    }

    public static void invalidateAllTabs() {
        Set<String> treeCodes = treeByCodeMap.keySet();
        for (String treeCode : treeCodes) {
            // tu specjalnie tak - bo bez invalidowania dla innych instancji
            BIKClientSingletons.getTabSelector().invalidateTabData(treeCode);
        }
    }

    public static boolean isDescriptionInJoinedPanelVisible() {
        return (BIKClientSingletons.enableAttributeJoinedObjs() || BIKClientSingletons.isAlternativeJoinedPanelViewEnabled())
                && getViewType() != ViewType.ClassicView;
//        return BIKClientSingletons.isDescriptionInJoinedPanelVisible;
    }

    public static boolean disableBuiltInRoles() {
        return appPropsBean.disableBuiltInRoles;
    }

    public static ConnectionParametersJdbcBean getJdbcSourceTypeBean(String name) {
        return jdbcSourceTypes.get(name);
    }

    public static Set<String> getJdbcSourceTypes() {
        return jdbcSourceTypes.keySet();
    }

    public static PushButton createEditButtonForAttribute(final IContinuation optOnClickCont) {
        PushButton editButton = new PushButton(new Image("images/edit_gray.png" /* I18N: no */), new Image("images/edit_yellow.png" /* I18N: no */));
        if (optOnClickCont != null) {
            editButton.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    optOnClickCont.doIt();
                }
            });
        }
        editButton.setStyleName("Attr" /* I18N: no */ + "-editBtn");
        //ww->authors: przerabiam na eksperta
        editButton.setVisible(optOnClickCont != null);
        editButton.setTitle(I18n.edytuj.get() /* I18N:  */);
        return editButton;
    }

    public static boolean isMultiDomainMode() {
        return isMultiDomainMode;
    }

    public static boolean isSendEmailNotifications() {
        return appPropsBean.sendEmailNotifications;
    }

    public static boolean isEnableFullImportExportTree() {
        return appPropsBean.enableFullImportExportTree;
    }

    public static boolean isUseDrools() {
        return appPropsBean.useDrools;
    }

    public static boolean isShowTutorials() {
        return appPropsBean.showTutorials;
    }

    public static boolean isShowTitleInEntityHeader() {
        return appPropsBean.showTitleInEntityHeader;
    }

    public static boolean isShowAttributeButtonsOnTheLeft() {
        return appPropsBean.showAttributeButtonsOnTheLeft;
    }

    //boolean hideTabs defines if the tabs on the bottom bar have to be hidden
    public static boolean isHideElementsForMM() {
        return appPropsBean.hideTabsFromBarForMM;
    }

    public static boolean showSuggestedElements() {
        return appPropsBean.showSuggestedElements;
    }

    public static boolean allowLinkingInDocumentation() {
        return appPropsBean.allowLinkingInDocumentation;
    }

    public static boolean useDqmConnectionMode() {
        return appPropsBean.useDqmConnectionMode;
    }

    public static boolean enableAttributeJoinedObjs() {
        return appPropsBean.enableAttributeJoinedObjs;
    }

    public static ViewType getViewType() {
        return BIKClientSingletons.viewType;
    }

    public static void setViewType(ViewType viewType) {
        BIKClientSingletons.viewType = viewType;
    }

    public static boolean isDifferentViewType() {
        return getViewType() == ViewType.DifferentView;
    }

    public static boolean disableImportExport() {
        return appPropsBean.disableImportExport;
    }

    public static String[] allowedFileExtensionsList() {
        return appPropsBean.allowedFileExtensions.toLowerCase().split(";");
    }

    public static boolean hideSearchTabByAppProps() {
        return appPropsBean.hideSearchTab;
    }

    public static boolean hideBlogsTabByAppProps() {
        return appPropsBean.hideBlogsTab;
    }

    public static boolean hideUsersTabByAppProps() {
        return appPropsBean.hideUsersTab;
    }

    public static boolean hideUserRolesTabByAppProps() {
        return appPropsBean.hideUserRolesTab;
    }

    public static boolean hideRightsBtn() {
        return appPropsBean.hideRightsBtn;
    }

    public static boolean hideHelpForAttr() {
        return appPropsBean.hideHelpForAttr;
    }

    public static boolean showDescriptionAfterAttributes() {
        return appPropsBean.showDescriptionAfterAttributes;
    }

    public static boolean hideGrantRightsBtnByAppProps() {
        return appPropsBean.hideGrantRightsBtn;
    }

    public static boolean newRightsSysByAppProps() {
        return appPropsBean.newRightsSys;
    }

    public static String htmlEntitiesDecode(String value) {
        return value.replaceAll("&amp;", "&").replaceAll("&gt;", ">").replaceAll("&lt;", "<");
    }

    public static AttributeHyperlinkBean getAttributeHyperlinkBeanFromValue(String atrLinValue) {
        AttributeHyperlinkBean ahb = new AttributeHyperlinkBean();
        List<String> encodedValues = BaseUtils.splitBySep(atrLinValue, ",");
        String nodeIdLink = encodedValues.get(0);
        ahb.caption = BaseUtils.substringFromEnd(atrLinValue, atrLinValue.length() - atrLinValue.indexOf(",") - 1);
        List<String> link = BaseUtils.splitBySep(nodeIdLink, "_");
        if (link.size() == 2) {
            ahb.nodeId = BaseUtils.tryParseInteger(link.get(0));
            ahb.treeCode = link.get(1);
        }
        return ahb;
    }

    public static boolean expandSelectedNodesInHyperlinkAttribute() {
        return appPropsBean.expandSelectedNodesInHyperlinkAttribute;
    }

//    public static boolean hideAttrAndNodeKindBtnsOnRight() {
//        return appPropsBean.hideAttrAndNodeKindBtnsOnRight;
//    }
    public static boolean hideAddAnyNodeForKindBtn() {
        return appPropsBean.hideAddAnyNodeForKindBtn;
    }

    public static boolean hideActionMoveInTree() {
        return appPropsBean.hideActionMoveInTree;
    }

    public static boolean isEnablePrinting() {
        return appPropsBean.enablePrinting;
    }

    public static boolean hideAddAttrBtnsOnRight() {
        return appPropsBean.hideAddAttrBtnsOnRight;
    }

    public static boolean hideAddNodeKindBtnsOnRight() {
        return appPropsBean.hideAddNodeKindBtnsOnRight;
    }

    public static boolean showAddStatus() {
        return appPropsBean.showAddStatus;
    }

}
