/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill.renderctx;

import java.util.Map;

/**
 *
 * @author wezyr
 */
public interface IRenderCtx {

    public IOutputDevice getOutputDevice();

    public Object getRootModel();

    public Map<String, Object> getVars();

    public void pushIfCondVal(boolean bVal);

    public boolean popIfCondVal();
}
