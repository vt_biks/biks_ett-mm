/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

/**
 *
 * @author beata
 */
public interface IGridNodeAwareBeanPropsExtractor<T> extends IGridGenericBeanPropsExtractor<T> {

    public Integer getNodeId(T b); //dstId

//    public String getTreeCode(T b); //tabId
    //public String getNodeKindCode(T b);  //kod do ikonki, dstcode
    public String getBranchIds(T b);

    public String getOptIconHint(T b);

    public boolean hasChildren(T b);

//    public String getActionIconUrl(T b, int idx);
//
//    public String getActionIconHint(T b, int idx);
//
//    public String executeAction(T b, int idx);
    public boolean isRowVisible(T b, int addColIdx);

    //ww: zakomentowałem, bo miało dziwną nazwę, chciałem poprawić, ale
    // nie udało mi się znaleźć użycia, więc zakomentowane. użycie było mi zaś
    // potrzebne aby zrozumieć jak to się powinno nazywać
    // tak czy inaczej "filtr" to nie bardzo, "filter" jeśli już ;-)
//    public boolean isSomethingToFiltr(List<T> beansToDisplay);
    public boolean hasNameHints();

    public int howManyTimesNameColumnWiderThanOther();

    public void setUsingOnDialog(boolean isWidgetRunFromDialog);

    public boolean hasSelectedRowStyle(T b);

    public Double getOptSimilarityScore(T b);

    public String getOptExtraStyleForRow(T b);

    public Integer getJoinedObjId(T b);
}
