/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.EntityDetailsPaneDialog;
import pl.fovis.common.FvsChangeExBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IContinuation;

/**
 *
 * @author bfechner
 */
public class MyBIKSSuggestedElementsGrid {

    public MyBIKSSuggestedElementsGrid(IContinuation ic) {
        this.ic = ic;
    }

    IContinuation ic;
    final FlexTable gp = new FlexTable();

    public Widget buildGrid() {
        gp.clear();
        gp.setWidth("100%");
        gp.setStyleName("gridJoinedObj");
        BIKClientSingletons.getService().getSuggestedElementsForLoggedUser(new StandardAsyncCallback<List<FvsChangeExBean>>() {

            @Override
            public void onSuccess(List<FvsChangeExBean> result) {
                int row = 0;

                for (FvsChangeExBean sb : result) {
                    createRowFromBean(row++, sb);
                }

            }
        });
        setUpGridColumnsWidths(gp);
        return gp;
    }

    protected Image getIconUrl(FvsChangeExBean n) {
        return new Image("images" /* I18N: no */ + "/" + BIKClientSingletons.getNodeKindIconNameByCode(n.code, n.tabId) + "." + "gif" /* I18N: no */);
    }

    protected void createRowFromBean(int row, FvsChangeExBean n) {
        int columnsCnt = 0;
        gp.setWidget(row, columnsCnt++, new Label(""));
        gp.setWidget(row, columnsCnt++, getIconUrl(n));

        Widget nameWidget = createHtmlLabelLookupButton(n.name, n.treeCode, n.nodeId);
        String title = n.menuPath + BIKGWTConstants.RAQUO_STR_SPACED + n.namePath;
        nameWidget.setTitle(title);
        String treeCode = n.tabId;
        int nodeId = n.nodeId;

        gp.setWidget(row, columnsCnt++, nameWidget);
        gp.setWidget(row, columnsCnt++, createLookupButton(treeCode, nodeId));
        gp.setWidget(row, columnsCnt++, BIKClientSingletons.createFollowButton(treeCode, nodeId, true));
        gp.setWidget(row, columnsCnt++, createAddToFvsBtn(nodeId));
        gp.getRowFormatter().setStyleName(row, row % 2 == 0 ? "RelatedObj-oneObj-grey" : "RelatedObj-oneObj-white");
    }

    protected PushButton createAddToFvsBtn(final int nodeId) {
        PushButton addToFvsBtn = new PushButton(new Image("images/addToFav.gif" /* I18N:  */));
        addToFvsBtn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                List<Integer> nodeIds = new ArrayList<Integer>();
                nodeIds.add(nodeId);
                BIKClientSingletons.getService().addToFvs(nodeIds, new StandardAsyncCallback<Boolean>() {

                    @Override
                    public void onSuccess(Boolean result) {
                        icDoIt();
                    }
                });

            }
        });

        addToFvsBtn.setTitle(I18n.dodajDoUlubionych.get() /* I18N:  */);
        addToFvsBtn.addStyleName("loupeBtn"/*
         * "zeroBtn loupe"
         */);
        return addToFvsBtn;
    }

    protected void setUpGridColumnsWidths(FlexTable gp) {
        gp.setWidth("100%");
        gp.getColumnFormatter().setWidth(0, "16px");
        gp.getColumnFormatter().setWidth(1, "16px");
        gp.getColumnFormatter().setWidth(3, "20px");
        gp.getColumnFormatter().setWidth(4, "26px");
        gp.getColumnFormatter().setWidth(5, "26px");
        gp.getColumnFormatter().setWidth(6, "26px");
    }

    public PushButton createLookupButton(final String nodeTreeCode, final int nodeId) {
        PushButton lookupBtn = new PushButton(new Image("images/loupe.gif" /* I18N:  */));
        lookupBtn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {

                buildAndShowDialog(nodeId, nodeTreeCode);

            }
        });

        lookupBtn.setTitle(I18n.podglad.get() /* I18N:  */);
        lookupBtn.addStyleName("loupeBtn");
        return lookupBtn;
    }

    public BikCustomButton createHtmlLabelLookupButton(String captionHtml, final String nodeTreeCode, final int nodeId) {
        BikCustomButton labelBtn = new BikCustomButton(captionHtml, "Blogs" /* I18N: no */ + "-blogTitle",
                new IContinuation() {
                    public void doIt() {
                        buildAndShowDialog(nodeId, nodeTreeCode);
                    }
                });
        return labelBtn;
    }

    protected void buildAndShowDialog(int nodeId, final String nodeTreeCode) {
        EntityDetailsPaneDialog edpd = new EntityDetailsPaneDialog() {

            @Override
            protected void doAction() {
                super.doAction(); //To change body of generated methods, choose Tools | Templates.
                icDoIt();
            }

            @Override
            protected void doCancel() {
                super.doCancel(); //To change body of generated methods, choose Tools | Templates.
                icDoIt();
            }

        };
        edpd.buildAndShowDialog(nodeId, null, nodeTreeCode);
    }

    public void icDoIt() {
        ic.doIt();
    }
}
