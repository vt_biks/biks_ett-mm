/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.TreeSelectorForUsers;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class NodeActionLinkUser extends NodeActionUpdatingBase {

    public String getCaption() {
        return I18n.przypiszDoGrupy.get() /* I18N:  */;
    }

    public void execute() {

//        BIKClientSingletons.getService().getBikAllUsers(
//                new StandardAsyncCallback<List<UserBean>>("Failure on getBikAllUsers") {
//
//                    public void onSuccess(List<UserBean> result) {

        final TreeSelectorForUsers tsfjod = new TreeSelectorForUsers();
        tsfjod.buildAndShowDialog(0, //"Users" /* I18N: no */,
                new IParametrizedContinuation<TreeNodeBean>() {
            public void doIt(final TreeNodeBean param) {
                BIKClientSingletons.getService().addLinkUser(BaseUtils.nullToDef(param.linkedNodeId, param.id), param.name, node.id, treeBean.id,
                        new StandardAsyncCallback< Pair<Integer, Boolean>>("Error in" /* I18N: no */ + " addLinkUser") {
//
                    public void onSuccess(Pair<Integer, Boolean> result) {
                        BIKClientSingletons.showInfo(result.v2 ? I18n.przypisanoUzytkownikaDoGrupy.get() /* I18N:  */ : I18n.uzytkownJestJuzPrzypisaDoTejGrup.get() /* I18N:  */);
                        refreshRedisplayDataAndSelectRecord(result.v1/*, false*/);
                    }
                });
            }
        }, null, false, node.id);

//                        new PickUserDialog().pickOneItem(result, null, new IParametrizedContinuation<UserBean>() {
//
//                            public void doIt(UserBean param) {
//                                BIKClientSingletons.getService().addLinkUser(param.nodeId, param.name, node.id, new StandardAsyncCallback<Void>("Error in addLinkUser") {
//
//                                    public void onSuccess(Void result) {
//                                        BIKClientSingletons.showInfo("Przypisano użytkownika do grupy");
//                                        refreshRedisplayDataAndSelectRecord(node.id/*, false*/);
//                                    }
//                                });
//                            }
//                        }, false);
//                    }
//                });
    }

    @Override
    public boolean isEnabledByDesign() {
        if (isNodeOneOfTheKind(true, BIKGWTConstants.NODE_KIND_USERS_GROUP) && isAdminOrExpertOrAuthorOrEditor()/*isUserLoggedIn()*/) {
            return true;
        } /*else {*/
        return false;
//        }
    }
}
