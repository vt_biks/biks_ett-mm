/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.treeandlist;

import com.google.gwt.http.client.Request;
import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import static pl.fovis.client.BIKGWTConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand;
import pl.fovis.client.bikpages.IPageDataFetchBroker;
import pl.fovis.client.bikpages.TreeDataFetchAsyncCallback;
import pl.fovis.common.BikNodeTreeOptMode;
import pl.fovis.foxygwtcommons.ClientDiagMsgs;
import pl.fovis.foxygwtcommons.treeandlist.IFoxyTreeGrid;
import pl.fovis.foxygwtcommons.treeandlist.ILazyRowLoaderEx;
import pl.fovis.foxygwtcommons.treeandlist.ITreeRowAndBeanStorage;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public abstract class AbstractLazyRowLoader<BEAN, IDT> implements ILazyRowLoaderEx<Map<String, Object>, IDT>/*, IBean4TreeExtractor<BEAN, IDT>*/ {

    protected ITreeRowAndBeanStorage<IDT, Map<String, Object>, BEAN> storage;
    protected IFoxyTreeGrid<IDT> treeGrid;
    protected BikNodeTreeOptMode optExtraMode;
    protected IPageDataFetchBroker dataFetchBroker;
    protected boolean calcHasNoChildren = true;
    protected IBean4TreeExtractor<BEAN, IDT> beanExtractor = null;
    protected String optFilteringNodeActionCode;

    public AbstractLazyRowLoader(IFoxyTreeGrid<IDT> treeGrid,
            ITreeRowAndBeanStorage<IDT, Map<String, Object>, BEAN> storage,
            BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode,
            IPageDataFetchBroker dataFetchBroker, boolean calcHasNoChildren) {
        this.treeGrid = treeGrid;
        this.storage = storage;
        this.optExtraMode = optExtraMode;
        this.optFilteringNodeActionCode = optFilteringNodeActionCode;
        this.dataFetchBroker = dataFetchBroker;
        this.calcHasNoChildren = calcHasNoChildren;
    }

//    protected void setNoLikedNodes(boolean noLinkedNodes){
//        this.noLinkedNodes = noLinkedNodes;
//    }
    protected BEAN getNodeBeanByDataNode(Map<String, Object> dataNode) {
        return storage.getBeanById(storage.getTreeBroker().getNodeId(dataNode));
    }

    protected Map<String, Object> registerAndConvertTreeNodeBeanToMap(BEAN node) {
        return storage.registerAndConvertBeanToRow(node);
    }

    public boolean isLeafForSure(Map<String, Object> row) {
        return getExtractor().extractHasNoChildren(getNodeBeanByDataNode(row));
    }

    public void loadSubRowsOnExpand(final Map<String, Object> row) {
        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$WW_loadSubRowsOnExpand)) {
            ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$WW_loadSubRowsOnExpand, "starting, row=" + row /* I18N: no */);
        }

        final BEAN rowAsTnb = getNodeBeanByDataNode(row);
//        if (extractLinkedNodeIdFromBean(rowAsTnb) != null) {
//            rowAsTnb.isFromLinkedSubtree = true;
//        }

        Request req = /*BIKClientSingletons.getService().getOneSubtreeLevelOfTree*/ getOneSubtreeLevelOfTree(rowAsTnb,
                        optExtraMode, optFilteringNodeActionCode, calcHasNoChildren(),
                        new TreeDataFetchAsyncCallback<List<BEAN>>(dataFetchBroker) {
                            @Override
                            protected void doOnSuccess(List<BEAN> result) {

                                if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$WW_loadSubRowsOnExpand)) {
                                    ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$WW_loadSubRowsOnExpand, "got rows from server, cnt" /* I18N: no */ + "=" + result.size());
                                }
                                List<Map<String, Object>> childRows = new ArrayList<Map<String, Object>>();
                                for (BEAN tnb : result) {
                                    optFixBeanPropsAfterLoad(tnb, rowAsTnb);
                                    childRows.add(registerAndConvertTreeNodeBeanToMap(tnb));
                                }

                                if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$WW_loadSubRowsOnExpand)) {
                                    ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$WW_loadSubRowsOnExpand, "rows converted to maps" /* I18N: no */);
                                }

                                treeGrid.addChildNodes(row, childRows);
                                if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$WW_loadSubRowsOnExpand)) {
                                    ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$WW_loadSubRowsOnExpand, "rows added to tree (optimized" /* I18N: no */ + "?)");
                                }
                            }
                        });

        dataFetchBroker.dataFetchStarting(IPageDataFetchBroker.TreeDataRequestKind.Additional, req,
                null);

        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$WW_loadSubRowsOnExpand)) {
            ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$WW_loadSubRowsOnExpand, "end - request is sent" /* I18N: no */);
        }
    }

    public boolean calcHasNoChildren() {
        return calcHasNoChildren;
    }

    public void optFixBeanPropsAfterLoad(BEAN tb, BEAN ancestorBean) {
        // no op
    }

    protected void registerAndAddToTreeRecursively(Set<IDT> existingParentIds,
            Map<IDT, List<BEAN>> nodesByParentIds) {

        if (existingParentIds.isEmpty()) {
            return;
        }

        Set<IDT> newExistingParentIds = new HashSet<IDT>();

        for (IDT existingParentId : existingParentIds) {
            if (storage.getBeanById(existingParentId) != null) {
                List<BEAN> children = nodesByParentIds.get(existingParentId);
                if (children != null) {

                    List<Map<String, Object>> childRowsToAdd = new ArrayList<Map<String, Object>>();

                    for (BEAN child : children) {
                        final IDT childId = getExtractor().extractId(child);
                        if (storage.getBeanById(childId) == null) {
                            childRowsToAdd.add(registerAndConvertTreeNodeBeanToMap(child));
                            newExistingParentIds.add(childId);
                        }
                    }

                    treeGrid.addChildNodes(storage.getRowById(existingParentId), childRowsToAdd);
                }
            }
        }

        registerAndAddToTreeRecursively(newExistingParentIds, nodesByParentIds);
    }

    protected void registerAndAddToTree(List<BEAN> nodes) {
        Map<IDT, List<BEAN>> nodesByParentIds = new HashMap<IDT, List<BEAN>>();
        Set<IDT> existingParentIds = new HashSet<IDT>();

        for (BEAN node : nodes) {
            IDT parentId = getExtractor().extractParentId(node);

            if (storage.getBeanById(parentId) != null) {
                existingParentIds.add(parentId);
            }

            List<BEAN> childNodes = nodesByParentIds.get(parentId);
            if (childNodes == null) {
                childNodes = new ArrayList<BEAN>();
                nodesByParentIds.put(parentId, childNodes);
            }

            childNodes.add(node);
        }

        registerAndAddToTreeRecursively(existingParentIds, nodesByParentIds);
    }

    // ta metoda jest wywoływana tylko po metodzie initWithFullData - czyli gdy zaczytamy główny poziom drzewa
    public void loadNodesAndAncestorsAndExpand(final Collection<IDT> nodeIds, final IDT optNodeIdToSelect, final IParametrizedContinuation<IDT> optCont) {

//        final Set<IDT> allIdsToExpand = new HashSet<IDT>(nodeIds);
        Set<IDT> unloadedIds = new HashSet<IDT>();

        for (IDT nodeId : nodeIds) {
            if (storage.getBeanById(nodeId) == null) {
                unloadedIds.add(nodeId);
            }
        }

        if (optNodeIdToSelect != null && storage.getBeanById(optNodeIdToSelect) == null) {
            unloadedIds.add(optNodeIdToSelect);
        }

        // te, których jeszcze nie mamy wczytanych (unloadedIds) - trzeba doczytać
        // ich przodków (całe gałęzie dla każdego z unloadedIds),
        // ale uwaga: ta lista może być w tym miejscu pusta
        // druga uwaga: jeżeli jakiś węzeł na liście unloadedIds jest korzeniem,
        // to dla niego nie będzie żadnego wyniku
        // z drugiej strony - na liście unloadedIds nie powinny pojawiać się węzły z
        // korzenia, bo one powinny być już wczytane we wcześniejszej metodzie initWithFullData
        Request req = getBikNodeAncestorIdsMulti(unloadedIds,
                new TreeDataFetchAsyncCallback<Map<IDT, List<IDT>>>(dataFetchBroker) {
                    @Override
                    protected void doOnSuccess(final Map<IDT, List<IDT>> allAncestorNodeIds) {

                        // dla tych węzłów trzeba będzie załadować ich dzieci, bo to konieczne,
                        // aby móc rozwinąć (expand) węzły
                        Set<IDT> nodeIdsToLoad = new HashSet<IDT>(nodeIds);

                        List<IDT> optAncestorIdsOfNodeToSelect = null;

                        for (Entry<IDT, List<IDT>> e : allAncestorNodeIds.entrySet()) {

                            IDT nodeId = e.getKey();

                            // ten warunek wydaje się bez sensu, bo nodeIdsToLoad
                            // zawiera inicjalnie nodeIds, a to jest tożsame (ta sama zawartość)
                            // co w allIdsToExpand, więc skoro:
                            // allIdsToExpand.contains(nodeId), to
                            // nodeIdsToLoad.contains(nodeId), więc nodeIdsToLoad.add
                            // jest nadmiarowe, ale nieszkodliwe
//                            if (allIdsToExpand.contains(nodeId)) {
//                                nodeIdsToLoad.add(nodeId);
//                            }
                            List<IDT> ancestorIds = e.getValue();

                            if (optNodeIdToSelect != null && BaseUtils.safeEquals(optNodeIdToSelect, nodeId)) {
                                optAncestorIdsOfNodeToSelect = ancestorIds;
                            }

                            // dla każdego przodka niezaładowanego węzła do rozwinięcia
                            // trzeba załadować jego dzieci
                            for (IDT ancestorId : ancestorIds) {
                                // warunek na hasUnloadedChildren jest konieczny - bo węzeł (szczególnie korzeń)
                                // może być już załadowany, ale nadal trzeba załadować jego dzieci, żeby
                                // potem dało się taki węzeł rozklikać (expand)
                                if (storage.getBeanById(ancestorId) == null || treeGrid.hasUnloadedChildren(ancestorId)) {
                                    nodeIdsToLoad.add(ancestorId);
                                }
                            }
                        }

                        if (optAncestorIdsOfNodeToSelect != null) {
                            optAncestorIdsOfNodeToSelect.add(optNodeIdToSelect);
                        } else if (optNodeIdToSelect != null) {
                            // ten przypadek oznacza, że optNodeIdToSelect jest już załadowany
                            // czyli najprawdopodobniej jest on jednym z korzeni (root node)
                            // inna możliwość - jest błędny i nie ma dla niego przodków
                            // bo dla usuniętych (logicznie: is_deleted <> 0) też mamy ścieżkę przodków

                            if (storage.getBeanById(optNodeIdToSelect) != null) {
                                optAncestorIdsOfNodeToSelect = new ArrayList<IDT>();
                                optAncestorIdsOfNodeToSelect.add(optNodeIdToSelect);
                            }
                        }

                        final List<IDT> finalOptAncestorIdsOfNodeToSelect = optAncestorIdsOfNodeToSelect;

                        Request req2 = getSubtreeLevelsByParentIds(nodeIdsToLoad,
                                optExtraMode, optFilteringNodeActionCode,
                                new TreeDataFetchAsyncCallback<List<BEAN>>(dataFetchBroker) {
                                    @Override
                                    protected void doOnSuccess(List<BEAN> result) {
                                        registerAndAddToTree(result);

                                        Set<IDT> nodesToExpand = new HashSet<IDT>();

                                        for (IDT nodeId : nodeIds) {
                                            if (storage.getBeanById(nodeId) != null) {
                                                nodesToExpand.add(nodeId);
                                            }
                                        }

                                        treeGrid.expandNodesByIds(nodesToExpand, null);

                                        IDT selectedId = null;

                                        if (finalOptAncestorIdsOfNodeToSelect != null) {
                                            for (int i = finalOptAncestorIdsOfNodeToSelect.size() - 1; i >= 0; i--) {
                                                IDT idToSelect = finalOptAncestorIdsOfNodeToSelect.get(i);
                                                if (storage.getBeanById(idToSelect) != null) {
                                                    treeGrid.selectRecord(idToSelect);
                                                    selectedId = idToSelect;
                                                    break;
                                                }
                                            }
                                        }

                                        if (optCont != null) {
                                            optCont.doIt(selectedId);
                                        }
                                    }
                                });

                        dataFetchBroker.dataFetchStarting(IPageDataFetchBroker.TreeDataRequestKind.Additional, req2, null);
                    }
                });

        dataFetchBroker.dataFetchStarting(IPageDataFetchBroker.TreeDataRequestKind.Additional, req, null);
    }

    @Override
    public void loadAncestorsForSelection(final IDT id, final boolean expand, final IParametrizedContinuation<IDT> optContInsteadOfSelectRecord) {

        Request req = /*BIKClientSingletons.getService().getBikNodeAncestorIds*/ getBikNodeAncestorIds(id,
                        new TreeDataFetchAsyncCallback<List<IDT>>(dataFetchBroker) {
                            @Override
                            protected void doOnSuccess(final List<IDT> allAncestorNodeIds) {
                                final Map<IDT, Integer> nodeIdsWithLevelOnPath = new HashMap<IDT, Integer>();
                                final int allAncestorsCnt = allAncestorNodeIds.size();
                                for (int i = 0; i < allAncestorsCnt; i++) {
                                    nodeIdsWithLevelOnPath.put(allAncestorNodeIds.get(i), i);
                                }
                                nodeIdsWithLevelOnPath.put(id, allAncestorsCnt);

                                int i = 0;
                                while (i < allAncestorsCnt && storage.getBeanById(allAncestorNodeIds.get(i)) != null) {
                                    i++;
                                }
                                final int firstUnloadedIdx = i;

                                if (firstUnloadedIdx == 0) {
                                    if (optContInsteadOfSelectRecord == null) {
                                        treeGrid.selectFirstRecord();
                                    } else {
                                        optContInsteadOfSelectRecord.doIt(null);
                                    }
                                    return;
                                }

//                                final List<IDT> ancestorIdsToLoad = new ArrayList<IDT>(allAncestorNodeIds.subList(firstUnloadedIdx - 1, allAncestorsCnt));
                                final Set<IDT> ancestorIdsToLoad = new LinkedHashSet<IDT>(allAncestorNodeIds.subList(firstUnloadedIdx - 1, allAncestorsCnt));
                                if (expand) {
                                    ancestorIdsToLoad.add(id);
                                }
                                Request req2 = /*BIKClientSingletons.getService().getSubtreeLevelsByParentIds*/ getSubtreeLevelsByParentIds(ancestorIdsToLoad,
                                        optExtraMode, optFilteringNodeActionCode,
                                        new TreeDataFetchAsyncCallback<List<BEAN>>(dataFetchBroker) {
                                            @Override
                                            protected void doOnSuccess(List<BEAN> result) {
                                                IDT bestIdToSelect = null;
                                                int bestLevel = -1;

                                                Map<IDT, List<Map<String, Object>>> collected = new HashMap<IDT, List<Map<String, Object>>>();
                                                for (BEAN tnb : result) {
                                                    final IDT beanId = getExtractor().extractId(tnb);

                                                    Integer candidateLevel = nodeIdsWithLevelOnPath.get(beanId);
                                                    if (candidateLevel != null && candidateLevel > bestLevel) {
                                                        bestLevel = candidateLevel;
                                                        bestIdToSelect = beanId;
                                                    }

                                                    if (storage.getBeanById(beanId) != null) {
                                                        //ww: to jest możliwe gdy następuje przejście na zakładkę
                                                        // z linku do węzła, który już został usunięty
                                                        // wtedy może być poziom jego rodzeństwa już zaczytany
                                                        // a jego samego brak, więc tutaj ignorujemy już to
                                                        // istniejące rodzeństwo - eliminacja duplikacji węzłów
                                                        continue;
                                                    }

                                                    List<Map<String, Object>> l = collected.get(getExtractor().extractParentId(tnb));
                                                    if (l == null) {
                                                        l = new ArrayList<Map<String, Object>>();
                                                        collected.put(getExtractor().extractParentId(tnb), l);
                                                    }

                                                    l.add(registerAndConvertTreeNodeBeanToMap(tnb));
                                                }
                                                //ww: warunek <= bo może (jak expand == true) czytamy razem z dziećmi wybranego
                                                for (int i = firstUnloadedIdx - 1; i <= allAncestorsCnt; i++) {
                                                    IDT parentId = i == allAncestorsCnt ? id : allAncestorNodeIds.get(i);
                                                    Map<String, Object> parentRow = storage.getRowById(parentId);
                                                    List<Map<String, Object>> childrenToAdd = collected.get(parentId);
                                                    if (childrenToAdd == null) {
                                                        //ww: to się może teoretycznie zdarzyć, gdy mamy link do
                                                        // usuniętego, jego poziom rozklikany, to może nic tu nie znaleźć
                                                        // będzie null, więc ignorujemy ten poziom przy dodawaniu

                                                        //ww: zadbamy jeszcze o ustawienie, że ojciec jest liściem
                                                        // o ile wiadomo, że powinniśmy to zrobić
                                                        if (ancestorIdsToLoad.contains(parentId)) {
                                                            treeGrid.addChildNodes(parentRow, null);
                                                        }

                                                        continue;
                                                    }
                                                    treeGrid.addChildNodes(parentRow, childrenToAdd);
                                                }

                                                //ww: zazwyczaj te bestIdToSelect to będzie po prostu id
                                                // ale gdy będzie to id usunietego, to już nie, a jego rodzic
                                                // też mógł zostać usunięty itd. więc będzie to najlepszy
                                                // przodek, albo cała gałaź usunięte, więc możliwy w teorii null
                                                if (bestIdToSelect == null) {
                                                    bestIdToSelect = allAncestorNodeIds.get(firstUnloadedIdx - 1);
                                                }

                                                if (optContInsteadOfSelectRecord == null) {
                                                    treeGrid.selectRecord(bestIdToSelect, expand);
                                                } else {
                                                    optContInsteadOfSelectRecord.doIt(bestIdToSelect);
                                                }
                                            }
                                        });
                                dataFetchBroker.dataFetchStarting(IPageDataFetchBroker.TreeDataRequestKind.Additional, req2, null);
                            }
                        });

        dataFetchBroker.dataFetchStarting(IPageDataFetchBroker.TreeDataRequestKind.Additional, req, null);
    }

    public abstract Request getOneSubtreeLevelOfTree(BEAN bean,
            BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, boolean calcHasNoChildren, AsyncCallback<List<BEAN>> asyncCallback);

    public abstract Request getBikNodeAncestorIds(IDT nodeId, AsyncCallback<List<IDT>> asyncCallback);

    protected Request getBikNodeAncestorIdsMulti(Collection<IDT> nodeIds, AsyncCallback<Map<IDT, List<IDT>>> asyncCallback) {
        throw new UnsupportedOperationException("getBikNodeAncestorIdsMulti");
    }

    public abstract Request getSubtreeLevelsByParentIds(Collection<IDT> parentNodeIds,
            BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, AsyncCallback<List<BEAN>> asyncCallback);

    protected IBean4TreeExtractor<BEAN, IDT> getExtractor() {
        if (beanExtractor == null) {
            beanExtractor = createBeanExtractor();
        }
        return beanExtractor;
    }

    public abstract IBean4TreeExtractor<BEAN, IDT> createBeanExtractor();
}
