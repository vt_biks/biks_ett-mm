---------------------------------------------------
---------------------------------------------------
---------------------------------------------------
-- przed uruchomieniem tego skryptu
-- konieczne jest doinstalowanie Full-Text Search
-- dost�pnego w wersji Enterprise MS SQL Servera
--
-- by� o tym mail...
---------------------------------------------------
---------------------------------------------------
---------------------------------------------------

exec sp_check_version '1.0.86.8';
go

if SERVERPROPERTY('IsFullTextInstalled') <> 1
exec sp_check_version 'NO FULL-TEXT SEARCH DETECTED!!!';
go


---------------------------------------------------
---------------------------------------------------
---------------------------------------------------

if exists(select 1 from sys.fulltext_indexes where object_id = object_id('bik_node'))
drop fulltext index on bik_node
go

if exists(select 1 from sys.fulltext_catalogs where name = 'BIKS_FTS_Catalog')
DROP FULLTEXT CATALOG BIKS_FTS_Catalog;
go

CREATE FULLTEXT CATALOG BIKS_FTS_Catalog AS DEFAULT;
go
declare @pk_name sysname = (select name from sys.indexes where object_id = object_id('bik_node') and is_primary_key = 1)
exec('CREATE FULLTEXT INDEX ON bik_node (name, descr) key index ' + @pk_name)
go

---------------------------------------------------
---------------------------------------------------
---------------------------------------------------


delete from bik_app_prop where name = 'useFullTextIndex'
go

insert into bik_app_prop (name, val, is_editable) values ('useFullTextIndex', 'true', 1)
go


---------------------------------------------------
---------------------------------------------------
---------------------------------------------------

exec sp_update_version '1.0.86.8', '1.0.86.9';
go
