﻿exec sp_check_version '1.1.6.1';
go

 alter table bik_attr_def 
 add type_attr varchar(30) 
 go
 update bik_attr_def set type_attr= 'shortText'
 -------------
 alter table bik_attr_def 
 add value_opt varchar(max) 
 go


exec sp_update_version '1.1.6.1', '1.1.6.2';
go