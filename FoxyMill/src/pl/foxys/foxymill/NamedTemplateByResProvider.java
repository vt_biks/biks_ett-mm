/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill;

import commonlib.ILameResourceProvider;
import commonlib.LameUtils;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.foxys.foxymill.tags.ITag;
import simplelib.BaseUtils;

/**
 *
 * @author wezyr
 */
public class NamedTemplateByResProvider implements IFoxyMillNamedTagsProvider {

    private static final boolean DISABLE_TAG_CACHING = true;
    private static final boolean DEFAULT_MVEL = true;
    private final static long TIMESTAMP_CHECK_INTERVAL_MILLIS = 1000;
    private ILameResourceProvider resProvider;
    private String pathPrefix;
    private Map<String, Long> timeStamps = new HashMap<String, Long>();
    private Map<String, List<ITag>> fileContentsMap = new HashMap<String, List<ITag>>();
    private Map<String, Long> lastTimeStampCheckMap = new HashMap<String, Long>();
    private static final RenderEnvironment renderEnv = FoxyMillUtils.makeRenderEnvForHTML(DEFAULT_MVEL, DISABLE_TAG_CACHING);

    public NamedTemplateByResProvider(ILameResourceProvider resProvider, String pathPrefix) {
        this.resProvider = resProvider;
        this.pathPrefix = BaseUtils.ensureDirSepPostfix(pathPrefix);
    }

    public List<ITag> getParsedTemplate(String templateName) {
        String fileName = pathPrefix + templateName + ".html";

        List<ITag> res;

        Long lastTimeStampCheck = lastTimeStampCheckMap.get(templateName);
        Long currTimeStamp = null;
        long currSysMillis = System.currentTimeMillis();

        if (lastTimeStampCheck == null || lastTimeStampCheck + TIMESTAMP_CHECK_INTERVAL_MILLIS
                < currSysMillis) {
            currTimeStamp = resProvider.getLastModifiedOfResource(fileName);
            lastTimeStampCheckMap.put(templateName, currSysMillis);
            Long lastTimeStamp = timeStamps.get(templateName);

            if (lastTimeStamp != null && lastTimeStamp == currTimeStamp) {
                currTimeStamp = null;
            }
        }

        if (currTimeStamp != null) {
            String fileContents = LameUtils.loadAsString(
                    resProvider.gainResource(fileName), "utf-8");
            ParserTask pt = new ParserTask(fileName, fileContents);
            res = pt.read();
            FoxyMillUtils.prepareTags(res, renderEnv);
            timeStamps.put(templateName, currTimeStamp);
            fileContentsMap.put(templateName, res);
        } else {
            res = fileContentsMap.get(templateName);
        }
        return res;
    }
}
