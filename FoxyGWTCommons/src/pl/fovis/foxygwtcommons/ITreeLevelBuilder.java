/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import java.util.Collection;

/**
 *
 * @author wezyr
 */
public interface ITreeLevelBuilder<DN> {

    public void createChildNodes(Collection<DN> ns, DN p);
}
