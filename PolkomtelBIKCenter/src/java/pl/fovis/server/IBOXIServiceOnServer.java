/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import pl.fovis.client.BOXIService;
import pl.trzy0.foxy.commons.IFoxyTransactionalPerformer;

/**
 *
 * @author wezyr
 */
public interface IBOXIServiceOnServer extends BOXIService,
        IBOXIServiceForSearch, IFoxyTransactionalPerformer {

    public boolean isServletRequestLoggingEnabled();

    public int insertServiceRequest(String methodName, String parameters, String ipAddr,
            String sessionId, String optExtraInfo);

    public void updateServiceRequest(int reqId, String optExtraInfo, String optExceptionMsg);
}
