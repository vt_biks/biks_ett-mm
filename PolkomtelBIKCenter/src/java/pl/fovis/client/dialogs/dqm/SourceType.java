/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.dqm;

import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.common.BIKConstants;

/**
 *
 * @author bfechner
 */
public enum SourceType {

    Oracle(PumpConstants.SOURCE_NAME_ORACLE, PumpConstants.SOURCE_NAME_ORACLE, new DQMWidgetConfigOracle(), BIKConstants.TREE_CODE_ORACLE), SQLServer(PumpConstants.SOURCE_NAME_MSSQL, PumpConstants.SOURCE_NAME_MSSQL, new DQMWidgetConfigMSSQL(), BIKConstants.TREE_CODE_MSSQL), PostgreSQL(PumpConstants.SOURCE_NAME_POSTGRESQL, PumpConstants.SOURCE_NAME_POSTGRESQL, new DQMWidgetConfigPostgreSQL(), BIKConstants.TREE_CODE_POSTGRES), Profile(PumpConstants.SOURCE_NAME_PROFILE, PumpConstants.SOURCE_NAME_PROFILE, new DQMWidgetConfigProfile(), BIKConstants.TREE_CODE_PROFILE)/*, Multi("Multi", null, new DQMWidgetConfigMultiSource(), null)*/;

    private String sourceName;
    private IDQMWidgetConfig widget;
    private String name;
    private String treeCode;

    SourceType(String name, String sourceName, IDQMWidgetConfig widget, String treeCode) {
        this.name = name;
        this.sourceName = sourceName;
        this.widget = widget;
        this.treeCode = treeCode;
    }

    public String getSourceName() {
        return sourceName;
    }

    public String getName() {
        return name;
    }

    public IDQMWidgetConfig getWidget() {
        return widget;
    }

    public String getTreeCode() {
        return treeCode;
    }
}
