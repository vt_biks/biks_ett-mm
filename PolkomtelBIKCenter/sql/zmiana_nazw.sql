/*
alter table bik_node
add old_name varchar(4000);
go

update bik_node
set old_name = name
go
*/

-- select * from bik_tree

update bik_node
set name = SUBSTRING(name, 7, LEN(name))
where bik_node.tree_id = (select id from bik_tree where code = 'Teradata')
 and bik_node.name like 'VD[_]US[_]' + '%'
go

update bik_node
set name = SUBSTRING(name, 4, LEN(name))
where bik_node.tree_id = (select id from bik_tree where code = 'Teradata')
 and bik_node.name like 'VD[_]' + '%'
go

update bik_node
set name = SUBSTRING(name, 5, LEN(name))
where  bik_node.name like 'DWH[_]' + '%'
go

update bik_node
set name = SUBSTRING(name, 10, LEN(name))
where bik_node.name like 'Onephone[_]' + '%'
go

update bik_node
set name = SUBSTRING(name, 10, LEN(name))
where bik_node.name like 'sr[_]daiwn[_]' + '%'
go

/*
update bik_node
set name = SUBSTRING(name, 5, LEN(name))
where  bik_node.name like 'DWH[_]' + '%'
go
*/

update bik_node
set name = SUBSTRING(name, 4, LEN(name))
where  bik_node.tree_id = (select id from bik_tree where code = 'Connections')
 and bik_node.name like 'sr[_]' + '%'
go

update bik_node
set name = 'Biling'
where  bik_node.name like 'S2K'
go

update bik_node
set name = 'Windykacja należności'
where  bik_node.name like 'SR_DAIWN'
go

update bik_node
set name = 'Revenue assurance'
where  bik_node.name like 'SR_DKPINT'
go


update bik_node
set name = 'Marketing'
where  bik_node.name like 'RODOS'
go

update bik_node
set name = 'Biuro planowania'
where  bik_node.name like 'BPKIA'
go

declare @brancz varchar(100);
select @brancz = branch_ids
from bik_node join bik_node_kind on bik_node.node_kind_id = bik_node_kind.id join bik_tree on bik_node.tree_id = bik_tree.id 
where bik_node.name like 'PlusC' and tree_id = (select id from bik_tree where code = 'Reports')

update bik_node
set is_deleted = 1
where branch_ids like @brancz + '%'

update bik_node
set is_deleted = 1
from bik_node inner join bik_node org on org.id = bik_node.linked_node_id
where org.branch_ids like @brancz + '%'
go


declare @brancz varchar(100);
select @brancz = branch_ids
from bik_node join bik_node_kind on bik_node.node_kind_id = bik_node_kind.id join bik_tree on bik_node.tree_id = bik_tree.id 
where bik_node.name like 'SICAP' and tree_id = 1

update bik_node
set is_deleted = 1
where branch_ids like @brancz + '%'

update bik_node
set is_deleted = 1
from bik_node inner join bik_node org on org.id = bik_node.linked_node_id
where org.branch_ids like @brancz + '%'
go


update bik_node
set name = SUBSTRING(name, 4, LEN(name))
where  bik_node.name like 'SR[_]%'
go


update bik_node
set name = SUBSTRING(name, 9, LEN(name))
where  bik_node.name like 'BI[_]WEBI[_]%'
go




declare @brancz varchar(100);
select top 1 @brancz = branch_ids
from bik_node join bik_node_kind on bik_node.node_kind_id = bik_node_kind.id join bik_tree on bik_node.tree_id = bik_tree.id 
where bik_node.name like 'PlusC' and tree_id = (select id from bik_tree where code = 'ObjectUniverses')

update bik_node
set is_deleted = 1
where branch_ids like @brancz + '%'

update bik_node
set is_deleted = 1
from bik_node inner join bik_node org on org.id = bik_node.linked_node_id
where org.branch_ids like @brancz + '%'
go

declare @brancz varchar(100);
select top 1 @brancz = branch_ids
from bik_node join bik_node_kind on bik_node.node_kind_id = bik_node_kind.id join bik_tree on bik_node.tree_id = bik_tree.id 
where bik_node.name like 'SICAP' and tree_id = (select id from bik_tree where code = 'ObjectUniverses')

update bik_node
set is_deleted = 1
where branch_ids like @brancz + '%'

update bik_node
set is_deleted = 1
from bik_node inner join bik_node org on org.id = bik_node.linked_node_id
where org.branch_ids like @brancz + '%'
go


update bik_node
set name = REPLACE(name, 'sprzedaz_bpkia_', '')
where  bik_node.name like 'sprzedaz[_]bpkia[_]%'
go

update bik_node
set name = REPLACE(name, '_bpkia_', '_')
where  bik_node.name like '%[_]bpkia[_]%'
go

update bik_node
set name = REPLACE(name, 'DPIWN_', '')
where  bik_node.name like 'DPIWN[_]%'
go

update bik_node
set name = REPLACE(name, 'DAIWN ', '')
where  bik_node.name like 'DAIWN__%'
go


update bik_node
set name = REPLACE(name, 'DKPINT', 'Revenue assurance')
where  bik_node.name like '%DKPINT%'
go


update bik_node
set name = REPLACE(name, 'DPIWN ', '')
where  bik_node.name like 'DPIWN %'
go


update bik_node
set name = REPLACE(name, 'HU_', '')
where  bik_node.name like 'HU[_]%'
go


update bik_node
set name = REPLACE(name, 'SAPBW_BWP_', '')
where  bik_node.name like 'SAPBW[_]BWP[_]%'
and tree_id = (select id from bik_tree where code = 'Connections')
go

update bik_node
set name = REPLACE(name, 'S2K_', 'BILING_')
where  bik_node.name like 'S2K[_]%'
go


update bik_node
set name = REPLACE(name, 'RODOS_', 'MARKETING_')
where  bik_node.name like 'RODOS[_]%'
go


update bik_node
set name = REPLACE(name, 'iPlus', 'jMinus')
where  bik_node.name like '%iPlus%'
go


update bik_node
set descr = REPLACE(descr, 'iPlus', 'jMinus')
where  bik_node.descr like '%iPlus%'
go

update bik_node
set name = REPLACE(name, 'RODOS', 'MARKETING')
where  bik_node.name like '%RODOS%'
go

update bik_node
set name = REPLACE(name, 'DWH', 'DWD')
where  bik_node.name like '%DWH%'
go

update bik_node
set name = REPLACE(name, 'PLUS', 'MULTI')
where  bik_node.name like '%PLUS%'
go


update bik_node
set name = REPLACE(name, 'SPECTRUM', 'SONDA')
where  bik_node.name like '%SPECTRUM%'
go


update bik_node
set name = REPLACE(name, 'S2K', 'SND')
where  bik_node.name like '%S2K%'
go


update bik_node
set name = REPLACE(name, 'ADA', 'IGA')
where  bik_node.name like '%ADA%'
go

update bik_node
set name = REPLACE(name, 'RODOS', 'KRETA')
where  bik_node.name like '%RODOS%'
go

update bik_node
set name = REPLACE(name, 'SUBIEKT', 'SUBJECT')
where  bik_node.name like '%SUBIEKT%'
go


update bik_node
set name = REPLACE(name, 'SICAP', 'WPR')
where  bik_node.name like '%SICAP%'
go

update bik_node
set name = REPLACE(name, 'SMOK', 'DRAGON')
where  bik_node.name like '%SMOK%'
go

update bik_node
set name = REPLACE(name, 'DWN ', '')
where  bik_node.name like 'DWN %'
go

update bik_node
set name = REPLACE(name, '_DWN_', '')
where  bik_node.name like '[_]DWN[_]%'
go

update bik_node
set name = REPLACE(name, 'DWN_', '')
where  bik_node.name like 'DWN[_]%'
go


exec sp_rename 'bik_sapbo_extradata', 'bik_sapbo_extradata_old'
go

create view bik_sapbo_extradata as
select node_id, author, owner, subject, comments, keyword, 
(select name from bik_node where id = universe_node_id) as universe_name, 
(select name from bik_node where id = connection_node_id) as connection_name,
universe_node_id,
connection_node_id 
from bik_sapbo_extradata_old
go


/*
update bik_node
set name = old_name
*/