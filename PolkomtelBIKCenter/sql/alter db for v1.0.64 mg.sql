﻿exec sp_check_version '1.0.64';


if OBJECT_ID (N'dbo.fn_user_node_name', N'FN') is not null
    drop function dbo.fn_user_node_name;
go

create function dbo.fn_user_node_name(@login_name varchar(250), @old_login_name varchar(250), @name varchar(250), @is_deleted int)
returns varchar(4000)
with execute as caller
as
begin
  declare @login varchar(250)
  declare @new_name varchar(4000);
  if @is_deleted = 0
  begin
    set @login = @login_name
  end
  else
  begin
    set @login = @old_login_name
  end
  if @login != @name
  begin
    set @new_name = @name + ' (' + @login + ')'
  end
  else
  begin
    set @new_name = @name
  end
  return @new_name
end
go
  
exec sp_update_version '1.0.64', '1.0.65';
go
