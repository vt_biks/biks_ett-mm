/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.grid;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import java.io.Serializable;
import pl.fovis.foxygwtcommons.i18n.I18n;
import simplelib.INamedTypedBeanPropsBeanWrapperCreator;
import simplelib.INamedTypedPropsBeanBroker;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author pmielanczuk
 */
public class StandardBeanDisplay<T extends Serializable> implements IBeanDisplay<T> {

    public static final String ACT_EDIT = "act:edit";
    public static final String ACT_DELETE = "act:delete";
    protected final INamedTypedPropsBeanBroker<T> beanPropsBroker;
    protected IBeanGridEventsListener<T> beanGrid;
    protected IParametrizedContinuation<T> doEditCont;
    protected IParametrizedContinuation<T> doDeleteCont;

    public StandardBeanDisplay(Class<T> beanClass,
            IParametrizedContinuation<T> doEditCont,
            IParametrizedContinuation<T> doDeleteCont,INamedTypedBeanPropsBeanWrapperCreator<T> beanCreator) {
        this.beanPropsBroker = beanCreator.getBroker(beanClass);
        this.doEditCont = doEditCont;
        this.doDeleteCont = doDeleteCont;
    }

    @Override
    public Object getHeader(String colName) {
        return colName;
    }

    @Override
    public Object getValue(final T bean, String colName) {
        if (colName.equals(ACT_EDIT)) {
            return new Button(I18n.zmien.get() /* I18N:  */, new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    doEditCont.doIt(bean);
                }
            });
        }
        if (colName.equals(ACT_DELETE)) {
            return new Button(I18n.usun.get() /* I18N:  */, new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    doDeleteCont.doIt(bean);
                }
            });
        }

        return beanPropsBroker.getPropValue(bean, colName);
    }

//    @Override
//    public void setBeanGrid(IBeanGridEventsListener<T> beanGrid) {
//        this.beanGrid = beanGrid;
//    }
}
