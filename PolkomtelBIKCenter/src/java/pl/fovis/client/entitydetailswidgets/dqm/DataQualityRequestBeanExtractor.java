/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.dqm;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import static pl.fovis.client.BIKClientSingletons.fixFileResourceUrlEx;
import pl.fovis.client.ICheckEnabledByDesign;
import pl.fovis.client.dialogs.BIKSProgressInfoDialog;
import pl.fovis.client.entitydetailswidgets.NodeAwareBeanExtractorBase;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.dqm.DQMConstanst;
import pl.fovis.common.dqm.DataQualityRequestBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.FoxyClientSingletons;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.OneListBoxDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;
import simplelib.SimpleDateUtils;

/**
 *
 * @author beata
 */
public class DataQualityRequestBeanExtractor extends NodeAwareBeanExtractorBase<DataQualityRequestBean> {

    public static final String NODE_ACTION_DOWNLOAD_ERROR_DQM_DATA = "DownloadErrorDqmData";

    protected double errorThreshold;
    protected EntityDetailsDataBean nodeData;
    protected boolean isDQM = false;

    public DataQualityRequestBeanExtractor(double errorThreshold, EntityDetailsDataBean nodeData, boolean isDQM) {
        super(false);
        this.errorThreshold = errorThreshold;
        this.nodeData = nodeData;
        this.isDQM = isDQM;
    }

    @Override
    public Integer getNodeId(DataQualityRequestBean b) {
        return null;
    }

    @Override
    public String getTreeCode(DataQualityRequestBean b) {
        return null;
    }

    @Override
    protected String getName(DataQualityRequestBean b) {
        return null;
    }

    @Override
    protected String getNodeKindCode(DataQualityRequestBean b) {
        return null;
    }

    @Override
    public String getBranchIds(DataQualityRequestBean b) {
        return null;
    }

    @Override
    public boolean hasIcon() {
        return false;
    }

    @Override
    public int getAddColCnt() {
        return isDQM ? 6 : 5;
    }

    @Override
    public Object getAddColVal(final DataQualityRequestBean b, int addColIdx) {
        Map<Integer, Object> m = new HashMap<Integer, Object>();
        int i = 0;
//        if (!isDQM) {
        m.put(i++, String.valueOf(b.ordinalId));
//        }
        m.put(i++, createDateAdded(b.startTimestamp, "dqc-dateAdded"));
        m.put(i++, b.errorCount.toString());
        m.put(i++, b.caseCount.toString());
        String caseCountDivErrorCount;
        if (b.caseCount == 0) {
            caseCountDivErrorCount = "0";
        } else {
            caseCountDivErrorCount = BaseUtils.doubleToString((((double) b.errorCount / (double) b.caseCount) * 100), 2);
            if (errorThreshold < ((double) b.errorCount / (double) b.caseCount)) {
                if ("0.00".equals(caseCountDivErrorCount)) {
                    caseCountDivErrorCount = " < 0.01";
                }
                caseCountDivErrorCount = "<font color=\"red\"><b>" + caseCountDivErrorCount + "</b></font>";
            }

        }
        m.put(i++, new HTML(caseCountDivErrorCount));

        if (BIKClientSingletons.isNodeActionEnabledByCustomRightRoles(NODE_ACTION_DOWNLOAD_ERROR_DQM_DATA, nodeData,
                new ICheckEnabledByDesign() {

                    @Override
                    public boolean isEnabledByDesign() {
                        return b.hasErrorsFile;
                    }
                }
        )) {
            Image downloadErrorsFile = BIKClientSingletons.createIconImg("images/dqm_download.png", "16px", "16px", I18n.pobierzBledneDane.get());
            downloadErrorsFile.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    new OneListBoxDialog().buildAndShowDialog(I18n.wybierzFormatExportu.get(), Arrays.<String>asList(BIKConstants.EXPORT_FORMAT_XLSX, BIKConstants.EXPORT_FORMAT_CSV), new IParametrizedContinuation<String>() {

                        @Override
                        public void doIt(final String param) {
                            String clientFileName = b.testName + " " + SimpleDateUtils.dateTimeToStringNoSep(b.startTimestamp) + "." + param;
                            if (BIKConstants.EXPORT_FORMAT_XLSX.equals(param)) {
                                if (b.xlsxFileName != null) {
                                    downloadXlsxFromFile(b, clientFileName);
                                } else {
                                    downloadXlsxFromDB(b);
                                }
                            } else {
                                if (b.csvFileName != null) {
                                    downloadCsvFromFile(b, clientFileName);
                                } else {
                                    downloadCsvFromDB(b);
                                }
                            }
                        }
                    });
                }
            });
            downloadErrorsFile.setStyleName("btnPointer gridJoinedObj");

            if (isDQM) {
                m.put(i++, downloadErrorsFile);
            }
        }

        return m.get(addColIdx);
    }

    protected void downloadCsvFromFile(DataQualityRequestBean b, String clientFileName) {
        Window.open(fixFileResourceUrlEx(b.csvFileName, BaseUtils.encodeForHTMLTag(clientFileName)), "_blank", "");
    }

    protected void downloadCsvFromDB(DataQualityRequestBean b) {
        Window.open(generateGenerateFileLink(BIKConstants.EXPORT_FORMAT_CSV, b.id), "_blank", "");
    }

    protected void downloadXlsxFromFile(DataQualityRequestBean b, String clientFileName) {
        Window.open(fixFileResourceUrlEx(b.xlsxFileName, BaseUtils.encodeForHTMLTag(clientFileName)), "_blank", "");
    }

    protected void downloadXlsxFromDB(DataQualityRequestBean b) {
        final BIKSProgressInfoDialog infoDialog = new BIKSProgressInfoDialog();
        infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.przygotowywaniePliku.get(), true);
        BIKClientSingletons.getDQMService().prepareFileToDownload(BIKConstants.EXPORT_FORMAT_XLSX, b.id, new StandardAsyncCallback<Pair<String, String>>("Error in prepareFileToDownload") {

            @Override
            public void onSuccess(Pair<String, String> result) {
                infoDialog.hideDialog();
                Window.open(fixFileResourceUrlEx(BaseUtils.encodeForHTMLTag(result.v2), result.v1), "_blank", "");
            }

            @Override
            public void onFailure(Throwable caught) {
                infoDialog.hideDialog();
                super.onFailure(caught);
            }
        });
    }

    @Override
    public int getActionCnt() {
        return 0;
    }

    @Override
    public boolean createGridColumnNames(FlexTable gp) {
        gp.setStyleName("gridJoinedObj");
        int index = 0;
        gp.setWidget(0, index++, new HTML(I18n.requestID.get()));
        //isDQM
        HTML startDate = new HTML(I18n.startTimestamp.get());
        if (isDQM && nodeData != null && nodeData.dataQualityTestExtradata != null && nodeData.dataQualityTestExtradata.resultFileName != null) {
            startDate.setTitle(I18n.dataWykonaniaDlaProfile.get());
        }
        gp.setWidget(0, index++, startDate);
        gp.setWidget(0, index++, new HTML(I18n.errorCount.get()));
        gp.setWidget(0, index++, new HTML(I18n.caseCount.get() + "&nbsp;&nbsp;&nbsp;"));
        HTML errDivCase = new HTML(I18n.errorCountDivCaseCountShort.get() + "&nbsp;&nbsp;&nbsp;");
        errDivCase.setTitle(I18n.errorCountDivCaseCount.get());
        gp.setWidget(0, index, errDivCase);

        gp.getColumnFormatter().setWidth(0, "10%");
        gp.getColumnFormatter().setWidth(1, "20%");
        gp.getColumnFormatter().setWidth(2, "23%");
        gp.getColumnFormatter().setWidth(3, "23%");
        gp.getColumnFormatter().setWidth(4, "24%");

        if (isDQM) {
            gp.getColumnFormatter().setWidth(2, "20%");
            gp.getColumnFormatter().setWidth(3, "20%");
            gp.getColumnFormatter().setWidth(4, "25%");
            gp.getColumnFormatter().setWidth(5, "5%");
            gp.setWidget(0, ++index, new HTML(I18n.pobierz.get()));
        }

        gp.getRowFormatter().setStyleName(0, "RelatedObj-oneObj-dqc");
        return true;
    }

    protected String generateGenerateFileLink(String exportFormat, long requestId) {
        StringBuilder sb = new StringBuilder(FoxyClientSingletons.getWebAppUrlPrefixForFileHandlingServlet());
        sb.append("?").append(DQMConstanst.DQM_EXPORT_PARAM_NAME).append("=").append(exportFormat).append("&").append(DQMConstanst.DQM_EXPORT_REQUEST_ID_PARAM_NAME).append("=").append(requestId);
        return sb.toString();
    }

    @Override
    public boolean hasSelectedRowStyle(DataQualityRequestBean b) {
        return BIKClientSingletons.requestId != null && BIKClientSingletons.requestId == b.id;
    }
}
