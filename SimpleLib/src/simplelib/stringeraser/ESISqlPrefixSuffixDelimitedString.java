/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.stringeraser;

import simplelib.LameRuntimeException;
import simplelib.Pair;

/**
 *
 * @author pmielanczuk
 */
public class ESISqlPrefixSuffixDelimitedString implements IErasableSyntaxItem {

    protected String prefix, suffix;

    public ESISqlPrefixSuffixDelimitedString(String prefix, String suffix) {
        this.prefix = prefix;
        this.suffix = suffix;
    }

    public EsiPos findStartPos(final String content, int pos, int searchPosLimit) {
        while (pos < searchPosLimit) {

            if (content.startsWith(prefix, pos)) {
                final int outerPos = pos;
                return new EsiPos() {
                    {
                        startPos = outerPos;
                        innerStartPos = startPos + prefix.length();
                    }

                    @Override
                    public Pair<Integer, Integer> calcEndPos() {
                        return calcEndPosX(content, innerStartPos);
                    }
                };
            }

            pos++;
        }

        return null;
    }

    protected int findAfterEscapePos(String content, int suffixPos) {
        return -1;
    }

    protected int findSuffixPos(String content, int pos) {
        return content.indexOf(suffix, pos);
    }

    protected int findAfterSuffixPos(String content, int suffixPos) {
        return suffixPos + suffix.length();
    }

    protected Pair<Integer, Integer> calcEndPosX(String content, int pos) {
        while (true) {
            int suffixPos = findSuffixPos(content, pos);

            if (suffixPos == -1) {
                throw new LameRuntimeException("no closing suffix \"" + suffix + "\" after pos=" + pos);
            }

            int afterEscapePos = findAfterEscapePos(content, suffixPos);

            if (afterEscapePos == -1) {
                return new Pair<Integer, Integer>(suffixPos, findAfterSuffixPos(content, suffixPos));
            }

            pos = afterEscapePos;
        }
    }
}
