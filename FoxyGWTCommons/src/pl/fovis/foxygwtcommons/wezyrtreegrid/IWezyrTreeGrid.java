/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.wezyrtreegrid;

import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.IsWidget;
import java.util.Comparator;
import java.util.Set;
import pl.fovis.foxygwtcommons.treeandlist.ILazyRowLoader;
import simplelib.IContinuation;

/**
 *
 * @author wezyr
 */
public interface IWezyrTreeGrid<ROW> extends IsWidget {

    public void addFooterRow(Object[] footerRowVals);

    public static enum AddRowMode {

        AsSibling, AsFirstChild, AsLastChild
    };
    String ACTIVE_ROW_STYLE_NAME = "active" /* I18N: no */;
    String CUT_ROW_STYLE_NAME = "cut" /* I18N: no */;

    void addExpandCollapseHandler(IExpandCollapseHandler<ROW> handler);

    void addDoubleClickHandler(IDoubleClickHandler<ROW> handler);

    void addRightClickHandler(IRightClickHandler<ROW> handler);

    void addRowSelectionHandler(IRowSelectionHandler<ROW> handler);

    void addRow(ROW row, ROW prevRow, AddRowMode mode, boolean isLeafForSure);

    void addRows(final Iterable<ROW> rows, ROW prevRow, AddRowMode mode, boolean isLeafForSure);

    void clearAllRows();

    void collapseRow(ROW row);

    void expandRow(ROW row);

    ROW getSelectedRow();

    public Set<ROW> getExpandedRows();

    Element getSelectedRowElement();
//    com.google.gwt.user.client.Element getSelectedRowElement();

    void markAsCutRow(ROW row);

    void markAsUncutRow(ROW row);

    void markNodeAsLeafForSure(ROW row);

    void pasteRow(ROW row, ROW srcRow, boolean isCut);

    void removeRow(ROW row);

    void selectRow(ROW row, boolean fireEvent);

    void setLazyRowLoader(ILazyRowLoader<ROW> lazyRowLoader);

    void updateRow(ROW row, IContinuation doUpdate);

    void addStyleName(String styleName);

    public int getRowChildCount(ROW row);

    public int getRowIndexInSiblings(ROW row);

    public void setRowIndexInSiblings(ROW row, int idx);

    public void sortChildRows(ROW row, Comparator<ROW> cmp);

    public ROW getRowAtIndexInSiblings(ROW parentRow, int idx);

    public boolean hasUnloadedChildren(ROW row);

    public boolean isRowExpanded(ROW row);

    public void addHeaderRow(Object... headerRowVals);
}
//---------
//boolean isLazyLoading();
//void onBrowserEvent(Event event);
//void replaceInVisualRows(ROW oldRow, ROW row);

