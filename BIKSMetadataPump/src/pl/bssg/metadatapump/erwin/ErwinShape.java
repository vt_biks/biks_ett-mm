/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.erwin;

import java.util.HashMap;

/**
 *
 * @author pgajda
 */
public class ErwinShape extends ErwinXMLElementString {

    public ErwinShape() {
        propertiesMap = new HashMap<String, String>() {
            {
                put("Long_Id", null);
                put("Area_Long_Id", null);
                put("Model_Object_Ref", null);
                put("Owner_Path", null);
                //specyficzne dla polozenia tabeli - START
                put("Anchor_Point", null);
                put("Fixed_Size_Point", null);
                //specyficzne dla polozenia tabeli - END
                //specyficzne dla linii laczacych - START
                put("Parent_Connector_Point", null);
                put("Child_Connector_Point", null);
                put("Child_Shape_Ref", null);
                put("Parent_Shape_Ref", null);
                //specyficzne dla linii laczacych - END
                
                
            }
        };
    }
}
