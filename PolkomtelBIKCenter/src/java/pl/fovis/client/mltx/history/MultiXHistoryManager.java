/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.mltx.history;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikEntryPoint;
import pl.fovis.client.menu.LoginLogoutHeader;

/**
 *
 * @author pku
 */
public class MultiXHistoryManager {

    private static final String SEPARATOR = "/";
    private static String id = null;
    private static MultiXHistoryToken mhToken;

    public static void init() {
        onHistoryTokenChange(History.getToken());
        bikEntryPoint.registerHistoryHandler(History.addValueChangeHandler(new ValueChangeHandler<String>() {
            @Override
            public void onValueChange(ValueChangeEvent event) {
                onHistoryTokenChange(event.getValue().toString());
                LoginLogoutHeader.refreshOtherLangAnchors();
            }
        }));
    }

    private static void onHistoryTokenChange(String token) {
        parseToken(token);
        BIKClientSingletons.getMainBodyRootPanel().clear();
        Widget w = mhToken.action.performAction(id);
        BIKClientSingletons.getMainBodyRootPanel().add(w);
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(w, BIKClientSingletons.ORIGINAL_MAIN_CONTAINER_HEIGHT);
    }

    private static void parseToken(String token) {
        if (token == null) {
            token = "";
        }
        String[] a = token.split(SEPARATOR, 2);
        if (a.length > 1) {
            id = a[1];
        } else {
            id = null;
        }
        mhToken = MultiXHistoryToken.getTokenByName(a[0]);
    }
}
