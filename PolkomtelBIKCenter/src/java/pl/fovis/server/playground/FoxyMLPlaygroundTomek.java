/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.playground;

import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author tflorczak
 */
// i18n-default: no
public class FoxyMLPlaygroundTomek extends FoxyMLPlaygroundBase {

    public static void main(String[] args) {
        startPlaying();
    }

    @Override
    protected void letsPlay() {
        dumpSql("owszem a" /* I18N:  */);
        dumpSql("tceles" /* I18N:  */);
    }
}
