﻿exec sp_check_version '1.8.5.15';
go

----#ExportImportTree
if not exists(select * from bik_node_action where code='ExportToPDF')
insert into bik_node_action (code) values('ExportToPDF');
go

if not exists(select * from bik_node_action where code='ExportToHTML')
insert into bik_node_action (code) values('ExportToHTML');
go


insert bik_node_action_in_custom_right_role (action_id,role_id)
select (select id from bik_node_action where code='ExportToHTML'),id from bik_custom_right_role where code!='RegularUser'
and  not exists  (select * from bik_node_action_in_custom_right_role where action_id in (select id from bik_node_action where code='ExportToHTML') and role_id in (
select id id from bik_custom_right_role where code!='RegularUser'))

insert bik_node_action_in_custom_right_role (action_id,role_id)
select (select id from bik_node_action where code='ExportToPDF'),id from bik_custom_right_role where code!='RegularUser'
and  not exists  (select * from bik_node_action_in_custom_right_role where action_id in (select id from bik_node_action where code='ExportToPDF') and role_id in (
select id id from bik_custom_right_role where code!='RegularUser'))

exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants

exec sp_update_version '1.8.5.15','1.8.5.16';
go

