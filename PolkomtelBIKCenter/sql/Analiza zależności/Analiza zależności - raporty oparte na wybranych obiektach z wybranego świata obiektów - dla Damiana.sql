IF OBJECT_ID('tempdb..#xxx') IS NOT NULL 
drop table #xxx
go

declare @repTreeId int = (select id from bik_tree where code = 'Reports')
declare @repWebiKindId int = (select dbo.fn_node_kind_id_by_code('Webi'))
declare @qryKindId int = (select dbo.fn_node_kind_id_by_code('ReportQuery'))
declare @objMeasureKindId int = (select dbo.fn_node_kind_id_by_code('Measure')) 
declare @objDimensionKindId int = (select dbo.fn_node_kind_id_by_code('Dimension')) 
declare @objDetailKindId int = (select dbo.fn_node_kind_id_by_code('Detail')) 
declare @univTreeId int = (select id from bik_tree where code = 'ObjectUniverses')
declare @univKindId int = (select dbo.fn_node_kind_id_by_code('Universe'))

select
		--		/*
		rep.name as raport_name,
		reped.cuid as rep_cuid,
		rep.obj_id as rep_si_id,
		qry.name as qry_name,
		qryText.sql_text as qry_sql_text,
		obj.name as obj_name,
		univ.name as univ_name,
		unived.cuid as univ_cuid,
		univ.obj_id as univ_si_id,
		cast('' as varchar(max)) as rep_parent_path, 
		rep.parent_node_id as rep_ancestor_id,
		cast('' as varchar(max)) as univ_parent_path, 
		univ.parent_node_id as univ_ancestor_id
 into #xxx
from
	-- raporty -> zapytania dla raport�w
		bik_node rep inner join bik_node qry on qry.parent_node_id = rep.id 
		-- dodatkowe info dla zapytania - tre�� SQL
		inner join bik_sapbo_query qryText on qry.id = qryText.node_id
		-- obiekty u�yte w zapytaniu na raporcie (miary, wymiary, detale)
		inner join bik_node objlinked on objlinked.parent_node_id = qry.id 
		-- idziemy do orygina�u dla miar, wymiar�w itp. -> czyli do obiekt�w ze �wiata obiekt�w
		inner join bik_node obj on objlinked.linked_node_id = obj.id 
		inner join bik_joined_objs qryjoins on qry.id = qryjoins.src_node_id
		-- ��czymy zapytanie ze �wiatem obiekt�w
		inner join bik_node univ on qryjoins.dst_node_id = univ.id		
		left join bik_sapbo_extradata reped on reped.node_id = rep.id
		left join bik_sapbo_extradata unived on unived.node_id = univ.id
where 
		rep.is_deleted = 0 and rep.linked_node_id is null and rep.tree_id = @repTreeId and rep.node_kind_id = @repWebiKindId
		and qry.is_deleted = 0 and qry.tree_id = @repTreeId and qry.node_kind_id = @qryKindId
		and objlinked.is_deleted = 0 and objlinked.tree_id = @repTreeId and objlinked.node_kind_id in (@objMeasureKindId, @objDimensionKindId, @objDetailKindId)
		and obj.is_deleted = 0 and obj.tree_id = @univTreeId and obj.linked_node_id is null and obj.node_kind_id in (@objMeasureKindId, @objDimensionKindId, @objDetailKindId)
		and univ.is_deleted = 0 and univ.linked_node_id is null and univ.tree_id = @univTreeId and univ.node_kind_id = @univKindId
		-- warunki na �wiat obiekt�w lub nazwy obiekt�w
		and 
		( --1 = 1 or
		  univ.name = 'SR_Technika_CELLMAN_CELLMAN_wer_250016'
		--or obj.name in ('Country', 'Resort', 'Service Line', 'Service', 'Sales revenue', 'State', 'City')
		)
go




while 1 = 1 begin
  declare @rc int = null

  update #xxx
  set rep_parent_path = bn.name + '\' + rep_parent_path, rep_ancestor_id = bn.parent_node_id
  from bik_node bn
  where bn.id = #xxx.rep_ancestor_id
  
  set @rc = @@rowcount
  
  if @rc = 0 break
end
go


while 1 = 1 begin
  declare @rc int = null

  update #xxx
  set univ_parent_path = bn.name + '\' + univ_parent_path, univ_ancestor_id = bn.parent_node_id
  from bik_node bn
  where bn.id = #xxx.univ_ancestor_id
  
  set @rc = @@rowcount
  
  if @rc = 0 break
end
go


select raport_name, rep_parent_path, rep_cuid, rep_si_id, qry_name, obj_name, univ_name, univ_parent_path, univ_cuid, univ_si_id, qry_sql_text from #xxx
