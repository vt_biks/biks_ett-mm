/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import java.util.Collection;
import pl.trzy0.foxy.commons.IFoxyTransactionalPerformer;
import simplelib.logging.ILameLogger.LameLogLevel;
import simplelib.logging.LameLoggerMsg;

/**
 *
 * @author wezyr
 */
public interface IFoxyBizLogic extends IFoxyTransactionalPerformer {

    public void logRequest(String clientIP, int clientPort, String method, String requestURI,
            String requestHeaders,
            String requestParams, String sessionId, int durationMillis, String errorMsg, String referer);

    //public void logMsg(LameLoggerMsg msg);

    public void logMsgs(Collection<LameLoggerMsg> msgs, LameLogLevel minLevel);

    public void destroy();
}
