/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.ArrayList;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.IMyBIKSPage.ReachableMyBIKSTabCode;
import pl.fovis.client.dialogs.HomePageUserTypeDesc;
import pl.fovis.client.dialogs.ViewSlideShowlInFullScreenDialog;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.HomePageBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;

/**
 *
 * @author mmastalerz
 */
public class MyBIKSHomePageTab extends MyBIKSTabBase {

    protected VerticalPanel mainVp = new VerticalPanel();
    protected HorizontalPanel searchHp = new HorizontalPanel();
    protected FlowPanel searchHpIn = new FlowPanel();
    protected VerticalPanel searchVp = new VerticalPanel();
    protected TextBox searchTb = new TextBox();
    protected FlexTable headerHPTab = new FlexTable();
    protected FlexTable footerHPTab = new FlexTable();
//    protected MyBIKSTutorialTab tutorialTab = new MyBIKSTutorialTab();
    protected ClickHandler tutorialTabCh;
    protected List<HomePageBean> hints = new ArrayList<HomePageBean>();
    protected Image searchImg = new Image("images/" /* I18N: no */ + I18n.searchHPBtn.get());

    public MyBIKSHomePageTab() {
        BIKClientSingletons.setupAdditionalSearchTextBoxEvents(searchTb, searchImg);
        searchImg.setStyleName("MyBIKHomePage-searchImg");
    }

    @Override
    public String getCaption() {
        return I18n.stronaStartowa.get() /* I18N:  */;
    }

    @Override
    public String getButtonStyle() {
        return "labelLink-home";
    }

    public HTML getWelcomeMsg() {
        if (BIKClientSingletons.isUserLoggedIn() && BIKClientSingletons.isDemoVersion()) {
            //narazie nie ma rozroznienia na admina -> brak tekstów dla admina 
            String userType = BIKClientSingletons.isEffectiveExpertLoggedInEx(BIKConstants.ACTION_WELCOME_MSG) ? I18n.ekspert.get() /* I18N:  */ : I18n.zwyklyUzytkownik.get() /* I18N:  */;
            HTML msg = new HTML("<b>" + userType + "</b>");
            ClickHandler msgDesc = new ClickHandler() {
                public void onClick(ClickEvent event) {
                    HomePageUserTypeDesc window = new HomePageUserTypeDesc();
                    window.buildAndShowDialog(BIKClientSingletons.getWelcomeMsgForUser());
                }
            };
            msg.addClickHandler(msgDesc);
            msg.setStyleName("MyBIKHomePage-welcomeMsg");
            return msg;
        } else {
            return new HTML("");
        }
    }

    protected HTML createOneSearchHint(final String text, String sep) {
        HTML htm = new HTML("<i>" + text + "</i>" + sep);
        ClickHandler hintSearch = new ClickHandler() {
            public void onClick(ClickEvent event) {
                searchTb.setText(text);
                BIKClientSingletons.getGlobalSearchTextBox().setText(searchTb.getText());
            }
        };
        htm.setStyleName("MyBIKHomePage-labelLinkSearch");
        htm.addClickHandler(hintSearch);
        return htm;
    }

    protected void createSearchHints() {
        BIKClientSingletons.getService().getSearchHints(new StandardAsyncCallback<List<String>>() {
            @Override
            public void onSuccess(List<String> result) {
                if (!BaseUtils.isCollectionEmpty(result)) {
                    HTML header = new HTML(I18n.przykladoweHasla.get() /* I18N:  */ + ":");
                    header.setStyleName("MyBIKHomePage-search");
                    searchHpIn.add(header);
                    if (!result.isEmpty()) {
                        for (int i = 0; i < result.size() - 1; i++) {
                            searchHpIn.add(createOneSearchHint(result.get(i), ", "));
                        }
                        searchHpIn.add(createOneSearchHint(result.get(result.size() - 1), ""));
                    }
                }
            }
        });

    }

    protected void createSearchHpIn() {
        searchHpIn.clear();
        searchHpIn.setStyleName("MyBIKHomePage-searchHp");
        createSearchHints();
        searchVp.add(searchHpIn);
    }

    protected void createSearchHp() {
        searchHp.clear();
        searchTb.setStyleName("MyBIKHomePage-searchTB");
        HorizontalPanel searchPanel = new HorizontalPanel();
        searchPanel.add(searchTb);
        searchPanel.setCellVerticalAlignment(searchTb, HasVerticalAlignment.ALIGN_MIDDLE);
        searchPanel.add(searchImg);
        searchHp.add(searchPanel);
    }

    protected Label createLabelLink(ClickHandler ch, String text, String styleName) {
        Label lbl = new Label(text);
        lbl.setStyleName(styleName);
        lbl.addClickHandler(ch);
        return lbl;
    }

    protected HorizontalPanel createHintBtn(String text, final int nodeId, final String treeCode) {
        HorizontalPanel hp = new HorizontalPanel();
        hp.add(BIKClientSingletons.createFollowButtonEx(treeCode, nodeId, true, "MyBIKHomePage-followBtn"));
        hp.add(createLabelLink(new ClickHandler() {
            public void onClick(ClickEvent event) {
                BIKClientSingletons.getTabSelector().submitNewTabSelection(treeCode, nodeId, true);
            }
        }, text, "MyBIKHomePage-labelLink"));
        return hp;
    }

    protected void addHomePageTabHeader() {
        headerHPTab.setStyleName("MyBIKHomePage");
        if (BIKClientSingletons.isFlashPlayerInstalledAndVersionIsValid()) {
            final String text = I18n.prezentaNarzedziBIKnowledgSystem.get() /* I18N:  */;
            ClickHandler ch = new ClickHandler() {
                public void onClick(ClickEvent event) {
                    new ViewSlideShowlInFullScreenDialog().buildAndShowDialog(text);
                }
            };
            headerHPTab.setWidget(0, 0, createLabelLink(ch, text, "MyBIKHomePage-slideshow"));
        }

        tutorialTabCh = BIKClientSingletons.createClickHandlerForShowMyBIKSTab(ReachableMyBIKSTabCode.Tutorial);

        if (tutorialTabCh != null) {
            headerHPTab.setWidget(1, 0, createLabelLink(tutorialTabCh, I18n.poznajBIKnowledgSystemZSamouczk.get() /* I18N:  */, "MyBIKHomePage-slideshow"));
        }
    }

    protected void addHomePageTabFooter() {
        footerHPTab.setStyleName("MyBIKHomePage");
        footerHPTab.setWidth("100%");
        HTML htm = new HTML("<b> " + I18n.przykladoweLinki.get() /* I18N:  */ + ": </b>");
        htm.setStyleName("MyBIKHomePage-links");
        footerHPTab.setWidget(0, 0, htm);
        BIKClientSingletons.getService().getHomePageHints(new StandardAsyncCallback<List<HomePageBean>>() {
            public void onSuccess(List<HomePageBean> result) {
                footerHPTab.setVisible(!BaseUtils.isCollectionEmpty(result));
//                hints.addAll(result);
                HomePageBean hpb;
                for (int i = 0; i < result.size(); i++) {
                    hpb = result.get(i);
                    // i+1, bo na 1 miejscu jest opis "Przykładowe linki"
                    footerHPTab.setWidget(i + 1, 0, createHintBtn(hpb.name, hpb.nodeId, hpb.treeCode));
                }
            }
        });

    }

    @Override
    public void show(final VerticalPanel vp) {
        vp.clear();
//        vp.add(new Button("Show progress info", new ClickHandler() {
//            public void onClick(ClickEvent event) {
//                final SimpleProgressInfoDialog spid =
//                        new SimpleProgressInfoDialog();
//                spid.buildAndShowDialog("aqq", "to zniknie za 5 sekund", true);
//                Timer t = new Timer() {
//                    @Override
//                    public void run() {
//                        spid.hideDialog();
//                    }
//                };
//                t.schedule(5000);
//            }
//        }));
        if (BIKClientSingletons.isBikUserLoggedIn()) {
            mainVp.clear();
            searchVp.clear();
            createSearchHp();
            searchVp.add(new HTML("<b>" + I18n.wpiszSzukanaFraze.get() /* I18N:  */ + "</b>"));
            searchHp.setVisible(true);
            searchVp.add(searchHp);

            searchVp.setStyleName("MyBIKHomePage-searchVp");
            createSearchHpIn();
            if (BIKClientSingletons.isShowTutorials()) {
                addHomePageTabHeader();
            }
            addHomePageTabFooter();
            mainVp.add(searchVp);
            mainVp.setWidth("100%");
            mainVp.setCellHorizontalAlignment(searchVp, HasHorizontalAlignment.ALIGN_CENTER);
            mainVp.add(headerHPTab);
            mainVp.add(footerHPTab);
            vp.add(mainVp);
        }
    }
}
