/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.jdbc;

import commonlib.LameUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.bssg.metadatapump.AdhocPartialBeanCollector;
import pl.bssg.metadatapump.GenericAttributeBean;
import pl.bssg.metadatapump.GenericNodeBean;
import pl.bssg.metadatapump.MetadataPumpBase;
import pl.bssg.metadatapump.PumpUtils;
import pl.bssg.metadatapump.common.ConnectionParametersJdbcBean;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.trzy0.foxy.serverlogic.db.ConnectionUtils;
import simplelib.BaseUtils;
import simplelib.CSVWriter;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author ctran
 */
public class JdbcPump extends MetadataPumpBase {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected Connection jdbcConn;
    protected List<ConnectionParametersJdbcBean> configs;

    @Override
    protected Pair<Integer, String> startPump() {
        try {
            configs = adhocDao.createBeansFromNamedQry("getJdbcConfigForInstanceName", ConnectionParametersJdbcBean.class, instanceName);
        } catch (Exception e) {
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "Invalid login for data loading! Error: " + e.getMessage());
        }
        int step = 0;
        for (ConnectionParametersJdbcBean config : configs) {
            pumpLogger.pumpLog("Connect to server: " + config.name, step * (80 / configs.size()));
            try {
                jdbcConn = ConnectionUtils.getConnectionByConnectionString(config.jdbcDriverName, config.jdbcUrl, config.user, config.password, null);
                adhocDao.execNamedCommand("deleteFromAmgNodeBase");

                if (BaseUtils.isStrEmptyOrWhiteSpace(config.folderPath)) {
                    pumpTree(config);
                    pumpNodeLinks(config);
                } else {
                    pumpFile(config);
                }
            } catch (Exception ex) {
                if (logger.isErrorEnabled()) {
                    logger.error("Error in JDBCPump for server: " + config.name + ", message: " + ex.getMessage(), ex);
                }
                return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "JDBC Pump error: " + ex.getMessage());
            } finally {
                if (jdbcConn != null) {
                    PumpUtils.closeConnection(jdbcConn);
                    jdbcConn = null;
                }
                step++;
            }
        }
        return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_DONE, PumpConstants.PUMP_DATA_LOAD_SUCCESS);
    }

    protected void pumpFile(ConnectionParametersJdbcBean config) throws SQLException, FileNotFoundException, IOException {
        File folder = new File(config.folderPath);
        if (!folder.isDirectory()) {
            throw new LameRuntimeException(config.folderPath + " nie jest ścieżką do katalogu");
        }
        List<Pair<String, String>> queries = parseAdhocSql(config.adhocSql);
        //Trzeba refaktoryzowac!!!
        for (Pair<String, String> p : queries) {
            String fileName = BaseUtils.ensureDirSepPostfix(config.folderPath) + p.v1 + ".csv";
            String txt = p.v2;
            Statement statement = jdbcConn.createStatement();
            ResultSet rs = statement.executeQuery(txt);
            ResultSetMetaData metaData = rs.getMetaData();
            int colCnt = metaData.getColumnCount();

            List<String> targetColNames = new ArrayList();

            for (int i = 1; i <= colCnt; i++) {
                String colName = metaData.getColumnName(i);
                targetColNames.add(colName);
            }

            List<Map<String, Object>> content = new ArrayList<Map<String, Object>>();
            while (rs.next()) {
                Map<String, Object> res = new LinkedHashMap<String, Object>(/*colCnt * 4 / 3 + 1*/
                        BaseUtils.calcHashMapCapacity(colCnt));

                for (int i = 1; i <= colCnt; i++) {
                    res.put(targetColNames.get(i - 1), rs.getObject(i));
                }
                content.add(res);
            }
            statement.close();

            CSVWriter writer = new CSVWriter(targetColNames, content);
            String s = writer.getText();
            String text = BaseUtils.isStrEmptyOrWhiteSpace(s) ? s : '\uFEFF' + s;
            FileOutputStream fo = new FileOutputStream(fileName);
            fo.write(text.getBytes(StandardCharsets.UTF_8));
            fo.close();
        }
    }

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_JDBC;
    }

    protected Set<String> checkAttributeAndOptCreateSet(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        int columnCount = metaData.getColumnCount();
        Set<String> attrColumns = new HashSet<String>();
        for (int i = 1; i <= columnCount; i++) {
            String columnName = metaData.getColumnName(i);
            if (!BaseUtils.safeEqualsStr(columnName, PumpConstants.GENERIC_NODE_OBJID, true) && !BaseUtils.safeEqualsStr(columnName, PumpConstants.GENERIC_NODE_PARENT_OBJID, true)
                    && !BaseUtils.safeEqualsStr(columnName, PumpConstants.GENERIC_NODE_NAME, true) && !BaseUtils.safeEqualsStr(columnName, PumpConstants.GENERIC_NODE_DESCR, true)
                    && !BaseUtils.safeEqualsStr(columnName, PumpConstants.GENERIC_NODE_NODE_KIND_CODE, true) && !BaseUtils.safeEqualsStr(columnName, PumpConstants.GENERIC_NODE_VISUAL_ORDER, true)) {
                attrColumns.add(columnName);
            }
        }
        return attrColumns;
    }

    protected void pumpTree(ConnectionParametersJdbcBean config) throws SQLException {
        AdhocPartialBeanCollector<GenericNodeBean> beanCollector = new AdhocPartialBeanCollector<GenericNodeBean>(PACKAGE_SIZE, adhocDao, "insertIntoGenericTable");
        AdhocPartialBeanCollector<GenericAttributeBean> beanAttrCollector = new AdhocPartialBeanCollector<GenericAttributeBean>(PACKAGE_SIZE, adhocDao, "insertIntoAttrGenericTable");

        Statement statement = jdbcConn.createStatement();
        ResultSet rs = statement.executeQuery(config.query);
        boolean wasAttributeChecking = false;
        Set<String> attributeSet = new HashSet<String>();
        while (rs.next()) {
            String objId = BaseUtils.ensureObjIdEndWith(rs.getString(PumpConstants.GENERIC_NODE_OBJID), "|");
            String parentObjId = BaseUtils.ensureObjIdEndWith(rs.getString(PumpConstants.GENERIC_NODE_PARENT_OBJID), "|");
            if (!wasAttributeChecking) {
                attributeSet.addAll(checkAttributeAndOptCreateSet(rs));
                wasAttributeChecking = true;
            }
            beanCollector.add(new GenericNodeBean(objId, parentObjId, rs.getString(PumpConstants.GENERIC_NODE_NODE_KIND_CODE), rs.getString(PumpConstants.GENERIC_NODE_NAME), rs.getString(PumpConstants.GENERIC_NODE_DESCR), BaseUtils.tryParseInt(rs.getString(PumpConstants.GENERIC_NODE_VISUAL_ORDER))));
            // get attributes
            for (String attr : attributeSet) {
                String attrVal = rs.getString(attr);
                if (!BaseUtils.isStrEmptyOrWhiteSpace(attrVal)) {
                    beanAttrCollector.add(new GenericAttributeBean(objId, attr, attrVal));
                }
            }
        }
        beanCollector.flush();
        beanAttrCollector.flush();
        adhocDao.execNamedCommand("insertGenericNodesIntoBikNode", config.treeCode); // init branch names
        adhocDao.execNamedCommand("insertGenericAttributes", config.treeCode, "JDBC");
        adhocDao.execNamedCommand("updateNodeKindsForTree", config.treeCode);
        statement.close();
    }

    protected void pumpNodeLinks(ConnectionParametersJdbcBean config) throws SQLException {
        Statement statement = jdbcConn.createStatement();
        ResultSet rs = statement.executeQuery(config.queryJoined);
        List<Pair<String, String>> links = new ArrayList<Pair<String, String>>();
        while (rs.next()) {
            links.add(new Pair<String, String>(rs.getString(PumpConstants.GENERIC_NODE_SRC_ID), rs.getString(PumpConstants.GENERIC_NODE_DST_ID)));
        }
        adhocDao.execNamedCommand("insertNodeLinks", config.treeCode, links);
        statement.close();
    }

    private List<Pair<String, String>> parseAdhocSql(String adhocSql) {
        List<Pair<String, String>> res = new ArrayList();
        int i = 0;
        do {
            i = adhocSql.indexOf("<f:set var=");
            if (i >= 0) {
                int j = adhocSql.indexOf("\">", i);
                if (j >= 0) {
                    String fileName = adhocSql.substring(i + 12, j);
                    int k = adhocSql.indexOf("</f:set>");
                    if (k >= 0) {
                        String txt = adhocSql.substring(j + 2, k - 1);
                        res.add(new Pair<String, String>(fileName.trim(), txt.trim()));
                    } else {
                        throw new LameRuntimeException("Nie poprawne zapytania");
                    }
                    adhocSql = adhocSql.substring(k + 8);
                } else {
                    throw new LameRuntimeException("Nie poprawne zapytania");
                }
            }
        } while (i >= 0);
        return res;
    }
}
