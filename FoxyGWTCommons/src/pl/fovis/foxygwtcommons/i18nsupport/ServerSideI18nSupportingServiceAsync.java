/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.i18nsupport;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.Set;

/**
 *
 * @author pmielanczuk
 */
//ww: tak nie zadziała, zresztą to powinno być odwołanie do
// FoxyClientSingletons.FILE_HANDLING_SERVLET_NAME które nie jest stałą
// zamiast tego w GWTCurrentLocaleNameProvider jest wywołanie
// setServiceEntryPoint(FoxyClientSingletons.FILE_HANDLING_SERVLET_NAME)
//@RemoteServiceRelativePath("fsb")
public interface ServerSideI18nSupportingServiceAsync {

    public void setSessionLocaleName(String localeName, Set<String> clientSupportedLocaleNames, AsyncCallback<LocaleNegotiationResultBean> callback);

    public void getServerSupportedLocaleNames(AsyncCallback<Set<String>> callback);
}
