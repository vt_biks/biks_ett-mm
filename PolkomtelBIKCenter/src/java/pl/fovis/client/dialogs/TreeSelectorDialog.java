/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.PushButton;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.AbstractBIKTreeWidget;
import pl.fovis.client.bikpages.BikTreeWidget;
import pl.fovis.client.treeandlist.IBean4TreeExtractor;
import pl.fovis.client.treeandlist.TreeBean4TreeExtractor;
import pl.fovis.client.treeandlist.TreeNodeBeanAsMapStorage;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.MenuExtender;
import pl.fovis.common.MenuNodeBean;
import pl.fovis.common.NodeKindBean;
import pl.fovis.common.TreeBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.Dialogs;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public abstract class TreeSelectorDialog extends AbstractSelectorDialog<TreeNodeBean, Integer> {

    protected boolean generateNodesForTreeCategories() {
        return true;
    }

    protected Set<String> getOptAllTreeCodesToShow() {
        return null;
    }

    @Override
    protected void optFixResults(List<TreeNodeBean> result) {
        //   resultRepo = result;
        //bikTree.isGetDataInProgress = false;
        //bikTree.buildUniTree(result);
//        Window.alert("A");
        List<TreeNodeBean> rootDataNodes = //bikTree.utb.getRootDataNodes();
                BIKCenterUtils.getRootNodes(result);

        Set<Pair<Integer, TreeNodeBean>> newRootNodeTreeIds = new HashSet<Pair<Integer, TreeNodeBean>>();
        for (TreeNodeBean tnb : rootDataNodes) {
            newRootNodeTreeIds.add(new Pair<Integer, TreeNodeBean>(tnb.treeId, tnb));
        }

        boolean generateNodesForTreeCategories = generateNodesForTreeCategories();

        Map<Integer, TreeNodeBean> newRootNodes = new HashMap<Integer, TreeNodeBean>();
        List<TreeNodeBean> newRootNodesAsList = new ArrayList<TreeNodeBean>();

        Map<Integer, TreeNodeBean> parentNewRootNodes = new HashMap<Integer, TreeNodeBean>();

//        List<Pair<Integer, TreeNodeBean>> newRootNodeTreeIdsAsList = new ArrayList<Pair<Integer, TreeNodeBean>>(newRootNodeTreeIds);
        for (Pair<Integer, TreeNodeBean> p : newRootNodeTreeIds) {
            if (newRootNodes.containsKey(p.v1)) {
                continue;
            }

            if (generateNodesForTreeCategories) {
                // Parent Tree category
                if (p.v2.parentTreeId != null && !parentNewRootNodes.containsKey(p.v2.parentTreeId)) {
                    TreeNodeBean parentNewRoot = new TreeNodeBean();
                    parentNewRoot.id = -p.v2.parentTreeId;
                    parentNewRoot.name = p.v2.parentTreeName;
                    parentNewRoot.nodeKindCode = I18n.kategoria.get() /* I18N:  *//*"Category"*/;
                    parentNewRoot.nodeKindCaption = I18n.kategoria.get() /* I18N:  */;
                    parentNewRoot.nodeKindId = -1;
                    parentNewRoot.parentNodeId = null;
                    parentNewRoot.branchIds = -p.v2.parentTreeId + "|";
                    parentNewRoot.treeId = -1;
                    parentNewRoot.treeCode = I18n.kategoria.get();
                    parentNewRoot.treeName = I18n.kategoria.get();
                    parentNewRootNodes.put(p.v2.parentTreeId, parentNewRoot);

                    if (alreadyJoinedManager != null && ancestorIdsOfJoinedObjs.contains(p.v2.id)) {
                        ancestorIdsOfJoinedObjs.add(parentNewRoot.id);
                    }
                }
            }
            // Tree category
            TreeNodeBean newRoot = new TreeNodeBean();
            newRoot.id = -p.v2.id; //newRootId--;
            newRoot.name = p.v2.treeName;
            //mm-> poprawka na szybko, żeby było po polsku - do poprawienia, jak będzie koncepcja
            newRoot.nodeKindCode = I18n.kategoria.get() /* I18N:  *//*"Category"*/;
            newRoot.nodeKindCaption = I18n.kategoria.get() /* I18N:  */;
            newRoot.nodeKindId = -1;
            newRoot.parentNodeId = p.v2.parentTreeId == null ? null : -p.v2.parentTreeId;
            newRoot.branchIds = p.v2.parentTreeId == null ? (-p.v2.treeId) + "|" : ((-p.v2.parentTreeId) + "|" + (-p.v2.id) + "|");
            newRoot.treeId = -p.v2.treeId; //-1;
            newRoot.treeCode = p.v2.treeCode;
            newRoot.treeName = p.v2.treeName;
            newRootNodes.put(p.v1, newRoot);
            newRootNodesAsList.add(newRoot);
//            result.add(newRoot);

            if (BIKClientSingletons.expandSelectedNodesInHyperlinkAttribute() && treeIdWhenParentIdIsNull.contains(newRoot.treeId)) {
                parentsDirectOverrideOrIdWhenParentNull.add(newRoot.id);
            }
            if (alreadyJoinedManager != null && ancestorIdsOfJoinedObjs.contains(p.v2.id)) {
                ancestorIdsOfJoinedObjs.add(newRoot.id);
            }
        }
        if (parentNewRootNodes.size() > 1) {
            for (Map.Entry<Integer, TreeNodeBean> entry : parentNewRootNodes.entrySet()) {
                result.add(entry.getValue());
            }
        }

        Set<String> optAllTreeCodesToShow = getOptAllTreeCodesToShow();

        if (!generateNodesForTreeCategories && optAllTreeCodesToShow != null) {

            int minId = 0;

            for (TreeNodeBean tnb : newRootNodesAsList) {
                int id = tnb.id;
                if (id < minId) {
                    minId = id;
                }
            }

            minId--;

            Collection<TreeBean> allTrees = BIKClientSingletons.getAllTrees();

            for (TreeBean tb : allTrees) {
                if (newRootNodes.containsKey(tb.id) || !optAllTreeCodesToShow.contains(tb.code)) {
                    continue;
                }

                //ww: c&p z kodu powyżej, ale taka logika pokręcona,
                // że nie mam teraz czasu i odwagi (z braku czasu) tego prostować
                // Tree category
                TreeNodeBean newRoot = new TreeNodeBean();
                newRoot.id = minId;
                newRoot.name = tb.name;
                //mm-> poprawka na szybko, żeby było po polsku - do poprawienia, jak będzie koncepcja
                newRoot.nodeKindCode = I18n.kategoria.get() /* I18N:  *//*"Category"*/;
                newRoot.nodeKindCaption = I18n.kategoria.get() /* I18N:  */;
                newRoot.nodeKindId = -1;
                newRoot.parentNodeId = null;
                newRoot.branchIds = minId + "|";
                newRoot.treeId = -tb.id;
                newRoot.treeCode = tb.code;
                newRoot.treeName = tb.name;
                newRoot.hasNoChildren = true;

                newRootNodes.put(tb.id, newRoot);
                newRootNodesAsList.add(newRoot);
//            result.add(newRoot);

                minId--;
            }
        }

        if (newRootNodes.size() > 1) {
            for (TreeNodeBean tnb : rootDataNodes) {
                tnb.parentNodeId = newRootNodes.get(tnb.treeId).id;
            }

            Collections.sort(newRootNodesAsList, new Comparator<TreeNodeBean>() {

                @Override
                public int compare(TreeNodeBean o1, TreeNodeBean o2) {
                    if (o1 == null || o2 == null
                            || o1.name == null || o2.name == null) {
                        return 0;
                    }

                    return o1.name.compareTo(o2.name);
                }
            });

            result.addAll(newRootNodesAsList);

//            for (Map.Entry<Integer, TreeNodeBean> entry : newRootNodes.entrySet()) {
//                result.add(entry.getValue());
//            }
            for (TreeNodeBean tnb : result) {
                if (tnb.treeId > 0) { // normalne obiekty, bez sztucznych
                    tnb.branchIds = (tnb.parentTreeId == null ? (-tnb.treeId) + "|" : (-tnb.parentTreeId) + "|" + ((-tnb.treeId) + "|")) + tnb.branchIds;
                }
//                tnb.branchIds = -tnb.treeId + "|" + (tnb.branchIds == null ? "" : tnb.branchIds);
            }
        }
    }

    @Override
    public void buildAndShowDialog(Integer nodeId, IParametrizedContinuation<TreeNodeBean> saveCont,
            IJoinedObjSelectionManager<Integer> alreadyJoinedManager) {
        super.buildAndShowDialog(nodeId, saveCont, alreadyJoinedManager);
    }

    @Override
    public void optGetNodePathAndCenter(final IContinuation con) {
        if (nodeId != null) {
            getService().getAncestorNodePathByNodeId(nodeId, new StandardAsyncCallback<List<String>>() {
                public void onSuccess(List<String> result) {
                    headLbl.setText(getHeadLblPrefixMsg() + BaseUtils.mergeWithSepEx(result, BIKConstants.RAQUO_STR_SPACED));
                    con.doIt();
                }
            });
        } else {
            super.optGetNodePathAndCenter(con);
        }
    }

    protected void optFixBeanPropsAfterLoad(TreeNodeBean tnb) {
        //no-op
    }

    @Override
    protected boolean isNodeSelectable(TreeNodeBean node) {
        return node.id >= 0 && node.hasProperGrantForNodeFilteringAction;
    }

    @Override
    protected Integer getCurrentNodeIdAsInteger() {
        TreeNodeBean tnb = bikTree.getCurrentNodeBean();

        return (tnb != null && tnb.id >= 0) ? tnb.id : null;
    }

    @Override
    protected void fetchCurrentFilterInSubTreeNamePathAndUpdateFilterButtonTitle() {

        final String oldFilterVal = currentFilterVal;
        final Integer oldFilterNodeId = currentFilterInSubTreeNodeId;

        getService().getAncestorNodePathByNodeId(currentFilterInSubTreeNodeId, new StandardAsyncCallback<List<String>>() {

            @Override
            public void onSuccess(List<String> result) {

                if (!BaseUtils.safeEquals(currentFilterVal, oldFilterVal)
                        || !BaseUtils.safeEquals(currentFilterInSubTreeNodeId, oldFilterNodeId)) {
                    return;
                }

                filterButton.setTitle("Filtrowanie [" + currentFilterVal + "] w poddrzewie węzła: " + BaseUtils.mergeWithSepEx(result, BIKConstants.RAQUO_STR_SPACED));
            }
        });
    }

    @Override
    protected Collection<Integer> splitBranchIdsIntoColl(String branchIds, boolean withOutSelf, Collection<Integer> coll) {
        return BIKCenterUtils.splitBranchIdsIntoColl(branchIds, withOutSelf, coll);
    }

    protected Integer getNodeKindId() {
        return null;
    }

    protected Integer getTreeId() {
        return null;
    }

    protected String getRelationType() {
        return null;
    }

    protected String getAttributeName() {
        return null;
    }

    @Override
    protected AbstractBIKTreeWidget<TreeNodeBean, Integer> getTreeWidget() {

        return new BikTreeWidget(mustHideLinkedNodes(),
                dataFetchBroker,
                calcHasNoChildren(), getRelationType(), getAttributeName(), getNodeKindId(), getTreeId(),
                TreeNodeBeanAsMapStorage.NAME_FIELD_NAME + "=300:Nazwa",
                TreeNodeBeanAsMapStorage.PRETTY_KIND_FIELD_NAME + "=100:Typ") {
                    @Override
                    protected void optFixBeanPropsAfterLoad(TreeNodeBean tnb) {
                        TreeSelectorDialog.this.optFixBeanPropsAfterLoad(tnb);
                    }

                    @Override
                    protected void afterInitWithNoData() {
                        super.afterInitWithNoData();
                        treeHasNoData();
                    }
                };
    }

    @Override
    public IBean4TreeExtractor<TreeNodeBean, Integer> createBeanExtractor() {
        return new TreeBean4TreeExtractor();
    }

    @Override
    protected void doAction() {
        if (isInMultiSelectMode()) {
            BIKClientSingletons.showInfo(I18n.zapisywaniePowiazanProszeCzekac.get() /* I18N:  */ + "...");
            getService().updateJoinedObjsForNode(nodeId, mustHideLinkedNodes(),
                    directOverride, subNodesOverride, new StandardAsyncCallback<Void>() {
                        public void onSuccess(Void result) {
                            BIKClientSingletons.showInfo(I18n.powiazaniaZapisaneOdswiezanie.get() /* I18N:  */);
                            alreadyJoinedManager.refreshJoinedNodes(nodeId);
                        }
                    });
        } else {
            saveCont.doIt(selectedNode);
        }
    }

    @Override
    protected void groupResultsByMenu(List<TreeNodeBean> result) {
        List<MenuNodeBean> menuItems = extendMenuNodes();

        Map<Integer, TreeNodeBean> menuItemsWithoutTrees = new HashMap<Integer, TreeNodeBean>();

        List<TreeNodeBean> newRootNodesAsList = new ArrayList<TreeNodeBean>();
        Map<Integer, TreeNodeBean> newTreeRootNodes = new HashMap<Integer, TreeNodeBean>();
        Collection<Integer> treeIdsForFiltering = getTreeIdsForFiltering();
        for (MenuNodeBean mnb : menuItems) {

            if (mnb.nodeKindId == null && (mnb.treeId == null || treeIdsForFiltering.contains(mnb.treeId))) {

                TreeNodeBean newRoot = new TreeNodeBean();
                newRoot.id = -mnb.id; //newRootId--;
                newRoot.name = mnb.name;
                //mm-> poprawka na szybko, żeby było po polsku - do poprawienia, jak będzie koncepcja
                newRoot.nodeKindCode = I18n.kategoria.get() /* I18N:  *//*"Category"*/;
                newRoot.nodeKindCaption = I18n.kategoria.get() /* I18N:  */;
                newRoot.nodeKindId = -1;
                newRoot.parentNodeId = mnb.parentNodeId == null ? null : -mnb.parentNodeId;
                newRoot.branchIds = mnb.parentNodeId == null ? (-mnb.id) + "|" : ((-mnb.parentNodeId) + "|" + (-mnb.id) + "|");
                newRoot.treeId = mnb.treeId == null ? -1 : -mnb.treeId; //-1;
                newRoot.treeCode = mnb.code;
                newRoot.treeName = mnb.name;
                newRoot.visualOrder = menuItems.indexOf(mnb); // do sortowania
                if (mnb.treeId != null) {
                    newTreeRootNodes.put(mnb.treeId, newRoot);
                    newRootNodesAsList.add(newRoot);
                } else {
                    menuItemsWithoutTrees.put(newRoot.id, newRoot);
                }
            }
        }

        List<TreeNodeBean> rootDataNodes
                = BIKCenterUtils.getRootNodes(result);
        if (newTreeRootNodes.size() > 1) {

            for (Map.Entry<Integer, TreeNodeBean> tnb : newTreeRootNodes.entrySet()) {
                getAndAddAncestors(tnb.getValue(), newRootNodesAsList, menuItemsWithoutTrees);
            }

            Collections.sort(newRootNodesAsList, new Comparator<TreeNodeBean>() {

                @Override
                public int compare(TreeNodeBean o1, TreeNodeBean o2) {
                    if (o1.visualOrder > o2.visualOrder) {
                        return 1;
                    }
                    if (o1.visualOrder < o2.visualOrder) {
                        return -1;
                    }
                    return 0;
                }
            });
            result.addAll(newRootNodesAsList);

            for (TreeNodeBean tnb : rootDataNodes) {
                tnb.parentNodeId = newTreeRootNodes.get(tnb.treeId) != null ? newTreeRootNodes.get(tnb.treeId).id : null;
            }

            for (TreeNodeBean tnb : result) {
                if (tnb.treeId > 0) { // normalne obiekty, bez sztucznych
                    tnb.branchIds = (tnb.parentTreeId == null ? (-tnb.treeId) + "|" : (-tnb.parentTreeId) + "|" + ((-tnb.treeId) + "|")) + tnb.branchIds;
                }
            }
        }
    }

    protected void getAndAddAncestors(TreeNodeBean tnb, List<TreeNodeBean> newRootNodesAsList, Map<Integer, TreeNodeBean> menuItemsWithoutTrees) {
        TreeNodeBean newTnb = menuItemsWithoutTrees.get(tnb.parentNodeId);
        if (newTnb != null && !newRootNodesAsList.contains(newTnb)) {
            newRootNodesAsList.add(newTnb);
            getAndAddAncestors(newTnb, newRootNodesAsList, menuItemsWithoutTrees);
        }
    }

    protected List<MenuNodeBean> extendMenuNodes() {
        return BIKClientSingletons.getExtendedMenuNodes(
                BIKClientSingletons.getMenuNodes(), BIKClientSingletons.getTrees(), null/* BIKClientSingletons.getNodeKindsByCodeMap()*/,
                true, MenuExtender.MenuBottomLevel.NodeTrees,
                false, false);
    }

    boolean isAddNodeInToTheSameTree; //zzy dodany został nowy node na hyperlinku do tego samego drzewa

    protected PushButton addNodeBtn() {
        PushButton addNodeBtn = NewLookUtils.newCustomPushButton(I18n.dodaj.get() /* I18N:  */, new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        final TreeNodeBean tnb = bikTree.getCurrentNodeBean();
//                        Window.alert(tnb.nodeKindCode+" "+tnb.nodeKindCaption+" x:"+tnb.objId);

                        if (BIKConstants.TREE_KIND_META_BIKS.equalsIgnoreCase(tnb.treeKind)) {
                            NodeKindBean nk = null ;//= new NodeKindBean();
                            if (BIKConstants.NODE_KIND_META_DEFINITION_GROUP.equalsIgnoreCase(tnb.nodeKindCode)
                            && BaseUtils.ensureObjIdEndWith(BIKConstants.METABIKS_OBJ_ID_NODE_KIND_DEF, "|").equalsIgnoreCase(tnb.objId)) {
                                nk = BIKClientSingletons.getNodeKindBeanByCode(BIKConstants.NODE_KIND_META_NODE_KIND);
                            } else if (BIKConstants.NODE_KIND_META_ATTRIBUTES_CATALOG.equalsIgnoreCase(tnb.nodeKindCode)) {
                                nk = BIKClientSingletons.getNodeKindBeanByCode(BIKConstants.NODE_KIND_META_ATTRIBUTE_TYPE);
                            } else {
                                Dialogs.alertHtml(I18n.brakTypowWezlow.get(), I18n.addAnyNode.get(), null);

                            }
                            if (nk != null) {
                                int nodeKindId = nk.id;
                                String nodeKindCode = nk.code;
                                addNodeInMetaBiksTree(nodeKindId, nodeKindCode, tnb, tnb.treeId, false);
                            }
                        } else {
                            addNode(tnb);
                        }
                    }
                });
        return addNodeBtn;
    }

    protected Pair<String, String> getInnerJoinRelationType(Integer nodeKindId) {
        return null;
    }

    public void addNode(final TreeNodeBean tnb) {
        Pair<String, String> relationTypes = getInnerJoinRelationType(tnb.nodeKindId);
        final int treeIdInner = Math.abs(tnb.treeId);

        BIKClientSingletons.getService().getInnerJoinKindsForLinkNodeKindAndHyperlink(getNodeKindId(),
                tnb.nodeKindId, getTreeId(), treeIdInner, relationTypes, getAttributeName(), new StandardAsyncCallback<List<TreeNodeBean>>() {

                    @Override
                    public void onSuccess(List<TreeNodeBean> result) {

                        if (result.isEmpty()) {
                            Dialogs.alertHtml(I18n.brakTypowWezlow.get(), I18n.addAnyNode.get(), null);
                        } else {
                            new AddAnyNodeForLinkKindDialog().buildAndShowDialog(
                                    tnb.nodeKindId, tnb.treeId, true,
                                    result, tnb, new IParametrizedContinuation<Pair<TreeNodeBean, Map<String, AttributeBean>>>() {

                                        @Override
                                        public void doIt(Pair<TreeNodeBean, Map<String, AttributeBean>> p) {

                                            final TreeNodeBean param = p.v1;

                                            if (!isAddNodeInToTheSameTree) {
                                                isAddNodeInToTheSameTree = (getTreeId() == treeIdInner);
                                            }

                                            BIKClientSingletons.getService().createTreeNodeWithAttr(tnb.id > 0 ? tnb.id : null /*param.parentNodeId*/,
                                                    param.nodeKindId, param.name, param.objId, treeIdInner, param.linkedNodeId, null,
                                                    p.v2,
                                                    new StandardAsyncCallback<Pair<Integer, List<String>>>("Failure on" /* I18N: no */ + " createTreeNode") {
                                                        @Override
                                                        public void onSuccess(Pair<Integer, List<String>> result) {
                                                            fetchData(result.v1);

                                                        }
                                                    });
                                        }
                                    });
                        }
                    }
                });
    }

    public void addNodeInMetaBiksTree(final int nodeKindId, final String nodeKindCode, final TreeNodeBean patentTnb, final int treeId, final boolean getNodeKindFromParent2) {

        BIKClientSingletons.getService().getRequiredAttrs(nodeKindId, new StandardAsyncCallback<List<AttributeBean>>() {
            @Override
            public void onSuccess(List<AttributeBean> result) {
                showDialog(result, nodeKindId, nodeKindCode, patentTnb, treeId, getNodeKindFromParent2);
            }

        });
    }

    public void showDialog(List<AttributeBean> result, int nodeKindId, String nodeKindCode, TreeNodeBean patentTnb, int treeId, boolean getNodeKindFromParent2) {
        new TreeNodeDialog().buildAndShowDialog(nodeKindId, nodeKindCode, null,
                patentTnb, treeId, null, getNodeKindFromParent2, result, new IParametrizedContinuation<Pair<TreeNodeBean, Map<String, AttributeBean>>>() {
                    @Override
                    public void doIt(final Pair<TreeNodeBean, Map<String, AttributeBean>> param2) {
                        TreeNodeBean param = param2.v1;
                        BIKClientSingletons.getService().createTreeNodeWithAttr(param.parentNodeId,
                                param.nodeKindId, param.name, param.objId, param.treeId, param.linkedNodeId, param.addingJoinedObjs,
                                param2.v2,
                                new StandardAsyncCallback<Pair<Integer, List<String>>>("Failure on" /* I18N: no */ + " createTreeNode") {
                                    @Override
                                    public void onSuccess(Pair<Integer, List<String>> result) {
                                        BIKClientSingletons.showInfo(I18n.dodanoNowyElement.get() /* I18N:  */);
                                        fetchData(result.v1);
//                                        refreshRedisplayDataAndSelectRecord(result.v1);
                                    }
                                });
                    }
                });
    }

}
