/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.ViewType;
import pl.fovis.client.dialogs.IJoinedObjSelectionManager;
import pl.fovis.common.JoinedObjBasicInfoBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import simplelib.BaseUtils;
import simplelib.IContinuation;

/**
 *
 * @author beata
 */
public abstract class EntityDetailsTabPaginationBase<T>
        extends EntityDetailsTabWithItemsInEditableGridBase<T>
        implements IJoinedObjSelectionManager<Integer> {

//    protected Set<Integer> alreadySentNodeIdsForNames = new HashSet<Integer>();
    protected static final int PACK_SIZE = 10;
    RadioButton classicVewRb, differentViewRb, attributesViewRb;
//    protected Map<String, Widget> postponedAncestorPaths = new HashMap<String,Widget>();
    protected VerticalPanel gridWithPagination = new VerticalPanel() {
        {
            setWidth("100%");
        }
    };
    protected IGridNodeAwareBeanPropsExtractor<T> extractor;
    protected HorizontalPanel paginationHeaderHp = new HorizontalPanel() {
        {
            setStyleName("JoinedObj-pagination");

            setWidth("100%");
        }
    };
    protected int currPageNum;
    protected List<T> displayedItems;

    protected VerticalPanel navigateToPageAndBuild(List<T> filteredItems, int newPageNum) {
        gridWithPagination.clear();

        int itemsToShowCnt = filteredItems.size();

        if (newPageNum > 0 && newPageNum * PACK_SIZE >= itemsToShowCnt) {
            newPageNum = (itemsToShowCnt - 1) / PACK_SIZE;
        }

        this.currPageNum = newPageNum;
        this.displayedItems = filteredItems;

        if (itemsToShowCnt > PACK_SIZE) {
            gridWithPagination.add(buildPaginationControls());
        }

        gridWithPagination.add(internalBuildGrid(filteredItems, newPageNum));
        return gridWithPagination;
    }

    protected void setPaginationControlsValues() {
        paginationHeaderHp.clear();
        final int pageCnt = (displayedItems.size() + PACK_SIZE - 1) / PACK_SIZE;

        int firstPosNum = currPageNum * PACK_SIZE;
        int lastPosNum = Math.min((currPageNum + 1) * PACK_SIZE - 1, displayedItems.size() - 1);

        paginationHeaderHp.add(new Label(
                I18n.stronaZPozOdDoWszystkich.format(currPageNum + 1, pageCnt, firstPosNum + 1, lastPosNum + 1, displayedItems.size()) //
        //                I18n.strona2.get() /* I18N:  */+ ": " + (currPageNum + 1) + " " +I18n.z_noSingleLetter.get() /* i18n:  */+ " " + pageCnt + ", " +I18n.pozycjeOd.get() /* i18n:  */+ " "
        //                + (firstPosNum + 1) + " " +I18n.do_.get() /* i18n:  */+ " " + (lastPosNum + 1) + " (" +I18n.wszystkich.get() /* I18N:  */+ ": " + displayedItems.size() + ")"
        ));

        FlowPanel btns = new FlowPanel();

        boolean prevEnabled = currPageNum > 0;
        if (true || prevEnabled) {
            BikCustomButton btnFirst = new BikCustomButton("|«", "labelLinkWWJoinedObjs", new IContinuation() {
                public void doIt() {
                    navigateToPageAndBuild(displayedItems, 0);
                }
            });
            btnFirst.setTitle(I18n.pierwszaStrona.get() /* I18N:  */);

            btns.add(btnFirst);
            btnFirst.setEnabled(prevEnabled);

            BikCustomButton btnPrev = new BikCustomButton("«", "labelLinkWWJoinedObjs", new IContinuation() {
                public void doIt() {
                    navigateToPageAndBuild(displayedItems, currPageNum - 1);
                }
            });
            btnPrev.setTitle(I18n.poprzedniaStrona.get() /* I18N:  */);

            btns.add(btnPrev);
            btnPrev.setEnabled(prevEnabled);
        }

        boolean nextEnabled = currPageNum < pageCnt - 1;

        if (true || nextEnabled) {
            BikCustomButton btnNext = new BikCustomButton("»", "labelLinkWWJoinedObjs", new IContinuation() {
                public void doIt() {
                    navigateToPageAndBuild(displayedItems, currPageNum + 1);
                }
            });
            btnNext.setTitle(I18n.nastepnaStrona.get() /* I18N:  */);
            btns.add(btnNext);
            btnNext.setEnabled(nextEnabled);
            BikCustomButton btnLast = new BikCustomButton("»|", "labelLinkWWJoinedObjs", new IContinuation() {
                public void doIt() {
                    navigateToPageAndBuild(displayedItems, pageCnt - 1);
                }
            });
            btnLast.setTitle(I18n.ostatniaStrona.get() /* I18N:  */);
            btns.add(btnLast);
            btnLast.setEnabled(nextEnabled);
        }

        paginationHeaderHp.add(btns);
        paginationHeaderHp.setCellHorizontalAlignment(btns, HasHorizontalAlignment.ALIGN_RIGHT);
    }

    protected HorizontalPanel buildPaginationControls() {
        if (displayedItems.size() <= PACK_SIZE) {
            return null;
        }

        setPaginationControlsValues();

        return paginationHeaderHp;
    }

    protected Widget internalBuildGrid(final List<T> filteredItems,
            final int pageNum) {
        NodeAwareBeanGridWidget<T> gw = new NodeAwareBeanGridWidget<T>(isWidgetRunFromDialog(), widgetsActive, getBeanExtractor(), cddb.nodeNamesById, getNodeName());

        gw.setDataChangedHandler(new IContinuation() {
            public void doIt() {
                dataChanged();
            }
        });

        if (BaseUtils.isCollectionEmpty(items)) {
            displayData();
        }
        return gw.buildGrid(items, filteredItems, pageNum, PACK_SIZE);
    }

    protected void dataChanged() {
        navigateToPageAndBuild(getItemsToShow(), currPageNum);
        refreshTabCaption();
        //setPaginationControlsValues(displayedItems, currPageNum);
    }

    protected List<T> getItemsToShow() {
        prepareItemFiltering();

        final List<T> filteredJOs = new ArrayList<T>();

        for (T jo : items) {
            if (shouldShowItem(jo)) {
                filteredJOs.add(jo);
            }
        }

        return filteredJOs;
    }

    protected Widget buildGrid() {
//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_joinedObjs)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_joinedObjs, "buildGrid: start, nodeId=" + getNodeId() + ", jobj.size=" + items.size());
//        }

        Widget res = navigateToPageAndBuild(getItemsToShow(), 0);

//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_joinedObjs)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_joinedObjs, "buildGrid: done");
//        }
        return res;
    }

    protected void prepareItemFiltering() {
        //no-op
    }

    protected boolean shouldShowItem(T e) {
        return true;
    }

    //ww: implementacja dla IJoinedObjSelectionManager
    public JoinedObjBasicInfoBean getInfoForJoinedNodeById(Integer dstNodeId) {
        return cddb.jobsByDstNodeId.get(dstNodeId);
    }

    //ww: implementacja dla IJoinedObjSelectionManager
    public void refreshJoinedNodes(Integer forNodeId) {
        if (nodeId == forNodeId) {
            refreshAndRedisplayData();
        }
    }

    //ww: implementacja dla IJoinedObjSelectionManager
    public Collection<Integer> getJoinedObjIds() {
        List<Integer> ids = new ArrayList<Integer>();
        for (T item : items) {
            if (extractor.getOptSimilarityScore(item) == null) {
                ids.add(extractor.getNodeId(item));
            }
        }
        return ids;
    }

    protected IJoinedObjSelectionManager<Integer> getAlreadyJoinedManager() {
        return this; //null;
    }
//  ---------------------------------------------------------
//  ---------------------------------------------------------
    protected VerticalPanel mainJoinedObjVl = new VerticalPanel();
    protected VerticalPanel mainVp = new VerticalPanel();
    private VerticalPanel vp = new VerticalPanel();
    private FlowPanel addBtnJoinedFp = new FlowPanel();
    private ScrollPanel scPanel = new ScrollPanel();
    protected boolean hasAddJoinedButton;

    @Override
    public void displayData() {
        extractor = getBeanExtractor();

        vp.clear();

        if (BaseUtils.isCollectionEmpty(items)) {
            vp.add(zeroMsg(textWhenNoItems()));
            vp.setWidth("100%");
            mainJoinedObjVl.setStyleName("Notes" /* i18n: no:css-class */ + "-mainNoteVp");
            mainVp.setStyleName("Joined" /* I18N: no */ + "-mainJoinedVp");
        } else {
            vp.add(buildGrid());
        }
        if (isAlternativeJoinedPanelViewEnabled()) {
            if (classicVewRb != null) {
                classicVewRb.setValue(BIKClientSingletons.getViewType() == ViewType.ClassicView);
            }
            if (differentViewRb != null) {
                differentViewRb.setValue(BIKClientSingletons.isDifferentViewType());
            }
            if (attributesViewRb != null) {
                attributesViewRb.setValue(BIKClientSingletons.getViewType() == ViewType.AttributesView);
            }
        }
    }

    @Override
    public Widget buildWidgets() {
        mainJoinedObjVl.clear();
        mainVp.clear();
        vp.clear();
        addBtnJoinedFp.clear();
        scPanel.clear();
        mainJoinedObjVl.setWidth("100%");
        vp.setWidth("100%");
        vp.addStyleName("obj" /* I18N: no */ + "-innerObjEntriesVp");
        mainJoinedObjVl.addStyleName("RelatedObj");

        addOptionalFilterWidgets();

        mainJoinedObjVl.add(vp);
        scPanel.add(mainJoinedObjVl);
        hasAddJoinedButton = hasAddJoinedButton() && isEditonEnabled();
//        scPanel.setHeight((BIKClientSingletons.ORIGINAL_EDP_BODY /*+ BIKClientSingletons.ORIGINAL_EDP_ADD_ASSOCIATION*/) + "px");
        scPanel.setHeight((hasAddJoinedButton ? BIKClientSingletons.ORIGINAL_EDP_BODY : BIKClientSingletons.ORIGINAL_EDP_BODY + BIKClientSingletons.ORIGINAL_EDP_ADD_ASSOCIATION) + "px");
        mainVp.add(scPanel);
        if (hasAddJoinedButton) {
            Set<String> treeKinds = new HashSet<String>();
            treeKinds.add(getTreeKindForSelected());
            Set<String> addsTreeKinds = getAdditionalsTreeKind();
            if (addsTreeKinds != null) {
                treeKinds.addAll(addsTreeKinds);
            }
            BikCustomButton btnAddJoinedObjs = createAddJoinedButton(treeKinds);
            if (btnAddJoinedObjs != null) {
                addBtnJoinedFp.add(btnAddJoinedObjs);
            }
            if (!BIKClientSingletons.isHideElementsForMM()) {
                BikCustomButton btnAddJoinedObjs2 = createAddJoinedButton2(treeKinds);
                if (btnAddJoinedObjs2 != null) {
                    addBtnJoinedFp.add(btnAddJoinedObjs2);
                }
            }
            if (isAlternativeJoinedPanelViewEnabled()) {
                addBtnJoinedFp.add(createChangedViewFp());
            }
            mainVp.add(addBtnJoinedFp);
        } else {
            if (isAlternativeJoinedPanelViewEnabled()) {
                mainVp.add(createChangedViewFp());
                scPanel.setHeight((BIKClientSingletons.ORIGINAL_EDP_BODY) + 8 + "px");
            }
        }
        mainVp.setWidth("100%");

        return mainVp;
    }

    protected boolean isAlternativeJoinedPanelViewEnabled() {
        return (isChangedViewDescriptionInJoinedPanelVisible() && BIKClientSingletons.isAlternativeJoinedPanelViewEnabled())
                || (isViewWithAttributeRbInJoinedPanelVisible() && BIKClientSingletons.enableAttributeJoinedObjs());
    }

    protected void addOptionalFilterWidgets() {
        //no-op
    }

    protected abstract IGridNodeAwareBeanPropsExtractor<T> getBeanExtractor();

    protected String textWhenNoItems() {
        return I18n.brakPowiazan.get() /* I18N:  */;
    }

//    protected abstract String getTreeKindForSelected();
    protected boolean hasAddJoinedButton() {
        return true;
    }

    @Override
    protected String getOptNodeActionCode() {
        return "AddJoinedObjs";
    }

    protected abstract BikCustomButton createAddJoinedButton(final Set<String> treeKinds);

    protected BikCustomButton createAddJoinedButton2(final Set<String> treeKinds) {
        return null;
    }

    protected boolean isChangedViewDescriptionInJoinedPanelVisible() {
        return false;
    }

    protected boolean isViewWithAttributeRbInJoinedPanelVisible() {
        return false;
    }

    protected FlowPanel createChangedViewFp() {
        FlowPanel fp = new FlowPanel();
        classicVewRb = createChangedViewRb(I18n.classicView.get(), false, "view_classic", ViewType.ClassicView);

        fp.add(classicVewRb);
        if (BIKClientSingletons.isAlternativeJoinedPanelViewEnabled()) {
            differentViewRb = createChangedViewRb(I18n.differentView.get(), true, "view_different", ViewType.DifferentView);
            fp.add(differentViewRb);
        }
        if (BIKClientSingletons.enableAttributeJoinedObjs() && isViewWithAttributeRbInJoinedPanelVisible()) {
            attributesViewRb = createChangedViewRb(I18n.attributeView.get(), true, "view_attributes", ViewType.AttributesView);
            fp.add(attributesViewRb);
            attributesViewRb.setValue(true);
            BIKClientSingletons.setViewType(ViewType.AttributesView);
        } else {
            classicVewRb.setValue(true);
            if (BIKClientSingletons.getViewType() == null) {
                BIKClientSingletons.setViewType(ViewType.ClassicView);
            }
        }
        return fp;
    }

    protected RadioButton createChangedViewRb(String caption, final boolean isDescrVisible, String style, final ViewType type) {

        RadioButton rb = new RadioButton(widgetsActive ? "rb1" : "rb2", "           ");
        rb.setHTML("<img style=\"cursor: pointer\" src=\"images/" + style + ".png" + "\"/>");
        rb.addStyleName("viewRb");
        rb.setTitle(caption);
        rb.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                BIKClientSingletons.setViewType(type);
                displayData();
            }
        });
        return rb;
    }
}
