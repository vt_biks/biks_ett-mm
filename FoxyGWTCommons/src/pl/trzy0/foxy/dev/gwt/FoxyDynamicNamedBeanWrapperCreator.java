/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.dev.gwt;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.ext.UnableToCompleteException;
import com.google.gwt.core.ext.typeinfo.JClassType;
import com.google.gwt.core.ext.typeinfo.JField;
import com.google.gwt.core.ext.typeinfo.JPackage;
import com.google.gwt.core.ext.typeinfo.JPrimitiveType;
import com.google.gwt.core.ext.typeinfo.JType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.trzy0.foxy.commons.annotations.CreatorPackage;
import pl.trzy0.foxy.commons.annotations.DisableDynamicClassCreation;
import simplelib.BaseUtils;
import simplelib.INamedPropGetter;
import simplelib.INamedPropGetterSetter;
import simplelib.INamedPropSetter;
import simplelib.INamedPropsBeanEx;
import simplelib.INamedTypedBeanPropsBeanWrapperCreator;
import simplelib.INamedTypedPropsBean;
import simplelib.INamedTypedPropsBeanBroker;
import simplelib.IWrapperCreator;
import simplelib.JsonValueHelper;
import simplelib.LameRuntimeException;
import simplelib.NamedPropsBeanGenericCodeGenerator;
import simplelib.NamedPropsBeanGenericCodeGenerator.IMainClass;
import simplelib.NamedPropsBeanGenericCodeGenerator.IPropClass;
import pl.fovis.foxygwtcommons.i18n.I18n;

/**
 *
 * @author wezyr
 */
// i18n-default: no
public class FoxyDynamicNamedBeanWrapperCreator extends FoxyGeneratorBase {

    protected JClassType optIntfParametrization;

    @Override
    protected void addImports() {
        super.addImports();
        addImport(LameRuntimeException.class);
        addImport(Map.class);
        addImport(Set.class);
        addImport(LinkedHashMap.class);
        addImport(INamedTypedBeanPropsBeanWrapperCreator.class);
        addImport(INamedPropsBeanEx.class);
        addImport(GWT.class);

        addImport(BaseUtils.class);
        addImport(INamedPropGetter.class);
        addImport(INamedPropGetterSetter.class);
        addImport(INamedPropSetter.class);
        addImport(INamedTypedPropsBean.class);
        addImport(JsonValueHelper.class);
        addImport(LameRuntimeException.class);
        addImport(IWrapperCreator.class);
        addImport(INamedTypedPropsBeanBroker.class);
    }

    @Override
    protected void generateClassBody() throws UnableToCompleteException {
        CreatorPackage cp = genClassType.getAnnotation(CreatorPackage.class);
        String relativePath;
        boolean includeSubPackages;
        if (cp == null) {
            //throwUnableToComplete("class \"" + genClassName + "\" must be annotated with CreatorPackage");
            relativePath = null;
            includeSubPackages = true;
        } else {
            relativePath = cp.value();
            includeSubPackages = cp.includeSubPackages();
        }

        String packagePath = getPackagePath(relativePath);
        log("package path for class" /* I18N: no */+ " \"" + genClassName + "\" " +"is" /* I18N: no */+ " \"" + packagePath + "\"");

        optIntfParametrization = getCreatorIntfParametrization();

        if (optIntfParametrization != null) {
            addImport(optIntfParametrization.getQualifiedSourceName());
        }

//        logWarn("class " + genClassName + " has parametrization: " + optIntfParametrization);

        final String parametrizedTypeName = optIntfParametrization == null ? "Object" /* I18N: no */ : optIntfParametrization.getSimpleSourceName();

//        String parametrizationDiamond = "<" + parametrizedTypeName + ">";

        String implsPackageName = BaseUtils.dropOptionalPrefix(packagePath.replace("/", "."), ".");
        //TypeOracle typeOracle = genClassType.getOracle();
        JPackage implsPackage = typeOracle.findPackage(implsPackageName);
        if (implsPackage == null) {
            throwUnableToComplete("cannot find package for implementations" /* I18N: no */+ ": \""
                    + implsPackageName + "\"");
        }

        List<JClassType> classesInPkg = new ArrayList<JClassType>();
        addClassesFromPackage(implsPackage, classesInPkg);

        if (includeSubPackages) {
            JPackage[] pkgs = typeOracle.getPackages();
            String implsSubPackagePrefix = implsPackageName + ".";
            for (JPackage aPkg : pkgs) {
                if (aPkg.getName().startsWith(implsSubPackagePrefix)) {
                    addClassesFromPackage(aPkg, classesInPkg);
                }
            }
        }

        log("found classes in package" /* I18N: no */+ " \"" + implsPackageName + "\": " + classesInPkg);

        for (JClassType type : classesInPkg) {
            addImport(type.getQualifiedSourceName());
        }

        for (JClassType type : classesInPkg) {
            addWrapperForBeanClass(type);
        }

//        for (JClassType type : classesInPkg) {
//            logWarn("will generate code for " + type.getQualifiedSourceName() + " with super types: " + type.getFlattenedSupertypeHierarchy());
//        }

//        String genSimpleClassName = BaseUtils.extractSimpleClassName(genClassName);
//        println("private static final " + genSimpleClassName + " impl = GWT.create(" + genSimpleClassName + ".class)");
//        println("public " + genSimpleClassName + " impl() { return impl; }");

        String creatorClass = "IWrapperCreator<? extends" /* I18N: no */+ " " + parametrizedTypeName + ", INamedTypedPropsBean>";
        String brokerClass = "INamedTypedPropsBeanBroker<? extends" /* I18N: no */+ " " + parametrizedTypeName + ">";
        String mapClassParams = "<" +"Class" /* I18N: no */+ ", " + creatorClass + ">";
        String mapClassParamsBroker = "<" +"Class" /* I18N: no */+ ", " + brokerClass + ">";

        println("private static final Map" /* I18N: no */ + mapClassParams + " " +"creators = new LinkedHashMap" /* I18N: no */
                + mapClassParams + "();");
        println("private static final Map" /* I18N: no */ + mapClassParamsBroker + " " +"brokers = new LinkedHashMap" /* I18N: no */
                + mapClassParamsBroker + "();");

        println("static" /* I18N: no */+ " {");

        println(creatorClass + " " +"creator" /* I18N: no */+ ";");
        println(brokerClass + " " +"broker" /* I18N: no */+ ";");
        for (JClassType type : classesInPkg) {
            String simpleName = type.getSimpleSourceName();
            String thisOneCreatorClass = "IWrapperCreator<" + simpleName + ", INamedTypedPropsBean>";
            println("creator = new" /* I18N: no */+ " " + thisOneCreatorClass + "() {" +""
                    + "public INamedTypedPropsBean create" /* I18N: no */+ "(" + simpleName + " " +"src) { return new" /* I18N: no */+ " " + simpleName
                    + "Wrapper(src" /* I18N: no */+ "); } };");
            println("creators.put" /* I18N: no */+ "(" + simpleName + "." +"class, creator" /* I18N: no */+ ");");

            println("broker = "
                    + "new INamedTypedPropsBeanBroker" /* I18N: no */+ "<" + simpleName + ">() {\n"
                    + "\n"
                    + "            @" +"Override\n"
                    + "            public Object getPropValue" /* I18N: no */+ "(" + simpleName + " " +"bean, String propName) {\n"
                    + "                return" /* I18N: no */+ " " + simpleName + "Wrapper.getPropValueStatic(bean, propName);\n"
                    + "            }\n"
                    + "\n"
                    + "            @Override\n"
                    + "            public void setPropValue" /* I18N: no */+ "(" + simpleName + " " +"bean, String propName, Object value" /* I18N: no */+ ") {\n"
                    + "                " + simpleName + "Wrapper.setPropValueStatic(bean, propName, value);\n"
                    + "            }\n"
                    + "\n"
                    + "            @Override\n"
                    + "            public Class getPropClass(String propName) {\n"
                    + "                return" /* I18N: no */+ " " + simpleName + "Wrapper.getPropClassStatic(propName);\n"
                    + "            }\n"
                    + "\n"
                    + "            @Override\n"
                    + "            public Set<String> getPropNames() {\n"
                    + "                return" /* I18N: no */+ " " + simpleName + "Wrapper.getPropNamesStatic();\n"
                    + "            }\n"
                    + "        };\n");
            println("brokers.put" /* I18N: no */+ "(" + simpleName + "." +"class, broker" /* I18N: no */+ ");\n");
        }
        println("}");

        println(""
                    + "public INamedTypedPropsBean create" /* I18N: no */+ "(" + parametrizedTypeName + " " +"src) { "
                + "  if (src == null) return null;"
                + "  Class cls = src.getClass();"
                + "IWrapperCreator" /* I18N: no */+ "<" + parametrizedTypeName + ", " +"INamedTypedPropsBean> creator = (IWrapperCreator)creators.get(cls);"
                + "if (creator == null" /* I18N: no */+ ") " + getThrowStmt("no creator for class" /* I18N: no */+ " ", "cls" /* I18N: no */) + "\n" +""
                + "return creator.create(src" /* I18N: no */+ "); }\n");
        println("public <BC extends" /* I18N: no */+ " " + parametrizedTypeName + "> " +"INamedTypedPropsBeanBroker<BC> getBroker(Class<BC> beanClass) {\n"
                + "INamedTypedPropsBeanBroker<BC> broker = (INamedTypedPropsBeanBroker)brokers.get(beanClass);\n"
                + "if (broker == null" /* I18N: no */+ ") " + getThrowStmt("no broker for class" /* I18N: no */+ " ", "beanClass") + "\n" +""
                + "return broker" /* I18N: no */+ "; }\n");
        //println("public Set<String> getNames() { return creators.keySet(); }");
    }

    protected JClassType getCreatorIntfParametrization() throws UnableToCompleteException {
        return getGenClassParametrizationOf(INamedTypedBeanPropsBeanWrapperCreator.class);
//        JGenericType intfType = getClassType(INamedTypedBeanPropsBeanWrapperCreator.class).isGenericType();
//        if (intfType == null) {
//            return null;
//        }
//
//        JParameterizedType parametrization = genClassType.asParameterizationOf(intfType);
//        if (parametrization == null) {
//            return null;
//        }
//
//        JClassType[] typeArgs = parametrization.getTypeArgs();
//
//        if (typeArgs.length != 1) {
//            throwUnableToComplete("parametrization for class \""
//                    + genClassName
//                    + "\" has " + typeArgs.length + " typeArgs, but should have just 1");
//        }
//
//        JClassType oneTypeArg = typeArgs[0];
//        JGenericType oneArgGenericType = oneTypeArg.isGenericType();
//        if (oneArgGenericType != null) {
//            throwUnableToComplete("type arg of dynamic class creator for class \""
//                    + genClassName
//                    + "\" is generic type \"" + oneTypeArg.getName() + "\"");
//        }
//
//        return oneTypeArg;
////        String classOfCreator = oneTypeArg.getParameterizedQualifiedSourceName();
////        log("dynamic class creator \"" + genClassName
////                + "\" creates subclasses of \"" + classOfCreator + "\"");
    }

    protected class PropClassByTypeOracle implements IPropClass {

        protected JType type;

        protected PropClassByTypeOracle(JType type) {
            this.type = type;
        }

        @SuppressWarnings("unchecked")
        protected boolean isAssignable(Class c, boolean toClassMode) {

            JPrimitiveType pt = type.isPrimitive();
//            logWarn("isAssignable: *** c=" + c + ", toClassMode=" + toClassMode + ", type=" + type);
            if (pt != null) {
                try {
                    Class ptClass = //Class.forName(pt.getQualifiedBoxedSourceName());
                            BaseUtils.findPrimitiveClassByName(pt.getSimpleSourceName());
//                    logWarn("isAssignable: ptClass=" + ptClass);
                    Class toClass = toClassMode ? c : ptClass;
                    Class fromClass = toClassMode ? ptClass : c;
                    boolean res = toClass.isAssignableFrom(fromClass);
//                    logWarn("isAssignable: ### fromClass=" + fromClass + ", toClass=" + toClass + ", res=" + res);
                    return res;
                } catch (Exception ex) {
                    throw new LameRuntimeException("cannot find class" /* I18N: no */+ " " + pt.getQualifiedBoxedSourceName(), ex);
                }
            } else {
//                logWarn("isAssignable: --- type=" + type + " is not primitive");
            }

            if (c.isPrimitive()) {
                c = BaseUtils.getEnvelopeForPrimitiveClass(c);
            }

            JClassType cT = getClassType(c);
            JClassType myCT = type.isClassOrInterface();
            if (myCT == null) {
                throw new LameRuntimeException("my type" /* I18N: no */+ ": " + BaseUtils.safeGetClassName(type) + " " +"is not class-type, name" /* I18N: no */+ "=" + type.getQualifiedSourceName());
            }

            JClassType toCT = toClassMode ? cT : myCT;
            JClassType fromCT = toClassMode ? myCT : cT;

            return toCT.isAssignableFrom(fromCT);
        }

        public boolean isAssignableTo(Class c) {
            return isAssignable(c, true);
        }

        public boolean isAssignableFrom(Class c) {
            return isAssignable(c, false);
        }

        public String getFullName() {
            return type.getQualifiedSourceName();
        }

        public boolean isPrimitiveType() {
            return type.isPrimitive() != null;
        }
    }

    protected class MainClassByTypeOracle implements IMainClass {

        protected JClassType type;
        protected Map<String, JField> allProperFields = new HashMap<String, JField>();

        protected MainClassByTypeOracle(JClassType type) {
            this.type = type;

            for (JField fld : getAllFields(type)) {
                if (fld.isPublic() && !fld.isStatic() && !fld.isFinal()) {
                    allProperFields.put(fld.getName(), fld);
                }
            }
        }

        public String getSimpleName() {
            return type.getSimpleSourceName();
        }

        public Iterable<String> getPropNames() {
            return allProperFields.keySet();
//            List<String> res = new ArrayList<String>();
//
//            for (JField fld : getAllFields(type)) {
//                if (fld.isPublic() && !fld.isStatic() && !fld.isFinal()) {
//                    res.add(fld.getName());
//                }
//            }
//
//            return res;
        }

        public IPropClass getPropClass(String name) {
            return new PropClassByTypeOracle(/*type.getField(name)*/
                    allProperFields.get(name).getType());
        }
    }

    private void addWrapperForBeanClass(JClassType type) {

        IMainClass mc = new MainClassByTypeOracle(type);
//        if (BaseUtils.iterableIsEmpty(mc.getPropNames())) {
//            return;
//        }

        println("static" /* I18N: no */+ " " + NamedPropsBeanGenericCodeGenerator.makeMethods(mc));
    }

    private void addClassesFromPackage(JPackage pkg, /*JClassType superClassType,*/
            Collection<JClassType> classes) {
        JClassType[] typesInPkg = pkg.getTypes();
        int cnt = 0;
        for (JClassType typeInPkg : typesInPkg) {
            DisableDynamicClassCreation ddcc = typeInPkg.getAnnotation(DisableDynamicClassCreation.class);
            if (ddcc == null && !typeInPkg.isAbstract()
                    && (optIntfParametrization == null || typeInPkg.isAssignableTo(optIntfParametrization))) {
//                if (typeInPkg.isDefaultInstantiable()) {

//                    String simpleName = typeInPkg.getSimpleSourceName();

//                    AlternativeSimpleName asn = typeInPkg.getAnnotation(AlternativeSimpleName.class);
//                    if (asn != null) {
//                        String altSimpleName = asn.value();
//                        if (asn.replacesDefaultName()) {
//                            simpleName = altSimpleName;
//                        } else {
//                            simpleName += "," + altSimpleName;
//                        }
//                    }

//                IMainClass mc = new MainClassByTypeOracle(typeInPkg);
                classes.add(typeInPkg);
                cnt++;
//                if (BaseUtils.iterableIsEmpty(mc.getPropNames())) {
//                    logWarn("class " + typeInPkg.getSimpleSourceName() + " has no public nonstatic nonfinal fields. to suppress this warning annotate class with @DisableDynamicClassCreation" /*+ ". superTypes: " + typeInPkg.getFlattenedSupertypeHierarchy()*/);
//                }

//                } else {
//                    logWarn("class " + typeInPkg.getSimpleSourceName() + " has no default constructor, cannot be instantiated dynamically. to suppress this warning annotate class with @DisableDynamicClassCreation");
//                }
            }
        }

        //com.google.gwt.user.client.rpc.impl.TypeHandler

        log("addClassesFromPackage(\"" + pkg.getName() + "\"): " +"added" /* I18N: no */+ " " + cnt
                + " " +"class(es) of total" /* I18N: no */+ " " + typesInPkg.length + " " +"class(es) in this package" /* I18N: no */);
    }
}