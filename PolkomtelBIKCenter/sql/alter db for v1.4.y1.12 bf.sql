﻿exec sp_check_version '1.4.y1.12';
go

update bik_node set descr_plain=null where node_kind_id=
(select id  from bik_node_kind where code='GlossaryNotCorpo' ) and is_deleted=0

update bik_metapedia set body='' where node_id in (
select bn.id from bik_node bn join bik_node_kind bnk on bn.node_kind_id=bnk.id where bnk.code='GlossaryNotCorpo' )

declare @treeId int
select @treeId=id from bik_tree where code='Glossary'

declare @filterTxt varchar(max) = 'n.tree_id = ' + cast(@treeId as varchar(20))
exec sp_verticalize_node_attrs @filterTxt
go

exec sp_update_version '1.4.y1.12', '1.4.y1.13';
go