select * from mltx_registered_user
--where 
;

-- update mltx_registered_user set first_name = 'Marketing', last_name = '' where id = 9146

/*


​​------ https://demo.biks.com.pl/?dbName=biks_pl_1KUWZB9G#

https://demo.biks.com.pl/?dbName=biks_pl_FQYD3DY5#


*/


select * from mltx_login_database
where login like '%a%wojci%'

select * from mltx_database where id = 7102




SELECT
  u.id, u.first_name, u.last_name, u.email, count(*) as created_db_cnt 
  --d.id as database_id, first_name, last_name, d.database_name, u.id as creator_id
FROM [MasterBIKS].[dbo].[mltx_registered_user] u inner join [MasterBIKS].[dbo].[mltx_database] d on u.id = d.creator_id
group by u.id, u.first_name, u.last_name, u.email
having count(*) > 1
order by created_db_cnt desc




-- powielone rejestracje baz
select
  d.id as database_id, u.id as creator_id, u.first_name, u.last_name, u.email, d.database_name
from [masterbiks].[dbo].[mltx_registered_user] u inner join [masterbiks].[dbo].[mltx_database] d on u.id = d.creator_id
where u.id in 
(
select
  u.id
from [masterbiks].[dbo].[mltx_registered_user] u inner join [masterbiks].[dbo].[mltx_database] d on u.id = d.creator_id
group by u.id, u.first_name, u.last_name, u.email
having count(*) > 1
)
order by u.email, d.database_name


-- klon zarejestrowanego użytkownika, przepięcie jednej bazy na klona
declare @orig_email varchar(max) = 'awojcik+it@bssg.pl'; 

declare @new_email varchar(max) = substring(@orig_email, 1, 1) + '.' + substring(@orig_email, 2, len(@orig_email));

insert into mltx_registered_user (first_name, last_name, email, phone_num, company, job_title, password)
select --*,
  first_name + '2', last_name + '2', @new_email, phone_num, company, job_title, password
from mltx_registered_user
where email = @orig_email;

declare @old_ru_id int = (select id from mltx_registered_user where email = @orig_email);
declare @new_ru_id int = (select id from mltx_registered_user where email = @new_email);

declare @db_to_swap_id int = (select max(id) from mltx_database where creator_id = @old_ru_id);

update mltx_login_database set login = @new_email where database_id = @db_to_swap_id;

update mltx_database set creator_id = @new_ru_id where id = @db_to_swap_id;
