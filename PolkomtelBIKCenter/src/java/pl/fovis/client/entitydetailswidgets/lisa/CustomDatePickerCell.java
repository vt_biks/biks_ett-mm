/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.lisa;

import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.text.shared.SafeHtmlRenderer;
import com.google.gwt.text.shared.SimpleSafeHtmlRenderer;
import java.util.Date;
import pl.fovis.common.lisa.LisaConstants;

/**
 *
 * @author ctran
 */
public class CustomDatePickerCell extends DatePickerCell {

    private LisaDataGrid dataGrid;
    private boolean isEditableColumn;
    private SafeHtmlRenderer<String> renderer;
//    private DateTimeFormat dateFormat;

    public CustomDatePickerCell(LisaDataGrid dataGrid, boolean isEditableColumn, DateTimeFormat format) {
        super(format);
//        this.dateFormat = format;
        renderer = SimpleSafeHtmlRenderer.getInstance();
        this.dataGrid = dataGrid;
        this.isEditableColumn = isEditableColumn;
        getDatePicker().setYearAndMonthDropdownVisible(true);
        getDatePicker().setVisibleYearCount(2000);
    }

    @Override
    public void render(Context context, Date value, SafeHtmlBuilder sb) {
//        if (context.getIndex() > 0 && !isEditableColumn) {
//            sb.appendHtmlConstant("<div>" + dateFormat.format(value) + "</div>");
//        } else {
//            super.render(context, value, sb);
//        }
        if (value == null) {
            sb.append(renderer.render(LisaConstants.NULL_VALUE));
        } else {
            super.render(context, value, sb);
        }
    }

    @Override
    public void onBrowserEvent(Context context, Element parent, Date value, NativeEvent event, ValueUpdater<Date> valueUpdater) {
        if (!dataGrid.isInEditMode() && (context.getIndex() > 0 || !dataGrid.isClickedAdd())) {
            return;
        }
        if (context.getIndex() > 0 && !isEditableColumn) {
            return;
        }
        if (value == null) {
            value = new Date();
        }
        super.onBrowserEvent(context, parent, value, event, valueUpdater); //To change body of generated methods, choose Tools | Templates.
    }
}
