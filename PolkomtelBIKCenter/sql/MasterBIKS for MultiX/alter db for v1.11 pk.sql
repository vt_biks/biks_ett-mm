------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- 1. Wyviagniecie informacji o hasle do jednego miejsca w bazie masterBiks
-- (wykonuje sie dlugo)
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
exec sp_mltx_check_version '1.11'
go
------------------------------------------------------

alter table mltx_registered_user add password varchar(128)
go

declare mltxusrs cursor for select email from mltx_registered_user;
open mltxusrs;

declare @usrlogin varchar(512);
declare @dbname varchar(512);
declare @pass varchar(512);
declare @sql nvarchar(max);
declare @ap varchar(4);

set @ap = '''';

fetch next from mltxusrs into @usrlogin
	while @@fetch_status = 0 begin
		set @dbname = (select top 1 db.database_name from mltx_registered_user ru join mltx_database db on ru.id = db.creator_id where ru.email = @usrlogin);
		if(@dbname is NULL)
		begin
			set @dbname = 
			(
				select db.database_name
				from mltx_login_database ld join mltx_database db on ld.database_id=db.id 
				join mltx_action_token at on (at.email = ld.login and at.database_id=db.id)
				where at.email=@usrlogin and at.created_at = (
				select min(at.created_at)
				from mltx_login_database ld join mltx_database db on ld.database_id=db.id 
				join mltx_action_token at on (at.email = ld.login and at.database_id=db.id)
				where at.email=@usrlogin)
			)
		end
		set @sql = 'update mltx_registered_user set password = (select password from ' + @dbname + '.dbo.bik_system_user where login_name = ' + @ap + @usrlogin + @ap + ') where email = '  + @ap + @usrlogin + @ap;
		EXECUTE sp_executesql @sql;
		fetch next from mltxusrs into @usrlogin
	end;
	close mltxusrs;
	deallocate mltxusrs;
go

--dla pewnosci
update mltx_registered_user set password = '' where password is null

alter table mltx_registered_user alter column password varchar(128) not null
go

------------------------------------------------------
exec sp_mltx_update_version '1.11', '1.12'
go
------------------------------------------------------