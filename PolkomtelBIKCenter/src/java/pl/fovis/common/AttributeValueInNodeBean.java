/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author ctran
 */
public class AttributeValueInNodeBean implements Serializable {

    public Integer nodeId;
    public String nodeName;
    public String treeCode;
    public String attrValue;
    public String nodeKindCode;
}
