/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.GrantRightsDialogNewRightSys;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.SystemUserGroupBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author Eugeniusz
 */
public class NodeActionGrantRightsNewRightsSys extends NodeActionGrantRights {

    protected SystemUserGroupBean sysUserGroupBean;

    public NodeActionGrantRightsNewRightsSys(SystemUserGroupBean nodeKindBean) {
        this.sysUserGroupBean = nodeKindBean;
    }

    @Override
    public boolean isEnabledByDesign() {
        return !BIKClientSingletons.hideGrantRightsBtnByAppProps()
                && BIKClientSingletons.newRightsSysByAppProps();
    }

    @Override
    public String getCaption() {
        if (BIKClientSingletons.isAppAdminLoggedIn() || BIKClientSingletons.isSysAdminLoggedIn()) {
            return I18n.nadajUprawnienia.get() + " " + sysUserGroupBean.name;
        } else {
            return I18n.podgladUprawnien.get() + " " + sysUserGroupBean.name;
        }
    }

    @Override
    public void execute() {
        BIKClientSingletons.getService().getAllUsersWithRightsInCurrentNode(node.id, sysUserGroupBean.id, new StandardAsyncCallback<List<SystemUserBean>>() {

            @Override
            public void onSuccess(List<SystemUserBean> result) {
                new GrantRightsDialogNewRightSys(node, sysUserGroupBean, result,
                        new IParametrizedContinuation<List<SystemUserBean>>() {
                            @Override
                            public void doIt(List<SystemUserBean> param) {
                                //przesyłamy wartość "kon" przy nadaniu uprawnień, żeby można było rozróżnić skąd nadane uprawnienia (kon - kontekst, int - interfejs, sql - sqlką, AD - przy zasilaniu z AD)
                                BIKClientSingletons.getService().grantRightsFromContextForUserNewRightsSys(param, node.id, sysUserGroupBean.id, "kon", new StandardAsyncCallback<Void>() {

                                    @Override
                                    public void onSuccess(Void result) {
                                        BIKClientSingletons.showInfo(I18n.uprawnieniaNadane.get() /* I18N:  */);
                                    }
                                });
                            }
                        }
                ).buildAndShowDialog();
            }
        });
    }
}
