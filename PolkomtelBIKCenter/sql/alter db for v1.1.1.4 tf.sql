﻿exec sp_check_version '1.1.1.4';
go

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------

create table bik_dqc_test_parameters(
		test_id int, 
		parameter_name varchar(300)
)
go

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_connections_for_dqc]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_insert_into_bik_joined_objs_connections_for_dqc
go

create procedure sp_insert_into_bik_joined_objs_connections_for_dqc as
begin

create table #tmp_jo (teradata int, dqc int);
create table #tmp_to_insert (src int, dst int);

declare @teradataTree int;
select @teradataTree=id from bik_tree where code='Teradata'
declare @dqcTree int;
select @dqcTree=id from bik_tree where code='DQC'
declare @reportsTree int;
select @reportsTree=id from bik_tree where code='Reports'
declare @universeTree int;
select @universeTree=id from bik_tree where code='ObjectUniverses'
declare @connectionTree int;
select @connectionTree=id from bik_tree where code='Connections'

insert into #tmp_jo(teradata,dqc)
select src.id, dst.id from bik_joined_objs jo
join bik_node src on src.id=jo.src_node_id
join bik_node dst on dst.id=jo.dst_node_id
where src.tree_id = @teradataTree and src.is_deleted = 0 and src.linked_node_id is null
and dst.tree_id=@dqcTree and dst.is_deleted = 0 and dst.linked_node_id is null
and jo.type=1 -- tylko automatyczne

insert into #tmp_to_insert(src,dst)
select distinct bn.id, tmp.dqc from #tmp_jo tmp
join bik_joined_objs jo on tmp.teradata=jo.dst_node_id 
join bik_node bn on bn.id=jo.src_node_id
where bn.is_deleted = 0 and linked_node_id is null and bn.id <> tmp.dqc
and bn.tree_id in (@reportsTree, @universeTree, @connectionTree)
and jo.type=1 -- tylko automatyczne

insert into bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
select src, dst, 1, 0
from #tmp_to_insert tmp
left join bik_joined_objs a on a.src_node_id=src and a.dst_node_id=dst
where a.id is null

insert into bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
select dst, src, 1, 0
from #tmp_to_insert tmp
left join bik_joined_objs a on a.src_node_id=dst and a.dst_node_id=src
where a.id is null

drop table #tmp_to_insert
drop table #tmp_jo

end
go

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------

exec sp_update_version '1.1.1.4', '1.1.1.5';
go