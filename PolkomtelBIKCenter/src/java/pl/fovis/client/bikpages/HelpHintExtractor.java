/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import java.util.ArrayList;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.EditHelpDescriptionDialog;
import pl.fovis.client.entitydetailswidgets.GenericBeanExtractorBase;
import pl.fovis.client.entitydetailswidgets.IGridBeanAction;
import pl.fovis.common.HelpBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;
import simplelib.i18nsupport.I18nMessage;

/**
 *
 * @author mmastalerz
 */
public class HelpHintExtractor extends GenericBeanExtractorBase<HelpBean> {

    public HelpHintExtractor() {
        super(false);
    }

    protected String getName(HelpBean b) {
        return "";
    }

    public String getHelpKey(HelpBean b) {
        return b.helpKey;
    }

    @Override
    public int getAddColCnt() {
        return 1;
    }

    @Override
    public Object getAddColVal(HelpBean b, int addColIdx) {
        Label helpName = new Label(b.caption);
        helpName.setStyleName("Notes" /* i18n: no:css-class */ + "-labBody");
        helpName.setTitle(b.helpKey);
        return helpName;
    }

    @Override
    public boolean createGridColumnNames(FlexTable gp) {
        gp.setStyleName("gridJoinedObj");
        gp.setWidget(0, 0, new HTML(I18n.nazwa.get() /* I18N:  */));
        gp.getFlexCellFormatter().setColSpan(0, 0, 4);
        gp.getRowFormatter().setStyleName(0, "RelatedObj-oneObj-dqc");
        return true;
    }

    @Override
    protected List<IGridBeanAction<HelpBean>> createActionList() {
        List<IGridBeanAction<HelpBean>> res = new ArrayList<IGridBeanAction<HelpBean>>();
        res.add(actEdit);
        return res;
    }

    @Override
    public String getIconUrl(HelpBean b) {
        return "images/question.png" /* I18N: no */;
    }

//    @Override
//    public String getOptIconHint(HelpBean b) {
//        return getHelpKey(b);
//    }
    @Override
    protected void execActEdit(final HelpBean b) {
        BIKClientSingletons.getService().getHelpDescr(b.helpKey, b.caption, new StandardAsyncCallback<HelpBean>() {
            public void onSuccess(final HelpBean result) {
                new EditHelpDescriptionDialog().buildAndShowDialog(result, new IParametrizedContinuation<HelpBean>() {
                    public void doIt(final HelpBean param) {
                        param.lang = I18nMessage.getCurrentLocaleName();
                        BIKClientSingletons.getService().updateBikHelpHint(param, new StandardAsyncCallback<Void>() {
                            public void onSuccess(Void result) {
                                refreshView(I18n.zmienionoOpis.get() /* I18N:  */);
                            }
                        });
                    }
                });
            }
        });
    }

    @Override
    public String getTreeCode(HelpBean b) {
        // nie uzywane
        return null;
    }
}
