/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.i18nsupport;

import java.util.Set;

/**
 *
 * @author pmielanczuk
 */
public interface IDynamicI18nMessages<T extends I18nMessages> {

    public I18nMessage find(String key);

    public Set<String> getKeys();
}
