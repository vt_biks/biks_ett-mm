/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.Set;
import simplelib.BaseUtils.Projector;

/**
 *
 * @author wezyr
 */
public class CachedGenericResourceProviderEx<T> extends CachedGenericResourceProvider<T>
        implements IGenericResourceProviderEx<T> {

    private Projector<String, String> innerToNameProj = new Projector<String, String>() {

        public String project(String val) {
            return innerNameToResourceName(val);
        }
    };

    public CachedGenericResourceProviderEx(IGenericResourceProviderEx<T> resProvider) {
        super(resProvider);
    }

    private IGenericResourceFinder getFinder() {
        return (IGenericResourceFinder) resProvider;
    }

    protected String innerNameToResourceName(String innerName) {
        return innerName;
    }

    public Set<String> getResourceNames(String base, Set<String> fileExts, boolean recurseSubDirs) {
        Set<String> innerRs = getFinder().getResourceNames(resourceNameToInnerName(base),
                fileExts, recurseSubDirs);
        return BaseUtils.projectToSet(innerRs, innerToNameProj, false);
    }
}
