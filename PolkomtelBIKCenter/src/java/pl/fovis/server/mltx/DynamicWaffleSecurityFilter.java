/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.mltx;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import pl.fovis.common.BIKConstants;
import simplelib.LameRuntimeException;
import waffle.servlet.NegotiateSecurityFilter;

/**
 *
 * @author pmielanczuk
 */
public class DynamicWaffleSecurityFilter implements Filter {

    protected NegotiateSecurityFilter waffleFilter;
    protected FilterConfig fc;
    protected static boolean useWaffle = false;
    protected static boolean isDWSFInited;

    @Override
    public void init(FilterConfig fc) throws ServletException {
        this.fc = fc;
        isDWSFInited = true;
    }

    protected synchronized boolean initOnceIfShouldUseWaffle() {
        if (useWaffle && waffleFilter == null) {
            waffleFilter = new NegotiateSecurityFilter();
            try {
                waffleFilter.init(fc);
            } catch (ServletException ex) {
                throw new LameRuntimeException("cannot init waffle", ex);
            }
        }
        return useWaffle;
    }

    @Override
    public void doFilter(ServletRequest sr, ServletResponse sr1, FilterChain fc) throws IOException, ServletException {
        boolean isRss = sr.getParameter(BIKConstants.RSS) != null; //bez zalogowania dla RSS

        if (!isRss && initOnceIfShouldUseWaffle()) {
            waffleFilter.doFilter(sr, sr1, fc);
        } else {
            fc.doFilter(sr, sr1);
        }
    }

    @Override
    public synchronized void destroy() {
        if (waffleFilter != null) {
            waffleFilter.destroy();
            waffleFilter = null;
        }
        isDWSFInited = false;
    }

    public static void setUseWaffle(boolean value) {
        useWaffle = value;
    }

    public static boolean isDWSFInited() {
        return isDWSFInited;
    }
}
