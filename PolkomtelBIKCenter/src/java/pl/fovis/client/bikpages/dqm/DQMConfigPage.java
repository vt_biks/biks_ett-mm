/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages.dqm;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.BikPageMinimalBase;
import pl.fovis.common.ConnectionParametersDQMBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.SimpleInfoDialog;
import simplelib.BaseUtils;

/**
 *
 * @author tflorczak
 */
public class DQMConfigPage extends BikPageMinimalBase {

    protected VerticalPanel vp;
    protected CheckBox isActive;
    protected TextBox errorData;
    protected PushButton btnSave;

    @Override
    protected Widget buildWidgets() {
        vp = new VerticalPanel();
        vp.setSpacing(4);
        vp.setStyleName("mainConnConf");
        isActive = new CheckBox(I18n.aktywny.get());
        isActive.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setWidgetActivity(event.getValue());
            }
        });

        vp.add(isActive);
        vp.add(createLine());
        vp.add(new Label(I18n.trzymajBledneDaneDlaOstatnichWykonan.get()));
        vp.add(errorData = new TextBox());
        vp.add(createLine());
        vp.add(btnSave = new PushButton(I18n.zapisz.get()));
        NewLookUtils.makeCustomPushButton(btnSave);
        btnSave.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                ConnectionParametersDQMBean bean = new ConnectionParametersDQMBean();
                bean.isActive = isActive.getValue() ? 1 : 0;
                bean.lastErrorRequests = BaseUtils.tryParseInteger(errorData.getValue());
                if (bean.lastErrorRequests == null) {
                    new SimpleInfoDialog().buildAndShowDialog(I18n.wpiszPoprawneWartosci.get(), null, null);
                } else {
                    BIKClientSingletons.getDQMService().setDQMConnectionParameters(bean, new StandardAsyncCallback<Void>("Error in setDQMConnectionParameters") {

                        @Override
                        public void onSuccess(Void result) {
                            BIKClientSingletons.showInfo(I18n.zapisanoZmiany.get() /* I18N:  */);
                        }
                    });
                }
            }
        });
        return vp;
    }

    @Override
    public String getId() {
        return "admin:dqm:config";
    }

    @Override
    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
        BIKClientSingletons.getDQMService().getDQMConnectionParameters(new StandardAsyncCallback<ConnectionParametersDQMBean>("Error in getDQMConnectionParameters") {

            @Override
            public void onSuccess(ConnectionParametersDQMBean result) {
                isActive.setValue(result.isActive == 1);
                errorData.setValue(String.valueOf(result.lastErrorRequests));
                setWidgetActivity(isActive.getValue());
            }
        });
    }

    @Override
    public boolean isPageEnabled() {
//        return BIKClientSingletons.availableConnectors().contains(PumpConstants.SOURCE_NAME_DQM) && BIKClientSingletons.isSysAdminLoggedIn();
        return BIKClientSingletons.isConnectorAvailable(PumpConstants.SOURCE_NAME_DQM) && super.isPageEnabled()/*BIKClientSingletons.isSysAdminLoggedInEx(getId())*/;
    }

    protected void setWidgetActivity(boolean isActive) {
        errorData.setEnabled(isActive);
    }

    protected Widget createLine() {
        return new HTML("<HR WIDTH=\"500\" ALIGN=LEFT>");
    }
}
