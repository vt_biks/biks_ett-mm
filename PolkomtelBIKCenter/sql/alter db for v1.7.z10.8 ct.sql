exec sp_check_version '1.7.z10.8';
go 

update bik_app_prop set val='1' where name='enableNodeHistory';
go

exec sp_update_version '1.7.z10.8', '1.7.z10.9';
go