/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import pl.trzy0.foxy.commons.MailServerConfig;
import simplelib.Pair;

/**
 *
 * @author pmielanczuk
 */
public class BiksSharedSessionObj {

    protected final ThreadLocal<String> currentSingleBiksDb = new ThreadLocal<String>();
    protected final ThreadLocal<Map<Integer, Pair<Long, String>>> overwrittenMods = new ThreadLocal<Map<Integer, Pair<Long, String>>>();
    protected Map<String, BOXIServiceImplData> instanceDataMap = new ConcurrentHashMap<String, BOXIServiceImplData>();

    public MailServerConfig mailServerCfg;
}
