/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.lisa;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.CheckBox;
import java.io.Serializable;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.FoxyDialogSerializer;
import pl.fovis.foxygwtcommons.dialogs.FoxyValidatingDialogBase;

/**
 *
 * @author tbzowka
 */
public abstract class SerializableLisaDialogBase<B extends Serializable> extends FoxyValidatingDialogBase<B> {

    public final static ILisaBeansWrapperCreator beanWrapperCreator = GWT.create(ILisaBeansWrapperCreator.class);
    public final static ILisaDialogsWrapperCreator dialogWrapperCreator = GWT.create(ILisaDialogsWrapperCreator.class);

    public SerializableLisaDialogBase() {
        setSerializer(new FoxyDialogSerializer<Serializable, B, B>(dialogWrapperCreator, beanWrapperCreator, this) {
            @Override
            protected void registerValueHandlers() {
                super.registerValueHandlers();
                registerValueHandler(CheckBox.class, new CheckBoxValueHandler());
            }

            @Override
            protected void registerAllInputControlPropNameSuffixes() {
                super.registerAllInputControlPropNameSuffixes();
                registerInputControlPropNameSuffixes("Cb" /* i18n: no */);
            }
        });
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get() /* I18N:  */;
    }
}
