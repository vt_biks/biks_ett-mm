﻿exec sp_check_version '1.5.x.3';
go

----------------------------------------------------
----------------------------------------------------
----------------------------------------------------
--		nowa tabelka z szablonami baz dla MULTIXa
--		dodanie kolumny i klucza obcego do mltx_registration
--		usuniecie kolumny db_option z mltx_registration
----------------------------------------------------
----------------------------------------------------
----------------------------------------------------
create table mltx_database_template (
id int identity(1,1) not null,
caption varchar(256) not null,
restore_prefix varchar(32) not null,
file_path varchar(512) not null,
lang varchar(3) not null,
constraint pk_mltx_database_template_id primary key clustered (	id asc ),
)
go

insert into mltx_database_template (caption, file_path, restore_prefix, lang)
     values ('Pusta baza BIKS','c:\biks_demo_empty.bak', 'empty_', 'pl');
insert into mltx_database_template (caption, file_path, restore_prefix, lang)
     values ('Baza BIKS z przykładowymi danymi w języku polskim','c:\biks_demo_pl.bak', 'pl_', 'pl');
insert into mltx_database_template (caption, file_path, restore_prefix, lang)
     values ('BIKS database with sample english data','c:\biks_demo_en.bak', 'en_', 'en');
go

alter table mltx_registration drop column db_option;
go

alter table mltx_registration add template_id int null;
go


alter table mltx_registration add constraint fk_mltx_registration_database_template FOREIGN KEY (template_id)
      REFERENCES mltx_database_template (id);
go

update mltx_registration set template_id = (select top 1 id from mltx_database_template);	  
	  

exec sp_update_version '1.5.x.3', '1.5.x.4';
go
