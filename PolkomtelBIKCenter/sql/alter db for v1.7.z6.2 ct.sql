exec sp_check_version '1.7.z6.2';
go


-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------
if not exists(select 1 from syscolumns sc where id = OBJECT_ID('[bik_search_cache]') and name = 'id')
begin
	alter table bik_search_cache add id int not null identity(1,1);
end;
go
------------

exec sp_update_version '1.7.z6.2', '1.7.z6.3';
go

 