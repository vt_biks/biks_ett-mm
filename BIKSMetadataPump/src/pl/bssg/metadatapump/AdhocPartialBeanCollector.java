/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump;

import pl.trzy0.foxy.commons.INamedSqlsDAO;

/**
 *
 * @author tflorczak
 */
public class AdhocPartialBeanCollector<T> extends BeanCollectorBase<T> {

    protected String namedQuery;
    protected INamedSqlsDAO<Object> adhocDAO;

    public AdhocPartialBeanCollector(int bufforSize, INamedSqlsDAO<Object> adhocDAO, String namedQuery) {
        super(bufforSize);
        this.adhocDAO = adhocDAO;
        this.namedQuery = namedQuery;
    }

    @Override
    protected void insertListIntoDatabaseAndClear() {
        adhocDAO.execNamedCommand(namedQuery, list);
        list.clear();
    }
}
