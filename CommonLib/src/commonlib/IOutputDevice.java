/*
 * IOutputDevice.java
 *
 * Created on 21 marzec 2007, 15:56
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package commonlib;

import java.io.InputStream;

/**
 *
 * @author wezyr
 */
public interface IOutputDevice {
    IOutputDevice append(String s);
    void redirect(String newLocation);
    void setContentType(String contentType);
    void setEncoding(String encoding);
    void addHeader(String name, String value);
    void writeBinaryContent(int contentLength, InputStream is);
}
