/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.confluence.wuwu;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import simplelib.BaseUtils;
import simplelib.IRegExpMatcher;
import simplelib.IRegExpPattern;
import simplelib.Pair;
import simplelib.deferredstringconverter.IDeferredRegExprReplacer;

/**
 *
 * @author pmielanczuk
 */
public class UrlAbsolutizingDeferredReplacer implements IDeferredRegExprReplacer<IRegExpMatcher, Pair<String, String>> {

    public static final String REL_HREF_QUOTE_REGEXPR = "[ \\t\\n\\r]+(href)[ \\t\\n\\r]*=[ \\t\\n\\r]*[\"]([^\"]*)[\"]";
    protected static final IRegExpPattern REL_HREF_PATT_Q = ConfluenceUtils.compileRegExpr(REL_HREF_QUOTE_REGEXPR);
    protected static final IRegExpPattern REL_HREF_PATT_A = ConfluenceUtils.compileRegExpr(REL_HREF_QUOTE_REGEXPR.replace("\"", "'"));
    public static final String REL_SRC_QUOTE_REGEXPR = "[ \\t\\n\\r]+(src)[ \\t\\n\\r]*=[ \\t\\n\\r]*[\"]([^\"]*)[\"]";
    protected static final IRegExpPattern REL_SRC_PATT_Q = ConfluenceUtils.compileRegExpr(REL_SRC_QUOTE_REGEXPR);
    protected static final IRegExpPattern REL_SRC_PATT_A = ConfluenceUtils.compileRegExpr(REL_SRC_QUOTE_REGEXPR.replace("\"", "'"));
    protected static final IRegExpPattern REL_URL_PATT = ConfluenceUtils.compileRegExpr("[:][ \\t\\n\\r]*(url)[ \\t\\n\\r]*[(][ \\t\\n\\r]*([^)]*)[)]");
    protected String absoluteUrlBase;
//    protected int aubEndsWithSlash;
    protected String protoAndSvr;
    protected String urlPath;
    protected String urlQuery;
    protected String urlPathEnsuringSlashStr;

    public UrlAbsolutizingDeferredReplacer(String absoluteUrlBase) {
        this.absoluteUrlBase = absoluteUrlBase;
//        this.aubEndsWithSlash = BaseUtils.booleanToInt(absoluteUrlBase.endsWith("/"));
        Pair<String, String> p1 = BaseUtils.splitGlobalUrl(absoluteUrlBase);
        this.protoAndSvr = p1.v1;
        Pair<String, String> p2 = BaseUtils.splitString(p1.v2, "?");
        this.urlPath = p2.v1;
        this.urlQuery = p2.v2;
        this.urlPathEnsuringSlashStr = this.urlPath.endsWith("/")
                ? "" : "/";
    }

    @Override
    public IRegExpMatcher[] makeMatchers(String s) {
        return new IRegExpMatcher[]{
            REL_HREF_PATT_Q.matcher(s),
            REL_HREF_PATT_A.matcher(s),
            REL_SRC_PATT_Q.matcher(s),
            REL_SRC_PATT_A.matcher(s),
            REL_URL_PATT.matcher(s)
        };
    }

    @Override
    public Pair<String, String> getDeferredReplacement(IRegExpMatcher m) {
        return new Pair<String, String>(m.group(1), m.group(2));
    }

    @Override
    public Collection<String> resolveReplacements(Collection<Pair<String, String>> replacements) {

        List<String> res = new ArrayList<String>(replacements.size());

        for (Pair<String, String> p : replacements) {
            String kind = p.v1;
            String url = p.v2;

            if (!BaseUtils.isUriGlobal(url)) {
                if (url.startsWith("#")) {
                    url = absoluteUrlBase + url;
                } else {
                    boolean urlStartsWithSlash = url.startsWith("/");
                    if (urlStartsWithSlash) {
                        url = protoAndSvr + url;
                    } else {
                        url = protoAndSvr + urlPath + urlPathEnsuringSlashStr + url;
                    }
                }
            }

            if (BaseUtils.safeEquals(kind, "url")) {
                res.add(": url(" + url + ")");
            } else {
                res.add(" " + kind + "=\"" + url + "\"");
            }
        }

        return res;
    }
}
