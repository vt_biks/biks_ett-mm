﻿
exec sp_check_version '1.0.66';
go

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_delete_linked_nodes_where_orignal_is_deleted]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_delete_linked_nodes_where_orignal_is_deleted
go

create procedure sp_delete_linked_nodes_where_orignal_is_deleted
as
	declare @rc int = 1;
begin
	while @rc > 0 
	begin

		update bik_node
		set is_deleted = 1
		from bik_node join bik_node oryginal on bik_node.linked_node_id = oryginal.id
		where oryginal.is_deleted = 1 and bik_node.is_deleted = 0

		set @rc = @@ROWCOUNT;

		update bik_node
		set is_deleted = 1
		from bik_node join bik_node parent on bik_node.parent_node_id = parent.id
		where parent.is_deleted = 1 and bik_node.is_deleted = 0

		set @rc = @rc + @@ROWCOUNT
	
	end;--end loop
end;--end procedure

go
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_insert_into_bik_node
go

create procedure sp_insert_into_bik_node(@tree_code varchar(155))
as
	declare @table_folders varchar(155);
	declare @tree_id int;
	declare @kinds varchar(155);
begin
	set nocount on;
	
	--print cast(sysdatetime() as varchar(11)) + ': faza 1'
	
	select @tree_id = id 
	from bik_tree
	where code=@tree_code;

	create table #primaryIds(si_id varchar(1000) primary key);
	create table #secondaryIds(si_id varchar(1000) primary key);

	if(@tree_code!='Reports' and @tree_code!='Teradata')
	begin
		set @table_folders = ' APP_FOLDER where 1=1 ';
		
		if(@tree_code = 'Connections')
		begin
			set @kinds = '''MetaData.DataConnection''';
			exec('insert into #primaryIds
					select convert(varchar,SI_ID) as SI_ID  
					from aaa_global_props
					where SI_PARENTID in (select si_id from ' + @table_folders + ') and SI_KIND in (' + @kinds + ')');
			exec('insert into #secondaryIds 
					select convert(varchar,SI_ID) as SI_ID
					from APP_UNIVERSE
					where SI_DATACONNECTION__1 in (select * from #primaryIds)');			
		end;
		if(@tree_code = 'ObjectUniverses')
		begin
			set @kinds = '''Universe''';
			exec('insert into #primaryIds
					select convert(varchar,SI_ID) as SI_ID  
					from aaa_global_props
					where SI_PARENTID in (select si_id from ' + @table_folders + ') and SI_KIND in (' + @kinds + ')');
		end;		
	end
	
	if(@tree_code='Reports')
	begin
		set @table_folders = ' INFO_FOLDER where si_name != ''~Webintelligence''';
		set @kinds = '''Webi'', ''Flash'', ''CristalReports''';
		
	end;
	
    declare @sql nvarchar(3000);
				
	declare @node_kind_id int;
	set @sql = N'';
	if(@tree_code = 'Teradata')
	begin
		--print cast(sysdatetime() as varchar(11)) + ': faza 4.1 - teradata przed sp_teradata_init_branch_names'
		exec sp_teradata_init_branch_names;
		--print cast(sysdatetime() as varchar(11)) + ': faza 4.1 - teradata po sp_teradata_init_branch_names'
		set @sql = N'select branch_names as SI_ID, 
											case 
												when SUBSTRING(branch_names, 1,len(branch_names) - len(name + ''|'')) =''''
													then null
												else SUBSTRING(branch_names, 1,len(branch_names) - len(name + ''|'')) end
												as SI_PARENTID, 
						type as SI_KIND, name as SI_NAME, extra_info as DESCR
				 from bik_teradata
				/*where parent_id is not null or name like ''VD_US_%''*/';
	end;
	else
	begin
		set @sql = N'select si_id, case 
									when SI_PARENTID in(select si_id from ' + @table_folders +') 
										then SI_PARENTID
									else 0 end as SI_PARENTID, SI_KIND, SI_NAME, NULL as DESCR 
				 from ' + @table_folders + 
													
				'union all

				 select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, NULL as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +')';
		if(@tree_code = 'Connections')
			set @sql = @sql + N'union all		  

								select SI_ID, SI_DATACONNECTION__1 as SI_PARENTID, SI_KIND, SI_NAME, NULL as DESCR 
								from APP_UNIVERSE
								where SI_DATACONNECTION__1 in (select * from #primaryIds)
	
								union all
								
								select SI_ID, SI_UNIVERSE__1 as SI_PARENTID, SI_KIND, SI_NAME, NULL as DESCR 
								from INFO_WEBI
								where SI_UNIVERSE__1 in (select * from #secondaryIds)';

		if(@tree_code = 'ObjectUniverses')
			set @sql = @sql + N'union all
			
								select SI_ID, SI_UNIVERSE__1 as SI_PARENTID, SI_KIND,  SI_NAME, NULL as DESCR 
								from INFO_WEBI
								where SI_UNIVERSE__1 in (select * from #primaryIds)';
	end;					

	--print cast(sysdatetime() as varchar(11)) + ': faza 6 - przed zapytaniem'
	--print 'zapytanie: ' + @sql;

	create table #tmpNodes (si_id varchar(1000) primary key, si_parentid varchar(1000), si_kind varchar(max), si_name varchar(max), descr varchar(30));

	set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
				select si_id, si_parentid, dbo.fn_node_kind_code_for_metadata_type(si_kind), si_name, DESCR
			    from (' + @sql + ') xxx';
			   
	EXECUTE(@sql);

	--print cast(sysdatetime() as varchar(11)) + ': faza 6.5 - po wrzuceniu do #tmpNodes'

	insert into bik_node_kind (code,caption) 
	select distinct si_kind, si_kind
	from #tmpNodes 
	where not exists (select 1 from bik_node_kind where code = si_kind);

	update bik_node 
	set parent_node_id = null, node_kind_id = bik_node_kind.id, name = #tmpNodes.si_name,
		is_deleted = 0, descr = #tmpNodes.descr 
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.obj_id = #tmpNodes.si_id and tree_id = @tree_id;

	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select bik_node_kind.id, #tmpNodes.si_name, @tree_id, #tmpNodes.si_id, #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
		left join bik_node on bik_node.obj_id = #tmpNodes.si_id 
	where bik_node.id is null;
	--print cast(sysdatetime() as varchar(11)) + ': faza 7.0 - po ciężkiej walce bez kursora?'

	--print cast(sysdatetime() as varchar(11)) + ': faza 9.5 - po pętli, zamknięte kursory, usuwamy pustaki z bik_node'
	set nocount on;

	drop table #primaryIds;
	drop table #secondaryIds;
	--print cast(sysdatetime() as varchar(11)) + ': faza 10 - przed uaktualnieniem parentów'
	
	update bik_node 
	set parent_node_id= bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id and bik_node.tree_id = @tree_id
		inner join bik_node bk on bk.obj_id = pt.si_parentid and bk.tree_id = @tree_id

	--print cast(sysdatetime() as varchar(11)) + ': faza 11 - po uaktualnieniu parentów'
	update --delete from 
	bik_node
	set is_deleted = 1
	--select *
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id join bik_node_kind on bik_node.node_kind_id = bik_node_kind.id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and not bik_node_kind.code in ('UniverseColumn', 'UniverseTable', 'UniverseObject', 'UniverseClass')

	declare @folder_node_kind_id int

	select @folder_node_kind_id = ID from bik_node_kind where code = 'MetadataFolder';

	declare @rc int = 1

	while @rc > 0 begin
		--delete from 
		update bik_node
		set is_deleted = 1
		where tree_id = @tree_id and node_kind_id = @folder_node_kind_id and is_deleted = 0
		 and not exists(select 1 from bik_node g where g.parent_node_id = bik_node.id and g.is_deleted = 0)

		set @rc = @@ROWCOUNT
	end;--end loop
				
	--drop table #pomTable;
	drop table #tmpNodes
	--print cast(sysdatetime() as varchar(11)) + ': faza 11 - po delete zbędnych'
	exec sp_delete_linked_nodes_where_orignal_is_deleted;
	exec sp_node_init_branch_id @tree_id, null;
	--print cast(sysdatetime() as varchar(11)) + ': faza 11 - po sp_node_init_branch_id, finito!'
end;
go

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------


if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_objects_from_Designer]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_insert_into_bik_node_objects_from_Designer
go

create procedure sp_insert_into_bik_node_objects_from_Designer
as
	declare @tree_id int;
	declare @kind_id int;
	declare @tabele_node_kind_id int;
	declare @obj_node_kind_id int;
	declare @column_node_kind_id int;
begin--begin procedure

	----wybranie id odpowiedniego drzewa
	select @tree_id = id
	from bik_tree
	where code = 'ObjectUniverses';	
	
	----wybranie id odpowiedniego kind'a
	select @kind_id = id
	from bik_node_kind
	where code = 'UniverseClass';
		
	create table #tmpNodes (si_id varchar(1000) , si_parentid varchar(1000), si_kind_id int, si_name varchar(max), si_description varchar(max));

	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Class
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id), 
		case
			when uc.parent_id is null then convert(varchar, u.global_props_si_id)
			else convert(varchar, uc.parent_id) + '|' + convert(varchar, u.global_props_si_id) end as parent_id, 
		@kind_id, uc.name, uc.description
	from aaa_universe_class uc join aaa_universe u on u.id = uc.universe_id 
	
	----wybranie id odpowiedniego kind'a
	select @obj_node_kind_id = id
	from bik_node_kind
	where code = 'UniverseObject';
	
	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Obj
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select convert(varchar, uo.si_id) + '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id), 
			case
				when uo.parent_id is null then convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id)
				else convert(varchar, uo.parent_id)+ '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id)
			end as parent_id, 
			@obj_node_kind_id, uo.name, uo.description
	from aaa_universe_obj uo join aaa_universe_class uc on uo.universe_class_id = uc.id
							 join aaa_universe u on u.id = uc.universe_id 
	
	----wybranie id odpowiedniego kind'a
	select @tabele_node_kind_id = id
	from bik_node_kind
	where code = 'UniverseTable';
							 
	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Table	
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name)		
	select convert(varchar, ut.their_id) + '|' + convert(varchar, u.global_props_si_id), convert(varchar, u.global_props_si_id), 
			@tabele_node_kind_id, ut.name 
	from aaa_universe_table ut	join aaa_universe u on ut.universe_id = u.id	
	
	----wybranie id odpowiedniego kind'a
	select @column_node_kind_id = id
	from bik_node_kind
	where code = 'UniverseColumn';
	
	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Column
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name)
	select convert(varchar, ut.their_id) + '|' + convert(varchar, u.global_props_si_id) + '|' + uc.name, 
		convert(varchar, ut.their_id) + '|' + convert(varchar, u.global_props_si_id), @column_node_kind_id, uc.name
	from aaa_universe_table ut join aaa_universe_column uc on ut.id = uc.universe_table_id
							   join aaa_universe u on ut.universe_id = u.id		
								
	--aktualizacja istniejących węzłów w drzewie							
	update bik_node 
	set parent_node_id = null, node_kind_id = #tmpNodes.si_kind_id, name = #tmpNodes.si_name,
		is_deleted = 0, descr = #tmpNodes.si_description
	from #tmpNodes 
	where bik_node.obj_id = #tmpNodes.si_id and tree_id = @tree_id and bik_node.node_kind_id = #tmpNodes.si_kind_id;
	
	--dorzucanie nowych węzłów w drzewie
	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select #tmpNodes.si_kind_id, #tmpNodes.si_name, @tree_id, #tmpNodes.si_id, #tmpNodes.si_description
	from #tmpNodes left join bik_node on bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id
		and bik_node.tree_id = @tree_id
	where bik_node.id is null;	
	
	--uaktualnianie parentów w nodach
	declare @universe_kind_id int;
	select @universe_kind_id = id
	from bik_node_kind
	where code = 'Universe';
	
	update bik_node 
	set parent_node_id= bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id and bik_node.tree_id = @tree_id and bik_node.node_kind_id = pt.si_kind_id
		inner join bik_node bk on bk.obj_id = pt.si_parentid and bk.tree_id = @tree_id -- and bk.node_kind_id = pt.si_kind_id
		and  ((pt.si_kind_id=@column_node_kind_id and bk.node_kind_id = @tabele_node_kind_id) or
		 (pt.si_kind_id=@obj_node_kind_id and (bk.node_kind_id = @obj_node_kind_id or bk.node_kind_id = @kind_id)) or 
		 (pt.si_kind_id=@tabele_node_kind_id and bk.node_kind_id = @universe_kind_id)or
		 (pt.si_kind_id=@kind_id and (bk.node_kind_id = @kind_id or bk.node_kind_id = @universe_kind_id)))
		
	--usuwanie nodów
	update bik_node
	set is_deleted = 1
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and bik_node.node_kind_id in (@kind_id, @tabele_node_kind_id, @obj_node_kind_id, @column_node_kind_id);
	
	drop table #tmpNodes;
	
	create table #tmp(table_node_id int, obj_node_id int)
	
	
	insert into #tmp(table_node_id, obj_node_id)
	select bn.id, bn1.id
	from bik_node bn join (		
					select  convert(varchar, ut.their_id) + '|' + convert(varchar, u.global_props_si_id) as table_id, 
							convert(varchar, uo.si_id) + '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u1.global_props_si_id) as obj_id
					from aaa_universe_obj_tables uob join aaa_universe_table ut on uob.universe_table_id = ut.id
 						join aaa_universe u on ut.universe_id = u.id
 						join aaa_universe_obj  uo on uo.id = uob.universe_obj_id
 						join aaa_universe_class uc on uo.universe_class_id = uc.id
						join aaa_universe u1 on u1.id = uc.universe_id ) ot on bn.obj_id = ot.table_id and bn.node_kind_id = @tabele_node_kind_id
						join bik_node bn1 on bn1.obj_id = ot.obj_id and bn1.node_kind_id = @obj_node_kind_id 

	delete from bik_joined_objs
	from bik_joined_objs join bik_node n on bik_joined_objs.dst_node_id = n.id and n.node_kind_id in (@tabele_node_kind_id, @obj_node_kind_id)
	
	----teraz insert
	----
	insert into bik_joined_objs(src_node_id, dst_node_id, type)
	select #tmp.table_node_id, #tmp.obj_node_id, 1
	from #tmp 
		
	insert into bik_joined_objs(src_node_id, dst_node_id, type)
	select #tmp.obj_node_id, #tmp.table_node_id, 1
	from #tmp  	
	
	drop table #tmp
	
	--uzupełnianie danych w tabelach extra 
	--dla object
	create table #tmpObjExtra(text_of_select varchar(max), type varchar(155), qualification varchar(50), aggregate_function varchar(50), node_id int)
	
	insert into #tmpObjExtra(text_of_select, type, qualification, aggregate_function, node_id)
	select univ_obj.text_of_select, univ_obj.type, univ_obj.qualification, univ_obj.aggregate_function, bik_node.id
	from (
	select convert(varchar, uo.si_id) + '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id) as obj_id, 
					uo.text_of_select, uo.type, uo.qualification, uo.aggregate_function
			from aaa_universe_obj uo join aaa_universe_class uc on uo.universe_class_id = uc.id
							 join aaa_universe u on u.id = uc.universe_id ) univ_obj 
			join bik_node on bik_node.obj_id = univ_obj.obj_id and bik_node.node_kind_id = @obj_node_kind_id;
			
	update bik_sapbo_universe_object
	set text_of_select = #tmpObjExtra.text_of_select, type = #tmpObjExtra.type, qualification = #tmpObjExtra.qualification, 
		           aggregate_function = #tmpObjExtra.aggregate_function 
	from #tmpObjExtra join bik_sapbo_universe_object on bik_sapbo_universe_object.node_id = #tmpObjExtra.node_id;
	
	insert into bik_sapbo_universe_object(text_of_select, type, qualification, aggregate_function, node_id)
	select #tmpObjExtra.text_of_select, #tmpObjExtra.type, #tmpObjExtra.qualification, #tmpObjExtra.aggregate_function, #tmpObjExtra.node_id 
	from #tmpObjExtra left join bik_sapbo_universe_object on bik_sapbo_universe_object.node_id = #tmpObjExtra.node_id
	where bik_sapbo_universe_object.id is null;

	drop table #tmpObjExtra;
	
	
	--dla tabeli
	create table #tmpTableExtra(
		is_alias int not null check(is_alias in (0,1)), 
		is_derived int not null check(is_derived in (0,1)),
		sql_of_derived_table varchar(max),
		sql_of_derived_table_with_alias varchar(max),
		original_table int ,
		node_id int );
		
	insert into #tmpTableExtra(is_alias, is_derived, sql_of_derived_table, sql_of_derived_table_with_alias, original_table, node_id)
	select ut.is_alias, ut.is_derived, ut.sql_of_derived_table, ut.sql_of_derived_table_with_alias, ut1.id, 
				bik_node.id
	from aaa_universe_table ut join aaa_universe u on ut.universe_id = u.id 
			join bik_node on bik_node.obj_id = convert(varchar, ut.their_id) + '|' + convert(varchar, u.global_props_si_id) and bik_node.node_kind_id = @tabele_node_kind_id
			left join aaa_universe_table ut1 on ut.original_table = ut1.their_id and ut.universe_id = ut1.universe_id;
			
	update #tmpTableExtra
	set original_table = bik_node.id
	from #tmpTableExtra join aaa_universe_table ut on #tmpTableExtra.original_table = ut.id join aaa_universe u on ut.universe_id = u.id 
			join bik_node on bik_node.obj_id = convert(varchar, ut.their_id) + '|' + convert(varchar, u.global_props_si_id) 
			and bik_node.node_kind_id = @tabele_node_kind_id
	
	update bik_sapbo_universe_table
	set is_alias = #tmpTableExtra.is_alias, is_derived = #tmpTableExtra.is_derived, sql_of_derived_table = #tmpTableExtra.sql_of_derived_table,
	 sql_of_derived_table_with_alias = #tmpTableExtra.sql_of_derived_table_with_alias, original_table = #tmpTableExtra.original_table, node_id = #tmpTableExtra.node_id
	from #tmpTableExtra join bik_sapbo_universe_table on bik_sapbo_universe_table.node_id = #tmpTableExtra.node_id
	
	insert into bik_sapbo_universe_table(is_alias, is_derived, sql_of_derived_table, sql_of_derived_table_with_alias, original_table, node_id)
	select #tmpTableExtra.is_alias, #tmpTableExtra.is_derived, #tmpTableExtra.sql_of_derived_table, #tmpTableExtra.sql_of_derived_table_with_alias,
			#tmpTableExtra.original_table, #tmpTableExtra.node_id
	from #tmpTableExtra left join bik_sapbo_universe_table on #tmpTableExtra.node_id = bik_sapbo_universe_table.node_id
	where bik_sapbo_universe_table.id is null;
	
	drop table #tmpTableExtra;
	--dla kolumn
	create table #tmpColumnExtra(node_id int, type varchar(155));
	
	insert into #tmpColumnExtra(node_id, type)
	select bn.id, uc.type
	from aaa_universe_table ut join aaa_universe_column uc on ut.id = uc.universe_table_id
							   join aaa_universe u on ut.universe_id = u.id	
							   join bik_node bn on bn.obj_id = convert(varchar, ut.their_id) + '|' + convert(varchar, u.global_props_si_id) + '|' + uc.name
						    and bn.node_kind_id = @column_node_kind_id;
						    
	update bik_sapbo_universe_column
	set bik_sapbo_universe_column.node_id = #tmpColumnExtra.node_id, bik_sapbo_universe_column.type = #tmpColumnExtra.type
	from #tmpColumnExtra join bik_sapbo_universe_column on #tmpColumnExtra.node_id = bik_sapbo_universe_column.node_id
	
	insert into bik_sapbo_universe_column(node_id, type)
	select #tmpColumnExtra.node_id, #tmpColumnExtra.type
	from #tmpColumnExtra left join bik_sapbo_universe_column on #tmpColumnExtra.node_id = bik_sapbo_universe_column.node_id
	where bik_sapbo_universe_column.id is null;
						    
	drop table #tmpColumnExtra;
	
	update bik_sapbo_universe_connection 
	set bik_sapbo_universe_connection.server = aaa_universe_connetion.server, bik_sapbo_universe_connection.user_name = aaa_universe_connetion.user_name,
		bik_sapbo_universe_connection.password = aaa_universe_connetion.password, bik_sapbo_universe_connection.database_source = aaa_universe_connetion.database_source, 
		bik_sapbo_universe_connection.connetion_networklayer_name = aaa_universe_connetion_networklayer.name, bik_sapbo_universe_connection.node_id = bik_node.id
	from aaa_universe_connetion join aaa_universe_connetion_networklayer on aaa_universe_connetion.connetion_networklayer_id = aaa_universe_connetion_networklayer.id
		join bik_node on aaa_universe_connetion.connetion_name = bik_node.name
		join bik_tree on bik_node.tree_id = bik_tree.id
		join bik_sapbo_universe_connection on  bik_sapbo_universe_connection.node_id = bik_node.id
	where bik_tree.code = 'Connections'
	
	insert into bik_sapbo_universe_connection (server, user_name, password, database_source, connetion_networklayer_name, node_id)
	select aaa_universe_connetion.server, aaa_universe_connetion.user_name, aaa_universe_connetion.password, aaa_universe_connetion.database_source, 
		aaa_universe_connetion_networklayer.name, bik_node.id
	from aaa_universe_connetion join aaa_universe_connetion_networklayer on aaa_universe_connetion.connetion_networklayer_id = aaa_universe_connetion_networklayer.id
		join bik_node on aaa_universe_connetion.connetion_name = bik_node.name
		join bik_tree on bik_node.tree_id = bik_tree.id
		left join bik_sapbo_universe_connection on  bik_sapbo_universe_connection.node_id = bik_node.id
	where bik_tree.code = 'Connections' and bik_sapbo_universe_connection.id is null;
				
	exec sp_delete_linked_nodes_where_orignal_is_deleted;
end--end procedure

go
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------


exec sp_update_version '1.0.66', '1.0.67';
go
