/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import com.google.gwt.event.logical.shared.AttachEvent;
import com.google.gwt.event.logical.shared.AttachEvent.Handler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import simplelib.Pair;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author wezyr
 */
public class GUIResizer {

    private Map<IResizeSensitiveWidget, Integer> resizeSensitiveWidgets = new HashMap<IResizeSensitiveWidget, Integer>();
    private int originalWindowClientHeight;
    private Integer mainBodyHeightDelta = null;
    protected Map<Widget, Pair<Integer, Integer>> customWidgets = new HashMap<Widget, Pair<Integer, Integer>>();

    public GUIResizer(int originalWindowClientHeight) {
        this.originalWindowClientHeight = originalWindowClientHeight;
        Window.addResizeHandler(new ResizeHandler() {

            public void onResize(ResizeEvent event) {
                // force recalculate
                mainBodyHeightDelta = null;
                fixResizeSensitiveWidgets();
                fixCustomWidgets();
            }
        });
    }

    protected void fixCustomWidgets() {
        int currentTotalHeight = Window.getClientHeight();

        for (Entry<Widget, Pair<Integer, Integer>> e : customWidgets.entrySet()) {
            Widget w = e.getKey();
            Pair<Integer, Integer> p = e.getValue();
            int oldTotalHeight = p.v1;
            int oldWidgetHeight = p.v2;
            int delta = oldTotalHeight - currentTotalHeight;
            w.setHeight((oldWidgetHeight - delta) + "px");
        }
    }

    public void setResizeSensitiveCustomWidgetHeight(Widget w, int currentHeight) {
        customWidgets.put(w, new Pair<Integer, Integer>(Window.getClientHeight(), currentHeight));
        w.setHeight(currentHeight + "px");
    }

    public void removeCustomWidget(Widget w) {
        customWidgets.remove(w);
    }

    private void addToResizeSensitiveWidgets(IResizeSensitiveWidget rsw,
            int originalHeight) {
        resizeSensitiveWidgets.put(rsw, originalHeight);
        fixOneResizeSensitiveWidget(rsw, originalHeight);
    }

    private int fixBigWidgetHeight(int oldSize) {
        return oldSize + getMainBodyHeightDelta();
    }

    private void fixOneResizeSensitiveWidget(IResizeSensitiveWidget rsw,
            int originalHeight) {
        rsw.setFixedHeight(fixBigWidgetHeight(originalHeight));
    }

    private void fixResizeSensitiveWidgets() {
        for (Entry<IResizeSensitiveWidget, Integer> e : resizeSensitiveWidgets.entrySet()) {
            fixOneResizeSensitiveWidget(e.getKey(), e.getValue());
        }
    }

    public void registerResizeSensitiveWidget(final IResizeSensitiveWidget rsw,
            final int originalHeight) {
        Widget w = rsw.asWidget();
        w.addAttachHandler(new Handler() {

            public void onAttachOrDetach(AttachEvent event) {
                if (event.isAttached()) {
                    addToResizeSensitiveWidgets(rsw, originalHeight);
                } else {
                    resizeSensitiveWidgets.remove(rsw);
                }
            }
        });

        if (w.isAttached()) {
            addToResizeSensitiveWidgets(rsw, originalHeight);
        }
    }

    public void registerResizeSensitiveWidgetByHeight(IsWidget w,
            final int originalHeight) {
        registerResizeSensitiveWidget(new ResizeSensitiveWidgetByHeight(w), originalHeight);
    }

    protected int getMainBodyHeightDelta() {
        if (mainBodyHeightDelta == null) {
            mainBodyHeightDelta = Window.getClientHeight() - originalWindowClientHeight;
        }

        return mainBodyHeightDelta;
//        return //getNewBodyHeight() - oldBodyHeight;
//                (Window.getClientHeight() - originalWindowClientHeight)
//                /*+ (ORIGINAL_MAINBODY_DIV_HEIGHT - oldBodyHeight)*/;
    }
}
