/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.user.client.Window;
import java.util.List;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.fovis.foxygwtcommons.i18n.I18n;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author mgraczkowski
 */
public abstract class FileUploadDialogBase extends BaseActionOrCancelDialog {

    private IParametrizedContinuation<FoxyFileUpload> saveCont;
    private FoxyFileUpload ffu;
    private List<String> allowedExst;
    @Override
    protected void buildMainWidgets() {
        setFfu(new FoxyFileUpload(new IContinuation() {
            public void doIt() {
                if (getFfu().isUploadIsAllowed()) {
                    saveCont.doIt(getFfu());
                } else {
                    Window.alert(I18n.wrongFileExtension.get() /* I18N:  */);
                }
                hideDialog();
            }
        }, false, allowedExst));
        main.add(getFfu());
    }

    @Override
    protected void doAction() {
        //no-op
    }

    @Override
    protected boolean doActionBeforeClose() {
        ffu.performUpload();

        return false;
    }

    public void buildAndShowDialog(IParametrizedContinuation<FoxyFileUpload> saveCont, List<String> allowedExst) {
        this.saveCont = saveCont;
        this.allowedExst = allowedExst;
        super.buildAndShowDialog();
    }

    public IParametrizedContinuation<FoxyFileUpload> getSaveCont() {
        return saveCont;
    }

    public FoxyFileUpload getFfu() {
        return ffu;
    }

    public void setSaveCont(IParametrizedContinuation<FoxyFileUpload> saveCont) {
        this.saveCont = saveCont;
    }

    public void setFfu(FoxyFileUpload ffu) {
        this.ffu = ffu;
    }
}
