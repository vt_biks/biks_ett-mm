/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.mltx;

import pl.fovis.common.BIKConstants;

/**
 *
 * @author lbiegniewski
 */
public class MultixConstants {

    public static final String MLTX_APP_VERSION_MAJOR = "1.16.z";
    public static final String MLTX_APP_VERSION = MLTX_APP_VERSION_MAJOR + BIKConstants.MAJOR_MINOR_VERSION_SEP + BIKConstants.SVN_REVISION;
    public static final String CONFIRMATION_TOKEN = "Confirm";
    public static final String INVITE_TOKEN = "Invite";
    public static final String RESET_TOKEN = "Reset";
    public static final String DB_NAME_PARAM = "dbName";
//    public static final String URL_FOR_PUBLIC_ACCESS = "http://DEFAULT_URL_NOT_SET";
    public static final String MULTIX_PROP_EMAIL_USER = "multixEmailUser";
    public static final String MULTIX_PROP_EMAIL_PASSWORD = "multixEmailPassword";
    public static final String MULTIX_PROP_EMAIL_SMTP_HOST_NAME = "multixEmailSmtpHostName";
    public static final String MULTIX_PROP_EMAIL_SMTP_PORT = "multixEmailSmtpPort";
    public static final String MULTIX_PROP_EMAIL_FROM = "multixEmailFrom";
    public static final String MULTIX_PROP_EMAIL_SSL = "multixEmailSsl";
    public static String APP_PROP_NAME_DATABASE_DEST_PATH = "database_files_destination_path";
    public static final String SUPPORT_EMAIL_ADDRESS = "support@biks.com.pl";
}
