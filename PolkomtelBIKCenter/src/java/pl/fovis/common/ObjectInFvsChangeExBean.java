/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 *
 * @author Wild Wezyr
 */
public class ObjectInFvsChangeExBean extends ObjectInFvsBean implements IsSerializable {

    public String branchIds;
    public String treeCode;
    public String menuPath;
    public String namePath;
    public String iconName;
    public int nodeState;
    public String changedAttrs;
    public String joinedObjIdsAdded;
    public String joinedObjIdsDeleted;
    public int newCommentCnt;
    public int voteValDelta;
    public int voteCntDelta;
    public int dqcTestFailCnt;
    public Integer cutParentId;
    public int isPasted;
}
