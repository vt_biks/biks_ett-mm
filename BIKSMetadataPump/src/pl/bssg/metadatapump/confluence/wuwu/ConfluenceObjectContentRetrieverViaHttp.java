/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.confluence.wuwu;

import commonlib.LameUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethodBase;
import org.apache.commons.httpclient.URI;
import org.apache.commons.httpclient.methods.GetMethod;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author pmielanczuk
 */
public class ConfluenceObjectContentRetrieverViaHttp implements IConfluenceObjectContentRetriever {

    private static ILameLogger logger = LameUtils.getMyLogger();
    public static String TABLE_WRAP = "class=\"table-wrap\"";
    public static String baseUrl_def = "http://192.168.167.54:8090/";
    public static String userName_def = "admin";
    public static String password_def = "1qaz2wsx#EDC";
    public static final String userAgent = "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36";
    protected HttpClient cli = new HttpClient();
    // musi się kończyć znakiem ukośnika ("/")
    protected String baseUrl;
    protected String userName;
    protected String password;
    protected boolean alreadyLoggedIn;

    public ConfluenceObjectContentRetrieverViaHttp(String baseUrl, String userName, String password) {
        this.baseUrl = baseUrl;
        this.userName = userName;
        this.password = password;
    }

    protected boolean needsLogin(String responseTxt) {
        return responseTxt.contains("id=\"os_username\"");
    }

    public static String encodeForUrl(String urlPart) {
        try {
            return URLEncoder.encode(urlPart, "utf-8");
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        }
    }

    protected void performLogin() {
        try {
            alreadyLoggedIn = false;
            HttpMethodBase m = new GetMethod();
            String url = baseUrl + "dologin.action?os_username="
                    + encodeForUrl(userName) + "&os_password=" + encodeForUrl(password);
//            System.out.println("login url: " + url);
            m.setRequestHeader("User-Agent", userAgent);
            m.setURI(new URI(url, true));
            cli.executeMethod(m);
            String resp = m.getResponseBodyAsString();
            if (needsLogin(resp)) {
                System.out.println("cannot log in, html:\n" + resp);
                throw new RuntimeException("error in login, still unlogged :-(");
            }
            alreadyLoggedIn = true;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public String getHtml(long objId) {
        return getHtmlInner(objId, true);
    }

    protected String getHtmlInner(long objId, boolean useViewSource) {
        if (!alreadyLoggedIn) {
            performLogin();
        }

        int iter = 0;
        while (iter < 2) {
            if (iter > 0) {
                performLogin();
            }
            try {
                HttpMethodBase m = new GetMethod();
                m.setRequestHeader("User-Agent", userAgent);
                m.setURI(new URI(baseUrl
                        + (useViewSource
                                ? "plugins/viewsource/viewpagesrc.action?pageId="
                                : "pages/viewpage.action?pageId=")
                        + objId, true));
                cli.executeMethod(m);
                String resp = m.getResponseBodyAsString();

                if (!needsLogin(resp)) {
                    return resp;
                }
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
            iter++;
        }

        throw new RuntimeException("cannot read text");
    }

    @Override
    public String getAbsoluteUrlBase() {
        return baseUrl;
    }
//    public static void main(String[] args) throws Exception {
//        ConfluenceObjectContentRetrieverViaHttp c = new ConfluenceObjectContentRetrieverViaHttp(baseUrl_def, userName_def, password_def);
//        System.out.println(c.getHtml(557120L));
//    }
    private static final String pageNotFoundHtml = "<html><body>page not found</body></html>";

    // jak nie znajdzie - zwraca nulla
    protected static Pair<Integer, Integer> findNextAttachmentSummaryToggle(String htmlFull, int pos) {

        int insideTdPos = -1;
        while (pos < htmlFull.length()) {
            final String attachmentsummarytoggle = "attachment-summary-toggle";
            insideTdPos = htmlFull.indexOf(attachmentsummarytoggle, pos);
            if (insideTdPos < 0) {
                return null;
            }

            int tdStart = htmlFull.lastIndexOf("<", insideTdPos);
            if (tdStart < 0) {
                return null;
            }
            if (htmlFull.startsWith("<td", tdStart)) {
                break;
            }
            pos = insideTdPos + attachmentsummarytoggle.length();
        }

        int spanStart = htmlFull.indexOf("<span", insideTdPos);
        if (spanStart < 0) {
            return null;
        }
        int tdEnd = htmlFull.indexOf("</td", spanStart);
        if (tdEnd < 0) {
            return null;
        }
        int spanEnd = htmlFull.lastIndexOf(">", tdEnd);
        if (spanEnd < 0) {
            return null;
        }

        return new Pair<Integer, Integer>(spanStart, spanEnd + 1);
    }

    protected static String fixCommentsHtml(String commentsHtml) {
        int pos = 0;
        int chLen = commentsHtml.length();

        StringBuilder sb = new StringBuilder();

        while (pos < chLen) {
            int commActDivInsidePos = commentsHtml.indexOf("class=\"comment-actions\"", pos);
            if (commActDivInsidePos < 0) {
                break;
            }
            int commActDivStartPos = commentsHtml.lastIndexOf("<div", commActDivInsidePos);
            final String divCloseStr = "</div>";
            int commActDivClosePos = commentsHtml.indexOf(divCloseStr, commActDivInsidePos);
            if (commActDivStartPos < 0 || commActDivStartPos < 0) {
                break;
            }
            sb.append(commentsHtml, pos, commActDivStartPos);
            pos = commActDivClosePos + divCloseStr.length();
        }

        sb.append(commentsHtml, pos, chLen);
        return sb.toString();
    }

    public static String innerExtractWikiContentFromFullHtml(String htmlFull, Long optObjId) {
        int bodyPos = htmlFull.indexOf("<body");
        int afterBodyPos = htmlFull.indexOf(">", bodyPos) + 1;
        int bodyClosePos = htmlFull.indexOf("</body");

        if (afterBodyPos < 0 || bodyClosePos < 0) {
            return "err: no body start or close. Object ID = " + optObjId;
        }

        int wikiContentInsideDivPos = htmlFull.indexOf("class=\"wiki-content\"");
        if (wikiContentInsideDivPos < 0) {
            return "err: no class=\"wiki-content\". Object ID = " + optObjId;
        }

        int wikiContentDivPos = htmlFull.lastIndexOf("<div", wikiContentInsideDivPos);
        int afterWikiClosePos = htmlFull.indexOf("<rdf:RDF");

        if (wikiContentDivPos < 0 || afterWikiClosePos < 0) {
            return "err: no close pos after wiki-content. Object ID = " + optObjId;
        }

        int labelsSectionInsideDivPos = htmlFull.indexOf("id=\"labels-section\"", wikiContentInsideDivPos);
        if (labelsSectionInsideDivPos >= 0) {
            int labelsSectionDivPos = htmlFull.lastIndexOf("<div", labelsSectionInsideDivPos);
            if (labelsSectionDivPos < afterWikiClosePos) {
                afterWikiClosePos = labelsSectionDivPos;
            }
        }

        int commentsSectionInsideDivPos = htmlFull.indexOf("id=\"comments-section\"", wikiContentInsideDivPos);
        int afterCommentsClosePos = -1;
        int commentsSectionDivPos = -1;
        if (commentsSectionInsideDivPos >= 0) {
            commentsSectionDivPos = htmlFull.lastIndexOf("<div", commentsSectionInsideDivPos);
            if (commentsSectionDivPos < afterWikiClosePos) {
                afterWikiClosePos = commentsSectionDivPos;
            }

            // <div class="bottom-comment-panels comment-panels">
            int commentsBottomInsidePos = htmlFull.indexOf("bottom-comment-panels", commentsSectionInsideDivPos);
            if (commentsBottomInsidePos >= 0) {
                afterCommentsClosePos = htmlFull.lastIndexOf("<div", commentsBottomInsidePos);
            }
        }

        int wikiContentClosePos = htmlFull.lastIndexOf("</div", afterWikiClosePos);

        if (wikiContentClosePos < 0) {
            return "err: no close of div wiki-content. Object ID = " + optObjId;
        }

        String metaDataStr = "";
        int metaDataInsidePos = htmlFull.indexOf("class=\"page-metadata\"", bodyPos);
        if (metaDataInsidePos >= 0) {
            int metaDataDivStartPos = htmlFull.lastIndexOf("<div", metaDataInsidePos);
            if (metaDataDivStartPos >= 0) {
                metaDataStr = htmlFull.substring(metaDataDivStartPos, wikiContentDivPos);
            }
        }

        String wikiContentStr = htmlFull.substring(wikiContentDivPos, wikiContentClosePos);

        StringBuilder sb = new StringBuilder();
        int pos = 0;
        int wcLen = wikiContentStr.length();

        while (pos < wcLen) {
            Pair<Integer, Integer> p = findNextAttachmentSummaryToggle(wikiContentStr, pos);
            if (p == null) {
                break;
            }
            sb.append(wikiContentStr, pos, p.v1);
            pos = p.v2;
        }
        sb.append(wikiContentStr, pos, wcLen);
        wikiContentStr = sb.toString();

        //attachments-table-drop-zone
        int attTDZPos = wikiContentStr.indexOf("plugin_attachments_upload_container");
        if (attTDZPos >= 0) {
            int attTDZEndPos = wikiContentStr.indexOf("<div", attTDZPos);
            int formInsideEndPos = wikiContentStr.indexOf("</form", attTDZPos);
            if (formInsideEndPos >= 0 && attTDZEndPos >= 0) {
                int closingPos = wikiContentStr.indexOf("</div", formInsideEndPos);
                if (closingPos >= 0) {
                    wikiContentStr = wikiContentStr.substring(0, attTDZEndPos)
                            + wikiContentStr.substring(closingPos);
//                        System.out.println("attachments drop zone removed");
                } else {
                    System.out.println("attachments drop zone - sth is wrong! #1 Object ID = " + optObjId);
                }
            } else {
                // to jest możliwe - brak formatki, nawet wewn. diva może nie być...
//                System.out.println("attachments drop zone - sth is wrong! #2 Object ID = " + optObjId);
            }
        } else {
//                System.out.println("no attachments!");
        }

        String commentsHtml = afterCommentsClosePos >= 0 ? fixCommentsHtml(htmlFull.substring(commentsSectionDivPos, afterCommentsClosePos)
                + "</div>") : "";

        String res = htmlFull.substring(0, afterBodyPos) + "<div id=\"main\" class=\"aui-page-panel\" style=\"border-bottom: 0px; min-height: 20px\">"
                + "<div id=\"content\" class=\"blogpost view\">"
                + metaDataStr
                + wikiContentStr
                + "</div>"
                + commentsHtml
                + "</div>" //---- ten jest zamiast tego za  wikiContentStr
                + "</div>"
                + htmlFull.substring(bodyClosePos);

        //kasowac wszystkie klasy class="table-wrap"
        int start;
        do {
            start = res.indexOf(TABLE_WRAP);
            if (start >= 0) {
                res = res.substring(0, start - 1) + res.substring(start + TABLE_WRAP.length());
            }
        } while (start >= 0);//        System.out.println("innerExtractWikiContentFromFullHtml:\n" + res);
        return res;
    }

    public static String extractWikiContentFromFullHtml(String htmlFull, Long optObjId) {
        try {
            String res = innerExtractWikiContentFromFullHtml(htmlFull, optObjId);
            if (res == null || res.startsWith("err:")) {
                throw new RuntimeException("Got error! " + res);
            }
            return res;
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("Page not found", ex);
            }
            return pageNotFoundHtml;
        }
    }

    @Override
    public String getHtmlFull(long objId) {
        return getHtmlInner(objId, false);
    }
//    public static void main(String[] args) {
//        System.out.println("<aqq>>".lastIndexOf(">", 5));
//    }
}
