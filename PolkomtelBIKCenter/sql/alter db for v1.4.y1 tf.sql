﻿exec sp_check_version '1.4.y1';
go

if not exists(select * from sys.columns where Name = N'database_filter' and Object_ID = Object_ID(N'bik_db_server'))    
begin
   alter table bik_db_server add database_filter varchar(max) null;
end
go

if not exists(select * from sys.columns where Name = N'object_filter' and Object_ID = Object_ID(N'bik_db_server'))    
begin
	alter table bik_db_server add object_filter varchar(max) null;
end
go


exec sp_update_version '1.4.y1', '1.4.y1.1';
go