/*
 * FlexiRuntimeException.java
 *
 * Created on 21 grudzie� 2006, 09:36
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
 
package pl.trzy0.foxy.commons;

import simplelib.LameRuntimeException;

/**
 *
 * @author wezyr
 */
public class FoxyRuntimeException extends LameRuntimeException {
    
    public FoxyRuntimeException(Throwable cause) {
        super(cause);
    }
    
    public FoxyRuntimeException(String errMsg) {
        super(errMsg);
    }
    
    public FoxyRuntimeException(String errMsg, Throwable cause) {
        super(errMsg, cause);
        //super(cause instanceof FoxyRuntimeException ? errMsg + "\nCause msg:" + ((FoxyRuntimeException)cause).getMessage() : errMsg,
        //        cause instanceof FoxyRuntimeException ? cause.getCause() : cause  );
        
        //System.out.println("*** FlexiRuntimeException: " + errMsg + ", cause: " + cause);
        //System.out.println("***   getCause(): " + getCause());
        //System.out.flush();
    }
}
