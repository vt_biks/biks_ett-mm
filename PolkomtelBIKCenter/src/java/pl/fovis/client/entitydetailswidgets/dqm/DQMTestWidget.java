/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.dqm;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.entitydetailswidgets.EntityDetailsWidgetFlatBase;
import pl.fovis.client.entitydetailswidgets.IMainHeaderSharedTable;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.dqm.DQMMultiSourceBean;
import pl.fovis.common.dqm.DataQualityTestBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.dialogs.SimpleInfoDialog;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.SimpleDateUtils;

/**
 *
 * @author tflorczak
 */
public class DQMTestWidget extends EntityDetailsWidgetFlatBase {

    protected IMainHeaderSharedTable imhst;

    public DQMTestWidget(IMainHeaderSharedTable imhst) {
        this.imhst = imhst;
    }

    @Override
    public void displayData() {
        DataQualityTestBean extradata = getData().dataQualityTestExtradata;
        if (extradata != null) {
//            imhst.addTranslatedRow("Nazwa biznesowa" /* I18N: no */, extradata.longName);
            imhst.addTranslatedRow("Opis testu" /* I18N: no */, extradata.description);
            imhst.addTranslatedRow("Definicja testu" /* I18N: no */, extradata.benchmarkDefinition);
            imhst.addTranslatedRow("Częstotliwość uruchamiania testu" /* i18n: no */, extradata.samplingMethod);
            imhst.addTranslatedRow("Sposób uruchamiania testu" /* I18N: no */, extradata.additionalInformation);
            String errorThresholdAfterFix = "";
            try {
                errorThresholdAfterFix = BaseUtils.doubleToString(extradata.errorThreshold * 100, 2) + " %";
            } catch (Exception e) {
                errorThresholdAfterFix = "0 %";
            }
            imhst.addTranslatedRow("Próg błędu" /* I18N: no */, errorThresholdAfterFix);
            boolean isMultiSource = !BaseUtils.isCollectionEmpty(extradata.multiSources);
            imhst.addTranslatedRow(I18n.typTestu.get(), isMultiSource ? I18n.testWielozrodlowy.get() : I18n.testJednozrodlowy.get());

            boolean useDqmConnectionMode = BIKClientSingletons.useDqmConnectionMode();
            if (isMultiSource) { // test wielozrodlowy
//                imhst.addTranslatedRow("Źródło" /* I18N: no */, "MS SQL");
//                imhst.addTranslatedRow("System" /* I18N: no */, "BIKS");
                imhst.addTranslatedRow(I18n.zrodla.get(), useDqmConnectionMode ? createManySourcesWidgetForConnections(extradata.multiSources) : createManySourcesWidgetForDatabases(extradata.multiSources));
            } else { // test jednoźródłowy
                if (!BaseUtils.isStrEmptyOrWhiteSpace(extradata.dataScope)) {
                    imhst.addTranslatedRow("Zakres danych" /* I18N: no */, null, createInfoButton(extradata.dataScope, I18n.pokazZakresDanych.get(), I18n.zakresDanych.get()));
                }

                if (!useDqmConnectionMode) {
                    imhst.addTranslatedRow("Źródło" /* I18N: no */, extradata.sourceName);

                    imhst.addTranslatedRow("System" /* I18N: no */, extradata.serverName);
                    imhst.addTranslatedRow("Baza danych" /* I18N: no */, extradata.databaseName);
                    imhst.addTranslatedRow("Prefiks nazwy pliku na SFTP" /* I18N: no */, extradata.resultFileName);
                } else {
                    imhst.addTranslatedRow("Połączenie" /* I18N: no */, extradata.connectionName);
                }
            }
            imhst.addTranslatedRow("Funkcja testująca" /* I18N: no */, extradata.procedureName);
            imhst.addTranslatedRow("Funkcja zwracająca błędne dane" /* I18N: no */, extradata.errorProcedureName);
            if (!BaseUtils.isStrEmptyOrWhiteSpace(extradata.sqlText)) {
                imhst.addTranslatedRow("Zapytanie testujące" /* I18N: no */, null, createLoopButton(extradata.sqlText, I18n.pokazSQL.get(), "SQL"));
            }
            if (!BaseUtils.isStrEmptyOrWhiteSpace(extradata.errorSqlText)) {
                imhst.addTranslatedRow("Zapytanie zwracające błędne dane" /* I18N: no */, null, createLoopButton(extradata.errorSqlText, I18n.pokazSQL.get(), "SQL"));
            }
            imhst.addTranslatedRow("Data utworzenia" /* I18N: no */, SimpleDateUtils.toCanonicalString(extradata.createDate));
            imhst.addTranslatedRow("Data modyfikacji" /* I18N: no */, SimpleDateUtils.toCanonicalString(extradata.modifyDate));
//            imhst.addTranslatedRow("Data aktywacji" /* I18N: no */, SimpleDateUtils.toCanonicalString(extradata.activateDate));
//            imhst.addTranslatedRow("Data dezaktywacji" /* I18N: no */, SimpleDateUtils.toCanonicalString(extradata.deactivateDate));
        }
    }

    @Override
    public Widget buildWidgets() {
        return new VerticalPanel();
    }

    @Override
    public boolean hasContent() {
        EntityDetailsDataBean data = getData();
        return data.dataQualityTestExtradata != null && data.node.treeCode.equals(BIKConstants.TREE_CODE_DQM);
    }

    protected HTML createLoopButton(final String content, String text, final String dialogTitle) {
        return new BikCustomButton(text, "lookupTrashBtn",
                new IContinuation() {
                    @Override
                    public void doIt() {
                        getForms().viewSQLDef(dialogTitle, content);;
                    }
                });
    }

    protected HTML createInfoButton(final String content, final String text, final String dialogTitle) {
        return new BikCustomButton(text, "lookupTrashBtn",
                new IContinuation() {
                    @Override
                    public void doIt() {
                        SimpleInfoDialog dialog = new SimpleInfoDialog() {
                            @Override
                            protected String getDefCloseBtnCaption() {
                                return I18n.zamknij.get();
                            }
                        };
                        dialog.buildAndShowDialog(BaseUtils.encodeForHTMLWithNewLinesAsBRs(content), dialogTitle, null);
                    }
                });
    }

    protected Widget createManySourcesWidgetForConnections(List<DQMMultiSourceBean> multiSources) {
        FlexTable sourceTable = new FlexTable();
        // create header
        sourceTable.setHTML(0, 0, "<b>" + I18n.dqmPolaczenie.get() + "</b>");
        sourceTable.setHTML(0, 1, "<b>" + I18n.zapytaniePobierajaceDane.get() + "</b>");
        sourceTable.setHTML(0, 2, "<b>" + I18n.zakresDanych.get() + "</b>");
        // width
        sourceTable.setWidth("100%");
        sourceTable.getColumnFormatter().setWidth(0, "57%");
        sourceTable.getColumnFormatter().setWidth(1, "23%");
        sourceTable.getColumnFormatter().setWidth(2, "20%");
        // fill with data
        int row = 1;
        for (DQMMultiSourceBean oneSource : multiSources) {
            sourceTable.setHTML(row, 0, oneSource.connectionName);
            sourceTable.setWidget(row, 1, BaseUtils.isAnyStringEmptyOrWhiteSpace(oneSource.sqlTest) ? new HTML("") : createLoopButton(oneSource.sqlTest, I18n.pokazSQL.get(), "SQL"));
            sourceTable.setWidget(row, 2, BaseUtils.isAnyStringEmptyOrWhiteSpace(oneSource.dataScope) ? new HTML("") : createInfoButton(oneSource.dataScope, I18n.pokazZakresDanych.get(), I18n.zakresDanych.get() + " - " + oneSource.serverName + " (" + oneSource.sourceName.trim() + ")"));
            row++;
        }
        return sourceTable;
    }

    protected Widget createManySourcesWidgetForDatabases(List<DQMMultiSourceBean> multiSources) {
        FlexTable sourceTable = new FlexTable();
        // create header
        sourceTable.setHTML(0, 0, "<b>" + I18n.konektor.get() + "</b>");
        sourceTable.setHTML(0, 1, "<b>" + I18n.system.get() + "</b>");
        sourceTable.setHTML(0, 2, "<b>" + I18n.bazaDanychLubPlik.get() + "</b>");
        sourceTable.setHTML(0, 3, "<b>" + I18n.zapytaniePobierajaceDane.get() + "</b>");
        sourceTable.setHTML(0, 4, "<b>" + I18n.zakresDanych.get() + "</b>");
        // width
        sourceTable.setWidth("100%");
        sourceTable.getColumnFormatter().setWidth(0, "12%");
        sourceTable.getColumnFormatter().setWidth(1, "20%");
        sourceTable.getColumnFormatter().setWidth(2, "25%");
        sourceTable.getColumnFormatter().setWidth(3, "23%");
        sourceTable.getColumnFormatter().setWidth(4, "20%");
        // fill with data
        int row = 1;
        for (DQMMultiSourceBean oneSource : multiSources) {
            sourceTable.setHTML(row, 0, oneSource.sourceName);
            sourceTable.setHTML(row, 1, oneSource.serverName);
            sourceTable.setHTML(row, 2, !BaseUtils.isStrEmptyOrWhiteSpace(oneSource.databaseName) ? oneSource.databaseName : oneSource.resultFileName);
            sourceTable.setWidget(row, 3, BaseUtils.isAnyStringEmptyOrWhiteSpace(oneSource.sqlTest) ? new HTML("") : createLoopButton(oneSource.sqlTest, I18n.pokazSQL.get(), "SQL"));
            sourceTable.setWidget(row, 4, BaseUtils.isAnyStringEmptyOrWhiteSpace(oneSource.dataScope) ? new HTML("") : createInfoButton(oneSource.dataScope, I18n.pokazZakresDanych.get(), I18n.zakresDanych.get() + " - " + oneSource.serverName + " (" + oneSource.sourceName.trim() + ")"));
            row++;
        }
        return sourceTable;
    }
}
