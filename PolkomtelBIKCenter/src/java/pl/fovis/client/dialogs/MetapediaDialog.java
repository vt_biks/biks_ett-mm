/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import pl.fovis.client.bikwidgets.MyRichTextAreaFoxyToolbar;
import pl.fovis.common.ArticleBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.richtexteditor.FoxyRichTextArea;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author młydkowski
 */
public class MetapediaDialog extends BaseRichTextAndTextboxDialog/*BaseActionOrCancelDialog*/ {

    protected Label lblTags;
    protected TextBox tbTytul;
    protected ListBox listBox;
    private IParametrizedContinuation<Pair<ArticleBean, Integer>> saveCont;
    private FlowPanel checkBoxContainer;
    private ArticleBean record;
    protected Set<String> tags;
    protected HorizontalPanel radioButtonHp;
    protected String caption = I18n.dodawaniePojeciaBiznesowego.get() /* I18N:  */;
    protected List<CheckBox> checkboxes = new ArrayList<CheckBox>();
    private Label lblDef;
    protected TextBox difVersion;
    protected RadioButton versionCorp;
    protected RadioButton versionWork;
    protected RadioButton versionDif;
    protected LinkedHashMap<String, Integer> defAndVersion;
    //w:close z nazwy nie wynika czy true oznacza Add czy true oznacza Close...
    // nazwa powinna to wyjaśniać, np. isAddMode albo isEditMode - i wiadomo
    protected boolean isNewDefinition;
    //ww->close: to pole jest używane tylko wewnątrz
    //protected String textalert;

    @Override
    protected String getDialogCaption() {
        return caption;
    }

    @Override
    protected void buildMainWidgets() {
        Grid mainGrid = new Grid(3, 2);
        mainGrid.setCellSpacing(3);

        checkboxes = new ArrayList<CheckBox>();
        radioButtonHp = new HorizontalPanel();
        radioButtonHp.setSpacing(4);
        radioButtonHp.setStyleName("radioBtnH");
        difVersion = new TextBox();
        difVersion.setStyleName("dfiVersionTb");
        lblTitle = new Label(I18n.tytul.get() /* I18N:  */ + ":");
        lblTitle.setStyleName("metaTitleLbl");
        tbTytul = new TextBox();
        tbTytul.setStyleName("metaTbTytul");
        tbTytul.setWidth("471px");
        mainGrid.setWidget(0, 0, lblTitle);
        mainGrid.setWidget(0, 1, tbTytul);
        radioButtonHp.add(new Label(I18n.wersja.get() /* I18N:  */));
        versionCorp = new RadioButton("group" /* I18N: no */, I18n.korporacyjna.get() /* I18N:  */);
        versionWork = new RadioButton("group" /* I18N: no */, I18n.robocza.get() /* I18N:  */);
        versionDif = new RadioButton("group" /* I18N: no */, I18n.inna.get() /* I18N:  */ + ":");
        versionCorp.setValue(true);
        radioButtonHp.add(versionCorp);
        radioButtonHp.add(versionWork);
        radioButtonHp.add(versionDif);
        radioButtonHp.add(difVersion);
        FoxyRichTextArea area = getRichTextArea();
        area.setSize("900px", "25em");
//        area.setSize("100%", "14em");
        HorizontalPanel hp = new HorizontalPanel();
        toolbar = new MyRichTextAreaFoxyToolbar(area/*
         * , record.nodeId
         */);
        toolbar.setStyleName("richTextAreaToolbar");
        hp.setStyleName("richTextAreaP");
        hp.add(toolbar);
        grid = new Grid(2, 1);
        grid.setWidget(0, 0, hp/*
         * toolbar
         */);
        grid.setWidget(1, 0, area);

        FlowPanel decPanel = new FlowPanel();
        decPanel.add(grid);
        decPanel.setStyleName("richTextArea");

        if (record != null) {
            area.setHTML(record.body);
            tbTytul.setText(record.subject);
            if (record.subject != null && record.official == 0 || !isNewDefinition) {
                tbTytul.setReadOnly(true);
                tbTytul.setEnabled(false);
            }

            if (record.official == 1) {
                versionCorp.setValue(true);

            } else if (record.official == 0) {
                versionWork.setValue(true);

            } else {
                versionDif.setValue(true);
            }
            if (record.versions != null) {
                difVersion.setText(record.versions.replace(I18n.robocza.get() /* I18N:  */, ""));
            }

            if (record.tags != null) {
                tags = new LinkedHashSet<String>(record.tags);
            }
        } else {
            tags = new LinkedHashSet<String>();
        }
        mainGrid.setWidget(1, 1, radioButtonHp);
        mainGrid.setWidget(2, 0, lblDef = new Label(I18n.definicja.get() /* I18N:  */ + ":"));
        lblDef.setStyleName("metaDefLbl");
        mainGrid.setWidget(2, 1, decPanel);
        clearCheckboxes();
        checkBoxContainer = new FlowPanel();
        finalizeCheckboxes();
        main.add(mainGrid);
    }

    protected void setDataArticle(int action, String body) {
        ArticleBean art = new ArticleBean();
        art.body =/* deleteBadURL(*/ body/*)*/;
//        art.tags = tabWidget.getKeywords();
        art.subject = tbTytul.getText().trim();
        if (versionCorp.getValue()) {
            art.official = 1;
        }
        if (versionWork.getValue()) {
            art.official = 0;
        }
        if (versionDif.getValue()) {
            art.official = 2;
            art.versions = difVersion.getText().trim();
        } else {
            art.versions = "";
        }

        art.id = (record != null) ? record.id : 0;
        art.nodeId = (record != null) ? record.nodeId : null;

        saveCont.doIt(new Pair<ArticleBean, Integer>(art, action));
    }
//wiem ze te ify sa brzydkie i ten parametr w setDataArticle

    protected void innerDoAction(String title, String body) {
        String subjectWithVersion = tbTytul.getText().trim();
        boolean vWork = versionWork.getValue();
        boolean vDif = versionDif.getValue();
        boolean vCorp = versionCorp.getValue();
        int official = record.official;

        if (vWork) {
            subjectWithVersion = subjectWithVersion + " (" + I18n.robocza.get() /* I18N:  */ + ")";
        }
        if (vDif) {
            subjectWithVersion = subjectWithVersion + " (" + difVersion.getText().trim() + ")";
        }
        subjectWithVersion = subjectWithVersion.toLowerCase();
        int action = 0;
        if (!isNewDefinition) {//Edycja wersji
            if (vDif && !record.versions.equals(difVersion.getText()) && (official != 1)) {
                action = 3;                          //gdy zmieni sie nazwa wersji
            } else if ((vCorp && official != 1) || (vWork && official != 0) || (vDif && official != 2)) {
                if (vCorp) {                        //zmiana z dowolnej wersji na korporacyjna
                    action = 4;
                } else if (official != 1) {         // zmiana z dowolnej wersji na dowolną (nie korporacyjną)
                    action = 3;
                } else {                            //zmiana z korporacyjnej na dowolną (przez zastąpienie starej lub na nową)
                    action = defAndVersion.containsKey(subjectWithVersion) ? 5 : 6;//zastap:nowa
                }
            } else {
                action = 1;
            }
        } else { //Dodawanie nowych wersji
            String subject = tbTytul.getText().toLowerCase().trim();
            if ((vCorp && defAndVersion.containsKey(subject)) || ((vDif || vWork) && defAndVersion.containsKey(subjectWithVersion))) {
                action = 1;//dodanie nowej przez zastapienie starej
            } else if ((vDif || vWork) && defAndVersion.containsKey(subject)) {
                action = 2; //dodanie nowej pod korpo
            } else {//if ((!keywords.contains(subject)) || !keywords.contains(tbTytul.getText())) {
                action = 0;//dodanie nowej
            }
        }
        setDataArticle(action, body);
//add: 0-nowa,1- istnieje,2 -pod korpo
//edit: 3- z kazdej innej na inna 4-z innej na korpo //5-z korpo na zastap 6-z korpo na nowa
    }

    @Override
    protected boolean doActionBeforeClose() {
        String subjectWithVersion = tbTytul.getText().trim();
        boolean vWork = versionWork.getValue();
        boolean vDif = versionDif.getValue();
        boolean vCorp = versionCorp.getValue();
        int official = record.official;
        String alertText = null;
        if (vWork) {
            subjectWithVersion = subjectWithVersion + " (" + I18n.robocza.get() /* I18N:  */ + ")";
        }
        if (vDif) {
            if (difVersion.getText().isEmpty()) {
                alertText = I18n.uzupelnijWersjeDefinicji.get() /* I18N:  */;
                Window.alert(alertText);
                return false;
            }
            subjectWithVersion = subjectWithVersion + " (" + difVersion.getText().trim() + ")";
        }

        if (tbTytul.getText().isEmpty()) {
            alertText = I18n.poleTytulNieMozeBycPuste.get() /* I18N:  */;
            Window.alert(alertText);
            return false;
        } else if (!isNewDefinition) {//Przy edycji
            if ((vCorp && official != 1) || (vWork && official != 0) || (vDif && official != 2)) {
                if (vCorp) {                        //zmiana z dowolnej wersji na korporacyjna
                    alertText = I18n.wersjaKorporacZostanieZastapio.get() /* I18N:  */;
                } else if (official != 1) {         // zmiana z dowolnej wersji na dowolną (nie korporacyjną)
                    alertText = I18n.definicja.get() /* I18N:  */ + " " + subjectWithVersion + " " + I18n.zostanieZastapiona.get() /* I18N:  */;
                } else {                            //zmiana z korporacyjnej na dowolną (przez zastąpienie starej lub na nową)
                    alertText = I18n.definicjKorporacZostanieZamienio.get() /* I18N:  */ + " "
                            + (defAndVersion.containsKey(subjectWithVersion.toLowerCase()) ? "(" + I18n.istniejaca.get() /* I18N:  */ + ")" : "(" + I18n.nowa.get() /* I18N:  */ + ")");
                }
            }
        } else { //Dodawanie nowych
            String tytul = tbTytul.getText().toLowerCase().trim();
            if ((vCorp && defAndVersion.containsKey(tytul)) || ((vDif || vWork) && defAndVersion.containsKey(subjectWithVersion.toLowerCase()))) {
                alertText = I18n.definicja.get() /* I18N:  */ + " " + subjectWithVersion + " " + I18n.juzIstniejeCzyZastapic.get() /* I18N:  */ + "? ";
            } else if ((vDif || vWork) && defAndVersion.containsKey(tytul)) {
                alertText = I18n.definicjZostanieDodanaPodPojecie.get() /* I18N:  */ + ": " + tbTytul.getText();
            }
        }
        if (alertText != null) {
            new DefDialog().buildAndShowDialog(alertText, new IParametrizedContinuation<Boolean>() {
                public void doIt(Boolean param) {
                    if (param) {
                        doAction();
                        hideDialog();
                    }
                }
            });
//            doAction();
            return false;
        }

        return true;
    }

    public void buildAndShowDialog(String caption, ArticleBean art,
            LinkedHashMap<String, Integer> defAndVersion, boolean isNewDefiniction,
            IParametrizedContinuation<Pair<ArticleBean, Integer>> saveCont //            IParametrizedContinuation<Pair<ArticleBean, Set<String>>> saveCont
    ) {
        this.caption = caption;
        this.record = art;
        this.saveCont = saveCont;
        this.defAndVersion = defAndVersion;
        this.isNewDefinition = isNewDefiniction;
        super.buildAndShowDialog();
    }

    public void setSubjectDisabled() {
        tbTytul.setEnabled(false);
    }

    protected void addCheckbox(String name, String caption, boolean active, final IContinuation contWhenChanged) {
        final CheckBox cb = new CheckBox(caption, true);
        cb.setStyleName("Filters-Checkbox");
        cb.setValue(active);
        cb.setName(name);
        cb.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent ce) {
                contWhenChanged.doIt();
            }
        });
        checkboxes.add(cb);
    }

    protected void finalizeCheckboxes() {
        checkBoxContainer.clear();

        for (CheckBox cb : checkboxes) {
            checkBoxContainer.add(cb);
        }
    }

    protected void clearCheckboxes() {
        checkboxes.clear();
    }
}
