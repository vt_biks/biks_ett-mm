/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.List;

/**
 *
 * @author pmielanczuk
 */
public class DynamicTreesBigBean implements IsSerializable {

    //public List<TreeBean> dynamicTrees;
    public List<TreeBean> allTrees;
    public List<MenuNodeBean> menuNodes;
}
