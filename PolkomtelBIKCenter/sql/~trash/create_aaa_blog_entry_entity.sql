USE [boxi]
GO

/****** Object:  Table [dbo].[aaa_blog_entry_entity]    Script Date: 03/11/2011 13:33:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[aaa_blog_entry_entity](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[si_id] [bigint] NOT NULL,
	[blog_entity_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uniqe] UNIQUE NONCLUSTERED 
(
	[blog_entity_id] ASC,
	[si_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[aaa_blog_entry_entity]  WITH CHECK ADD FOREIGN KEY([blog_entity_id])
REFERENCES [dbo].[aaa_blog_entry] ([id])
GO


