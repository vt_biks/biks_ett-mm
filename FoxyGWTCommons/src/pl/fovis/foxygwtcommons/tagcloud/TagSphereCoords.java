/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.tagcloud;

import com.google.gwt.user.client.ui.Label;
import pl.fovis.foxygwtcommons.i18n.I18n;
import simplelib.Tuple3;

/**
 *
 * @author wezyr
 */
// i18n-default: no
public class TagSphereCoords {

    public int tagCount;
    public double radius;
    public Double[] axes;
    public Double[] ayes;
    public Double[] azes;
    public double offsetAx;
    public double offsetAz;
    public double lastShowOffsetAx;
    public double lastShowOffsetAz;
    private boolean useDistribution;
    private Label debugLabel;

    public TagSphereCoords(int tagCount, double radius,
            boolean useDistribution, Label debugLabel) {
        this.tagCount = tagCount;
        this.radius = radius;
        this.useDistribution = useDistribution;
        this.debugLabel = debugLabel;
        init();
    }
    private Tuple3<Integer, Integer, Double>[] lastPoints;

    @SuppressWarnings("unchecked")
    private void initLastPoints() {
        lastPoints = new Tuple3[tagCount];
    }

    private void init() {
        axes = new Double[tagCount];
        ayes = new Double[tagCount];
        azes = new Double[tagCount];
        for (int i = 0; i < tagCount; i++) {
            double phi;
            double theta;
            if (useDistribution) {
                phi = Math.acos(-1.0 + (2.0 * i) / tagCount);
                theta = Math.sqrt(tagCount * Math.PI) * phi;
            } else {
                phi = Math.random() * Math.PI;
                theta = Math.random() * 2 * Math.PI;
            }

            axes[i] = radius * Math.cos(theta) * Math.sin(phi);
            ayes[i] = radius * Math.sin(theta) * Math.sin(phi);
            azes[i] = radius * Math.cos(phi);
        }
        initLastPoints();
        internalMove();
    }

    private void internalMove() {
        for (int i = 0; i < tagCount; i++) {
            double sinY = Math.sin(offsetAz);
            double cosY = Math.cos(offsetAz);
            double sinX = Math.sin(offsetAx);
            double cosX = Math.cos(offsetAx);

            double x = axes[i];
            double y = ayes[i];
            double z = azes[i];

            double nx1 = x;
            double ny1 = y * cosY - z * sinY;
            double nz1 = y * sinY + z * cosY;

            double nx2 = nx1 * cosX + nz1 * sinX;
            double ny2 = ny1;
            double nz2 = nz1 * cosX - nx1 * sinX;

            Tuple3<Double, Double, Double> xyp = projectToXY(
                    new Tuple3<Double, Double, Double>(nx2, ny2, nz2), radius);
            lastPoints[i] = new Tuple3<Integer, Integer, Double>(xyp.v1.intValue(), xyp.v2.intValue(),
                    xyp.v3);

            if (i == 0) {
                if (debugLabel != null) {
                    debugLabel.setText("i" /* I18N: no */ + "=" + i + ", " + "lastPoints[i" /* I18N: no */ + "]=" + lastPoints[i]
                            + ", " + "radius" /* I18N: no */ + "=" + radius
                            + ", " + "nx" /* I18N: no */ + "2=" + nx2 + ", " + "ny" /* I18N: no */ + "2=" + ny2 + ", " + "nz" /* I18N: no */ + "2=" + nz2);
                }
            }
        }
        lastShowOffsetAx = offsetAx;
        lastShowOffsetAz = offsetAz;
    }
    public static final double MOVE_THRESHOLD = 0.001;

    public Tuple3<Integer, Integer, Double>[] moveTo(double newAx, double newAz) {
        this.offsetAx = newAx;
        this.offsetAz = newAz;

        if (Math.abs(offsetAx - lastShowOffsetAx) > MOVE_THRESHOLD
                || Math.abs(offsetAz - lastShowOffsetAz) > MOVE_THRESHOLD) {
            internalMove();
        }
        return lastPoints;
    }

    public Tuple3<Integer, Integer, Double>[] moveByDelta(double deltaAx, double deltaAz) {
        return moveTo(offsetAx + deltaAx, offsetAz + deltaAz);
    }

    private static Tuple3<Double, Double, Double> projectToXY(Tuple3<Double, Double, Double> xyz, double radius) {
        double per = radius / (2 * radius - xyz.v3);
        return new Tuple3<Double, Double, Double>(xyz.v1 * per, xyz.v2 * per, per);
    }

    private static Tuple3<Double, Double, Double> calcXYZ(double radius, double ax, double az) {
        return new Tuple3<Double, Double, Double>(
                radius * Math.sin(az) * Math.cos(ax), // x
                radius * Math.sin(ax), // y
                radius * Math.cos(az) * Math.cos(ax) // z
                );
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public static void main(String[] args) {
        System.out.println(calcXYZ(100, 0, 0.5 * Math.PI));
        System.out.println(calcXYZ(100, 0.25 * Math.PI, 0.25 * Math.PI));
        System.out.println(projectToXY(calcXYZ(100, 0, 0.25 * Math.PI), 100));
        System.out.println(projectToXY(calcXYZ(100, 0, 0.75 * Math.PI), 100));
    }
}
