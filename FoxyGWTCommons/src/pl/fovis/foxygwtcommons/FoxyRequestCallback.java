/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;
import java.util.Date;
import java.util.List;
import pl.trzy0.foxy.commons.FoxyCommonConsts;
import simplelib.BaseUtils;
import simplelib.SimpleDateUtils;
import simplelib.StackTraceUtil;

/**
 *
 * @author pmielanczuk
 */
public class FoxyRequestCallback implements RequestCallback {

    protected static final String DIAG_MSG_KIND$FOXY_REQUEST_TIMING = "foxyRequestTiming";
    public static String serviceExtraResponseData;
    private static Object optCallContextForHookInNextCall;
//    public static Long serviceTimeStampForResponse;
    protected RequestCallback callback;
    protected FoxyRequestBuilder frb;
    protected Date backToClientDate;
    protected Object optCallContextForHook;

    private static IResponseProcessingHook hook = null;

    public static void setResponseProcessingHook(IResponseProcessingHook hook) {
        FoxyRequestCallback.hook = hook;
    }

    public FoxyRequestCallback(RequestCallback callback, FoxyRequestBuilder frb) {
        this.callback = callback;
        this.frb = frb;
        this.optCallContextForHook = optCallContextForHookInNextCall;
        optCallContextForHookInNextCall = null;
    }

//    private static final Pair<Boolean, Long> afterResponseProcessingCallbackContParam = new Pair<Boolean, Long>();
    protected void logRequestFinished(boolean isError, Response response, RuntimeException optClientEx, Throwable optServerEx) {

//        if (isError) {
//            System.out.println("logRequestFinished: has error in call to " + getServiceMethodName() + ", serviceExtraResponseData=" + serviceExtraResponseData
//                    + ", loggedUserName=" + FoxyRpcRequestBuilder.getOptLoggedUserNameOnClient() + ", response=" + response);
//        }
        if (hook != null) {
            try {
                Long foxyRequestTimingAfterCommitTimeStamp = BaseUtils.tryParseLong(response.getHeader(FoxyCommonConsts.FOXY_REQUEST_TIMING_AFTER_COMMIT_TIMESTAMP_HEADER_NAME), null);
//                afterResponseProcessingCallbackContParam.v1 = isError;
//                afterResponseProcessingCallbackContParam.v2 = foxyRequestTimingAfterCommitTimeStamp;
                hook.afterResponseProcessing(isError, foxyRequestTimingAfterCommitTimeStamp);
            } catch (Exception ex) {
                // cicho-loguj-cicho
                ClientDiagMsgs.showException("error in logRequestFinished", ex);
//                if (ClientDiagMsgs.isShowDiagEnabled(FoxyCommonConsts.DIAG_MSG_KIND$GLOBAL_UNCAUGHT_EXCEPTIONS)) {
//                    ClientDiagMsgs.showDiagMsg(FoxyCommonConsts.DIAG_MSG_KIND$GLOBAL_UNCAUGHT_EXCEPTIONS, StackTraceUtil.getCustomStackTrace(ex));
//                }
            }
        }

        if (!ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$FOXY_REQUEST_TIMING)) {
            return;
        }

        String methodStr = getServiceMethodName();

        Date end = new Date();
        final Date requestSendStart = frb.requestSendStart;

        String onServerTimingStr = "";

        if (response != null) {
            String serverStartStr = response.getHeader("_foxy_request_timing_start_");
            String serverEndStr = response.getHeader("_foxy_request_timing_end_");

            long millisBeforeBackToClient = SimpleDateUtils.getElapsedMillis(requestSendStart, backToClientDate);

//            onServerTimingStr = ", serverStart=" + serverStartStr + ", serverEnd=" + serverEndStr;
            Date serverStart = SimpleDateUtils.parseFromCanonicalString(serverStartStr);
            Date serverEnd = SimpleDateUtils.parseFromCanonicalString(serverEndStr);

            if (serverStart != null && serverEnd != null) {

                long processingByServerMillis = SimpleDateUtils.getElapsedMillis(serverStart, serverEnd);

                if (processingByServerMillis > millisBeforeBackToClient) {
                    processingByServerMillis = millisBeforeBackToClient;
                }

                boolean needToFixServerTimes = serverStart.after(backToClientDate) || serverEnd.after(backToClientDate)
                        || serverStart.before(requestSendStart) || serverEnd.before(requestSendStart);

                if (needToFixServerTimes) {
                    long halfNetMillis = (millisBeforeBackToClient - processingByServerMillis) / 2;
                    long halfNetMillis2 = (millisBeforeBackToClient - processingByServerMillis) - halfNetMillis;

                    serverStart = SimpleDateUtils.addMillis(requestSendStart, halfNetMillis2);
                    serverEnd = SimpleDateUtils.addMillis(backToClientDate, -halfNetMillis);
                }

                onServerTimingStr = ", serverStart=" + SimpleDateUtils.toCanonicalStringWithMillis(serverStart)
                        + ", serverEnd=" + SimpleDateUtils.toCanonicalStringWithMillis(serverEnd);
                onServerTimingStr += ", before serverStart: " + SimpleDateUtils.getElapsedMillis(requestSendStart, serverStart) + " ms";
                onServerTimingStr += ", processing on server: " + processingByServerMillis + " ms"
                        + ", back to client: " + SimpleDateUtils.getElapsedMillis(serverEnd, backToClientDate) + " ms"
                        + ", transport: " + (millisBeforeBackToClient - processingByServerMillis) + " ms";
            }

//            if (isError) {
//                System.out.println("all headers=" + response.getHeadersAsString());
//            }
        } else {
//            System.out.println("no response! isError=" + isError);
        }
        final String fullMsgTxt = "request {" + methodStr + "} done"
                + (response != null ? " [" + response.getStatusCode() + "]" : "[no resp.]")
                + (isError ? " (error)" : "") + ": start=" + SimpleDateUtils.toCanonicalStringWithMillis(requestSendStart)
                + ", backToClientDate=" + SimpleDateUtils.toCanonicalStringWithMillis(backToClientDate)
                + ", end=" + SimpleDateUtils.toCanonicalStringWithMillis(end)
                + onServerTimingStr + ", elapsed total: "
                + SimpleDateUtils.getElapsedMillis(requestSendStart, end) + " ms"
                + ", final processing response on client: " + SimpleDateUtils.getElapsedMillis(backToClientDate, end) + " ms";

        ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$FOXY_REQUEST_TIMING, fullMsgTxt);

//        if (isError) {
//            System.out.println("fullMsgTxt=" + fullMsgTxt);
//        }
        if (optClientEx != null) {
            ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$FOXY_REQUEST_TIMING, "client exception:\n"
                    + StackTraceUtil.getCustomStackTrace(optClientEx));
        }
        if (optServerEx != null) {
            ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$FOXY_REQUEST_TIMING, "server exception:\n"
                    + StackTraceUtil.getCustomStackTrace(optServerEx));
        }
    }

    protected String getServiceMethodName() {
        String methodStr = "";
        List<String> reqTokens = BaseUtils.splitBySep(frb.getRequestData(), "|", true);
        if (reqTokens != null && reqTokens.size() >= 7) {
            methodStr = reqTokens.get(6);
        }
        return methodStr;
    }

    protected void beforeResponseProcessing(Request request, Response optResponse) {
        backToClientDate = new Date();

        serviceExtraResponseData = optResponse == null ? null : optResponse.getHeader(FoxyCommonConsts.SERVICE_EXTRA_RESPONSE_DATA_HEADER_NAME);

//        System.out.println("beforeResponseProcessing: " + getServiceMethodName() + ", serviceExtraResponseData=" + serviceExtraResponseData
//                + ", loggedUserName=" + FoxyRpcRequestBuilder.getOptLoggedUserNameOnClient());
//        String serviceTimeStampForResponseStr = response.getHeader("_Service_TimeStamp_For_Response_");
//        serviceTimeStampForResponse = BaseUtils.tryParseLong(serviceTimeStampForResponseStr, null);
    }

    @Override
    public void onResponseReceived(Request request, Response response) {
        beforeResponseProcessing(request, response);

        RuntimeException exx = null;
        try {

            boolean doNotProcess = false;

            if (hook != null) {
                doNotProcess = hook.isProcessingResponseRedundant(optCallContextForHook);
            }

            if (!doNotProcess) {
                callback.onResponseReceived(request, response);
            }

        } catch (RuntimeException ex) {
            exx = ex;
            throw ex;
        } finally {
            logRequestFinished(response.getStatusCode() != Response.SC_OK, response, exx, null);
        }
    }

    @Override
    public void onError(Request request, Throwable exception) {
        beforeResponseProcessing(request, null);

        RuntimeException exx = null;

        try {
            callback.onError(request, exception);
        } catch (RuntimeException ex) {
            exx = ex;
            throw ex;
        } finally {
            logRequestFinished(true, null, exx, exception);
        }
    }

    public static void setOptCallContextForHookInNextCall(Object val) {
        optCallContextForHookInNextCall = val;
    }
}
