﻿exec sp_check_version '1.1.6';
go
---do konca lipca
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_ex]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_into_bik_node_ex]
go

create procedure sp_insert_into_bik_node_ex(@a int)
as

begin

if(@a<>NULL)
begin
	declare @tree_code varchar(255)
	declare @table_folders varchar(255);
	declare @tree_id int;
	declare @kinds varchar(255);

	set nocount on;
	
	declare @diag_level int = 1;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': poczatek'
	
	select @tree_id = id 
	from bik_tree
	where code=@tree_code;

	create table #primaryIds(si_id varchar(5000) primary key);

	if(@tree_code!='Reports' and @tree_code!='Teradata')
	begin
		set @table_folders = ' APP_FOLDER ';
		if(@tree_code = 'Connections')
		begin
			set @kinds = '''MetaData.DataConnection''';
			exec('insert into #primaryIds
					select convert(varchar(30),SI_ID) as SI_ID  
					from APP_METADATA__DATACONNECTION
					where SI_KIND in (' + @kinds + ') and SI_OBJECT_IS_CONTAINER=0');			
		end;
		if(@tree_code = 'ObjectUniverses')
		begin
			set @kinds = '''Universe''';
			exec('insert into #primaryIds
					select convert(varchar(30),SI_ID) as SI_ID  
					from APP_UNIVERSE
					where SI_KIND in (' + @kinds + ')');
		end;		
	end
	
	if(@tree_code='Reports')
	begin
		-- wybranie folderw raportów bez raportów użytkowników
		set @table_folders = ' INFO_FOLDER where si_name != ''~Webintelligence''';
		--set @table_folders = ' INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2 is null or SI_PATH__SI_FOLDER_NAME2 != ''User Folders''';
		set @kinds = '''Webi'', ''Flash'', ''CrystalReport'',''Excel'',''FullClient'',''Pdf'',''Hyperlink''
		,''Rtf'',''Txt'',''Powerpoint'',''Word''';
	end;
	
    declare @sql nvarchar(max);
				
	declare @node_kind_id int;
	set @sql = N'';
	if(@tree_code = 'Teradata')
	begin
		--print cast(sysdatetime() as varchar(22)) + ': faza 4.1 - teradata przed sp_teradata_init_branch_names'
		--exec sp_teradata_init_branch_names;
		if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': tworzenie pierwszego @sql'
		set @sql = N'select branch_names as SI_ID, parent_branch_names	as SI_PARENTID, 
						type as SI_KIND, name as SI_NAME, extra_info as DESCR
				 from bik_teradata';
	end;
	else
	begin
		if(@tree_code = 'Reports')
		-- sztuczny podzial na root folder i user folders dla raportów
		set @sql = N'select distinct SI_PARENT_FOLDER as SI_ID, (select si_id from INFO_FOLDER where SI_NAME=''User Folders'') as SI_PARENTID, SI_KIND, SI_PATH__SI_FOLDER_NAME1 as SI_NAME, NULL as DESCR
				 from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2=''User Folders'' 
				 and SI_NAME!=''~Webintelligence'' 

				 union all
				
				 select si_id, case
									when SI_PARENTID=0 and si_name!=''User Folders''
										then (select si_id from INFO_FOLDER where SI_NAME=''Root Folder'')
									when si_id in(select si_id from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2 = ''User Folders'')
                                        then SI_PARENTID
									when SI_PARENTID in(select si_id from INFO_FOLDER)
										then SI_PARENTID
									else 0 end as SI_PARENTID, SI_KIND, case
																			when SI_NAME=''Root Folder''
																				then ''Foldery korporacyjne''
																			when SI_NAME=''User Folders''
																				then ''Foldery użytkowników''
																			else SI_NAME end, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from ' + @table_folders + 								
				' union all

				 select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +') and SI_INSTANCE = 0
				union all
				
				select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR
				from aaa_global_props where si_parentid in (select si_parentid from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2=''User Folders'' and SI_NAME!=''~Webintelligence'') and si_kind!=''Folder''
				';
		if(@tree_code = 'Connections')
			set @sql = N'select 1 as si_id, 0 as si_parentid, ''ConnectionFolder'' as si_kind, ''Połączenia'' as si_name, '''' as descr
				
				 union all

				 select SI_ID, 1, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from APP_METADATA__DATACONNECTION
				 where SI_KIND in ('+ @kinds +') and SI_OBJECT_IS_CONTAINER=0';

		if(@tree_code = 'ObjectUniverses')
			set @sql = N'select si_id, case 
									when SI_NAME=''Universes''
										then 0
									when SI_PARENTID in(select si_id from ' + @table_folders +') 
										then SI_PARENTID
									else 0 end as SI_PARENTID, SI_KIND, case when SI_NAME=''Universes'' then ''Światy obiektów'' else SI_NAME end as SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from ' + @table_folders + 
													
				'union all

				 select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +')';
	end;					

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed zapytaniem'
	print 'zapytanie: ' + @sql;

	create table #tmpNodes (si_id varchar(1000) primary key, si_parentid varchar(1000), si_kind varchar(max), si_name varchar(max), descr varchar(max));
	if(@tree_code = 'Reports')
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''ReportFolder'' 
										else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	if(@tree_code = 'Connections')
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''ConnectionFolder''
										when si_kind=''MetaData.DataConnection'' then ''DataConnection''
										else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	if(@tree_code = 'ObjectUniverses')
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''UniversesFolder'' else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	if(@tree_code = 'Teradata')
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, si_kind, si_name, DESCR
					from (' + @sql + ') xxx';

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po stworzeniu drugiego @sql'
			   
	EXECUTE(@sql);

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po wrzuceniu do #tmpNodes'

	insert into bik_node_kind (code,caption) 
	select distinct si_kind, si_kind
	from #tmpNodes 
	where not exists (select 1 from bik_node_kind where code = si_kind);
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/

	declare @rc int = 1

	while @rc > 0 begin
	
	delete from #tmpNodes
	where (si_kind = 'UniversesFolder' or si_kind = 'ConnectionFolder' or si_kind = 'ReportFolder') and not exists(select 1 from #tmpNodes g where g.si_parentid = #tmpNodes.si_id)
		set @rc = @@ROWCOUNT
	end;--end loop
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/
	
	declare @report_tree_id int;
	select @report_tree_id=id from bik_tree where code='Reports'

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed updatetami'

	update bik_node 
	set parent_node_id = null, node_kind_id = bik_node_kind.id, name = ltrim(rtrim(#tmpNodes.si_name)),
		is_deleted = 0, descr = #tmpNodes.descr, tree_id = @tree_id
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.obj_id = #tmpNodes.si_id and tree_id = @tree_id and bik_node.linked_node_id is null
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed update podlinkowanych'
	--update podlinkowanych
	update bik_node 
	set parent_node_id = null, node_kind_id = bik_node_kind.id, name = ltrim(rtrim(#tmpNodes.si_name)),
		 descr = #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.obj_id = #tmpNodes.si_id and (tree_id = @report_tree_id or tree_id in (select id from bik_tree where tree_kind='Taxonomy')) and not bik_node.linked_node_id is null

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed insertami'
	
	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select bik_node_kind.id, ltrim(rtrim(#tmpNodes.si_name)), @tree_id, #tmpNodes.si_id, #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
		left join bik_node on bik_node.obj_id = #tmpNodes.si_id and bik_node.tree_id=@tree_id
	where bik_node.id is null;

	set nocount on;

	drop table #primaryIds;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed updateami parentow'
	

	update bik_node 
	set parent_node_id= bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id --and bik_node.tree_id = @tree_id
	inner join bik_node bk on bk.obj_id = pt.si_parentid where bik_node.linked_node_id is null--and bk.tree_id = @tree_id 

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed usuwaniem'
	
	update --delete from 
	bik_node
	set is_deleted = 1
	--select *
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id join bik_node_kind on bik_node.node_kind_id = bik_node_kind.id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and not bik_node_kind.code in 
	('Measure', 'Dimension', 'Detail', 'UniverseClass', 'ReportQuery', 'Filter', 'UniverseColumn', 'UniverseTable', 'UniverseAliasTable', 'UniverseDerivedTable', 'UniverseTablesFolder', 'ConnectionEngineFolder', 'ConnectionNetworkFolder')
				
	drop table #tmpNodes
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po usuwaniu i po dropie'

	exec sp_delete_linked_nodes_where_orignal_is_deleted;
	exec sp_node_init_branch_id @tree_id, null;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': koniec'
end;

 if(sysdatetime()>'2012-08-01 00:00:00.0000000')
	begin
	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbo_extradata]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[sp_insert_into_bik_node_sapbo_extradata]
	
	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbo_connections]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[sp_insert_into_bik_node_sapbo_connections]

	
	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[sp_insert_into_bik_node]
	
	
	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_metadata_connections]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[sp_insert_into_bik_joined_objs_metadata_connections]


	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_dqc]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[sp_insert_into_bik_node_dqc]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_objects_from_Designer]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[sp_insert_into_bik_node_objects_from_Designer]

	end
end
go


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_update_into_bik_node_ex]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_update_into_bik_node_ex]
go

create procedure sp_update_into_bik_node_ex as
begin 
 if(sysdatetime()>'2012-08-01 00:00:00.0000000')
	begin
		EXEC sp_rename 'bik_node.[is_built_in]', 'is_build_in', 'COLUMN'
		update bik_node set is_deleted=-100 where is_deleted=0
	end
end
go

------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------


if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node]
go

create procedure [dbo].[sp_insert_into_bik_node](@tree_code varchar(255))
as
	declare @table_folders varchar(255);
	declare @tree_id int;
	declare @kinds varchar(255);
begin
	set nocount on;
	
	declare @diag_level int = 1;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': poczatek'
	
	select @tree_id = id 
	from bik_tree
	where code=@tree_code;

	create table #primaryIds(si_id varchar(5000) primary key);

	if(@tree_code!='Reports' and @tree_code!='Teradata')
	begin
		set @table_folders = ' APP_FOLDER ';
		if(@tree_code = 'Connections')
		begin
			set @kinds = '''MetaData.DataConnection''';
			exec('insert into #primaryIds
					select convert(varchar(30),SI_ID) as SI_ID  
					from APP_METADATA__DATACONNECTION
					where SI_KIND in (' + @kinds + ') and SI_OBJECT_IS_CONTAINER=0');			
		end;
		if(@tree_code = 'ObjectUniverses')
		begin
			set @kinds = '''Universe''';
			exec('insert into #primaryIds
					select convert(varchar(30),SI_ID) as SI_ID  
					from APP_UNIVERSE
					where SI_KIND in (' + @kinds + ')');
		end;		
	end
	
	if(@tree_code='Reports')
	begin
		-- wybranie folderw raportów bez raportów użytkowników
		set @table_folders = ' INFO_FOLDER where si_name != ''~Webintelligence''';
		--set @table_folders = ' INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2 is null or SI_PATH__SI_FOLDER_NAME2 != ''User Folders''';
		set @kinds = '''Webi'', ''Flash'', ''CrystalReport'',''Excel'',''FullClient'',''Pdf'',''Hyperlink''
		,''Rtf'',''Txt'',''Powerpoint'',''Word''';
	end;
	
    declare @sql nvarchar(max);
				
	declare @node_kind_id int;
	set @sql = N'';
	if(@tree_code = 'Teradata')
	begin
		--print cast(sysdatetime() as varchar(22)) + ': faza 4.1 - teradata przed sp_teradata_init_branch_names'
		--exec sp_teradata_init_branch_names;
		if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': tworzenie pierwszego @sql'
		set @sql = N'select branch_names as SI_ID, parent_branch_names	as SI_PARENTID, 
						type as SI_KIND, name as SI_NAME, extra_info as DESCR
				 from bik_teradata';
	end;
	else
	begin
		if(@tree_code = 'Reports')
		-- sztuczny podzial na root folder i user folders dla raportów
		set @sql = N'select distinct SI_PARENT_FOLDER as SI_ID, (select si_id from INFO_FOLDER where SI_NAME=''User Folders'') as SI_PARENTID, SI_KIND, SI_PATH__SI_FOLDER_NAME1 as SI_NAME, NULL as DESCR
				 from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2=''User Folders'' 
				 and SI_NAME!=''~Webintelligence'' 

				 union all
				
				 select si_id, case
									when SI_PARENTID=0 and si_name!=''User Folders''
										then (select si_id from INFO_FOLDER where SI_NAME=''Root Folder'')
									when si_id in(select si_id from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2 = ''User Folders'')
                                        then SI_PARENTID
									when SI_PARENTID in(select si_id from INFO_FOLDER)
										then SI_PARENTID
									else 0 end as SI_PARENTID, SI_KIND, case
																			when SI_NAME=''Root Folder''
																				then ''Foldery korporacyjne''
																			when SI_NAME=''User Folders''
																				then ''Foldery użytkowników''
																			else SI_NAME end, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from ' + @table_folders + 								
				' union all

				 select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +') and SI_INSTANCE = 0
				union all
				
				select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR
				from aaa_global_props where si_parentid in (select si_parentid from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2=''User Folders'' and SI_NAME!=''~Webintelligence'') and si_kind!=''Folder''
				';
		if(@tree_code = 'Connections')
			set @sql = N'select 1 as si_id, 0 as si_parentid, ''ConnectionFolder'' as si_kind, ''Połączenia'' as si_name, '''' as descr
				
				 union all

				 select SI_ID, 1, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from APP_METADATA__DATACONNECTION
				 where SI_KIND in ('+ @kinds +') and SI_OBJECT_IS_CONTAINER=0';

		if(@tree_code = 'ObjectUniverses')
			set @sql = N'select si_id, case 
									when SI_NAME=''Universes''
										then 0
									when SI_PARENTID in(select si_id from ' + @table_folders +') 
										then SI_PARENTID
									else 0 end as SI_PARENTID, SI_KIND, case when SI_NAME=''Universes'' then ''Światy obiektów'' else SI_NAME end as SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from ' + @table_folders + 
													
				'union all

				 select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +')';
	end;					

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed zapytaniem'
	print 'zapytanie: ' + @sql;

	create table #tmpNodes (si_id varchar(1000) primary key, si_parentid varchar(1000), si_kind varchar(max), si_name varchar(max), descr varchar(max));
	if(@tree_code = 'Reports')
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''ReportFolder'' 
										else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	if(@tree_code = 'Connections')
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''ConnectionFolder''
										when si_kind=''MetaData.DataConnection'' then ''DataConnection''
										else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	if(@tree_code = 'ObjectUniverses')
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''UniversesFolder'' else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	if(@tree_code = 'Teradata')
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, si_kind, si_name, DESCR
					from (' + @sql + ') xxx';

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po stworzeniu drugiego @sql'
			   
	EXECUTE(@sql);

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po wrzuceniu do #tmpNodes'

	insert into bik_node_kind (code,caption) 
	select distinct si_kind, si_kind
	from #tmpNodes 
	where not exists (select 1 from bik_node_kind where code = si_kind);
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/

	declare @rc int = 1

	while @rc > 0 begin
	
	delete from #tmpNodes
	where (si_kind = 'UniversesFolder' or si_kind = 'ConnectionFolder' or si_kind = 'ReportFolder') and not exists(select 1 from #tmpNodes g where g.si_parentid = #tmpNodes.si_id)
		set @rc = @@ROWCOUNT
	end;--end loop
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/
	
	declare @report_tree_id int;
	select @report_tree_id=id from bik_tree where code='Reports'

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed updatetami'

	update bik_node 
	set parent_node_id = null, node_kind_id = bik_node_kind.id, name = ltrim(rtrim(#tmpNodes.si_name)),
		is_deleted = 0, descr = #tmpNodes.descr, tree_id = @tree_id
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.obj_id = #tmpNodes.si_id and tree_id = @tree_id and bik_node.linked_node_id is null
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed update podlinkowanych'
	--update podlinkowanych
	update bik_node 
	set parent_node_id = null, node_kind_id = bik_node_kind.id, name = ltrim(rtrim(#tmpNodes.si_name)),
		 descr = #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.obj_id = #tmpNodes.si_id and (tree_id = @report_tree_id or tree_id in (select id from bik_tree where tree_kind='Taxonomy')) and not bik_node.linked_node_id is null

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed insertami'
	
	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select bik_node_kind.id, ltrim(rtrim(#tmpNodes.si_name)), @tree_id, #tmpNodes.si_id, #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
		left join bik_node on bik_node.obj_id = #tmpNodes.si_id and bik_node.tree_id=@tree_id
	where bik_node.id is null;

	set nocount on;

	drop table #primaryIds;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed updateami parentow'
	

	update bik_node 
	set parent_node_id= bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id --and bik_node.tree_id = @tree_id
	inner join bik_node bk on bk.obj_id = pt.si_parentid where bik_node.linked_node_id is null--and bk.tree_id = @tree_id 

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed usuwaniem'
	
	update --delete from 
	bik_node
	set is_deleted = 1
	--select *
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id join bik_node_kind on bik_node.node_kind_id = bik_node_kind.id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and not bik_node_kind.code in 
	('Measure', 'Dimension', 'Detail', 'UniverseClass', 'ReportQuery', 'Filter', 'UniverseColumn', 'UniverseTable', 'UniverseAliasTable', 'UniverseDerivedTable', 'UniverseTablesFolder', 'ConnectionEngineFolder', 'ConnectionNetworkFolder')
				
	drop table #tmpNodes
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po usuwaniu i po dropie'

	exec sp_delete_linked_nodes_where_orignal_is_deleted;
	exec sp_node_init_branch_id @tree_id, null;
	
	exec sp_update_into_bik_node_ex;
	exec sp_insert_into_bik_node_ex null;
	
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': koniec'
end;
go

exec sp_update_version '1.1.6', '1.1.6 TRIAL';
go
