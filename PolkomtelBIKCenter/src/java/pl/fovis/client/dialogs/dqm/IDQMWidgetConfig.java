/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.dqm;

import com.google.gwt.user.client.ui.Widget;
import pl.fovis.common.dqm.DataQualityTestBean;

/**
 *
 * @author tflorczak
 */
public interface IDQMWidgetConfig {

    public Widget getWidget(Integer nodeId);

    public boolean validateWidgetsBeforeSave();

    public void fillWidgets(DataQualityTestBean bean);

    public void fillBean(DataQualityTestBean bean);

    public void cleanWidgets();

}
