/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib.i18nsupport;

import simplelib.i18nsupport.I18nMessage;
import simplelib.i18nsupport.ICurrentLocaleNameProvider;

/**
 *
 * @author pmielanczuk
 */
public class JavaCurrentLocaleNameProvider implements ICurrentLocaleNameProvider {

    private static boolean isFactorySet = false;
    protected static String defaultLocaleName = DEFAULT_LOCALE_NAME;
    protected static ThreadLocal<String> localeNamePerThread = new ThreadLocal<String>();

    public static synchronized void setFactory() {
        if (!isFactorySet) {
            isFactorySet = true;
            I18nMessage.setCurrentLocaleNameProvider(new JavaCurrentLocaleNameProvider());
        }
    }

    @Override
    public String getCurrentLocaleName() {
        String ln = localeNamePerThread.get();
        if (ln == null) {
            localeNamePerThread.remove();
        }
        return ln == null ? defaultLocaleName : ln;
    }

    public static void setCurrentLocaleName(String localeName) {
        if (localeName != null) {
            localeNamePerThread.set(localeName);
        } else {
            localeNamePerThread.remove();
        }
    }

    public static void setDefaultLocaleName(String localeName) {
        defaultLocaleName = localeName;
    }
}
