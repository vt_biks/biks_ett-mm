/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.lisa;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author pku
 */
public enum ColumnAvailabilityText {
    NOT_VISIBLE(0,I18n.niewidoczna.get() /* i18n:  */),
    READ_ONLY(1,I18n.tylkoDoOdczytu.get() /* i18n:  */),
    EDITABLE(2,I18n.edytowalna.get() /* I18N:  */);
    
    private Integer code;
    public String description;

    private ColumnAvailabilityText(Integer code, String description) {
        this.code = code;
        this.description = description;
    }
    
    public static String getDescription(Integer code){
        for(ColumnAvailabilityText value: values()){
            if(value.code.equals(code)){
                return value.description;
            }
        }
        return "?";
    }
    
    
    
}
