/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

/**
 *
 * @author wezyr
 */
public class RegExpMatcherFactoryPoison implements IRegExpMatcherFactory {

    private void throwUninitializedFactory() {
        throw new LameRuntimeException("matcherFactory uninitialized!");
    }

    public IRegExpPattern compile(String pattern, boolean ignoreCase, boolean multiline) {
        throwUninitializedFactory();
        return null;
    }
}
