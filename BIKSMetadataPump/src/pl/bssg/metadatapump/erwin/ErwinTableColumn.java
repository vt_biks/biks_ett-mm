/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.erwin;

import java.util.HashMap;

/**
 *
 * @author pgajda
 */
public class ErwinTableColumn extends ErwinXMLElementString{

    public ErwinTableColumn() {
        propertiesMap = new HashMap<String, String>() {
            {
                put("Name", null);
                put("Long_Id", null);
                put("Type", null);
                put("Physical_Name", null);
                put("Table_Long_Id", null);
                put("Comment", null);
                put("Parent_Attribute_Ref", null);
                put("Parent_Relationship_Ref", null);
            }
        };        
    }
    
}
