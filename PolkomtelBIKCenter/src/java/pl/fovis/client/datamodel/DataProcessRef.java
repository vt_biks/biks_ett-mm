/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.datamodel;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasHorizontalAlignment.HorizontalAlignmentConstant;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.ErwinDataModelProcessObjectsRel;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;

/**
 *
 * @author pgajda
 */
public class DataProcessRef {

    public final static String STATUS_DEFAULT_COLOR = "#dfe7c0"; // szary
    public final static String STATUS_Z_COLOR = "#39e639"; // zielony
    public final static String STATUS_D_COLOR = "#39e639"; // zielony
    public final static String STATUS_B_COLOR = "#39e639"; // zielony
    public final static String STATUS_R_COLOR = "#ff4040"; // czerwony
    public final static String STATUS_E_COLOR = "#ff4040"; // czerwony
    public final static String STATUS_I_COLOR = "#ff4040"; // czerwony
    public final static String STATUS_J_COLOR = "#ff4040"; // czerwony
    public final static String STATUS_C_COLOR = "#ffff33"; // żółty
    public final static String STATUS_A_COLOR = "#ffff33"; // żółty
    public final static String STATUS_W_COLOR = "#82CAFF"; // niebieski
    public final static String STATUS_X_COLOR = "#E0B0FF"; // fiolet
    public final static String STATUS_F_COLOR = "#E0B0FF"; // fiolet
    public final static String PROCESS_STATUS_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public final static Map<String, String> statusColorMap = new HashMap<String, String>() {
        {
            put("Z", STATUS_Z_COLOR);
            put("D", STATUS_D_COLOR);
            put("B", STATUS_B_COLOR);
            put("R", STATUS_R_COLOR);
            put("C", STATUS_C_COLOR);
            put("E", STATUS_E_COLOR);
            put("I", STATUS_I_COLOR);
            put("J", STATUS_J_COLOR);
            put("A", STATUS_A_COLOR);
            put("W", STATUS_W_COLOR);
            put("X", STATUS_X_COLOR);
            put("F", STATUS_F_COLOR);
        }
    };

    protected HorizontalPanel panel;
    protected VerticalPanel modifyPanel;
    protected VerticalPanel mainObject;
    protected VerticalPanel usePanel;
    protected EntityDetailsDataBean eddb;
    protected HorizontalPanel controlPanel;
    protected String activeSubjectArea;
    protected DateTimeFormat dateFormat;

    public DataProcessRef() {
        dateFormat = DateTimeFormat.getFormat(PROCESS_STATUS_DATE_FORMAT);
    }

    public void setEntityDetailsDataBean(EntityDetailsDataBean eddb) {
        this.eddb = eddb;
    }

    public Widget buildWidgets() {
        if (panel != null) {
            panel.clear();
        } else {
            panel = new HorizontalPanel();
        }

        modifyPanel = new VerticalPanel();
        modifyPanel.setWidth("200px");
        mainObject = new VerticalPanel();
        modifyPanel.setWidth("200px");
        usePanel = new VerticalPanel();
        usePanel.setWidth("200px");

        HorizontalPanel mainObjectHorz = new HorizontalPanel();
        mainObjectHorz.setWidth("350px");
        Image oneWay1 = new Image("images/oneWayArrow.jpg" /* I18N: no */);
        mainObjectHorz.add(oneWay1);
        mainObjectHorz.setCellWidth(oneWay1, "72px");
        mainObjectHorz.setCellVerticalAlignment(oneWay1, HasVerticalAlignment.ALIGN_MIDDLE);
        mainObjectHorz.add(mainObject);
        mainObjectHorz.setCellWidth(mainObject, "200px");
        Image oneWay2 = new Image("images/oneWayArrow.jpg" /* I18N: no */);
        mainObjectHorz.add(oneWay2);
        mainObjectHorz.setCellWidth(oneWay2, "72px");
        mainObjectHorz.setCellVerticalAlignment(oneWay2, HasVerticalAlignment.ALIGN_MIDDLE);

        panel.add(modifyPanel);
        panel.setCellWidth(modifyPanel, "200px");
        panel.add(mainObjectHorz);
        panel.setCellWidth(mainObjectHorz, "350px");
        panel.add(usePanel);
        panel.setCellWidth(usePanel, "200px");

        panel.setStyleName("dataModel");
        return panel;

    }

    public void displayData() {
        downloadAllData();
    }

    /*
     IMP - mod  import
     INS - mod
     DEL - mod
     SEL - use
     EKS - use
     UPD - mod
     */
    public void displayAfterDataDown() {
        mainObject.clear();
        usePanel.clear();
        modifyPanel.clear();

        if (eddb == null || eddb.erwinDataModelProcessObjectsRel == null) {
            return;
        }

        ProcessWidget procWid = new ProcessWidget(eddb.node.name);
        GWTUtils.setStyleAttribute(procWid, "backgroundColor", "#33CC33"); // zieleń
        procWid.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        procWid.setHeight("150px");
        procWid.showWidget();
        mainObject.add(procWid);

        Label areaLaber = new Label(I18n.obszarDanych.get() + " : " + activeSubjectArea);
        mainObject.add(areaLaber);
        mainObject.setCellHorizontalAlignment(areaLaber, HasHorizontalAlignment.ALIGN_CENTER);

        if (!BaseUtils.isCollectionEmpty(eddb.erwinDataModelProcessObjectsRel) && eddb.erwinDataModelProcessObjectsRel.get(0).dbName != null) {
            Label dbLaber = new Label("DB: " + eddb.erwinDataModelProcessObjectsRel.get(0).dbName);
            mainObject.add(dbLaber);
            mainObject.setCellHorizontalAlignment(dbLaber, HasHorizontalAlignment.ALIGN_CENTER);
        }

        for (ErwinDataModelProcessObjectsRel edmpor : eddb.erwinDataModelProcessObjectsRel) {

            String procesType = edmpor.idTypProcess;
            procWid = new ProcessWidget(edmpor);

            if (procesType.equals("SEL") || procesType.equals("EKS")) {
                usePanel.add(procWid);
            } else {
                modifyPanel.add(procWid);
            }
            procWid.showWidget();

        }
    }

    public Widget buildContlolWidgets() {

        if (controlPanel == null) {
            controlPanel = new HorizontalPanel();
        } else {
            controlPanel.clear();
        }

        Label label = new Label(I18n.obszarDanych.get() + " : ");
        controlPanel.add(label);
        controlPanel.setCellWidth(label, "110px");
        controlPanel.setCellHorizontalAlignment(label, HasHorizontalAlignment.ALIGN_RIGHT);
        controlPanel.add(getSubjectAreaListBox());

        controlPanel.setWidth("100%");

        return controlPanel;

    }

    protected Widget getSubjectAreaListBox() {
        final ListBox listSubjectArea = new ListBox();
        if (eddb == null) {
            return listSubjectArea;
        }

        if (eddb.erwinDataModelProcessAreas == null || eddb.erwinDataModelProcessAreas.isEmpty()) {
            listSubjectArea.addItem("<Model>", "000000");
        } else {
            activeSubjectArea = eddb.erwinDataModelProcessAreas.get(0);
            for (Iterator<String> iterStr = eddb.erwinDataModelProcessAreas.iterator(); iterStr.hasNext();) {
                listSubjectArea.addItem(iterStr.next());
            }
        }
        listSubjectArea.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                activeSubjectArea = listSubjectArea.getValue(listSubjectArea.getSelectedIndex());
                displayData();
            }
        });

        return listSubjectArea;

    }

    class ProcessWidget extends SimplePanel {

        protected VerticalPanel panel;
        protected String processName;
        protected String processType;
        protected String processStatus;
        protected Date processDate;

        public ProcessWidget(String processName) {
            this.processName = processName;
            init();
        }

        public ProcessWidget(ErwinDataModelProcessObjectsRel edmpor) {
            this.processName = edmpor.symbProc;
            this.processType = edmpor.idTypProcess;
            this.processStatus = edmpor.procStatus;
            this.processDate = edmpor.procDate;
            init();
        }

        protected final void init() {
            panel = new VerticalPanel();
        }

        public void showWidget() {
            setStyleName("processTable");

            panel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
            panel.setWidth("200px");
            panel.setHeight("100%");

            HTML lab = new HTML(processName + (processType != null ? ("  (" + processType + ")") : ""));
            panel.add(lab);
            panel.setCellVerticalAlignment(lab, HasVerticalAlignment.ALIGN_MIDDLE);
            setWidget(panel);

            String backgroundColor = null;
            if (processStatus != null) {
                backgroundColor = statusColorMap.get(processStatus);
            }

            if (backgroundColor == null) {
                backgroundColor = STATUS_DEFAULT_COLOR; //szary
            }
            GWTUtils.setStyleAttribute(this, "backgroundColor", backgroundColor);

            String title = "";
            if (processDate != null) {
                title = dateFormat.format(processDate);
                //this.setTitle(dateFormat.format(processDate));
            }
            if (processStatus != null) {
                title += " status: " + processStatus;
            }
            if (title.length() > 0) {
                this.setTitle(title);
            }
        }

        public void setHorizontalAlignment(HorizontalAlignmentConstant align) {
            panel.setHorizontalAlignment(align);
        }
    }

    public void downloadAllData() {
        BIKClientSingletons.showInfo(I18n.zadanieWToku.get() /* i18n:  */);
        mainObject.clear();
        usePanel.clear();
        modifyPanel.clear();

        BIKClientSingletons.getLisaService().getDataModelProcessObjectsRel(eddb.node.id, activeSubjectArea, false,
                new StandardAsyncCallback<List<ErwinDataModelProcessObjectsRel>>("error in" /* I18N: no:err-fail-in */ + " getDataModelProcessObjectsRel()") {
                    @Override
                    public void onSuccess(List<ErwinDataModelProcessObjectsRel> result) {
                        eddb.erwinDataModelProcessObjectsRel = result;
                        displayAfterDataDown();
                        BIKClientSingletons.showInfo(I18n.pobieranieDanychProcesowErwin.get());
                    }
                });
    }
}
