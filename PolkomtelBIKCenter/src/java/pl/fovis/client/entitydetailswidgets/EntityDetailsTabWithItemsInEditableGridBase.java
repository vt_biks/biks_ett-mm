/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

/**
 *
 * @author wezyr
 */
public abstract class EntityDetailsTabWithItemsInEditableGridBase<ITEM>
        extends EntityDetailsTabWithItemsBase<ITEM> {

    //ww\jo: czy ta metoda jest tutaj potrzebna? kto ją wywołuję? tutaj w tej klasie
    // kodu wywołującego brak, więć chyba nie musi być taka metoda abstrakcyjna
    // skoro nikt nie korzysta? a potem w niektórych podklasach widzę pustą
    // i zupełnie zbędną implementację, bo niepotrzebnie tu jest ta deklaracja
    //protected abstract PushButton createDeleteButton(int nodeId, int paramNodeId, Object ob);
//    protected abstract void setUpGridColumnsWidths(HTMLTable gp);
    // domyślnie zakładki oparte o listę elementów mają kontrolki do dodania
    // nowego elementu
    @Override
    public boolean hasEditableControls() {
        return false;
    }
}
