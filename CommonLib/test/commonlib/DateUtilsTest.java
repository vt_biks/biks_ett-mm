/*
 * DateUtilsTest.java
 * JUnit based test
 *
 * Created on 18 stycze� 2007, 12:58
 */

package commonlib;

import java.text.SimpleDateFormat;
import junit.framework.*;
import java.util.Date;

/**
 *
 * @author msmoktunowicz
 */
public class DateUtilsTest extends TestCase {
    
    public DateUtilsTest(String testName) {
        super(testName);
    }
    
    protected void setUp() throws Exception {
    }
    
    protected void tearDown() throws Exception {
    }
    
    
    public void testInt() {
        Integer i = null; //new Integer(5);
        Integer j = new Integer(5);
        if (5 == 4)
            assertEquals(1, 0);
    }
    /**
     * Test of getDateAsISO8601String method, of class commonlib.DateUtils.
     */
    public void testGetDateAsISO8601String() {
        System.out.println("getDateAsISO8601String");
        Date date = new Date();
        
        String str = DateUtils.getDateAsISO8601DateTimeString(date);
        Date result = DateUtils.getDateFromISO8601String(str);
        assertEquals(date, result);
    }
    
    public void testGetDateAsISO8601DateString() throws Exception {
        System.out.println("getDateAsISO8601String");
        Date date = new Date();
        
        SimpleDateFormat timeFormat = new java.text.SimpleDateFormat("yyyy-MM-dd");
        String str = timeFormat.format(date);
        date = timeFormat.parse(str);
        
        str = DateUtils.getDateAsISO8601DateString(date);
        Date result = DateUtils.getDateFromISO8601String(str);
        assertEquals(date, result);
    }
    
    public void testGetDateFromFlexiFormattedString() throws Exception {
        // IZ
        assertEquals(DateUtils.getDateFromISO8601String("2004-02-29T13:49:51.987"),
                DateUtils.getDateFromFlexiFormattedString("2004-02-29T13:49:51.987"));

        // Z
        assertEquals(DateUtils.getDateFromISO8601String("2004-02-29T13:49:51.987"),
                DateUtils.getDateFromFlexiFormattedString("2004-02-29 13:49:51.987"));

        // S
        assertEquals(DateUtils.getDateFromISO8601String("2004-02-29T13:49:51.000"),
                DateUtils.getDateFromFlexiFormattedString("2004-02-29 13:49:51"));

        // N
        assertEquals(DateUtils.getDateFromISO8601String("2004-02-29T13:49:00.000"),
                DateUtils.getDateFromFlexiFormattedString("2004-02-29 13:49"));

        // D
        assertEquals(DateUtils.getDateFromISO8601String("2004-02-29T00:00:00.000"),
                DateUtils.getDateFromFlexiFormattedString("2004-02-29"));
    }

    public void testGetDateAsFlexiSDateTimeString() {
        System.out.println("getDateAsFlexiSDateTimeString: " + DateUtils.getDateAsFlexiSDateTimeString(new Date()));
    }
}