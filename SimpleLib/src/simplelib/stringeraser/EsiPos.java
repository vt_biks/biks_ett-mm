/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.stringeraser;

import simplelib.Pair;

/**
 *
 * @author pmielanczuk
 */
public abstract class EsiPos {

    public int startPos;
    public int innerStartPos;

    // rzuca wyjątek jeżeli nie udało się znaleźć pozycji końcowej - koniec
    // stringa, a prawego zamknięcia esi brak
    // wpp. zwraca koniec wnętrza (inner content) i koniec za prawym zamknięciem
    // UWAGA: każde wywołanie liczy jeszcze raz! czyli to jest kosztowne!
    public abstract Pair<Integer, Integer> calcEndPos();
}
