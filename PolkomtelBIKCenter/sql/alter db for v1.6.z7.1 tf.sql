﻿-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
--------------------   Implementacja modułu DQM  ------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
exec sp_check_version '1.6.z7.1';
go

if not exists (select 1 from bik_tree where code = 'DQM')
begin
	-- node kindy
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values ('DQMGroup', 'Grupa testów', 'dqmgroup', 'QualityMetadata', 1, 10, 0);
	
	--insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	--values ('DQMAllTestsFolder', 'Folder testów', 'dqmalltests', 'QualityMetadata', 0, 0, 0);
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values ('DQMTestSuccess', 'Test pozytywny', 'dqmtestsuccess', 'QualityMetadata', 0, 25, 1);
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values ('DQMTestFailed', 'Test negatywny', 'dqmtestfailed', 'QualityMetadata', 0, 25, 1);
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values ('DQMTestInactive', 'Test nieaktywny', 'dqmtestinactive', 'QualityMetadata', 0, 25, 1);
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values ('DQMTestNotStarted', 'Test', 'dqmtestnotstarted', 'QualityMetadata', 0, 25, 1);
	
	-- tree
	insert into bik_tree(name, node_kind_id, code, tree_kind, is_hidden, is_in_ranking, branch_level_for_statistics, is_built_in)
	values('DQM', (select id from bik_node_kind where code = 'DQMGroup'), 'DQM', 'QualityMetadata', 1, 0, 1, 0);
	
	insert into bik_translation(code, txt, lang, kind)
	values ('DQM', 'DQM', 'en', 'tree')

	insert into bik_icon(name)
	values  ('dqmgroup'),
			('dqmtestsuccess'),
			('dqmtestfailed'),
			('dqmtestinactive'),
			('dqmtestnotstarted');
			--('dqmalltests');
			
	insert into bik_translation(code, txt, lang, kind)
	values  ('DQMGroup', 'Test group', 'en', 'kind'),
			('DQMTestSuccess', 'Successful test', 'en', 'kind'),
			('DQMTestFailed', 'Failed test', 'en', 'kind'),
			('DQMTestInactive', 'Inactive test', 'en', 'kind'),
			('DQMTestNotStarted', 'Not started test', 'en', 'kind');
			--('DQMAllTestsFolder', 'Test folder', 'en', 'kind');
			
	declare @DQMTree int = (select id from bik_tree where code = 'DQM');
	
	--insert into bik_node(parent_node_id, node_kind_id, name, tree_id, obj_id, is_built_in, visual_order)
	--values (null, (select id from bik_node_kind where code = 'DQMAllTestsFolder'), 'Wszystkie testy', @DQMTree, null, 1, -1)
	
	exec sp_node_init_branch_id @DQMTree, null
end;
go

declare @treeOfTreeId int = (select id from bik_tree where code = 'TreeOfTrees');
declare @menuItemId int = (select id from bik_node_kind where code = 'MenuItem');
declare @visualOrder int = (select visual_order from bik_node where parent_node_id is null and tree_id = @treeOfTreeId and obj_id = 'metadata');

if not exists(select * from bik_node where parent_node_id is null and tree_id = @treeOfTreeId and obj_id = 'dqm')
begin
	insert into bik_node(parent_node_id, node_kind_id, name, tree_id, obj_id, visual_order)
	values (null, @menuItemId, 'DQM', @treeOfTreeId, 'dqm', @visualOrder)
end;

if not exists(select * from bik_node where tree_id = @treeOfTreeId and obj_id = '$DQM')
begin
	exec sp_add_menu_node 'dqm', '@', '$DQM'
	exec sp_add_menu_node '$DQM', '@', '&DQMGroup'
	exec sp_add_menu_node '&DQMGroup', '@', '&DQMTestSuccess'
	exec sp_add_menu_node '&DQMGroup', '@', '&DQMTestFailed'
	exec sp_add_menu_node '&DQMGroup', '@', '&DQMTestInactive'
	exec sp_add_menu_node '&DQMGroup', '@', '&DQMTestNotStarted'
end;

exec sp_node_init_branch_id @treeOfTreeId, null
go

update bik_app_prop set val = 'images/santa2.png|12.06|01.06|-120|8;images/easter.png|03.20|04.20|-51|3;' 
where name = 'logoOverlay'

update bik_node_kind set tree_kind = 'QualityMetadata' where tree_kind = 'DQCMetadata'

update bik_tree set tree_kind = 'QualityMetadata' where tree_kind = 'DQCMetadata'

if not exists (select * from bik_app_prop where name = 'showAnticipatedObjLink')
begin
	insert into bik_app_prop(name, val, is_editable)
	values('showAnticipatedObjLink', 'false', 1)
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_dqm_test') and type in (N'U'))
begin
	create table bik_dqm_test (
		id int not null identity(1,1) primary key,
		test_node_id int not null references bik_node(id),
		name varchar(512) not null,
		description varchar(max) null,
		long_name varchar(max) null,
		benchmark_definition varchar(max) null,
		sampling_method varchar(max) null,
		additional_information varchar(max) null,
		error_threshold decimal (18, 8) not null default(0),
		create_date datetime not null,
		modify_date datetime null,
		activate_date datetime null,
		deactivate_date datetime null,
		db_server_id int null references bik_db_server(id),
		error_db_server_id int null references bik_db_server(id),
		procedure_node_id int null references bik_node(id),
		error_procedure_node_id int null references bik_node(id),
		source_id int not null references bik_data_source_def(id),
		type varchar(50) not null,
		is_active int not null default(1),
		is_deleted int not null default(0)
	);
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_dqm_test]') and name = N'uq_bik_dqm_test_test_node_id')
begin
	create unique index uq_bik_dqm_test_test_node_id on bik_dqm_test (test_node_id);
end;
go

if OBJECT_ID (N'dbo.fn_get_db_server_id_by_procedure_node_id', N'FN') is not null
    drop function dbo.fn_get_db_server_id_by_procedure_node_id;
go

create function dbo.fn_get_db_server_id_by_procedure_node_id(@procedure_node_id int, @sourceId int)
returns int
as
begin
	declare @objId varchar(max) = (select obj_id from bik_node where id = @procedure_node_id);
	declare @tab table (str varchar(max), idx int);
	
	insert into @tab(str, idx)
	select str, idx from dbo.fn_split_by_sep(@objId, '|', 0);
	
	declare @serverName varchar(max) = (select str from @tab where idx = 1);
	declare @databaseName varchar(max) = (select str from @tab where idx = 2);
	declare @dbServerId int = null;

	select top 1 @dbServerId = id 
	from bik_db_server ser
	where source_id = @sourceId
	and (name = @serverName or name like @serverName + '#%')
	and exists(select * from dbo.fn_split_by_sep(coalesce(ser.database_list, ''), ',', 3) where str = @databaseName)

	return @dbServerId;
end
go

if not exists (select * from bik_attr_category where name = 'DQM' and is_built_in = 1)
begin

	insert into bik_attr_category(name, is_built_in)
	values ('DQM', 1)

	declare @categoryId int = (select id from bik_attr_category where name = 'DQM' and is_built_in = 1);

	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('Nazwa biznesowa', @categoryId, 1, '', 'shortText')

	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('Opis testu', @categoryId, 1, '', 'shortText')

	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('Definicja testu', @categoryId, 1, '', 'shortText')

	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('Metoda próbkowania', @categoryId, 1, '', 'shortText')

	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('Informacje dodatkowe', @categoryId, 1, '', 'shortText')

	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('Próg błędu', @categoryId, 1, '', 'shortText')
	
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('Data utworzenia', @categoryId, 1, '', 'shortText')
	
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('Data modyfikacji', @categoryId, 1, '', 'shortText')
	
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('Data aktywacji', @categoryId, 1, '', 'shortText')
	
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('Data dezaktywacji', @categoryId, 1, '', 'shortText')
	
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('Funkcja testująca', @categoryId, 1, '', 'shortText')
		
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('Funkcja zwracająca błędne dane', @categoryId, 1, '', 'shortText')
	
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('Źródło', @categoryId, 1, '', 'shortText')
	

	-- tłumacznia
	insert into bik_translation(code, txt, lang, kind)
	values ('Nazwa biznesowa', 'Long name', 'en', 'adef')

	insert into bik_translation(code, txt, lang, kind)
	values ('Opis testu', 'Test description', 'en', 'adef')

	insert into bik_translation(code, txt, lang, kind)
	values ('Definicja testu', 'Benchmark definition', 'en', 'adef')

	insert into bik_translation(code, txt, lang, kind)
	values ('Metoda próbkowania', 'Sampling method', 'en', 'adef')

	insert into bik_translation(code, txt, lang, kind)
	values ('Informacje dodatkowe', 'Additional information', 'en', 'adef')

	insert into bik_translation(code, txt, lang, kind)
	values ('Próg błędu', 'Error threshold', 'en', 'adef')
	
	insert into bik_translation(code, txt, lang, kind)
	values ('Data utworzenia', 'Create date', 'en', 'adef')
	
	insert into bik_translation(code, txt, lang, kind)
	values ('Data modyfikacji', 'Modify date', 'en', 'adef')
	
	insert into bik_translation(code, txt, lang, kind)
	values ('Data aktywacji', 'Activate date', 'en', 'adef')
	
	insert into bik_translation(code, txt, lang, kind)
	values ('Data dezaktywacji', 'Deactivate date', 'en', 'adef')
	
	insert into bik_translation(code, txt, lang, kind)
	values ('Funkcja testująca', 'Test function', 'en', 'adef')
	
	insert into bik_translation(code, txt, lang, kind)
	values ('Funkcja zwracająca błędne dane', 'Error returning function', 'en', 'adef')

	insert into bik_translation(code, txt, lang, kind)
	values ('Źródło', 'Source', 'en', 'adef')
	
	declare @DQMTestInactive int = dbo.fn_node_kind_id_by_code('DQMTestInactive');
    declare @DQMTestSuccess int = dbo.fn_node_kind_id_by_code('DQMTestSuccess');
    declare @DQMTestFailed int = dbo.fn_node_kind_id_by_code('DQMTestFailed');
    declare @DQMTestNotStarted int = dbo.fn_node_kind_id_by_code('DQMTestNotStarted');
    --
    declare @attrBusinessName int = (select id from bik_attr_def where name = 'Nazwa biznesowa' and is_built_in = 1);
    declare @attrTestDescription int = (select id from bik_attr_def where name = 'Opis testu' and is_built_in = 1);
    declare @attrTestDefinition int = (select id from bik_attr_def where name = 'Definicja testu' and is_built_in = 1);
    declare @attrSamplingMethod int = (select id from bik_attr_def where name = 'Metoda próbkowania' and is_built_in = 1);
    declare @attrAdditionalInfo int = (select id from bik_attr_def where name = 'Informacje dodatkowe' and is_built_in = 1);
    declare @attrErrorThreshold int = (select id from bik_attr_def where name = 'Próg błędu' and is_built_in = 1);
    declare @attrCreateDate int = (select id from bik_attr_def where name = 'Data utworzenia' and is_built_in = 1);
    declare @attrModifyDate int = (select id from bik_attr_def where name = 'Data modyfikacji' and is_built_in = 1);
    declare @attrActivateDate int = (select id from bik_attr_def where name = 'Data aktywacji' and is_built_in = 1);
    declare @attrDeactivateDate int = (select id from bik_attr_def where name = 'Data dezaktywacji' and is_built_in = 1);
    declare @attrFunctionName int = (select id from bik_attr_def where name = 'Funkcja testująca' and is_built_in = 1);
    declare @attrErrorFuncitonName int = (select id from bik_attr_def where name = 'Funkcja zwracająca błędne dane' and is_built_in = 1);
    declare @attrSource int = (select id from bik_attr_def where name = 'Źródło' and is_built_in = 1);

	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestInactive, @attrBusinessName, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestSuccess, @attrBusinessName, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestFailed, @attrBusinessName, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestNotStarted, @attrBusinessName, 1)
	
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestInactive, @attrTestDescription, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestSuccess, @attrTestDescription, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestFailed, @attrTestDescription, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestNotStarted, @attrTestDescription, 1)
	
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestInactive, @attrTestDefinition, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestSuccess, @attrTestDefinition, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestFailed, @attrTestDefinition, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestNotStarted, @attrTestDefinition, 1)
	
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestInactive, @attrSamplingMethod, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestSuccess, @attrSamplingMethod, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestFailed, @attrSamplingMethod, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestNotStarted, @attrSamplingMethod, 1)
	
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestInactive, @attrAdditionalInfo, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestSuccess, @attrAdditionalInfo, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestFailed, @attrAdditionalInfo, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestNotStarted, @attrAdditionalInfo, 1)
	
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestInactive, @attrErrorThreshold, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestSuccess, @attrErrorThreshold, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestFailed, @attrErrorThreshold, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestNotStarted, @attrErrorThreshold, 1)
	
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestInactive, @attrCreateDate, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestSuccess, @attrCreateDate, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestFailed, @attrCreateDate, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestNotStarted, @attrCreateDate, 1)
	
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestInactive, @attrModifyDate, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestSuccess, @attrModifyDate, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestFailed, @attrModifyDate, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestNotStarted, @attrModifyDate, 1)
	
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestInactive, @attrActivateDate, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestSuccess, @attrActivateDate, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestFailed, @attrActivateDate, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestNotStarted, @attrActivateDate, 1)
	
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestInactive, @attrDeactivateDate, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestSuccess, @attrDeactivateDate, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestFailed, @attrDeactivateDate, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestNotStarted, @attrDeactivateDate, 1)
	
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestInactive, @attrFunctionName, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestSuccess, @attrFunctionName, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestFailed, @attrFunctionName, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestNotStarted, @attrFunctionName, 1)
	
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestInactive, @attrErrorFuncitonName, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestSuccess, @attrErrorFuncitonName, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestFailed, @attrErrorFuncitonName, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestNotStarted, @attrErrorFuncitonName, 1)
	
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestInactive, @attrSource, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestSuccess, @attrSource, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestFailed, @attrSource, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestNotStarted, @attrSource, 1)
	
end;
go

if exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.sp_verticalize_node_attrs_dqm_test') and type in (N'P', N'PC'))
drop procedure dbo.sp_verticalize_node_attrs_dqm_test
go

create procedure dbo.sp_verticalize_node_attrs_dqm_test(@optNodeFilter varchar(max))
as
begin
	set nocount on
	
	declare @sqlText varchar(max) = '(select te.test_node_id, te.description, te.long_name, te.benchmark_definition, te.sampling_method, te.additional_information, te.error_threshold, te.create_date, te.modify_date, te.activate_date, te.deactivate_date, def.description as source_name, bn1.name as proc_name, bn2.name as err_proc_name
		from bik_dqm_test te 
		inner join bik_data_source_def def on def.id = te.source_id
		inner join bik_node bn1 on bn1.id = te.procedure_node_id
		left join bik_node bn2 on bn2.id = te.error_procedure_node_id
		where te.is_deleted = 0)';
	
	exec sp_verticalize_node_attrs_one_table @sqlText, 'test_node_id', 'description, long_name, benchmark_definition, sampling_method, additional_information, error_threshold, create_date, modify_date, activate_date, deactivate_date, proc_name, err_proc_name, source_name', 'Opis testu, Nazwa biznesowa, Definicja testu, Metoda próbkowania, Informacje dodatkowe, Próg błędu, Data utworzenia, Data modyfikacji, Data aktywacji, Data dezaktywacji, Funkcja testująca, Funkcja zwracająca błędne dane, Źródło', @optNodeFilter
end
go

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees') 
if not exists(select * from bik_node where tree_id = @tree_id and obj_id = 'admin:dqm')
begin
	exec sp_add_menu_node 'Admin', 'DQM', 'admin:dqm'
	exec sp_add_menu_node 'admin:dqm', 'Logi', '#admin:dqm:logs'
	exec sp_add_menu_node 'admin:dqm', 'Harmonogram i uruchamianie', '#admin:dqm:schedule'
	exec sp_add_menu_node 'admin:dqm', 'Konfiguracja', '#admin:dqm:config'
	exec sp_node_init_branch_id @tree_id, null
	
	insert into bik_translation(code, txt, lang, kind)
	values ('admin:dqm', 'DQM', 'en', 'node')
	insert into bik_translation(code, txt, lang, kind)
	values ('#admin:dqm:logs', 'Logs', 'en', 'node')
	insert into bik_translation(code, txt, lang, kind)
	values ('#admin:dqm:schedule', 'Schedule and execution', 'en', 'node')
	insert into bik_translation(code, txt, lang, kind)
	values ('#admin:dqm:config', 'Configuration', 'en', 'node')
end
go

if not exists(select * from bik_translation where kind = 'node' and lang = 'en' and code = 'admin:lisa')
begin
	insert into bik_translation(code, txt, lang, kind)
	values ('admin:lisa', 'NAS', 'en', 'node')
	insert into bik_translation(code, txt, lang, kind)
	values ('#admin:lisa:metadata', 'Metadata edit', 'en', 'node')
	insert into bik_translation(code, txt, lang, kind)
	values ('#admin:lisa:connection', 'Connection configuration', 'en', 'node')
	insert into bik_translation(code, txt, lang, kind)
	values ('#admin:lisa:logs', 'Logs', 'en', 'node')
end
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_dqm_request') and type in (N'U'))
begin
	create table bik_dqm_request (
		id bigint not null identity(1,1) primary key,
		test_id int not null references bik_dqm_test(id),
		test_node_id int not null references bik_node(id),
		case_count bigint null,
		error_count bigint null,
		error_percentage decimal(18, 8) not null default(0),
		passed int null,
		start_timestamp datetime not null,
		end_timestamp datetime null,
		is_manual int not null,
		is_deleted int not null default(0)
	);
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_dqm_request]') and name = N'idx_bik_dqm_request_test_node_id')
begin
	create index idx_bik_dqm_request_test_node_id on bik_dqm_request (test_node_id);
end;
go

if not exists (select 1 from bik_attr_def where is_built_in = 1 and name = 'Expected Result')
begin
	insert into bik_attr_def(name, attr_category_id, is_built_in, type_attr, hint)
	values ('Expected Result', (select id from bik_attr_category where is_built_in = 1 and name = 'DQC'), 1, 'shortText', '')
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_dqm_log') and type in (N'U'))
begin
	create table bik_dqm_log (
		id bigint not null identity(1,1) primary key,
		test_id int not null references bik_dqm_test(id),
		start_time datetime not null,
		finish_time datetime null,
		description varchar(max) null
	);
end;
go

if not exists(select * from bik_data_source_def where description = 'DQM')
begin
	insert into bik_data_source_def(description) values ('DQM')
end;
go

if not exists(select * from bik_data_source_def where description = 'Profile')
begin
	insert into bik_data_source_def(description) values ('Profile')
end;
go

exec sp_update_version '1.6.z7.1', '1.6.z7.2';
go