/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.lisa;

/**
 *
 * @author lbiegniewski
 */
public class LisaConstants {

    public static final String UNASSIGNED_CATEGORY_OBJ_ID_SUFFIX = "0";
    public static final String PARAM_UNASSIGNED_CATEGORY_NAME = "lisateradata.defaultUnassignedCategoryName";
    public static final String PARAM_TERADATA_URL = "lisateradata.url";
    public static final String PARAM_TERADATA_PORT = "lisateradata.port";
    public static final String PARAM_TERADATA_DATABASE = "lisateradata.database";
    public static final String PARAM_TERADATA_CHARSET = "lisateradata.charset";
    public static final String PARAM_TERADATA_TMODE = "lisateradata.tmode";
    public static final String PARAM_TERADATA_SCHEMA_NAME = "lisateradata.schemaName";
    public static final String OBJ_ID_SEPARATOR = ":";
    public static final String ROOT_OBJ_ID = "DWHDictionaryRoot";
    public static final String LISA_DATE_STING_FORMAT = "yyyy-MM-dd";
    public static final int VALIDATE_FROM_DICT = 2;
    public static final int VALIDATE_FROM_LIST = 1;
    public static final int NO_VALIDATED = 0;
    public static final Integer EDITABLE = 2;
    public static final Integer READ_ONLY = 1;
    public static final Integer NOT_VISIBLE = 0;
//    public static final Integer IMPORT_PACK_SIZE = 1000;
    public static final Integer EXPORT_PACK_SIZE = 10000;
    public static final String CATEGORIZATION_TYPE_DESCR = "opis_typ_kateg";
    public static final String CATEGORIZATION_TYPE_NAME = "nazwa_typ_kateg";
    public static final String METADATA_COLUMN_NULLABLE = "Nullable";
    public static final String METADATA_COLUMN_TYPE = "ColumnType";
    public static final String DICTIONARY_ID_COL = "id_slownik";
    public static final String CATEGORIZATION_TYPE_ID = "id_typ_kateg";
    public static final String CATEGORY_TYPE_DEF_ID_COL = "id_typ_kateg_def";
    public static final String METADATA_COLUMN_LENGTH = "ColumnLength";
    public static final String CATEGORY_NAME = "nazwa_kateg";
    public static final String METADATA_COLUMN_DECIMALTOTALDIGITS = "DecimalTotalDigits";
    public static final String IS_NULLABLE = "Y";
    public static final String METADATA_COLUMN_NAME = "ColumnName";
    public static final String CATEGORY_ID = "id_kateg";
    public static final String METADATA_COLUMN_ID = "ColumnId";
    public static final String METADATA_COLUMN_DECIMALFRACTIONALDIGITS = "DecimalFractionalDigits";
    public static final String IMPORT_ACTION_ADD = "Add";
    public static final String IMPORT_ACTION_DELETE = "Delete";
    public static final String IMPORT_ACTION_UPDATE = "Update";
    public static final String IMPORT_ACTION_INVALID = "Invalid";
    public static final String IMPORT_ACTION_PK_EMPTY = "PkEmpty";
    public static final String IMPORT_ACTION_PK_DUPLICATE = "PkDup";
    public static final String PK_VALUE_SEPERATOR = ",";
    public static final int GRID_PAGE_SIZE = 25;
    public static final String NULL_VALUE = "NULL";

    public static enum ImportMode {

        ADD, OVERWRITE, DELETE
    }
}
