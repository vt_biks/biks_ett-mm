/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author ctran
 */
public class BIAArchiveLogBean implements Serializable {

    public int id;
    public String jobCuid;
    public Date startTime;
    public Date endTime;
    public String status;
    public int fileCnt;
}
