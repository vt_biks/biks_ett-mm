/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.common;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tflorczak
 */
public class SchedulePumpBean implements Serializable {

    public int id;
    public String source;
    public int hour;
    public int minute;
    public int interval;
    public Date dateStarted;
    public boolean isSchedule;
    public int priority;
    public String instanceName;
}
