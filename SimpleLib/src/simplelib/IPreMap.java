/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package simplelib;

import java.util.Collection;

/**
 *
 * @author wezyr
 */
public interface IPreMap<K, V> {
    public Collection<K> keySet();
    public V get(K key);
}
