/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.user.client.Window;
import java.util.List;
import pl.fovis.foxygwtcommons.i18n.I18n;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author administrator
 */
public class FileUploadDialogForPhoto extends FileUploadDialogBase {

    public IParametrizedContinuation<FoxyFileUpload> saveCont;
    public static List allowedExst;

    @Override
    protected String getDialogCaption() {
        return I18n.dodajZdjecie.get() /* I18N:  */;
    }

    @Override
    protected String getActionButtonCaption() {
        //return null;
        return I18n.zaladuj.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {
        setFfu(new FoxyFileUpload(new IContinuation() {
            public void doIt() {
                
                if (getFfu().isUploadIsAllowed()) {
                    saveCont.doIt(getFfu());
                } else {
                    Window.alert(I18n.wrongFileExtension.get() /* I18N:  */);
                }
                hideDialog();
            }
        }, false, allowedExst));
        main.add(getFfu());
    }

    public void buildAndShowDialog(IParametrizedContinuation<FoxyFileUpload> saveCont, List<String> allowedExst) {
        this.saveCont = saveCont;
        this.allowedExst = allowedExst;
        super.buildAndShowDialog();
    }
}
