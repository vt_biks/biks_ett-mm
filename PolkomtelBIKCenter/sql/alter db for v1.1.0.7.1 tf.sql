﻿exec sp_check_version '1.1.0.7.1';
go

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
update bik_joined_objs
set type = 1
--select *
from bik_joined_objs jo inner join bik_node ns on jo.src_node_id = ns.id
inner join bik_node nd on jo.dst_node_id = nd.id
where ns.is_deleted = 0 and ns.linked_node_id is not null and nd.is_deleted = 0 and ns.disable_linked_subtree = 1
and not exists (select 1 from bik_joined_objs jo2 where jo2.dst_node_id = jo.src_node_id and jo2.src_node_id = jo.dst_node_id)
and ns.node_kind_id in (select id from bik_node_kind where code in ('Measure', 'Dimension', 'Detail'))
and ns.tree_id = (select id from bik_tree where code = 'Reports')
and nd.tree_id in (select id from bik_tree where code in ('ObjectUniverses', 'Connections'))


---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------

exec sp_update_version '1.1.0.7.1', '1.1.1';
go
