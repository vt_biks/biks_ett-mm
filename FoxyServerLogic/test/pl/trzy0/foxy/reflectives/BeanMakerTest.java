/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.reflectives;

import pl.trzy0.foxy.serverlogic.BeanMaker;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author wezyr
 */
public class BeanMakerTest {

    public BeanMakerTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    static public class TestClass {
        public String f1;
        public String forXor;
        public Integer andnotor;
        public Boolean yYy;
    }
    
    @Test
    public void testPrepareStandardColumnToPropNameMapping() {
        String[] colNames = {"F1", "FOR_Xor", "_andNot__OR", "xX"};
        Map<String, String> res = BeanMaker.prepareStandardColumnToPropNameMapping(
                Arrays.asList(colNames), TestClass.class);
        
        System.out.println(res);
        
        assertEquals(3, res.size());
        assertEquals("f1", res.get("F1"));
        assertEquals("forXor", res.get("FOR_Xor"));
        assertEquals("andnotor", res.get("_andNot__OR"));
    }
    
    private Map<String, Object> packInMapForTestClass(String f1, String forXor,
        Integer andnotor, String xX) {
        Map<String, Object> res = new HashMap<String, Object>();
        res.put("F1", f1);
        res.put("FOR_Xor", forXor);
        res.put("_andNot__OR", andnotor);
        res.put("xX", xX);
        return res;
    }
    
    @Test
    public void testCreateBean() {
        Map<String, Object> testMap = packInMapForTestClass("fff", "xor", -1, "xXx");
        BeanMaker<TestClass> bm = new BeanMaker<TestClass>(testMap.keySet(), TestClass.class);
        TestClass val = bm.createBean(testMap);
        
        assertEquals("fff", val.f1);
        assertEquals("xor", val.forXor);
        assertEquals(new Integer(-1), val.andnotor);
        assertNull(val.yYy);
    }
    
    @Test
    public void testCreateBeans() {
        List<Map<String, Object>> testList = new ArrayList<Map<String, Object>>();
        testList.add(packInMapForTestClass("fff", "xor", -1, "xXx"));
        testList.add(packInMapForTestClass("ggg", "NAND", 1000, "uuu"));

        List<TestClass> vals = BeanMaker.createBeansWithColsAsInFirstOne(testList, TestClass.class);

        assertEquals(2, vals.size());
        
        TestClass val = vals.get(0);
        assertEquals("fff", val.f1);
        assertEquals("xor", val.forXor);
        assertEquals(new Integer(-1), val.andnotor);
        assertNull(val.yYy);
        
        val = vals.get(1);
        assertEquals("ggg", val.f1);
        assertEquals("NAND", val.forXor);
        assertEquals(new Integer(1000), val.andnotor);
        assertNull(val.yYy);        
    }
}
