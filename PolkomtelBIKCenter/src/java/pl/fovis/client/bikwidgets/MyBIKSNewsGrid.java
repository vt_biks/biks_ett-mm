/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.NewsActionExtractor;
import pl.fovis.client.entitydetailswidgets.IGridBeanAction;
import pl.fovis.common.NewsBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;
import simplelib.SimpleDateUtils;

/**
 *
 * @author mmastalerz
 * bf->pozbyc sie jeszcze extractora!
 */
public class MyBIKSNewsGrid extends MyBIKSGridDoubleRowBase<NewsBean> implements IPaginatedItemsDisplayer<NewsBean> {

    public String getIconUrl(NewsBean n) {
        return "images/news.gif" /* I18N: no */;
    }

    public void createActionList(int row, final NewsBean n) {
        NewsActionExtractor extractor = new NewsActionExtractor();
        extractor.setDataChangedHandler(new IParametrizedContinuation<Pair<Boolean, Boolean>>() {

            public void doIt(Pair<Boolean, Boolean> param) {
                if (!param.v1) {
                    if (filteredItems != allItems) {
                        filteredItems.remove(n);
                        allItems.remove(n);
                    } else {
                        filteredItems.remove(n);
                    }
                }
                rebuildTab(param.v1, param.v2);
            }
        });
        List<IGridBeanAction<NewsBean>> l = extractor.createActionList();
        for (IGridBeanAction<NewsBean> act : l) {
            gp.setWidget(row, columnsCnt++, createActionBtn(act, act.getActionIconUrl(n), n));
        }
    }

    ;

    protected void addOrRefreshTitles(final int row, final NewsBean n) {
        String s = BaseUtils.encodeForHTMLTag(n.title, true, 1);
        HTML subject = new HTML(n.isRead ? s : "<b>" + s + "</b>" + "<font color=red><i> (" + I18n.nowy.get() /* I18N:  */ + ") </i></font>");
        subject.setStyleName("News" /* i18n: no:css-class */ + "-newsTitle");
        titles.put(row, subject);
        subject.addClickHandler(createClickHandlerForRow(row, n));
    }

    protected void createRowFromBean(int row, NewsBean n) {
        columnsCnt = 0;
        addWidgetsToRow(row, n);
        gp.setWidget(row, columnsCnt++, addOrRefreshAuthor(n));
        gp.setWidget(row, columnsCnt++, addOrRefreshDate(n));
        createActionList(row, n);
        gp.getRowFormatter().setStyleName(row, getBackgroundStyleForRowInGrid(row, 4));
    }

    protected Widget getWidgetToRowExpanded(NewsBean n) {

        HTML text = new HTML(n.text/*BaseUtils.encodeForHTMLTag(n.text, true, 1)*/);
        text.setStyleName("News" /* i18n: no:css-class */ + "-newsText");
        return text;
    }

    protected void setUpGridColumnsWidths(FlexTable gp) {
        gp.setWidth("100%");
        gp.getColumnFormatter().setWidth(0, "16px");
        gp.getColumnFormatter().setWidth(1, "16px");
        gp.getColumnFormatter().setWidth(3, "200px");
        gp.getColumnFormatter().setWidth(4, "150px");
        gp.getColumnFormatter().setWidth(5, "26px");
        gp.getColumnFormatter().setWidth(6, "26px");
    }

    protected HTML addOrRefreshDate(NewsBean n) {
        HTML dateAddedHTML = new HTML(SimpleDateUtils.toCanonicalString(n.dateAdded));
        dateAddedHTML.setStyleName("News" /* i18n: no:css-class */ + "-dateAdded");
        return dateAddedHTML;
    }

    protected HTML addOrRefreshAuthor(NewsBean n) {
        HTML authorHTML = new HTML(n.authorName);
        authorHTML.setStyleName("News" /* i18n: no:css-class */ + "-authorName");
        return authorHTML;
    }

    protected ClickHandler createClickHandlerForRow(final int newsNr, final NewsBean n) {
        final int row = newsNr * 2;
        ClickHandler click = new ClickHandler() {

            public void onClick(ClickEvent event) {
                String s = buttons.get(newsNr).getStyleName();
                buttons.get(newsNr).setStyleName(s.equals(dartAtStyle()) ? dartDownStyle() : dartAtStyle());
                //ww->mybiks: coś jest mocno nie tak w używaniu tutaj wyrażenia
                // "row * 2 + 1" i nie chodzi o to, że ono dwa razy jest powielone
                // tylko o jego magiczność - dlaczego jak raz takie wyrażenie tutaj?
                // od czego zależy "* 2", a od czego te "+ 1"?
                // mm-> row jest zla nazwa, tutaj ta zmienna oznacza numer NewsBeana do wyświetlania,
                // rozdzielam na dwie zmienne row - nr wiersza w gridzie, a newsNr - numer aktualnego beana,
                // wierszy jest 2 razy wiecej, a +1 wybiera wiersz z opisem aktualności
                gp.getRowFormatter().setVisible(row + 1, !gp.getRowFormatter().isVisible(row + 1));
                setUpGridColumnsWidths(gp);
                if (!n.isRead) {
                    BIKClientSingletons.getService().markNewsAsReadAndGetNew(n.id, new StandardAsyncCallback<NewsBean>() {

                        public void onSuccess(NewsBean result) {
                            //no-op
                            //n.isRead = result.isRead;
                        }
                    });
                    n.isRead = true;
                    //ww->mybiks: o, a tutaj "row * 2" i też dwa razy, czemu nie
                    // "+ 1" jak powyżej?
                    // a czy czasem może być coś innego niż "row * 2"?
                    createRowFromBean(row, n);
                    addRowExpandedToItem(row, n);
                    itemsProvider.itemsChanged();
                }
            }
        };
        return click;
    }
}
