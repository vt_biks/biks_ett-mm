/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts.luceneUtils;

import commonlib.LameUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import simplelib.logging.ILameLogger;

/**
 *
 * @author pmielanczuk
 */
public class QueryWords {

    private static final ILameLogger logger = LameUtils.getMyLogger();

    public List<WordWithOtherForms> plainWords;
    public List<WildcardWord> wildcardWords;

    @Override
    public String toString() {
        return "{" + "plainWords=" + plainWords + ", wildcardWords=" + wildcardWords + '}';
    }

    public boolean matches(Set<String> words) {
        if (words.size() < plainWords.size()) {
            return false;
        }

        for (WordWithOtherForms wwof : plainWords) {

            boolean contains = false;
            for (String plainWord : wwof.words) {
                if (words.contains(plainWord)) {
                    contains = true;
                    break;
                }
            }

            if (!contains) {
                if (logger.isDebugEnabled()) {
                    logger.debug("words: " + words + " do not containt any of words: " + wwof.words);
                }
                return false;
            }
        }

        if (wildcardWords.isEmpty()) {
            return true;
        }

        Set<WildcardWord> notMatched = new HashSet<WildcardWord>(wildcardWords);

        for (String word : words) {

            Set<WildcardWord> matching = new HashSet<WildcardWord>();

            for (WildcardWord wcw : notMatched) {
                if (wcw.matches(word)) {
                    matching.add(wcw);
                    if (notMatched.size() - matching.size() <= 0) {
                        return true;
                    }
                }
            }

            notMatched.removeAll(matching);
        }

        if (!notMatched.isEmpty()) {
            for (WildcardWord wcw : notMatched) {
                if (logger.isDebugEnabled()) {
                    logger.debug("words: " + words + " do not containt wildcard word: " + wcw.pattern);
                }
            }
        }

        return notMatched.isEmpty();
    }

    @Deprecated
    public static QueryWords make(Collection<String> words) {
        QueryWords res = new QueryWords();

        res.plainWords = new ArrayList<WordWithOtherForms>();
        res.wildcardWords = new ArrayList<WildcardWord>();

        Set<String> plainWords = new HashSet<String>();

        for (String word : words) {
            WildcardWord wcw = WildcardWord.splitWildcardWord(word);
            if (wcw == null) {
                if (plainWords.add(word)) {
                    res.plainWords.add(new WordWithOtherForms(word));
                }
            } else {
                res.wildcardWords.add(wcw);
            }
        }

        return res;
    }
}
