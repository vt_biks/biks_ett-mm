/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.SapBWBean;
import simplelib.BaseUtils;
import simplelib.SimpleDateUtils;

/**
 *
 * @author tflorczak
 */
public class SapBWWidget extends EntityDetailsWidgetFlatBase {

    protected VerticalPanel mainVp;
    protected IMainHeaderSharedTable imhst;
    public static final String SAPBW_TECHNICAL_NAME = "$$BIKS_TEMP_ARCHIWUM$$";
    public static final Set<String> objectsSet = new HashSet<String>() {

        {
            add(BIKGWTConstants.NODE_KIND_SAPBW_UNI);
            add(BIKGWTConstants.NODE_KIND_SAPBW_TIM);
            add(BIKGWTConstants.NODE_KIND_SAPBW_KYF);
            add(BIKGWTConstants.NODE_KIND_SAPBW_DPA);
            add(BIKGWTConstants.NODE_KIND_SAPBW_CHA);
        }
    };

    public SapBWWidget(IMainHeaderSharedTable imhst) {
        this.imhst = imhst;
    }

    @Override
    public void displayData() {
        final EntityDetailsDataBean data = getData();
        SapBWBean bean = data.sapbwExtradata;
        String oryginalArea = data.sapbwOriginalProvidersArea;
        if (bean != null) {
            if (objectsSet.contains(data.node.nodeKindCode)) {
                imhst.addTranslatedRow("Nazwa techniczna" /* I18N: no */, trimTechnicalName(data.node.objId));
            } else if (!data.node.objId.equals(SAPBW_TECHNICAL_NAME)) {
                imhst.addTranslatedRow("Nazwa techniczna" /* I18N: no */, data.node.objId);
            }
            imhst.addTranslatedRow("Osoba odpowiedzialna" /* i18n: no */, bean.queryOwnerName);
            Date parsedDate = parseStringToDate(bean.queryUpdateTime);
            imhst.addTranslatedRow("Ostatnia zmiana" /* I18N: no */, parsedDate != null ? SimpleDateUtils.toCanonicalString(parsedDate) : bean.queryUpdateTime);
            imhst.addTranslatedRow("Ostatnio zmieniony przez" /* I18N: no */, bean.queryLastEdit);
        }
        if (isNodeLinked && oryginalArea != null) {
            imhst.addTranslatedRow("Występuje w obszarze" /* i18n: no */, null, BIKClientSingletons.createLinkAsAnchor(oryginalArea, data.node.treeCode, data.node.id));
        }
    }

    @Override
    public Widget buildWidgets() {
        mainVp = new VerticalPanel();
        return mainVp;
    }

    @Override
    public boolean hasContent() {
        return (getData().sapbwExtradata != null);
    }

    protected Date parseStringToDate(String dateInString) {
        if (dateInString != null && dateInString.length() == 14) {
            int year = BaseUtils.tryParseInt(dateInString.substring(0, 4));
            int month = BaseUtils.tryParseInt(dateInString.substring(4, 6));
            int day = BaseUtils.tryParseInt(dateInString.substring(6, 8));
            int hour = BaseUtils.tryParseInt(dateInString.substring(8, 10));
            int minute = BaseUtils.tryParseInt(dateInString.substring(10, 12));
            int second = BaseUtils.tryParseInt(dateInString.substring(12));
            return SimpleDateUtils.newDate(year, month, day, hour, minute, second);
        }
        return null;
    }

    protected String trimTechnicalName(String objId) {
        int indexOf = objId.indexOf("|");
        return new String(objId.substring(indexOf + 1));
    }
}
