-- wat database

-- Folder wynagrodze�
update bik_node_kind
set attr_calculating_expressions =
'select node_id, case when rozd < 0 then ''<html><span style="background-color:#ff0000">'' + cast(rozd as varchar(100)) + ''</span></html>'' else cast(rozd as varchar(100)) end as [Kwota do rozdysponowania], [Bud�et] - rozd as [Wykorzystanie bud�etu (z�)], case when wb >= 80 and wb <= 100 then ''<html><span style="background-color:#ffff40">'' + cast(wb as varchar(100)) + ''</span></html>'' when wb > 100 then ''<html><span style="background-color:#ff0000">'' + cast(wb as varchar(100)) + ''</span></html>'' else cast(wb as varchar(100)) end as [Wykorzystanie bud�etu [%]]]
from
  (select (select [Bud�et] - sum(case when isnumeric(value) = 1 then cast(replace(value,'','', ''.'') as numeric(15,2)) else 0 end)         from bik_attribute_linked av inner join bik_node bn on av.node_id = bn.id and bn.is_deleted = 0        where av.attribute_id = 1066 and bn.parent_node_id = x.node_id       ) as rozd,         
	  case when [Bud�et] = 0 then 0 else cast(coalesce((select sum(case when isnumeric(value) = 1 then cast(replace(value,'','', ''.'') as numeric(15,2)) else 0 end)         from bik_attribute_linked av inner join bik_node bn on av.node_id = bn.id and bn.is_deleted = 0       where av.attribute_id = 1066 and bn.parent_node_id = x.node_id          ), 0) * 100 / [Bud�et] as numeric(15,2)) end    as wb, [Bud�et]
	  , node_id
  from ${src_vals}$ as x
) x
'
where code = 'dtk_Folderwynagrodzen'

-- Folder zapotrzebowa�
update bik_node_kind
set attr_calculating_expressions =
' select node_id, [Kwota planowana], [Kwota zrealizowana], [Wykorzystanie bud�etu po planowaniu], [Wykorzystanie bud�etu po zrealizowaniu], 
case when wbp >= 80 and wbp <= 100 then ''<html><span style="background-color:#ffff40">'' + cast(wbp as varchar(100)) + ''</span></html>'' when wbp > 100 then ''<html><span style="background-color:#ff0000">'' + cast(wbp as varchar(100)) + ''</span></html>'' else cast(wbp as varchar(100)) end as [Wykorzystanie bud�etu po planowaniu [%]]],
case when wbz >= 80 and wbz <= 100 then ''<html><span style="background-color:#ffff40">'' + cast(wbz as varchar(100)) + ''</span></html>'' when wbz > 100 then ''<html><span style="background-color:#ff0000">'' + cast(wbz as varchar(100)) + ''</span></html>'' else cast(wbz as varchar(100)) end as [Wykorzystanie bud�etu po zrealizowaniu [%]]]
from (
  select(  select sum(case when isnumeric(value) = 1 then cast(replace(value,'','', ''.'') as numeric(15,0)) else 0 end)         
  from bik_attribute_linked av inner join bik_node bn on av.node_id = bn.id and bn.is_deleted = 0        
  where av.attribute_id = 1132 and bn.parent_node_id = x.node_id ) 
 as [Kwota planowana],   
  (select sum(case when isnumeric(value) = 1 then cast(replace(value,'','', ''.'') as numeric(15,2)) else 0 end)         
  from bik_attribute_linked av inner join bik_node bn on av.node_id = bn.id and bn.is_deleted = 0        
  where av.attribute_id = 1133 and bn.parent_node_id = x.node_id)
 as [Kwota zrealizowana],  
  (select [Bud�et] - sum(case when isnumeric(value) = 1 then cast(replace(value,'','', ''.'') as numeric(15,2)) else 0 end)         
  from bik_attribute_linked av inner join bik_node bn on av.node_id = bn.id and bn.is_deleted = 0        
  where av.attribute_id = 1132 and bn.parent_node_id = x.node_id)
 as [Wykorzystanie bud�etu po planowaniu],  
  (select [Bud�et] - sum(case when isnumeric(value) = 1 then cast(replace(value,'','', ''.'') as numeric(15,2)) else 0 end)         
  from bik_attribute_linked av inner join bik_node bn on av.node_id = bn.id and bn.is_deleted = 0        
  where av.attribute_id = 1133 and bn.parent_node_id = x.node_id) 
 as [Wykorzystanie bud�etu po zrealizowaniu], 
  case when [Bud�et] = 0 then 0 else cast(coalesce((select [Bud�et] - sum(case when isnumeric(value) = 1 then cast(replace(value,'','', ''.'') as numeric(15,2)) else 0 end)         
  from bik_attribute_linked av inner join bik_node bn on av.node_id = bn.id and bn.is_deleted = 0        
  where av.attribute_id = 1132 and bn.parent_node_id = x.node_id), 0) * 100 / [Bud�et] as numeric(15,2)) end   
 as wbp, 
  case when [Bud�et] = 0 then 0 else cast(coalesce((select [Bud�et] - sum(case when isnumeric(value) = 1 then cast(replace(value,'','', ''.'') as numeric(15,2)) else 0 end)         
  from bik_attribute_linked av inner join bik_node bn on av.node_id = bn.id and bn.is_deleted = 0        
  where av.attribute_id = 1133 and bn.parent_node_id = x.node_id), 0) * 100 / [Bud�et] as numeric(15,2)) end   
 as wbz, 
  node_id  from ${src_vals}$ as x
) x
'
where code = 'dtk_Folderzapotrzebowan'

-- Zadanie
update bik_node_kind
set attr_calculating_expressions =
'select node_id, case when rozd < 0 then ''<html><span style="background-color:#ff0000">'' + cast(rozd as varchar(100)) + ''</span></html>'' else cast(rozd as varchar(100)) end as [Kwota do rozdysponowania], [Bud�et] - rozd as [Wykorzystanie bud�etu (z�)], case when wbtask >= 80 and wbtask <= 100 then ''<html><span style="background-color:#ffff40">'' + cast(wbtask as varchar(100)) + ''</span></html>'' when wbtask > 100 then ''<html><span style="background-color:#ff0000">'' + cast(wbtask as varchar(100)) + ''</span></html>'' else cast(wbtask as varchar(100)) end as [Wykorzystanie bud�etu [%]]]
from
  (select 
  (select [Bud�et] - coalesce(sum(case when isnumeric(value) = 1 then cast(replace(value,'','', ''.'') as numeric(15,2)) else 0 end), 0)
  from bik_attribute_linked av inner join bik_node bn on av.node_id = bn.id and bn.is_deleted = 0        
  where av.attribute_id = 1056 and bn.parent_node_id = x.node_id)
 as rozd,
  case when [Bud�et] = 0 then 0 else cast(coalesce((select sum(case when isnumeric(value) = 1 then cast(replace(value,'','', ''.'') as numeric(15,2)) else 0 end)         
  from bik_attribute_linked av inner join bik_node bn on av.node_id = bn.id and bn.is_deleted = 0       
  where av.attribute_id = 1056 and bn.parent_node_id = x.node_id          
  ), 0) * 100 / [Bud�et] as numeric(15,2)) end    
 as wbtask  , [Bud�et], node_id
  from ${src_vals}$ as x
) x
'
where code = 'dtk_TaxonomyEntity'

-- Wniosek o p�atno��
update bik_node_kind
set attr_calculating_expressions =
'(
select sum(case when isnumeric(value) = 1 then cast(replace(value,'','', ''.'') as numeric(15,2)) else 0 end)       
from bik_attribute_linked av inner join bik_joined_objs jo on av.node_id = jo.dst_node_id
inner join bik_node as bnsrc on bnsrc.id = jo.dst_node_id
where av.attribute_id in (444, 1066) and bnsrc.is_deleted = 0 and jo.src_node_id = x.node_id        
) as [Warto�� brutto WNP]
'
where code = 'dtk_TaxonomyEntityLeaf'

-- Lista p�ac
update bik_node_kind set attr_dependent_nodes_sql = 
'select n.id from ${node_ids}$ as src inner join bik_joined_objs jo on jo.src_node_id = src.node_id inner join    
 bik_node n on n.id = jo.dst_node_id and n.is_deleted = 0 and n.node_kind_id in (114, 207) 
union all
 select distinct n.parent_node_id from ${node_ids}$ as src inner join bik_node n on n.id = src.node_id and n.is_deleted = 0
'
where code = 'dtk_Listaplac'


/*
	PROJEKTY	
*/

--14 Kategoria Projektowe
insert into bik_attr_def ( name,attr_category_id,is_deleted,is_built_in,type_attr)
values('Bud�et ca�o�ciowy',14,0,0,'calculation');
insert into bik_attr_def ( name,attr_category_id,is_deleted,is_built_in,type_attr)
values('Planowana kwota do rozdysponowania',14,0,0,'calculation');

create table #node_kind_ids (id int not null);
 
	insert into #node_kind_ids (id) 
	select id from bik_node_kind where code in('dtk_Project','dtk_MileStone');
 
	declare @attr_def_id int = (select id from bik_attr_def where name='Bud�et');
	
 -- usun podlinkowanie do budzetu dla projektu i kategorii
delete from bik_attribute_linked 
	where 
		node_id in(select id from bik_node where node_kind_id in(select id from #node_kind_ids)) 
	and 
		attribute_id in(select id from bik_attribute where attr_def_id =@attr_def_id
		);

delete from bik_attribute where node_kind_id in(select id from #node_kind_ids) and attr_def_id=@attr_def_id;

-- przy��czenie atrybutow do node_kind
	insert into bik_attribute( node_kind_id,attr_def_id,is_deleted)
	select #node_kind_ids.id,ba.id,0 from bik_attr_def ba,#node_kind_ids where name='Planowana kwota do rozdysponowania'
	
	insert into bik_attribute( node_kind_id,attr_def_id,is_deleted)
	select #node_kind_ids.id,ba.id,0 from bik_attr_def ba,#node_kind_ids where name='Bud�et ca�o�ciowy'
	
	
drop table #node_kind_ids;


update bik_node_kind set attr_dependent_nodes_sql='select distinct n.parent_node_id from ${node_ids}$ as src inner join bik_node n on n.id = src.node_id and n.is_deleted = 0'
where code='dtk_MileStone'

/* Folder Kategorii
Dla node_kind code = dtk_TaxonomyEntity
	 select ba.id,bad.name,* from bik_attribute ba join bik_attr_def bad
	 on ba.attr_def_id=bad.id where ba.node_kind_id in 
	 (select id from bik_node_kind where code='dtk_TaxonomyEntity')
425	Bud�et
436	Kwota do rozdysponowania
454	Wykorzystanie bud�etu [%]
1151 Wykorzystanie bud�etu [zl]
*/
update bik_node_kind
	set attr_calculating_expressions =
	' select node_id
			, cast(budzet as varchar(100)) as [Bud�et ca�o�ciowy] 
			, case when rozd < 0 then ''<html><span style="background-color:#ff0000">'' + cast(rozd as varchar(100)) + ''</span></html>'' else cast(rozd as varchar(100)) end as [Kwota do rozdysponowania]
			, wyk as [Wykorzystanie bud�etu (z�)]
			, case 
				when wbtask >= 80 and wbtask <= 100 then ''<html><span style="background-color:#ffff40">'' + cast(wbtask as varchar(100)) + ''</span></html>'' 
				when wbtask > 100 then ''<html><span style="background-color:#ff0000">'' + cast(wbtask as varchar(100)) + ''</span></html>'' 
				else cast(wbtask as varchar(100)) end as [Wykorzystanie bud�etu [%]]]
			, plankw as [Planowana kwota do rozdysponowania]
			from (select node_id, budzet , rozd , cast((budzet-rozd) as varchar(100)) as wyk, cast(case when budzet = 0 then 0 else coalesce((budzet-rozd),0)*100/budzet end as numeric(15,2)) as wbtask, plankw 
				from
					(select 
						(select 
							coalesce(sum(case when isnumeric(value) = 1 then cast(replace(value,'','', ''.'') as numeric(15,2)) else 0 end), 0)
						from 
							bik_attribute_linked av 
							inner join bik_node bn 
							on av.node_id = bn.id and bn.is_deleted = 0        
							where av.attribute_id = 425 and bn.parent_node_id = x.node_id
						)
						as budzet ,
						(select 
							coalesce(sum(case when isnumeric(value) = 1 then cast(replace(value,'','', ''.'') as numeric(15,2)) else 0 end), 0)
						from (select replace(replace (value,''<html><span style="background-color:#ff0000">'',''''),''</span></html>'','''') as value 
							from bik_attribute_linked av inner join bik_node bn on av.node_id = bn.id and bn.is_deleted = 0        
							where av.attribute_id = 436 and 
							bn.parent_node_id = x.node_id)val
						)
						as rozd,
						(
						select 
						coalesce(sum(case when isnumeric(value) = 1 then cast(replace(value,'','', ''.'') as numeric(15,2)) else 0 end), 0)
						from (		
							select 
								case when isnumeric(value) = 1 then  (
									case when cast(replace(value,'','', ''.'') as numeric(15,2))=0 then 
										(select value from bik_attribute_linked where node_id=	val.node_id and attribute_id = 425) 
									else 	
										(select value from bik_attribute_linked where node_id=	val.node_id and attribute_id = 1151)
									end)
								end
								as value 
							from 
								(select 
									node_id, 
									replace(replace (value,''<html><span style="background-color:#ff0000">'',''''),''</span></html>'','''') as value  
								from 
									bik_attribute_linked av 
								inner join bik_node bn 
								on av.node_id = bn.id and bn.is_deleted = 0        
								where av.attribute_id = 454 and bn.parent_node_id = x.node_id
								)val	
							) val2	
						) as plankw,
						node_id
						from ${src_vals}$ as x
				) x )y'
			where code = 'dtk_MileStone'

/* Folder Projekty
Dla node_kind code = dtk_MileStone
	 select ba.id,bad.name,* from bik_attribute ba join bik_attr_def bad
	 on ba.attr_def_id=bad.id where ba.node_kind_id in 
	 (select id from bik_node_kind where code='dtk_MileStone')
	 
1182	Bud�et ca�o�ciowy
468 	Kwota do rozdysponowania
1180	Planowana kwota do rozdysponowania
*/



update bik_node_kind
set attr_calculating_expressions =
		' select node_id
			, cast(budzet as varchar(100)) as [Bud�et ca�o�ciowy] 
			, case when rozd < 0 then ''<html><span style="background-color:#ff0000">'' + cast(rozd as varchar(100)) + ''</span></html>'' else cast(rozd as varchar(100)) end as [Kwota do rozdysponowania]
			, wyk as [Wykorzystanie bud�etu (z�)]
			, case 
				when wbtask >= 80 and wbtask <= 100 then ''<html><span style="background-color:#ffff40">'' + cast(wbtask as varchar(100)) + ''</span></html>'' 
				when wbtask > 100 then ''<html><span style="background-color:#ff0000">'' + cast(wbtask as varchar(100)) + ''</span></html>'' 
				else cast(wbtask as varchar(100)) end as [Wykorzystanie bud�etu [%]]]
			, plankw as [Planowana kwota do rozdysponowania]
			from (select node_id, budzet , rozd , cast((budzet-rozd) as varchar(100)) as wyk, cast(case when budzet = 0 then 0 else coalesce((budzet-rozd),0)*100/budzet end as numeric(15,2)) as wbtask, plankw 
					from
						(select 
							(select 
								coalesce(sum(case when isnumeric(value) = 1 then cast(replace(value,'','', ''.'') as numeric(15,2)) else 0 end), 0)
							from 
								bik_attribute_linked av 
								inner join bik_node bn 
								on av.node_id = bn.id and bn.is_deleted = 0        
								where av.attribute_id = 1182 and bn.parent_node_id = x.node_id
							)
							as budzet ,
							(select 
								coalesce(sum(case when isnumeric(value) = 1 then cast(replace(value,'','', ''.'') as numeric(15,2)) else 0 end), 0)
							from (select replace(replace (value,''<html><span style="background-color:#ff0000">'',''''),''</span></html>'','''') as value 
								from bik_attribute_linked av inner join bik_node bn on av.node_id = bn.id and bn.is_deleted = 0        
								where av.attribute_id = 468 and 
								bn.parent_node_id = x.node_id)val
							)
							as rozd,
							
							(select 
								coalesce(sum(case when isnumeric(value) = 1 then cast(replace(value,'','', ''.'') as numeric(15,2)) else 0 end), 0)
							from 
								bik_attribute_linked av 
								inner join bik_node bn 
								on av.node_id = bn.id and bn.is_deleted = 0        
								where av.attribute_id = 1180 and bn.parent_node_id = x.node_id
							)
							as plankw ,			
							node_id
							from ${src_vals}$ as x
					) x ) y'
where code = 'dtk_Project'

/*
	WYNAGRODZENIA
	23- kategoria Wynagrodzenia
*/
	insert into bik_attr_def ( name,attr_category_id,is_deleted,is_built_in,type_attr)
	values('Ilo�� miesi�cy do ko�ca roku',23,0,0,'shortText');
	
	insert into bik_attr_def ( name,attr_category_id,is_deleted,is_built_in,type_attr)
	values('Planowane miesi�czne wykorzystanie',23,0,0,'calculation');


-- przy��czenie atrybutow do node_kind

	insert into bik_attribute( node_kind_id,attr_def_id,is_deleted)
	values  ((select id from bik_node_kind where code='dtk_Folderwynagrodzen'),
	(select id from bik_attr_def where name='Ilo�� miesi�cy do ko�ca roku'),0);
	
	insert into bik_attribute( node_kind_id,attr_def_id,is_deleted)
	values  ((select id from bik_node_kind where code='dtk_Folderwynagrodzen'),
	(select id from bik_attr_def where name='Planowane miesi�czne wykorzystanie'),0);

	update bik_node_kind
	set attr_calculating_types ='[Bud�et] numeric(15,2) ,[Ilo�� miesi�cy do ko�ca roku] int' 
	where code='dtk_Folderwynagrodzen'
	
	/*Folder wynagrodzen */
	update bik_node_kind
	set attr_calculating_expressions =
	'select node_id, case when rozd < 0 then ''<html><span style="background-color:#ff0000">'' + cast(rozd as varchar(100)) + ''</span></html>'' else cast(rozd as varchar(100)) end as [Kwota do rozdysponowania]
	, [Bud�et] - rozd as [Wykorzystanie bud�etu (z�)]
	, case when wb >= 80 and wb <= 100 then ''<html><span style="background-color:#ffff40">'' + cast(wb as varchar(100)) + ''</span></html>'' when wb > 100 then ''<html><span style="background-color:#ff0000">'' + cast(wb as varchar(100)) + ''</span></html>'' else cast(wb as varchar(100)) end as [Wykorzystanie bud�etu [%]]]
	, cast( case when planwykon=0 then 0 else rozd/planwykon  end as numeric(15,2))  as [Planowane miesi�czne wykorzystanie]
	from
	  (select (select [Bud�et] - sum(case when isnumeric(value) = 1 then cast(replace(value,'','', ''.'') as numeric(15,2)) else 0 end)         from bik_attribute_linked av inner join bik_node bn on av.node_id = bn.id and bn.is_deleted = 0        where av.attribute_id = 1066 and bn.parent_node_id = x.node_id       ) as rozd,         
		  case when [Bud�et] = 0 then 0 else cast(coalesce((select sum(case when isnumeric(value) = 1 then cast(replace(value,'','', ''.'') as numeric(15,2)) else 0 end) from bik_attribute_linked av inner join bik_node bn on av.node_id = bn.id and bn.is_deleted = 0       where av.attribute_id = 1066 and bn.parent_node_id = x.node_id          ), 0) * 100 / [Bud�et] as numeric(15,2)) end    as wb, [Bud�et]
		  ,case when  isnumeric([Ilo�� miesi�cy do ko�ca roku]) = 1 then 
				cast(replace([Ilo�� miesi�cy do ko�ca roku],'','', ''.'') as numeric(15,2))
			else 
				0
		   end as planwykon
		  , node_id
	  from ${src_vals}$ as x
	) x
	'
	where code = 'dtk_Folderwynagrodzen'
	
-----------------------------------------------------
-- bez uwzgl�dniania is_deleted dla obiektow zaleznych	
	update  bik_node_kind set attr_dependent_nodes_sql='select distinct n.parent_node_id from ${node_ids}$ as src inner join bik_node n on n.id = src.node_id'
	where code='dtk_TaxonomyEntity' or code='dtk_MileStone' or code='dtk_TaxonomyEntityLeaf' or code='dtk_Zapotrzebowanie'
	
	update  bik_node_kind set attr_dependent_nodes_sql='select n.id from ${node_ids}$ as src inner join bik_joined_objs jo on jo.src_node_id = src.node_id inner join bik_node n on n.id = jo.dst_node_id and n.node_kind_id in (114, 207)   union all   select distinct n.parent_node_id from ${node_ids}$ as src inner join bik_node n on n.id = src.node_id'
	where code='dtk_Listaplac' 
	
	