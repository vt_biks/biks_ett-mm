--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[fn_escape_and_quote_val]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [fn_escape_and_quote_val]
GO

create function fn_escape_and_quote_val(@val sql_variant) returns varchar(max) as
begin

  if @val is null return 'null'

  declare @base_type sysname = cast(SQL_VARIANT_PROPERTY(@val, 'BaseType') as sysname)
  
  /* 
    obs�ugiwane: bigint, char, date, datetime, datetime2, decimal, float, int, nchar, numeric, nvarchar,
      real, smalldatetime, smallint, time, tinyint, uniqueidentifier, varchar  
       
    nieobs�ugiwane: binary, datetimeoffset, money, smallmoney, varbinary
  */
  
  -- typy liczbowe - bez ciapk�w (unquoted)
  if @base_type in ('bigint', 'int', 'smallint', 'tinyint', 'decimal', 'float', 'numeric', 'real') 
    return cast(@val as varchar(max))

  -- typy w ciapkach - data, czas i uniqueidentifier
  if @base_type in ('date', 'datetime', 'datetime2', 'smalldatetime', 'time', 
    'uniqueidentifier')
    return '''' + convert(varchar(max), @val, 120) + ''''

  -- typy w ciapkach i z podwojonym ciapkiem - stringowe
  if @base_type in ('char', 'nchar', 'nvarchar', 'varchar')
    return '''' + replace(cast(@val as varchar(max)), '''', '''''') + ''''

  return '<unsupported base_type: ' + @base_type + '>'
end
go

--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------


if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_script_data_as_inserts]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_script_data_as_inserts]
go

create procedure [dbo].[sp_script_data_as_inserts](@source nvarchar(max), @opt_source_table_name sysname = null, 
  @output_table_name_prefix sysname = null, @output_row_num int = 0, @row_num_inc int = 0, @diag_level int = 0) as
begin
  set nocount on;

  if @output_table_name_prefix is null set @output_table_name_prefix = 'tmp_output_';

  if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..##script_data_as_inserts_output_tab'))
    exec('drop table ##script_data_as_inserts_output_tab');

  create table ##script_data_as_inserts_output_tab (id int not null identity primary key, row varchar(max) not null, row_num int not null default 0);

  insert into ##script_data_as_inserts_output_tab (row) values ('/* starting for @source = ' + @source + ' */');

  declare @output_table_name sysname;

  if rtrim(substring(ltrim(@source), 1, 7)) <> 'select' begin
    if @opt_source_table_name is null
      set @opt_source_table_name = @source; -- + '_' + replace(newid(), '-', '_');
    set @source = 'select * from ' + @source;
  end else begin
    if @opt_source_table_name is null
      set @opt_source_table_name = 'tbl_' + replace(newid(), '-', '_');    
  end;

  set @output_table_name_prefix = coalesce(@output_table_name_prefix, '');

  set @output_table_name = @output_table_name_prefix + substring(@opt_source_table_name, 1, 128 - len(@output_table_name_prefix));    

  if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..##tbl_4_script_data_as_inserts'))
    exec('drop table ##tbl_4_script_data_as_inserts');
    
  declare @sql_txt nvarchar(max) = 'select * into ##tbl_4_script_data_as_inserts from (' + @source + ') x where 1 = 0';
  exec(@sql_txt);
  
  declare @row_txt varchar(max) = null;
  declare @insert_into_const_part varchar(max) = null;
  declare @insert_into_var_part varchar(max) = null;
  
  select
    @row_txt = case when @row_txt is null then '' else @row_txt + ', ' end + column_name + ' ' + column_type,
    @insert_into_const_part = case when @insert_into_const_part is null then '' else @insert_into_const_part + ', ' end + column_name,
    @insert_into_var_part = case when @insert_into_var_part is null then '' else @insert_into_var_part + ' + '', '' + ' end + 
      'dbo.fn_escape_and_quote_' + case when column_type like 'varchar%' or column_type like 'nvarchar%' then 'str' else 'val' end + '(' + column_name + ')'
  from
	  (
	  select 
	    table_name, column_name, 
	    data_type + case data_type
		    when 'sql_variant' then ''
		    when 'text' then ''
		    when 'ntext' then ''
		    when 'xml' then ''
		    when 'decimal' then '(' + cast(numeric_precision as varchar) + ', ' + cast(numeric_scale as varchar) + ')'
		    when 'numeric' then '(' + cast(numeric_precision as varchar) + ', ' + cast(numeric_scale as varchar) + ')'
		    else coalesce('('+case when character_maximum_length = -1 then 'max' else cast(character_maximum_length as varchar) end +')','')
      end as column_type, 
      ordinal_position
    from tempdb.information_schema.columns isc inner join tempdb.sys.objects so on isc.TABLE_NAME = so.name
    where so.object_id = --object_id('tempdb..##aqq')   --
      object_id('tempdb..##tbl_4_script_data_as_inserts')
    ) x
  order by table_name, ordinal_position;
  
  insert into ##script_data_as_inserts_output_tab (row) values ('if object_id(''' + @output_table_name + ''') is not null drop table ' + @output_table_name + ';');

  insert into ##script_data_as_inserts_output_tab (row) values ('create table ' + @output_table_name + ' (' + @row_txt + ');');

  set @insert_into_const_part = 'insert into ' + @output_table_name + ' (' + @insert_into_const_part + ') values';  
  
  if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..##script_data_as_inserts_output_tab2'))
    exec('drop table ##script_data_as_inserts_output_tab2');

  create table ##script_data_as_inserts_output_tab2 (row_num int not null identity primary key, row varchar(max) not null);
  
  set @sql_txt = 'insert into ##script_data_as_inserts_output_tab2 (row) select ''('' + ' + @insert_into_var_part + ' + '')'' from (' + @source + ') x';
  --print @sql_txt;
  exec(@sql_txt);

  declare @max_row_num0 int = (select max(row_num) from ##script_data_as_inserts_output_tab2);

  if @max_row_num0 is not null
  insert into ##script_data_as_inserts_output_tab (row) values (@insert_into_const_part);

  declare @batch_size int = 500;

  insert into ##script_data_as_inserts_output_tab (row, row_num)
  select
    row + case when row_num = @max_row_num0 or row_num % @batch_size = 0 then ';' else ',' end, --row_num, 
    row_num + cast(((row_num - 1) / @batch_size) * 2 as int) as row_num
  from ##script_data_as_inserts_output_tab2
  union all
  select 
  bs.txt, rn.row_num + bs.id
  from
  (
  select *, (batch_no + 1) * @batch_size + batch_no * 2 + 1 as row_num, row_number() over (order by batch_no desc) as rev_row_num 
  from
  (select distinct cast(row_num / @batch_size as int) as batch_no from ##script_data_as_inserts_output_tab2) bn
  ) rn cross join (values (0, 'GO'), (1, @insert_into_const_part)) as bs(id, txt)
  where rev_row_num > 1 or bs.id = 0
  order by row_num
  ;

/*
  set @sql_txt = 'insert into ##script_data_as_inserts_output_tab (row) select ''' + @insert_into_const_part + ' ('' + ' + @insert_into_var_part + ' + '');'' from (' + @source + ') x';
  --print @sql_txt;
  exec(@sql_txt);
*/

  declare @max_row_num int = coalesce((select max(row_num) from ##script_data_as_inserts_output_tab), 0);
  
  insert into ##script_data_as_inserts_output_tab (row, row_num) values ('
  ', @max_row_num + 1);
  
  if @output_row_num = 0  
    select row as [---- row] from ##script_data_as_inserts_output_tab order by row_num, id;
  else
    select row as [---- row], @row_num_inc + row_number() over(order by row_num, id) as row_num
    from ##script_data_as_inserts_output_tab order by row_num, id;
    
/*
  set @sql_txt = 'select row as [---- row]' + 
    case when @output_row_num = 0 then '' else ', ' + cast(@row_num_inc as varchar(20)) + ' + row_number() over(order by row_num, id) as row_num' end + 
    ' from ##script_data_as_inserts_output_tab order by row_num, id';
  print @sql_txt;
  exec(@sql_txt);
*/
end;
go


--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------


if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_script_multi_data_as_inserts_clear]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_script_multi_data_as_inserts_clear]
go

create procedure [dbo].[sp_script_multi_data_as_inserts_clear] as
begin
  set nocount on;

  if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..##script_multi_data_as_inserts_output_tab'))
    exec('drop table ##script_multi_data_as_inserts_output_tab');  
end;
go


--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_script_multi_data_as_inserts_add]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_script_multi_data_as_inserts_add]
go

create procedure [dbo].[sp_script_multi_data_as_inserts_add](@source nvarchar(max), @opt_source_table_name sysname = null, 
  @output_table_name_prefix sysname = null) as
begin
  set nocount on;
  
  if not exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..##script_multi_data_as_inserts_output_tab'))
    exec('create table ##script_multi_data_as_inserts_output_tab (row_num int not null primary key, row varchar(max))');  

  declare @max_row_num int = coalesce((select max(row_num) from ##script_multi_data_as_inserts_output_tab), 0);

  insert into ##script_multi_data_as_inserts_output_tab (row, row_num)
  exec sp_script_data_as_inserts @source, @opt_source_table_name, @output_table_name_prefix, 1, @max_row_num;
end;
go



--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------


/*

-- Wszystkie tabelki do jednego pliku - odpala� razem od pierwszej do ostatniej linii

set nocount on;
exec [sp_script_multi_data_as_inserts_clear];
exec [sp_script_multi_data_as_inserts_add] 'bik_tree';
exec [sp_script_multi_data_as_inserts_add] 'bik_help';
exec [sp_script_multi_data_as_inserts_add] 'select * from bik_node where tree_id = -111117', 'bik_node';
select row [---- row] from ##script_multi_data_as_inserts_output_tab order by row_num;

*/


/*

-- Ka�da tabelka do osobnego pliku - odpala� oddzielnie ka�de wywo�anie sp_script_data_as_inserts

set nocount on;



exec sp_script_data_as_inserts 'bik_tree'


exec sp_script_data_as_inserts 'select * from bik_node where tree_id = 17', 'bik_node'


exec sp_script_data_as_inserts 'bik_help'


*/
