/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import junit.framework.TestCase;
import simplelib.IInitByJson;
import simplelib.JsonReader;
import simplelib.MapHelper;
import simplelib.SimpleDateUtils;

/**
 *
 * @author wezyr
 */
public class JsonConverterExTest extends TestCase {

    public JsonConverterExTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public static class C implements IInitByJson, Comparable {

        public int i;
        public String s;
        public boolean b;
        public Date d;
        public Boolean b2;
        public Integer i2;
        public Set<C> subItems;
        public LinkedList<C> subItems2;
        public LinkedList<String> strItems;
        public Set<Integer> intItems;

        public void initByJsonMap(MapHelper<String> mh) {
            i = mh.getInt("i");
            s = mh.getString("s");
            b = mh.getBool("b");
            d = mh.getDate("d");
            b2 = mh.getBoolean("b2");
            i2 = mh.getInteger("i2");
            Collection<Map<String, Object>> _subItems_as_map = (Collection<Map<String, Object>>) mh.getCollection("subItems");
            if (_subItems_as_map != null) {
                subItems = new LinkedHashSet();
                for (Map<String, Object> item : _subItems_as_map) {
                    C v = new C();
                    v.initByJsonMap(new MapHelper<String>(item));
                    subItems.add(v);
                }
            }
            Collection<Map<String, Object>> _subItems2_as_map = (Collection<Map<String, Object>>) mh.getCollection("subItems2");
            if (_subItems2_as_map != null) {
                subItems2 = new LinkedList();
                for (Map<String, Object> item : _subItems2_as_map) {
                    C v = new C();
                    v.initByJsonMap(new MapHelper<String>(item));
                    subItems2.add(v);
                }
            }
            Collection<String> _strItems_as_map = (Collection<String>) mh.getCollection("strItems");
            if (_strItems_as_map != null) {
                strItems = new LinkedList();
                for (String item : _strItems_as_map) {
                    strItems.add(item);
                }
            }
            Collection<Integer> _intItems_as_map = (Collection<Integer>) mh.getCollection("intItems");
            if (_intItems_as_map != null) {
                intItems = new LinkedHashSet();
                for (Integer item : _intItems_as_map) {
                    intItems.add(item);
                }
            }
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 97 * hash + this.i;
            hash = 97 * hash + (this.s != null ? this.s.hashCode() : 0);
            hash = 97 * hash + (this.b ? 1 : 0);
            hash = 97 * hash + (this.d != null ? this.d.hashCode() : 0);
            hash = 97 * hash + (this.b2 != null ? this.b2.hashCode() : 0);
            hash = 97 * hash + (this.i2 != null ? this.i2.hashCode() : 0);
            hash = 97 * hash + (this.subItems != null ? this.subItems.hashCode() : 0);
            hash = 97 * hash + (this.subItems2 != null ? this.subItems2.hashCode() : 0);
            hash = 97 * hash + (this.strItems != null ? this.strItems.hashCode() : 0);
            hash = 97 * hash + (this.intItems != null ? this.intItems.hashCode() : 0);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final C other = (C) obj;
            if (this.i != other.i) {
                return false;
            }
            if ((this.s == null) ? (other.s != null) : !this.s.equals(other.s)) {
                return false;
            }
            if (this.b != other.b) {
                return false;
            }
            if (this.d != other.d && (this.d == null || !this.d.equals(other.d))) {
                return false;
            }
            if (this.b2 != other.b2 && (this.b2 == null || !this.b2.equals(other.b2))) {
                return false;
            }
            if (this.i2 != other.i2 && (this.i2 == null || !this.i2.equals(other.i2))) {
                return false;
            }
            if (this.subItems != other.subItems && (this.subItems == null || !this.subItems.equals(other.subItems))) {
                return false;
            }
            if (this.subItems2 != other.subItems2 && (this.subItems2 == null || !this.subItems2.equals(other.subItems2))) {
                return false;
            }
            if (this.strItems != other.strItems && (this.strItems == null || !this.strItems.equals(other.strItems))) {
                return false;
            }
            if (this.intItems != other.intItems && (this.intItems == null || !this.intItems.equals(other.intItems))) {
                return false;
            }
            return true;
        }

        public int compareTo(Object o) {
            return 0;
        }
    }

    public void testMakeInitByJsonMap1() {
        String result = JsonCodeGenerator.makeInitByJsonMap(C.class);
        System.out.println(result);
    }

    public void testMakeInitByJsonMapReverseWay() {
        C obj = new C();
        obj.b = false;
        obj.i2 = 10;
        obj.d = SimpleDateUtils.newDate(2008, 11, 30);
        obj.s = "ala ma kota";
        obj.subItems = new TreeSet<C>();
        obj.subItems.add(new C());
        obj.strItems = new LinkedList<String>();
        obj.strItems.add("x");
        obj.strItems.add("y");
        obj.intItems = new TreeSet<Integer>();
        obj.intItems.add(100);
        obj.intItems.add(200);
        obj.intItems.add(-666);
        String jsonString = JsonConverterEx.convertObject(obj);
        System.out.println("/*");
        System.out.println(jsonString);
        System.out.println("*/");
        C obj2 = new C();
        MapHelper<String> mh = JsonReader.readMapHelper(jsonString);
        obj2.initByJsonMap(mh);

        assertEquals(obj.d, obj2.d);
        assertEquals(obj.b, obj2.b);
        assertEquals(obj.s, obj2.s);
        assertEquals(obj.subItems, obj2.subItems);
        assertEquals(obj.subItems.size(), obj2.subItems.size());
        Iterator<C> iter = obj2.subItems.iterator();
        for (C item : obj.subItems) {
            assertEquals(item, iter.next());
        }
        assertTrue(obj.subItems.equals(obj2.subItems));
        assertEquals(obj.subItems2, obj2.subItems2);
        assertEquals(obj, obj2);
    }

    public static class ExampleForInnotion {

        public String x;
        public int ii;
        public Date dd;
        public Integer iII;
    }

    public void testExampleForInnotion() {
        ExampleForInnotion e4i = new ExampleForInnotion();
        e4i.dd = new Date();
        e4i.x = "ala ma kota";
        e4i.ii = 11;

        String json = JsonConverterEx.convertObject(e4i);
        System.out.println("json=" + json);
        Map<String, Object> m = JsonReader.readMap(json);
        ExampleForInnotion e4i2 = MuppetMaker.newMuppet(ExampleForInnotion.class, m);
        System.out.println("json2=" + JsonConverterEx.convertObject(e4i2));
    }
}
