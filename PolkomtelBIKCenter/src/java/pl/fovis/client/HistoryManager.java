/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.http.client.UrlBuilder;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import pl.fovis.client.bikpages.IBikPage;
import pl.fovis.client.menu.LoginLogoutHeader;
import pl.fovis.common.TreeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.BaseUtils;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public class HistoryManager {

    public static void setupChangeListenerAndFireCurrentStateToken(final IBikPage<Integer> defaultTabOnStart) {
        bikEntryPoint.registerHistoryHandler(History.addValueChangeHandler(new ValueChangeHandler<String>() {
            @Override
            public void onValueChange(ValueChangeEvent<String> event) {
                historyTokenChanged(defaultTabOnStart, event);
                BaseActionOrCancelDialog.hideAllDialogs();
                LoginLogoutHeader.refreshOtherLangAnchors();
            }
        }));
        History.fireCurrentHistoryState();
    }

    public static void historyTokenChanged(final IBikPage<Integer> defaultTabOnStart, ValueChangeEvent<String> event) {
        BIKClientSingletons.setBrowserTitle();
        String historyToken = event.getValue();

//        Window.alert("historyTokenChanged: historyToken=" + historyToken);
        //ww: nowa obsługiwana postać tokenów:
        // tabId/[nodeId]|encodedNameOfAnticipatedNode
        // zasługuje na oddzielne traktowanie - strzał do bazy itp.
        //
        // obsługiwana postać tokenów historii:
        // tabId[/nodeId[_anchor]]
        // czyli nodeId jest opcjonalny i oddzielony od id zakładki przez '/'
        // natomiast gdy jest nodeId to anchor jest opcjonalny i oddzielony
        // od nodeId przez '_'
        // samo tabId też jest właściwie opcjonalne
        //ww: uwaga: tabId może zawierać w sobie '_', więc trzeba we właściwej
        // kolejności wyciągać poszczególne elementy z tokena
        Pair<String, String> pTabIdAndRest = BaseUtils.splitString(historyToken, "/");
        final String tabId = BaseUtils.emptyStringOrWhiteSpaceToNull(pTabIdAndRest.v1);
        TreeBean treeForTabId = BIKClientSingletons.getTreeBeanByCode(tabId);
        final String restAfterTabId = pTabIdAndRest.v2;

        if (treeForTabId != null && !BaseUtils.isStrEmptyOrWhiteSpace(restAfterTabId)) {
            final Pair<String, String> nodeIdAndAticipadedName = BaseUtils.splitString(restAfterTabId, "|");
            String anticipatedName = BaseUtils.emptyStringOrWhiteSpaceToNull(nodeIdAndAticipadedName.v2);
            if (anticipatedName != null) {

                BIKClientSingletons.getService().findAnticipatedNodeOfTree(
                        treeForTabId.id,
                        BaseUtils.tryParseInteger(nodeIdAndAticipadedName.v1),
                        //                        URL.decodeQueryString(anticipatedName),
                        anticipatedName,
                        new StandardAsyncCallback<Integer>() {

                            @Override
                            public void onSuccess(Integer result) {
                                if (result == null) {
                                    BIKClientSingletons.showInfo(I18n.anticipatedObjectNotFound.get());
                                    historyTokenChangedPart2(tabId, nodeIdAndAticipadedName.v1, defaultTabOnStart);
                                } else {
                                    BIKClientSingletons.showInfo(I18n.foundAnticipatedObject.get());
                                    addHistoryToken(tabId, result.toString(), true);
                                }
                            }
                        });

                return;
            }
        }

        historyTokenChangedPart2(tabId, restAfterTabId, defaultTabOnStart);
    }

    protected static void historyTokenChangedPart2(String tabId, final String optNodeIdAndOptAnchor, IBikPage<Integer> defaultTabOnStart) {
        String nodeIdStr = null;
        String anchor = null;

        if (optNodeIdAndOptAnchor != null) {
            Pair<String, String> pNodeIdAndOptAnchor = BaseUtils.splitString(optNodeIdAndOptAnchor, "_");
            nodeIdStr = BaseUtils.emptyStringOrWhiteSpaceToNull(pNodeIdAndOptAnchor.v1);
            anchor = BaseUtils.emptyStringOrWhiteSpaceToNull(pNodeIdAndOptAnchor.v2);
        }
        String tabIdBeforeFix = tabId;
        String defTabId = defaultTabOnStart.getId();
        tabId = BaseUtils.nullToDef(tabId, defTabId);
        final boolean isTabIdProper = BIKClientSingletons.getTabSelector().hasTab(tabId);
        if (!isTabIdProper) {
            tabId = defTabId;
        }
        // kod zakładki naprawiony -> czyścimy inne pola
        if (!BaseUtils.safeEquals(tabId, tabIdBeforeFix)) {
            nodeIdStr = null;
            anchor = null;
        }

        Integer nodeId = null;
        if (!BaseUtils.isStrEmptyOrWhiteSpace(nodeIdStr)) {
            nodeId = BaseUtils.tryParseInteger(nodeIdStr);
        }
        Pair<String, Integer> currTabAndVal = BIKClientSingletons.getTabSelector().getCurrentTabIdAndSelectedVal();
        Pair<String, Integer> newTabAndVal = new Pair<String, Integer>(tabId, nodeId);
        if (anchor != null) {
            // mamy anchor, sprawdźmy czy jesteśmy aktualnie na tej zakładce
            // i tym samym obiekcie
            if (currTabAndVal.equals(newTabAndVal)) {
                // ignorujemy to - to jest przeskok na inny rozdział (np. w definicji biznesowej)
                return;
            }
            // to jest inna zakładka / inny węzeł - anchora zignorujemy
            // czyli tu nic nie robimy, kod idzie dalej
        }
        if (nodeId != null && !currTabAndVal.equals(newTabAndVal)
                && /*
                 * BIKClientSingletons.isUserLoggedIn() &&
                 */ BIKClientSingletons.isBikUserLoggedIn()) {
            // dodajemy nowy wpis do historii dla zalogowanego
            // tf: przenienione do metody addHistoryToken(String tabId, String id, boolean fireOnChange)
//            BIKClientSingletons.getService().addToHist(nodeId, BIKClientSingletons.<Void>getDoNothingCallback());
        }
        BIKClientSingletons.getTabSelector().selectValueOnTab(tabId, nodeId);
    }

    public static boolean addHistoryToken(IBikPage mc) {
        return addHistoryToken(mc, null);
    }

    public static boolean addHistoryToken(IBikPage mc, String id) {
        return addHistoryToken(mc.getId(), id, true);
    }

    public static String makeHistoryToken(String tabId, String id) {
        return tabId + (id != null ? "/" + id : "");
    }

    public static boolean addHistoryToken(String tabId, String id, boolean fireOnChange) {
        String tokenStr = makeHistoryToken(tabId, id);

        if (BaseUtils.safeEquals(tokenStr, History.getToken())) {
            return false;
        } else {
            History.newItem(tokenStr, fireOnChange);
            if (!fireOnChange) {
                LoginLogoutHeader.refreshOtherLangAnchors();
            }
            return true;
        }
    }

    public static void addToUserHistoryAndHistoryToken(String tabId, String id, boolean fireOnChange) {
        addHistoryToken(tabId, id, fireOnChange);
        // dodajemy nowy wpis do historii usera
        Integer nodeId = BaseUtils.tryParseInteger(id);
        if (nodeId != null) {
            BIKClientSingletons.getService().addToHist(nodeId, BIKClientSingletons.<Void>getDoNothingCallback());
        }
    }

    public static String getCurrentToken() {
        return History.getToken();
    }

    public static String getFullUrl(String tabId, String id) {
        UrlBuilder ub = Window.Location.createUrlBuilder();
        ub.setHash(makeHistoryToken(tabId, id));
        return ub.buildString();
    }

//    public static String getFullUrlOld(String tabId, String id) {
//        String queryString = Location.getQueryString();
//        String pathString = Location.getPath();
//        return Location.getProtocol() + "//" + Location.getHost()
//                + ((pathString == null || pathString.length() == 0) ? "/" : pathString)
//                + ((queryString == null || queryString.length() == 0) ? "" : (queryString + "/")) + "#"
//                + makeHistoryToken(tabId, id);
//    }
}
