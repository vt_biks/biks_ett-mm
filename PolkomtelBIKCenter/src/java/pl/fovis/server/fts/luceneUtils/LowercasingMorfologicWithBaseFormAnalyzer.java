/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts.luceneUtils;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.morfologik.MorfologikFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;

/**
 *
 * @author pmielanczuk
 */
public class LowercasingMorfologicWithBaseFormAnalyzer extends Analyzer {

    @Override
    protected TokenStreamComponents createComponents(String fieldName) {
        final Tokenizer src = new StandardTokenizer();
        final StandardFilterWithTerm fltr = new StandardFilterWithTerm(src);
        return new TokenStreamComponents(
                src,
                new LowercasingMorfologicWithBaseFormFilter(new MorfologikFilter(fltr), fltr));
    }
}
