/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.widgets.sbavc;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.dialogs.IAdvancedSearchDialog;
import pl.fovis.common.AttrSearchableBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.sbavc.ISearchByAttrValueCriteria;

/**
 *
 * @author tflorczak
 */
public class SearchByAttrValueCriteriaManager {

    // tf: ważna kolejność, dlatego LinkedHashMap!
    private final static Map<Class, String> availableCriteria = new LinkedHashMap<Class, String>();

    static {
        availableCriteria.put(SearchByAttrValueCriteriaNotEmptyWidget.class, I18n.noValueAttribute.get());
        availableCriteria.put(SearchByAttrValueCriteriaEqualsWidget.class, I18n.singleValueAttributeEquals.get());
        availableCriteria.put(SearchByAttrValueCriteriaContainsWidget.class, I18n.singleValueAttributeContains.get());
        availableCriteria.put(SearchByAttrValueCriteriaSqlLikeWidget.class, I18n.singleValueAttributeSqlLike.get());
        availableCriteria.put(SearchByAttrValueCriteriaStartsWithWidget.class, I18n.singleValueAttributeStartsWith.get());
        availableCriteria.put(SearchByAttrValueCriteriaEndsWithWidget.class, I18n.singleValueAttributeEndsWith.get());
        availableCriteria.put(SearchByAttrValueCriteriaInWidget.class, I18n.singleValueAttributeIn.get());
        availableCriteria.put(SearchByAttrValueCriteriaBetweenWidget.class, I18n.fromTo.get());
    }

    // lista zapamiętanych kryteriów
    protected List<ISearchByAttrValueCriteriaWidget> selectedWidget = new ArrayList<ISearchByAttrValueCriteriaWidget>();
    // lista tymczasowych kryteriów
    protected List<ISearchByAttrValueCriteriaWidget> tempSelectedCriteria = new ArrayList<ISearchByAttrValueCriteriaWidget>();
    protected VerticalPanel main = new VerticalPanel();
    protected ISBAVCWidgetCreator criteriaCreator = GWT.create(ISBAVCWidgetCreator.class);
    protected List<AttrSearchableBean> attrs;
    protected IAdvancedSearchDialog advancedSearchDialog;
    protected Map<Integer, AttrSearchableBean> attrsSearchableTypes = new HashMap<Integer, AttrSearchableBean>();

//    {
//        for (AttrSearchableBean bean : BIKClientSingletons.getAttrsSearchable()) {
//            attrsSearchableTypes.put(bean.id, bean);
//        }
//    }
    public SearchByAttrValueCriteriaManager(IAdvancedSearchDialog advancedSearchDialog) {
        this.advancedSearchDialog = advancedSearchDialog;
    }

    public void deleteRow(ISearchByAttrValueCriteriaWidget widget) {
        main.remove(widget.getWidget());
        tempSelectedCriteria.remove(widget);
        resetGroupingExprTbInDialogAndIdCriteriaLbl();
    }

    /**
     * Dodaje wiersz na końcu
     */
    public void addRow() {
        ISearchByAttrValueCriteriaWidget widget = criteriaCreator.create(getFirstClassName());
        widget.setup(this, attrs);
        main.add(widget.getWidget());
        tempSelectedCriteria.add(widget);
        resetGroupingExprTbInDialogAndIdCriteriaLbl();
    }

    public void changeCriterium(ISearchByAttrValueCriteriaWidget widgetToReplace, int newCriteriumIndex) {
        int widgetIndex = main.getWidgetIndex(widgetToReplace.getWidget());
//        tempSelectedCriteria.remove(widgetToReplace);
        main.remove(widgetIndex);
        int i = 0;
        for (Class class1 : availableCriteria.keySet()) {
            if (i == newCriteriumIndex) {
                ISearchByAttrValueCriteriaWidget widget = criteriaCreator.create(class1.getSimpleName());
                widget.setup(this, attrs);
                widget.setCriteriaId(widgetIndex + 1);
                main.insert(widget.getWidget(), widgetIndex);
                widget.populateWidgets(widgetToReplace.getSelectedAttrId(), widgetToReplace.isNegation(), widgetToReplace.getCriteriumIndex());
                tempSelectedCriteria.set(widgetIndex, widget);
                break;
            }
            i++;
        }
    }

    public Widget getWidget() {
        //
        return main;
    }

    public void populateWidgets(List<AttrSearchableBean> attrs) {
        this.attrs = attrs;
        List<ISearchByAttrValueCriteriaWidget> widgetsToDelete = new ArrayList<ISearchByAttrValueCriteriaWidget>();
        for (ISearchByAttrValueCriteriaWidget oneWidget : selectedWidget) {
            boolean shouldDeleteWidget = oneWidget.populateAttributeListBox(attrs);
            if (shouldDeleteWidget) {
                widgetsToDelete.add(oneWidget);
            }
        }
        for (ISearchByAttrValueCriteriaWidget widToDel : widgetsToDelete) {
            selectedWidget.remove(widToDel);
            main.remove(widToDel.getWidget());
        }
        tempSelectedCriteria.clear();
        tempSelectedCriteria.addAll(selectedWidget);

        if (!widgetsToDelete.isEmpty()) {
            resetGroupingExprTbInDialogAndIdCriteriaLbl();
        }
    }

    protected String getFirstClassName() {
        Set<Class> keySet = availableCriteria.keySet();
        for (Class class1 : keySet) {
            return class1.getSimpleName();
        }
        return SearchByAttrValueCriteriaNotEmptyWidget.class.getSimpleName();
    }

    public List<ISearchByAttrValueCriteria> getCriteriaToSearch() {
        List<ISearchByAttrValueCriteria> getCriteriaToSearch = new ArrayList<ISearchByAttrValueCriteria>();
        for (int i = 0; i < selectedWidget.size(); i++) {
            ISearchByAttrValueCriteriaWidget oneWidget = selectedWidget.get(i);
            ISearchByAttrValueCriteria bean = oneWidget.provideBean();
            if (bean != null) {
                bean.setIdInSelectedCriteriaList(i + 1);
            }
            getCriteriaToSearch.add(bean);
        }
        return getCriteriaToSearch;
    }

    public static Collection<String> getAvailableCriteria() {
        return availableCriteria.values();
    }

    // po otworzeniu dialoga, odtwarzamy kryteria
    public void prepareCriteria() {
        tempSelectedCriteria.clear();
        main.clear();
        for (ISearchByAttrValueCriteriaWidget widget : selectedWidget) {
            tempSelectedCriteria.add(widget);
            main.add(widget.getWidget());
        }
//        resetGroupingExprTbInDialogAndIdCriteriaLbl();
    }

    // po nacisnieciu guzika szukaj, tymczasowe kryteria sa przerzucane do wybranych
    public void storeCriteria() {
        selectedWidget.clear();
        for (ISearchByAttrValueCriteriaWidget widget : tempSelectedCriteria) {
            selectedWidget.add(widget);
        }
        tempSelectedCriteria.clear();
    }

    // po anulowaniu tymczasowe kryteria są czyszczone
    public void clearCriteria() {
        tempSelectedCriteria.clear();
        resetGroupingExprTbInDialogAndIdCriteriaLbl();
    }

    private void resetGroupingExprTbInDialogAndIdCriteriaLbl() {
        for (int i = 0; i < tempSelectedCriteria.size(); i++) {
            ISearchByAttrValueCriteriaWidget widget = tempSelectedCriteria.get(i);
            widget.setCriteriaId(i + 1); //bo ludzie zaczyna od 1 :D
        }
        advancedSearchDialog.resetGroupingExprTb();
    }

    public int getCurretnSelectedCriteria() {
        return tempSelectedCriteria.size();
    }

    public String getAttrSearchableType(int selectedAttrId) {
        return (attrsSearchableTypes.containsKey(selectedAttrId) ? attrsSearchableTypes.get(selectedAttrId).typeAttr : null);
    }

    public boolean isDisplayedAsNumber(int selectedAttrId) {
        return (attrsSearchableTypes.containsKey(selectedAttrId) ? attrsSearchableTypes.get(selectedAttrId).displayAsNumber > 0 : false);
    }

    public String getAttrValueOpt(int selectedAttrId) {
        return (attrsSearchableTypes.containsKey(selectedAttrId) ? attrsSearchableTypes.get(selectedAttrId).valueOpt : "");
    }
}
