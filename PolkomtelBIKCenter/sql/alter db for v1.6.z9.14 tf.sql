﻿exec sp_check_version '1.6.z9.14';
go

if not exists(select * from bik_app_prop where name = 'disableSSOForLogin')
begin
	insert into bik_app_prop (name, val, is_editable)
	values ('disableSSOForLogin', 'false', 1)
end;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('[bik_dqm_test_multi]') and name = 'opt_Test_Grants_Per_Objects')
begin
    alter table bik_dqm_test_multi add opt_Test_Grants_Per_Objects varchar(max) null;
end;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('[bik_dqm_test]') and name = 'opt_Test_Grants_Per_Objects')
begin
    alter table bik_dqm_test add opt_Test_Grants_Per_Objects varchar(max) null;
end;
go

exec sp_update_version '1.6.z9.14', '1.6.z9.15';
go
