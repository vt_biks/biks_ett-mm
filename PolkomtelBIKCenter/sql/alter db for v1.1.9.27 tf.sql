﻿exec sp_check_version '1.1.9.27';
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

-- poprawka do [TSB-FIX:21]
if exists(select * from bik_attr_def def
			inner join bik_attribute atr on atr.attr_def_id=def.id
			where def.is_deleted = 1 and atr.is_deleted = 0)
begin

update bik_attribute
set is_deleted = 1
from bik_attribute
inner join bik_attr_def on bik_attr_def.id = bik_attribute.attr_def_id
where bik_attr_def.is_deleted = 1
and bik_attribute.is_deleted = 0

end


-- poprawka do [TSB-FIX:22]
if exists(select * from bik_attribute atr
			inner join bik_attribute_linked lin on lin.attribute_id=atr.id
			where atr.is_deleted = 1 and lin.is_deleted = 0)
begin

update bik_attribute_linked
set is_deleted = 1
from bik_attribute_linked
inner join bik_attribute on bik_attribute.id = bik_attribute_linked.attribute_id
where bik_attribute.is_deleted = 1
and bik_attribute_linked.is_deleted = 0

end


------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

exec sp_update_version '1.1.9.27', '1.1.9.28';
go