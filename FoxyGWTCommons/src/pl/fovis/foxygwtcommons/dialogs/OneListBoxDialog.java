/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.dialogs;

import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.ListBox;
import java.util.List;
import pl.fovis.foxygwtcommons.i18n.I18n;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public class OneListBoxDialog extends BaseActionOrCancelDialog {

    private IParametrizedContinuation<String> saveCont;
    protected ListBox tbValue;
    protected List<String> possibleValues;
    protected String name;

    @Override
    protected String getDialogCaption() {
        return name;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.wybierz.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {
        tbValue = new ListBox();
//        tbValue.addStyleName("sendUrlInput");
        tbValue.setTitle(name);
        if (possibleValues == null || possibleValues.isEmpty()) {
            tbValue.addItem(I18n.bRAKWARTOSCI.get() /* I18N:  */ + "!");
            tbValue.setEnabled(false);
        } else {
            for (String value : possibleValues) {
                tbValue.addItem(value);
            }
        }
        main.add(tbValue);
        main.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
    }

    @Override
    protected void doAction() {
        if (tbValue.isEnabled()) {
            saveCont.doIt(tbValue.getItemText(tbValue.getSelectedIndex()));
        }
    }

    public void buildAndShowDialog(String name, List<String> initValue, IParametrizedContinuation<String> saveCont) {
        this.name = name;
        this.possibleValues = initValue;
        this.saveCont = saveCont;
        super.buildAndShowDialog();
    }
}
