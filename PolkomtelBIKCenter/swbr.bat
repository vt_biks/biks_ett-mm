@echo off
call prob_cfg.bat
SETLOCAL EnableDelayedExpansion
for /F "tokens=1,2 delims=#" %%a in ('"prompt #$H#$E# & echo on & for %%b in (1) do rem"') do (
  set "DEL=%%a"
)
cd ..
@rem %1 - switching direction (T (from Trunk)or B (from Branch)), %2 - branch version (needed if first parameter is B)
@rem example: swbr.bat B 1.2.1
echo Welcome in BIKS Switcher 
echo .
call :ColorText 0C "You have to commit all changes in your current projects"  
echo .
call :ColorText 0C "Do You want continue to with BIKS switching now (please type Y or N)" 
echo . 
set /p choice="Enter your choice: "

if "%choice%"=="Y" set cancontinue=1
if "%choice%"=="y" set cancontinue=1
if not "%cancontinue%"=="1" (
	echo You don't want to continue now, bye
	goto exit
)

set svn_branch_ver=%2
set svn_switching_string=

if "%1"=="T" (
 call :ColorText 0A "You chosen switch to Trunk"
 echo.
) else if "%1"=="B" (
 call :ColorText 0A "You chosen switch to Branch"
 echo.
) else (
 echo 
 call :ColorText 0C "You have to type correct switching direction"
 goto exiterror
)

if "%1"=="B" (
 if "%2"=="" (
  call :ColorText 0C "You have to type branch version"
  echo.
  goto exiterror
 ) else (
  set svn_switching_string=branches/%MAIN_PROJECT_NAME%/v%svn_branch_ver%/
 
 )
)

for %%p in (%PROJECTS%) do ( 
	cd %%p
	svn sw  %SVN_LOCALIZATION%%svn_switching_string%%%p
	cd ..
)

svn info %SVN_LOCALIZATION%%svn_switching_string%PolkomtelBIKCenter>current_branch.txt

goto exitsuc

:exiterror
 call :ColorText 0C "Switching failed"
 echo.
 goto exit

:exitsuc
 call :ColorText 0A "Switching successful"
 goto exit

:exit
 call :ColorText 0A "To see You soon"
 goto :eof
 
:ColorText
echo off
<nul set /p ".=%DEL%" > "%~2"
findstr /v /a:%1 /R "^$" "%~2" nul
del "%~2" > nul 2>&1
goto :eof
