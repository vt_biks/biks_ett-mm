/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import pl.fovis.foxygwtcommons.BasePickerDialog;
import java.util.Map;
import pl.fovis.common.BlogBean;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author młydkowski
 * bf:-->do usuniecia
 */
public class PickBlogEntryDialog extends BasePickerDialog<BlogBean> {

    @Override
    protected String getDialogCaption() {
//        return "Wybierz artykuł do podłączenia";
        return I18n.wybierzWpisBlogowy.get() /* I18N:  */;
    }

    @Override
    protected void setupListGridRecord(BlogBean item, Map<String, Object> record) {
        record.put("title" /* I18N: no */, item.subject);
        record.put("id" /* I18N: no */, item.id);
    }

    @Override
    protected String[] createFields() {
        return createFields("title=" /* i18n: no */ + I18n.tytul.get() /* I18N:  */, "id=id" /* I18N: no */);
    }

    @Override
    protected String getIdFieldName() {
        return "id" /* I18N: no */;
    }

    @Override
    protected String getListBoxLabelCaption() {
        return I18n.blogi.get() /* I18N:  */+ ":";
    }
}
