/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.plainfile;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ctran
 */
public class TreeExportMetadata implements Serializable {

    public String treeCode;
    public String baseFileName;
    public String name;
    public String nodeKindCode;
    public String desc;
    public String visualOrder;
    public List<String> parentObjIds;
    public Map<String, String> attributes;
    public List<TreeExportRelatedFileMetaData> relatedFiles;
}
