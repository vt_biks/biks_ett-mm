/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.db;

import static commonlib.JdbcUtil.generateTargetColNames;
import static commonlib.JdbcUtil.readOneRowFast;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import simplelib.FieldNameConversion;
import simplelib.LameRuntimeException;

/**
 *
 * @author pmielanczuk
 */
public class IteratorForResultSet implements Iterator<Map<String, Object>> {

    protected ResultSet rs;
    protected Boolean hasNext;
    protected String[] targetColNames;
    public int rowCnt;

    public IteratorForResultSet(ResultSet rs, FieldNameConversion optConversion) {
        this.rs = rs;
        targetColNames = generateTargetColNames(rs, optConversion);
    }

    @Override
    public boolean hasNext() {
        if (hasNext == null) {
            try {
                hasNext = rs.next();
            } catch (SQLException ex) {
                throw new LameRuntimeException("error in hasNext", ex);
            }
        }
        return hasNext;
    }

    @Override
    public Map<String, Object> next() {
        boolean hn = hasNext();

        if (!hn) {
            throw new NoSuchElementException();
        }

        rowCnt++;
        hasNext = null;

        return readOneRowFast(rs, targetColNames);
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported");
    }
}
