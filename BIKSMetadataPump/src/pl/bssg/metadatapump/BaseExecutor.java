/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump;

import commonlib.LameUtils;
import static pl.bssg.metadatapump.BaseExecutor.createAdhocDao;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.commons.IBeanConnectionThreadHandle;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import pl.trzy0.foxy.serverlogic.FoxyAppUtils;
import pl.trzy0.foxy.serverlogic.db.MssqlConnection;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;

/**
 *
 * @author tflorczak
 */
public class BaseExecutor {

    protected static String ADHOC_SQLS_PATH = LameUtils.getPackagePathOfClass(BaseExecutor.class) + "metadataAdhocSqls.html";
    protected IBeanConnectionThreadHandle threadHandle;
    protected MssqlConnectionConfig connConfg;
    protected IBeanConnectionEx<Object> mc;
    protected INamedSqlsDAO<Object> adhocDao;

    protected void initExecutor() {
        this.mc = new MssqlConnection(connConfg);
        this.mc.setDbStatementExecutionWarnThresholdMillis(-1);
        this.adhocDao = createAdhocDao(mc);
        this.threadHandle = mc.getCurrentThreadHandle();
    }

    protected void execNamedCommand(String name, Object... args) {
        adhocDao.execNamedCommand(name, args);
    }

    protected <V> V execNamedQuerySingleVal(String qryName, boolean allowNoResult,
            String optColumnName, Object... args) {
        return BeanConnectionUtils.<V>execNamedQuerySingleVal(adhocDao, qryName, allowNoResult,
                optColumnName, args);
    }

    protected static INamedSqlsDAO<Object> createAdhocDao(IBeanConnectionEx<Object> mc) {
        return FoxyAppUtils.createNamedSqlsDAOFromJar(mc, ADHOC_SQLS_PATH);
    }

    protected MssqlConnectionConfig getConnectionConfig() {
        return connConfg;
    }
}
