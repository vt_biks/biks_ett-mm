/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.RequiresResize;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author BSSG-Stacjonarny
 */
public class ResizableFlowPanel extends FlowPanel implements RequiresResize {

    public void onResize() {
        for (Widget w : getChildren()) {
            if (w instanceof RequiresResize) {
                RequiresResize rqrw = (RequiresResize) w;
//                Window.alert("w=" + w);
                rqrw.onResize();
            }
        }
    }
}
