/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.bssg.metadatapump.common.SapBo4ExPropConfigBean;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.AdminConfigBaseWidget;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;

/**
 *
 * @author ctran
 */
public class AdminConfigSapBo4ExtPropWidget extends AdminConfigBaseWidget<SapBo4ExPropConfigBean> {

    protected TextBox reportSDKPath;
    protected TextBox tbxPath;
    protected TextBox idtPath;
    protected TextBox clientToolsPath;
//    protected TextBox lcmCliPath;
    protected TextBox jREx86Path;

    protected FlexTable table;

    @Override
    protected void buildInnerWidget() {
        buildTable();
        main.add(table);
    }

    @Override
    public void populateWidgetWithData(SapBo4ExPropConfigBean data) {
        tbxPath.setText(data.path);
        reportSDKPath.setText(data.reportPath);
        idtPath.setText(data.idtPath);
        clientToolsPath.setText(data.clientToolsPath);
        jREx86Path.setText(data.jREx86Path);
    }

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_SAPBO4;
    }

    protected TextBox addRow(String labelName, String pathName, String info) {
        TextBox txtBox = new TextBox();
        txtBox.setWidth("468px");
        int row = table.getRowCount();
        table.setWidget(row, 0, new Label(labelName));
        table.setWidget(row, 1, txtBox);
        table.setWidget(row, 2, createButton(I18n.zapisz.get(), new ButtonSaveClickHandler(txtBox, info, pathName), null));

        return txtBox;
    }

    protected void buildTable() {
        table = new FlexTable();
        table.setCellSpacing(10);
        tbxPath = addRow(I18n.sciezkaDoKonektoraDesigner.get(), "sapbo4.path", I18n.zapisanoZmiany.get());
        reportSDKPath = addRow(I18n.sciezkaDoKonektoraReportSDK41.get(), "sapbo4.reportPath", I18n.zapisanoZmiany.get());
        idtPath = addRow(I18n.sciezkaDoKonektoraIDTSDK41.get(), "sapbo4.idtPath", I18n.zapisanoZmiany.get());
        clientToolsPath = addRow(I18n.sciezkaDoNarzedziKlienckich41.get(), "sapbo4.clientToolsPath", I18n.zapisanoZmiany.get());
//        lcmCliPath = addRow(I18n.sciezkaDoLcmCli.get(), "sapbo4.lcmCliPath", I18n.zapisanoZmiany.get());
        jREx86Path = addRow(I18n.sciezkaDoJREx86.get(), "sapbo4.jREx86Path", I18n.zapisanoZmiany.get());
    }

    protected void callServiceToSaveReportSDKPath(String path, AsyncCallback<Void> asyncCallback) {
        BIKClientSingletons.getService().setReportSDKPath(path, asyncCallback);
    }

    protected void callServiceToSaveBODesignerPath(String path, AsyncCallback<Void> asyncCallback) {
        BIKClientSingletons.getService().setBODesignerPath(path, getSourceName(), asyncCallback);
    }

    public class ButtonSaveClickHandler implements ClickHandler {

        TextBox txtBox;
        String info;
        String pathName;

        public ButtonSaveClickHandler(TextBox txtBox, String info, String pathName) {
            this.txtBox = txtBox;
            this.info = info;
            this.pathName = pathName;
        }

        @Override
        public void onClick(ClickEvent event) {
            callServiceToSavePath(txtBox.getText(), new StandardAsyncCallback<Void>("error in" /* I18N: no:err-fail-in */ + " setPath") {
                        @Override
                        public void onSuccess(Void result) {
                            BIKClientSingletons.showInfo(info /* I18N: no */);
                        }
                    });
        }

        protected void callServiceToSavePath(String path, AsyncCallback<Void> asyncCallback) {
            BIKClientSingletons.getService().setPath(path, pathName, asyncCallback);
        }
    }
}
