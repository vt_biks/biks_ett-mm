/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author mmastalerz
 */
public class HelpBean implements Serializable {

    public String helpKey;
    public String caption;
    public String textHelp;
    public String lang;

    public void langToLowerCase() {
        if(lang != null){
            lang = lang.toLowerCase();
        }
    }
}
