/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.sapbo.javasdk;

import com.crystaldecisions.sdk.framework.CrystalEnterprise;
import com.crystaldecisions.sdk.framework.IEnterpriseSession;
import com.crystaldecisions.sdk.framework.ISessionMgr;
import com.crystaldecisions.sdk.occa.infostore.IInfoObject;
import com.crystaldecisions.sdk.occa.infostore.IInfoObjects;
import com.crystaldecisions.sdk.occa.infostore.IInfoStore;
import com.crystaldecisions.sdk.occa.pluginmgr.IPluginInfo;
import com.crystaldecisions.sdk.occa.pluginmgr.IPluginMgr;

/**
 *
 * @author tflorczak
 */
public class TestCreateDashboard {

    public static void main(String[] args) {
        IEnterpriseSession enterpriseSession = null;
        try {
            ISessionMgr sessionMgr = CrystalEnterprise.getSessionMgr();
            enterpriseSession = sessionMgr.logon("Administrator", "1qaz2wsx", "192.168.167.57", "secEnterprise");
            IInfoStore infoStore = (IInfoStore) enterpriseSession.getService("InfoStore");
            IInfoObjects query = infoStore.query("select * from ci_infoobjects where si_id = 106387");
            IInfoObject get = (IInfoObject) query.get(0);
//            Set keySet = get.properties().keySet();
//            for (Object object : keySet) {
//                System.out.println("" + object + " = " + get.properties().get(object));
//            }

            IPluginMgr pluginMgr = infoStore.getPluginMgr();

// Retrieve the Category plugin.
            IPluginInfo categoryPlugin = pluginMgr.getPluginInfo("CrystalEnterprise.AFDashboardPage");

// Create a new InfoObjects collection.
            IInfoObjects newInfoObjects = infoStore.newInfoObjectCollection();

// Add the Category plugin to the collection.
//            newInfoObjects.add(categoryPlugin);

// Retrieve the newly created category object.
            IInfoObject iObject = newInfoObjects.copy(get,
                    IInfoObjects.CopyModes.COPY_NEW_OBJECT_NEW_FILES);
//            IInfoObject iObject = (IInfoObject) newInfoObjects.get(0);

// Set the category object's InfoObject properties.
            iObject.setTitle("tomek3");
//            iObject.setDescription("");
//            iObject.properties().setProperty(CePropertyID.SI_PARENTID,
//                    721);
//            iObject.properties().setProperty(CePropertyID.SI_PARENT_FOLDER,
//                    721);
            iObject.properties().setProperty(16777250,
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <LAYOUT> <DOCKING_MODE/> <PAGE> <HEADER HEIGHT=\"1\"/> <COLUMN> <WINDOW TITLE=\"test\" STATE=\"OPEN\" BORDER=\"YES\" WINID=\"id_81_559owoib\" EXPANDEDBAR=\"NO\"> <GEOMETRY X=\"0\" Y=\"0\" WIDTH=\"1420\" HEIGHT=\"373\"/> <DOCUMENT NAME=\"test\" DOCEXT=\"wid\" REPORTMODE=\"part\" DOCNAME=\" test\" REPOTYPE=\"C\" DESCRIPTION=\"\" DOCID=\"AZlWqjAyECVGmx5MxqPJhYY\" DOMAIN=\"DEV/ST\" /> </WINDOW> </COLUMN> </PAGE> </LAYOUT> ");
//            objectID = iObject.getID();

// Save the new category to the CMS.
            infoStore.commit(newInfoObjects);



            enterpriseSession.logoff();
        } catch (Exception ex) {
            if (enterpriseSession != null) {
                enterpriseSession.logoff();
            }
            System.out.println("Błąd: " + ex.getMessage());
        }
    }
}
