/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.Window;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.BIKSProgressInfoDialog;
import pl.fovis.client.dialogs.SystemConfigurationDialogBase;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.LameRuntimeException;

/**
 *
 * @author ctran
 */
public class MenuConfigurationDialog extends SystemConfigurationDialogBase {

    @Override
    public void doImplement() {
        final BIKSProgressInfoDialog dialog = new BIKSProgressInfoDialog();
        dialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.trwaPostepowanie.get(), true);
        BIKClientSingletons.getService().implementMenuConfigTree(new StandardAsyncCallback<Void>() {

            @Override
            public void onSuccess(Void result) {
                dialog.hideDialog();
                BIKClientSingletons.showInfo(I18n.proszeOdswiezycPrzegladarke.get());
            }

            @Override
            public void onFailure(Throwable caught) {
                dialog.hideDialog();
                LameRuntimeException e = (LameRuntimeException) caught;
                Window.alert(e.getMessage());
            }
        });
    }

    @Override
    public void doBuildConfigTree(boolean deleteOldTree) {
        final BIKSProgressInfoDialog dialog = new BIKSProgressInfoDialog();
        dialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.trwaPostepowanie.get(), true);
        BIKClientSingletons.getService().buildMenuConfigTree(deleteOldTree, new StandardAsyncCallback<Void>() {

            @Override
            public void onSuccess(Void result) {
                dialog.hideDialog();
                BIKClientSingletons.showInfo(I18n.proszeOdswiezycPrzegladarke.get());
            }

            @Override
            public void onFailure(Throwable caught) {
                dialog.hideDialog();
                LameRuntimeException e = (LameRuntimeException) caught;
                Window.alert(e.getMessage());
            }
        });
    }

    @Override
    public String getConfigurationAppPropCode() {
        return BIKConstants.APP_PROP_MENU_CONF;
    }
}
