/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.wezyrtreegrid;

import java.util.Map;

/**
 *
 * @author wezyr
 */
public class MapRowCellValBroker implements IRowCellValBroker<Map<String, Object>> {

    protected String rowIconUrlFieldName;
    protected String rowIconTitleFieldName;
    protected String additionalRowIconUrlFieldName;

    public MapRowCellValBroker() {
        this(null);
    }

    public MapRowCellValBroker(String rowIconUrlFieldName) {
        this(rowIconUrlFieldName, null, null);
    }

    public MapRowCellValBroker(String rowIconUrlFieldName,
            String additionalRowIconUrlFieldName, String rowIconTitleFieldName) {
        this.rowIconUrlFieldName = rowIconUrlFieldName;
        this.additionalRowIconUrlFieldName = additionalRowIconUrlFieldName;
        this.rowIconTitleFieldName = rowIconTitleFieldName;
    }

    public Object getCellVal(Map<String, Object> row, String colName) {
        return row.get(colName);
    }

    public String getRowIconUrl(Map<String, Object> row) {
        return rowIconUrlFieldName == null ? null : (String) row.get(rowIconUrlFieldName);
    }

    public String getAdditionalRowIconUrl(Map<String, Object> row) {
        return additionalRowIconUrlFieldName == null ? null : (String) row.get(additionalRowIconUrlFieldName);
    }

    public String getRowIconTitle(Map<String, Object> row) {
        return rowIconTitleFieldName == null ? null : (String) row.get(rowIconTitleFieldName);
    }
}
