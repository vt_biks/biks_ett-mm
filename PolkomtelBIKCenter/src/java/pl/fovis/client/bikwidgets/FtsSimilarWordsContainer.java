/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.event.logical.shared.AttachEvent;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.List;
import pl.fovis.common.fts.SimilarWordsWithCounts;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.ViewAndJoinSimilarNodesDialog;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.basewidgets.IsWidgetBase;
import pl.fovis.foxygwtcommons.wezyrtreegrid.InlineFlowPanel;
import simplelib.BaseUtils;
import simplelib.IContinuation;

/**
 *
 * @author pmielanczuk
 */
public class FtsSimilarWordsContainer extends IsWidgetBase<FlowPanel> {

    protected List<SimilarWordsWithCounts> words;
    protected IContinuation wordWeightChangedCont;
    protected IContinuation wordListChangedCont;
    protected TextBox tbFilter = new TextBox();
    protected List<Widget> wordBoxWidgets;
    protected String currFilterVal;
    protected Timer filterValChangeTimer;
    protected long lastPossibleFilterValChange = -1;

    public FtsSimilarWordsContainer(IContinuation wordWeightChangedCont, IContinuation wordListChangedCont) {
        this.wordWeightChangedCont = wordWeightChangedCont;
        this.wordListChangedCont = wordListChangedCont;

        tbFilter.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                maybeFilterValueChanged();
            }
        });

        tbFilter.addMouseUpHandler(new MouseUpHandler() {

            @Override
            public void onMouseUp(MouseUpEvent event) {
                maybeFilterValueChanged();
            }
        });

        tbFilter.addAttachHandler(new AttachEvent.Handler() {

            @Override
            public void onAttachOrDetach(AttachEvent event) {
                boolean isAttached = event.isAttached();
                if (filterValChangeTimer != null) {
                    filterValChangeTimer.cancel();
                }
                if (isAttached) {
                    filterValChangeTimer = new Timer() {

                        @Override
                        public void run() {
                            checkIfFilterValueChanged();
                        }
                    };
                    filterValChangeTimer.scheduleRepeating(200);
                } else {
                    filterValChangeTimer = null;
                }
            }
        });
    }

    protected void checkIfFilterValueChanged() {

        long currentTimeMillis = System.currentTimeMillis();

        if (lastPossibleFilterValChange == -1 || currentTimeMillis - lastPossibleFilterValChange < 300) {
            return;
        }

        String newFilterVal = tbFilter.getValue();

        lastPossibleFilterValChange = -1;

        if (BaseUtils.safeEquals(currFilterVal, newFilterVal)) {
            return;
        }

        currFilterVal = newFilterVal;

        for (int i = mainWidget.getWidgetCount() - 1; i > 0; i--) {
            mainWidget.remove(i);
        }

        addWordBoxes();

        if (wordListChangedCont != null) {
            wordListChangedCont.doIt();
        }
    }

    protected void maybeFilterValueChanged() {
        lastPossibleFilterValChange = System.currentTimeMillis();
    }

    public static void diagMsg(String msg) {
        ViewAndJoinSimilarNodesDialog.diagMsg(msg);
    }

    @Override
    protected FlowPanel createMainWidget() {
        FlowPanel res = new FlowPanel();

        res.setStyleName("fts_similarWordsContainer");

        return res;
    }

    @Override
    protected void clearMainWidget() {
        super.clearMainWidget();
        clear();
    }

    protected void wordWeightChanged() {
        if (wordWeightChangedCont != null) {
            wordWeightChangedCont.doIt();
        }
    }

    @Override
    protected void fillMainWidget() {

        mainWidget.add(tbFilter);

        addWordBoxes();
    }

    protected void addWordBoxes() {
        if (words == null) {
            return;
        }

        String filterTxt = tbFilter.getValue();

        for (int i = 0; i < words.size(); i++) {

            SimilarWordsWithCounts word = words.get(i);

            if (!BaseUtils.isStrEmpty(filterTxt) && !(word.word.contains(filterTxt))) {
                continue;
            }

            mainWidget.add(wordBoxWidgets.get(i));
        }
    }

    protected void createWordBoxWidgets() {
        wordBoxWidgets = new ArrayList<Widget>(words.size());

        for (SimilarWordsWithCounts word : words) {

            final SimilarWordsWithCounts fWord = word;

            InlineFlowPanel fpInner = new InlineFlowPanel();
            fpInner.setStyleName("fts_similarWordBox");

            final InlineLabel ilblIncWeight = new InlineLabel("");
            ilblIncWeight.setStyleName("fts_adjustWeightBtn fts_incWeight");
            InlineLabel ilblDecWeight = new InlineLabel("");
            ilblDecWeight.setStyleName("fts_adjustWeightBtn fts_decWeight");

            ClickHandler ch = new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    diagMsg("click!");
                    boolean doInc = event.getSource() == ilblIncWeight;
                    BIKClientSingletons.getFtsService().changeWordWeight(fWord.word,
                            fWord.weight + (doInc ? 1 : -1) * 0.5, new StandardAsyncCallback<Void>() {

                                @Override
                                public void onSuccess(Void result) {
                                    diagMsg("click - back!");
                                    wordWeightChanged();
                                }
                            });
                }
            };

            ilblIncWeight.addClickHandler(ch);
            ilblDecWeight.addClickHandler(ch);

            fpInner.add(ilblIncWeight);
            final InlineLabel lblWord = new InlineLabel(word.word);
            lblWord.setStyleName("fts_word");
            final InlineLabel lblUsageCnt = new InlineLabel(Integer.toString(word.usageInObjCount));
            lblUsageCnt.setStyleName("fts_usageCnt");
            final InlineLabel lblWeight = new InlineLabel(BaseUtils.doubleToString(word.weight, 3));
            lblWeight.setStyleName("fts_weight");
            fpInner.add(lblWord);
            fpInner.add(lblWeight);
            fpInner.add(lblUsageCnt);
            fpInner.add(ilblDecWeight);
//            final InlineLabel lblSpaceDummy = new InlineLabel("");
//            lblSpaceDummy.setWidth("0px");
//            fpInner.add(lblSpaceDummy); // żeby się mogło złamać na wiele linii

            wordBoxWidgets.add(fpInner);
        }
    }

    public void setData(List<SimilarWordsWithCounts> words) {
        this.words = words;

        createWordBoxWidgets();

        if (mainWidget != null) {
            rebuildMainWidget();
        }
    }

    public void clear() {
        mainWidget.clear();
    }
}
