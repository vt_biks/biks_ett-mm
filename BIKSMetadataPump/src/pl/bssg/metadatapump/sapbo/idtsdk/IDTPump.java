/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.sapbo.idtsdk;

import commonlib.LameUtils;
import commonlib.ProcessUtils;
import commonlib.StreamGobbler;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import pl.bssg.metadatapump.MetadataPumpBase;
import pl.bssg.metadatapump.PumpUtils;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.bssg.metadatapump.common.SapBo4ExPropConfigBean;
import pl.bssg.metadatapump.sapbo.javasdk.SapBOBean;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class IDTPump extends MetadataPumpBase {

    private static final ILameLogger logger = LameUtils.getMyLogger();

    @Override
    public String getPumpGroup() {
        return PumpConstants.PUMP_GROUP_BO;
    }

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_SAPBO4;
    }

    @Override
    protected Integer insertMainLog() {
        return getLastStartLog(getSourceName());
    }

    @Override
    protected Pair<Integer, String> startPump() {
        SapBOBean configBean = getAdhocDao().createBeanFromNamedQry("getBOConnectionParamatersForSource", SapBOBean.class, true, getSourceName(), instanceName);
        if (configBean.isBo40) {
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_DONE, "Skipping the loading from IDT SDK (only supported in BO 4.1+)");
        }

        execNamedCommandWithCommit("createBOInstanceFunctions", getSourceName(), instanceName);

        SapBo4ExPropConfigBean params = PumpUtils.readAdminBean("sapbo4", SapBo4ExPropConfigBean.class, adhocDao);
//        String boClientToolsPATH = "C:\\Program Files (x86)\\SAP BusinessObjects\\SAP BusinessObjects Enterprise XI 4.0\\";
        String boClientToolsPATH = BaseUtils.ensureStrHasSuffix(params.clientToolsPath.replace("\\", "/").trim(), "/", true);
//        String pathToIDTPump = "C:\\Users\\tflorczak\\Desktop\\TEST\\BIKSPumpSAPBO41IDT.jar";
        String pathToIDTPump = params.idtPath.replace("\\", "/");
        String jREx86Path = params.jREx86Path.replace("\\", "/");

        File file = new File(pathToIDTPump);
        if (!file.exists()) {
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "Wrong path or file (idt pump) does not exists!");
        }

        MssqlConnectionConfig connectionConfig = pumpLogger.getConnectionConfig();
        execNamedCommandWithCommit("prepareMultiInstanceTablesForDesignerSDK");
        // create candidate list (in aaa_universe_delta)
        execNamedCommandWithCommit("createCandidateList", true);
        // remove old / bad records in temp tables
        execNamedCommandWithCommit("deleteOldOrBadRecords");
        // run exe file / pump
        try {
            Map<String, String> env = new HashMap<String, String>(System.getenv());
            env.put("Path", jREx86Path + ";" // fix pod jre x86
                    + boClientToolsPATH + "win32_x86;" + env.get("Path"));
            final String[] envAsStringTable = BaseUtils.mapToStringArray(env);

//            set path=C:\Program Files (x86)\SAP BusinessObjects\SAP BusinessObjects Enterprise XI 4.0\win32_x86;%path%
//            "C:\Program Files (x86)\Java\jre1.8.0\bin\java" -Dbusinessobjects.connectivity.directory="C:\Program Files (x86)\SAP BusinessObjects\SAP BusinessObjects Enterprise XI 4.0\dataAccess\connectionServer" -jar BIKSPumpSAPBO41IDT.jar
            String[] callAndArgs = {"cmd.exe", "/C", "java -Xmx1024M -Dbusinessobjects.connectivity.directory=\"" + boClientToolsPATH + "dataAccess/connectionServer\" -jar \"" + pathToIDTPump + "\" " + connectionConfig.server + " " + (connectionConfig.instance == null ? "\"\"" : connectionConfig.instance) + " " + connectionConfig.database + " " + connectionConfig.user + " " + connectionConfig.password + " \"" + instanceName + "\""};
//                System.out.println("Polecenie: " + Arrays.toString(callAndArgs));
            Process p = ProcessUtils.executeProcess(callAndArgs, envAsStringTable);
            StreamGobbler errorGobbler = new StreamGobbler(p.getErrorStream(), "ERROR", true);
            StreamGobbler outputGobbler = new StreamGobbler(p.getInputStream(), "OUTPUT", true);
            errorGobbler.start();
            outputGobbler.start();
            //
            p.waitFor();
            errorGobbler.join();
//            String errorMsg = errorGobbler.getErrorMsg();
//            if (errorMsg != null) {
//                return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, errorMsg);
//            }
            execNamedCommandWithCommit("finishMultiInstanceTablesForDesignerSDK");

            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_DONE, "IDT SDK: Success");
        } catch (InterruptedException ie) {
            if (logger.isErrorEnabled()) {
                logger.error("InterruptedException Error: " + ie.getMessage(), ie);
            }
            throw new LameRuntimeException(ie);
        }
    }
}
