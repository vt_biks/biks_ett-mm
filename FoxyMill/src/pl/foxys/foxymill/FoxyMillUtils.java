/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill;

import pl.foxys.foxymill.exprlang.IParsedExpression;
import pl.foxys.foxymill.exprlang.IExpressionLanguage;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import pl.foxys.foxymill.exprlang.mvel.MvelExpressionLanguage;
import pl.foxys.foxymill.exprlang.ognl.OgnlExpressionLanguage;
import pl.foxys.foxymill.renderctx.CachingOutputDevice;
import pl.foxys.foxymill.renderctx.IRenderCtx;
import pl.foxys.foxymill.renderctx.IOutputDevice;
import pl.foxys.foxymill.renderctx.RenderCtx;
import pl.foxys.foxymill.renderctx.StringOutputDevice;
import pl.foxys.foxymill.tags.CacheTag;
import pl.foxys.foxymill.tags.ITag;
import pl.trzy0.foxy.commons.templates.ParamInfo;
import simplelib.BaseUtils;
import simplelib.IValueFormatter;
import simplelib.LameRuntimeException;
import simplelib.TextFilePosition;

/**
 *
 * @author wezyr
 */
public class FoxyMillUtils {

    public static boolean evalCondExpr(IParsedExpression condExpr, //IRenderContext
            IRenderCtx renderCtx) {
        Object condVal = condExpr.eval(renderCtx);

        if (condVal != null && !(condVal instanceof Boolean)) {
            throw new LameRuntimeException("cond value is not a boolean (it is of class: "
                    + BaseUtils.safeGetClassName(condVal) + ")");
        }
        boolean bVal = BaseUtils.boolNullToDef((Boolean) condVal, false);
        return bVal;
    }

    public static void render(Iterable<? extends IRenderable> rs, //IRenderContext renderCtx
            IRenderCtx renderCtx) {
        if (rs == null) {
            return;
        }
        for (IRenderable r : rs) {

            try {
                r.render(renderCtx);
            } catch (Exception ex) {
                TextFilePosition tfp;
                if (r instanceof IHasTextFilePosition) {
                    IHasTextFilePosition htfp = (IHasTextFilePosition) r;
                    tfp = htfp.getTextFilePosition();
                } else {
                    tfp = null;
                }
                throw new LameRuntimeException("error while rendering " + r + "\n at: " + tfp, ex);
            }
        }
    }

    public static void prepareTags(Collection<ITag> tags, RenderEnvironment renderEnv) {
        if (tags == null) {
            return;
        }
        for (ITag tag : tags) {
            tag.prepare(renderEnv);
        }
    }

    public static void fixupTags(Iterable<ITag> tags, Map<String, Object> globals) {
        if (tags == null) {
            return;
        }
        for (ITag subTag : tags) {
            subTag.fixUp(globals);
        }
    }

//    public static IRenderable wrapAsRenderable(final Collection<? extends IRenderable> rs) {
//        return wrapAsRenderable(rs, null);
//    }
//
    public static IRenderable wrapAsRenderable(final Collection<? extends IRenderable> rs,
            final Map<String, Object> optVarsReplacement, final Map<String, ParamInfo> paramInfos) {
        return new IRenderableWithParamSignature() {

            public void render(IRenderCtx renderCtx) {
                if (optVarsReplacement != null) {
                    IRenderCtx newRenderCtx = RenderCtx.makeRenderCtx(renderCtx.getRootModel(),
                            optVarsReplacement, renderCtx.getOutputDevice());
                    renderCtx = newRenderCtx;
                }
                FoxyMillUtils.render(rs, renderCtx);
            }

            public Collection<String> getParamNames() {
                return paramInfos == null ? Collections.<String>emptyList() : paramInfos.keySet();
            }

            public ParamInfo getParamInfo(String paramName) {
                if (paramInfos != null) {
                    return paramInfos.get(paramName);
                }
                throw new IllegalArgumentException("unknown param name: " + paramName);
            }
        };
    }

    public static IRenderable wrapAsRenderable(final Collection<? extends IRenderable> rs,
            final Map<String, Object> optVarsReplacement) {
        return wrapAsRenderable(rs, optVarsReplacement, null);
//        return new IRenderable() {
//
//            public void internalRender(IRenderContext renderCtx) {
//                if (baseVars != null) {
//                    renderCtx = new ClonedRenderContext(renderCtx, baseVars);
//                }
//                FoxyMillUtils.internalRender(rs, renderCtx);
//            }
//        };
    }

//    public static String renderWithParams(IRenderable r, Map<String, Object> params,
//            //Object evalContext
//            IRenderContext ctxBase) {
//        SimpleRenderContext newRenderCtx = new SimpleRenderContext(//evalContext
//                ctxBase);
//
//        if (params != null) {
//            newRenderCtx.setVars(params);
//        }
//        r.internalRender(newRenderCtx);
//        return newRenderCtx.getOutputStr();
//    }
    public static RenderEnvironment makeRenderEnv(boolean defaultMvel, boolean disableCaching,
            IValueFormatter vf) {
        RenderEnvironment res = new RenderEnvironment(vf);
        res.exprLangs = getExprLangs(defaultMvel);
        res.cacheStorage = disableCaching ? null : new CacheStorage();
        return res;
    }

    public static RenderEnvironment makeRenderEnvForHTML(boolean defaultMvel, boolean disableCaching) {
        return makeRenderEnv(defaultMvel, disableCaching, new ValueFormatterForHTML());
    }

    public static Map<String, IExpressionLanguage> getExprLangs(boolean defaultMvel) {
        Map<String, IExpressionLanguage> res = new LinkedHashMap<String, IExpressionLanguage>();
        MvelExpressionLanguage mvel = new MvelExpressionLanguage();
        OgnlExpressionLanguage ognl = new OgnlExpressionLanguage();
        res.put("mvel", mvel);
        res.put("ognl", ognl);
        res.put(null, defaultMvel ? mvel : ognl);
        return res;
    }

//    public CacheItem renderWithCaching(IRenderable r, Map<String, Object> newVars, Object rootModel) {
//        IRenderCtx renderCtx = RenderCtx.makeCachingRenderCtx(rootModel, newVars);
//        r.internalRender(renderCtx);
//        CacheItem cacheItem = renderCtx.getSubCacheHandler().doneRendering();
//        return cacheItem;
//    }
    public static void renderCacheItem(CacheItem item, Object rootModel, IOutputDevice outputDevice,
            boolean performRecache) {
        int i;
        for (i = 0; i < item.subCacheTags.size(); i++) {
            outputDevice.append(item.renderedParts.get(i));
            //SimpleRenderContext src = new SimpleRenderContext(renderCtx.getEvalContext(), item.subCacheVars.get(i));
            //renderCtx.getVars().putAll(item.subCacheVars.get(i));
            IRenderCtx rc = RenderCtx.makeRenderCtx(rootModel, item.subCacheVars.get(i),
                    outputDevice);
            CacheTag ct = item.subCacheTags.get(i);
            if (performRecache) {
                ct.internalRender(rc);
            } else {
                ct.renderSubTags(rc);
            }
            //renderCtx.appendToOutput(src.getOutputStr());
        }
        outputDevice.append(item.renderedParts.get(i));
    }

    public static CacheItem renderToCacheItem(//IRenderCtx renderCtx,
            Object rootModel, Map<String, Object> vars,
            IRenderable r) {
        CachingOutputDevice cod = new CachingOutputDevice();
        IRenderCtx cachingRenderCtx = RenderCtx.makeRenderCtx(
                //renderCtx.getRootModel(), renderCtx.getVars(),
                rootModel, vars,
                cod);
        //FoxyMillUtils.internalRender(subTags, cachingRenderCtx);
        r.render(cachingRenderCtx);
        CacheItem item = cod.doneRendering();
        return item;
    }

    // rx instanceof IRenderable or rx instanceof Iterable(<? extends IRenderable>)
    @SuppressWarnings("unchecked")
    public static String innerRenderToString(IRenderCtx renderCtx, Map<String, Object> vars,
            Object rx) {
        StringOutputDevice od = new StringOutputDevice();
        IRenderCtx cachingRenderCtx = RenderCtx.makeRenderCtx(
                renderCtx.getRootModel(), vars, od);

        if (rx instanceof IRenderable) {
            IRenderable r = (IRenderable) rx;
            r.render(cachingRenderCtx);
        } else if (rx instanceof Iterable) {
            Iterable<IRenderable> rs = (Iterable<IRenderable>) rx;
            render(rs, cachingRenderCtx);
        }

        return od.doneRendering();
    }

    public static String innerRenderToString(IRenderCtx renderCtx, Object rx) {
        return innerRenderToString(renderCtx, renderCtx.getVars(), rx);
//        StringOutputDevice od = new StringOutputDevice();
//        IRenderCtx cachingRenderCtx = RenderCtx.makeRenderCtx(
//                renderCtx.getRootModel(), renderCtx.getVars(), od);
//
//        if (rx instanceof IRenderable) {
//            IRenderable r = (IRenderable) rx;
//            r.render(cachingRenderCtx);
//        } else if (rx instanceof Iterable) {
//            Iterable rs = (Iterable) rx;
//            render(rs, cachingRenderCtx);
//        }
//
//        return od.doneRendering();
    }

    public static String renderToString(IRenderCtx renderCtx, IRenderable r) {
//        StringOutputDevice od = new StringOutputDevice();
//        IRenderCtx cachingRenderCtx = RenderCtx.makeRenderCtx(
//                renderCtx.getRootModel(), renderCtx.getVars(), od);
//        //FoxyMillUtils.internalRender(subTags, cachingRenderCtx);
//        r.internalRender(cachingRenderCtx);
//        return od.doneRendering();
        return innerRenderToString(renderCtx, r);
    }

    public static String renderToString(IRenderCtx renderCtx, Map<String, Object> vars,
            IRenderable r) {
        return innerRenderToString(renderCtx, vars, r);
    }

    public static String renderToString(IRenderCtx renderCtx, Iterable<? extends IRenderable> rs) {
//        StringOutputDevice od = new StringOutputDevice();
//        IRenderCtx cachingRenderCtx = RenderCtx.makeRenderCtx(
//                renderCtx.getRootModel(), renderCtx.getVars(), od);
//        //FoxyMillUtils.internalRender(subTags, cachingRenderCtx);
//        internalRender(rs, cachingRenderCtx);
//        return od.doneRendering();
        return innerRenderToString(renderCtx, rs);
    }
}
