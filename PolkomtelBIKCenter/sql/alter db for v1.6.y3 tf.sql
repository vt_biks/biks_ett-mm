﻿------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- Indexy do przyśpieszenia wyświetlania stron z Con.
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
exec sp_check_version '1.6.y3';
go

if not exists (select * from sys.indexes where object_id = OBJECT_ID(N'[dbo].[bik_confluence_extradata]') and name = N'idx_bik_confluence_extradata_node_id_url')
begin
	create index idx_bik_confluence_extradata_node_id_url on bik_confluence_extradata(node_id, url)
end;
go

if not exists (select * from sys.indexes where object_id = OBJECT_ID(N'[dbo].[bik_confluence_extradata]') and name = N'idx_bik_confluence_extradata_url_node_id')
begin
	create index idx_bik_confluence_extradata_url_node_id on bik_confluence_extradata(url, node_id)
end;
go

exec sp_update_version '1.6.y3', '1.6.y4';
go
