/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.plainfile;

import com.fasterxml.jackson.databind.ObjectMapper;
import commonlib.LameUtils;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import jcifs.smb.NtlmPasswordAuthentication;
import pl.bssg.metadatapump.IPumpLogger;
import pl.bssg.metadatapump.MetadataPumpBase;
import pl.bssg.metadatapump.common.ConnectionParametersPlainFileBean;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.bssg.metadatapump.filesystem.IFileDelegator;
import pl.bssg.metadatapump.filesystem.LocalFileDelegator;
import pl.bssg.metadatapump.filesystem.PumpFileUtils;
import pl.bssg.metadatapump.filesystem.SmbFileDelegator;
import pl.bssg.metadatapump.genericimporter.CSVImporter;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import simplelib.BaseUtils;
import simplelib.CSVReader;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author ctran
 */
public class PlainFilePump extends MetadataPumpBase {

    protected static final String DEFAULT_DESCR = "";
    protected static final String DEFAULT_VISUAL_ORDER = "0";
    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected ConnectionParametersPlainFileBean config;
    protected TreeExportMetadata[] importConfigs;
    protected TreeExportMetadata metadata;
    protected String branchNodeKindCode;
    protected String leafNodeKindCode;
    protected Set<String> parentObjIds = new HashSet<String>();
    protected Set<String> nodeKindCodes = new HashSet<String>();
    protected List<Map<String, String>> content = new ArrayList<Map<String, String>>();
    protected boolean isEmbeded = false; //config nie jest pobrany z bazy, tylko gdzies indziej ustawiamy
    protected Map<String, Integer> joinedAttrNames = new HashMap<String, Integer>();
    protected List<ImportedJoinedObj> allJoinedObjs = new ArrayList<ImportedJoinedObj>();
    protected List<ImportedLinkedObj> allLinkedObjs = new ArrayList<ImportedLinkedObj>();
    protected List<ImportedHyperLinkInObj> allHyperLinkInObjs = new ArrayList<ImportedHyperLinkInObj>();

    public PlainFilePump() {
    }

    public PlainFilePump(String instanceName) {
        this.instanceName = instanceName;
    }

    public void setEmbedded(boolean embeddedMode) {
        this.isEmbeded = embeddedMode;
    }

    public void setConfig(ConnectionParametersPlainFileBean config) {
        this.config = config;
    }

    public void setPumpLogger(IPumpLogger pumpLogger) {
        this.pumpLogger = pumpLogger;
    }

    public void setMetadata(TreeExportMetadata metadata) {
        this.metadata = metadata;
    }

    @Override
    protected boolean isEmbeded() {
        return isEmbeded;
    }

    @Override
    protected Pair<Integer, String> startPump() {
        try {
            if (!isEmbeded()) {
                config = adhocDao.createBeanFromNamedQry("getPlainFileConfigForInstanceName", ConnectionParametersPlainFileBean.class, true, instanceName);
            }
            tryReadImportMetadata(config);

            checkTreeAutoObjIds();

            importTrees();

            importRelatedObjs();

            pumpLogger.pumpLog("Zakończono zasilanie", null);

            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_DONE, PumpConstants.PUMP_DATA_LOAD_SUCCESS);
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in Plain File pump", ex);
            }
            pumpLogger.pumpLog("Niespodziewany błąd: " + ex.getMessage(), null);
            ex.printStackTrace();
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "PlainFilePump error: " + ex.getMessage());
        }
    }

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_PLAIN_FILE;
    }

    private void tryReadImportMetadata(ConnectionParametersPlainFileBean config) throws MalformedURLException, IOException {
        if (!isEmbeded()) {
            IFileDelegator folder;
            if (config.isRemote) {
                NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(config.domain, config.username, config.password);
                folder = new SmbFileDelegator(PumpFileUtils.fixPathIfNessecary(config.sourceFolderPath), auth);
            } else {
                folder = new LocalFileDelegator(config.sourceFolderPath);
            }
            IFileDelegator[] listMetadataFiles = folder.listFiles("*.metadata");
            if (listMetadataFiles.length != 1) {
                throw new LameRuntimeException("Aby poprawnie zaimportować dane, umieść tylko jeden plik METADATA w folderze, z którego importujesz.");
            }
            IFileDelegator file = listMetadataFiles[0];
            ObjectMapper jsonMapper = new ObjectMapper();
            importConfigs = jsonMapper.readValue(file.getInputStream(), TreeExportMetadata[].class);
        } else {
            if (metadata == null) {
                throw new LameRuntimeException("EmbededMode: brak pliku METADATA");
            }
            importConfigs = new TreeExportMetadata[1];
            importConfigs[0] = metadata;
        }
    }

    private List<Map<String, String>> rebuildTreeContentAndReCheckCompatibleness() {
        Pair<List<Map<String, String>>, List<String>> fileContent = readCsvAsMap(metadata.baseFileName);
        List<String> headerList = fileContent.v2;
        if (BaseUtils.isCollectionEmpty(metadata.parentObjIds)) {
            metadata.parentObjIds = new ArrayList<String>();
            for (String colName : headerList) {
                if (colName.startsWith(PumpConstants.GENERIC_NODE_PARENT_OBJID)) {
                    metadata.parentObjIds.add(colName);
                }
            }
        }
        if (BaseUtils.isMapEmpty(metadata.attributes)) {
            metadata.attributes = new HashMap<String, String>();
            for (String colName : headerList) {
                if (!PumpConstants.GENERIC_NODE_NAME.equals(colName)
                        && !PumpConstants.GENERIC_NODE_NODE_KIND_CODE.equals(colName)
                        && !PumpConstants.GENERIC_NODE_DESCR.equals(colName)
                        && !PumpConstants.GENERIC_NODE_VISUAL_ORDER.equals(colName)
                        && !colName.startsWith(PumpConstants.GENERIC_NODE_PARENT_OBJID)) {
                    metadata.attributes.put(colName, colName);
                }
            }
        }
        Set<String> allColNames = new HashSet<String>(fileContent.v2);
        if (!allColNames.contains(metadata.name)) {
            //                || !allColNames.contains(metadata.nodeKindCode)
//                || !allColNames.contains(metadata.desc)
//                || !allColNames.contains(metadata.visualOrder)) {
            throw new LameRuntimeException("Brak wymaganych kolumn. Poprawnie uzupełnij plik METADATA ");
        }
        if (!BaseUtils.isMapEmpty(metadata.attributes)) {
            for (String attrCol : metadata.attributes.values()) {
                if (!allColNames.contains(attrCol)) {
                    throw new LameRuntimeException("Brak wymaganych kolumn lub ich niepoprawna nazwa:\"" + attrCol + "\"");
                }
            }
        }

        autoSelectNodeKindCodeIfNecessary(allColNames);

        convertRawTreeContentFromFile(fileContent.v1);

        generateParentNodesIfNecessary();

        addNodeKindIfNecessary();

        checkNodeKindCompatibleness();

        return content;
    }

    private Pair<List<Map<String, String>>, List<String>> readCsvAsMap(String fileName) {
        String filePath = BaseUtils.ensureDirSepPostfix(config.sourceFolderPath) + fileName;
        NtlmPasswordAuthentication auth = config.isRemote ? new NtlmPasswordAuthentication(config.domain, config.username, config.password) : null;
        String csvContent = LameUtils.loadAsString(PumpFileUtils.readFileAsInputStream(filePath, config.isRemote, auth), "*Cp1250,ISO-8859-2,UTF-8");
        CSVReader reader = new CSVReader(csvContent, CSVReader.CSVParseMode.CONSIDER_QUOTES, ';', true);
        return new Pair<List<Map<String, String>>, List<String>>(reader.readFullTableAsMaps(), reader.getColNames());
    }

    public Map<String, Integer> extractJoinedAttrNames(List<String> attrColNames, Integer lastDstParentColId) {
        Map<String, Integer> attrNames2IdMap = new HashMap<String, Integer>();
        for (int i = lastDstParentColId + 1; i < attrColNames.size(); i++) {
            String attrColName = attrColNames.get(i);
            if (!PumpConstants.GENERIC_IS_INHERIT.equalsIgnoreCase(attrColName)
                    && !PumpConstants.GENERIC_TYPE.equalsIgnoreCase(attrColName)
                    && !PumpConstants.GENERIC_MAIN_ATTRIBUTE.equalsIgnoreCase(attrColName)) {

                String attrName = null;
                if (attrColName.endsWith(PumpConstants.GENERIC_ATTRIBUTE_SRC_SUFFIX)) {
                    attrName = attrColName.substring(0, attrColName.lastIndexOf(PumpConstants.GENERIC_ATTRIBUTE_SRC_SUFFIX));
                } else if (attrColName.endsWith(PumpConstants.GENERIC_ATTRIBUTE_DST_SUFFIX)) {
                    attrName = attrColName.substring(0, attrColName.lastIndexOf(PumpConstants.GENERIC_ATTRIBUTE_DST_SUFFIX));
                }
                if (!BaseUtils.isStrEmptyOrWhiteSpace(attrName)) {
                    Integer attrId = BeanConnectionUtils.execNamedQuerySingleVal(adhocDao, "getOrCreateJoinedObjAttr", false, "id", attrName);
                    attrNames2IdMap.put(attrName, attrId);
                }
            }
        }
        return attrNames2IdMap;
    }

    protected void checkTreeExistenceAndCompatibleAttributes() {
        //check treeCode
        Integer treeId = execNamedQuerySingleVal("treeIdByTreeCode", true, null, metadata.treeCode);
        if (treeId == null) {
            throw new LameRuntimeException("Drzewo \"" + metadata.treeCode + "\" nie zostało utworzone");
        }
        //sprawdź czy atrybuty zostały przypisane do drzewa
        //TODO: sprawdz czy atrybuty zostaly przypisane do konkretnego typu node_kind
        if (!BaseUtils.isMapEmpty(metadata.attributes)) {
            for (String attrName : metadata.attributes.keySet()) {
                if (BaseUtils.isCollectionEmpty(execNamedQuerySingleColAsSet("getAttrIdByAttrNameAndTreeId", null, attrName, treeId))) {
                    throw new LameRuntimeException("Atrybut \"" + attrName + "\" nie został przypisany do drzewa \"" + metadata.treeCode + "\"");
                }
            }
        }
        //obj_ids
//        if (BaseUtils.isCollectionEmpty(metadata.objIds)) {
//            throw new LameRuntimeException("Brak objIds.");
//        }
    }

    private void generateDefaultParentNode(String parentObjId) {
        if (BaseUtils.isStrEmptyOrWhiteSpace(parentObjId) || parentObjIds.contains(parentObjId)) {
            return;
        }
        if (BaseUtils.isStrEmptyOrWhiteSpace(branchNodeKindCode)) {
            throw new LameRuntimeException("Brak zdefiniowanego NODE_KIND_CODE dla elementu nadrzędnego(gałęzi) dla drzewa: \"" + metadata.treeCode + "\"");
        }
        pumpLogger.pumpLog("Generowanie elementu nadrzędnego(gałęzi): \"" + parentObjId + "\"", null);
        Map<String, String> parentNode = new HashMap<String, String>();
        List<String> elementalParentObjIds = BaseUtils.splitBySep(parentObjId, "|");
        elementalParentObjIds.remove(elementalParentObjIds.size() - 1);
        String parentName = elementalParentObjIds.get(elementalParentObjIds.size() - 1);
        elementalParentObjIds.remove(elementalParentObjIds.size() - 1);
        String grandParentObjId = BaseUtils.ensureObjIdEndWith(BaseUtils.mergeWithSepEx(elementalParentObjIds, "|"), "|");
        if ("|".equals(grandParentObjId)) {
            grandParentObjId = "";
        }

        parentNode.put(PumpConstants.GENERIC_NODE_OBJID, parentObjId);
        parentNode.put(PumpConstants.GENERIC_NODE_PARENT_OBJID, grandParentObjId);
        parentNode.put(PumpConstants.GENERIC_NODE_DESCR, DEFAULT_DESCR);
        parentNode.put(PumpConstants.GENERIC_NODE_NAME, parentName);
        parentNode.put(PumpConstants.GENERIC_NODE_NODE_KIND_CODE, branchNodeKindCode);
        parentNode.put(PumpConstants.GENERIC_NODE_VISUAL_ORDER, DEFAULT_VISUAL_ORDER);

        content.add(parentNode);
        parentObjIds.add(parentObjId.toLowerCase());
    }

    private void convertRawTreeContentFromFile(List<Map<String, String>> raw) {
        content.clear();
        nodeKindCodes.clear();

        for (Map<String, String> line : raw) {
            Map<String, String> node = new HashMap<String, String>();

            String objId = "";
            String parentObjId = "";
            if (!BaseUtils.isCollectionEmpty(metadata.parentObjIds)) {
                for (String idColName : metadata.parentObjIds) {
                    if (BaseUtils.isStrEmptyOrWhiteSpace(line.get(idColName))) {
                        break;
                    }
                    parentObjId = objId;
                    objId = objId + line.get(idColName) + "|";
                }
            }
            parentObjId = objId;
            objId = BaseUtils.ensureObjIdEndWith("".equals(objId) ? line.get(metadata.name) : objId + line.get(metadata.name), "|");
            String nodeKindCode = line.get(metadata.nodeKindCode);
            if (nodeKindCode != null) {
                nodeKindCodes.add(nodeKindCode);
            }
            node.put(PumpConstants.GENERIC_NODE_OBJID, objId);
            node.put(PumpConstants.GENERIC_NODE_PARENT_OBJID, parentObjId);
            String name = line.get(metadata.name);
            if (BaseUtils.isStrEmptyOrWhiteSpace(name)) {
                throw new LameRuntimeException("Brak kolumny NAME lub jej wartość jest pusta");
            }
            node.put(PumpConstants.GENERIC_NODE_NAME, name);
            if (nodeKindCode != null) {
                node.put(PumpConstants.GENERIC_NODE_NODE_KIND_CODE, nodeKindCode);
            }
            String desc = BaseUtils.isStrEmptyOrWhiteSpace(line.get(metadata.desc)) ? DEFAULT_DESCR : line.get(metadata.desc);
            node.put(PumpConstants.GENERIC_NODE_DESCR, desc);
            String visualOrder = BaseUtils.isStrEmptyOrWhiteSpace(line.get(metadata.visualOrder)) ? DEFAULT_VISUAL_ORDER : line.get(metadata.visualOrder);
            node.put(PumpConstants.GENERIC_NODE_VISUAL_ORDER, visualOrder);

            if (!BaseUtils.isMapEmpty(metadata.attributes)) {
                for (Entry<String, String> attr : metadata.attributes.entrySet()) {
                    String attrName = attr.getKey();
                    String attrValue = line.get(attr.getValue());
                    node.put(attrName, attrValue);
                }
            }

            content.add(node);
        }
        if (BaseUtils.isCollectionEmpty(nodeKindCodes)) {
            nodeKindCodes.add(branchNodeKindCode);
            nodeKindCodes.add(leafNodeKindCode);
        }
    }

    private void autoSelectNodeKindCodeIfNecessary(Set<String> allColNames) {
        branchNodeKindCode = null;
        leafNodeKindCode = null;
        boolean mustThrowException = false;
        if (!allColNames.contains(metadata.nodeKindCode)) {
            pumpLogger.pumpLog("Automatycznie wybrano NODE_KIND_CODE", null);
            mustThrowException = true;
        }
        List<Map<String, Object>> allNodeKindOfTree = adhocDao.execNamedQueryCFN("getAllNodeKindOfTree", null, metadata.treeCode);
        if (allNodeKindOfTree.size() != 2) {
            if (mustThrowException) {
                throw new LameRuntimeException("Nie można zaimportować do drzewa \"" + metadata.treeCode + "\", które ma więcej niż dwa typy węzłów bez wskazywania kolumny NODE_KIND_CODE");
            } else {
                return;
            }
        }
        for (Map<String, Object> e : allNodeKindOfTree) {
            if ("1".equals(BaseUtils.safeToString(e.get("is_folder")))) {
                branchNodeKindCode = BaseUtils.safeToString(e.get("node_kind_code"));
            } else if ("0".equals(BaseUtils.safeToString(e.get("is_folder")))) {
                leafNodeKindCode = BaseUtils.safeToString(e.get("node_kind_code"));
            }
        }

    }

    private void generateParentNodesIfNecessary() {
        //ct: musi być tutaj Queue
        Set<String> allObjIds = new HashSet<String>();
        for (Map<String, String> line : content) {
            String objId = line.get(PumpConstants.GENERIC_NODE_OBJID);
            allObjIds.add(objId.toLowerCase());
        }
        parentObjIds.clear();
        for (Map<String, String> line : content) {
            String parentObjId = line.get(PumpConstants.GENERIC_NODE_PARENT_OBJID);
            if (allObjIds.contains(parentObjId.toLowerCase())) {
                parentObjIds.add(parentObjId);
            }
        }
        for (int i = 0; i < content.size(); i++) {
            Map<String, String> line = content.get(i);
            String objId = line.get(PumpConstants.GENERIC_NODE_OBJID);
            String parentObjId = line.get(PumpConstants.GENERIC_NODE_PARENT_OBJID);
//            if (!parentObjIds.contains(parentObjId)) {
//                throw new LameRuntimeException("Brak wpisu dla elementu nadrzędnego (gałęzi):" + parentObjId + " dla objId:" + objId);
//            }
            if (!BaseUtils.isStrEmptyOrWhiteSpace(parentObjId) && !parentObjIds.contains(parentObjId)) {
                if (!BaseUtils.isStrEmptyOrWhiteSpace(branchNodeKindCode)) {
                    generateDefaultParentNode(parentObjId);
                } else {
                    throw new LameRuntimeException("Brak wpisu dla elementu nadrzędnego(gałęzi):" + parentObjId + " dla objId:" + objId + " lub nie można wybrać NODE_KIND_CODE dla tego elementu."); //OLA
                }
            }
        }
    }

    private void addNodeKindIfNecessary() {
        for (Map<String, String> line : content) {
            String objId = line.get(PumpConstants.GENERIC_NODE_OBJID);
            if (BaseUtils.isStrEmptyOrWhiteSpace(line.get(PumpConstants.GENERIC_NODE_NODE_KIND_CODE))) {
                if (!parentObjIds.contains(objId)) {
                    if (BaseUtils.isStrEmptyOrWhiteSpace(leafNodeKindCode)) {
                        throw new LameRuntimeException("Brak zdefiniowanego NODE_KIND_CODE dla liści drzewa: \"" + metadata.treeCode + "\"");
                    } else {
                        line.put(PumpConstants.GENERIC_NODE_NODE_KIND_CODE, leafNodeKindCode);
                    }
                } else if (BaseUtils.isStrEmptyOrWhiteSpace(branchNodeKindCode)) {
                    throw new LameRuntimeException("Brak zdefiniowanego NODE_KIND_CODE dla gałęzi drzewa: \"" + metadata.treeCode + "\"");
                } else {
                    line.put(PumpConstants.GENERIC_NODE_NODE_KIND_CODE, branchNodeKindCode);
                }
            }
        }
    }

    private void checkNodeKindCompatibleness() {
        for (String nodeKindCode : nodeKindCodes) {
            Integer nodeKindCodeId = execNamedQuerySingleVal("getNodeKindCodeId4TreeCode", true, null, nodeKindCode, metadata.treeCode);
            if (nodeKindCodeId == null) {
                throw new LameRuntimeException("Brak odpowiedniego typu węzła \"" + nodeKindCode + "\"w drzewie " + metadata.treeCode);
            }
        }

        for (Map<String, String> line : content) {
            String nodeKindCode = line.get(PumpConstants.GENERIC_NODE_NODE_KIND_CODE);
            if (!BaseUtils.isMapEmpty(metadata.attributes)) {
                for (Entry<String, String> attr : metadata.attributes.entrySet()) {
                    String attrName = attr.getKey();
                    String attrValue = line.get(attrName);

                    Map<String, Object> m = adhocDao.execNamedQuerySingleRowCFN("getAtttributeInfo4NodeKind", true, null, nodeKindCode, attrName);
                    if (m != null) { //jest taki atrybut dla takiego node_kind
                        boolean isRequired = ((Integer) m.get("is_required")) == 0 ? false : true;
                        String defaultValue = (String) m.get("default_value");
                        if (BaseUtils.isStrEmptyOrWhiteSpace(attrValue) && isRequired) {
                            line.put(attrName, defaultValue);
                        }
//                        if (!BaseUtils.isStrEmptyOrWhiteSpace(attrValue)) {
//                            Integer attrId = execNamedQuerySingleVal("getAttrId4NodeKindByAttrName", true, null, nodeKindCode, attrName);
//                            if (attrId == null) {
//                                throw new LameRuntimeException("Atrybut \"" + attrName + "\" nie został przypisany do węzła typu \"" + nodeKindCode + "\" objId:" + line.get(PumpConstants.GENERIC_NODE_OBJID));
//                            }
//                        }
                    } else if (!BaseUtils.isStrEmptyOrWhiteSpace(attrValue)) {
                        throw new LameRuntimeException("Atrybut \"" + attrName + "\" nie został przypisany do węzła typu \"" + nodeKindCode + "\" objId:" + line.get(PumpConstants.GENERIC_NODE_OBJID));
                    }
                }
            }
        }
    }

    private void importTrees() {
        for (int i = 0; i < importConfigs.length; i++) {
            updateMetadata(importConfigs[i]);
            if (!BaseUtils.isStrEmptyOrWhiteSpace(metadata.baseFileName)) {
                pumpLogger.pumpLog("Importowanie do drzewa \"" + metadata.treeCode + "\"", null);

                checkTreeExistenceAndCompatibleAttributes();
                execNamedQuerySingleVal("recalculateObjId", true, null, metadata.treeCode);
                try {
                    List<Map<String, String>> treeContent = rebuildTreeContentAndReCheckCompatibleness();

                    CSVImporter csvImporter = new CSVImporter(adhocDao, metadata.treeCode, config.isIncremental > 0);
                    csvImporter.setPumpLogger(pumpLogger);
                    Pair<Integer, String> pumpResult = csvImporter.pumpTree(treeContent);
                    if (PumpConstants.LOG_LOAD_STATUS_DONE != pumpResult.v1) {
                        throw new LameRuntimeException(pumpResult.v2);
                    }
                    pumpLogger.pumpLog("Drzewo \"" + metadata.treeCode + "\" zostało pomyślnie zaimportowane", null);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    pumpLogger.pumpLog("Błąd: " + ex.getMessage(), null);
                    throw new LameRuntimeException(ex);
                }
            } else {
                if (!isEmbeded()) {
                    throw new LameRuntimeException("Brak pliku CSV ze strukturą drzewa"); //OLA
                }
            }
        }
    }

    private void importRelatedObjs() {
        allLinkedObjs.clear();
        allJoinedObjs.clear();
        allHyperLinkInObjs.clear();
        joinedAttrNames.clear();
        for (int i = 0; i < importConfigs.length; i++) {
            updateMetadata(importConfigs[i]);
            if (!BaseUtils.isCollectionEmpty(metadata.relatedFiles)) {
                for (TreeExportRelatedFileMetaData relatedFile : metadata.relatedFiles) {
                    String bindedTreeCode = relatedFile.relatedTreeCode;
                    execNamedQuerySingleVal("recalculateObjId", true, null, bindedTreeCode);
                    Pair<List<Map<String, String>>, List<String>> content = readCsvAsMap(relatedFile.relatedFileName);
                    Integer dstNameColId = checkAndGetDstNameColumnIndexInHeaderList(relatedFile.relatedFileName, content.v2);
                    Integer lastDstParentColId = convert2SrcAndDstObjIdsAndGetLastParentColumnIndex(content.v1, content.v2, dstNameColId);
                    Map<String, Integer> attrNames2IdMap = extractJoinedAttrNames(content.v2, lastDstParentColId);

                    collectLinkedObjs(metadata.treeCode, relatedFile.relatedTreeCode, content.v1);
                    collectJoinedObjs(metadata.treeCode, relatedFile.relatedTreeCode, content.v1, attrNames2IdMap);
                    collectHyperLinkInObjs(metadata.treeCode, relatedFile.relatedTreeCode, content.v1);
                    joinedAttrNames.putAll(attrNames2IdMap);
                }
            }
        }

        //TO-DO: powinnien robić w jednym zapytaniu SQL
        for (ImportedLinkedObj obj : allLinkedObjs) {
            adhocDao.execNamedCommand("addLinkedObj", obj.srcNodeId, obj.dstNodeId, obj.isDeleted);
        }
        
        for (ImportedJoinedObj obj : allJoinedObjs) {
            adhocDao.execNamedCommand("addOrDeleteJoinedObj", obj.srcNodeId, obj.dstNodeId, obj.isDeleted, obj.isInherit);
            if (obj.isDeleted == 0) {
                for (ImportedJoinedObjAttr joinedAttr : obj.attrs) {
                    adhocDao.execNamedCommand("addJoinedObjAttrLinked", obj.srcNodeId, obj.dstNodeId, joinedAttr.joinedAttrId, joinedAttr.joinedAttrId == obj.mainAttrId ? 1 : 0, joinedAttr.value, joinedAttr.valueTo);
                }
            }
        }

        for (ImportedHyperLinkInObj obj : allHyperLinkInObjs) {
            adhocDao.execNamedCommand("addOrDeleteHyperLinkInObj", obj.attrName, obj.srcNodeId, obj.atrLinValue, obj.isDeleted);
        }        
    }

    private void checkTreeAutoObjIds() {
        Set<String> allTreeCodes = new HashSet<String>();
        for (TreeExportMetadata config : importConfigs) {
            if (!BaseUtils.isStrEmptyOrWhiteSpace(config.treeCode)) {
                allTreeCodes.add(config.treeCode);
            }
            for (TreeExportRelatedFileMetaData related : config.relatedFiles) {
                if (!BaseUtils.isStrEmptyOrWhiteSpace(related.relatedTreeCode)) {
                    allTreeCodes.add(related.relatedTreeCode);
                }
            }
        }
        Set<String> notAutoObjTrees = BeanConnectionUtils.execNamedQuerySingleColAsSet(adhocDao, "checkAutoObj4Tree", null, allTreeCodes);
        for (String treeCode : notAutoObjTrees) {
            throw new LameRuntimeException("Brak własności auto_obj_id dla drzewa \"" + treeCode + "\"");
        }
    }

    private Integer checkAndGetDstNameColumnIndexInHeaderList(String fileName, List<String> headerList) {
        if (BaseUtils.isCollectionEmpty(headerList)) {
            throw new LameRuntimeException("Brak nagłówka w pliku \"" + fileName + "\""); //OLA
        }
        if (!PumpConstants.GENERIC_SRC_NAME.equalsIgnoreCase(headerList.get(0))) {
            throw new LameRuntimeException("Pierwsza kolumna musi być " + PumpConstants.GENERIC_SRC_NAME);
        }
        Integer dstNameColId = 0;
        for (int i = 1; i < headerList.size(); i++) {
            if (PumpConstants.GENERIC_DST_NAME.equalsIgnoreCase(headerList.get(i))) {
                dstNameColId = i;
                break;
            }
            if (!headerList.get(i).startsWith(PumpConstants.GENERIC_SRC_PARENT_OBJID)) {
                throw new LameRuntimeException("Niepoprawny format pliku \"" + fileName + "\": po " + PumpConstants.GENERIC_SRC_NAME + " muszą być kolumny " + PumpConstants.GENERIC_SRC_PARENT_OBJID);//OLA
            }
        }
        if (dstNameColId == 0) {
            throw new LameRuntimeException("Brak kolumny " + PumpConstants.GENERIC_DST_NAME);
        }
        return dstNameColId;
    }

    private Integer convert2SrcAndDstObjIdsAndGetLastParentColumnIndex(List<Map<String, String>> content, List<String> headerList, Integer dstNameColId) {
        int dstParentColCnt = 0;
        for (int i = dstNameColId + 1; i < headerList.size(); i++) {
            if (headerList.get(i).startsWith(PumpConstants.GENERIC_DST_PARENT_OBJID)) {
                dstParentColCnt++;
            } else {
                break;
            }
        }
        int lastDstParentColId = dstNameColId + dstParentColCnt;
        for (Map<String, String> line : content) {
            String srcObjId = "";
            for (int i = 1; i < dstNameColId; i++) {
                String oneNodeName = line.get(headerList.get(i));
                if (!BaseUtils.isStrEmptyOrWhiteSpace(oneNodeName)) {
                    srcObjId = BaseUtils.ensureObjIdEndWith(srcObjId + oneNodeName, "|");
                }
                line.remove(headerList.get(i));
            }
            srcObjId = BaseUtils.ensureObjIdEndWith(srcObjId + line.get(headerList.get(0)), "|");
            line.remove(headerList.get(0));

            String dstObjId = "";
            for (int i = dstNameColId + 1; i <= lastDstParentColId; i++) {
                String oneNodeName = line.get(headerList.get(i));
                if (!BaseUtils.isStrEmptyOrWhiteSpace(oneNodeName)) {
                    dstObjId = BaseUtils.ensureObjIdEndWith(dstObjId + oneNodeName, "|");
                }
                line.remove(headerList.get(i));
            }
            dstObjId = BaseUtils.ensureObjIdEndWith(dstObjId + line.get(headerList.get(dstNameColId)), "|");
            line.remove(headerList.get(dstNameColId));

            line.put(PumpConstants.GENERIC_SRC_OBJID, srcObjId);
            line.put(PumpConstants.GENERIC_DST_OBJID, dstObjId);
        }

        return lastDstParentColId;
    }

    private void collectHyperLinkInObjs(String treeCode, String bindedTreeCode, List<Map<String, String>> content) {
        //HyperLinkIn
        Comparator<ImportedConnectionObj> comp = new Comparator<ImportedConnectionObj>() {

            @Override
            public int compare(ImportedConnectionObj o1, ImportedConnectionObj o2) {
                if (o1.srcNodeId == o2.srcNodeId && o1.dstNodeId == o2.dstNodeId && ImportedHyperLinkInObj.class.cast(o1).attrName.equals(ImportedHyperLinkInObj.class.cast(o2).attrName)) {
                    return 1;
                } else {
                    return 0;
                }
            }
        };
        for (Map<String, String> row : content) {
            String isHyperLinkInObjStr = row.get(PumpConstants.GENERIC_TYPE);
            if (PumpConstants.HYPERLINK.equals(isHyperLinkInObjStr)) {
                String objId = row.get(PumpConstants.GENERIC_SRC_OBJID);
                String hyperLinkInObjId = row.get(PumpConstants.GENERIC_DST_OBJID);

                if (!BaseUtils.isStrEmptyOrWhiteSpace(hyperLinkInObjId)) {
                    ImportedHyperLinkInObj hyperLinkInObj = new ImportedHyperLinkInObj();
                    List<String> l = BaseUtils.splitBySep(hyperLinkInObjId, "|");

                    try {
                        hyperLinkInObj.srcNodeId = execNamedQuerySingleVal("getNodeIdByTreecodeAndObjId", false, null, treeCode, objId);
                    } catch (Exception ex) {
                        throw new LameRuntimeException("Link wewnętrzny: brak węzła " + objId + " w drzewie \"" + treeCode + "\" lub niespodziewany błąd.", ex);
                    }
                    try {
                        hyperLinkInObj.dstNodeId = execNamedQuerySingleVal("getNodeIdByTreecodeAndObjId", false, null, bindedTreeCode, hyperLinkInObjId);
                    } catch (Exception ex) {
                        throw new LameRuntimeException("Link wewnętrzny: brak węzła " + hyperLinkInObjId + " w drzewie \"" + bindedTreeCode + "\" lub niespodziewany błąd.", ex);
                    }

                    hyperLinkInObj.isDeleted = "1".equals(row.get(PumpConstants.GENERIC_IS_DELETED)) ? 1 : 0;
                    hyperLinkInObj.atrLinValue = hyperLinkInObj.dstNodeId + "_" + bindedTreeCode + "," + l.get(l.size() - 2);
                    hyperLinkInObj.attrName = row.get(PumpConstants.GENERIC_HYPER_LINK_IN_ATTR_NAME);
                    addOrUpdateConnectionObj(allHyperLinkInObjs, hyperLinkInObj, comp);
                } else {
                    throw new LameRuntimeException("Dowiązanie: brak węzła, do którego " + objId + " ma dowiązać"); //OLA
                }
            }
        }
    }

    private void collectLinkedObjs(String treeCode, String bindedTreeCode, List<Map<String, String>> content) {
        //Dowiazanie
        Comparator<ImportedConnectionObj> comp = new Comparator<ImportedConnectionObj>() {

            @Override
            public int compare(ImportedConnectionObj o1, ImportedConnectionObj o2) {
                if (o1.srcNodeId == o2.srcNodeId && o1.dstNodeId == o2.dstNodeId) {
                    return 1;
                } else {
                    return 0;
                }
            }
        };
        for (Map<String, String> row : content) {
            String isLinkedObjStr = row.get(PumpConstants.GENERIC_TYPE);
            if (PumpConstants.LINKING.equals(isLinkedObjStr)) {
                String objId = row.get(PumpConstants.GENERIC_SRC_OBJID);
                String linkedObjId = row.get(PumpConstants.GENERIC_DST_OBJID);

                if (!BaseUtils.isStrEmptyOrWhiteSpace(linkedObjId)) {
                    ImportedLinkedObj linkedObj = new ImportedLinkedObj();
                    try {
                        linkedObj.srcNodeId = execNamedQuerySingleVal("getNodeIdByTreecodeAndObjId", false, null, treeCode, objId);
                    } catch (Exception ex) {
                        throw new LameRuntimeException("Dowiązanie: brak węzła " + objId + " w drzewie \"" + treeCode + "\" lub niespodziewany błąd.", ex);
                    }
                    try {
                        linkedObj.dstNodeId = execNamedQuerySingleVal("getNodeIdByTreecodeAndObjId", false, null, bindedTreeCode, linkedObjId);
                    } catch (Exception ex) {
                        throw new LameRuntimeException("Dowiązanie: brak węzła " + linkedObjId + " w drzewie \"" + bindedTreeCode + "\" lub niespodziewany błąd.", ex);
                    }

                    linkedObj.isDeleted = "1".equals(row.get(PumpConstants.GENERIC_IS_DELETED)) ? 1 : 0;
                    addOrUpdateConnectionObj(allLinkedObjs, linkedObj, comp);
                } else {
                    throw new LameRuntimeException("Dowiązanie: brak węzła, do którego " + objId + " ma dowiązać"); //OLA
                }
            }
        }
    }

    private void collectJoinedObjs(String treeCode, String bindedTreeCode, List<Map<String, String>> content, Map<String, Integer> attrNames2IdMap) {
        //Powiazania
        Comparator<ImportedConnectionObj> comp = new Comparator<ImportedConnectionObj>() {

            @Override
            public int compare(ImportedConnectionObj o1, ImportedConnectionObj o2) {
                if ((o1.srcNodeId == o2.srcNodeId && o1.dstNodeId == o2.dstNodeId)
                        || (o1.srcNodeId == o2.dstNodeId && o1.dstNodeId == o2.srcNodeId)) {
                    return 1;
                } else {
                    return 0;
                }
            }
        };
        for (Map<String, String> row : content) {
            String isJoinedObjStr = row.get(PumpConstants.GENERIC_TYPE);
            if (PumpConstants.ASSOCIATION.equals(isJoinedObjStr)) {
                String objId = row.get(PumpConstants.GENERIC_SRC_OBJID);
                String joinedObjId = row.get(PumpConstants.GENERIC_DST_OBJID);
                if (!BaseUtils.isStrEmptyOrWhiteSpace(joinedObjId)) {
                    ImportedJoinedObj joinedObj = new ImportedJoinedObj();
                    try {
                        joinedObj.srcNodeId = execNamedQuerySingleVal("getNodeIdByTreecodeAndObjId", false, null, treeCode, objId);
                    } catch (Exception ex) {
                        throw new LameRuntimeException("Powiązanie: brak węzła " + objId + " w drzewie \"" + treeCode + "\" lub niespodziewany błąd.", ex);
                    }
                    try {
                        joinedObj.dstNodeId = execNamedQuerySingleVal("getNodeIdByTreecodeAndObjId", false, null, bindedTreeCode, joinedObjId);
                    } catch (Exception ex) {
                        throw new LameRuntimeException("Powiązanie: brak węzła " + joinedObjId + " w drzewie \"" + bindedTreeCode + "\" lub niespodziewany błąd.", ex);
                    }
                    joinedObj.isInherit = BaseUtils.tryParseInteger(row.get(PumpConstants.GENERIC_IS_INHERIT));
//                    adhocDao.execNamedCommand("addJoinedObj", treeCode, objId, bindedTreeCode, joinedObjId, isInherit);

                    String mainAttr = row.get(PumpConstants.GENERIC_MAIN_ATTRIBUTE);
                    joinedObj.mainAttrId = (!BaseUtils.isStrEmptyOrWhiteSpace(mainAttr) && mainAttr.contains(mainAttr)) ? attrNames2IdMap.get(mainAttr) : 0;

                    joinedObj.attrs = new ArrayList<ImportedJoinedObjAttr>();
                    for (Entry<String, Integer> e : attrNames2IdMap.entrySet()) {
                        ImportedJoinedObjAttr joinedAttr = new ImportedJoinedObjAttr();
                        joinedAttr.joinedAttrId = e.getValue();
                        String attrName = e.getKey();
//                        Integer attrId = e.getValue();
                        joinedAttr.value = BaseUtils.trimLeftAndRight(row.get(attrName + PumpConstants.GENERIC_ATTRIBUTE_SRC_SUFFIX));
                        joinedAttr.valueTo = BaseUtils.trimLeftAndRight(row.get(attrName + PumpConstants.GENERIC_ATTRIBUTE_DST_SUFFIX));
                        if (!BaseUtils.isStrEmptyOrWhiteSpace(joinedAttr.value) || !BaseUtils.isStrEmptyOrWhiteSpace(joinedAttr.valueTo)) {
//                            adhocDao.execNamedCommand("addJoinedObjAttrLinked", treeCode, objId, bindedTreeCode, joinedObjId, attrId, mainAttr.equalsIgnoreCase(attrName), value, valueTo);
                            joinedObj.attrs.add(joinedAttr);
                        }
                    }
                    joinedObj.isDeleted = "1".equals(row.get(PumpConstants.GENERIC_IS_DELETED)) ? 1 : 0;

                    addOrUpdateConnectionObj(allJoinedObjs, joinedObj, comp);
                } else {
                    throw new LameRuntimeException("Powiązanie: brak węzła, do którego " + objId + " ma powiązać");//OLA
                }
            }
        }
    }

    private <T extends ImportedConnectionObj> void addOrUpdateConnectionObj(List<T> connectedObjs, T newObj, Comparator<ImportedConnectionObj> comp) {
        int index = -1;

        for (int i = 0; i < connectedObjs.size(); i++) {
            T obj = connectedObjs.get(i);
            if (comp.compare(obj, newObj) == 1) {
                index = i;
                break;
            }
        }

        /*
         Tylko raz dodajemy
         */
        if (index == -1) {
            connectedObjs.add(newObj);
        } else {
            connectedObjs.set(index, newObj);
        }
    }

    private void updateMetadata(TreeExportMetadata importConfig) {
        metadata = importConfig;
        if (BaseUtils.isStrEmptyOrWhiteSpace(metadata.name)) {
            metadata.name = PumpConstants.GENERIC_NODE_NAME;
        }
    }
}
