/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.dqm;

import java.util.List;
import pl.bssg.metadatapump.common.ConnectionParametersDBServerBean;
import pl.fovis.common.ConnectionParametersProfileBean;
import pl.fovis.common.dqm.DQMLogBean;
import pl.fovis.common.dqm.DQMMultiSourceBean;
import pl.fovis.common.dqm.DataQualityRequestBean;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.IContinuation;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public interface IBIKSDQMSubservice {

    public boolean useFunctionInsteadSQLQuery();

    public DQMTestBean getDQMConfigForTest(int testId);

    public MssqlConnectionConfig getBIKSDBParameters();

    public ConnectionParametersDBServerBean getDBParametersForId(Integer serverId, Integer connectionId);

    public ConnectionParametersProfileBean getProfileConnectionParameters();

    public Pair<Integer, List<DQMLogBean>> getDQMLogs(int pageNum, int pageSize);

    public long insertDQMLog(int testId, Integer multiSourceId);

    public void updateDQMLog(long logId, String descr, boolean isEnd);

    public long insertDQMRequest(DataQualityRequestBean bean);

    public void updateDQMRequest(DataQualityRequestBean bean);

    public void deleteDQMRequest(long requestId);

    public void finishWorkUnit(boolean success);

    public void execInAutoCommitMode(IContinuation whatToExec);

    public void insertErrorResults(List<DQMErrorBean> list);

    public void deleteOldErrorData(int testId);

    public List<DataQualityRequestBean> getRequestsStartDateForTest(int testId);

    public List<DQMMultiSourceBean> getDQMTestMultiSources(int nodeId);

    public DQMMultiSourceBean getDQMTestMultiSource(int multiSourceId);

    public String getSensitiveAppPropValue(String propName);

//    public void execSqlOnBiksDatabaseInAutoCommitMode(String sqlTxt);
    public IBeanConnectionEx<Object> getConnection();

    public boolean checkMultiSourceIsSuccessForTest(int testId);

    public void dropTempTables(int testId);

    public void setScheduleStatusFlag(int testId, Integer multiSourceId);

    public void fixOneTimeSchedules(int testId);

    public void fixOneTimeOneMultiSourceSchedule(int testId, Integer multiSourceId);

    public void updateRequestDataReadSecs(long logId, int dataReadSecs);

    public Pair<String, String> saveErrorDataOnDisk(String tmpTableName);

    public int countRecordNumber(String tmpTableName);

    public void dropTmpTable(String tmpTableName);

}
