/*
 * LameBagTest.java
 * JUnit based test
 *
 * Created on 9 wrzesie� 2007, 12:28
 */

package commonlib;

import simplelib.LameBag;
import junit.framework.*;

/**
 *
 * @author wezyr
 */
public class LameBagTest extends TestCase {
    
    public LameBagTest(String testName) {
        super(testName);
    }
    
    public void test1() {
        LameBag<String> bag = new LameBag();
        bag.add("ala");
        bag.add("kot");
        bag.add("kot");
        bag.add("ala");
        bag.add("pies");
        assertEquals(5, bag.size());
        assertEquals(2, bag.getCount("ala"));
        assertEquals(1, bag.getCount("pies"));
        assertEquals(3, bag.uniqueSet().size());
        assertTrue(bag.contains("ala"));
        assertTrue(bag.contains("pies"));
        assertFalse(bag.contains("szczur"));
        
        bag.remove("ala");
        assertTrue(bag.contains("ala"));
        bag.remove("ala");
        assertFalse(bag.contains("ala"));
        bag.remove("pies");
        assertFalse(bag.contains("pies"));
        bag.add("ala");
        assertTrue(bag.contains("ala"));
        assertEquals(1, bag.getCount("ala"));
        assertEquals(0, bag.getCount("pies"));
        assertEquals(2, bag.getCount("kot"));
        assertEquals(3, bag.size());
        assertEquals(2, bag.uniqueSet().size());
    }
    
//    public void test4smok() {
//        BigDecimal big = new BigDecimal("0.00033");
//        System.out.println(String.format( "%.2f", big));
//    }
}
