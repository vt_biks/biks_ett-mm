/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions.dqm;

import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.dqm.AddOrEditDQMTestDialogForConnections;
import pl.fovis.client.dialogs.dqm.AddOrEditDQMTestDialogForDatabases;
import pl.fovis.common.ConnectionParametersDQMConnectionsBean;
import pl.fovis.common.dqm.DataQualityTestBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class NodeActionDQMEditTest extends NodeActionDQMBase {

    @Override
    public boolean isEnabledByDesign() {
        return node != null && isInDQMAdminMode() && isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_DQM_TEST_FAILED, BIKGWTConstants.NODE_KIND_DQM_TEST_INACTIVE, BIKGWTConstants.NODE_KIND_DQM_TEST_SUCCESS, BIKGWTConstants.NODE_KIND_DQM_TEST_NOT_STARTED);
    }

    @Override
    public String getCaption() {
        return I18n.edytujTest.get();
    }

    @Override
    public void execute() {
        BIKClientSingletons.getDQMService().getDQMTestsNameAndOptConnections(new StandardAsyncCallback<Pair<Set<String>, Map<Integer, ConnectionParametersDQMConnectionsBean>>>("Error in getDQMTestsNameAndOptConnections") {

            @Override
            public void onSuccess(Pair<Set<String>, Map<Integer, ConnectionParametersDQMConnectionsBean>> result) {

                IParametrizedContinuation<DataQualityTestBean> continuation = new IParametrizedContinuation<DataQualityTestBean>() {

                    @Override
                    public void doIt(DataQualityTestBean param) {
                        final int nodeId = node.linkedNodeId != null ? node.linkedNodeId : node.id;
                        param.testNodeId = nodeId;
                        BIKClientSingletons.getDQMService().editDQMTest(param, new StandardAsyncCallback<Void>("Error in editDQMTest") {

                            @Override
                            public void onSuccess(Void result) {
                                BIKClientSingletons.showInfo(I18n.zmienionoTest.get());
                                refreshRedisplayDataAndSelectRecord(nodeId);
                            }
                        });
                    }
                };

                if (BIKClientSingletons.useDqmConnectionMode()) {
                    new AddOrEditDQMTestDialogForConnections().buildAndShowDialog(node.id, data.dataQualityTestExtradata, result, continuation);
                } else {
                    new AddOrEditDQMTestDialogForDatabases().buildAndShowDialog(node.id, data.dataQualityTestExtradata, result, continuation);
                }

//                        new IParametrizedContinuation<DataQualityTestBean>() {
//                    @Override
//                    public void doIt(DataQualityTestBean param) {
//                        final int nodeId = node.linkedNodeId != null ? node.linkedNodeId : node.id;
//                        param.testNodeId = nodeId;
//                        BIKClientSingletons.getDQMService().editDQMTest(param, new StandardAsyncCallback<Void>("Error in editDQMTest") {
//
//                            @Override
//                            public void onSuccess(Void result) {
//                                BIKClientSingletons.showInfo(I18n.zmienionoTest.get());
//                                refreshRedisplayDataAndSelectRecord(nodeId);
//                            }
//                        });
//                    }
//                });
            }
        });
    }
}
