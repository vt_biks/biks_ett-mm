﻿exec sp_check_version '1.0.88.1';
go

create procedure [dbo].[sp_prepare_bik_joined_objs_tmp]
as
begin
	if exists(select 1 from sysobjects where id = object_id('tmp_bik_joined_objs'))
	drop table tmp_bik_joined_objs

	CREATE TABLE tmp_bik_joined_objs(
	[src_node_id] [int]  NULL,
	[dst_node_id] [int]  NULL,
	[type] [int]  NULL,
	[inherit_to_descendants] [int]  not null default 0
	)
end
go

create procedure [dbo].[sp_move_bik_joined_objs_tmp]
as
begin
	if exists(select 1 from sysobjects where id = object_id('tmp_unique_bik_joined_objs'))
	drop table tmp_unique_bik_joined_objs
	
	CREATE TABLE [dbo].[tmp_unique_bik_joined_objs](
	[src_node_id] [int]  NULL,
	[dst_node_id] [int]  NULL,
	[type] [int]  NULL,
	[inherit_to_descendants] [int] not null default 0
	)

	insert into tmp_unique_bik_joined_objs(src_node_id, dst_node_id, type, inherit_to_descendants)
	select distinct *
	from tmp_bik_joined_objs
	
	insert into bik_joined_objs(src_node_id, dst_node_id, type, inherit_to_descendants)
	select u.src_node_id, u.dst_node_id, u.type,u.inherit_to_descendants from tmp_unique_bik_joined_objs u left join bik_joined_objs bjo on bjo.src_node_id=u.src_node_id
    and bjo.dst_node_id=u.dst_node_id
    where bjo.id is null
    
    update bik_joined_objs
    set type=1
    from tmp_unique_bik_joined_objs u
    where     
    bik_joined_objs.src_node_id=u.src_node_id 
    and bik_joined_objs.dst_node_id=u.dst_node_id 
    and bik_joined_objs.type = 0
	
	if exists(select 1 from sysobjects where id = object_id('tmp_bik_joined_objs'))
	drop table tmp_bik_joined_objs
	
	if exists(select 1 from sysobjects where id = object_id('tmp_unique_bik_joined_objs'))
	drop table tmp_unique_bik_joined_objs
end
go



exec sp_update_version '1.0.88.1', '1.0.88.2';
go
