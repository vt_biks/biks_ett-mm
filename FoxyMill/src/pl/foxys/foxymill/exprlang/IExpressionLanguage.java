/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill.exprlang;

import simplelib.TextFilePosition;

/**
 *
 * @author wezyr
 */
public interface IExpressionLanguage {

    public IParsedExpression parseExpression(String exprStr, String name, TextFilePosition filePos);
}
