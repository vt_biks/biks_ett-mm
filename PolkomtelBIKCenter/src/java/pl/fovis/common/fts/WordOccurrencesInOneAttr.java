/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.fts;

import java.io.Serializable;

/**
 *
 * @author pmielanczuk
 */
public class WordOccurrencesInOneAttr implements Serializable {

    public String word;

    //FTSWordOrigin: Original(1), Synonym(0.9), DictStemmed(0.8), AlgoStemmed(0.7), AccentFree
//    public int originalCnt;
//    public int synonymCnt;
//    public int dictStemmedCnt;
//    public int algoStemmedCnt;
//    public int accentFreeCnt;
    //---
    public int attrId;

    public WordOccurrencesInOneAttr() {
        // aby się dało serializować
    }

    public WordOccurrencesInOneAttr(String word, int attrId) {
        this.word = word;
        this.attrId = attrId;
    }

//    public void addOrigin(FTSWordOrigin origin) {
//        switch (origin) {
//            case Original:
//                originalCnt++;
//                break;
//            case Synonym:
//                synonymCnt++;
//                break;
//            case DictStemmed:
//                dictStemmedCnt++;
//                break;
//            case AlgoStemmed:
//                algoStemmedCnt++;
//                break;
//            case AccentFree:
//                accentFreeCnt++;
//                break;
//            default:
//                throw new LameRuntimeException("unsupported origin: " + origin);
//        }
//    }
//    @Override
//    public String toString() {
//        return "WordOccurrencesInOneAttr{" + "word=" + word + ", originalCnt=" + originalCnt + ", synonymCnt=" + synonymCnt + ", dictStemmedCnt=" + dictStemmedCnt + ", algoStemmedCnt=" + algoStemmedCnt + ", accentFreeCnt=" + accentFreeCnt + ", attrId=" + attrId + '}';
//    }
    @Override
    public String toString() {
        return "WordOccurrencesInOneAttr{" + "word=" + word + ", attrId=" + attrId + '}';
    }
}
