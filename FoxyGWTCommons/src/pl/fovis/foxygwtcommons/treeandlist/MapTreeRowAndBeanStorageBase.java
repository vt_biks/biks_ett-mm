/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.treeandlist;

import java.util.Map;

/**
 *
 * @author wezyr
 */
public abstract class MapTreeRowAndBeanStorageBase<IDT, BEAN> extends TreeRowAndBeanStorageBase<IDT, Map<String, Object>, BEAN> {

    public static final String ID_FIELD_NAME = "id" /* I18N: no */;
    public static final String NAME_FIELD_NAME = "name" /* I18N: no */;
    public static final String PRETTY_KIND_FIELD_NAME = "nodeKindCaption";
    public static final String ICON_FIELD_NAME = "icon" /* I18N: no */;
    public static final String PARENT_ID_FIELD_NAME = "parentNodeId";
    public static final String VIEW_NODE_FIELD_NAME = "view" /* I18N: no */;
    public static final String FOLLOW_FIELD_NAME = "follow" /* I18N: no */;
    public static final String EMPTY_COLUMN_FIELD_NAME = "empty" /* I18N: no */;
    public static final String ADDITIONAL_ICON_FIELD_NAME = "additionalIcon";
    public static final String CHECKBOX = "checkbox";
    public static final String RADIOBUTTONO = "radioButtono";
    public static final String RADIOBUTTONW = "radioButtonw";
    protected String idFieldName;
    protected String parentIdFieldName;
    protected ITreeGridCellWidgetBuilder<BEAN> optCellWidgetBuilder;

    public MapTreeRowAndBeanStorageBase(String idFieldName, String parentIdFieldName,
            //            boolean isWidgetRunFromDialog,
            ITreeGridCellWidgetBuilder<BEAN> optCellWidgetBuilder) {
        this.idFieldName = idFieldName;
        this.parentIdFieldName = parentIdFieldName;
        this.optCellWidgetBuilder = optCellWidgetBuilder;
    }

    public MapTreeRowAndBeanStorageBase(String idFieldName, String parentIdFieldName) {
        this(idFieldName, parentIdFieldName, null);
    }

    @Override
    @SuppressWarnings("unchecked")
    public IDT getNodeId(Map<String, Object> n) {
        return (IDT) n.get(idFieldName);
    }

    @Override
    @SuppressWarnings("unchecked")
    public IDT getParentId(Map<String, Object> n) {
        return (IDT) n.get(parentIdFieldName);
    }

    protected void putColValueWithConvertion(Map<String, Object> row, BEAN node, String colName, Object defVal) {
        if (optCellWidgetBuilder != null) {
            Object widgetVal = optCellWidgetBuilder.getOptWidget(node, colName);
            if (widgetVal != null) {
                defVal = widgetVal;
            }
        }
        row.put(colName, defVal);
    }
}
