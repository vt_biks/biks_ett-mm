/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.biks.adm.server;

/**
 *
 * @author pmielanczuk
 */
public interface IAppAndDBVerInfoServiceForUpgrader {

    int execNamedCommand(String name, Object... args);

    void updateProcessFinalDbVer(int procId, String dbVer);

    String getDbVer();
}
