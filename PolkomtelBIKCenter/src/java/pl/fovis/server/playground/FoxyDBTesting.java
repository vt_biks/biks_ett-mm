/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.playground;

import java.util.List;
import java.util.Map;
import pl.fovis.common.TreeNodeBean;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.commons.IRawConnection;
import pl.trzy0.foxy.serverlogic.db.MssqlConnection;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuationWithReturn;
import pl.fovis.common.i18npoc.I18n;
import simplelib.FieldNameConversion;

/**
 *
 * @author pmielanczuk
 */
// i18n-default: no
public class FoxyDBTesting {

    public static void runTesting(String testName, int repeatCnt, IParametrizedContinuationWithReturn<IBeanConnectionEx<Object>, Integer> testCont) {
        MssqlConnectionConfig cfg = new MssqlConnectionConfig(
                "localhost" /* I18N:  */, "SQLEXPRESS" /* I18N:  */, "biks_polkomtel_2012_10_22", "sa" /* I18N:  */, "MSsql123");

        IBeanConnectionEx<Object> conn = new MssqlConnection(cfg);

        System.out.println("opening connection - warm up dummy queries" /* I18N:  */);

        // otworzenie połączenia
        conn.execQry("select" /* I18N:  */+ " 1");
        conn.execQry("select" /* I18N:  */+ " 2");
        conn.execQry("select" /* I18N:  */+ " 3");

//        System.out.println("before runGc - first time");
//        LameUtils.runGc(5, 1000);

        testCont.doIt(conn);

//        System.out.println("before runGc - second time");
//        LameUtils.runGc(5, 1000);

//        System.out.println("sleep for 2 seconds");
//        LameUtils.threadSleep(2000);

        System.out.println("run test" /* I18N:  */+ "!");

        long totalRowCnt = 0;
        long startMillis = System.currentTimeMillis();
        for (int i = 0; i < repeatCnt; i++) {
            totalRowCnt += testCont.doIt(conn);
//        System.out.println("node cnt: " + nodes.size());
        }
        long endMillis = System.currentTimeMillis();
        long millisTaken = endMillis - startMillis;
        System.out.println("Executing" /* I18N:  */+ " \"" + testName + "\" " +"" +"" +"for" /* I18N:  */+ " " + repeatCnt
                + " " +"" +"" +"time(s) took" /* I18N:  */+ ": " + millisTaken + " " +"" +"" +"milli(s" /* I18N:  */+ ")");
        System.out.println("Average time for single row" /* I18N:  */+ ": "
                + BaseUtils.doubleToString(1.0 * millisTaken / totalRowCnt, 4, true, true) + " " +"" +"" +"milli(s" /* I18N:  */+ ")");
        System.out.println("Average rows per second" /* I18N:  */+ ": "
                + BaseUtils.doubleToString(totalRowCnt * 1000.0 / millisTaken, 2, true, true) + " " +"" +"" +"rps" /* I18N:  */);
    }

    public static void main(String[] args) {

        //---------------------------
//        if (true) {
//            return;
//        }

//        LameLoggerFactory.setOneConfigEntry(ConnectionPoolManagerPlain.class, ILameLogger.LameLogLevel.Info);

        int rowCnt = 10000;
        int repeatCnt = 20;

        final String sqlTxt = "select top" /* I18N:  */+ " " + rowCnt + " " +"" +"" +"bn.*,\n"
                + "    bnk.caption as node_kind_caption,\n"
                + "    bnk.code as node_kind_code,\n"
                + "    bnk.is_folder as is_folder,\n"
                + "    bt.name as tree_name, bt.tree_kind, bt.code as tree_code\n"
                + "    from\n"
                + "    bik_node\n"
                + "    bn inner join bik_node_kind bnk on bn.node_kind_id = bnk.id\n"
                + "    inner join bik_tree bt on bn.tree_id = bt.id" /* I18N:  */;

        runTesting("warm up: reading" /* I18N:  */+ " " + rowCnt + " " +"" +"" +"rows for TNB (toLowerCase FNC" /* I18N:  */+ ")", 2, new IParametrizedContinuationWithReturn<IBeanConnectionEx<Object>, Integer>() {
            public Integer doIt(IBeanConnectionEx<Object> param) {
                List<Map<String, Object>> rows = param.execQry(sqlTxt, FieldNameConversion.ToLowerCase);
                return rows.size();
            }
        });
        
        runTesting("reading" /* I18N:  */+ " " + rowCnt + " " +"" +"" +"rows for TNB (toLowerCase FNC" /* I18N:  */+ ")", repeatCnt, new IParametrizedContinuationWithReturn<IBeanConnectionEx<Object>, Integer>() {
            public Integer doIt(IBeanConnectionEx<Object> param) {
                List<Map<String, Object>> rows = param.execQry(sqlTxt, FieldNameConversion.ToLowerCase);
                return rows.size();
            }
        });

        runTesting("reading" /* I18N:  */+ " " + rowCnt + " " +"" +"" +"rows for TNB (null FNC" /* I18N:  */+ ")", repeatCnt, new IParametrizedContinuationWithReturn<IBeanConnectionEx<Object>, Integer>() {
            public Integer doIt(IBeanConnectionEx<Object> param) {
                List<Map<String, Object>> rows = param.execQry(sqlTxt, null);
                return rows.size();
            }
        });

        runTesting("reading" /* I18N:  */+ " " + rowCnt + " TreeNodeBeans", repeatCnt, new IParametrizedContinuationWithReturn<IBeanConnectionEx<Object>, Integer>() {
            public Integer doIt(IBeanConnectionEx<Object> conn) {
                List<TreeNodeBean> nodes = conn.createBeansFromQry(sqlTxt, TreeNodeBean.class);
                return nodes.size();
            }
        });
    }
}
