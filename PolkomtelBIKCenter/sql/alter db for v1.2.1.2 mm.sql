﻿exec sp_check_version '1.2.1.2';
go

alter table bik_user_in_node
add date_added datetime null default null
go

alter table bik_user_in_node
add is_deleted int not null default 0
go

exec sp_update_version '1.2.1.2', '1.2.1.3';
go
