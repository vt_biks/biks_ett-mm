/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.bikwidgets.JoinTypeRolesWidget;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.BikNodeTreeOptMode;
import pl.fovis.common.JoinTypeRoles;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.UseInNodeSelectionBean;
import pl.fovis.common.UserBean;
import pl.fovis.common.UserInNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author AMamcarz
 * z poziomu specjalisty
 */
public class TreeAndDictionarySelectorForUsers extends TreeSelectorForUsers {

//    protected CategorizedRoleForNodeDictWidgetForUsers roles;
    protected IParametrizedContinuation<UseInNodeSelectionBean> saveContForUser;
    protected List<UserInNodeBean> usersInNodeBean;
    protected JoinTypeRolesWidget joinTypeRoleWidget;
    protected Map<Integer, Integer> descendantsCntInRoles;
    protected PushButton addUserToRoleBtn;

    @Override
    protected Widget buildOptionalRightSide() {
        // allHp = new HorizontalPanel();
        VerticalPanel rightVp = new VerticalPanel();

        rightVp.setWidth("350px"); //300
        rightVp.setStyleName("EntityDetailsPane selectRoleInDialogPane");
        main.setWidth("900px");

//        roles = new CategorizedRoleForNodeDictWidgetForUsers(usersInNodeBean);
//        roles.setOnSelectionChanged(new IContinuation() {
//
//            public void doIt() {
//                configureButtons();
//                refreshRolePanel();
//            }
//        });
//        ScrollPanel scroll = new ScrollPanel();
//        scroll.add(roles.buildWidgets());
//
//        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(scroll,
//                BIKClientSingletons.ORIGINAL_SELECT_ROLE_IN_DIALOG_SCROLL_HEIGHT);
//
//        rightVp.add(scroll);
//        roles.displayData();
        ScrollPanel scroll = new ScrollPanel();
        joinTypeRoleWidget = new JoinTypeRolesWidget(usersInNodeBean, nodeId, descendantsCntInRoles);
        scroll.add(joinTypeRoleWidget.buildJointTypeRoleWidget(0/*roles.markedNode()*/, isInheritToDescendantsChecked()));
        scroll.setHeight("100%");
        rightVp.add(scroll);

        inheritToDescendantsCb = new CheckBox(I18n.pokazujWObiektachPodrzednych.get() /* I18N:  */);
        inheritToDescendantsCb.setStyleName("Filters-Checkbox");
        inheritToDescendantsCb.addStyleName("Roles-Checkbox");
        inheritToDescendantsCb.setValue(true);
//        rightVp.add(inheritToDescendantsCb);
//        inheritToDescendantsCb.setHeight("28px");
        inheritToDescendantsCb.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                refreshRolePanel();
            }
        });
        rightVp.add(inheritToDescendantsCb);

        return rightVp;
    }

//    protected boolean isMarkedCheckBox() {
//        return (roles.markedNode() != 0);
//    }
    protected void refreshRolePanel() {
        if (joinTypeRoleWidget != null) {
            joinTypeRoleWidget.buildRolePanelWithData(getRoleForNodeId()/*roles.markedNode()*/, isInheritToDescendantsChecked());
        }
    }

    protected void setVisibleJoinTypeRoleWidget(boolean isEnabled) {
        if (joinTypeRoleWidget != null) {
            joinTypeRoleWidget.setVisibleMainFP(isEnabled);
        }
    }

    @Override
    protected boolean isActionButtonEnabled() {
        return selectedNode != null && BaseUtils.safeEquals(selectedNode.nodeKindCode, BIKGWTConstants.NODE_KIND_USER);
        //ww:mltx był tutaj taki kod - bez sensu chyba te lastSelectedNode
//        if (selectedNode == null || lastSelectedNode == null) {
//            return false;
//        }
//        boolean isUserNodeKind = BaseUtils.safeEquals(selectedNode.nodeKindCode, BIKGWTConstants.NODE_KIND_USER);
//        setVisibleJoinTypeRoleWidget(isUserNodeKind);
//        int lastParentId = lastSelectedNode.parentNodeId == null ? -1 : lastSelectedNode.parentNodeId;
//        int selectedParentId = selectedNode.parentNodeId == null ? -1 : selectedNode.parentNodeId;
//        return (super.isActionButtonEnabled() && isUserNodeKind && (lastParentId != selectedParentId));
    }

    @Override
    protected void doAction() {
        //no-op
    }

    @Override
    protected boolean doActionBeforeClose() {
        //ww->prim2: mam wątpliwość czy tu należy tak po prostu wołać super
        // tamto zwraca true/false i chyba trzeba coś z tym wynikiem zrobić?
        if (!super.doActionBeforeClose()) {
            return false;
        }

        JoinTypeRoles typeSelectedRb = null;
        if (joinTypeRoleWidget != null) {
            if (!joinTypeRoleWidget.isJoinTypeRoleRbSelected()) {
                return false;
            }
            typeSelectedRb = joinTypeRoleWidget.getJoinTypeRoleSelectedRb();
        } else {
            typeSelectedRb = JoinTypeRoles.Auxiliary;
        }
        final UseInNodeSelectionBean uinb = new UseInNodeSelectionBean();
        uinb.userId = BaseUtils.nullToDef(selectedNode.linkedNodeId, selectedNode.id);
        int roleForNodeId = getRoleForNodeId();
        uinb.roleForNodeId = roleForNodeId;//roles.markedNode();
        uinb.inheritToDescendants = BaseUtils.booleanToInt(isInheritToDescendantsChecked());
//        uinb.isAuxiliary = (typeSelectedRb == JoinTypeRoles.Auxiliary);
//        uinb.joinTypeRole = typeSelectedRb;
        if (joinTypeRoleWidget != null) {
            uinb.joinTypeRole = (JoinTypeRoles.PrimaryOldNotReplace == typeSelectedRb && joinTypeRoleWidget.primaryReplace.getValue()) ? JoinTypeRoles.SelectedPrimaryOldReplace : typeSelectedRb;
        } else {
            uinb.joinTypeRole = typeSelectedRb;
        }

//        int roleId = roles.markedNode();joinTypeRoleWidget.primaryReplace.getValue()
        //ww->prim2: co znaczy wartość < 0 w descendantsCntInRole pod jakimś kluczem?
        // trochę dziwne te warunki, za bardzo zamotane? a może nie...?
        Integer descendantsCntInRole = descendantsCntInRoles.get(roleForNodeId);
        int cnt = descendantsCntInRole != null ? descendantsCntInRole : 0;

        if (isInheritToDescendantsChecked() && (cnt > 0)) {
            String msg = null;
            if (typeSelectedRb == JoinTypeRoles.PrimaryOldDelete) {
                msg = I18n.usunieszRoleDla.get() /* I18N:  */ + " " + cnt + " " + I18n.obiektowPodrzednych.get() /* I18N:  */ + ".";
            } //ww->prim2: te nawiasy w typeSelectedRb == (JoinTypeRoles.PrimaryOldNotReplace)
            // są zbędne, zaciemniają...
            // po drugie - te spacje mi tu nie pasują (znów) czasem wielokrotne na końcu
            // po co?
            else if (typeSelectedRb == JoinTypeRoles.PrimaryOldReplace) {
                msg = I18n.zamienisRoleGlowneNaPomocnicDla.get() /* I18N:  */ + " " + cnt + " " + I18n.obiektowPodrzednych.get() /* I18N:  */ + ".";
            } else if (typeSelectedRb == JoinTypeRoles.PrimaryOldNotReplace) {
                msg = I18n.pozostawiszRoleGlowneDla.get() /* I18N:  */ + " " + cnt + " " + I18n.obiektowPodrzednych.get() /* I18N:  */ + ".";
            } else if (typeSelectedRb == JoinTypeRoles.Primary) {
                msg = I18n.usunieszRoleDla.get() /* I18N:  */ + " " + cnt + " " + I18n.obiektowPodrzednych.get() /* I18N:  */ + ".";
            } else {
                hideDialogEx(uinb);
            }
            if (msg != null) {
                new ConfirmDialog().buildAndShowDialog(msg, new IContinuation() {
                    public void doIt() {
                        hideDialogEx(uinb);
                    }
                });
            }
        } else {
            hideDialogEx(uinb);
        }
        return false;
    }

    protected void hideDialogEx(UseInNodeSelectionBean uinb) {
        hideDialog();
        saveContForUser.doIt(uinb);
    }

    @Override
    protected void doCancel() {
        BIKClientSingletons.getService().deleteBadLinkedUsersUnderRole(new StandardAsyncCallback<Boolean>() {
            public void onSuccess(Boolean result) {
                BIKClientSingletons.refreshAndRedisplayDataUsersTree(result);
            }
        });
    }

    public void buildAndShowDialog(int thisNodeId, //String treeKind,
            List<UserInNodeBean> usersInNodeBean,
            Map<Integer, Integer> descendantsCntInRole,
            IParametrizedContinuation<UseInNodeSelectionBean> saveCont) {
//        this.thisNodeId = thisNodeId;
//        this.treeKind = treeKind;
        this.usersInNodeBean = usersInNodeBean;
        this.saveContForUser = saveCont;
        this.descendantsCntInRoles = descendantsCntInRole;
        super.buildAndShowDialog(thisNodeId, //null,
                null, null, true, null);
    }

    @Override
    protected void callServiceToGetData(AsyncCallback<List<TreeNodeBean>> callback) {
//        if (useLazyLoading) {
        BIKClientSingletons.getService().getBikUserRolesTreeNodes(optUpSelectedNodeId, null, useLazyLoading, callback);
//        } else {
//            BIKClientSingletons.getService().getBikUserTreeNodes(optUpSelectedNodeId, callback);
//        }
    }

    @Override
    protected void addAdditionalButtons(HorizontalPanel hp) {
        addUserToRoleBtn = new PushButton(I18n.przypiszUzytkownikaDoRoli.get() /* I18N:  */);
        NewLookUtils.makeCustomPushButton(addUserToRoleBtn);
        addUserToRoleBtn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {

                BIKClientSingletons.getService().getBikAllUsers(
                        new StandardAsyncCallback<List<UserBean>>("Failure on" /* I18N: no */ + " getBikAllUsers") {
                            public void onSuccess(List<UserBean> result) {
                                new PickUserDialog().pickOneItem(result, selectedNode.id, new IParametrizedContinuation<UserBean>() {
                                    public void doIt(UserBean param) {
                                        getService().addLinkUser(param.nodeId, param.name, selectedNode.id, selectedNode.treeId, new StandardAsyncCallback< Pair<Integer, Boolean>>() {
                                            public void onSuccess(Pair<Integer, Boolean> result) {
                                                BIKClientSingletons.showInfo(result.v2 ? I18n.przypisanoUzytkownikaDoRoli.get() /* I18N:  */ : I18n.uzytkownJestJuzPrzypisaDoTejRoli.get() /* I18N:  */);
                                                fetchData(result.v1);
                                                BIKClientSingletons.refreshAndRedisplayDataUsersTree(true);
                                            }
                                        });

                                    }
                                }, false);
                            }
                        });
            }
        });
        hp.add(addUserToRoleBtn);

        addUserToRoleBtn.setVisible(/*BIKClientSingletons.isEffectiveExpertLoggedIn*/
                BIKClientSingletons.isEffectiveExpertLoggedInEx(BIKConstants.ACTION_LINK_USER_TO_ROLE));
    }

    @Override
    protected void configureButtons() {
//        actionBtn.setEnabled(isActionButtonEnabled());
        //ww:mltx poniższe robi dokładnie to, co zakomentowana linia powyżej...
        super.configureButtons();
        addUserToRoleBtn.setEnabled(BIKClientSingletons.isEffectiveExpertLoggedInEx(BIKConstants.ACTION_LINK_USER_TO_ROLE)
                //                BIKClientSingletons.isEffectiveExpertLoggedIn()
                && selectedNode != null && BaseUtils.safeEquals(selectedNode.nodeKindCode, BIKGWTConstants.NODE_KIND_USERS_ROLE));

        final boolean actionButtonEnabled = isActionButtonEnabled();
        setVisibleJoinTypeRoleWidget(actionButtonEnabled);
        if (actionButtonEnabled) {
            refreshRolePanel();
        }
    }

    protected int getRoleForNodeId() {
        TreeNodeBean tb = bikTree.getTreeStorage().getBeanById(selectedNode.parentNodeId);
        return BaseUtils.tryParseInt(tb.objId);
    }

    @Override
    protected BikNodeTreeOptMode getTreeOptExtraMode() {
//        return BikNodeTreeOptMode.OnlyLinkedNodes; -- sprawdzić 
        return null;
    }
}
