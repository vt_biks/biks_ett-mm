/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import pl.fovis.common.BikNodeTreeOptMode;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author bfechner
 */
public class TreeSelectorBIAInstanceFoldersDialog extends TreeSelectorBIAInstanceDialog {

    @Override
    protected BikNodeTreeOptMode getTreeOptExtraMode() {
        return BikNodeTreeOptMode.SAPBOReportFoldersOnly;
    }

    @Override
    protected String getDialogCaption() {
        return I18n.scheduleSelectFolders.get();
    }
}
