/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package simplelib;

/**
 *
 * @author wezyr
 */
public interface INamedPropsRenameStrategy {
    public Iterable<String> getSrcPropNames(INamedPropsBean src);

    public String getDestPropName(INamedPropsBean src, INamedPropsBeanWriter dst, String srcPropName);
}
