/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill.exprlang.mvel;

import org.mvel2.optimizers.OptimizerFactory;
import pl.foxys.foxymill.exprlang.ExpressionLanguageBase;
import pl.foxys.foxymill.exprlang.ParsedExpressionBase;

/**
 *
 * @author wezyr
 */
public class MvelExpressionLanguage extends ExpressionLanguageBase {

    private static boolean optimizerSet = false;

    private static void setProperOptimizer() {
        if (optimizerSet) {
            return;
        }
        optimizerSet = true;
        OptimizerFactory.setDefaultOptimizer("reflective");
        //System.out.println("MvelExpressionLanguage.setProperOptimizer(): optimizer is set!!!");
    }

    static {
        setProperOptimizer();
    }

    @Override
    protected Class<? extends ParsedExpressionBase> getExprClass() {
        return MvelParsedExpression.class;
    }
}
