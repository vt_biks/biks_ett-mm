/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.ICheckEnabledByDesign;
import pl.fovis.common.VoteBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.PushButtonWithHideText;
import simplelib.BaseUtils;

/**
 *
 * @author beata
 */
public class EntityVoteValsWidget extends EntityDetailsWidgetFlatBase {

    protected PopupPanel hintPopup;
    protected HorizontalPanel hpImages;
    protected PushButtonWithHideText likeBtn;
    protected PushButtonWithHideText dislikeBtn;
    protected HTML hintHtml;
    protected HTML likeCntHtml;
    protected HTML dislikeCntHtml;

    @Override
    public void displayData() {

        hpImages.setVisible(true);
        VoteBean vote = getData().vote;
        likeCntHtml.setHTML(
                " " + "x" /* I18N: no */ + " " + BaseUtils.nullToDef(vote.sumalike, 0));
        dislikeCntHtml.setHTML(
                " " + "x" /* I18N: no */ + " " + Math.abs(BaseUtils.nullToDef(vote.sumaunlike, 0)));
        likeCntHtml.addStyleName("voteCnt");
        dislikeCntHtml.addStyleName("voteCnt");

        int voteVal = getOptUserVoteVal();
        String like3 = voteVal == 1 ? "3" : "";
        String unlike3 = voteVal == -1 ? "3" : "";

        likeBtn.getUpFace().setImage(new Image("images/like" /* I18N: no */ + like3 + "." + "gif" /* I18N: no */));
        dislikeBtn.getUpFace().setImage(new Image("images/unlike" /* I18N: no */ + unlike3 + "." + "gif" /* I18N: no */));
        addBtnTittle(likeBtn, 1);
        addBtnTittle(dislikeBtn, -1);
        setAllBtnActiveOrUnActive();
    }

    public Widget buildWidgets() {
        HorizontalPanel hp = new HorizontalPanel();
        hpImages = new HorizontalPanel();
        likeCntHtml = new HTML();
        dislikeCntHtml = new HTML();

        VerticalPanel popPanel = new VerticalPanel();
        hintHtml = new HTML();
        popPanel.add(hintHtml);

        hintPopup = new PopupPanel(true);
        hintPopup.setAnimationEnabled(true);
        hintPopup.setWidget(popPanel);

        likeBtn = new PushButtonWithHideText(new Image("images/like.gif" /* I18N: no */));
        dislikeBtn = new PushButtonWithHideText(new Image("images/unlike.gif" /* I18N: no */));
        likeBtn.setStyleName("Vote-Btn");
        dislikeBtn.setStyleName("Vote-Btn");

        hpImages.add(likeBtn);
        hpImages.add(likeCntHtml);
        hpImages.add(dislikeBtn);
        hpImages.add(dislikeCntHtml);
        addVoteClickHandler(likeBtn, 1);
        addVoteClickHandler(dislikeBtn, -1);
//        addBtnMouseOverHandler(likeBtn, 1);
//        addBtnMouseOverHandler(dislikeBtn, -1);
        hpImages.setVisible(false);
        hp.add(hpImages);

        return hp;
    }

    protected void setupVoteBtnEvents(Widget btn, int voteVal) {
        addVoteClickHandler(btn, voteVal);
//        addBtnMouseOverHandler(btn, voteVal);
    }

    // voteVal == 0 -> brak głosu, jeżeli był głos to skasować
    protected void updateVoteCountersInData(int newVoteVal) {
        int oldVoteVal = getOptUserVoteVal();

        if (oldVoteVal == newVoteVal) {
            return; // no-op
        }
        VoteBean vote = getData().vote;

        int likeCnt = BaseUtils.nullToDef(vote.sumalike, 0);
        int unlikeCnt = BaseUtils.nullToDef(vote.sumaunlike, 0);

        if (oldVoteVal < 0) {
            unlikeCnt--;
        } else if (oldVoteVal > 0) {
            likeCnt--;
        }

        if (newVoteVal < 0) {
            unlikeCnt++;
        } else if (newVoteVal > 0) {
            likeCnt++;
        }

        vote.sumalike = likeCnt;
        vote.sumaunlike = unlikeCnt;
        vote.userValue = newVoteVal == 0 ? null : newVoteVal;
    }

    public void addVoteClickHandler(Widget btn, final int voteVal) {

        GWTUtils.addOnClickHandler(btn, new ClickHandler() {

            public void onClick(ClickEvent event) {
                boolean isDeleteOp = getOptUserVoteVal() == voteVal;

                if (isDeleteOp) {
                    BIKClientSingletons.getService().deleteLoggedUserVoteForNode(nodeId,
                            getAntiNodeRefreshingCallback(I18n.usunietoOcene.get() /* I18N:  */));
                } else {
                    BIKClientSingletons.getService().addVote(nodeId, voteVal,
                            getAntiNodeRefreshingCallback(I18n.ocenaDodana.get() /* I18N:  */));
                }

                updateVoteCountersInData(isDeleteOp ? 0 : voteVal);

                displayData();
            }
        });
    }

    // nie obsługujemy tutaj w specjalny sposób wartości 0 (brak głosu)
    // jak się trafi -> będzie że użyteczne
    // kod wywołujący powinien zadbać o niewywoływanie tej metody dla 0 bo to
    // po prostu nie ma sensu
    protected String getUsefullnessTextByVoteVal(int voteVal) {
        return voteVal == -1 ? I18n.nieuzyteczne.get() /* I18N:  */ : I18n.uzyteczne.get() /* I18N:  */;
    }

    public void addBtnTittle(final Widget btn, final int voteVal) {
        btn.setTitle((getOptUserVoteVal() == voteVal)
                ? I18n.ocenilesJako.get() /* I18N:  */ + " "
                + getUsefullnessTextByVoteVal(getOptUserVoteVal())
                + ". " + I18n.kliknijAbyUsunac.get() /* I18N:  */ + "."
                : I18n.ocenJako.get() /* I18N:  */ + " " + getUsefullnessTextByVoteVal(voteVal));
    }

    // 0 -> nie głosowal, +1 - like (użyteczne), -1 - dislike (nieużyteczne)
    // nie wiem co to za wartość -2, ale takie coś było w starym kodzie...
    protected int getOptUserVoteVal() {
        VoteBean vote = getData().vote;
        if (vote != null && vote.userValue != null && vote.userValue != -2) {
            return vote.userValue;
        } else {
            return 0;
        }
    }

    protected void setAllBtnActiveOrUnActive() {
        boolean enabled = BIKClientSingletons.isNodeActionEnabledByCustomRightRoles("VoteForUsefulness", getData(), new ICheckEnabledByDesign() {

            @Override
            public boolean isEnabledByDesign() {
                return BIKClientSingletons.isBikUserLoggedIn();
            }
        });
        dislikeBtn.setEnabled(enabled);
        likeBtn.setEnabled(enabled);
    }
}
