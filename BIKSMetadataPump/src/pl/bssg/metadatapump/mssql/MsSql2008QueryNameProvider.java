/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.mssql;

/**
 *
 * @author tflorczak
 */
public class MsSql2008QueryNameProvider implements IMsSqlQueryNameProvider {

    @Override
    public String getDatabases() {
        return "getMSSQLDatabases";
    }

    @Override
    public String getExtendedProperties() {
        return "getMSSQLDatabaseEXProp";
    }

    @Override
    public String getObjects() {
        return "getMSSQLTables";
    }

    @Override
    public String getObjectEXProp() {
        return "getMSSQLObjectEXProp";
    }

    @Override
    public String getColumns() {
        return "getMSSQLColumns";
    }

    @Override
    public String getIndexes() {
        return "getMSSQLIndexes";
    }

    @Override
    public String getForeignKeys() {
        return "getMSSQLForeignKeys";
    }

    @Override
    public String getDependencies() {
        return "getMSSQLDependencies";
    }

    @Override
    public String getGrants() {
        return "getMSSQLGrants";
    }
}
