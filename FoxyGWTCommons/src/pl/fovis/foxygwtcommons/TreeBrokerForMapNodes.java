/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import java.util.Map;

/**
 *
 * @author wezyr
 */
public class TreeBrokerForMapNodes<IDT> implements ITreeBroker<Map<String, Object>, IDT> {

    protected String idFieldName;
    protected String parentIdFieldName;

    public TreeBrokerForMapNodes(String idFieldName, String parentIdFieldName) {
        this.idFieldName = idFieldName;
        this.parentIdFieldName = parentIdFieldName;
    }

    @SuppressWarnings("unchecked")
    public IDT getNodeId(Map<String, Object> n) {
        return (IDT) n.get(idFieldName);
    }

    @SuppressWarnings("unchecked")
    public IDT getParentId(Map<String, Object> n) {
        return (IDT) n.get(parentIdFieldName);
    }
}
