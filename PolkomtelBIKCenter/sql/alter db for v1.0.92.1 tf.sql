﻿exec sp_check_version '1.0.92.1';
go

-----------------------------------
-----------------------------------

update bik_node 
set is_deleted=1 
where node_kind_id in (select id from bik_node_kind where code='ConnectionUserFolder' or code='ConnectionEngineFolder')

update bik_node_kind
set code='ConnectionNetworkFolder', caption='Folder warstwy sieci', icon_name='connectionNetworkFolder'
where code='ConnectionUserFolder'
go

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_connections_into_bik_node]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_insert_connections_into_bik_node
go


create procedure [dbo].[sp_insert_connections_into_bik_node]
as
begin

create table #tmp_network (name varchar(max));
create table #tmp_engine (db_engine varchar(max),network_layer varchar(max));
create table #tmp_conn (conn_node_id int,parent_node_id int);

declare @connTree int;
select @connTree = id from bik_tree where code='Connections'
declare @connFolderKind int;
select @connFolderKind = id from bik_node_kind where code='ConnectionFolder'
declare @connEngineFolderKind int;
select @connEngineFolderKind = id from bik_node_kind where code='ConnectionEngineFolder'
declare @connNetworkFolderKind int;
select @connNetworkFolderKind = id from bik_node_kind where code='ConnectionNetworkFolder'
declare @parentNodeId int;
select @parentNodeId = id from bik_node where name='Połączenia' and tree_id=@connTree and node_kind_id=@connFolderKind and is_deleted=0 and linked_node_id is null

-- wrzucenie network layers folders
insert into #tmp_network(name)
select distinct connetion_networklayer_name from bik_sapbo_universe_connection

-- przywrocenie dobrych folderkow, usunietych po 1 fazie zasilania
update bik_node
set is_deleted=0
where node_kind_id=@connNetworkFolderKind
and linked_node_id is null
and is_deleted=1
and tree_id=@connTree
and parent_node_id=@parentNodeId
and name in (select name from #tmp_network)

-- dodanie nowych, jeśli są nowe
insert into bik_node (parent_node_id, node_kind_id, name, tree_id)
	select @parentNodeId,@connNetworkFolderKind, case when #tmp_network.name is null then 'Inny' else #tmp_network.name end, @connTree
	from #tmp_network left join bik_node bn on bn.name=#tmp_network.name and bn.is_deleted=0
	and bn.linked_node_id is null and bn.node_kind_id=@connNetworkFolderKind and bn.tree_id=@connTree
	where bn.id is null;

-- usuniecie starych/niepotrzebnych
update bik_node
set is_deleted=1
where node_kind_id=@connNetworkFolderKind
and name not in (select name from #tmp_network)
and linked_node_id is null
and is_deleted=0

-- wrzucenie DB engine folders
insert into #tmp_engine(db_engine, network_layer)
select distinct case when database_enigme is null then '(nieskategoryzowane)' else database_enigme end,connetion_networklayer_name from bik_sapbo_universe_connection

-- przywrocenie dobrych folderkow, usunietych po 1 fazie zasilania
update bik_node
set is_deleted=0
where node_kind_id=@connEngineFolderKind
and linked_node_id is null
and is_deleted=1
and tree_id=@connTree
and parent_node_id in (select id from bik_node where is_deleted=0 and linked_node_id is null and node_kind_id=@connNetworkFolderKind and tree_id=@connTree)
and name in (select db_engine from #tmp_engine)

-- dodanie nowych, jeśli są nowe
insert into bik_node (parent_node_id, node_kind_id, name, tree_id)
	select (select id from bik_node where name=#tmp_engine.network_layer and is_deleted=0 and linked_node_id is null and node_kind_id=@connNetworkFolderKind and tree_id=@connTree),@connEngineFolderKind, #tmp_engine.db_engine, @connTree
	from #tmp_engine left join bik_node bn on bn.name=#tmp_engine.db_engine and bn.is_deleted=0
	and bn.linked_node_id is null and bn.node_kind_id=@connEngineFolderKind and bn.tree_id=@connTree
	and bn.parent_node_id=(select id from bik_node where name=#tmp_engine.network_layer and is_deleted=0 and linked_node_id is null and node_kind_id=@connNetworkFolderKind and tree_id=@connTree)
	where bn.id is null;

-- usuniecie starych/niepotrzebnych	
update bik_node
set is_deleted=1
where node_kind_id=@connEngineFolderKind
and name not in (select db_engine from #tmp_engine)
and linked_node_id is null
and is_deleted=0

-- podłączenie połączeń pod odpowiednie foldery
insert into #tmp_conn(conn_node_id,parent_node_id)
select bsuc.node_id, bn.id from bik_sapbo_universe_connection bsuc 
join bik_node bn on bn.name = case when bsuc.database_enigme is null then '(nieskategoryzowane)' else bsuc.database_enigme end
and bn.is_deleted=0 and bn.linked_node_id is null 
and bn.node_kind_id=@connEngineFolderKind and bn.tree_id=@connTree
join bik_node bn2 on bn.parent_node_id = bn2.id
and bn2.name=bsuc.connetion_networklayer_name

update bik_node
set parent_node_id = #tmp_conn.parent_node_id
from #tmp_conn
where bik_node.id=#tmp_conn.conn_node_id

drop table #tmp_engine;
drop table #tmp_network;
drop table #tmp_conn;

exec sp_node_init_branch_id @connTree,@parentNodeId
end;
go

-------------------------------------
-------------------------------------
-------------------------------------

delete from bik_node where tree_id = dbo.fn_tree_id_by_code('TreeOfTrees')
go

exec sp_add_menu_node null, 'Mój BIKS', '#MyBIKS'
exec sp_add_menu_node null, 'Biblioteka', 'metadata'
exec sp_add_menu_node 'metadata', 'SAP BO', 'sapbo'
exec sp_add_menu_node 'sapbo', '@', '$Reports'
exec sp_add_menu_node 'sapbo', '@', '$ObjectUniverses'
exec sp_add_menu_node 'sapbo', '@', '$Connections'
exec sp_add_menu_node 'metadata', '@', '$Teradata'
exec sp_add_menu_node null, '@', '$Glossary'
exec sp_add_menu_node null, 'Kategoryzacje', '@Taxonomy'
exec sp_add_menu_node null, 'Dokumenty', '$Documents'
exec sp_add_menu_node null, 'Blogi', '$Blogs'
exec sp_add_menu_node null, 'Społeczność BI', '$Users'
exec sp_add_menu_node null, 'Baza Wiedzy', '$FAQ'
exec sp_add_menu_node null, 'Admin', '#Admin'
exec sp_add_menu_node null, 'Szukaj', '#Search'

exec sp_add_menu_node '$Teradata', '@', '&TeradataOwner'
exec sp_add_menu_node '&TeradataOwner', '@', '&TeradataSchema'
exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataView'
exec sp_add_menu_node '&TeradataView', '@', '&TeradataColumn'
exec sp_add_menu_node '@Taxonomy', '@', '&TaxonomyEntity'
exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataTable'
exec sp_add_menu_node '&TeradataTable', '@', '&TeradataColumn'
exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataProcedure'
exec sp_add_menu_node '$Reports', '@', '&ReportFolder'
exec sp_add_menu_node '&ReportFolder', '@', '&Webi'
exec sp_add_menu_node '&ReportFolder', '@', '&FullClient'
exec sp_add_menu_node '&Webi', '@', '&ReportQuery'
exec sp_add_menu_node '&ReportQuery', '@', '&Dimension$Reports'
exec sp_add_menu_node '&ReportQuery', '@', '&Measure'
exec sp_add_menu_node '&ReportQuery', '@', '&Detail'
exec sp_add_menu_node '&ReportFolder', '@', '&Flash'
exec sp_add_menu_node '&ReportFolder', '@', '&Excel'
exec sp_add_menu_node '&ReportFolder', '@', '&Hyperlink'
exec sp_add_menu_node '&ReportFolder', '@', '&Powerpoint'
exec sp_add_menu_node '&ReportFolder', '@', '&Pdf'
exec sp_add_menu_node '&ReportFolder', '@', '&CrystalReport'
exec sp_add_menu_node '$ObjectUniverses', '@', '&UniversesFolder'
exec sp_add_menu_node '&UniversesFolder', '@', '&Universe'

exec sp_add_menu_node '&Universe', '@', '&UniverseDerivedTable'
exec sp_add_menu_node '&Universe', '@', '&UniverseAliasTable'
exec sp_add_menu_node '&Universe', '@', '&UniverseTable'

exec sp_add_menu_node '&Universe', '@', '&UniverseClass'
exec sp_add_menu_node '&UniverseClass', '@', '&Dimension'
exec sp_add_menu_node '&UniverseClass', '@', '&Measure'
exec sp_add_menu_node '&Dimension', '@', '&Detail'
exec sp_add_menu_node '&UniverseClass', '@', '&Filter'

exec sp_add_menu_node '$Connections', '@', '&ConnectionNetworkFolder'
exec sp_add_menu_node '&ConnectionNetworkFolder', '@', '&ConnectionEngineFolder'

exec sp_add_menu_node '&ConnectionEngineFolder', '@', '&DataConnection'

exec sp_add_menu_node '$Documents', '@', '&DocumentsFolder'
exec sp_add_menu_node '&DocumentsFolder', '@', '&Document'

exec sp_add_menu_node '$Users', '@', '&UsersGroup'
exec sp_add_menu_node '&UsersGroup', '@', '&User'
exec sp_add_menu_node '$Blogs', '@', '&Blog'

exec sp_add_menu_node '$FAQ', '@', '&CategoriesOfQuestions'
exec sp_add_menu_node '&CategoriesOfQuestions', '@', '&Question'

exec sp_add_menu_node '$Glossary', '@', '&Keyword'

exec sp_add_menu_node '$Glossary', '@', '&GlossaryCategory'

exec sp_add_menu_node '&GlossaryCategory', '@', '&Glossary'
exec sp_add_menu_node '&GlossaryCategory', '@', '&GlossaryNotCorpo'
exec sp_add_menu_node '&GlossaryCategory', '@', '&GlossaryVersion'

exec sp_add_menu_node '&Blog', '@', '&BlogEntry'
go

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
exec sp_node_init_branch_id @tree_id, null
go



------------------------------------
------------------------------------

exec sp_update_version '1.0.92.1', '1.0.93';
go