/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tflorczak
 */
public class SapBoOLAPConnectionExtraDataBean implements Serializable {
    
    public String type;
    public String providerCaption;
    public String cubeCaption;
    public String cuid;
    public Date created;
    public Date modified;
    public String owner;
    public String networkLayer;
    public String mdUsername;
}
