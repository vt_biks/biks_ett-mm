delete from bik_translation
go

delete from bik_help_translation
go


insert into bik_translation (code, txt, lang, kind)
select  bik_node_kind.code, cast(bik_node_kind.caption as varchar(800))+cast('_en' as varchar(3)), 'en', 'kind'
from  bik_node_kind;
go

insert into bik_translation (code, txt, lang, kind)
select  ad.name, cast(ad.name as varchar(800))+cast('_en' as varchar(3)), 'en', 'adef'
from  bik_attr_def ad
group by name;
go

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
insert into bik_translation (code, txt, lang, kind)
select  bik_node.obj_id, cast(bik_node.name as nvarchar(800))+cast('_en' as nvarchar(3)), 'en', 'node'
from  bik_node
where (tree_id=@tree_id or is_built_in=1) and name<>'@' and is_deleted = 0;
go

insert into bik_help_translation (help_key, caption, text_help, text_help_plain, lang)
select  bik_help.help_key, cast('en_' as varchar(3))+cast(bik_help.caption as nvarchar(800)),
        cast('en_' as varchar(3))+cast(bik_help.text_help as varchar(800)),
        cast('en_' as varchar(3))+cast(bik_help.text_help_plain as varchar(800)), 'en'
from  bik_help;
go

insert into bik_translation (code, txt, lang, kind)
select  bik_tree_kind.code, cast(bik_tree_kind.caption as varchar(800))+cast('_en' as varchar(3)), 'en', 'tkind'
from  bik_tree_kind;
go
 
insert into bik_translation (code, txt, lang, kind)
select  bik_tree.code, cast(bik_tree.name as varchar(800))+cast('_en' as varchar(3)), 'en', 'tree'
from  bik_tree
where code <> 'TreeOfTrees';
go
