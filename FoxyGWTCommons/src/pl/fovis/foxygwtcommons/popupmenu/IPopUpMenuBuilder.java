/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.popupmenu;

/**
 *
 * @author pmielanczuk
 */
public interface IPopUpMenuBuilder<CTX> {

    public void addActions(IFoxyPopUpMenu<CTX> popupMenuWidget);
}
