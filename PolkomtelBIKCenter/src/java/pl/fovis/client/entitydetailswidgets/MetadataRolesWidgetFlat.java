/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import pl.fovis.client.BIKGWTConstants;

/**
 *
 * @author bfechner
 */
public class MetadataRolesWidgetFlat extends UserInNodeWidgetFlatBase {

   
    @Override
    public String getCaption() {
        return BIKGWTConstants.MENU_NAME_METADATA;
    }

    @Override
    public String getTreeKindForSelected() {
        return BIKGWTConstants.TREE_KIND_METADATA;
    }

}
