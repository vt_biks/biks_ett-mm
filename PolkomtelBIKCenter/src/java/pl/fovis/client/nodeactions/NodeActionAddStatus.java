/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.AddStatusAttributeDialog;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author bfechner
 */
public class NodeActionAddStatus extends NodeActionAddAttributeForKind {

    public boolean isEnabledByDesign() {

        return BIKClientSingletons.showAddStatus() && !BaseUtils.isCollectionEmpty(getStatusAttr());
    }

    protected List<AttributeBean> getStatusAttr() {
        List<AttributeBean> attrs = BIKClientSingletons.filtrPublicAttributes(node, data.attributes);
        List<AttributeBean> statusAttrs = new ArrayList<AttributeBean>();
        for (AttributeBean ab : attrs) {
            if (ab.isStatus) {
                statusAttrs.add(ab);
            }
        }
        return statusAttrs;
    }

    @Override
    public String getCaption() {
        return I18n.ustawStatus.get();
    }

    @Override
    public void execute() {

//        List<AttributeBean> attrs = BIKClientSingletons.filtrPublicAttributes(node, data.attributes);
        List<AttributeBean> statusAttrs = getStatusAttr();
//        for (AttributeBean ab : attrs) {
//            if (ab.isStatus) {
//                statusAttrs.add(ab);
//            }
//        }

        new AddStatusAttributeDialog().buildAndShowDialog(statusAttrs, new IParametrizedContinuation<Map<String, AttributeBean>>() {
            @Override
            public void doIt(Map<String, AttributeBean> param) {
                BIKClientSingletons.getService().setStatus(node.id, param, new StandardAsyncCallback<Void>() {
                    @Override
                    public void onSuccess(Void result) {
                        BIKClientSingletons.showInfo(I18n.statusUstawiony.get() /* I18N:  */);
                    }
                });
            }
        });

//        BIKClientSingletons.getEDPForms().showAttributeFormForStatus(node.id, node, null, node.treeId, BIKClientSingletons.filtrPublicAttributes(node, data.attributes), node.nodeKindId,
//                (BIKClientSingletons.isTreeKindDynamic(node.treeKind) && !BIKConstants.TREE_KIND_META_BIKS.equalsIgnoreCase(node.treeKind)), null);
    }
}
