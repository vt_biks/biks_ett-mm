﻿exec sp_check_version '1.2.3.2';
go


insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank)
values('ReportSchedule', 'Harmonogram', 'schedule', 'Metadata', 0, 15)

exec sp_add_menu_node '&Webi', '@', '&ReportSchedule'

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
exec sp_node_init_branch_id @tree_id, null
go

create table aaa_report_schedule (
	id int not null,
	parent_id int not null,
	name varchar(max) not null,
	destination varchar(max) null,
	owner varchar(max) not null,
	creation_time datetime not null,
	nextruntime datetime not null,
	expire datetime null,
	type int not null,
	interval_minutes int not null,
	interval_hours int not null,
	interval_days int not null,
	interval_months int not null,
	interval_nth_day int not null,
	format varchar(max) not null,
	parameters varchar(max) null
);
go

create table bik_sapbo_schedule (
	node_id int not null unique,
	destination varchar(max) null,
	owner varchar(max) not null,
	creation_time datetime not null,
	nextruntime datetime not null,
	expire datetime null,
	type int not null,
	interval_minutes int not null,
	interval_hours int not null,
	interval_days int not null,
	interval_months int not null,
	interval_nth_day int not null,
	format varchar(max) not null,
	parameters varchar(max) null
);
go

-- poprzednia wersja w: alter db for v1.2.0.1 tf.sql
-- dodanie harmonogramow pod raportami
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node]
go

create procedure [dbo].[sp_insert_into_bik_node](@tree_code varchar(255))
as
	declare @table_folders varchar(255);
	declare @tree_id int;
	declare @kinds varchar(255);
begin
	set nocount on;
	
	declare @diag_level int = 1;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': poczatek'
	
	select @tree_id = id 
	from bik_tree
	where code=@tree_code;

	create table #primaryIds(si_id varchar(5000) primary key);

	if(@tree_code!='Reports' and @tree_code!='Teradata')
	begin
		set @table_folders = ' APP_FOLDER ';
		if(@tree_code = 'Connections')
		begin
			set @kinds = '''MetaData.DataConnection''';
			exec('insert into #primaryIds
					select convert(varchar(30),SI_ID) as SI_ID  
					from APP_METADATA__DATACONNECTION
					where SI_KIND in (' + @kinds + ') and SI_OBJECT_IS_CONTAINER=0');			
		end;
		if(@tree_code = 'ObjectUniverses')
		begin
			set @kinds = '''Universe''';
			exec('insert into #primaryIds
					select convert(varchar(30),SI_ID) as SI_ID  
					from APP_UNIVERSE
					where SI_KIND in (' + @kinds + ')');
		end;		
	end
	
	if(@tree_code='Reports')
	begin
		-- wybranie folderw raportów bez raportów użytkowników
		set @table_folders = ' INFO_FOLDER where si_name != ''~Webintelligence''';
		--set @table_folders = ' INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2 is null or SI_PATH__SI_FOLDER_NAME2 != ''User Folders''';
		set @kinds = '''Webi'', ''Flash'', ''CrystalReport'',''Excel'',''FullClient'',''Pdf'',''Hyperlink''
		,''Rtf'',''Txt'',''Powerpoint'',''Word''';
	end;
	
    declare @sql nvarchar(max);
				
	declare @node_kind_id int;
	set @sql = N'';
	if(@tree_code = 'Teradata')
	begin
		--print cast(sysdatetime() as varchar(22)) + ': faza 4.1 - teradata przed sp_teradata_init_branch_names'
		--exec sp_teradata_init_branch_names;
		if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': tworzenie pierwszego @sql'
		set @sql = N'select branch_names as SI_ID, parent_branch_names	as SI_PARENTID, 
						type as SI_KIND, name as SI_NAME, extra_info as DESCR
				 from bik_teradata';
	end;
	else
	begin
		if(@tree_code = 'Reports')
		-- sztuczny podzial na root folder i user folders dla raportów
		set @sql = N'select distinct SI_PARENT_FOLDER as SI_ID, (select si_id from INFO_FOLDER where SI_NAME=''User Folders'') as SI_PARENTID, SI_KIND, SI_PATH__SI_FOLDER_NAME1 as SI_NAME, NULL as DESCR
				 from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2=''User Folders'' 
				 and SI_NAME!=''~Webintelligence'' 

				 union all
				
				 select si_id, case
									when SI_PARENTID=0 and si_name!=''User Folders''
										then (select si_id from INFO_FOLDER where SI_NAME=''Root Folder'')
									when si_id in(select si_id from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2 = ''User Folders'')
                                        then SI_PARENTID
									when SI_PARENTID in(select si_id from INFO_FOLDER)
										then SI_PARENTID
									else 0 end as SI_PARENTID, SI_KIND, case
																			when SI_NAME=''Root Folder''
																				then ''Foldery korporacyjne''
																			when SI_NAME=''User Folders''
																				then ''Foldery użytkowników''
																			else SI_NAME end, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from ' + @table_folders + 								
				' union all

				 select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +') and SI_INSTANCE = 0
				union all
				
				select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR
				from aaa_global_props where si_parentid in (select si_parentid from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2=''User Folders'' and SI_NAME!=''~Webintelligence'') and si_kind!=''Folder''
				
				union all
				
				select id, parent_id, ''ReportSchedule'', name, null
				from aaa_report_schedule sch
				inner join aaa_global_props gp on gp.si_id=sch.parent_id
				';
		if(@tree_code = 'Connections')
			set @sql = N'select 1 as si_id, 0 as si_parentid, ''ConnectionFolder'' as si_kind, ''Połączenia'' as si_name, '''' as descr
				
				 union all

				 select SI_ID, 1, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from APP_METADATA__DATACONNECTION
				 where SI_KIND in ('+ @kinds +') and SI_OBJECT_IS_CONTAINER=0';

		if(@tree_code = 'ObjectUniverses')
			set @sql = N'select si_id, case 
									when SI_NAME=''Universes''
										then 0
									when SI_PARENTID in(select si_id from ' + @table_folders +') 
										then SI_PARENTID
									else 0 end as SI_PARENTID, SI_KIND, case when SI_NAME=''Universes'' then ''Światy obiektów'' else SI_NAME end as SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from ' + @table_folders + 
													
				'union all

				 select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +')';
	end;					

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed zapytaniem'
	print 'zapytanie: ' + @sql;

	create table #tmpNodes (si_id varchar(1000) primary key, si_parentid varchar(1000), si_kind varchar(max), si_name varchar(max), descr varchar(max));
	if(@tree_code = 'Reports')
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''ReportFolder'' 
										else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	if(@tree_code = 'Connections')
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''ConnectionFolder''
										when si_kind=''MetaData.DataConnection'' then ''DataConnection''
										else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	if(@tree_code = 'ObjectUniverses')
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''UniversesFolder'' else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	if(@tree_code = 'Teradata')
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, si_kind, si_name, DESCR
					from (' + @sql + ') xxx';

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po stworzeniu drugiego @sql'
			   
	EXECUTE(@sql);

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po wrzuceniu do #tmpNodes'

	insert into bik_node_kind (code,caption) 
	select distinct si_kind, si_kind
	from #tmpNodes 
	where not exists (select 1 from bik_node_kind where code = si_kind);
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/

	declare @rc int = 1

	while @rc > 0 begin
	
	delete from #tmpNodes
	where (si_kind = 'UniversesFolder' or si_kind = 'ConnectionFolder' or si_kind = 'ReportFolder') and not exists(select 1 from #tmpNodes g where g.si_parentid = #tmpNodes.si_id)
		set @rc = @@ROWCOUNT
	end;--end loop
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/
	
	declare @report_tree_id int;
	select @report_tree_id=id from bik_tree where code='Reports'

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed updatetami'

	update bik_node 
	set parent_node_id = null, node_kind_id = bik_node_kind.id, name = ltrim(rtrim(#tmpNodes.si_name)),
		is_deleted = 0, descr = #tmpNodes.descr, tree_id = @tree_id
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.obj_id = #tmpNodes.si_id and tree_id = @tree_id and bik_node.linked_node_id is null
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed update podlinkowanych'
	--update podlinkowanych
	update bik_node 
	set /*parent_node_id = null, */node_kind_id = bik_node_kind.id, name = ltrim(rtrim(#tmpNodes.si_name)),
		 descr = #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.obj_id = #tmpNodes.si_id and (tree_id = @report_tree_id or tree_id in (select id from bik_tree where tree_kind='Taxonomy')) and not bik_node.linked_node_id is null

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed insertami'
	
	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select bik_node_kind.id, ltrim(rtrim(#tmpNodes.si_name)), @tree_id, #tmpNodes.si_id, #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
		left join bik_node on bik_node.obj_id = #tmpNodes.si_id and bik_node.tree_id=@tree_id
	where bik_node.id is null;

	set nocount on;

	drop table #primaryIds;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed updateami parentow'
	

	update bik_node 
	set parent_node_id= bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id and bik_node.tree_id = @tree_id
	inner join bik_node bk on bk.obj_id = pt.si_parentid where bik_node.linked_node_id is null and bk.tree_id = @tree_id 

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed usuwaniem'
	
	update --delete from 
	bik_node
	set is_deleted = 1
	--select *
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id join bik_node_kind on bik_node.node_kind_id = bik_node_kind.id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and not bik_node_kind.code in 
	('Measure', 'Dimension', 'Detail', 'UniverseClass', 'ReportQuery', 'Filter', 'UniverseColumn', 'UniverseTable', 'UniverseAliasTable', 'UniverseDerivedTable', 'UniverseTablesFolder', 'ConnectionEngineFolder', 'ConnectionNetworkFolder')
				
	drop table #tmpNodes
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po usuwaniu i po dropie'

	exec sp_delete_linked_nodes_where_orignal_is_deleted;
	exec sp_node_init_branch_id @tree_id, null;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': koniec'
end;
go

-- poprzednia wersja w: alter db for v1.0.93.1 tf.sql
-- dodanie ekstradaty dla harmonogramow
if exists(select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbo_extradata]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node_sapbo_extradata]
go

create procedure [dbo].[sp_insert_into_bik_node_sapbo_extradata]
as
begin
	delete from bik_sapbo_extradata

	insert into bik_sapbo_extradata (node_id,author,owner,keyword,file_path,file_name,created,modified,cuid,files_value,size,has_children,guid,instance,owner_id,progid,obtype,flags,children,ruid,runnable_object,content_locale,is_schedulable,webi_doc_properties,read_only,last_successful_instance_id,last_run_time,timestamp,progid_machine,endtime,starttime,schedule_status,recurring,nextruntime,doc_sender,revisionnum,application_object,shortname,dataconnection_total,universe_total )
    select bik.id as node_id,
    nullif(ltrim(rtrim(iw.SI_AUTHOR)), '') as author,
    nullif(ltrim(rtrim(p.SI_OWNER)), '') as owner,
    nullif(ltrim(rtrim(p.SI_KEYWORD)), '') as keyword,
    nullif(ltrim(rtrim(p.SI_FILES__SI_PATH)), '') as file_path,
    nullif(ltrim(rtrim(p.SI_FILES__SI_FILE1)), '') as file_name,
    nullif(ltrim(rtrim(p.SI_CREATION_TIME)), '') as created,
    nullif(ltrim(rtrim(p.SI_UPDATE_TS)), '') as modified,
    nullif(ltrim(rtrim(p.SI_CUID)), '') as cuid,
    nullif(ltrim(rtrim(p.SI_FILES__SI_VALUE1)), '') as files_value,
    nullif(ltrim(rtrim(p.SI_SIZE)), '') as size,
    p.SI_HAS_CHILDREN as has_children,
    nullif(ltrim(rtrim(p.SI_GUID)), '') as guid,
    p.SI_INSTANCE as instance,
    p.SI_OWNERID as owner_id,
    nullif(ltrim(rtrim(p.SI_PROGID)), '') as progid,
    p.SI_OBTYPE as obtype,
    p.SI_FLAGS as flags,
    p.SI_CHILDREN as children,
    nullif(ltrim(rtrim(p.SI_RUID)), '') as ruid,
    p.SI_RUNNABLE_OBJECT as runnable_object,
    nullif(ltrim(rtrim(p.SI_CONTENT_LOCALE)), '') as content_locale,
    p.SI_IS_SCHEDULABLE as is_schedulable,
    nullif(ltrim(rtrim(p.SI_WEBI_DOC_PROPERTIES)), '') as webi_doc_properties,
    p.SI_READ_ONLY as read_only,
    nullif(ltrim(rtrim(p.SI_LAST_SUCCESSFUL_INSTANCE_ID)), '') as last_successful_instance_id,
    nullif(ltrim(rtrim(p.SI_LAST_RUN_TIME)), '') as last_run_time,
    nullif(ltrim(rtrim(iw.SI_TIMESTAMP)), '')  as timestamp,
    nullif(ltrim(rtrim(iw.SI_PROGID_MACHINE)), '') as progid_machine,
    nullif(ltrim(rtrim(p.SI_ENDTIME)), '') as endtime,
    nullif(ltrim(rtrim(p.SI_STARTTIME)), '') as starttime,
    nullif(ltrim(rtrim(p.SI_SCHEDULE_STATUS)), '') as schedule_status,
    nullif(ltrim(rtrim(p.SI_RECURRING)), '') as recurring,
    nullif(ltrim(rtrim(p.SI_NEXTRUNTIME)), '') as nextruntime,
    nullif(ltrim(rtrim(p.SI_DOC_SENDER)), '') as doc_sender,
    au.SI_REVISIONNUM as revisionnum,
    au.SI_APPLICATION_OBJECT as application_object,
    nullif(ltrim(rtrim(au.SI_SHORTNAME)), '') as shortname,
    au.SI_DATACONNECTION__SI_TOTAL as dataconnection_total,
    iw.SI_UNIVERSE__SI_TOTAL as universe_total
    from aaa_global_props p
    left join INFO_WEBI iw on p.si_id = iw.si_id
    left join APP_UNIVERSE au on p.si_id = au.si_id
    inner join bik_node bik on convert(varchar(30),p.si_id) = bik.obj_id
    order by bik.id
    
    delete from bik_sapbo_schedule
    
    insert into bik_sapbo_schedule(node_id,destination,owner,creation_time,nextruntime,expire,type,interval_minutes,interval_hours,interval_days,interval_months,interval_nth_day,format,parameters)
    select bn.id,destination,owner,creation_time,nextruntime,expire,type,interval_minutes,interval_hours,interval_days,interval_months,interval_nth_day,format,parameters 
    from aaa_report_schedule sch
    inner join bik_node bn on convert(varchar(30),sch.id) = bn.obj_id
    
end;
go

-- poprzednia wersja w pliku alter db for v1.2.1.5 tf.sql
-- dodanie wyszukiwania po harmonogramach
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs_metadata]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs_metadata
go

create procedure [dbo].[sp_verticalize_node_attrs_metadata](@optNodeFilter varchar(max))
as
begin
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_extradata', 'node_id', 'author, owner, cuid, guid, ruid', null, @optNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_query', 'node_id', 'sql_text, filtr_text', null, @optNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_connection', 'node_id', 'database_engine, database_source, connetion_networklayer_name, user_name, server', null, @optNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_object', 'node_id', 'text_of_select, text_of_where', null, @optNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_table', 'node_id', 'sql_of_derived_table', null, @optNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_schedule', 'node_id', 'owner, creation_time, nextruntime, expire, format, parameters', null, @optNodeFilter
  
	declare @tree_id int = (select id from bik_tree where code = 'DQC')
	declare @inact_nk_id int = (select id from bik_node_kind where code = 'DQCTestInactive'),
    @succ_nk_id int = (select id from bik_node_kind where code = 'DQCTestSuccess'),
    @fail_nk_id int = (select id from bik_node_kind where code = 'DQCTestFailed')

	declare @dqc_src_sql varchar(max) = '(select cast(dt.long_name as varchar(max)) as long_name, cast(dt.sampling_method as varchar(max)) as sampling_method, cast(dt.verified_attributes as varchar(max)) as verified_attributes,cast(dt.expected_result as varchar(max)) as expected_result,cast(dt.logging_details as varchar(max)) as logging_details, cast(dt.benchmark_definition as varchar(max)) as benchmark_definition, cast(dt.results_object as varchar(max)) as results_object, cast(dt.additional_information as varchar(max)) as additional_information, n.id as node_id
		from bik_dqc_test dt inner join bik_node n on dt.__obj_id = n.obj_id and n.node_kind_id in (' +
		cast(@inact_nk_id as varchar(20)) + ',' + cast(@succ_nk_id as varchar(20)) + ',' + cast(@fail_nk_id as varchar(20)) + ') and tree_id = ' + cast(@tree_id as varchar(20)) + '
		where n.linked_node_id is null and n.is_deleted = 0)'
	exec sp_verticalize_node_attrs_one_metadata_table @dqc_src_sql, 'node_id', 'long_name, sampling_method, verified_attributes,expected_result,logging_details, benchmark_definition, results_object, additional_information', null, @optNodeFilter
	
	declare @ad_src_sql varchar(max) = '(select bu.email, coalesce(bu.phone_num, ad.telephone_number) as phone_num,
		ad.physical_delivery_office_name,ad.postal_code, ad.manager, 
		ad.description, ad.department, ad.title, ad.mobile, 
		ad.display_name, bu.node_id
		from bik_user bu
		join bik_node bn on bn.id = bu.node_id and bn.is_deleted = 0
		left join bik_system_user bsu on bsu.user_id = bu.id 
		left join bik_active_directory ad 
		on (bu.login_name_for_ad = ad.s_a_m_account_name or bsu.login_name = ad.s_a_m_account_name))'
	exec sp_verticalize_node_attrs_one_metadata_table @ad_src_sql, 'node_id', 'email, phone_num, physical_delivery_office_name,postal_code,manager, description, department, title, mobile, display_name', null, @optNodeFilter
	
	declare @obj_id_sql varchar(max) = '(select bn.id, bn.obj_id as si_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id=bnk.id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''DataConnection'', ''Webi'', ''Flash'', ''CrystalReport'', ''Universe'', ''Excel'', ''FullClient'', ''Pdf'', ''Hyperlink'', ''Powerpoint'',
		''ReportFolder'', ''UniversesFolder'', ''ConnectionFolder'', ''ReportSchedule''))'
	exec sp_verticalize_node_attrs_one_metadata_table @obj_id_sql, 'id', 'si_id', null, @optNodeFilter
	
	-- SAP BW query extradata
	declare @bwreports_id int = (select id from bik_tree where code = 'BWReports')
	declare @query_nk_id int = (select id from bik_node_kind where code = 'BWBEx')
	declare @sapbw_query_src_sql varchar(max) = '(select q.update_time, q.owner, q.last_edit, bn.id as node_id
		from bik_sapbw_query q inner join bik_node bn on q.obj_name = bn.obj_id and bn.node_kind_id = ' +
		cast(@query_nk_id as varchar(20)) + ' and bn.tree_id = ' + cast(@bwreports_id as varchar(20)) + '
		where bn.linked_node_id is null and bn.is_deleted = 0)'
	exec sp_verticalize_node_attrs_one_metadata_table @sapbw_query_src_sql, 'node_id', 'update_time, owner, last_edit', null, @optNodeFilter
	
	-- SAP BW providers, query and areas - technical id
	declare @bw_obj_id_sql varchar(max) = '(select bn.id, bn.obj_id as technical_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id=bnk.id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''BWBEx'', ''BWArea'', ''BWMPRO'', ''BWCUBE'', ''BWISET'', ''BWODSO''))'
	exec sp_verticalize_node_attrs_one_metadata_table @bw_obj_id_sql, 'id', 'technical_id', null, @optNodeFilter
	
	-- SAP BW objects - technical id
	declare @bw_objs_id_sql varchar(max) = '(select bn.id, obj.obj_name as obj_technical_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id=bnk.id
		inner join bik_sapbw_object obj on obj.provider + ''|'' + obj.obj_name=bn.obj_id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''BWUni'',''BWKyf'', ''BWTim'', ''BWDpa'', ''BWCha'')
		and obj.provider_parent is null)'
	exec sp_verticalize_node_attrs_one_metadata_table @bw_objs_id_sql, 'id', 'obj_technical_id', null, @optNodeFilter
	
end;
go

-- atrybuty do SAP BW 
declare @techName int;
declare @ownerName int;
declare @lastModify int;
declare @modifyBy int;
declare @BExKind int;
select @techName = id from bik_attr_def where name = 'Nazwa techniczna' and is_built_in = 1
select @ownerName = id from bik_attr_def where name = 'Osoba odpowiedzialna' and is_built_in = 1
select @lastModify = id from bik_attr_def where name = 'Ostatnia zmiana' and is_built_in = 1
select @modifyBy = id from bik_attr_def where name = 'Zmieniony przez' and is_built_in = 1
select @BExKind = id from bik_node_kind where code = 'BWBEx'


insert into bik_attr_system(attr_id,node_kind_id,is_visible)
select @techName, id, 1 from bik_node_kind
where code in ('BWArea', 'BWBEx', 'BWMPRO', 'BWCUBE', 'BWODSO', 'BWISET', 'BWUni', 'BWTim', 'BWKyf', 'BWCha', 'BWDpa')

insert into bik_attr_system(attr_id,node_kind_id,is_visible)
values(@ownerName,@BExKind, 1)

insert into bik_attr_system(attr_id,node_kind_id,is_visible)
values(@lastModify,@BExKind, 1)

insert into bik_attr_system(attr_id,node_kind_id,is_visible)
values(@modifyBy,@BExKind, 1)
go

-- atrybuty do harmonogramu
declare @catBO int;
select @catBO = id from bik_attr_category where name = 'SAP BO' and is_built_in = 1

insert into bik_attr_def(name,attr_category_id,is_deleted,is_built_in)
values('Typ cyklu', @catBO, 0, 1)

insert into bik_attr_def(name,attr_category_id,is_deleted,is_built_in)
values('Miejsce docelowe', @catBO, 0, 1)

insert into bik_attr_def(name,attr_category_id,is_deleted,is_built_in)
values('Właściciel', @catBO, 0, 1)

insert into bik_attr_def(name,attr_category_id,is_deleted,is_built_in)
values('Godzina utworzenia', @catBO, 0, 1)

insert into bik_attr_def(name,attr_category_id,is_deleted,is_built_in)
values('Godzina następnego uruchomienia', @catBO, 0, 1)

insert into bik_attr_def(name,attr_category_id,is_deleted,is_built_in)
values('Godzina wygaśnięcia', @catBO, 0, 1)

insert into bik_attr_def(name,attr_category_id,is_deleted,is_built_in)
values('Formaty', @catBO, 0, 1)

insert into bik_attr_def(name,attr_category_id,is_deleted,is_built_in)
values('Parametry', @catBO, 0, 1)
go

declare @scheduleKind int;
select @scheduleKind = id from bik_node_kind where code = 'ReportSchedule'

insert into bik_attr_system(attr_id,node_kind_id,is_visible)
select def.id, @scheduleKind, 1  from bik_attr_def def inner join bik_attr_category cat on cat.id=def.attr_category_id
where cat.name = 'SAP BO'
and cat.is_built_in = 1
and def.name in ('Parametry', 'Formaty', 'Godzina wygaśnięcia', 'Godzina następnego uruchomienia', 'Godzina utworzenia', 'Właściciel', 'Miejsce docelowe', 'Typ cyklu', 'Id')


exec sp_update_version '1.2.3.2', '1.2.3.3';
go
