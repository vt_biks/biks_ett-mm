/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.simpledblib;

import simplelib.LameRuntimeException;
import commonlib.JdbcUtil;
import commonlib.LameUtils;
import commonlib.MuppetMaker;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

/**
 *
 * @author wezyr
 */
public class ConnectionViaJdbc implements IConnection {

    protected Connection conn;
    //protected Statement st;
    private PreparedStatement batchStmt;
    protected String batchTxt;
    protected int[] paramTypes;

    public ConnectionViaJdbc(Map<String, Object> props) {
        JdbcConnectionProps propsMuppet = MuppetMaker.newMuppet(JdbcConnectionProps.class, props);
        conn = JdbcUtil.getConnection(propsMuppet.driverClass,
                propsMuppet.connUrl, propsMuppet.user, propsMuppet.password, propsMuppet.catalog, true);
        try {
            conn.setAutoCommit(false);
            //st = conn.createStatement();
        } catch (Exception ex) {
            throw new LameRuntimeException("cannot create statement", ex);
        }
    }

    public int execCommand(String sqlTxt) {
        int res;

        try {
            Statement st = conn.createStatement();
            res = st.executeUpdate(sqlTxt);
            st.close();
            conn.commit();
        } catch (Exception ex) {
            throw new LameRuntimeException("cannot execute command: " + sqlTxt, ex);
        }

        return res;
    }

    public void startBatch(String sqlTxt, int[] paramTypes) {
        if (batchStmt != null) {
            throw new LameRuntimeException("cannot start new batch (\"" +
                    sqlTxt + "\") while other (\"" + batchTxt + "\") is started");
        }
        try {
            batchStmt = conn.prepareStatement(sqlTxt);
            //conn.setAutoCommit(false);
        } catch (Exception ex) {
            throw new LameRuntimeException("error preparing statement for sql: \"" + sqlTxt + "\"", ex);
        }
        batchTxt = sqlTxt;
        this.paramTypes = paramTypes;
    }

    public void addToBatch(Object[] parVals) {
        if (batchStmt == null) {
            throw new LameRuntimeException("no batch is started");
        }
        try {
            for (int i = 0; i < parVals.length; i++) {
                Object param = parVals[i];
                try {
                    if (param != null) {
                        if (paramTypes[i] == Types.DATE) {
                            batchStmt.setObject(i + 1, new Timestamp(((Date)param).getTime()));
                        } else
                            batchStmt.setObject(i + 1, param, paramTypes[i]);
                    } else {
                        batchStmt.setNull(i + 1, paramTypes[i]);
                    }
                } catch (Exception ex) {
                    throw new LameRuntimeException("error setting param number: " +
                            i + ", value=\"" + param + "\", class=" + 
                            LameUtils.safeGetClassName(param) + ", paramType=" + 
                            paramTypes[i], ex);
                }
            }
            batchStmt.addBatch();
        } catch (Exception ex) {
            throw new LameRuntimeException("error adding to batch for sql: \"" +
                    batchTxt + "\" and param values: " +
                    Arrays.toString(parVals), ex);
        }
    }

    public void execBatch() {
        try {
            batchStmt.executeBatch();
            conn.commit();
        } catch (Exception ex) {
            throw new LameRuntimeException("error executing batch for sql: \"" +
                    batchTxt + "\"", ex);
        }
        clearBatch();
    }

    public void clearBatch() {
        try {
            if (batchStmt != null) {
                batchStmt.clearBatch();
                batchStmt.close();
                conn.rollback();
            }
            //conn.setAutoCommit(true);
        } catch (Exception ex) {
            throw new LameRuntimeException("error clearing batch for sql: \"" +
                    batchTxt + "\"", ex);
        }
        batchStmt = null;
        batchTxt = null;
    }
}
