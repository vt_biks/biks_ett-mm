/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author beata
 */
public class AttachmentsRolesWidgetFlat extends UserInNodeWidgetFlatBase {

    @Override
    public String getTreeKindForSelected() {
        return BIKGWTConstants.TREE_KIND_DOCUMENTS;
    }

    @Override
    public String getCaption() {
        return /*BIKGWTConstants.MENU_NAME_KNOWLEDGE  + "<br>" +*/ BIKGWTConstants.MENU_NAME_DOCUMENTS;
    }

    @Override
    protected String textWhenNoItems() {
        return I18n.brakDokumentacji.get() /* I18N:  */;
    }
}
