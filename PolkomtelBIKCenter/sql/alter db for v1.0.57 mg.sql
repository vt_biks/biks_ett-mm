﻿exec sp_check_version '1.0.57';
go

alter table bik_node_kind
add tree_kind varchar(255)
go

update bik_node_kind
set tree_kind = 'Metadata' 
where code in ('MetadataFolder', 'DataConnection', 'Webi', 'Flash', 'CristalReports', 'Universe',
               'UniverseClass', 'UniverseObject', 'UniverseTable', 'UniverseColumn',
               'Schema', 'Table', 'View', 'Column', 'Procedure')
go

update bik_node_kind
set tree_kind = 'Taxonomy' where code in ('TaxonomyEntity')
go

update bik_node_kind
set tree_kind = 'Tezaurus' where code in ('Tezaurus', 'TezaurusCategory', 'Keyword')
go

update bik_node_kind
set tree_kind = 'Documents' where code in ('Document', 'DocumentsFolder', 'DocumentsDefaultFolder')
go

update bik_node_kind
set tree_kind = 'Users' where code in ('UsersGroup', 'User')
go

update bik_node_kind
set tree_kind = 'Blogs' where code in ('BlogEntry', 'Blog')
go

alter table bik_node_kind
alter column tree_kind varchar(255) not null
go

update bik_node_kind
set code = 'TeradataSchema' where code = 'Schema'
go

update bik_node_kind
set code = 'TeradataTable' where code = 'Table'
go

update bik_node_kind
set code = 'TeradataView' where code = 'View'
go

update bik_node_kind
set code = 'TeradataColumn' where code = 'Column'
go

update bik_node_kind
set code = 'TeradataProcedure' where code = 'Procedure'
go

alter table bik_node_kind
add is_folder int default 0 not null
go

update bik_node_kind
set is_folder = 1
where code in ('MetadataFolder', 'TaxonomyEntity', 'TezaurusCategory', 'DocumentsFolder', 'DocumentsDefaultFolder', 'UsersGroup', 'Blog')
go

exec sp_update_version '1.0.57', '1.0.58';
go
 