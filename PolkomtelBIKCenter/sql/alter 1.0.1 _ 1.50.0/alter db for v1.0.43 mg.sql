exec sp_check_version '1.0.43';

if not exists (
  select * from bik_role_for_node
  where caption = 'Odpowiedzialny'
)
begin
  insert into bik_role_for_node(caption)
  values('Odpowiedzialny')
end
go

delete from bik_user_in_node
from
  bik_user_in_node uin
  inner join bik_role_in_node_kind rnk on rnk.id = uin.role_in_kind_id
  inner join bik_node_kind nk on nk.id = rnk.node_kind_id
where
  nk.code not in ('MetaData.DataConnection', 'Webi', 'Flash', 'CristalReports', 'Universe', '_BIZAREA_', 'SCHEMA', 'TABLE', 'COLUMN', 'VIEW', 'PROCEDURE', 'Tezaurus', 'Document')
go

delete from bik_user_in_node
from
  bik_user_in_node uin
  inner join bik_role_in_node_kind rnk on rnk.id = uin.role_in_kind_id
  inner join bik_node_kind nk on nk.id = rnk.node_kind_id
  inner join bik_role_for_node rfn on rfn.id = rnk.role_id
where
  nk.code in ('MetaData.DataConnection', 'Webi', 'Flash', 'CristalReports', 'Universe', '_BIZAREA_', 'SCHEMA', 'TABLE', 'COLUMN', 'VIEW', 'PROCEDURE')
  and rfn.caption = 'Odpowiedzialny'
go

delete from bik_user_in_node
from
  bik_user_in_node uin
  inner join bik_role_in_node_kind rnk on rnk.id = uin.role_in_kind_id
  inner join bik_node_kind nk on nk.id = rnk.node_kind_id
  inner join bik_role_for_node rfn on rfn.id = rnk.role_id
where
  nk.code in ('Tezaurus', 'Document')
  and rfn.caption != 'Odpowiedzialny'
go

delete from bik_user_in_node
from
  bik_user_in_node uin
  inner join bik_role_in_node_kind rnk on rnk.id = uin.role_in_kind_id
  inner join bik_node_kind nk on nk.id = rnk.node_kind_id
  inner join bik_role_for_node rfn on rfn.id = rnk.role_id
where
  nk.code = '_BIZAREA_'
  and rfn.caption not in ('Sponsor', 'Ekspert BI')
go

delete from bik_role_in_node_kind
from
  bik_role_in_node_kind rnk
  inner join bik_node_kind nk on nk.id = rnk.node_kind_id
  inner join bik_role_for_node rfn on rfn.id = rnk.role_id
where
  nk.code not in ('MetaData.DataConnection', 'Webi', 'Flash', 'CristalReports', 'Universe', '_BIZAREA_', 'SCHEMA', 'TABLE', 'COLUMN', 'VIEW', 'PROCEDURE', 'Tezaurus', 'Document')
go

delete from bik_role_in_node_kind
from
  bik_role_in_node_kind rnk
  inner join bik_node_kind nk on nk.id = rnk.node_kind_id
  inner join bik_role_for_node rfn on rfn.id = rnk.role_id
where
  nk.code in ('MetaData.DataConnection', 'Webi', 'Flash', 'CristalReports', 'Universe', '_BIZAREA_', 'SCHEMA', 'TABLE', 'COLUMN', 'VIEW', 'PROCEDURE')
  and rfn.caption = 'Odpowiedzialny'
go

delete from bik_role_in_node_kind
from
  bik_role_in_node_kind rnk
  inner join bik_node_kind nk on nk.id = rnk.node_kind_id
  inner join bik_role_for_node rfn on rfn.id = rnk.role_id
where
  nk.code in ('Tezaurus', 'Document')
  and rfn.caption != 'Odpowiedzialny'
go

delete from bik_role_in_node_kind
from
  bik_role_in_node_kind rnk
  inner join bik_node_kind nk on nk.id = rnk.node_kind_id
  inner join bik_role_for_node rfn on rfn.id = rnk.role_id
where
  nk.code = '_BIZAREA_'
  and rfn.caption not in ('Sponsor', 'Ekspert BI')
go

declare @nk_codes varchar(200)
declare @nk_codes_cnt int
declare @nk_code varchar(20)
declare @nk_codes_ind int
declare @rfn_captions varchar(200)
declare @rfn_captions_cnt int
declare @rfn_caption varchar(20)
declare @rfn_captions_ind int
set @nk_codes = 'MeataData.DataConnection,Webi,Flash,CristalReports,Universe,_BIZAREA_,SCHEMA,TABLE,COLUMN,VIEW,PROCEDURE,Tezaurus,Document,'
set @nk_codes_cnt = 1
while (@nk_codes_cnt = 1)
begin
  set @nk_codes_ind = charindex(',', @nk_codes)
  if (@nk_codes_ind > 0)
  begin
    set @nk_code = substring(@nk_codes, 1, @nk_codes_ind - 1)
    set @nk_codes = substring(@nk_codes, @nk_codes_ind + 1, len(@nk_codes) - @nk_codes_ind)
    set @rfn_captions = 'Specjalista IT,Sponsor,Ekspert BI,Odpowiedzialny,'
    set @rfn_captions_cnt = 1
    while (@rfn_captions_cnt = 1)
    begin
      set @rfn_captions_ind = charindex(',', @rfn_captions)
      if (@rfn_captions_ind > 0)
      begin
        set @rfn_caption = substring(@rfn_captions, 1, @rfn_captions_ind - 1)
        set @rfn_captions = substring(@rfn_captions, @rfn_captions_ind + 1, len(@rfn_captions) - @rfn_captions_ind)
        if ((@nk_code not in ('_BIZAREA_', 'Tezaurus', 'Document') and @rfn_caption != 'Odpowiedzialny')
            or (@nk_code = '_BIZAREA_' and @rfn_caption in ('Sponsor', 'Ekspert BI'))
            or (@nk_code in ('Tezaurus', 'Document') and @rfn_caption = 'Odpowiedzialny'))
        begin 
          if not exists (
            select * from
              bik_role_in_node_kind rnk
              inner join bik_node_kind nk on nk.id = rnk.node_kind_id
              inner join bik_role_for_node rfn on rfn.id = rnk.role_id
            where
              nk.code = @nk_code and rfn.caption = @rfn_caption
          )
          begin
            insert into bik_role_in_node_kind(node_kind_id, role_id)
            select nk.id, rfn.id
            from
              bik_role_for_node rfn,
              bik_node_kind nk
              where
                rfn.caption = @rfn_caption
                and nk.code = @nk_code
          end
        end
      end
      else
      begin
        set @rfn_captions_cnt = 0
      end
    end
  end
  else
  begin
    set @nk_codes_cnt = 0
  end
end
go
    

exec sp_update_version '1.0.43', '1.0.44';
go
