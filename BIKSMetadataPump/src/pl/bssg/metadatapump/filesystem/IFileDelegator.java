/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.filesystem;

import java.io.InputStream;

/**
 *
 * @author ctran
 */
public interface IFileDelegator {

    public boolean isDirectory();

    public Integer getFileCount();

    public IFileDelegator[] listFiles();

    public String getPath();

    public String getFolderName();

    public IFileDelegator[] listFiles(String wildcard);

    public long lastModified();

    public String getName();

    public long length();

    public InputStream getInputStream();
}
