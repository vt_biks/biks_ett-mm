/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.util.Map;

/**
 *
 * @author wezyr
 */
public interface IRequestContext {

    // GET / POST (uppercased!) but might be null or "" (for dummy reqCtxes)
    public String getRequestMethod();

    public String encodeUrl(String url);

    public String getServletUrlBase(boolean forRemoteURL);

    public String getResourceUrlBase(boolean forRemoteURL);

    // might be null
    public Map<String, Object> getParams();

    public boolean getUsesSubdomains();
}
