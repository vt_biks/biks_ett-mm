    select
    bn.node_kind_id, bnk.caption,
    count(*) as cnt
    from (
    
    
    


    select bn.*, 
    sr.sum_combined_rank, sr.max_combined_rank, sr.sum_search_weight, sr.max_search_weight
    
    from
    bik_node bn
    
        inner join
        (select node_id,
        sum(sr.[rank] * coalesce(ap.search_weight, 1)) as sum_combined_rank,
        max(sr.[rank] * coalesce(ap.search_weight, 1)) as max_combined_rank,
        sum(coalesce(ap.search_weight, 1)) as sum_search_weight,
        max(coalesce(ap.search_weight, 1)) as max_search_weight
        from bik_searchable_attr_val av inner join
        containstable(bik_searchable_attr_val, *, '"id" AND "konto"') sr on av.id = sr.[key]
        inner join bik_searchable_attr a on av.attr_id = a.id
        left join bik_searchable_attr_props ap on a.name = ap.name
        group by node_id) sr on sr.node_id = bn.id
    
    where bn.is_deleted = 0 and (bn.linked_node_id is null or bn.disable_linked_subtree <> 0)
    and bn.tree_id in (1,2)
    and  bn.node_kind_id in (39,56)


    ) as bn inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
    group by
    bn.node_kind_id, bnk.caption
    order by bnk.caption
