/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib.connectionpool;

import java.sql.Connection;

/**
 *
 * @author wezyr
 */
public interface IConnectionPoolManager {

    public void markConnectionAsInvalid();

    public Connection getConnection();

    // to jest szybkie gdy thread nie bra� connectiona po nic
    public void recycleThreadConnection(boolean doCommit);

    public void dispose();

    //ww: zb�dne
//    public boolean hasConnectionInUse();
}
