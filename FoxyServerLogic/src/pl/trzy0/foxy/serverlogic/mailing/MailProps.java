/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.mailing;

import java.util.Map;

/**
 *
 * @author wezyr
 */
public class MailProps {

    public String recipients[];
    public String cc[];
    public String subject;
    public String message;
    public Map<String, String> embeddedFiles;
    public Map<String, byte[]> embeddedByteArrays;

    public MailProps(String recipients[], String subject, String message) {
        this(recipients, new String[]{}, subject, message, null, null);
    }

    public MailProps(String recipients[], String[] cc, String subject, String message,
            Map<String, String> embeddedFiles, Map<String, byte[]> embeddedByteArrays) {
        this.recipients = recipients;
        this.cc = cc;
        this.subject = subject;
        this.message = message;
        this.embeddedFiles = embeddedFiles;
        this.embeddedByteArrays = embeddedByteArrays;
    }
}
