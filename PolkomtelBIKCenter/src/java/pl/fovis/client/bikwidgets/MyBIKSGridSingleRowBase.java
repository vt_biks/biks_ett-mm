/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import simplelib.BaseUtils;

/**
 *
 * @author bfechner
 */
public abstract class MyBIKSGridSingleRowBase<T> extends MyBIKSGridBase<T> {

    protected int rowIdx;

    @Override
    public Widget addItemsToGrid(List<T> itemsToDisplay) {
        activeTab.clear();
        gp.clear();
        activeTab.setWidth("100%");

        int newsCnt = BaseUtils.collectionSizeFix(itemsToDisplay);
        if (newsCnt > 0) {
            gp.setWidth("100%");
            gp.setStyleName("gridJoinedObj");
            int row = 0;
            if (isHeaderRowVisible()) {
                addHeaderRow();
                row = 1;
            }
            rowIdx = 0;
            for (T r : itemsToDisplay) {
                //ww->mybiks: wyczuwam złożoność n^2 - jeżeli wyświetlamy 1000 pozycji
                // (itemsToDisplay) to poniższe indexOf(r) będzie działać średnio w czasie 500 operacji
                // (1/2 n, bo złożoność O(n)) czyli razem 1000 * 500 = 500 000 operacji dla 1000 pozycji
                // ...litości, pliz. coś mocno koncepcyjnie poszło nie tak.
//                gp.setWidget(row, 0, new HTML(firstItemToDisplayIdx + row + " "));
                //ww->mybiks: a teraz ciekawostka, czy poniższe wywołanie nie nadpisze
                // tego pracowicie wyciąganego numerka (taka złożoność wyciągania i co?
                // i jak krew w piach?) bo chyba czasem numerka wiersza jednak nie widać...?
                createRowFromBean(row, r);
                //ww->mybiks: czy konieczne jest wołanie poniższego dla każdego
                // dodawanego wiersza? i po co przekazujemy tu gp, które jest
                // protected, a więc dostępne swobodnie w podklasach?

                row++;
                rowIdx++;
            }
            setUpGridColumnsWidths(gp);
            activeTab.add(gp);
        }
        return activeTab;
    }

    protected boolean isHeaderRowVisible() {
        return false;
    }

    protected void addHeaderRow() {
        //no-op
    }

    //ww: liczone względem allItems (czyli uwzględnia paginację)
    // globalny indeks aktualnego wiersza do wyświetlenia
    // do używania wewnątrz wywołania addRowToItem
    protected int getCurrentRowGlobalIdx() {
        return firstItemToDisplayIdx + rowIdx;
    }
}
