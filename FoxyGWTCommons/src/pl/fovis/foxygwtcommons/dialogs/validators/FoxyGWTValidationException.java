/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.dialogs.validators;

/**
 *
 * @author pmielanczuk
 */
public class FoxyGWTValidationException extends Exception {

    public FoxyGWTValidationException(String msg) {
        super(msg);
    }
}
