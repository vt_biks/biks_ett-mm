/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pl.trzy0.foxy.serverlogic.FoxyUrlRewriter;
import pl.trzy0.foxy.serverlogic.IApplicationEnvironment;
import pl.trzy0.foxy.serverlogic.IFoxyService;
import simplelib.MapHelper;

/**
 *
 * @author wezyr
 */
public interface IBundleDerivedLogic {

    public void init(FoxyMillBundleData bundleData,
            FoxyUrlRewriter urlRewriter, IApplicationEnvironment appEnv);

    public Object createPageModel(HttpServletRequest request, HttpServletResponse response,
            IFoxyService service,
            String pageName, MapHelper<String> params);

    public Iterable<String> getSupportedClientLangs();

    public String getFallThroughLoginUrl();
}
