﻿exec sp_check_version '1.2.3.1';
go


create function fn_string_counter(@expressionToFind char(1), @expressionToSearch varchar(1000))
returns int
as
begin
	declare @position int, @counter int
	-- Initialize the current position and counter.
	set @position = 1
	set @counter = 0

	set @position = charindex(@expressionToFind, @expressionToSearch, @position)
	while @position>0
	   begin
	   set @counter = @counter + 1
	   set @position = charindex(@expressionToFind, @expressionToSearch, @position+1)
	   end

	return @counter
end
go

create function fn_take_branch_from_branches(@expressionToFind char(1), @expressionToSearch varchar(1000), @branchNo int)
returns varchar(255)
as
begin
	
	if @branchNo <=0
		return ''

	declare @positionCurr int, @positionPrev int, @positionBeg int,@positionEnd int, @counter int
	-- Initialize the current position and counter.
	set @positionCurr = 0
	set @positionBeg = 0
	set @positionEnd = 0
	set @positionPrev = 0
	set @counter = 0
	set @branchNo = @branchNo -1
	
	set @positionCurr = charindex(@expressionToFind, @expressionToSearch, @positionCurr)
	while @positionCurr>0
	   begin
		   if @counter=@branchNo 
			 begin
				set @positionBeg = @positionPrev+1
				set @positionEnd = @positionCurr
			 end --@counter=@branchNo

		   set @counter = @counter + 1
		   set @positionPrev = @positionCurr
		   set @positionCurr = charindex(@expressionToFind, @expressionToSearch, @positionCurr+1)
	   end --while end
	
	if @counter=@branchNo 
	  begin
		set @positionBeg = @positionPrev+1
		set @positionEnd = len(@expressionToSearch)+1
	  end

	return substring(@expressionToSearch,@positionBeg, @positionEnd-@positionBeg)
end
go


create table bik_statistic_ext(
	tree_id int not null references bik_tree(id),
	clicked_node_id int not null references bik_node(id),
	user_id int NOT NULL references bik_system_user(id),
	event_date date not null default convert( date, sysdatetime()),
	event_datetime datetime not null default sysdatetime(),
	level_in_tree int not null,
	grouping_node_id int
) 
go

alter table bik_statistic_ext add  constraint pk_bik_statistic_ext primary key clustered 
(tree_id, clicked_node_id, user_id, event_datetime)
go


--ok
create table bik_object_excluded_from_statistic (
	node_id int not null primary key references bik_node(id),
)
go

--foldery tylko dla Polkomtela, więc wyciągane po name(niestety)
insert into bik_object_excluded_from_statistic
	select id from bik_node 
	where
		tree_id = (select id from bik_tree where code = 'Reports')
		and
		node_kind_id = (select id from bik_node_kind where code = 'ReportFolder')
		and 
		parent_node_id is null
		and 
		is_deleted = 0
		and name = 'Foldery użytkowników'
go

insert into bik_object_excluded_from_statistic
select id from bik_node 
	where
		tree_id = (select id from bik_tree where code = 'DQC')
		and
		node_kind_id = (select id from bik_node_kind where code = 'DQCAllTestsFolder')
		and 
		parent_node_id is null
		and 
		is_deleted = 0 and is_built_in = 1
		and name = 'Wszystkie testy'
go

insert into bik_object_excluded_from_statistic
select id from bik_node 
	where
		tree_id = (select id from bik_tree where code = 'BWReports')
		and
		node_kind_id = (select id from bik_node_kind where code = 'BWArea')
		and 
		parent_node_id is null
		and 
		is_deleted = 0 
		and name = '(Archiwum)'
go

insert into bik_object_excluded_from_statistic
select id from bik_node 
	where
		tree_id = (select id from bik_tree where code = 'BWProviders')
		and
		node_kind_id = (select id from bik_node_kind where code = 'BWArea')
		and 
		parent_node_id is null
		and 
		is_deleted = 0 
		and name = '(Archiwum)'
go

--to może nie mieć sensu, jeśli is_leaf_for_statistic może być jednoznaczne z !is_folder, ale 
--być może inne liście będą ustalone!!
alter table bik_node_kind
 add is_leaf_for_statistic int not null default 0 
go
--update bik_node_kind set is_leaf_for_statistic = 1
--	where is_folder=0

update bik_node_kind set is_leaf_for_statistic = 1
  where code in( 'Glossary' )


--2a -Biblioteki, SAP BO Raporty
update bik_node_kind set is_leaf_for_statistic = 1
  where code in('Webi','CrystalReport','FullClient' )

--2a -Biblioteki, SAP BO "Światy obiektów"
update bik_node_kind set is_leaf_for_statistic = 1
  where code in('Universe' )

--2a -Biblioteki, SAP BO "połączenia"
update bik_node_kind set is_leaf_for_statistic = 1
  where code in('DataConnection' )

--2b bazy danych
update bik_node_kind set is_leaf_for_statistic = 1
  where code in( 'TeradataTable','TeradataView','TeradataProcedure' )

--2c DQC
update bik_node_kind set is_leaf_for_statistic = 1
  where code in( 'DQCTestSuccess','DQCTestFailed','DQCTestInactive' )

--2d "SAP BW"->"Raporty"
update bik_node_kind set is_leaf_for_statistic = 1
  where code in( 'BWBEx')

--2d "SAP BW"->"Dostawcy informacji"
update bik_node_kind set is_leaf_for_statistic = 1
  where code in( 'BWMPRO','BWCUBE','BWISET','BWODSO')

--3a "Bazy Wiedzy" -> "Dokumantacja"
update bik_node_kind set is_leaf_for_statistic = 1
  where code in( 'Document','Article')
 
--3b "Bazy Wiedzy" -> Drzewa typu kategorzacja
update bik_node_kind set is_leaf_for_statistic = 1
  where code in( 'TaxonomyEntity')

 
--4a "Społeczności BI" -> "Blogi "
update bik_node_kind set is_leaf_for_statistic = 1
  where code in( 'BlogEntry')

--4b "Użytkownicy"
update bik_node_kind set is_leaf_for_statistic = 1
  where code in( 'User')


--update bik_node_kind set is_leaf_for_statistic = 0
--	where code in( 'GlossaryVersion', 'GlossaryNotCorpo' )

--poziom na którym będą liczone statystyki dla konkretnego drzewa
alter table bik_tree
 add branch_level_for_statistics int not null default 1
go

update bik_tree set branch_level_for_statistics = 2 where code = 'Reports'
update bik_tree set branch_level_for_statistics = 2 where code = 'ObjectUniverses'
update bik_tree set branch_level_for_statistics = 0 where code = 'Connections'
update bik_tree set branch_level_for_statistics = 0 where code = 'Documents'
update bik_tree set branch_level_for_statistics = 2 where code = 'BWReports'
update bik_tree set branch_level_for_statistics = 2 where code = 'BWProviders'
go

create procedure sp_get_all_statistics_ext_dict as
begin
	 set nocount on

    create table #tmp
    (
            counter_name varchar(255),
            description varchar(255),
            best_parameter varchar(255),
            day_cnt int,
            week_cnt int,
            thirty_cnt int,
            all_cnt int,
            total int
    )

    declare @treeId int
    declare @counterName varchar(255)
    declare @description varchar(255)
    declare @bestParameter varchar(255)
    declare @cnt1 int, @cnt7 int, @cnt30 int, @cntAll int, @total int 

    declare tree_to_statistics_curs cursor for 
            select bt.id, bsd.counter_name, bsd.description
                    from bik_statistic_dict bsd
                    left join bik_tree bt on bsd.counter_name = bt.code
                    where 
                     bt.is_hidden = 0

    open tree_to_statistics_curs
    fetch next from tree_to_statistics_curs into @treeId, @counterName, @description

    while @@fetch_status = 0
            begin
                    with table_stat
                    as
                    (
                            select bse.grouping_node_id, bn.name, count(*) as all_cnt, 
                            sum(case when datediff(d, bse.event_date, sysdatetime()) <= 1 then 1 else 0 end) as day_cnt, 
                            sum(case when datediff(d, bse.event_date, sysdatetime())  <= 7 then 1 else 0 end) as week_cnt,
                            sum(case when datediff(d, bse.event_date, sysdatetime())  <= 30 then 1 else 0 end) as thirty_cnt
                            from bik_statistic_ext bse
                            left join bik_node bn on bn.id = bse.grouping_node_id
                            where bse.tree_id = @treeId
                            group by bse.tree_id, bse.grouping_node_id, bn.name
                    )
                    select top 1 @bestParameter=name, @cntAll=all_cnt, @cnt1=statGroup.day_cnt, 
                    @cnt7=statGroup.week_cnt, @cnt30=statGroup.thirty_cnt, @total=statGroup.total
                    from table_stat,
                    (select sum(day_cnt) day_cnt, sum(week_cnt) week_cnt, sum(thirty_cnt) thirty_cnt , sum( all_cnt) total from table_stat) as statGroup
                    order by table_stat.all_cnt desc
                    --select top 1 @bestParameter=name, @cnt1=day_cnt, @cnt7=week_cnt, @cnt30=thirty_cnt, @cntAll=all_cnt 
                    --,@total=(select sum( all_cnt) from table_stat) 
                    --from table_stat order by all_cnt desc			

                    insert into #tmp(counter_name, description, best_parameter, day_cnt, week_cnt, thirty_cnt, all_cnt, total)
                            values(@counterName, @description, @bestParameter, @cnt1, @cnt7, @cnt30, @cntAll, @total )

                    fetch next from tree_to_statistics_curs into @treeId, @counterName, @description
            end	

    close tree_to_statistics_curs;
    deallocate tree_to_statistics_curs


	declare tree_to_statistics_curs cursor for 
            select bt.id, bsd.counter_name, bsd.description
                    from bik_statistic_dict bsd
                    left join bik_tree bt on bsd.counter_name = bt.code
                    where 
                     bt.id is null

    open tree_to_statistics_curs
    fetch next from tree_to_statistics_curs into @treeId, @counterName, @description

    while @@fetch_status = 0
            begin

                    with table_stat
                    as
                    (
							select bs.parametr as name,
							count(*) as all_cnt,
							sum(case when datediff(D, bs.event_date, sysdatetime()) <= 1 then 1 else 0 end) as day_cnt, 
							sum(case when datediff(D, bs.event_date, sysdatetime())  <= 7 then 1 else 0 end) as week_cnt,
							sum(case when datediff(D, bs.event_date, sysdatetime())  <= 30 then 1 else 0 end) as thirty_cnt
							from 
							bik_statistic bs 
							left join bik_tree bt on bs.counter_name = bt.code  
							where 
							bs.counter_name = @counterName and (bt.id is null or bt.is_hidden = 0)
							group by bs.parametr 
                            
                    )
                    select top 1 @bestParameter=name, @cntAll=all_cnt, @cnt1=statGroup.day_cnt, 
                    @cnt7=statGroup.week_cnt, @cnt30=statGroup.thirty_cnt, @total=statGroup.total
                    from table_stat,
                    (select sum(day_cnt) day_cnt, sum(week_cnt) week_cnt, sum(thirty_cnt) thirty_cnt , sum( all_cnt) total from table_stat) as statGroup
                    order by table_stat.all_cnt desc

                    insert into #tmp(counter_name, description, best_parameter, day_cnt, week_cnt, thirty_cnt, all_cnt, total)
                            values(@counterName, @description, @bestParameter, @cnt1, @cnt7, @cnt30, @cntAll, @total )

                    fetch next from tree_to_statistics_curs into @treeId, @counterName, @description
            end	

    close tree_to_statistics_curs;
    deallocate tree_to_statistics_curs
    
    select * from #tmp
end
go

alter table bik_statistic alter column counter_name varchar(255) not null
go

create index idx_bik_statistic_counter_name on bik_statistic (counter_name)
go

insert into bik_statistic_dict(counter_name,description)
	values('userLoginOnce','Liczba użytkowników w systemie' )
go

insert into bik_statistic(counter_name,user_id,event_date,event_datetime)
	select 'userLoginOnce', user_id, min(event_date), min(event_datetime)  
	from bik_statistic where counter_name in('userLogin' )
	group by counter_name, user_id
go

exec sp_update_version '1.2.3.1', '1.2.3.2';
go
