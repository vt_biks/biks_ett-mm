/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.common.ConnectionParametersDQMBean;
import pl.fovis.common.ConnectionParametersDQMConnectionsBean;
import pl.fovis.common.NoteBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.dqm.DQMDataErrorIssueBean;
import pl.fovis.common.dqm.DQMEvaluationBean;
import pl.fovis.common.dqm.DQMLogBean;
import pl.fovis.common.dqm.DQMScheduleBean;
import pl.fovis.common.dqm.DataQualityTestBean;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public interface DQMServiceAsync {

    public void addDQMTest(DataQualityTestBean bean, int parentNodeId,/* boolean isAddedToAllTestFolder,*/ AsyncCallback<Integer> asyncCallback);

    public void assignDQMTestToGroup(int testId, int groupId, String name, AsyncCallback<Integer> asyncCallback);

    public void editDQMTest(DataQualityTestBean bean, AsyncCallback<Void> asyncCallback);

    public void getDQMTestsTreeNodes(Integer optUpSelectedNodeId, AsyncCallback<List<TreeNodeBean>> asyncCallback);

    public void deleteDQMTest(TreeNodeBean node, AsyncCallback<List<String>> asyncCallback);

    public void changeDQMTestActivity(int nodeId, boolean deactivateTest, AsyncCallback<Void> asyncCallback);

    public void getAlreadyAssignedDQMTest(int nodeId, AsyncCallback<Set<Integer>> asyncCallback);

    public void getDQMTestsNameAndOptConnections(AsyncCallback<Pair<Set<String>,Map<Integer, ConnectionParametersDQMConnectionsBean>>> asyncCallback);

    public void getBikAllRootTreeNodesForTreeCodes(Set<String> treeCodes, AsyncCallback<List<TreeNodeBean>> asyncCallback);

    public void runDQMTest(int testId, AsyncCallback<Void> asyncCallback);

    public void getDQMTests(AsyncCallback<List<DataQualityTestBean>> asyncCallback);

    public void getDQMLogs(int pageNum, int pageSize, AsyncCallback<Pair<Integer, List<DQMLogBean>>> asyncCallback);

    public void getDQMConnectionParameters(AsyncCallback<ConnectionParametersDQMBean> asyncCallback);

    public void setDQMConnectionParameters(ConnectionParametersDQMBean bean, AsyncCallback<Void> asyncCallback);

    public void getDQMSchedules(int pageNum, int pageSize, AsyncCallback<Pair<Integer, List<DQMScheduleBean>>> asyncCallback);

    public void setDQMSchedules(List<DQMScheduleBean> schedules, AsyncCallback<Void> asyncCallback);

    public void getEvaluationPreInfo(int nodeId, AsyncCallback<DQMEvaluationBean> asyncCallback);

    public void saveEvaluation(DQMEvaluationBean bean, AsyncCallback<Integer> asyncCallback);

    public void setDQMEvaluationStatus(int nodeId, NoteBean comment, AsyncCallback<Void> asyncCallback);

    public void generateDQMReport(int nodeId, Date fromDate, Date toDate, AsyncCallback<Integer> asyncCallback);

    public void refreshDQMReport(int nodeId, AsyncCallback<Void> asyncCallback);

    public void getEvaluationPeriods(int nodeId, AsyncCallback<List<DQMEvaluationBean>> asyncCallback);

    public void saveDataErrorIssue(DQMDataErrorIssueBean bean, AsyncCallback<Integer> asyncCallback);

    public void getDQMDataErrorIssuePreInfo(int nodeId, AsyncCallback<DQMDataErrorIssueBean> asyncCallback);

    public void getDQMDataErrorIssueByNodeId(int nodeId, AsyncCallback<DQMDataErrorIssueBean> asyncCallback);

    public void prepareFileToDownload(String exportType, Long requestId, AsyncCallback<Pair<String, String>> asyncCallback);
}
