/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.menu;

import pl.fovis.client.bikpages.IBikPage;

/**
 *
 * @author wezyr
 */
public class MenuItemMainWidget implements IMenuItemMainWidget {

    protected IBikPage<Integer> tab;
    protected String caption;

    public MenuItemMainWidget(String caption, IBikPage<Integer> tab) {
        this.tab = tab;
        this.caption = caption;
    }

    public String getCaption() {
        return caption; //getTab().getCaption();
    }

    public IBikPage<Integer> getTab() {
        return tab;
    }
}
