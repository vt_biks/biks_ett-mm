/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package commonlib;

import simplelib.LameRuntimeException;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 *
 * @author wezyr
 */
public class StringIteratorFromReader implements Iterator<String> {
    private BufferedReader r;
    private String nextLine;
    
    private void readNextLine() {
        try {
            nextLine = r.readLine();
        } catch (IOException ex) {
            throw new LameRuntimeException("error while reading line", ex);
        }
    }
    
    public StringIteratorFromReader(BufferedReader r) {
        this.r = r;
        readNextLine();
    }

    public boolean hasNext() {
        return nextLine != null;
    }

    public String next() throws NoSuchElementException {
        if (!hasNext()) {
            throw new NoSuchElementException("no (more) elements");
        }
        
        String res = nextLine;
        readNextLine();

        return res;
    }

    public void remove() {
        throw new UnsupportedOperationException("readonly iterator, sorry for this ;-)");
    }
}
