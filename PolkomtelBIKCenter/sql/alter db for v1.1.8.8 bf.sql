﻿exec sp_check_version '1.1.8.8';
go
--drop table bik_node_author
create table bik_node_author(
	id int identity not null primary key,
	user_id int not null references bik_system_user(id),
	node_id int not null references bik_node(id),
	constraint UQ_user_id_node_id unique(user_id, node_id)
)
go

alter table bik_tree
add is_hidden int default 0 not null
go

update bik_tree set is_hidden = 1 where code = 'Articles' or code = 'MSSQL'

exec sp_update_version '1.1.8.8', '1.1.8.9';
go