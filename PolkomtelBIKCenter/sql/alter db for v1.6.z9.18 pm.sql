﻿exec sp_check_version '1.6.z9.18';
go

--------------------------------------
--------------------------------------
--------------------------------------

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_attr_def') and name = 'visual_order')
  alter table bik_attr_def add visual_order int not null default 0;
go


-- specyficzne dla PB
update 
-- select * from
bik_attr_def 
set visual_order = -1
where name = 'Klasyfikacja SI'
;
	
--------------------------------------
--------------------------------------
--------------------------------------

exec sp_update_version '1.6.z9.18', '1.6.z9.19';
go
