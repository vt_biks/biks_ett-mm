/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author tflorczak
 */
public class DBDependencyBean implements Serializable {

    public String name;
    public String owner;
    public String treeCode;
    public int nodeId;
    public String type;
    public String typeAsIconName;
}
