/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.drools;

import commonlib.LameUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import static pl.bssg.metadatapump.ThreadedPumpExecutor.MILLIS_FOR_MINUTE;
import pl.fovis.common.DroolsConfigBean;
import pl.fovis.server.BIKSThreadedWorker;
import pl.fovis.server.BOXIServiceImpl;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import simplelib.BaseUtils;
import simplelib.Pair;
import simplelib.SimpleDateUtils;
import simplelib.logging.ILameLogger;

/**
 *
 * @author ctran
 */
public class ThreadedDroolsEngine extends BIKSThreadedWorker {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected INamedSqlsDAO<Object> adhocDao;
    protected BOXIServiceImpl service;
    protected DroolsEngineWorker worker;
    protected IRulesManagementEngine engine;

    public void setService(BOXIServiceImpl service) {
        this.service = service;
    }

    public void setEngine(IRulesManagementEngine engine) {
        this.engine = engine;
    }

    public synchronized void schedule(final DroolsConfigBean droolsConfig) {
        destroy();
        if (worker != null) {
            worker.cancel();
        }
        if (droolsConfig.isActive != 1) {
            return;
        }
        worker = new DroolsEngineWorker(service, adhocDao, engine, droolsConfig);
        // Wykonac tylko w przypadku gdy isActive = 1
        timer = new Timer();
        SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd HH:mm");
        TimerTask task = new TimerTask() {

            @Override
            public void run() {
                jobExecutor.addJob(new Runnable() {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");

                    @Override
                    public void run() {
                        worker.doit();
                    }
                });
            }
        };
        int frequency = BaseUtils.tryParseInt(droolsConfig.frequency);
        Date dateSchedule = null;
        Date today = new Date();
        Pair<String, String> timeToStart = BaseUtils.splitString(droolsConfig.startHour, ":");

        if (!BaseUtils.isStrEmptyOrWhiteSpace(droolsConfig.startDate) || !BaseUtils.isStrEmptyOrWhiteSpace(droolsConfig.startHour)) {
            if (BaseUtils.isStrEmptyOrWhiteSpace(droolsConfig.startDate)) { //nie byl wprowadzony czas
                dateSchedule = SimpleDateUtils.newDate(SimpleDateUtils.getYear(today), SimpleDateUtils.getMonth(today), SimpleDateUtils.getDay(today), BaseUtils.tryParseInt(timeToStart.v1), BaseUtils.tryParseInt(timeToStart.v2));
            } else {
                try {
                    dateSchedule = sdf.parse(droolsConfig.startDate + " " + droolsConfig.startHour);
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
            }
        }
        if (dateSchedule != null) { //poprawnie wprowadzony czas
            if (frequency == 0) { //jednorazowy
                if (today.before(dateSchedule)) {
                    timer.schedule(task, dateSchedule);
                }
            } else {
                dateSchedule = SimpleDateUtils.newDate(SimpleDateUtils.getYear(today), SimpleDateUtils.getMonth(today), SimpleDateUtils.getDay(today), BaseUtils.tryParseInt(timeToStart.v1), BaseUtils.tryParseInt(timeToStart.v2));
                if (dateSchedule.before(today)) {
//                    dateSchedule = SimpleDateUtils.addDays(dateSchedule, 1);
                    dateSchedule = today;
                }
                timer.scheduleAtFixedRate(task, dateSchedule, MILLIS_FOR_MINUTE * frequency);
            }
        } else if (frequency == 0) {
            timer.schedule(task, today);
        }
    }

    public String tryCompile(DroolsConfigBean droolsConfig) {
        DroolsEngineWorker worker = new DroolsEngineWorker(service, adhocDao, engine, droolsConfig);
        return worker.tryCompile();
    }

    public void setAdhocDao(INamedSqlsDAO<Object> adhocDao) {
        this.adhocDao = adhocDao;
    }
}
