/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CellPanel;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import java.util.ArrayList;
import java.util.List;
import pl.bssg.metadatapump.common.ConnectionParametersPlainFileBean;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.DynamicTreesBigBean;
import pl.fovis.common.TreeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.trzy0.foxy.commons.FoxyRuntimeException;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuationWithReturn;
import simplelib.Pair;

/**
 *
 * @author ctran
 */
public class AdminConfigPlainFileWidget extends AdminConfigManyServersBase<List<ConnectionParametersPlainFileBean>, ConnectionParametersPlainFileBean> {

    protected CheckBox chbIsActive;
    protected CheckBox chbIsIncremental;
    protected CheckBox chbIsRemote;
    protected TextBox tbxUsername;
    protected TextBox tbxPw;
    protected TextBox tbxDomain;
    protected TextBox tbxSourceFolderPath;
    protected ListBox lbTreePath;
    protected List<TreeBean> allDynamicTrees;
    protected ConnectionParametersPlainFileBean activatedPlainFileBean;

    @Override
    protected void activateForServer(String id) {
        activatedPlainFileBean = serversMap.get(id);
        chbIsActive.setValue(activatedPlainFileBean.isActive);
        chbIsIncremental.setValue(activatedPlainFileBean.isIncremental > 0);
        chbIsRemote.setValue(activatedPlainFileBean.isRemote);
        tbxUsername.setText(activatedPlainFileBean.username);
        tbxPw.setText(activatedPlainFileBean.password);
        tbxDomain.setText(activatedPlainFileBean.domain);
        tbxSourceFolderPath.setText(activatedPlainFileBean.sourceFolderPath);
//        setSelectedTree();
        setWidgetsEnabled(activatedPlainFileBean.isActive);
        setAccountWidgetEnabled(chbIsRemote.getValue());
    }

    @Override
    protected void buildRightWidget(CellPanel panelRight) {
        chbIsActive = createCheckBox(I18n.aktywny.get(), new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setWidgetsEnabled(chbIsActive.getValue());
                setAccountWidgetEnabled(chbIsRemote.getValue());
            }
        }, false, panelRight);
        chbIsIncremental = createCheckBox(I18n.zasilaniePrzyrostowe.get(), null, true, panelRight);
        createLabel(I18n.sciezkiDoPlikow.get() + ":", panelRight);
        tbxSourceFolderPath = createTextBox(panelRight);
        chbIsRemote = createCheckBox(I18n.zdalnie.get(), new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setAccountWidgetEnabled(chbIsRemote.getValue());
            }
        }, true, panelRight);
        createLabel(I18n.uzytkownik.get() + ":", panelRight);
        tbxUsername = createTextBox(panelRight);
        createLabel(I18n.haslo.get() + ":", panelRight);
        tbxPw = createPasswordTextBox(panelRight);
//        chbPw = createAndAddCheckBox(I18n.ukryjHaslo.get(), new ValueChangeHandler<Boolean>() {
//            @Override
//            public void onValueChange(ValueChangeEvent<Boolean> event) {
//                tbxPw.getElement().setAttribute("type" /* I18N: no */, chbPw.getValue() ? "password" /* I18N: no */ : "text" /* I18N: no */);
//            }
//        }, panelRight);
        createLabel(I18n.domena.get() + ":", panelRight);
        tbxDomain = createTextBox(panelRight);
//        createLabel(I18n.przypisanaDo.get() + ":", panelRight);
//        lbTreePath = createListBoxEx(new HashSet<String>(), panelRight);
//        populateTreePathsListBox();
        createButton(I18n.zapiszZmiany.get(), new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (BaseUtils.isStrEmptyOrWhiteSpace(tbxPw.getText()) && chbIsRemote.getValue()) {
                    Window.alert(I18n.hasloNieMozeBycPuste.get());
                } else {
                    updateBean();
                    callServiceToSaveData(activatedPlainFileBean, new StandardAsyncCallback<Void>("Error in" /* I18N: no */ + " setPlainFileConnectionParameters: " + getSourceName()) {

                                @Override
                                public void onSuccess(Void result) {
                                    resultTest.setText("");
                                    listbox.setItemText(getSelectedIndex(), activatedPlainFileBean.name);
                                    BIKClientSingletons.showInfo(I18n.zmienionoParametryDoPolaczenia.get() /* I18N:  */ + ": " + getSourceName());
                                }

                                @Override
                                public void onFailure(Throwable caught) {
                                    super.onFailure(caught); //To change body of generated methods, choose Tools | Templates.
                                    if (caught instanceof FoxyRuntimeException) {
                                        BIKClientSingletons.showWarning(caught.getMessage());
                                    }
                                }
                            });
                }
            }

        }, panelRight);
        createTestConnectionWidget(panelRight, new IParametrizedContinuationWithReturn<Void, Integer>() {

            @Override
            public Integer doIt(Void param) {
                return activatedPlainFileBean.id;
            }
        });
        createTreeCodeTable(panelRight);
        panelRight.setWidth("100%");
    }

    private void updateBean() {
        activatedPlainFileBean.isActive = chbIsActive.getValue();
        activatedPlainFileBean.isIncremental = chbIsIncremental.getValue() ? 1 : 0;
        activatedPlainFileBean.username = tbxUsername.getValue();
        activatedPlainFileBean.password = tbxPw.getValue();
        activatedPlainFileBean.domain = tbxDomain.getValue();
        activatedPlainFileBean.isRemote = chbIsRemote.getValue();
        activatedPlainFileBean.sourceFolderPath = tbxSourceFolderPath.getValue();
//        activatedPlainFileBean.treeId = BaseUtils.tryParseInteger(lbTreePath.getSelectedValue(), null);
    }

    @Override
    protected void callServiceToAddNewServer(String source, String serverName, boolean needSeparateSchedule, AsyncCallback<ConnectionParametersPlainFileBean> asyncCallback) {
        BIKClientSingletons.getService().addPlainFileConfig(serverName, asyncCallback);
    }

    @Override
    protected void callServiceToDeleteServer(int id, String serverName, AsyncCallback<Void> asyncCallback) {
        BIKClientSingletons.getService().deletePlainFileConfig(getSourceName(), id, serverName, asyncCallback);
    }

    @Override
    protected void callServiceToTestConnection(int id, AsyncCallback<Pair<Integer, String>> asyncCallback) {
        BIKClientSingletons.getService().runPlainFileTestConnection(id, asyncCallback);
    }

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_PLAIN_FILE;
    }

    protected void callServiceToSaveData(ConnectionParametersPlainFileBean bean, AsyncCallback<Void> asyncCallback) {
        BIKClientSingletons.getService().setPlainFileConnectionParameters(bean, asyncCallback);
    }

    private void populateTreePathsListBox() {
        BIKClientSingletons.getService().getBikTreeForData(new StandardAsyncCallback<DynamicTreesBigBean>() {

            @Override
            public void onSuccess(DynamicTreesBigBean result) {
                allDynamicTrees = BIKClientSingletons.getDynamicTrees(result.allTrees);
                lbTreePath.clear();
                lbTreePath.addItem("");
                lbTreePath.setValue(0, "");
                for (TreeBean tree : allDynamicTrees) {
                    if (tree.treePath != null) {
                        lbTreePath.addItem(tree.treePath);
                        lbTreePath.setValue(lbTreePath.getItemCount() - 1, BaseUtils.safeToString(tree.id));
                    }
                }
                setSelectedTree();
            }
        }
        );
    }

    private void setSelectedTree() {
        if (activatedPlainFileBean == null || activatedPlainFileBean.treeId == null) {
            return;
        }

        for (int i = 0; i < lbTreePath.getItemCount(); i++) {
            if (lbTreePath.getValue(i).equals(BaseUtils.safeToString(activatedPlainFileBean.treeId))) {
                lbTreePath.setSelectedIndex(i);
                return;
            }
        }
    }

    private void setAccountWidgetEnabled(Boolean isRemote) {
        if (chbIsActive.getValue()) {
            tbxDomain.setEnabled(isRemote);
            tbxUsername.setEnabled(isRemote);
            tbxPw.setEnabled(isRemote);
        }
    }

    private void createTreeCodeTable(CellPanel panelRight) {
//        List<TreeBean> trees = new ArrayList<TreeBean>();
//        for (TreeBean tree : BIKClientSingletons.getTrees()) {
//            String menuPath = BIKClientSingletons.getMenuPathForTreeId(tree.id);
//            if (!BaseUtils.isStrEmptyOrWhiteSpace(menuPath)) {
//                trees.add(tree);
//            }
//        }

        final ScrollPanel sp = new ScrollPanel();
        BIKClientSingletons.getService().getImportableTrees(new StandardAsyncCallback<List<TreeBean>>() {

            @Override
            public void onSuccess(List<TreeBean> result) {
                List<TreeBean> trees = new ArrayList<TreeBean>();
                for (TreeBean tree : result) {
                    String menuPath = BIKClientSingletons.getMenuPathForTreeId(tree.id);
                    if (!BaseUtils.isStrEmptyOrWhiteSpace(menuPath)) {
                        trees.add(tree);
                    }
                }
                sp.clear();
                Grid grid = new Grid(trees.size() + 1, 2);
                int cnt = 0;
                grid.setWidget(cnt, 0, new Label(I18n.drzewo.get()));
                grid.setWidget(cnt++, 1, new Label(I18n.kodDrzewa.get()));
                for (TreeBean tree : trees) {
                    grid.setWidget(cnt, 0, new HTML(BIKClientSingletons.getMenuPathForTreeId(tree.id)));
                    grid.setWidget(cnt++, 1, new HTML(tree.code));
                }
                sp.add(grid);
            }
        });
        sp.setHeight("300px");
        sp.setWidth("100%");
        panelRight.add(sp);
    }
}
