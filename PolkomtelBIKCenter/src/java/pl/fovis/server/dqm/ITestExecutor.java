/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.dqm;

import java.util.List;
import java.util.Map;
import pl.fovis.common.dqm.DQMMultiSourceBean;

/**
 * Interface z metodami do wykonywania w ramach jednego testu: inicjlizacja,
 * wykonanie, pobranie błędnych danych oraz zamknięcie połączenia.
 *
 * @author tflorczak
 */
public interface ITestExecutor {

    /**
     * Wstępna inicjalizacja executora. Metoda pobiera przewidywalne wykonania
     * tesu dla pojedynczego uruchomienia.
     *
     * @param test Bean z charakterystyka testu
     * @param optMultiSources opcjonalnie lista testów podrzędnych dla testu
     * głównego multiźródłowego
     * @return list przewidywalnech wykonań - domyslnie zwraca listę z
     * pojedynczym obiektem, który został dostarczony w parametrze metody
     */
    public List<DQMTestBean> preInitAndGetRequestToRun(DQMTestBean test, List<DQMMultiSourceBean> optMultiSources);

    /**
     * Metoda do utworzenia połączenia w ramach jednego wykonania testu
     *
     * @param test Bean z charakterystyka testu
     * @return true - jeśli udało się zainicjować połączenie, false w przeciwnym
     * przypadku
     */
    public boolean initConnection(DQMTestBean test);

    /**
     * Metoda do utworzenia połączenia do pobrania danych - dla testu
     * wieloźródłowego
     *
     * @param source Bean z charakterystyka źródła
     * @return true - jeśli udało się zainicjować połączenie, false w przeciwnym
     * przypadku
     */
    public boolean initConnection(DQMMultiSourceBean source);

    /**
     * Metoda do zamknięcia połączenia, otwartego w initConnection
     */
    public void closeConnection();

    /**
     * Metoda wołana na sam koniec uruchamiania testu
     */
    public void exit();

    /**
     * Metoda do uruchomienia testu
     *
     * @return lista w jednym wierszem. Wiesz zawiera dwie kolumny: case_count i
     * error_count z wynikami testu.
     */
    public List<Map<String, Object>> runTest();

    /**
     * Pobranie błędnych rekordów
     *
     * @return lista wierszy z danymi
     */
    public List<Map<String, Object>> getErrorData();

//    /**
//     Pobranie danych ze źródła - dla testu wieloźródłowego
//     @return lista wierszy z danymi
//     */
//    public List<Map<String, Object>> getData();
//
//    /**
//     * Metoda do zapisu w tabeli tymczasowej dqm_tmp_* danych ze źródła - dla testu wieloźródłowego
//     * @param data dane do przechowania
//     */
//    public void saveDataInBIKSDatabase(List<Map<String, Object>> data);
    public int pumpDataToBiksDatabase();

    public int pumpErrorDataToBiksDatabase(String tmpTableName);
}
