﻿exec sp_check_version '1.3.0.14';
go

exec sp_add_menu_node 'admin:lisa', 'Dane połączenia', '#admin:lisa:connection'
go

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
declare @menuNodeId int = (select id from bik_node where tree_id = @tree_id and obj_id = '#admin:lisa:connection')        
exec sp_node_init_branch_id @tree_id, @menuNodeId
go

exec sp_update_version '1.3.0.14', '1.3.0.15';
go

