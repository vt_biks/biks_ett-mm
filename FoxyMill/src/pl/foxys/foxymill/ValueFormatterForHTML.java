/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill;

import simplelib.IValueFormatter;
import commonlib.LameUtils;
import java.util.Date;
import simplelib.BaseUtils;
import simplelib.SimpleDateUtils;

/**
 *
 * @author wezyr
 */
public class ValueFormatterForHTML implements IValueFormatter {

    public String valToString(Object val) {
        String valStr;

        if (val instanceof Date) {
            valStr = SimpleDateUtils.toISOString((Date) val);
        } else {
            valStr = BaseUtils.safeToString(val);
        }

        valStr = LameUtils.encodeForHTMLTag(valStr/*, true, 25*/);
        return valStr;
    }
}
