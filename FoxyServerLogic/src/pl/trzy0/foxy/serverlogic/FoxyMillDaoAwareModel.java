/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import java.util.Map;
import simplelib.BaseUtils;
import simplelib.i18nsupport.I18nMessage;
import simplelib.i18nsupport.ICurrentLocaleNameProvider;

/**
 *
 * @author wezyr
 */
public class FoxyMillDaoAwareModel<DAO> extends FoxyMillModelBaseEx implements IDaoAwareNamedSqlsModel<DAO> {

    private static final String defaultLocaleName = ICurrentLocaleNameProvider.DEFAULT_LOCALE_NAME;
    public DAO dao;
    public Map<String, Object> globals;
    protected IServerSupportedLocaleNameRetriever optServerSupportedLocaleNameRetriever;

    public FoxyMillDaoAwareModel() {
    }

    public void setDao(DAO dao, IServerSupportedLocaleNameRetriever optServerSupportedLocaleNameRetriever) {
        this.dao = dao;
        this.optServerSupportedLocaleNameRetriever = optServerSupportedLocaleNameRetriever;
    }

    public void setGlobals(Map<String, Object> globals) {
        this.globals = globals;
    }

    public String getLocaleLang() {
        return I18nMessage.getCurrentLocaleName();
    }

    public String getServerDefaultLocaleLang() {
        if (optServerSupportedLocaleNameRetriever == null) {
            return defaultLocaleName;
        }
        return BaseUtils.safeGetFirstItem(
                optServerSupportedLocaleNameRetriever.getServerSupportedLocaleNames(),
                defaultLocaleName);
    }
}
