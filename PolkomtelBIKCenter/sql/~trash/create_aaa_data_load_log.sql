SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[aaa_data_load_log](
	[id] [int] IDENTITY(1,1) NOT NULL,
        [data_source_type_id] [int] NOT NULL,
	[creation_time] [datetime] NOT NULL,
        [status] [int] NOT NULL,
        [status_description] [varchar](1000) NULL
PRIMARY KEY CLUSTERED
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
)

GO

ALTER TABLE [dbo].[aaa_data_load_log] WITH CHECK ADD FOREIGN KEY([data_source_type_id])
REFERENCES [dbo].[aaa_data_source_def] ([id])
GO

SET ANSI_PADDING OFF
GO
