/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts;

import edu.emory.mathcs.backport.java.util.Collections;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import pl.fovis.client.FtsService;
import pl.fovis.common.SimilarNodeBean;
import pl.fovis.common.fts.SimilarWordsWithCounts;
import pl.fovis.server.BIKCenterConfigObj;
import pl.fovis.server.BIKSServiceBaseForSingleBIKS;
import pl.fovis.server.BOXIServiceImpl;
import simplelib.Pair;
import simplelib.SimpleDateUtils;

/**
 *
 * @author pmielanczuk
 */
//@WithoutAdhocSqls
public class FtsServiceImpl extends BIKSServiceBaseForSingleBIKS implements FtsService/*, IHasFoxyTransactionalPerformers*/ {

//    static {
//
//        for (String string : TimeZone.getAvailableIDs()) {
//            System.out.println(string);
//        }
//
//        TimeZone.setDefault(TimeZone.getTimeZone("Poland"));
//    }
    protected FtsExecutor executor;
    protected BOXIServiceImpl boxiService;
//    protected List<IFoxyTransactionalPerformer> tpsSingleton;

    public FtsServiceImpl(BOXIServiceImpl boxiService) {
        this.boxiService = boxiService;
    }

    @Override
    protected void initWithConfig(BIKCenterConfigObj cfg) {
        super.initWithConfig(cfg);
        executor = new FtsExecutor(getAdhocDao());
//        executor = new FtsExecutor(cfg.connConfig);
//        tpsSingleton = Collections.singletonList(executor);
    }

    protected List<SimilarNodeBean> convertToSnbs(List<Pair<Integer, Double>> foundObjs) {

        List<Integer> nodeIds = new ArrayList<Integer>(foundObjs.size());

        for (Pair<Integer, Double> p : foundObjs) {
            nodeIds.add(p.v1);
        }

        Map<Integer, SimilarNodeBean> m = boxiService.getNodeDataByIds("getNodeNameTreeIdBranchIdsByIds", SimilarNodeBean.class, nodeIds);

        for (Pair<Integer, Double> p : foundObjs) {
            final Integer nodeId = p.v1;
            SimilarNodeBean snb = m.get(nodeId);
            if (snb == null) {
                continue;
            }
            snb.relevance = p.v2;
        }

        final ArrayList<SimilarNodeBean> res = new ArrayList<SimilarNodeBean>(m.values());

        Collections.sort(res, new Comparator<SimilarNodeBean>() {

            @Override
            public int compare(SimilarNodeBean o1, SimilarNodeBean o2) {
                double delta = o1.relevance - o2.relevance;

                int res;

                if (delta == 0) {
                    res = (o1.name == null ? "" : o1.name).compareTo(o2.name == null ? "" : o2.name);
                } else if (delta < 0) {
                    res = 1;
                } else {
                    res = -1;
                }

                return res;
            }
        });

        return res;
    }

    @Override
    public List<SimilarNodeBean> findSimilarObjects(int objId, double minRelevance, int maxResults) {
        return convertToSnbs(executor.findSimilarObjects(objId, minRelevance, maxResults));
    }

    @Override
    public List<SimilarNodeBean> findSimilarObjectsByLucene(int objId, double minRelevance, int maxResults) {

        List<SimilarNodeBean> snbsTemp = FtsSearchByLucene.getMoreLikeThis(objId);

        List<Pair<Integer, Double>> foundObjs = new ArrayList<Pair<Integer, Double>>();

        for (SimilarNodeBean snb : snbsTemp) {
            Pair<Integer, Double> p = new Pair<Integer, Double>(snb.id, snb.relevance);
            foundObjs.add(p);
        }

        return convertToSnbs(foundObjs);
    }

    @Override
    public Pair<List<SimilarNodeBean>, List<SimilarWordsWithCounts>> findSimilarObjectsAndWords(int objId, double minRelevance, int maxResults) {
        Pair<List<Pair<Integer, Double>>, List<SimilarWordsWithCounts>> p = executor.findSimilarObjectsAndWords(objId, minRelevance, maxResults);

        return new Pair<List<SimilarNodeBean>, List<SimilarWordsWithCounts>>(convertToSnbs(p.v1), p.v2);
    }

    @Override
    public List<SimilarWordsWithCounts> getSimilarWordsWithCountsForObjPair(int objId1, int objId2) {
        return executor.getSimilarWordsWithCountsForObjPair(objId1, objId2);
    }

    @Override
    public void changeWordWeight(String word, double weight) {
        executor.changeWordWeight(word, weight);
    }

//    @Override
//    public Collection<? extends IFoxyTransactionalPerformer> getTransactionalPerformers() {
//        return tpsSingleton;
//    }
    @Override
    public String getDateDiagInfo(Date d) {
        return SimpleDateUtils.dateDiagInfo(d);
    }
}
