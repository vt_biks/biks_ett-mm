/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages.drools;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.BikPageAdminConfigBase;
import pl.fovis.common.DroolsConfigBean;
import pl.fovis.common.DynamicTreesBigBean;
import pl.fovis.common.TreeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;

/**
 *
 * @author ctran
 */
public class DroolsManagerPage extends BikPageAdminConfigBase<DroolsConfigBean> {

    protected DroolsConfigBean config;

    protected CheckBox chbIsActivated;
//    protected ListBox lbxFrequency;
    protected TextBox tbxFrequency;
    protected DateBox dbxStartDate;
    protected TextBox tbxStartHour;
    protected TextBox drlFolderPathTb;
    protected ListBox lbTreePath;
    protected List<TreeBean> allTrees;
    protected RadioButton frequencyRb;

    protected Label statusLbl;
//    public static final String[] frequencyTxt = {I18n.codziennie.get(), I18n.co3Dni.get(), I18n.co5Dni.get(), I18n.co7Dni.get(), I18n.co14Dni.get(), I18n.co30Dni.get(), I18n.jednorazowo.get()};
//    public static final Map<String, Integer> frequency2IdMap = new HashMap<String, Integer>();

//    static {
//        frequency2IdMap.put("1", 0);
//        frequency2IdMap.put("3", 1);
//        frequency2IdMap.put("5", 2);
//        frequency2IdMap.put("7", 3);
//        frequency2IdMap.put("14", 4);
//        frequency2IdMap.put("30", 5);
//        frequency2IdMap.put("0", 6);
//    }
    @Override
    public boolean isPageEnabled() {
        return super.isPageEnabled() && BIKClientSingletons.isUseDrools(); //To change body of generated methods, choose Tools | Templates.
    }

    private void createLaunchingSection() {
        HorizontalPanel hp = new HorizontalPanel();
        createLabel(I18n.dataRozpoczecia.get(), hp);
        dbxStartDate = createDateBox(hp);
        dbxStartDate.setWidth("125px");
//        dbxStartDate.getTextBox().getElement().setAttribute("readonly", "true");
        createLabel(I18n.godzina.get() + " (hh:mm)", hp);
        tbxStartHour = createTextBox(hp);
        tbxStartHour.setWidth("50px");
        createLabel(I18n.czestotliwosc.get(), hp);
        tbxFrequency = createTextBox(hp);
        tbxFrequency.setWidth("50px");//        lbxFrequency = createFrequencyBox(hp);
        setHorizontalPanelAlignmentMiddle(hp);
        main.add(hp);
        //        tbxStartHour.setWidth("100px");
    }

    private void setHorizontalPanelAlignmentMiddle(HorizontalPanel hp) {
        hp.addStyleName("tableAsInlineDiv");
        for (int i = 0; i < hp.getWidgetCount(); i++) {
            hp.setCellVerticalAlignment(hp.getWidget(i), HasVerticalAlignment.ALIGN_MIDDLE);
        }
    }

//    protected ListBox createFrequencyBox(CellPanel cp) {
//        ListBox listbox = new ListBox();
//        listbox.addItem(frequencyTxt[0] /* I18N:  */, "1");
//        listbox.addItem(frequencyTxt[1] /* I18N:  */, "3");
//        listbox.addItem(frequencyTxt[2] /* I18N:  */, "5");
//        listbox.addItem(frequencyTxt[3] /* I18N:  */, "7");
//        listbox.addItem(frequencyTxt[4] /* I18N:  */, "14");
//        listbox.addItem(frequencyTxt[5] /* I18N:  */, "30");
//        listbox.addItem(frequencyTxt[6] /* I18N:  */, "0");
//        cp.add(listbox);
//        widgetList.add(listbox);
//        return listbox;
//    }
    @Override
    protected void buildInnerWidget() {
        chbIsActivated = createCheckBox(I18n.aktywny.get(), new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setWidgetsEnabled(chbIsActivated.getValue());
            }
        }, false);
        createLaunchingSection();

        createLabel(I18n.sciezkaDoDrl.get());
        drlFolderPathTb = createTextBox();
//        lbTreePath = createListBox(new HashSet<String>());
        createButton(I18n.zapisz.get(), new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                callService2SaveConfig();
            }

        });
        createButton(I18n.reset.get(), new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                callService2Start();
            }

        });
//        createLine();
//        createLabel(I18n.status.get());
        statusLbl = createLabel("");
        callService2GetData();
    }

    private void callService2SaveConfig() {
        config.drlFolder = drlFolderPathTb.getText();
        config.isActive = chbIsActivated.getValue() ? 1 : 0;
        config.startDate = dbxStartDate.getTextBox().getText();
        config.frequency = tbxFrequency.getValue();
        config.startHour = tbxStartHour.getText();
//        config.inspectedTreeIds = lbTreePath.getSelectedValue();
        statusLbl.setText("");
        BIKClientSingletons.getService().setDroolsConfig(config, new StandardAsyncCallback<Void>() {

            @Override
            public void onSuccess(Void result) {
                BIKClientSingletons.showInfo(I18n.zapisanoZmiany.get());
            }
        });
    }

    private void callService2Start() {
        statusLbl.setText("");
        BIKClientSingletons.getService().resetDrools(new StandardAsyncCallback<String>() {

            @Override
            public void onSuccess(String result) {
                BIKClientSingletons.showInfo(I18n.zapisanoZmiany.get());
                statusLbl.setText(result);
            }
        });
    }

    @Override
    public void populateWidgetWithData(DroolsConfigBean data) {
        config = data;
        chbIsActivated.setValue(data.isActive > 0);
        dbxStartDate.getTextBox().setText(data.startDate);
        tbxStartHour.setText(data.startHour);
//        lbxFrequency.setSelectedIndex(frequency2IdMap.get(data.frequency));
        tbxFrequency.setText(data.frequency);
        drlFolderPathTb.setText(data.drlFolder);
        setWidgetsEnabled(data.isActive > 0);
        BIKClientSingletons.getService().getBikTreeForData(new StandardAsyncCallback<DynamicTreesBigBean>() {

            @Override
            public void onSuccess(DynamicTreesBigBean result) {
                allTrees = result.allTrees;
                lbTreePath.clear();
                lbTreePath.addItem("");
                lbTreePath.setValue(0, "");
                for (TreeBean tree : allTrees) {
                    if (!BaseUtils.isStrEmptyOrWhiteSpace(tree.treePath)) {
                        lbTreePath.addItem(tree.treePath);
                        lbTreePath.setValue(lbTreePath.getItemCount() - 1, BaseUtils.safeToString(tree.id));
                    }
                }
//                setSelectedTree();
            }
        });
    }

//    private void setSelectedTree() {
//        Integer selectedTreeeId = BaseUtils.tryParseInteger(config.inspectedTreeIds, null);
//        for (int i = 0; i < lbTreePath.getItemCount(); i++) {
//            if (lbTreePath.getValue(i).equals(BaseUtils.safeToString(selectedTreeeId))) {
//                lbTreePath.setSelectedIndex(i);
//                return;
//            }
//        }
//    }
    @Override
    public String getId() {
        return "admin:drools:cfg";
    }

    @Override
    public Widget createMainWidget() {
        return buildWidget();
    }

    @Override
    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
    }

    private void callService2GetData() {
        BIKClientSingletons.getService().getDroolsConfig(new StandardAsyncCallback<DroolsConfigBean>() {

            @Override
            public void onSuccess(DroolsConfigBean result) {
                populateWidgetWithData(result);
            }
        });
    }
}
