/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import commonlib.LameUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import static pl.bssg.metadatapump.ThreadedPumpExecutor.MILLIS_FOR_24H;
import pl.bssg.metadatapump.sapbo.biadmin.IBIAConnector;
import pl.fovis.common.BIArchiveConfigBean;
import pl.fovis.common.ConnectionParametersBIAdminBean;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import simplelib.BaseUtils;
import simplelib.Pair;
import simplelib.SimpleDateUtils;
import simplelib.logging.ILameLogger;

/**
 *
 * @author ctran
 */
public class ThreadedBIAdminArchive extends BIKSThreadedWorker {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected INamedSqlsDAO<Object> adhocDao;
    protected IBIAConnector biaProxyConnector;
    protected String dirForUpload;

    protected BIAArchiveWorker worker;

    public ThreadedBIAdminArchive(INamedSqlsDAO<Object> adhocDao, IBIAConnector biaProxyConnector, String dirForUpload) {
        this.adhocDao = adhocDao;
        this.biaProxyConnector = biaProxyConnector;
        this.dirForUpload = dirForUpload;
    }

    public synchronized void schedule(ConnectionParametersBIAdminBean biServerConfig, final BIArchiveConfigBean archiveConfig) {
        destroy();
        if (worker != null) {
            worker.cancel();
        }
        if (archiveConfig.isActive != 1) {
            return;
        }
        worker = new BIAArchiveWorker(adhocDao, biaProxyConnector, dirForUpload, biServerConfig, archiveConfig);
        // Wykonac tylko w przypadku gdy isActive = 1
        timer = new Timer();
        SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd HH:mm");
        TimerTask task = new TimerTask() {

            @Override
            public void run() {
                jobExecutor.addJob(new Runnable() {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");

                    @Override
                    public void run() {
//                        System.out.println(sdf.format(new Date(System.currentTimeMillis())) + ": BIAArchive is Running");
                        worker.doit();
                    }
                });
            }
        };
//        Date startDate = SimpleDateUtils.parseFromCanonicalString(
//                BaseUtils.isStrEmptyOrWhiteSpace(archiveConfig.startDate) ? sdf.format(new Date()) : archiveConfig.startDate
//                        + " " + archiveConfig.startHour);

        int frequency = BaseUtils.tryParseInt(archiveConfig.frequency);
        Pair<String, String> timeToStart = BaseUtils.splitString(archiveConfig.startHour, ":");
        Date dateSchedule = null;
        Date today = new Date();

        if (BaseUtils.isStrEmptyOrWhiteSpace(archiveConfig.startDate)) { //nie byl wprowadzony czas
            dateSchedule = SimpleDateUtils.newDate(SimpleDateUtils.getYear(today), SimpleDateUtils.getMonth(today), SimpleDateUtils.getDay(today), BaseUtils.tryParseInt(timeToStart.v1), BaseUtils.tryParseInt(timeToStart.v2));
        } else {
            try {
                dateSchedule = sdf.parse(archiveConfig.startDate + " " + archiveConfig.startHour);
            } catch (ParseException ex) {
            }
        }
        if (dateSchedule != null) { //poprawnie wprowadzony czas
            if (frequency == 0) { //jednorazowy
                if (today.before(dateSchedule)) {
                    timer.schedule(task, dateSchedule);
                }
            } else {
                dateSchedule = SimpleDateUtils.newDate(SimpleDateUtils.getYear(today), SimpleDateUtils.getMonth(today), SimpleDateUtils.getDay(today), BaseUtils.tryParseInt(timeToStart.v1), BaseUtils.tryParseInt(timeToStart.v2));
                if (dateSchedule.before(today)) {
                    dateSchedule = SimpleDateUtils.addDays(dateSchedule, 1);
                }
//                System.out.println("Delayed in " + (dateSchedule.getTime() - today.getTime()) / 1000.0);
//                timer.scheduleAtFixedRate(task, dateSchedule.getTime() - today.getTime(), MILLIS_FOR_24H * frequency);
                timer.scheduleAtFixedRate(task, dateSchedule, MILLIS_FOR_24H * frequency);
            }
        }
    }
}
