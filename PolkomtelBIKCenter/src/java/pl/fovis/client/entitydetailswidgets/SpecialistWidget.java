/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

//import java.lang.Integer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PushButton;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.TreeAndDictionarySelectorForUsers;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.JoinTypeRoles;
import pl.fovis.common.UseInNodeSelectionBean;
import pl.fovis.common.UserInNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public class SpecialistWidget extends EntityDetailsTabPaginationBase<UserInNodeBean> {

    protected Map<PushButton, Boolean> showClearButtonsMap = new HashMap<PushButton, Boolean>();
    protected List<PushButton> clearButtons = new ArrayList<PushButton>();
    protected Map<Integer, String> nodeNamesInBranches;
//    private VerticalPanel mainSpecialistVp;
//    private HorizontalPanel innerSpecialistHp;

    public SpecialistWidget() {
    }

    @Override
    public void refreshWidgetsState() {
        super.refreshWidgetsState();

        for (int i = 0; i < clearButtons.size(); i++) {
            clearButtons.get(i).setEnabled(widgetsActive && BIKClientSingletons.isUserLoggedIn());
            clearButtons.get(i).setVisible(showClearButtonsMap.get(clearButtons.get(i)));
        }
    }

    protected void execAddUserInNodeAndRefresh(UseInNodeSelectionBean uinb) {
        BIKClientSingletons.getService().addUserInNode(nodeId, uinb.userId, uinb.roleForNodeId, uinb.inheritToDescendants, uinb.joinTypeRole == JoinTypeRoles.Auxiliary, uinb.joinTypeRole,
                new StandardAsyncCallback<Boolean>("Failed in adding new pair of role and user" /* I18N: no:err-fail-in */) {
                    @Override
                    public void onSuccess(Boolean result) {
                        refreshAndRedisplayData();
                        BIKClientSingletons.showInfo(I18n.dodanoPowiazanie.get() /* I18N:  */);
                        BIKClientSingletons.refreshAndRedisplayDataUsersTree(result);
                    }
                });
    }

    @Override
    protected List<UserInNodeBean> extractItemsFromFetchedData(EntityDetailsDataBean data) {
//        this.nodeNamesInBranches = data.usersInNode.v2;
//        return data.usersInNode.v1;
        return BaseUtils.isCollectionEmpty(items) ? new ArrayList<UserInNodeBean>() : items;
    }

    @Override
    public String getCaption() {
        return /*BIKGWTConstants.MENU_NAME_USERS + "<br>" + BIKGWTConstants.MENU_NAME_USERS2*/ I18n.osoby.get();
    }

    @Override
    public boolean hasEditableControls() {
        return !isInUserTree(getNodeTreeCode());
    }

    @Override
    protected NodeAwareBeanExtractorBase<UserInNodeBean> getBeanExtractor() {
        return new UserInNodeBeanExtractor(hasAddJoinedButton, nodeId, nodeNamesInBranches);
    }

    @Override
    public String getTreeKindForSelected() {
        //ww->dynTrees: być może tu powinno być TREE_KIND_USER_ROLES???
        return BIKGWTConstants.TREE_KIND_USER_ROLES;
    }

    @Override
    protected BikCustomButton createAddJoinedButton(Set<String> treeKinds) {
        actionBtn = new BikCustomButton(I18n.dodajPowiazanie.get() /* I18N:  */, "Attachs" /* I18N: no */ + "-addAttachEntry",
                new IContinuation() {
                    @Override
                    public void doIt() {
                        BIKClientSingletons.getService().getDescendantsCntInMainRoles(nodeId, new StandardAsyncCallback<Map<Integer, Integer>>() {
                            @Override
                            public void onSuccess(Map<Integer, Integer> result) {
                                new TreeAndDictionarySelectorForUsers().buildAndShowDialog(nodeId, //"Users" /* I18N: no */,
                                        items, result, new IParametrizedContinuation<UseInNodeSelectionBean>() {
                                    @Override
                                    public void doIt(UseInNodeSelectionBean param) {
                                        execAddUserInNodeAndRefresh(param);
                                    }
                                });
                            }
                        });
                    }
                });
        addWidgetVisibledByState(actionBtn);
        return actionBtn;
    }

    @Override
    public void optCallServiceToGetData(final IContinuation con) {
        BIKClientSingletons.getService().getUsersInNode(nodeId, getData().node.branchIds, new AsyncCallback<Pair<List<UserInNodeBean>, Map<Integer, String>>>() {

            @Override
            public void onFailure(Throwable caught) {
                SpecialistWidget.super.displayData();
            }

            @Override
            public void onSuccess(Pair<List<UserInNodeBean>, Map<Integer, String>> result) {
                SpecialistWidget.this.nodeNamesInBranches = result.v2;
                items = result.v1;
//                Window.alert("Itemsize: " + items.size());
                con.doIt();
//                SpecialistWidget.super.displayData();
//                edwc.refreshTabCaption(SpecialistWidget.this);
            }
        });
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
