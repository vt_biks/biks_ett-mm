﻿IF OBJECT_ID('tempdb..#xxx') IS NOT NULL 
drop table #xxx
go

declare @rapTreeId int = (select id from bik_tree where code = 'Reports')
declare @rapWebiKindId int = (select dbo.fn_node_kind_id_by_code('Webi'))
declare @qryKindId int = (select dbo.fn_node_kind_id_by_code('ReportQuery'))
declare @objMeasureKindId int = (select dbo.fn_node_kind_id_by_code('Measure')) 
declare @objDimensionKindId int = (select dbo.fn_node_kind_id_by_code('Dimension')) 
declare @objDetailKindId int = (select dbo.fn_node_kind_id_by_code('Detail')) 
declare @univTreeId int = (select id from bik_tree where code = 'ObjectUniverses')
declare @univKindId int = (select dbo.fn_node_kind_id_by_code('Universe'))
declare @connKindId int = (select dbo.fn_node_kind_id_by_code('DataConnection'))
declare @connTreeId int = (select id from bik_tree where code = 'Connections')

select
		cast('' as varchar(max)) as rap_parent_path,
		rap.parent_node_id as rap_ancestor_id,
		rap.name as report_name,
		extRap.cuid as rep_cuid,
		extRap.created as rep_created,
		extRap.modified as rep_modified,
		extRap.author as rep_author,
		extRap.owner as rep_owner,
		extRap.file_path + extRap.file_name as rep_file_path,
		rap.descr as rep_description,
		qry.name as qry_name,
		qryText.sql_text as sql_text,
		obj.name as obj_name,
		extObj.text_of_select as obj_text_of_select,
		extObj.text_of_where as obj_text_of_where,
		extObj.aggregate_function as obj_aggregate_function,
		extObj.type as obj_type,
		univ.name as univ_name,
		conn.name as conn_name,
		cast('' as varchar(max)) as univ_parent_path, 
		univ.parent_node_id as univ_ancestor_id
 into #xxx
from
		bik_node rap inner join bik_node qry on qry.parent_node_id = rap.id 
		inner join bik_sapbo_query qryText on qry.id = qryText.node_id
		inner join bik_node objlinked on objlinked.parent_node_id = qry.id 
		inner join bik_node obj on objlinked.linked_node_id = obj.id 
		inner join bik_joined_objs qryjoins on qry.id = qryjoins.src_node_id and qryjoins.type=1
		inner join bik_node univ on qryjoins.dst_node_id = univ.id
		inner join bik_sapbo_extradata extRap on extRap.node_id=rap.id
		inner join bik_sapbo_universe_object extObj on extObj.node_id=obj.id
		left join ( bik_joined_objs connjoins
		inner join bik_node conn on conn.id = connjoins.dst_node_id ) on qry.id = connjoins.src_node_id and conn.is_deleted = 0 and conn.linked_node_id is null and conn.tree_id = @connTreeId and conn.node_kind_id = @connKindId
where 
		rap.is_deleted = 0 and rap.linked_node_id is null and rap.tree_id = @rapTreeId and rap.node_kind_id = @rapWebiKindId
		and qry.is_deleted = 0 and qry.tree_id = @rapTreeId and qry.node_kind_id = @qryKindId
		and objlinked.is_deleted = 0 and objlinked.tree_id = @rapTreeId and objlinked.node_kind_id in (@objMeasureKindId, @objDimensionKindId, @objDetailKindId)
		and obj.is_deleted = 0 and obj.tree_id = @univTreeId and obj.linked_node_id is null and obj.node_kind_id in (@objMeasureKindId, @objDimensionKindId, @objDetailKindId)
		and univ.is_deleted = 0 and univ.linked_node_id is null and univ.tree_id = @univTreeId and univ.node_kind_id = @univKindId
go




while 1 = 1 begin
  declare @rc int = null

  update #xxx
  set rap_parent_path = bn.name + '\' + rap_parent_path, rap_ancestor_id = bn.parent_node_id
  from bik_node bn
  where bn.id = #xxx.rap_ancestor_id
  
  set @rc = @@rowcount
  
  if @rc = 0 break
end
go


while 1 = 1 begin
  declare @rc int = null

  update #xxx
  set univ_parent_path = bn.name + '\' + univ_parent_path, univ_ancestor_id = bn.parent_node_id
  from bik_node bn
  where bn.id = #xxx.univ_ancestor_id
  
  set @rc = @@rowcount
  
  if @rc = 0 break
end
go


--select * from #xxx
select rap_parent_path, report_name,rep_cuid,rep_created,rep_modified,rep_author,rep_owner,rep_file_path,rep_description,qry_name,sql_text,obj_name,obj_text_of_select,obj_text_of_where,obj_aggregate_function,obj_type,univ_name,conn_name,univ_parent_path
from #xxx
order by rap_parent_path,report_name,qry_name,obj_name