﻿exec sp_check_version '1.0.99.2';
go

------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------


if not exists(select * from sys.types where name = 'bik_joined_obj_modes_table_type')
create type bik_joined_obj_modes_table_type as table (dst_id int not null,
  mode int null, -- 0: none, 1: with inheritance, 2: single, null oznacza - tak jak było do teraz, nie zmieniaj poniżej
  level int not null)
go


------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

if object_id('sp_update_Joined_Objs_For_Node') is not null
  drop procedure sp_update_Joined_Objs_For_Node
go

create procedure sp_update_Joined_Objs_For_Node(@src_id int, @joined_obj_modes bik_joined_obj_modes_table_type readonly) as
begin
  set nocount on
  declare @diags_level int = 0 -- 0 oznacza brak logowania

  declare @tmp_joined_objs table (dst_node_id int not null primary key, type int not null, inherit_to_descendants int not null)
  
  declare @to_join table (dst_id int not null, mode int null, level int not null, branch_ids varchar(2000) not null)

  insert into @to_join (dst_id, mode, level, branch_ids)
  select t.dst_id, t.mode, t.level, n.branch_ids
  from @joined_obj_modes t inner join bik_node n on t.dst_id = n.id

  declare to_join cursor for 
  select dst_id, mode, level, branch_ids from @to_join
  order by branch_ids

  open to_join

  declare @dst_id int, @mode int, @level int, @branch_ids varchar(2000)

  fetch next from to_join into @dst_id, @mode, @level, @branch_ids

  while @@fetch_status = 0
  begin
    if @diags_level > 0 begin
      print 'row in loop: @dst_id=' + cast(@dst_id as varchar(20)) + ', @mode=' + cast(@mode as varchar(20)) + ', @branch_ids=' + @branch_ids
    end
  
    declare @br_ids_for_like varchar(3000) = @branch_ids + '%'

    if @level = 0 -- this node only (@dst_id)
    begin
      delete from @tmp_joined_objs
      where dst_node_id = @dst_id

  	  if @mode is not null begin
        insert into @tmp_joined_objs (dst_node_id, type, inherit_to_descendants)
        values (@dst_id, case when @mode = 0 then -1 else 0 end, case when @mode = 1 then 1 else 0 end)
        if @diags_level > 0 begin
          print 'row in loop: inserted single'
        end        
      end
    end else if @level = 1 -- direct subnodes only
    begin
        delete from @tmp_joined_objs
        where dst_node_id in (select id from bik_node where parent_node_id = @dst_id)

  	  if @mode is not null begin
  	    insert into @tmp_joined_objs (dst_node_id, type, inherit_to_descendants)
	    select n.id, case when @mode = 0 then -1 else 0 end, case when @mode = 1 then 1 else 0 end
	    from bik_node n left join bik_joined_objs jo on n.id = jo.dst_node_id and jo.src_node_id = @src_id
	    where n.parent_node_id = @dst_id and (jo.id is null or jo.type = 0) and n.is_deleted = 0 and (n.linked_node_id is null or n.disable_linked_subtree = 0)
      end
    end else -- all subdones
    begin
      delete from @tmp_joined_objs
      where dst_node_id <> @dst_id and dst_node_id in (select id from bik_node where branch_ids like @br_ids_for_like)

	  if @mode is not null begin
        insert into @tmp_joined_objs (dst_node_id, type, inherit_to_descendants)
        select n.id, case when @mode = 0 then -1 else 0 end, case when @mode = 1 then 1 else 0 end
        from bik_node n left join bik_joined_objs jo on n.id = jo.dst_node_id and jo.src_node_id = @src_id
        where n.id <> @dst_id and n.branch_ids like @br_ids_for_like and (jo.id is null or jo.type = 0) and n.is_deleted = 0 and (n.linked_node_id is null or n.disable_linked_subtree = 0)
      end
    end
    fetch next from to_join into @dst_id, @mode, @level, @branch_ids
  end

  close to_join;

  deallocate to_join;

  if @diags_level > 0 begin
    select * from @tmp_joined_objs
  end

  declare @src_branch_ids varchar(2000) = (select branch_ids from bik_node where id = @src_id)

  declare @fixed_dst_node_tab table (node_id int not null primary key)

  insert into @fixed_dst_node_tab (node_id)
  select distinct jo.dst_node_id from bik_joined_objs jo inner join bik_node n on jo.dst_node_id = n.id
  where src_node_id in (
  select ancestor_id from (
  select cast(str as int) as ancestor_id, idx 
  from dbo.fn_split_by_sep(@src_branch_ids, '|', 7)
  ) x 
  where ancestor_id <> @src_id
  ) and jo.inherit_to_descendants = 1 and n.is_deleted = 0

  insert into @fixed_dst_node_tab (node_id)
  select dst_node_id from bik_joined_objs where src_node_id = @src_id and type = 1

  delete from bik_joined_objs where dst_node_id in (select dst_node_id from @tmp_joined_objs) and src_node_id = @src_id and
  dst_node_id not in (select node_id from @fixed_dst_node_tab)

  insert into bik_joined_objs (src_node_id, dst_node_id, type, inherit_to_descendants)
  select @src_id, dst_node_id, 0, inherit_to_descendants
  from @tmp_joined_objs
  where type <> -1 and dst_node_id not in (select node_id from @fixed_dst_node_tab) and dst_node_id <> @src_id

  declare @fixed_src_node_tab table (node_id int not null primary key)

  insert into @fixed_src_node_tab (node_id)
  select jo.src_node_id from bik_joined_objs jo inner join @tmp_joined_objs t on jo.src_node_id = t.dst_node_id
  where jo.dst_node_id = @src_id and jo.type = 1

  delete from bik_joined_objs where src_node_id in (select dst_node_id from @tmp_joined_objs) and dst_node_id = @src_id and
  src_node_id not in (select node_id from @fixed_src_node_tab)

  insert into bik_joined_objs (src_node_id, dst_node_id, type, inherit_to_descendants)
  select dst_node_id, @src_id, 0, inherit_to_descendants
  from @tmp_joined_objs
  where type <> -1 and dst_node_id not in (select node_id from @fixed_src_node_tab) and dst_node_id <> @src_id
end
go


------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

exec sp_update_version '1.0.99.2', '1.0.99.3';
go
