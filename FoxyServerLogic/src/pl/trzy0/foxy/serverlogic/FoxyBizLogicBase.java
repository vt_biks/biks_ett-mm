/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import commonlib.HtmlI18nKeyMaker;
import commonlib.JsonConverterEx;
import commonlib.ThreadCustomVars;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.commons.IBeanConnectionThreadHandle;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import simplelib.FieldNameConversion;
import simplelib.IContinuation;
import simplelib.IContinuationWithReturn;
import simplelib.Pair;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public abstract class FoxyBizLogicBase<CFG, SD, SO> implements IFoxyServiceOnServer<CFG, SD, SO> {

    public static final String FOXYSERVICE_AFTERCOMMITTASKSFORCURRENTTHREAD = "FoxyService_AfterCommitTasksOfCurrentThread";
    // ma być private, to jest pro!
    private static final ILameLogger logger = LameLoggerFactory.getLogger(FoxyBizLogicBase.class);
    // ma być private, to jest pro!
    //private IBeanConnectionEx<Object> connection;
    private INamedSqlsDAO<Object> adhocDao;
    private IFoxySessionDataProvider<SD> sessionDataProvider;
    protected SO sharedServiceObj;

//    protected FoxySessionData getFoxySessionData() {
//        return null;
//    }
    protected Map<String, IBeanConnectionThreadHandle> getCancellableExecutionsInSession() {
        throw new UnsupportedOperationException("no support for cancellable executions, override this method in subclass");
    }

    protected SD getSessionData() {
        return sessionDataProvider.getSessionData();
    }

    protected <R> R getRequestCachedValue(String valueName, IContinuationWithReturn<R> calcCont) {
        Map<String, Object> rts = getRequestTempStorage();

        String rcvStr = "_rcv_" + valueName;

        @SuppressWarnings("unchecked")
        R val = (R) rts.get(rcvStr);

        if (val == null) {
            val = calcCont.doIt();
            rts.put(rcvStr, val);
        }

        return val;
    }

    protected Map<String, Object> getRequestTempStorage() {
        return sessionDataProvider.getRequestTempStorage();
    }

    protected void initWithConfig(CFG configBean) {
        // no-op
    }

    @Override
    public void init(CFG configBean, IFoxySessionDataProvider<SD> sessionDataProvider,
            INamedSqlsDAO<Object> adhocDao, SO sharedServiceObj) {
        if (logger.isInfoEnabled()) {
            logger.info("::: FoxyBizLogicBase.init: cfg=" + JsonConverterEx.convertObject(configBean) + " :::");
        }

        this.adhocDao = adhocDao;
        this.sessionDataProvider = sessionDataProvider;
        this.sharedServiceObj = sharedServiceObj;

        try {
            initWithConfig(configBean);
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.errorAndThrowNew("FoxyBizLogicBase.init failed!", ex);
            }
        }
    }

    protected INamedSqlsDAO<Object> getAdhocDao() {
        return adhocDao;
    }

    protected <V> V execNamedQuerySingleVal(String qryName, boolean allowNoResult,
            String optColumnName, Object... args) {
        return BeanConnectionUtils.<V>execNamedQuerySingleVal(getAdhocDao(), qryName, allowNoResult, optColumnName,
                args);
    }

    protected Integer execNamedQuerySingleValAsInteger(String qryName, boolean allowNoResult,
            String optColumnName, Object... args) {
        return sqlNumberToInt(execNamedQuerySingleVal(qryName, allowNoResult, optColumnName,
                args));
    }

    protected <V> List<V> execNamedQuerySingleColAsList(String qryName, String optColumnName, Object... args) {
        return BeanConnectionUtils.execNamedQuerySingleColAsList(getAdhocDao(), qryName, optColumnName,
                args);
    }

    protected <V> Set<V> execNamedQuerySingleColAsSet(String qryName, String optColumnName, Object... args) {
        return BeanConnectionUtils.execNamedQuerySingleColAsSet(getAdhocDao(), qryName, optColumnName, args);
    }

    protected int execNamedCommand(String name, Object... args) {
        return getAdhocDao().execNamedCommand(name, args);
    }

//    protected int execNamedCommand(INamedSqlsDAO<Object> adhocDao, String name, Object... args) {
//        return adhocDao.execNamedCommand(name, args);
//    }
    protected <T> T createBeanFromNamedQry(String name, Class<T> beanClass,
            boolean allowNoResult, Object... args) {
        return getAdhocDao().createBeanFromNamedQry(name, beanClass, allowNoResult, args);
    }

    protected Map<String, Object> execNamedQuerySingleRowCFN(String name,
            boolean allowNoResult, FieldNameConversion optConversion, Object... args) {
        return adhocDao.execNamedQuerySingleRowCFN(name, allowNoResult, optConversion, args);
    }

    protected <T> List<T> createBeansFromNamedQry(String name, Class<T> beanClass,
            Object... args) {
        return adhocDao.createBeansFromNamedQry(name, beanClass, args);
    }

    public void finishWorkUnit(boolean success) {
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("finishWorkUnit: success=" + success);
            }
            adhocDao.finishWorkUnit(success);

            if (success) {
                executeAfterCommitTasksForCurrentThread();
            }
        } finally {
            clearAfterCommitTasksForCurrentThread();
            if (logger.isDebugEnabled()) {
                logger.debug("finishWorkUnit: after clearAfterCommitTasksForCurrentThread");
            }
        }
    }

    @SuppressWarnings("unchecked")
    protected Collection<Pair<IContinuation, String>> getAfterCommitTasksForCurrentThread() {
        return (Collection<Pair<IContinuation, String>>) ThreadCustomVars.getVar(FOXYSERVICE_AFTERCOMMITTASKSFORCURRENTTHREAD);
    }

    public void addAfterCommitTaskForCurrentThreadX(IContinuation taskCont, String descr) {
        Collection<Pair<IContinuation, String>> tasks = getAfterCommitTasksForCurrentThread();
        if (tasks == null) {
            tasks = new ArrayList<Pair<IContinuation, String>>();
            ThreadCustomVars.setVar(FOXYSERVICE_AFTERCOMMITTASKSFORCURRENTTHREAD, tasks);
        }
        tasks.add(new Pair<IContinuation, String>(taskCont, descr));
    }

    protected void executeAfterCommitTasksForCurrentThread() {
        Collection<Pair<IContinuation, String>> tasks = getAfterCommitTasksForCurrentThread();
        if (tasks != null) {
            for (Pair<IContinuation, String> task : tasks) {
                if (logger.isDebugEnabled()) {
                    logger.debug("executeAfterCommitTasksForCurrentThread: before task. descr: " + task.v2);
                }
                task.v1.doIt();
                if (logger.isDebugEnabled()) {
                    logger.debug("executeAfterCommitTasksForCurrentThread: after task. descr: " + task.v2);
                }
            }
        }
    }

    protected void clearAfterCommitTasksForCurrentThread() {
        Collection<Pair<IContinuation, String>> tasks = getAfterCommitTasksForCurrentThread();
        if (tasks != null) {
            tasks.clear();
            ThreadCustomVars.setVar(FOXYSERVICE_AFTERCOMMITTASKSFORCURRENTTHREAD, null);
        }
    }

    protected List<Map<String, Object>> execNamedQueryCFN(String name,
            FieldNameConversion optConversion, Object... args) {
        return adhocDao.execNamedQueryCFN(name, optConversion, args);
    }

    protected void execInAutoCommitMode(IContinuation whatToExec) {
        adhocDao.execInAutoCommitMode(whatToExec);
        //BeanConnectionUtils.execInAutoCommitMode(connection, whatToExec);
    }

    protected void execInAutoCommitMode(IBeanConnectionEx<Object> conn, IContinuation whatToExec) {
        BeanConnectionUtils.execInAutoCommitMode(conn, whatToExec);
    }

    protected Integer sqlNumberToInt(Object numVal) {
        return adhocDao.sqlNumberToInt(numVal);
    }

    protected Integer sqlNumberInRowToInt(Map<String, Object> row, String colName) {
        return sqlNumberToInt(row.get(colName));
    }

//    protected IBeanConnectionEx<Object> getConnection() {
//        return connection;
//    }
    protected List<Integer> sqlNumbersToInts(Iterable<Object> nums) {
        List<Integer> ints = new ArrayList<Integer>();
        for (Object idObj : nums) {
            ints.add(sqlNumberToInt(idObj));
        }
        return ints;
    }

    protected String stripHtmlTags(String htmlText) {
        if (htmlText == null) {
            return null;
        }
        return HtmlI18nKeyMaker.stripHtmlTagsAndCommentsUnescapeEntities(htmlText, true);
    }

    protected <T> T runCancellableDBJob(String ticket, IBeanConnectionThreadHandle handle, IContinuationWithReturn<T> jobCont) {
        //FoxySessionData sessionData = getFoxySessionData();

        Map<String, IBeanConnectionThreadHandle> cancellableExecutions = getCancellableExecutionsInSession();

        if (cancellableExecutions.containsKey(ticket)) {
            throw new RuntimeException("runCancellableDBJob: ticket [" + ticket + "] is currently in use");
        }

        cancellableExecutions.put(ticket, handle);
        try {
            return jobCont.doIt();
        } catch (Exception ex) {
            throw new RuntimeException("execution was cancelled for ticket [" + ticket + "]", ex);
        } finally {
            cancellableExecutions.remove(ticket);
            handle.clearCancelFlag();
        }
    }

    protected <T> T runCancellableDBJob(String ticket, IContinuationWithReturn<T> jobCont) {
        IBeanConnectionThreadHandle handle = adhocDao.getCurrentThreadHandle();
        return runCancellableDBJob(ticket, handle, jobCont);
    }

    protected boolean isDBJobCanceled() {
        return adhocDao.getCurrentThreadHandle().isExecutionCanceled();
    }

    protected void checkIfDBJobIsCanceled() {
        if (isDBJobCanceled()) {
            throw new RuntimeException("checkIfDBJobIsCanceled: db job is canceled");
        }
    }

    public boolean cancelDBJob(String ticket) {
        //System.out.println("cancelDBJob: zaczynamy");
        //FoxySessionData sessionData = getFoxySessionData();
        Map<String, IBeanConnectionThreadHandle> cancellableExecutions = getCancellableExecutionsInSession();

        IBeanConnectionThreadHandle handle = cancellableExecutions.get(ticket);

        if (handle == null) {
            //System.out.println("cancelDBJob: nie ma co przerywać");
            return false;
        }

        //System.out.println("cancelDBJob: przerywam");
        handle.cancelExecution();
        //System.out.println("cancelDBJob: przerwane!");

        return true;
    }

    protected void setServiceExtraResponseData(String data) {
        sessionDataProvider.setServiceExtraResponseData(data);
    }

    protected String getFoxyAppInstanceId() {
        return sessionDataProvider.getFoxyAppInstanceId();
    }

    protected String getClientLoggedUserName() {
        return sessionDataProvider.getClientLoggedUserName();
    }

    public long getServiceMethodInvokeCurrentMillis() {
        return sessionDataProvider.getServiceMethodInvokeCurrentMillis();
    }

    protected String getRequestHeader(String name) {
        return sessionDataProvider.getRequestHeader(name);
    }

    protected void setSessionAttribute(String name, Object val) {
        sessionDataProvider.setSessionAttribute(name, val);
    }

    protected String getSessionId() {
        return sessionDataProvider.getSessionId();
    }

    protected IFoxySessionDataProvider<SD> getSessionDataProvider() {
        return sessionDataProvider;
    }

    @Override
    public void destroy() {
        // no-op
    }
}
