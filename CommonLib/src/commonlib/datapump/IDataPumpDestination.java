/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib.datapump;

/**
 *
 * @author pmielanczuk
 */
public interface IDataPumpDestination {

    public abstract void openDestination(ColumnMetadata[] sourceColsMetadata) throws Exception;

    public abstract void closeDestination(boolean success) throws Exception;

    public abstract void writeDataRow(Object[] dataRow) throws Exception;
}
