﻿exec sp_check_version '1.1.7_A';
go

------------------------------------------------
------------------------------------------------
------------------------------------------------
----Poprzednia wersja w v1.1.0.6 pm-------------
------------------------------------------------

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs_one_table]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs_one_table
go

create procedure [dbo].[sp_verticalize_node_attrs_one_table](@source varchar(max), @nodeIdCol sysname, @cols varchar(max),
  @properNames varchar(max), @optNodeFilter varchar(max))
as
begin
  set nocount on
  
  declare @diags_level int = 0 -- 0 oznacza brak logowania

  -- diag
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': start, @source=' + @source

  --------------
  -- 1. rozbijamy nazwy atrybutów - nazwy kolumn w źródle i nazwy właściwe (opcjonalne)
  --------------

  declare @attrPropNamesTab table (idx int primary key, name sysname not null)
  
  insert into @attrPropNamesTab (idx, name)
  select idx, str from dbo.fn_split_by_sep(@properNames, ',', 7)
  
  declare @attrNamesTab table (name sysname not null primary key, 
    proper_name varchar(255) not null, search_weight int not null,
    attr_id int null
  )
  
  insert into @attrNamesTab (name, proper_name, search_weight,
    attr_id)
  select aaa.str, coalesce(apnt.name, aaa.str), coalesce(a.search_weight, 1),
    a.id
  from dbo.fn_split_by_sep(@cols, ',', 7) aaa left join @attrPropNamesTab apnt on aaa.idx = apnt.idx
    left join 
      bik_searchable_attr a on a.name = coalesce(apnt.name, aaa.str)

  --------------
  -- 2. znamy nazwy atrybutów i node_kindy, teraz uzupełniamy bik_attribute, ew. poprawiamy tam typy
  --------------
  
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed uzupełnieniem searchable attrs'
  
  insert into bik_searchable_attr (name, caption, search_weight)
  select ant.proper_name, ant.proper_name, 1
  from @attrNamesTab ant
  where ant.attr_id is null  
  
  update @attrNamesTab set attr_id = a.id
  from @attrNamesTab aaa inner join bik_searchable_attr a on aaa.attr_id is null and a.name = aaa.proper_name
  
  --------------
  -- 3. wrzucamy wartości atrybutów do tabeli pomocniczej
  --------------

  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przygotowanie do wrzucania do @attrValsTab'
  
  declare @attrValsTab table (attr_id int not null, node_id int not null, value varchar(max) null, 
    --type int not null, 
    search_weight int not null, tree_id int not null, node_kind_id int not null, unique (attr_id, node_id))
  
  declare attrCur cursor for select name, --attr_type, 
  proper_name, search_weight, attr_id from @attrNamesTab
  
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed open attrCur'

  if @diags_level > 0 begin
    select name, --attr_type, 
    proper_name, search_weight, attr_id from @attrNamesTab
  end

  open attrCur
  
  declare @attrName sysname, @attrType int, @attrProperName varchar(255), @searchWeight int, @attr_id int
  declare @attrValSql varchar(max)
  
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed pierwszym fetch next from attrCur'

  fetch next from attrCur into @attrName, --@attrType, 
    @attrProperName, @searchWeight, @attr_id
  
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed pętlą'

  while @@fetch_status = 0
  begin
    if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': start pętli dla pola: ' + @attrName + ' (' + @attrProperName + ')'
    
    set @attrValSql = 
    'select attr_id, node_id, /*case when is_deleted = 1 or value is null or ltrim(rtrim(value)) = '''' then null else value end*/ value, search_weight,' +
    ' tree_id, node_kind_id from (' +
    'select ' + cast(@attr_id as varchar(20)) + ' as attr_id, x.' + @nodeIdCol + ' as node_id, x.' + @attrName + ' as value, ' + --', a.type, ' + 
    --cast(@searchWeight as varchar(20)) + ' * (n.search_rank + nk.search_rank) * 100 + n.vote_sum as search_weight, ' +
    'dbo.fn_calc_searchable_attr_val_weight(' + cast(@searchWeight as varchar(20)) + ', n.search_rank, nk.search_rank, n.vote_sum) as search_weight, ' + 
    + 'case when n.is_deleted = 0 and (n.linked_node_id is null or n.disable_linked_subtree <> 0) then 0 else 1 end is_deleted,' + 
      ' n.tree_id, n.node_kind_id' +
      ' from ' + @source + ' as x inner join bik_node n on n.id = x.' + @nodeIdCol +
      ' inner join bik_node_kind nk on n.node_kind_id = nk.id' +
      --' inner join bik_searchable_attr a on n.node_kind_id = a.node_kind_id and a.name = ''' + @attrProperName + '''' +
      --' where x.' + @attrName + ' is not null' +
      case when @optNodeFilter is null then '' else ' where ' + @optNodeFilter end +
      ' ) x where not (is_deleted = 1 or value is null or ltrim(rtrim(value)) = '''')'
    if @diags_level > 0 print 'sql for attr=' + @attrName + ' is: 
    ' + @attrValSql
  
    insert into @attrValsTab (attr_id, node_id, value, --type, 
    search_weight, tree_id, node_kind_id)
    exec (@attrValSql)
  
    if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed końcem iteracji pętli dla pola: ' + @attrName
    
    fetch next from attrCur into @attrName, --@attrType, 
      @attrProperName, @searchWeight, @attr_id
  end
    
  close attrCur
  
  deallocate attrCur
  
  --------------
  -- 5. aktualizacja bik_searchable_attr_val - na podstawie wyciągniętych wartości
  --------------
  
  if @diags_level > 0 begin
    declare @avtCnt int = (select count(*) from @attrValsTab)
    print cast(sysdatetime() as varchar(23)) + ': za pętlą, kursor zamknięty, liczba wartości razem: ' +
      cast(@avtCnt as varchar(20)) + ', przed delete'
  end
  
  declare @attrIdsStr varchar(max) = ''
  update @attrNamesTab set @attrIdsStr = @attrIdsStr + ',' + cast(attr_id as varchar(20))
  set @attrIdsStr = substring(@attrIdsStr, 2, len(@attrIdsStr))
  
  declare @attrValsInRangeSql varchar(max) = 'select av.id, av.node_id, av.attr_id 
  from bik_searchable_attr_val av' + case when @optNodeFilter is null then '' else ' inner join bik_node n on n.id = av.node_id and ' + @optNodeFilter end +
  ' where av.attr_id in (' + @attrIdsStr + ')'
  
  declare @attrValsInRange table (id int not null primary key, node_id int not null, attr_id int not null, unique (node_id, attr_id))
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': przed insert into @attrValsInRange, sql=' + @attrValsInRangeSql
  end 
  
  insert into @attrValsInRange (id, node_id, attr_id)
  exec(@attrValsInRangeSql)
  
  declare @delCnt int
/*
  delete from bik_searchable_attr_val
  from @attrValsTab avt 
  where avt.value is null and bik_searchable_attr_val.node_id = avt.node_id and bik_searchable_attr_val.attr_id = avt.attr_id
*/
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': przed właściwym delete'
  end 
  
  delete from bik_searchable_attr_val
  from @attrValsInRange avir left join @attrValsTab av on avir.attr_id = av.attr_id and avir.node_id = av.node_id
  where bik_searchable_attr_val.id = avir.id and av.node_id is null
  set @delCnt = @@rowcount
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': po delete, rowcnt=' + cast(@delCnt as varchar(20)) + ', przed update'
  end

  update bik_searchable_attr_val
  set value = avt.value, 
    fixed_value = case when bik_searchable_attr_val.value <> avt.value then dbo.fn_normalize_text_for_fts(avt.value) else fixed_value end,
    search_weight = avt.search_weight,
    tree_id = avt.tree_id , node_kind_id = avt.node_kind_id
  from @attrValsTab avt
  where avt.value is not null and bik_searchable_attr_val.node_id = avt.node_id and bik_searchable_attr_val.attr_id = avt.attr_id and
     (bik_searchable_attr_val.value <> avt.value or bik_searchable_attr_val.search_weight <> avt.search_weight
     or bik_searchable_attr_val.tree_id <> avt.tree_id or bik_searchable_attr_val.node_kind_id <> avt.node_kind_id )
  
  declare @updRowCnt int = @@rowcount
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': po update, row count: ' +
      cast(@updRowCnt as varchar(20)) + ', przed insert'
  end
  
  --------------
  -- 6. dorzucenie nowych wartości
  --------------
  
  insert into bik_searchable_attr_val (node_id, attr_id, value, fixed_value, search_weight, tree_id, node_kind_id)
  select avt.node_id, avt.attr_id, avt.value, dbo.fn_normalize_text_for_fts(avt.value), avt.search_weight, avt.tree_id, avt.node_kind_id
  from @attrValsTab avt left join bik_searchable_attr_val av on
  avt.attr_id = av.attr_id and avt.node_id = av.node_id
  where av.id is null and avt.value is not null

  declare @insRowCnt int = @@rowcount

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': po insert, row count: ' +
      cast(@insRowCnt as varchar(20)) + ', KONIEC!'
  end
  
  -- select * from @attrValsTab
  
  --declare @sql varchar(max) = 'select ' + @nodeIdCol + ', ' + @normalCols + ', ' + @identCols + ' from ' + @source
  --print @sql
  
  --select * from @attrNamesTab
  --select * from @attrNodeKindsTab ankt inner join bik_node_kind nk on ankt.node_kind_id = nk.id
end
go



---------------------------------------------
---------------------------------------------
---------------------------------------------
----Poprzednia w v1.0.88.12 ww---------------
---------------------------------------------


IF EXISTS (SELECT name FROM sysobjects
      WHERE name = 'trg_bik_node_chunking' AND type = 'TR')
   DROP TRIGGER trg_bik_node_chunking
GO

CREATE TRIGGER trg_bik_node_chunking
ON bik_node
FOR insert, update, delete AS
begin
    -- diag: print cast(sysdatetime() as varchar(23)) + ': start!'

set nocount on

declare @diags_level int = 0;

declare @to_delete table (id int not null primary key);

declare @bik_node_name_chunk table (node_id int not null, tree_id int not null, chunk_txt varchar(3) not null)

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before calc @to_delete'
    end
   
insert into @to_delete (id)    
select d.id from deleted d left join inserted i on d.id = i.id
where i.name is null or i.name <> d.name or i.chunked_ver <> d.chunked_ver or (d.is_deleted = 0 and i.is_deleted = 1) or i.tree_id <> d.tree_id
   
   declare @rowcnt int;
   
    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before delete by ids'
      set @rowcnt = (select count(*) from @to_delete)
      print cast(sysdatetime() as varchar(23)) + ': before delete by ids, rowcnt=' + cast(@rowcnt as varchar(20))
    end
   
delete from bik_node_name_chunk where node_id in (select id from @to_delete
--select d.id from deleted d left join inserted i on d.id = i.id
--where i.name is null or i.name <> d.name or i.chunked_ver <> d.chunked_ver or (d.is_deleted = 0 and i.is_deleted = 1)
);

declare @to_insert table (id int not null, name varchar(max) not null, tree_id int not null);

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before calc @to_insert'
    end
   
insert into @to_insert (id, name, tree_id)
select i.id, i.name, i.tree_id from inserted i left join deleted d on d.id = i.id
where d.name is null or i.name <> d.name or i.chunked_ver <> d.chunked_ver or (d.is_deleted = 1 and i.is_deleted = 0) or i.tree_id <> d.tree_id;

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before declare cursor'
      set @rowcnt = (select count(*) from @to_insert)
      print cast(sysdatetime() as varchar(23)) + ': before declare cursor, rowcnt=' + cast(@rowcnt as varchar(20))
    end
   
declare nodes cursor for select id, name, tree_id from @to_insert;
/*select id, name, tree_id from bik_node where id in (
select i.id from inserted i left join deleted d on d.id = i.id
where d.name is null or i.name <> d.name or (d.is_deleted = 1 and i.is_deleted = 0)
)*/;

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before open cursor'
    end
   
open nodes;

declare @id int, @name varchar(8000), @tree_id int;

  if @diags_level > 0 begin
  -- diag:
    print cast(sysdatetime() as varchar(23)) + ': before first fetch'
  end
 
fetch next from nodes into @id, @name, @tree_id;

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before loop'
    end
   
    declare @cnt int = 0;
   
while @@fetch_status = 0
begin
  insert into @bik_node_name_chunk (node_id, chunk_txt, tree_id)
  select distinct @id, chunk_str, @tree_id
  from dbo.fn_split_string(@name, 3);
 
    if @diags_level > 1 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': in loop after insert'
    end
   
  set @cnt = @cnt + 1;
 
  if (@cnt % 1000 = 0) begin
 
    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': in loop, before insert into bik_node_name_chunk, cnt:' + cast(@cnt as varchar(10))
    end
   
    insert into bik_node_name_chunk (node_id, chunk_txt, tree_id)
    select node_id, chunk_txt, tree_id
    from @bik_node_name_chunk
   
    delete from @bik_node_name_chunk
  end;   
 
  if @diags_level > 1 begin
     -- diag:
     print cast(sysdatetime() as varchar(23)) + ': in loop before fetch next'
  end
 
  fetch next from nodes into @id, @name, @tree_id;
  if @diags_level > 1 begin
    -- diag:
    print cast(sysdatetime() as varchar(23)) + ': in loop after fetch next'
  end
 
end;

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': after loop, before insert into bik_node_name_chunk, @cnt=' + cast(@cnt as varchar(10))
    end
   
    insert into bik_node_name_chunk (node_id, chunk_txt, tree_id)
    select node_id, chunk_txt, tree_id
    from @bik_node_name_chunk
     
    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': closing, deallocating cursor'
    end
   
close nodes;

deallocate nodes;

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': done, stop!'
    end
end
go 


exec sp_verticalize_node_attrs null
go

exec sp_update_version '1.1.7_A', '1.1.7.1';
go