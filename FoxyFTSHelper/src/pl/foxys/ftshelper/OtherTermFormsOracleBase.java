/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.ftshelper;

import commonlib.LameUtils;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.trzy0.foxy.commons.FTSWordOrigin;
import pl.trzy0.foxy.commons.IOtherTermFormsOracle;
import simplelib.BaseUtils;
import simplelib.BaseUtils.Projector;
import simplelib.Pair;

/**
 *
 * @author wezyr
 */
public class OtherTermFormsOracleBase implements IOtherTermFormsOracle {

    private Map<String, Set<String>> otherFormsMap = new HashMap<String, Set<String>>();
    private Map<Pair<String, EnumSet<FTSWordOrigin>>, Set<Pair<String, FTSWordOrigin>>> cache = new HashMap<Pair<String, EnumSet<FTSWordOrigin>>, Set<Pair<String, FTSWordOrigin>>>();
    public static final int OTFOB_VERSION = 1;

    protected void addConnectedTerms(String... terms) {
        for (String term : terms) {
            Set<String> lst = otherFormsMap.get(term);
            if (lst == null) {
                lst = new LinkedHashSet<String>();
                otherFormsMap.put(term, lst);
            }
            for (String otherTerm : terms) {
                if (otherTerm != term) {
                    lst.add(otherTerm);
                }
            }
        }
    }

    protected synchronized Set<Pair<String, FTSWordOrigin>> cacheGet(String term, EnumSet<FTSWordOrigin> kinds) {
        return cache.get(new Pair<String, EnumSet<FTSWordOrigin>>(term, kinds));
    }

    protected synchronized void cachePut(String term, EnumSet<FTSWordOrigin> kinds, Set<Pair<String, FTSWordOrigin>> finalForms) {
        cache.put(new Pair(term, kinds), finalForms);
    }

    public Set<Pair<String, FTSWordOrigin>> getOtherForms(String term) {
        return getOtherForms(term, IOtherTermFormsOracle.allButOriginal);
    }

    protected List<String> getDictStemmedWords(String srcWord) {
        return null;
    }

    protected List<String> getAlgoStemmedWords(String srcWord) {
        return null;
    }

    protected String getAccentFreeWord(String srcWord) {

        return LameUtils.deAccentExAndLowerCase(srcWord);

        //return srcWord;
    }

    @Override
    public Set<Pair<String, FTSWordOrigin>> getOtherForms(String term, EnumSet<FTSWordOrigin> kinds) {
        Set<Pair<String, FTSWordOrigin>> res = cacheGet(term, kinds);
        if (res != null) {
            return res;
        }
        res = new LinkedHashSet<Pair<String, FTSWordOrigin>>();
        if (kinds.contains(FTSWordOrigin.Original)) {
            res.add(new Pair(term, FTSWordOrigin.Original));
        }
        if (kinds.contains(FTSWordOrigin.Synonym)) {
            addStringsWithKind(res, otherFormsMap.get(term), FTSWordOrigin.Synonym);
        }

        if (kinds.contains(FTSWordOrigin.AccentFree)) {
            String accFree = getAccentFreeWord(term);
            if (accFree != null && !accFree.equals(term)) {
                res.add(new Pair<String, FTSWordOrigin>(accFree, FTSWordOrigin.AccentFree));
            }
        }

        if (kinds.contains(FTSWordOrigin.AlgoStemmed)) {
            addStringsWithKind(res, getAlgoStemmedWords(term), FTSWordOrigin.AlgoStemmed);
        }

        if (kinds.contains(FTSWordOrigin.DictStemmed)) {
            addStringsWithKind(res, getDictStemmedWords(term), FTSWordOrigin.DictStemmed);
        }

        cachePut(term, kinds, res);
        return res;
    }

    private void addStringsWithKind(Collection<Pair<String, FTSWordOrigin>> res, Collection<String> strs, final FTSWordOrigin kind) {
        BaseUtils.addAllWithProjector(res, strs, new Projector<String, Pair<String, FTSWordOrigin>>() {

            public Pair<String, FTSWordOrigin> project(String val) {
                return new Pair<String, FTSWordOrigin>(val, kind);
            }
        });
    }

    public String getNameAndVer() {
        return BaseUtils.getPrettyClassName(this) + ":" + getVersionNumber();
    }

    protected int getVersionNumber() {
        return OTFOB_VERSION;
    }

//    public static void main(String[] args) {
//        String xxx = LameUtils.LOWERPL + LameUtils.UPPERPL + "Ȳ";
//        System.out.println("xxx=" + xxx + ", deAccented=" + LameUtils.deAccentExAndLowerCase(xxx));
//        main2(xxx);
//    }
//
//    public static void main2(String s) {
//        final Form[] forms = {Form.NFC, Form.NFD, Form.NFKC, Form.NFKD};
//        try {
//            for (Form f : forms) {
//                if (Normalizer.isNormalized(s, f)) {
//                    System.out.println(s + " is already normalized with " + f.toString());
//                }
//                {
//                    System.out.println("after normalizing with " + f.toString() + ": "
//                            + LameUtils.deAccent(s, f));
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
    private static class SingletonHolder {

        private static final OtherTermFormsOracleBase INSTANCE = new OtherTermFormsOracleBase();
    }

    public static OtherTermFormsOracleBase getInstance() {
        return SingletonHolder.INSTANCE;
    }
}
