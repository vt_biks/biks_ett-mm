/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.glass;

import java.util.HashMap;
import java.util.Map;
import pl.trzy0.foxy.commons.templates.INamedTemplateProvider;
import pl.trzy0.foxy.commons.templates.TemplateUtils;
import simplelib.BaseUtils;

/**
 *
 * @author wezyr
 */
public class GlassToText {

    private INamedTemplateProvider tmplProvider;
    private String resourceKeyPrefix;

    public GlassToText(String resourceKeyPrefix, INamedTemplateProvider tmplProvider) {
        this.tmplProvider = tmplProvider;
        this.resourceKeyPrefix = resourceKeyPrefix;
    }

    // append prefix to key if necessary
    protected String keyAsString(Object key) {
        String str = BaseUtils.safeToString(key);
        if (resourceKeyPrefix != null && str != null) {
            str = resourceKeyPrefix + str;
        }
        return str;
    }

    public String parse(Glass glass, Object model) {
        //System.out.println("glass=" + glass);
        if (glass == null) {
            return null;
        }

        String resourceKey = keyAsString(glass.getKey());

        if (resourceKey == null) {
            return null;
        }

        Map<String, Object> vars = new HashMap<String, Object>();
        for (int i = 0; i < glass.getArgCount(); i++) {
            String argKey = "p" + i;
            Object argValue;
            Object arg = glass.getArg(i);

            if (arg instanceof Glass) {
                argValue = parse((Glass) arg, model);
            } else {
                argValue = arg;
            }

            vars.put(argKey, argValue);
        }

        return TemplateUtils.render(tmplProvider, resourceKey, vars, model);
    }

    public static String asText(INamedTemplateProvider tmplProvider, Glass glass, String resKeyPrfx) {
        return asTextEx(tmplProvider, glass, resKeyPrfx, null);
    }

    public static String asTextEx(INamedTemplateProvider tmplProvider, Glass glass, String resKeyPrfx, Object model) {
        return new GlassToText(resKeyPrfx, tmplProvider).parse(glass, model);
    }
}
