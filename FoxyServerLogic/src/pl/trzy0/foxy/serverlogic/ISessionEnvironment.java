/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import commonlib.ILameResourceProvider;

/**
 *
 * @author wezyr
 */
public interface ISessionEnvironment {

    public String getCaptchaText();

//    public String getHttpBaseAddr();

    public ILameResourceProvider getResourceProvider();
}
