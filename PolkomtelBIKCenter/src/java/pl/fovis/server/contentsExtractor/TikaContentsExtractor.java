/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.contentsExtractor;

import java.io.File;
import org.apache.tika.Tika;

/**
 *
 * @author pmielanczuk
 */
public class TikaContentsExtractor extends BaseContentExtractor {

//    protected Tika tika = new Tika();
//    protected Class tikaClass;
    public TikaContentsExtractor(String pathToTikaJar, String fileExtensions) {
        super(fileExtensions);
//        try {
//            tikaClass = loadTikaClass(pathToTikaJar);
//        } catch (Exception ex) {
//            throw new RuntimeException(ex);
//        }
    }

//    protected static Class loadTikaClass(String pathToTikaJar) throws Exception {
////        File file = new File("C:\\projekty\\svn\\PolkomtelBIKCenter\\libs\\tika-app-1.11.jar");
//        File file = new File(pathToTikaJar);
//
//        URL url = file.toURI().toURL();
//        URL[] urls = new URL[]{url};
//
//        ClassLoader cl = new URLClassLoader(urls);
//        Class cls = cl.loadClass("org.apache.tika.Tika");
//        return cls;
//    }
//    protected static Object newTika(Class c) throws Exception {
//        return c.newInstance();
//    }
//    protected static String runTika(Class c, String fullFilePath) throws Exception {
//        Object tika = newTika(c);
//        Tika tika = new Tika();
//
//        Method m = tika.getClass().getMethod("parseToString", File.class);
//        Object res = m.invoke(tika, new File(fullFilePath));
//        return (String) res;
//    }
    @Override
    public String extractContents(String fullFilePath) {
        try {
            System.out.println("before tika.parse, fullFilePath=" + fullFilePath);
            String res = new Tika().parseToString(new File(fullFilePath));
//            String res = runTika(tikaClass, fullFilePath);
//            String res = runTika(fullFilePath);
            System.out.println("done tika.parse, fullFilePath=" + fullFilePath);
            return res;
        } catch (Exception ex) {
            throw new RuntimeException("error in extractContents for file=\"" + fullFilePath + "\"", ex);
        }
    }

    public static void main(String[] args) throws Exception {
//        Class tikaClass = loadTikaClass();
//        String res = runTika(tikaClass, "c:/temp/bikcenter-upload/file_2638751705764413807.doc");
        String res = new TikaContentsExtractor("C:\\projekty\\svn\\PolkomtelBIKCenter\\libs\\tika-app-1.11.jar", "*")
                .extractContents("c:/temp/bikcenter-upload/file_2638751705764413807.doc");
        System.out.println("res=" + res);
    }
}
