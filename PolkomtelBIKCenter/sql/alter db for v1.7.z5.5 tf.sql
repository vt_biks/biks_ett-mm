﻿exec sp_check_version '1.7.z5.5';
go

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_drop_tables]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_drop_tables]
go

create procedure [dbo].[sp_drop_tables] (@tablesNames varchar(max)) as
begin
  set nocount on;

	------------------------------------------------------------
	-- Tabele do usunięcia podajemy po przecinku
	-- 
	-- Proces usunięcia tabel realizowany jest w nastepujących krokach:
	-- 1. sprawdzanie czy jakieś FK nie odwołuje się z poza listy tabel - wtedy rzucamy błąd
	-- 2. usunięcie FK, do i z tabel, które usuwamy
	-- 3. usunięcie tabel
	-------------------------------------------------------------

	--declare @tablesNames varchar(max) = 'a00_global_props,a00_number,a00_teradata_test,aa0_node,aa0_node_connection,aa0_node_kind,aaa_additional_descr,aaa_attribute,aaa_attribute_linked,aaa_aux_teradata_object,aaa_aux_tree_node'
	declare @tablesToDrop table(name varchar(512));
	declare @dropSql varchar(max) = '';

	insert into @tablesToDrop(name)
	select str from dbo.fn_split_by_sep(@tablesNames, ',', 7) where exists(select * from sys.objects where object_id = object_id(str) and type in (N'U'));

	if exists(select * from vw_ww_foreignkeys where ReferencedTableName in (select Name from @tablesToDrop) and Table_Name not in (select Name from @tablesToDrop))
	begin
		print 'Błąd! Próbujesz usunąć tabelę, do której odwołują się inne tabele (poprzez klucze obce)!'
		return;
	end;

	set @dropSql = '';
	select @dropSql = @dropSql + 'alter table ' + Table_Name + ' drop constraint ' + ForeignKey_Name + '; ' from vw_ww_foreignkeys where ReferencedTableName in (select Name from @tablesToDrop) or Table_Name in (select Name from @tablesToDrop)
	--select @dropSql
	exec (@dropSql);

	set @dropSql = '';
	select @dropSql = @dropSql + 'drop table ' + name + '; ' from @tablesToDrop
	--select @dropSql
	exec (@dropSql);

end;
go

-- usuwamy nieużywane tabele BIKSowa
declare @tables varchar(max) = 'a00_global_props,a00_number,a00_teradata_test,aa0_node,aa0_node_connection,aa0_node_kind,aaa_additional_descr,aaa_attribute,aaa_attribute_linked,aaa_aux_teradata_object,aaa_aux_tree_node,aaa_blog_entry,aaa_blog_entry_entity,aaa_custom_prop,aaa_data_load_log,aaa_data_source_def,aaa_entity_in_tree_node,aaa_entity_keyword,aaa_joined_objs,aaa_keyword,aaa_linked_resource,aaa_mpart,aaa_mpart_keyword,aaa_mpart_linked,aaa_note,aaa_object_in_fvs,aaa_object_in_history,aaa_specialist,aaa_tree_node,aaa_user,bik_metapedia_keyword,bik_metapedia_linked,bik_keyword,bik_keyword_linked,bik_biz_def,bik_biz_def_body,bik_def_dependency,bik_monitor,bik_specialist,';
select @tables = @tables + name + ',' from sys.tables where type = 'U' and (name like 'INFO[_]%' or name like 'APP[_]%' or name like 'SYS[_]%') 
order by name

exec sp_drop_tables @tables;
go

exec sp_update_version '1.7.z5.5', '1.7.z5.6';
go
