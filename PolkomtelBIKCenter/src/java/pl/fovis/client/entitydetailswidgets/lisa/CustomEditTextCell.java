/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.lisa;

import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;

/**
 *
 * @author ctran
 */
//pierwszy wiersz zawsze jest edytowalny
public class CustomEditTextCell extends EditTextCell {

    private LisaDataGrid dataGrid;
    private final boolean isEditableColumn;

    public CustomEditTextCell(LisaDataGrid dataGrid, boolean isEditableColumn) {
        super();
        this.dataGrid = dataGrid;
        this.isEditableColumn = isEditableColumn;
    }

    @Override
    public void onBrowserEvent(Context context, Element parent, String value, NativeEvent event, ValueUpdater<String> valueUpdater) {
        if (!dataGrid.isInEditMode() && (context.getIndex() > 0 || !dataGrid.isClickedAdd())) {
            return;
        }
        if (context.getIndex() > 0 && !isEditableColumn) {
            return;
        }
        super.onBrowserEvent(context, parent, value, event, valueUpdater); //To change body of generated methods, choose Tools | Templates.
    }
}
