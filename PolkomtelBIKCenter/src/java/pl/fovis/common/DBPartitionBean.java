/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author tflorczak
 */
public class DBPartitionBean implements Serializable {

    public String columnName;
    public String partitionSchema;
    public String partitionFunction;
    public String partitionFunctionType;
    public String indexName;
    // technical
    public String icon;
    public String treeCode;
    public int nodeId;
}
