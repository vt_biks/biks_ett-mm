/*
 * TestCommonLib.java
 *
 * Created on 28 grudzie� 2006, 10:57
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package commonlib;

import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author wezyr
 */
public class TestCommonLib {
    
    /** Creates a new instance of TestCommonLib */
    public TestCommonLib() {
    }
    
    public static void testGetMethod() throws NoSuchMethodException {
        Method m = TestCommonLib.class.getMethod("main", String[].class);
        if (m == null) System.out.println("m is null");
        else System.out.println("m is not null!!!");
    }
    
    public static void testGetMethods(Class c) {
        Method[] ms = c.getDeclaredMethods();
        for (Method m : ms) {
            System.out.println(MuppetMaker.toDiagString(m));
        }
    }

    public static void testGetFields(Class c) {
        Field[] fs = c.getDeclaredFields();
        for (Field f : fs) {
            System.out.println(Modifier.toString(f.getModifiers()) + " " + f.getType().getCanonicalName() + " " + f.getName() + ";");
        }
    }
    
    public void setFldDyn(String val) {
        System.out.println("setFldDyn(" + val + ")");
        fldDyn = val;
    }
    
    public void setFldDynB(Boolean val) {
        System.out.println("setFldDynB(" + val + ")");
        fldDynB = val;
    }
    
    public static void testSetProperty() {
        TestCommonLib tcl = new TestCommonLib();
        MuppetMaker.setProperty(tcl, "fldDyn", "aaa");
        System.out.println("fldDyn: " + tcl.fldDyn);
        MuppetMaker.setProperty(tcl, "fldStat", "113");
        System.out.println("fldStat: " + tcl.fldStat);
        MuppetMaker.setProperty(tcl, "fldDynB", "false");
        System.out.println("fldDynB: " + tcl.fldDynB);
        MuppetMaker.setProperty(tcl, "fldDynB2", "true");
        System.out.println("fldDynB2: " + tcl.fldDynB2);
    }
    
    public static void testNewMuppet() {
        Map<String, Object> m = new HashMap<String, Object>();
        m.put("fldDyn", "aaaaaX");
        m.put("fldDynB", "false");
        m.put("fldDynB2", "true");
        m.put("fldStat", "-103");
        Map<String, Object> m2 = new HashMap<String, Object>(m);
        m.put("tcl", m2);
        
        TestCommonLib tcl = MuppetMaker.newMuppet(TestCommonLib.class, m);
    }
    
    public static void testReadMap() throws Exception {
        //System.out.println(XMLMapper.readMap(new StringReader("<aa x='y'>aaa<bb z='v'/></aa>")));
        System.out.println(XMLMapper.readMap("<aa x='y'>aaa<bb z='v'/></aa>".getBytes("UTF-8"), null));
    }
    
    public String fldDyn;
    public Boolean fldDynB;
    public boolean fldDynB2;
    public static int fldStat;
    private static int fldStat2;
    public TestCommonLib tcl;
    public TestCommonLib[] tcls;
    
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("fldDyn: " + LameUtils.safeToStringTxt(fldDyn) + ",");
        sb.append("fldDynB2: " + LameUtils.safeToStringTxt(fldDynB2) + ",");
        sb.append("tcls: ").append(LameUtils.arrayToDiagString(tcls));
        return sb.toString();
    }
    
    public static void testReadMapToMuppet() throws Exception {
        //Map m = XMLMapper.readMap(new StringReader("<aa fldStat='-103'><tcls><tcl fldDynB2='true'/><tcl fldDynB2='true'/></tcls></aa>"));
        Map<String, Object> m = XMLMapper.readMap("<aa fldStat='-103'><tcls><tcl fldDynB2='true'/><tcl fldDynB2='true'/></tcls></aa>".getBytes("UTF-8"), null);
        System.out.println(m);

        TestCommonLib tcl = MuppetMaker.newMuppet(TestCommonLib.class, m);
        System.out.println(tcl.toString());
    }

    public static void testHasSubMap() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("x", 10);
        map.put("y", "zzz");

        Map<String, Object> submap = new HashMap<String, Object>();
        submap.put("y", "zzz");
        
        if (LameUtils.hasSubMap(map, submap))
            System.out.println("ok");
        else
            System.out.println("zle");
    }
    
    
    public static void testGetMonth() {
        System.out.println(new Date());
        System.out.println(DateUtils.getDayOfDate(new Date()));        
        System.out.println(DateUtils.getMonthOfDate(new Date()));
        System.out.println(DateUtils.getYearOfDate(new Date()));
    }
    
    public static void main(String[] args) throws Exception {
        //testGetMethods(LameUtils.getCurrentClass());
        //testGetFields(LameUtils.getCurrentClass());
        //testSetProperty();
        //testNewMuppet();
        //testReadMap();
        //testReadMapToMuppet();
        //testHasSubMap();
        testGetMonth();
    }
}
