/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Widget;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.TreeNodeBranchBean;
import pl.fovis.foxygwtcommons.GWTUtils;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.LameBag;

/**
 *
 * @author pmielanczuk
 */
public class TreeSelectorForRoleRestrictionsDialog extends TreeSelectorForExpliciteTreeCodesDialog {

    protected IParametrizedContinuation<Set<TreeNodeBranchBean>> saveRestrictionsCont;
    protected String roleCaption;
    protected Set<TreeNodeBranchBean> selectedNodes;
    protected Map<TreeNodeBranchBean, CheckBox> nodeToCheckBoxMap = new HashMap<TreeNodeBranchBean, CheckBox>();
    protected LameBag<TreeNodeBranchBean> selectedDescendantCounts = new LameBag<TreeNodeBranchBean>();
    protected Map<TreeNodeBranchBean, List<TreeNodeBranchBean>> childNodeMap = new HashMap<TreeNodeBranchBean, List<TreeNodeBranchBean>>();

    public void buildAndShowDialog(String roleCaption, Set<String> treeCodes, Set<TreeNodeBranchBean> selectedNodes,
            IParametrizedContinuation<Set<TreeNodeBranchBean>> saveCont) {
        this.roleCaption = roleCaption;
        this.saveRestrictionsCont = saveCont;
        this.selectedNodes = BaseUtils.safeNewHashSet(selectedNodes);
        setupSelectedDescendantCounts();
        super.buildAndShowDialog(null, treeCodes, null, null);
        actionBtn.setEnabled(true);
    }

    protected TreeNodeBranchBean getParentNode(TreeNodeBranchBean node) {
        if (node.nodeId == null) {
            return null;
        }

        List<String> s = BaseUtils.splitBySep(node.branchIds, "|", true);

        final int nodeIdIdx = s.size() - 2;

        // zabiezpieczenie przed dziwna akcją typu pusty string, typu "123", "123|"
        // dopiero "123|456|" będzie OK, bo daje 3 elementy
        if (nodeIdIdx < 0) {
            return null;
        }

        if (nodeIdIdx == 0) {
            // czyli było "123|" -> parentem jest samo drzewo
            return new TreeNodeBranchBean(node.treeId, null, null);
        }

        // tutaj mamy przynajmniej "123|456|" - 3 elementy
        Integer parentId = BaseUtils.tryParseInteger(s.get(nodeIdIdx - 1), -1);
        s.remove(nodeIdIdx);

        return new TreeNodeBranchBean(node.treeId, parentId, BaseUtils.mergeWithSepEx(s, "|"));
    }

    protected void addOrRemoveNodeToCnts(TreeNodeBranchBean node, boolean doAdd) {
        while (node != null) {
            if (doAdd) {
                selectedDescendantCounts.add(node);
            } else {
                selectedDescendantCounts.remove(node);
            }

            CheckBox cb = nodeToCheckBoxMap.get(node);
            setStyleForCheckBox(cb, node);

            node = getParentNode(node);
        }
    }

    protected void setStyleForCheckBox(CheckBox cb, TreeNodeBranchBean node) {
        if (cb != null) {
            GWTUtils.addOrRemoveStyleName(cb, "node-has-selected-descendants", cb.isEnabled() && selectedDescendantCounts.getCount(node) > 0);
        }
    }

    protected final void setupSelectedDescendantCounts() {
        for (TreeNodeBranchBean n : selectedNodes) {
            addOrRemoveNodeToCnts(n, true);
        }
    }

    @Override
    protected void doAction() {
        saveRestrictionsCont.doIt(selectedNodes);
    }

    protected boolean isAncestorNodeSelected(TreeNodeBranchBean node) {

        while (node != null) {
            node = getParentNode(node);

            if (selectedNodes.contains(node)) {
                return true;
            }
        }

        return false;
    }

    protected boolean isAnyDescendantSelected(TreeNodeBranchBean nodePair) {
        return selectedDescendantCounts.getCount(nodePair) > 0;
    }

    protected void setEnabledForDescendants(TreeNodeBranchBean node, boolean isEnabled) {
        List<TreeNodeBranchBean> childNodes = childNodeMap.get(node);
        if (childNodes == null) {
            return;
        }

        for (TreeNodeBranchBean childNode : childNodes) {
            CheckBox childCb = nodeToCheckBoxMap.get(childNode);
            if (childCb != null) {
                childCb.setEnabled(isEnabled);
                setStyleForCheckBox(childCb, childNode);
            }

            setEnabledForDescendants(childNode, isEnabled);
        }
    }

    // zawsze na końcu niepustego branchIds jest "|"
    // np. 1234|
    //     34561|1111|
    protected String getProperBranchIds(String branchIds) {
        List<String> s = BaseUtils.splitBySep(branchIds, "|", true);

        // s.size() - 1, bo zawsze jest pałka na końcu
        int i = 0;
        final int properItemCnt = s.size() - 1;
        for (; i < properItemCnt; i++) {
            if (!BaseUtils.strHasPrefix(s.get(i), "-", false)) {
                break;
            }
        }

        if (i >= properItemCnt) {
            return null;
        }

        // to nam dorzuci pałkę na końcu
        return BaseUtils.mergeWithSepEx(s.subList(i, s.size()), "|");
    }

    @Override
    protected Widget createWidgetForNameColumn(final TreeNodeBean node) {
        Integer nId = node.id;
        int treeId = node.treeId;

        //showCheckboxForTreeCategories
        if (treeId == -1) {
            return new InlineLabel(node.name);
        }

        if (treeId < 0) {
            treeId = -treeId;
            nId = null;
        }

        final TreeNodeBranchBean currNode = new TreeNodeBranchBean(treeId, nId, getProperBranchIds(node.branchIds));
//        final TreeNodeBranchBean selectedNodePair = new TreeNodeBranchBean(treeId, nId, node.branchIds);

        final CheckBox cb = new CheckBox(node.name);
        nodeToCheckBoxMap.put(currNode, cb);
        TreeNodeBranchBean parentNode = getParentNode(currNode);
        BaseUtils.addToCollectingMapWithList(childNodeMap, parentNode, currNode);

        cb.setEnabled(!isAncestorNodeSelected(currNode));
        cb.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                final boolean doAdd = event.getValue();
                // działaj tylko, gdy faktycznie nastąpiła zmiana wartości checkboxa
                // w stosunku do zapamiętanego stanu
                if (selectedNodes.contains(currNode) != doAdd) {
                    BaseUtils.setAddOrRemoveItem(selectedNodes, currNode, doAdd);
                    addOrRemoveNodeToCnts(currNode, doAdd);
                    setEnabledForDescendants(currNode, !doAdd);
                }
            }
        });

        cb.setValue(selectedNodes.contains(currNode), false);
        setStyleForCheckBox(cb, currNode);

        return cb;
    }

    @Override
    protected String getDialogCaption() {
        return "Wybierz obiekty dla uprawnienia [" + roleCaption + "]";
    }

    @Override
    protected boolean isInheritToDescendantsCheckBoxVisible() {
        return false;
    }

    @Override
    protected boolean isInMultiSelectMode() {
        return true;
    }

    @Override
    protected void addPopupMenuInMultiSelectMode() {
        // no-op
    }

    @Override
    protected boolean generateNodesForTreeCategories() {
        return true;
    }

    @Override
    protected boolean isHeaderVisible() {
        return false;
    }

    @Override
    protected Set<String> getOptAllTreeCodesToShow() {
        return treeCodes;
    }

    @Override
    protected boolean isGrouppingByMenuEnabled() {
        return true;
    }
}
