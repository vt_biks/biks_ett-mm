/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package simplelib;

import java.util.Map;
import java.util.Set;

/**
 *
 * @author wezyr
 */
public class MapHelperWrapperKeyPrefixer implements IMapHelperWrapper<String> {

    private String keyPrefix;
    private Map<String, Object> map;

    public MapHelperWrapperKeyPrefixer(String keyPrefix, Map<String, Object> map) {
        this.keyPrefix = keyPrefix;
        this.map = map;
    }

    public Object getPropValue(String name) {
        String innerName = BaseUtils.dropPrefixOrGiveNull(name, keyPrefix);
        return innerName == null ? null : map.get(innerName);
    }

    public Set<String> getPropNames() {
        return BaseUtils.addPrefixToItems(map.keySet(), keyPrefix);
    }
}
