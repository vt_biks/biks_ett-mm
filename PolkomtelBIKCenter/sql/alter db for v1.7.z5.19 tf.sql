﻿exec sp_check_version '1.7.z5.19';
go


-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------


-- poprzednia wersja w pliku: alter db for v1.1.8.6 tf.sql
-- fix na inne instancje BO
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_connections_for_dqc]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_insert_into_bik_joined_objs_connections_for_dqc
go

create procedure sp_insert_into_bik_joined_objs_connections_for_dqc as
begin

	-------------- Test <-> Teradata
	exec sp_prepare_bik_joined_objs_tmp

	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
	select distinct test.id, ter.id, 1, 0
	from bik_dqc_test_parameters dtp
	join bik_node test on test.obj_id='t:' + convert(varchar(max), dtp.test_id) and test.is_deleted=0 and test.linked_node_id is null
	join bik_node ter on ter.obj_id=convert(varchar(max), dtp.parameter_name) and ter.is_deleted=0 and ter.linked_node_id is null

	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
	select distinct ter.id, test.id, 1, 0
	from bik_dqc_test_parameters dtp
	join bik_node test on test.obj_id='t:' + convert(varchar(max), dtp.test_id) and test.is_deleted=0 and test.linked_node_id is null
	join bik_node ter on ter.obj_id=convert(varchar(max), dtp.parameter_name) and ter.is_deleted=0 and ter.linked_node_id is null

	exec sp_move_bik_joined_objs_tmp 'DQCTestSuccess,DQCTestFailed,DQCTestInactive', '@Teradata'

	----------------- Test <-> BO

	create table #tmp_jo (teradata int, dqc int);
	create table #tmp_to_insert (src int, dst int);

	declare @teradataTree int = dbo.fn_tree_id_by_code('Teradata');
	declare @dqcTree int = dbo.fn_tree_id_by_code('DQC');

	insert into #tmp_jo(teradata, dqc)
	select src.id, dst.id 
	from bik_joined_objs as jo
	join bik_node as src on src.id = jo.src_node_id
	join bik_node as dst on dst.id = jo.dst_node_id
	where src.tree_id = @teradataTree and src.is_deleted = 0 and src.linked_node_id is null
	and dst.tree_id = @dqcTree and dst.is_deleted = 0 and dst.linked_node_id is null
	and jo.type = 1 -- tylko automatyczne

	insert into #tmp_to_insert(src,dst)
	select distinct bn.id, tmp.dqc 
	from #tmp_jo as tmp
	join bik_joined_objs as jo on tmp.teradata = jo.dst_node_id 
	join bik_node as bn on bn.id = jo.src_node_id
	where bn.is_deleted = 0 and linked_node_id is null and bn.id <> tmp.dqc
	and bn.tree_id in (select report_tree_id from bik_sapbo_server where is_deleted = 0 union all select universe_tree_id from bik_sapbo_server where is_deleted = 0 union all select connection_tree_id from bik_sapbo_server where is_deleted = 0)
	and jo.type = 1 -- tylko automatyczne

	exec sp_prepare_bik_joined_objs_tmp

	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
	select src, dst, 1, 0
	from #tmp_to_insert as tmp

	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
	select dst, src, 1, 0
	from #tmp_to_insert as tmp
	
	exec sp_move_bik_joined_objs_tmp 'DQCTestSuccess,DQCTestFailed,DQCTestInactive', '@Reports, @ObjectUniverses, @Connections'

end;
go

if (select val from bik_app_prop where name = 'biks_id') = '7CB602FD-4C29-4C5D-A3A9-82E814EFADD2' -- PLK
begin
	update bik_app_prop set val = replace(val, 'Business Objects 3.1,', '') where name = 'availableConnectors'
end;
go

update bik_attr_def
set attr_category_id = (select id from bik_attr_category where name = 'Ogólne')
where name = 'Wizualizacja' and attr_category_id = (select id from bik_attr_category where name = 'DQC')

if (select val from bik_app_prop where name = 'biks_id') = '5005832A-6D1D-4F70-BB6D-32146BF003AA' -- PB
begin
	update bik_dqm_test 
	set data_scope = 'ACN – nr klienta
	CIF.TYPE – typ Klienta
	ZLNAM – pełna nazwa klienta
	CIF.BOO – kod oddziału, do którego przypisany jest klient
	CID – nr rachunku,
	DEP.BOO – kod oddziału/agenta do którego przypisany jest rachunek
	BRCD – kod oddziału lub oddziału patrona agenta
	DEP.TYPE – typ produktu,
	DEP.ZOFSRC – źródło pozyskania rachunku,
	DEP.ODT – data otwarcia,
	DEP.BAL – saldo rachunku
	DEP.ACR - naliczone odsetki ogółem
	DEP.POSACAR - naliczone odsetki od salda dodatniego
	DEP.RESINT - odsetki rezydualne (od salda ujemnego)
	DEP.NEGACRUN - naliczone odsetki od salda ujemnego autoryzowanego
	DEP.NEGACR - naliczone odsetki od salda ujemngo nieautoryzowanego
	DEP.ZPROWNZ - kwota zaległych prowizji
	DEP.STAT – status rachunku,
	DEP.EMPLNO – status monitorowania,
	DEP.FEEPLN – plan prowizyjny,
	DEP.INDEX – indeks odsetkowy,
	DEP.SFRE – częstotliwość generacji wyciągów
	DEP.MF – indeks odsetkowy
	DEP.ZNBEAVL – rachunek dostępny w bankowości elektronicznej
	DEP.ZNBEAVLDT – data udostępnienia rachunku w bankowości elektronicznej
	RESTR29 – czy istnieje restrykcja 29
	AKTKARTY  - czy istnieją aktywne karty debetowe
	AKTZLEC – czy istnieją aktywne zlecenia stałe/jednorazowe
	AKTPZ – czy istnieją aktywne zgody na polecenie zapłaty' 
	where name in ('Bledy_w_parametrach_rachunkow_Profile - Centrum Sprzedaży Agencyjnej', 'Bledy_w_parametrach_rachunkow_Profile - Kanały teleinformatyczne','Bledy_w_parametrach_rachunkow_Profile - Oddziały Operacyjne','Bledy_w_parametrach_rachunkow_Profile - Klient instytucjonalny')

	update bik_dqm_test
	set result_file_name = 'KD_TELEINF_TELEFONY'
	where name = 'Poprawne_numery_telefonu_Profile - Kanały teleinformatyczne'
end;
go


if  exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_node]') and name = N'idx_bik_node_parent_deleted_tree_linked_branch')
	drop index idx_bik_node_parent_deleted_tree_linked_branch on bik_node;
go

create index idx_bik_node_parent_deleted_tree_linked_branch on [dbo].[bik_node] ([parent_node_id],[is_deleted])
  include ([tree_id],[linked_node_id],[branch_ids])
go

if not exists (select 1 from bik_app_prop where name = 'enableAttributeUsagePage')
begin
	insert into bik_app_prop(name, val, is_editable) values('enableAttributeUsagePage', 'false', 1);
end;

update bik_dqm_test
set type = bnk.code
from bik_dqm_test as te
inner join bik_node as bn on bn.id = te.test_node_id
inner join bik_node_kind as bnk on bn.node_kind_id = bnk.id
where bnk.code <> te.type


-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------


exec sp_update_version '1.7.z5.19', '1.7.z5.20';
go
