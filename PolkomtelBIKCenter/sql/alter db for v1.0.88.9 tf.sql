﻿exec sp_check_version '1.0.88.9';
go

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_insert_into_bik_node
go


create procedure [dbo].[sp_insert_into_bik_node](@tree_code varchar(255))
as
	declare @table_folders varchar(255);
	declare @tree_id int;
	declare @kinds varchar(255);
begin
	set nocount on;
	
	--print cast(sysdatetime() as varchar(22)) + ': faza 1'
	
	select @tree_id = id 
	from bik_tree
	where code=@tree_code;

	create table #primaryIds(si_id varchar(5000) primary key);

	if(@tree_code!='Reports' and @tree_code!='Teradata')
	begin
		set @table_folders = ' APP_FOLDER where 1=1 ';
		if(@tree_code = 'Connections')
		begin
			set @kinds = '''MetaData.DataConnection''';
			exec('insert into #primaryIds
					select convert(varchar,SI_ID) as SI_ID  
					from aaa_global_props
					where SI_PARENTID in (select si_id from ' + @table_folders + ') and SI_KIND in (' + @kinds + ')');			
		end;
		if(@tree_code = 'ObjectUniverses')
		begin
			set @kinds = '''Universe''';
			exec('insert into #primaryIds
					select convert(varchar,SI_ID) as SI_ID  
					from aaa_global_props
					where SI_PARENTID in (select si_id from ' + @table_folders + ') and SI_KIND in (' + @kinds + ')');
		end;		
	end
	
	if(@tree_code='Reports')
	begin
		-- wybranie folderów raportów bez raportów użytkowników
		set @table_folders = ' INFO_FOLDER where si_name != ''~Webintelligence''';
		--set @table_folders = ' INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2 is null or SI_PATH__SI_FOLDER_NAME2 != ''User Folders''';
		set @kinds = '''Webi'', ''Flash'', ''CrystalReport'',''Excel'',''FullClient'',''Pdf'',''Hyperlink''
		,''Rtf'',''Txt'',''Powerpoint'',''Word''';
	end;
	
    declare @sql nvarchar(max);
				
	declare @node_kind_id int;
	set @sql = N'';
	if(@tree_code = 'Teradata')
	begin
		--print cast(sysdatetime() as varchar(22)) + ': faza 4.1 - teradata przed sp_teradata_init_branch_names'
		exec sp_teradata_init_branch_names;
		--print cast(sysdatetime() as varchar(22)) + ': faza 4.2 - teradata po sp_teradata_init_branch_names'
		set @sql = N'select branch_names as SI_ID, 
											case 
												when SUBSTRING(branch_names, 1,len(branch_names) - len(name + ''|'')) =''''
													then null
												else SUBSTRING(branch_names, 1,len(branch_names) - len(name + ''|'')) end
												as SI_PARENTID, 
						type as SI_KIND, name as SI_NAME, extra_info as DESCR
				 from bik_teradata
				/*where parent_id is not null or name like ''VD_US_%''*/';
	end;
	else
	begin
		if(@tree_code = 'Reports')
		-- sztuczny podzial na root folder i user folders dla raportów
		set @sql = N'select distinct SI_PARENT_FOLDER as SI_ID, (select si_id from INFO_FOLDER where SI_NAME=''User Folders'') as SI_PARENTID, SI_KIND, SI_PATH__SI_FOLDER_NAME1 as SI_NAME, NULL as DESCR
				 from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2=''User Folders'' 
				 and SI_NAME!=''~Webintelligence'' 

				 union all
				
				 select si_id, case
									when SI_PARENTID=0 and si_name!=''User Folders''
										then (select si_id from INFO_FOLDER where SI_NAME=''Root Folder'')
									when si_id in(select si_id from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2 = ''User Folders'')
                                        then SI_PARENTID
									when SI_PARENTID in(select si_id from INFO_FOLDER)
										then SI_PARENTID
									else 0 end as SI_PARENTID, SI_KIND, case
																			when SI_NAME=''Root Folder''
																				then ''Foldery korporacyjne''
																			when SI_NAME=''User Folders''
																				then ''Foldery użytkowników''
																			else SI_NAME end, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from ' + @table_folders + 								
				'union all

				 select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +') and SI_INSTANCE = 0
				union all
				
				select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR
				from aaa_global_props where si_parentid in (select si_parentid from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2=''User Folders'' and SI_NAME!=''~Webintelligence'') and si_kind!=''Folder''
				';
		if(@tree_code = 'Connections')
			set @sql = N'select (select si_id from APP_FOLDER where SI_NAME=''Connections'') as si_id, 0 as si_parentid, ''ConnectionFolder'' as si_kind, ''Połączenia'' as si_name, '''' as descr
				
				 union all

				 select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +')';

		if(@tree_code = 'ObjectUniverses')
			set @sql = N'select si_id, case 
									when SI_NAME=''Universes''
										then 0
									when SI_PARENTID in(select si_id from ' + @table_folders +') 
										then SI_PARENTID
									else 0 end as SI_PARENTID, SI_KIND, case when SI_NAME=''Universes'' then ''Światy obiektów'' else SI_NAME end as SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from ' + @table_folders + 
													
				'union all

				 select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +')';
	end;					

	--print cast(sysdatetime() as varchar(22)) + ': faza 6 - przed zapytaniem'
	print 'zapytanie: ' + @sql;

	create table #tmpNodes (si_id varchar(1000) primary key, si_parentid varchar(1000), si_kind varchar(max), si_name varchar(max), descr varchar(max));
	if(@tree_code = 'Reports')
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, dbo.fn_node_kind_code_for_metadata_type(case 
										when si_kind=''Folder'' then ''ReportFolder'' else si_kind end), si_name, DESCR
					from (' + @sql + ') xxx';
	if(@tree_code = 'Connections')
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, dbo.fn_node_kind_code_for_metadata_type(case 
										when si_kind=''Folder'' then ''ConnectionFolder'' else si_kind end), si_name, DESCR
					from (' + @sql + ') xxx';
	if(@tree_code = 'ObjectUniverses')
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, dbo.fn_node_kind_code_for_metadata_type(case 
										when si_kind=''Folder'' then ''UniversesFolder'' else si_kind end), si_name, DESCR
					from (' + @sql + ') xxx';
	if(@tree_code = 'Teradata')
	set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
				select si_id, si_parentid, dbo.fn_node_kind_code_for_metadata_type(si_kind), si_name, DESCR
			    from (' + @sql + ') xxx';


			   
	EXECUTE(@sql);

	--print cast(sysdatetime() as varchar(22)) + ': faza 6.5 - po wrzuceniu do #tmpNodes'

	insert into bik_node_kind (code,caption) 
	select distinct si_kind, si_kind
	from #tmpNodes 
	where not exists (select 1 from bik_node_kind where code = si_kind);
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/

	declare @rc int = 1

	while @rc > 0 begin
	
	delete from #tmpNodes
	where (si_kind = 'UniversesFolder' or si_kind = 'ConnectionFolder' or si_kind = 'ReportFolder') and not exists(select 1 from #tmpNodes g where g.si_parentid = #tmpNodes.si_id)
		set @rc = @@ROWCOUNT
	end;--end loop
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/	

	update bik_node 
	set parent_node_id = null, node_kind_id = bik_node_kind.id, name = #tmpNodes.si_name,
		is_deleted = 0, descr = #tmpNodes.descr, tree_id = @tree_id
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.obj_id = #tmpNodes.si_id /*and tree_id = @tree_id*/ and bik_node.linked_node_id is null
	
	--update podlinkowanych
	update bik_node 
	set parent_node_id = null, node_kind_id = bik_node_kind.id, name = #tmpNodes.si_name,
		 descr = #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.obj_id = #tmpNodes.si_id /*and tree_id = @tree_id*/ and not bik_node.linked_node_id is null

	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select bik_node_kind.id, #tmpNodes.si_name, @tree_id, #tmpNodes.si_id, #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
		left join bik_node on bik_node.obj_id = #tmpNodes.si_id 
	where bik_node.id is null;

	set nocount on;

	drop table #primaryIds;
	
	update bik_node 
	set parent_node_id= bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id --and bik_node.tree_id = @tree_id
		inner join bik_node bk on bk.obj_id = pt.si_parentid where bik_node.linked_node_id is null--and bk.tree_id = @tree_id 

	update --delete from 
	bik_node
	set is_deleted = 1
	--select *
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id join bik_node_kind on bik_node.node_kind_id = bik_node_kind.id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and not bik_node_kind.code in 
	('Measure', 'Dimension', 'Detail', 'UniverseClass', 'ReportQuery', 'Filter')
				
	drop table #tmpNodes

	exec sp_delete_linked_nodes_where_orignal_is_deleted;
	exec sp_node_init_branch_id @tree_id, null;
end;
go


------------------------------------------------------
------------------------------------------------------

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_connections_into_bik_node]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_insert_connections_into_bik_node
go


create procedure [dbo].[sp_insert_connections_into_bik_node]
as
begin

create table #tmp_engine (name varchar(max));
create table #tmp_users (user_name varchar(max),db_engine varchar(max));
create table #tmp_conn (conn_node_id int,parent_node_id int);

declare @connTree int;
select @connTree = id from bik_tree where code='Connections'
declare @connFolderKind int;
select @connFolderKind = id from bik_node_kind where code='ConnectionFolder'
declare @parentNodeId int;
select @parentNodeId = id from bik_node where name='Połączenia' and tree_id=@connTree and node_kind_id=@connFolderKind and is_deleted=0 and linked_node_id is null

-- wrzucenie DB engine folders
insert into #tmp_engine(name)
select distinct database_enigme from bik_sapbo_universe_connection

insert into bik_node (parent_node_id, node_kind_id, name, tree_id)
	select @parentNodeId,@connFolderKind, case when #tmp_engine.name is null then 'Inny' else #tmp_engine.name end, @connTree
	from #tmp_engine left join bik_node bn on bn.name=#tmp_engine.name and bn.is_deleted=0
	and bn.linked_node_id is null and bn.node_kind_id=@connFolderKind and bn.tree_id=@connTree
	where bn.id is null;

-- wrzucenie users folders
insert into #tmp_users(user_name,db_engine)
select distinct case when user_name is null then '(bez użytkownika)' else user_name end,database_enigme from bik_sapbo_universe_connection

insert into bik_node (parent_node_id, node_kind_id, name, tree_id)
	select (select id from bik_node where name=#tmp_users.db_engine and is_deleted=0 and linked_node_id is null and node_kind_id=@connFolderKind and tree_id=@connTree),@connFolderKind, #tmp_users.user_name, @connTree
	from #tmp_users left join bik_node bn on bn.name=#tmp_users.user_name and bn.is_deleted=0
	and bn.linked_node_id is null and bn.node_kind_id=@connFolderKind and bn.tree_id=@connTree
	and bn.parent_node_id=(select id from bik_node where name=#tmp_users.db_engine and is_deleted=0 and linked_node_id is null and node_kind_id=@connFolderKind and tree_id=@connTree)
	where bn.id is null;

-- podłączenie połączeń pod odpowiednie foldery
insert into #tmp_conn(conn_node_id,parent_node_id)
select bsuc.node_id, bn.id from bik_sapbo_universe_connection bsuc 
join bik_node bn on bn.name = case when bsuc.user_name is null then '(bez użytkownika)' else bsuc.user_name end
and bn.is_deleted=0 and bn.linked_node_id is null 
and bn.node_kind_id=@connFolderKind and bn.tree_id=@connTree
join bik_node bn2 on bn.parent_node_id = bn2.id
and bn2.name=bsuc.database_enigme

update bik_node
set parent_node_id = #tmp_conn.parent_node_id
from #tmp_conn
where bik_node.id=#tmp_conn.conn_node_id

drop table #tmp_engine;
drop table #tmp_users;
drop table #tmp_conn;

exec sp_node_init_branch_id @connTree,@parentNodeId
end;
go



insert into bik_node_kind(code, caption, icon_name,tree_kind,is_folder,search_rank)
values ('AllUsersFolder','Folder użytkowników', 'userFolder','Users',1,0)
go

insert into bik_node_kind(code, caption, icon_name,tree_kind,is_folder,search_rank)
values ('KeywordFolder','Katalog słów kluczowych', 'keywordFolder','Glossary',1,0)
go

update bik_node
set name='Wszyscy użytkownicy', is_built_in=1, node_kind_id=(select id from bik_node_kind where code='AllUsersFolder')
where name='Użytkownicy' and tree_id=(select id from bik_tree where code='Users')
go

update bik_node
set parent_node_id=(select id from bik_node where name='Wszyscy użytkownicy' and tree_id=(select id from bik_tree where code='Users'))
where node_kind_id=(select id from bik_node_kind where code='User')
go

update bik_node
set node_kind_id=(select id from bik_node_kind where code='KeywordFolder')
where tree_id=(select id from bik_tree where code='Glossary') 
and is_deleted=0 and linked_node_id is null and name='Słowa kluczowe'
go


exec sp_update_version '1.0.88.9', '1.0.88.10';
go