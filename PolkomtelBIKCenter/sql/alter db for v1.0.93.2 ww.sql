exec sp_check_version '1.0.93.2';
go

----------------------------------------------------
----------------------------------------------------
----------------------------------------------------

-- tu chodzi tylko o za�o�enie odpowiedniego propsa 
-- w tabeli bik_app_prop - on b�dzie edytowalny z 
-- poziomu aplikacji: Admin/BikAppProps
-- null jest z�� warto�ci� - procedura usuwa wpis
-- dla kt�rego pr�buje si� ustawi� warto�� null,
-- st�d podany jest pusty string ('')
exec sp_set_app_prop 'disallowedShowDiagKinds', ''

----------------------------------------------------
----------------------------------------------------
----------------------------------------------------

exec sp_update_version '1.0.93.2', '1.0.93.3';
go
