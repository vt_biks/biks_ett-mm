/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.servlet;

import java.util.Map;
import java.util.Properties;
import pl.foxys.foxymill.IRenderable;
import pl.foxys.foxymill.RenderEnvironment;
import pl.trzy0.foxy.commons.templates.INamedTemplateProvider;
import pl.trzy0.foxy.serverlogic.FoxyMillSrcFilesSpec;
import simplelib.MapHelper;

/**
 *
 * @author wezyr
 */
public class FoxyMillBundleData {
    public Map<String, Object> globals;
    public Map<String, Properties> textsProps;
    public Map<String, INamedTemplateProvider> tmplProviders;
    public Map<String, IRenderable> boxes;
    public RenderEnvironment renderEnv;
    public FoxyMillSrcFilesSpec filesSpec;
    public MapHelper<String> sysProps;
}
