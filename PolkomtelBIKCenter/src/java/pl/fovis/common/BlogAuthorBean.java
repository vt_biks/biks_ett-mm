/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author bfechner
 */
public class BlogAuthorBean implements Serializable {

    public int userId;
    public boolean isAdmin;
    public int nodeId;
    public String userName;
    public Integer userNodeId;
    public String avatar;
    public String loginName;
    public boolean activ;
    public String branchIds;
//    public String oldLoginName;
}
