/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import java.util.List;
import pl.fovis.common.AttrSearchableBean;

/**
 *
 * @author ctran
 */
public interface IAdvancedSearchDialog {

    public void resetGroupingExprTb();

    public void updateAttributes(List<AttrSearchableBean> result, String lastOpexpr);

    public String getGroupingExpr();

    public void setGroupingExpr(String savedGroupingExpr);

}
