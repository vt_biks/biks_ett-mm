/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

/**
 *
 * @author wezyr
 */
public interface IOutboundUrlRewriter {

    public String rewriteOutgoingUrl(String uglyUrl);
}
