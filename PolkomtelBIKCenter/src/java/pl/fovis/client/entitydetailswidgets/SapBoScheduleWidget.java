/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.common.SapBoScheduleBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.SimpleDateUtils;

/**
 *
 * @author tflorczak
 */
public class SapBoScheduleWidget extends EntityDetailsWidgetFlatBase {

    protected final static String DEFAULT_DESTINATION_NAME = I18n.domyslne.get() /* I18N:  */;
    protected IMainHeaderSharedTable imhst;

    public SapBoScheduleWidget(IMainHeaderSharedTable imhst) {
        this.imhst = imhst;
    }

    @Override
    public void displayData() {
        SapBoScheduleBean bean = getData().sapBoScheduleBean;
        if (bean != null) {
            // i18n-default: no
            imhst.addTranslatedRow("Id" /* I18N: no */, getData().node.objId);
            imhst.addTranslatedRow("Status" /* i18n:  */, getScheduleStatus(bean));
            imhst.addTranslatedRow("Typ cyklu" /* i18n:  */, getScheduleTypeAsString(bean));
            imhst.addTranslatedRow("Miejsce docelowe" /* i18n:  */, bean.destination);
            imhst.addTranslatedRow("Adresaci" /* I18N:  */, bean.destinationEmailTo); // emaile
            imhst.addTranslatedRow("Właściciel" /* I18N:  */, bean.owner);
            imhst.addTranslatedRow("Godzina utworzenia" /* i18n:  */, SimpleDateUtils.toCanonicalString(bean.creationTime));
            imhst.addTranslatedRow("Godzina następnego uruchomienia" /* i18n:  */, SimpleDateUtils.toCanonicalString(bean.nextruntime));
            imhst.addTranslatedRow("Godzina wygaśnięcia" /* i18n:  */, SimpleDateUtils.toCanonicalString(bean.expire));
            imhst.addTranslatedRow("Formaty" /* I18N:  */, bean.format);
            imhst.addTranslatedRow("Parametry" /* I18N:  */, bean.parameters);
            // i18n-default:
        }
    }

    // tf: ściema...
    public Widget buildWidgets() {
        return new FlowPanel();
    }

    @Override
    public boolean hasContent() {
        return (getData().sapBoScheduleBean != null);
    }

    @Deprecated
    protected String getDestination(String destination) {
        if (destination == null) {
            return DEFAULT_DESTINATION_NAME;
        }
        if (destination.equals("CrystalEnterprise.Ftp" /* I18N: no */)) {
            return I18n.serwerFTP.get() /* I18N:  */;
        }
        if (destination.equals("CrystalEnterprise.DiskUnmanaged")) {
            return I18n.lokalizacjaPliku.get() /* I18N:  */;
        }
        if (destination.equals("CrystalEnterprise.Smtp" /* I18N: no */)) {
            return I18n.adresaci.get() /* I18N:  */;
        }
        if (destination.equals("CrystalEnterprise.Managed" /* I18N: no */)) {
            return I18n.skrzynkaOdbiorcza.get() /* I18N:  */;
        }
        return DEFAULT_DESTINATION_NAME;
    }

    protected String getScheduleStatus(SapBoScheduleBean bean) {
        String toReturn = null;
        if (bean.scheduleType == null) {
            return null;
        }
        switch (bean.scheduleType) {
            case 0: //ISchedulingInfo.ScheduleStatus.RUNNING:
                toReturn = I18n.scheduleRunning.get() /* I18N:  */;
                break;
            case 1: //ISchedulingInfo.ScheduleStatus.COMPLETE:
                toReturn = I18n.scheduleComplete.get() /* I18N:  */;
                break;
            case 3: //ISchedulingInfo.ScheduleStatus.FAILURE:
                toReturn = I18n.scheduleFailure.get() /* I18N:  */;
                break;
            case 8: //ISchedulingInfo.ScheduleStatus.PAUSED:
                toReturn = I18n.schedulePaused.get() /* I18N:  */;
                break;
            case 9: //ISchedulingInfo.ScheduleStatus.PENDING:
                toReturn = I18n.schedulePending.get() /* I18N:  */;
                break;
        }
        return toReturn;
    }

    protected String getScheduleTypeAsString(SapBoScheduleBean bean) {
        StringBuilder type = new StringBuilder();
        switch (bean.type) {
            case 0: //CeScheduleType.ONCE:
                type.append(I18n.jednorazowo.get() /* I18N:  */);
                break;
            case 1: //CeScheduleType.HOURLY:
                type.append(I18n.obiektUruchamianyCo.get() /* I18N:  */ + " ").append(bean.intervalHours).append(" " + I18n.godzinI.get() /* I18N:  */ + " ").append(bean.intervalMinutes).append(" " + I18n.minut.get() /* I18N:  */);
                break;
            case 2: //CeScheduleType.DAILY:
                type.append(I18n.obiektUruchamianyCo.get() /* I18N:  */ + " ").append(bean.intervalDays).append(bean.intervalDays == 1 ? " " + I18n.dzien.get() /* I18N:  */ : " " + I18n.dni.get() /* I18N:  */);
                break;
            case 3: //CeScheduleType.WEEKLY:
                type.append(I18n.obiektUruchamianyCoTydzien.get() /* I18N:  */);
                break;
            case 4: //CeScheduleType.MONTHLY:
                type.append(I18n.obiektUruchamianyCo.get() /* I18N:  */ + " ").append(bean.intervalMonths).append(" " + I18n.mies.get() /* I18N:  */ + ".");
                break;
            case 5: //CeScheduleType.NTH_DAY:
                type.append(I18n.obiektJestUruchamiany.get() /* I18N:  */ + " ").append(bean.intervalNthDay).append(" " + I18n.dniaKazdegoMiesiaca.get() /* I18N:  */);
                break;
            case 6: //CeScheduleType.FIRST_MONDAY:
                type.append(I18n.obiektJestUruchamiWPierwszyPonie.get() /* I18N:  */);
                break;
            case 7: //CeScheduleType.LAST_DAY:
                type.append(I18n.obiektJestUruchamiOstatnieDniaMi.get() /* I18N:  */);
                break;
            case 8: //CeScheduleType.CALENDAR:
                type.append(I18n.obiektJestUruchamiZgodnieZKalend.get() /* I18N:  */);
                break;
            case 9: //CeScheduleType.CALENDAR_TEMPLATE:
                type.append(I18n.obiektJestUruchamiZgodnieZKalend.get() /* I18N:  */);
                break;
        }
        return type.toString();
    }
}
