/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import java.util.List;

/**
 *
 * @author pmielanczuk
 */
public interface ITreeBottomUpBuilder<DN, TN, ID> extends ITreeBroker<DN, ID> {
    public TN createTreeNode(DN dataNode, List<TN> childTreeNodes);
}
