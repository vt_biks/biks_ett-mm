/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import commonlib.DateUtils;
import commonlib.LameUtils;
import commonlib.MuppetMerger;
import commonlib.NamedPropsBeanEx;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.HtmlResourceHandler;
import net.sf.jasperreports.export.Exporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleHtmlExporterConfiguration;
import net.sf.jasperreports.export.SimpleHtmlExporterOutput;
import org.apache.commons.codec.binary.Base64;
import org.apache.lucene.document.Document;
import org.drools.template.ObjectDataCompiler;
import org.eclipse.jgit.api.Git;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;
import pl.bssg.gwtservlet.AdhocSqlsName;
import pl.bssg.gwtservlet.IRestrictedAccessService;
import pl.bssg.metadatapump.IBIKPumpTaskNew;
import pl.bssg.metadatapump.IPumpLogger;
import pl.bssg.metadatapump.PumpUtils;
import pl.bssg.metadatapump.ThreadedPumpExecutor;
import pl.bssg.metadatapump.ad.ADLoginAuthenticationChecker;
import pl.bssg.metadatapump.ad.ADPump;
import pl.bssg.metadatapump.ad.ADTestConnection;
import pl.bssg.metadatapump.anySql.AnySqlPump;
import pl.bssg.metadatapump.ax.DynamicAxPump;
import pl.bssg.metadatapump.common.ConfluenceObjectBean;
import pl.bssg.metadatapump.common.ConnectionParametersDBServerBean;
import pl.bssg.metadatapump.common.ConnectionParametersDynamicAxBean;
import pl.bssg.metadatapump.common.ConnectionParametersFileSystemBean;
import pl.bssg.metadatapump.common.ConnectionParametersJdbcBean;
import pl.bssg.metadatapump.common.ConnectionParametersPlainFileBean;
import pl.bssg.metadatapump.common.MsSqlExPropConfigBean;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.bssg.metadatapump.common.SAPBOObjectBean;
import pl.bssg.metadatapump.common.SAPBOScheduleBean;
import pl.bssg.metadatapump.common.SAPBOScheduleOtherInfFromJoinObjBean;
import pl.bssg.metadatapump.common.SapBo4ExPropConfigBean;
import pl.bssg.metadatapump.common.SchedulePumpBean;
import pl.bssg.metadatapump.confluence.ConfluenceConfigBean;
import pl.bssg.metadatapump.confluence.ConfluenceConnector;
import pl.bssg.metadatapump.confluence.ConfluenceGroupBean;
import pl.bssg.metadatapump.confluence.ConfluenceProxy;
import pl.bssg.metadatapump.confluence.ConfluencePump;
import pl.bssg.metadatapump.confluence.ConfluenceTestConnection;
import pl.bssg.metadatapump.confluence.IConfluenceConnector;
import pl.bssg.metadatapump.confluence.wuwu.ConfluenceContentFixer;
import pl.bssg.metadatapump.confluence.wuwu.ConfluenceUtils;
import pl.bssg.metadatapump.confluence.wuwu.DeferredConfluenceObjAnchorReplacer;
import pl.bssg.metadatapump.confluence.wuwu.IConfluenceObjectImportedChecker;
import pl.bssg.metadatapump.confluence.wuwu.UrlAbsolutizingDeferredReplacer;
import pl.bssg.metadatapump.dqc.DQCPump;
import pl.bssg.metadatapump.dqc.DQCTestConnection;
import pl.bssg.metadatapump.dqc.DQCTestRequestsReceiver;
import pl.bssg.metadatapump.erwin.ErwinPump;
import pl.bssg.metadatapump.filesystem.FileSystemPump;
import pl.bssg.metadatapump.filesystem.FileSystemTestConnection;
import pl.bssg.metadatapump.filesystem.PumpFileUtils;
import pl.bssg.metadatapump.genericimporter.AttributeUsageCalculator;
import pl.bssg.metadatapump.jdbc.JdbcPump;
import pl.bssg.metadatapump.jdbc.JdbcTestConnection;
import pl.bssg.metadatapump.mssql.MsSqlPump;
import pl.bssg.metadatapump.mssql.MsSqlTestConnection;
import pl.bssg.metadatapump.oracle.OraclePump;
import pl.bssg.metadatapump.oracle.OracleTestConnection;
import pl.bssg.metadatapump.plainfile.PlainFilePump;
import pl.bssg.metadatapump.plainfile.PlainFileTestConnection;
import pl.bssg.metadatapump.plainfile.TreeExportMetadata;
import pl.bssg.metadatapump.plainfile.TreeExportRelatedFileMetaData;
import pl.bssg.metadatapump.postgresql.PostgreSQLPump;
import pl.bssg.metadatapump.postgresql.PostgreSQLTestConnection;
import pl.bssg.metadatapump.profile.ProfilePump;
import pl.bssg.metadatapump.profile.ProfileTestConnection;
import pl.bssg.metadatapump.sapbo.biadmin.BIAConnector;
import pl.bssg.metadatapump.sapbo.biadmin.BIAProxy;
import pl.bssg.metadatapump.sapbo.biadmin.IBIAConnector;
import pl.bssg.metadatapump.sapbo.designersdk.Designer4Pump;
import pl.bssg.metadatapump.sapbo.designersdk.DesignerPump;
import pl.bssg.metadatapump.sapbo.idtsdk.IDTPump;
import pl.bssg.metadatapump.sapbo.javasdk.SapBO4Pump;
import pl.bssg.metadatapump.sapbo.javasdk.SapBOPump;
import pl.bssg.metadatapump.sapbo.javasdk.SapBOReportInstancePump;
import pl.bssg.metadatapump.sapbo.javasdk.SapBOTestConnection;
import pl.bssg.metadatapump.sapbo.reportsdk.Report4Pump;
import pl.bssg.metadatapump.sapbo.reportsdk.ReportPump;
import pl.bssg.metadatapump.sapbw.SapBWPump;
import pl.bssg.metadatapump.sapbw.SapBWTestConnection;
import pl.bssg.metadatapump.sas.SASTestConnection;
import pl.bssg.metadatapump.teradata.TeradataDataModel;
import pl.bssg.metadatapump.teradata.TeradataPump;
import pl.bssg.metadatapump.teradata.TeradataTestConnection;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.BOXIService;
import pl.fovis.client.UniTreeNode;
import pl.fovis.client.VersionInfoService;
import pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog.AttributeType;
import pl.fovis.client.dialogs.ImportTreeLogDialog.TreeImportLogStatus;
import pl.fovis.client.treeandlist.UniTreeBuilder;
import pl.fovis.common.AppPropBean;
import pl.fovis.common.ArticleBean;
import pl.fovis.common.AttachmentBean;
import pl.fovis.common.AttrHintBean;
import pl.fovis.common.AttrSearchableBean;
import pl.fovis.common.AttributeAndTheirKindsBean;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.AttributeValueInNodeBean;
import pl.fovis.common.AuditBean;
import pl.fovis.common.BIAArchiveLogBean;
import pl.fovis.common.BIArchiveConfigBean;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.BIKRightRoles;
import pl.fovis.common.BISystemsConnectionsParametersBean;
import pl.fovis.common.BikNodeTreeOptMode;
import pl.fovis.common.BikRoleForNodeBean;
import pl.fovis.common.BiksSearchResult;
import pl.fovis.common.BlogBean;
import pl.fovis.common.ChangedRankingBean;
import pl.fovis.common.ChildrenBean;
import pl.fovis.common.ConfigPlainFilePumpBean;
import pl.fovis.common.ConnectionParametersADBean;
import pl.fovis.common.ConnectionParametersAuditBean;
import pl.fovis.common.ConnectionParametersBIAdminBean;
import pl.fovis.common.ConnectionParametersConfluenceBean;
import pl.fovis.common.ConnectionParametersDQCBean;
import pl.fovis.common.ConnectionParametersDQMBean;
import pl.fovis.common.ConnectionParametersDQMConnectionsBean;
import pl.fovis.common.ConnectionParametersErwinBean;
import pl.fovis.common.ConnectionParametersMsSqlBean;
import pl.fovis.common.ConnectionParametersProfileBean;
import pl.fovis.common.ConnectionParametersSASBean;
import pl.fovis.common.ConnectionParametersSapBWBean;
import pl.fovis.common.ConnectionParametersSapBo4ExBean;
import pl.fovis.common.ConnectionParametersSapBoBean;
import pl.fovis.common.ConnectionParametersSapBoExBean;
import pl.fovis.common.ConnectionParametersTeradataBean;
import pl.fovis.common.CustomRightRoleBean;
import pl.fovis.common.CustomRightRoleUserEntryBean;
import pl.fovis.common.DataSourceStatusBean;
import pl.fovis.common.DroolsConfigBean;
import pl.fovis.common.DroolsLogBean;
import pl.fovis.common.DynamicTreesBigBean;
import pl.fovis.common.EDDBProp4NodeKind;
import pl.fovis.common.EDDBProp4TreeCode;
import pl.fovis.common.EDDBProp4TreeKind;
import pl.fovis.common.EDDBPropNot4NodeKind;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.ErwinDataModelProcessObjectsRel;
import pl.fovis.common.ErwinDiagramDataBean;
import pl.fovis.common.ErwinRelationshipBean;
import pl.fovis.common.ErwinShapeBean;
import pl.fovis.common.ErwinTableBean;
import pl.fovis.common.ErwinTableColumnBean;
import pl.fovis.common.FrequentlyAskedQuestionsBean;
import pl.fovis.common.FvsChangeExBean;
import pl.fovis.common.HelpBean;
import pl.fovis.common.HomePageBean;
import pl.fovis.common.JoinTypeRoles;
import pl.fovis.common.JoinedObjAttributeLinkedBean;
import pl.fovis.common.JoinedObjBasicInfoBean;
import pl.fovis.common.JoinedObjBean;
import pl.fovis.common.JoinedObjMode;
import pl.fovis.common.LoggedUserOnClientVsServerStatus;
import pl.fovis.common.MailLogBean;
import pl.fovis.common.MenuNodeBean;
import pl.fovis.common.MyObjectsBean;
import pl.fovis.common.NameDescBean;
import pl.fovis.common.NewDefinitionDataBean;
import pl.fovis.common.NewsBean;
import pl.fovis.common.NodeAuthorBean;
import pl.fovis.common.NodeHistoryChangeBean;
import pl.fovis.common.NodeKind4TreeKind;
import pl.fovis.common.NodeKindBean;
import pl.fovis.common.NodeModificationLogEntryBean;
import pl.fovis.common.NoteBean;
import pl.fovis.common.ObjectInFvsChangeBean;
import pl.fovis.common.ObjectInHistoryBean;
import pl.fovis.common.ParametersAnySqlBean;
import pl.fovis.common.PrintTemplateBean;
import pl.fovis.common.RightRoleBean;
import pl.fovis.common.RoleForNodeBean;
import pl.fovis.common.SapBoExPropConfigBean;
import pl.fovis.common.SapBoUniverseColumnBean;
import pl.fovis.common.SearchFullResult;
import pl.fovis.common.SearchFullResult.IndexCrawlStatus;
import pl.fovis.common.StartupDataBean;
import pl.fovis.common.StatisticsBean;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.SystemUserGroupBean;
import pl.fovis.common.SystemUserInGroupBean;
import pl.fovis.common.TreeBean;
import pl.fovis.common.TreeIconBean;
import pl.fovis.common.TreeKindBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.TreeNodeBranchBean;
import pl.fovis.common.TutorialBean;
import pl.fovis.common.UserBean;
import pl.fovis.common.UserExtBean;
import pl.fovis.common.UserExtradataBean;
import pl.fovis.common.UserInNodeBean;
import pl.fovis.common.UserObjectBean;
import pl.fovis.common.ValidationExceptionBiks;
import pl.fovis.common.VoteBean;
import pl.fovis.common.dqm.DataQualityGroupBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.mltx.MultixConstants;
import pl.fovis.common.sbavc.ISearchByAttrValueCriteria;
import pl.fovis.foxygwtcommons.ServiceMethodCallException;
import pl.fovis.foxygwtcommons.TreeAlgorithms;
import pl.fovis.server.connector.plainfile.BikPlainFilePump;
import pl.fovis.server.dqm.DQMConnectionsTestConnection;
import pl.fovis.server.drools.DroolsEngine;
import pl.fovis.server.drools.DroolsEngineWorker;
import pl.fovis.server.drools.ThreadedDroolsEngine;
import pl.fovis.server.fts.ISimilarNodesWizard;
import pl.fovis.server.fts.luceneUtils.SearchWizardBucketManagerJoinedObjsTabKindEx;
import pl.fovis.server.jasperreport.ReportDataProvider;
import pl.fovis.server.mltx.DynamicWaffleSecurityFilter;
import pl.fovis.server.searchservice.BiksSearchConstant;
import pl.fovis.server.searchservice.BiksSearchService;
import pl.fovis.server.searchservice.IBiksSearchService;
import pl.fovis.server.searchservice.IndexTask;
import pl.trzy0.foxy.commons.FoxyRuntimeException;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.commons.IBeanConnectionThreadHandle;
import pl.trzy0.foxy.commons.IFoxyTransactionalPerformer;
import pl.trzy0.foxy.commons.IHasFoxyTransactionalPerformers;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import pl.trzy0.foxy.commons.MailServerConfig;
import pl.trzy0.foxy.serverlogic.BeanMaker;
import pl.trzy0.foxy.serverlogic.FoxyAppUtils;
import pl.trzy0.foxy.serverlogic.IServerSupportedLocaleNameRetriever;
import pl.trzy0.foxy.serverlogic.db.MssqlConnection;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.BaseUtils;
import simplelib.BeanWithIntIdAndNameBase;
import simplelib.BeanWithIntIdBase;
import simplelib.CSVReader;
import simplelib.CSVWriter;
import simplelib.FieldNameConversion;
import simplelib.IContinuation;
import simplelib.IContinuationWithReturn;
import simplelib.ILameCollector;
import simplelib.IParametrizedContinuation;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.SimpleDateUtils;
import simplelib.deferredstringconverter.DeferredStringConverter;
import simplelib.deferredstringconverter.IDeferredRegExprReplacer;
import simplelib.i18nsupport.I18nMessage;
import simplelib.logging.ILameLogger;

/**
 *
 * @author wezyr
 */
@AdhocSqlsName("adhocSqls.html")
public class BOXIServiceImpl extends BIKSServiceBaseForSingleBIKS implements /*IBOXIServiceOnServer,*/
        BOXIService,
        IRestrictedAccessService,
        IBOXIServiceForSearch,
        IBOXIServiceForEDDBPropsReader,
        IServerSupportedLocaleNameRetriever,
        IBIKSSystemUsersSubservice,
        IHasFoxyTransactionalPerformers {

    protected static final Integer DEFAULT_PACK_SIZE = 1000;
    protected static final long originalBOXIServiceThreadId = Thread.currentThread().getId();
    private static final ILameLogger logger = LameUtils.getMyLogger();
    //ww: przeniesione do BIKConstants
    public static final int MAX_NUMBER_OF_RESULTS_PER_SEARCH_REQUEST = 100000;
    public static final String APP_VERSION = BIKConstants.APP_VERSION;
    public static final String CREATE_DISABLED_USERS_IN_SSO_PROP_NAME = "createDisabledUsersInSSO";
    private static final DroolsEngine droolsEngine = new DroolsEngine();

//    public static final String AVAILABLE_CONNECTORS_PROP_NAME = "availableConnectors";
//    protected IBeanConnectionEx<Object> connection;
//    protected INamedSqlsDAO<Object> adhocDao;
//    protected IBeanConnectionEx<Object> connectionLoad;
//    protected INamedSqlsDAO<Object> adhocDaoForLoad;
//    protected TheadedHeartbeatMonitor heartbeat;
//    protected HeartbeatMonitorTask heartbeat;
//    protected long lastRankingChangedTimeStamp;
//    public static final String NOT_CORPORATE_VERSION = I18n.brakWersjiKorporacyjnej.get() /* I18N:  */;
    // lb: synchronizowana mapa do obslugi puli polaczen teradaty per uzytkownik
    protected VersionInfoService versionInfoService;
//    protected IBikFullTextSearch fts;
    protected IBiksSearchService searchService;
    protected DQMServiceImpl dqmService; //brzydkie, tylko do Drools

    protected static final ThreadedDroolsEngine threadedDroolsEngine = new ThreadedDroolsEngine();
    protected static final Thread droolsEngineThread = new Thread(threadedDroolsEngine);
    protected static final ExecutorService artificialAdminThread = Executors.newSingleThreadExecutor();
    protected static final AtomicBoolean hasFinishedInit = new AtomicBoolean(false);
    protected BIKCenterConfigObj cfg;
    protected Map<String, INamedSqlsDAO<Object>> adhocDao4Report = new HashMap<String, INamedSqlsDAO<Object>>();
    protected INamedSqlsDAO<Object> adhocDaoJs = null;

    private Whitelist myWhiteList = Whitelist.none().preserveRelativeLinks(true);

    static {
        LameUtils.logExtendedDiagVals();
    }

    public BOXIServiceImpl(VersionInfoService versionInfoService) {
        this.versionInfoService = versionInfoService;
    }

    @Override
    @PerformOnDefaultDb
    protected void initWithConfig(BIKCenterConfigObj cfg) {

        super.initWithConfig(cfg);
        this.cfg = cfg;
        if (!isMultiXMode()) {
            System.out.println("Config file content: " + cfg);
            setSharedMailServerCfg(readMailServerConfigFromSingleBiks());

            boolean useDrools = BaseUtils.tryParseBoolean(getAppPropValue("useDrools"), false);
            DroolsConfigBean droolsConfig = getDroolsConfig();
            if (useDrools && !hasFinishedInit.get()) {
                threadedDroolsEngine.setAdhocDao(getAdhocDao());

                getDroolsEngine().setService(this);

                threadedDroolsEngine.setEngine(getDroolsEngine());
                threadedDroolsEngine.setService(this);

                droolsEngineThread.start();
                if (droolsConfig.isActive > 0) {
                    threadedDroolsEngine.schedule(droolsConfig);
                }
            }
        }
//        System.out.println("initWithConfig(): before setUseWaffle");
        // w trybie MultiX nie będzie wiadomo o którą bazę chodzi, więc to działa
        // tylko w trybie SingleBiks
        DynamicWaffleSecurityFilter.setUseWaffle(!isMultiXMode() && getShowSSOLoginDialog());

//        connection = new MssqlConnection(cfg.connConfig);
//        adhocDao = FoxyAppUtils.createNamedSqlsDAOFromResource(connection, namedSqlsTextResource);
//        IBeanConnectionEx<Object> connection = new MssqlConnection(cfg.connConfig);
//        INamedSqlsDAO<Object> adhocDao = FoxyAppUtils.createNamedSqlsDAOFromResource(connection, namedSqlsTextResource);
//        System.out.println("initWithConfig(): before new BIKFullTextSearchViaMSSQL");
//        fts = //new BikLuceneFullTextSearcher(this, cfg.dirForLucene + "/" + cfg.connConfig.server + "_" + cfg.connConfig.database/*, this.getIndexVer()*/);
//                new BIKFullTextSearchViaMSSQL(this, getAdhocDao());
        if (BaseUtils.isStrEmptyOrWhiteSpace(cfg.dirForLucene)) {
            cfg.dirForLucene = BaseUtils.ensureDirSepPostfix(cfg.dirForUpload) + "searchengine";
        }
        searchService = new BiksSearchService(cfg); //        System.out.println("end initWithConfig");
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
//        Timer timer = new Timer();
        Integer indexTimeInterval = BaseUtils.tryParseInteger(cfg.indexTimeInterval);
        if (indexTimeInterval == null || indexTimeInterval <= 0) {
            indexTimeInterval = 300;
        }
//        timer.schedule(new IndexTimerTask(cfg), new Date(), indexTimeInterval.longValue() * 60 * 1000);
        System.out.println("Indeksowanie co " + indexTimeInterval + " sekundy");
        executor.scheduleWithFixedDelay(new IndexTask(cfg), 0, indexTimeInterval, TimeUnit.SECONDS);

        loadXSSSettings();
        hasFinishedInit.set(true);
    }

    private void loadXSSSettings() {
        Whitelist myList = Whitelist.none().preserveRelativeLinks(true);
        try {

            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            InputStream inputStream = classLoader.getResourceAsStream("xss.properties");

            //File configFile = new File("c:\\xss.properties");
            //InputStream inputStream = new FileInputStream(configFile);
            Properties props = new Properties();
            props.load(inputStream);

            for (Object key : props.keySet()) {
                String extraAttrs = props.getProperty((String) key);
                String[] attrs = extraAttrs.split(";");
                myList.addTags((String) key);
                if (attrs.length > 0 && attrs[0].length() > 0) {
                    myList.addAttributes((String) key, attrs);
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BOXIServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BOXIServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        myWhiteList = myList;
    }

    @PerformOnSingleBiksDb
    protected MailServerConfig readMailServerConfigFromSingleBiks() {
        // String host, String port, String user, String password, String fromAddress, boolean ssl
        MailServerConfig res = new MailServerConfig(
                getSensitiveAppPropValue("sendmail.host"),
                getSensitiveAppPropValue("sendmail.port"),
                getSensitiveAppPropValue("sendmail.user"),
                getSensitiveAppPropValue("sendmail.pwd"),
                getSensitiveAppPropValue("sendmail.from"),
                BaseUtils.tryParseBoolean(getSensitiveAppPropValue("sendmail.ssl")));

        return res;
    }

    public int getIndexVer() {
        return BaseUtils.tryParseInteger(getSensitiveAppPropValue("indexVer"), -1);
    }

    public void setIndexVer(int indexVer) {
        execNamedCommand("setIndexVer", Integer.toString(indexVer));
    }

    protected ConfluenceContentFixer getConfluenceContentFixer() {
        final String baseConfluenceUrl = BaseUtils.ensureStrHasSuffix(getConfluenceConnectionParameters().url, "/", false);
        ConfluenceContentFixer confluenceContentFixer = new ConfluenceContentFixer(urlForPublicAccess,
                baseConfluenceUrl, new IConfluenceObjectImportedChecker() {
            @Override
            public Map<String, String> checkConfluenceImportedObjs(Collection<String> urls, Integer optSelectedNodeId) {

//                    System.out.println("checkConfluenceImportedObjs: urls=" + urls);
                Set<String> urlsToCheck = new HashSet<String>();

                for (String url : urls) {
                    if (BaseUtils.isStrEmptyOrWhiteSpace(url)
                            || url.length() > 512
                            || !url.startsWith("http")) {
                        continue;
                    }
                    if (BaseUtils.isUriGlobal(url)) {
                        urlsToCheck.add(url);
                    } else {
                        urlsToCheck.add(baseConfluenceUrl + url.substring(1));
                    }
                }

                if (urlsToCheck.isEmpty()) {
                    return new HashMap<String, String>();
                }

                List<Map<String, Object>> rows = execNamedQueryCFN("getNodesConfluenceImportedObjsByUrlsAndNodeId",
                        FieldNameConversion.ToLowerCase, urlsToCheck, optSelectedNodeId);

                Map<String, String> res = BaseUtils.makeHashMapOfSize(rows.size());

                for (Map<String, Object> row : rows) {
                    String objIdStr = (String) row.get("url");
                    res.put(objIdStr, (String) row.get("biks_url"));
                }
                return res;
            }

            public Map<String, String> checkDwhImportedObjs(Collection<String> dwhObjNames) {
                return new HashMap<String, String>();
            }

            public Map<String, String> checkConfluenceImportedLogins(Collection<String> logins) {
                if (logins.isEmpty()) {
                    return new HashMap<String, String>();
                }

                List<Map<String, Object>> rows = execNamedQueryCFN("getNodesConfluenceImportedLogins",
                        FieldNameConversion.ToLowerCase, logins);

                Map<String, String> res = BaseUtils.makeHashMapOfSize(rows.size());

                for (Map<String, Object> row : rows) {
                    res.put((String) row.get("login_name"), "UserRoles/" + row.get("node_id"));
                }

                return res;
            }
        });

        return confluenceContentFixer;
    }

    @Override
    public TreeNodeBean getNodeBeanById(int nodeId, boolean allowNull) {
        TreeNodeBean tnb = createBeanFromNamedQry("getBikNode", TreeNodeBean.class, allowNull, nodeId);

        if (tnb == null) {
            return null;
        }

        if (BaseUtils.safeEqualsAny(tnb.nodeKindCode, BIKConstants.NODE_KIND_CONFLUENCE_BLOG,
                BIKConstants.NODE_KIND_CONFLUENCE_PAGE)) {

            tnb.descr = getConfluenceContentFixer().fixContent(tnb.descr, nodeId);
        }

        translateSpecialTreeNodeBeans(Collections.singletonList(tnb), false);
        return tnb;
    }

    public List<TreeNodeBean> getChildrenNodeBeansByParentId(int parentNodeId) {
        List<TreeNodeBean> tnb = createBeansFromNamedQry("getChildrenForNode", TreeNodeBean.class, parentNodeId);
        translateSpecialTreeNodeBeans(tnb, false);
        return tnb;
    }
    protected Map<Integer, EntityDetailsDataBean> fakeNodeCache = new HashMap<Integer, EntityDetailsDataBean>();
    private static final boolean DumpMemUsageModeOn = false;

    @Override
    public EntityDetailsDataBean getBikEntityDetailsData(int nodeId, Integer linkingParentId, boolean fetchExtraData) {

        if (DumpMemUsageModeOn) {
            LameUtils.dumpMemoryUsage("getBikEntityDetailsData: " + "start" /* I18N: no */);
        }

        try {
            EntityDetailsDataBean ret = fakeNodeCache.get(nodeId);

            if (ret != null) {
                return ret;
            }

            ret = new EntityDetailsDataBean();

            ret.node = getNodeBeanById(nodeId, true);
            if (ret.node == null) {
                return null;
            }
            ret.user
                    = //getUser4EDDB(ret);
                    bikUserByNodeId(nodeId);

            ret.grantedNodeRightActionIds = execNamedQuerySingleColAsSet("getActionRightsInNode", null, nodeId);
//            List<Map<String, Object>> rights = execNamedQueryCFN("getActionRightsInNode", FieldNameConversion.ToLowerCase, nodeId);
//
//            ret.grantedNodeRightActionIds = new HashSet<Integer>();
//
//            for (Map<String, Object> rightRow : rights) {
//
//                Object isGrant = rightRow.get("is_grant");
//                int actionId = (Integer) rightRow.get("action_id");
//
////                System.out.println("actionId=" + actionId + ", isGrant=" + isGrant + ", class=" + BaseUtils.safeGetClassName(isGrant));
//                boolean isGranted = BaseUtils.safeEquals(sqlNumberToInt(isGrant), 1);
//
//                ret.grantedNodeRightActionIds.add(actionId);
//            }

//            System.out.println("granted: " + ret.grantedNodeRightActionIds + ", denied: " + ret.deniedNodeRightActionIds);
            String nodeKindCode = ret.node.nodeKindCode;
            String treeCode = ret.node.treeCode;
            String treeKind = ret.node.treeKind;

            Set<String> propNames = MuppetMerger.getMuppetProps(ret, 3);

            EntityDetailsDataBeanPropsReader reader = new EntityDetailsDataBeanPropsReader(this,
                    getAdhocDao(), ret,
                    linkingParentId);
            //GlobalThreadStopWatch.startProcessStage("entering for-loop");

            int propIdx = 0;

            for (String propName : propNames) {

//                System.out.println("property name: " + propName + " (" + propIdx++ + "/" + propNames.size() + ")");
                Field f;
                boolean fieldWasSet = false;
                Set<String> allowedNodeKinds = null;
                Set<String> allowedTreeCodes = null;
                Set<String> allowedTreeKinds = null;
                Set<String> disallowedNodeKinds = null;
                String methodName = "get" /* I18N: no */ + BaseUtils.capitalize(propName) /*
                         * + "4EDDB"
                         */;
                boolean isAllowed;

                try {
                    try {
                        f = ret.getClass().getField(propName);
                    } catch (Exception ex) {
                        continue;
                    }

                    if (f.isAnnotationPresent(EDDBProp4NodeKind.class)) {
                        EDDBProp4NodeKind a = f.getAnnotation(EDDBProp4NodeKind.class);
                        allowedNodeKinds = BaseUtils.paramsAsSet(a.value());
                    }
                    if (f.isAnnotationPresent(EDDBProp4TreeCode.class)) {
                        EDDBProp4TreeCode a = f.getAnnotation(EDDBProp4TreeCode.class);
                        allowedTreeCodes = BaseUtils.paramsAsSet(a.value());
                    }
                    if (f.isAnnotationPresent(EDDBProp4TreeKind.class)) {
                        EDDBProp4TreeKind a = f.getAnnotation(EDDBProp4TreeKind.class);
                        allowedTreeKinds = BaseUtils.paramsAsSet(a.value());
                    }
                    if (f.isAnnotationPresent(EDDBPropNot4NodeKind.class)) {
                        EDDBPropNot4NodeKind a = f.getAnnotation(EDDBPropNot4NodeKind.class);
                        disallowedNodeKinds = BaseUtils.paramsAsSet(a.value());
                    }

                    if (allowedNodeKinds != null || allowedTreeCodes != null || allowedTreeKinds != null) {
                        isAllowed = ((allowedNodeKinds != null && allowedNodeKinds.contains(nodeKindCode))
                                || (allowedTreeCodes != null && allowedTreeCodes.contains(treeCode))
                                || (allowedTreeKinds != null && allowedTreeKinds.contains(treeKind))) && !(disallowedNodeKinds != null && disallowedNodeKinds.contains(nodeKindCode));
                    } else if (disallowedNodeKinds != null) {
                        isAllowed = !disallowedNodeKinds.contains(nodeKindCode);
                    } else {
                        isAllowed = true;
                    }

                    if (!isAllowed) {
//                    if (logger.isDebugEnabled()) logger.debug("not setting property [" + propName + "] via method ["
//                            + methodName + "] - kind [" + nodeKindCode + "] not allowed in: "
//                            + "[" + allowedNodeKinds + "]");
                        continue;
                    }

                    Method m;
                    try {
                        m = reader.getClass().getMethod(methodName);
                    } catch (Exception ex) {
                        //m = null; // no such method?
                        throw new RuntimeException("no such method" /* I18N: no */ + " [" + methodName + "] " + "with proper signature" /* I18N: no */, ex);
                    }

                    if (m != null) {
                        Object val = null;

                        try {
//                            if (GlobalThreadStopWatch.isProcInfoLoggingEnabled()) {
//                                GlobalThreadStopWatch.startProcess("invoke method: " + methodName);
//                            }
                            try {
//                                long startTime = System.currentTimeMillis();
                                val = m.invoke(reader);
//                                long endTime = System.currentTimeMillis();
//                                long elapsedInInvoke = endTime - startTime;
//
//                                totalElapsedInInvoke += elapsedInInvoke;
//
//                                System.out.println("invoke method: " + methodName + ", elapsed time=" + elapsedInInvoke + " millis");
                            } finally {
//                                if (GlobalThreadStopWatch.isProcInfoLoggingEnabled()) {
//                                    GlobalThreadStopWatch.endProcess();
//                                }
                            }
                        } catch (Exception ex) {
                            throw new RuntimeException("error in method" /* I18N: no */ + ": " + methodName, ex);
                        }

//                    if (logger.isDebugEnabled()) logger.debug("setting property [" + propName + "] via method ["
//                            + methodName + "]");
                        MuppetMerger.setProperty(ret, propName, val);
                        fieldWasSet = true;
                    }
                } finally {
//                if (logger.isDebugEnabled()) logger.debug((fieldWasSet ? "" : "not ") + "setting property [" + propName + "] via method ["
//                        + methodName + "] - kind [" + nodeKindCode + "]"
//                        + (isAllowed ? "" : " not") + " allowed in: "
//                        + allowedNodeKinds);
                }
            }

            //GlobalThreadStopWatch.startProcessStage("after for-loop");
            if (ret.thisBizDef != null) {
                ret.properDescr = ret.thisBizDef.body;
            } else if (ret.blogEntry != null) {
                ret.properDescr = ret.blogEntry.content;
                //bf: ten isHTMLTextEmpty musi tu zostac, bo
                // metadataEditedDescription nadpisuje thisBizDef.body jak jest
                // niepusty
            } else if (!BaseUtils.isHTMLTextEmpty(ret.metadataEditedDescription)) {
                ret.properDescr = ret.metadataEditedDescription;
            } else {
                ret.properDescr = ret.node.descr;
            }

            if (BaseUtils.isHTMLTextEmpty(ret.properDescr)) {
                ret.properDescr = null;
            } else {
                Pair<String, Map<Integer, Pair<String, String>>> extracted = extractInnerLinks(ret.properDescr, false);
                ret.properDescr = extracted.v1;
                ret.innerLinks = extracted.v2;
            }
            if (!fetchExtraData) {
                ret.ancestorNodePath = getAncestorNodePath(ret.node.branchIds);
            }
//            fakeNodeCache.put(nodeId, ret);     //!!!!PG zakomentować

//            long totalEndTime = System.currentTimeMillis();
//            long totalElapsedTime = totalEndTime - totalStartTime;
//            long totalOutsideOfInvoke = totalElapsedTime - totalElapsedInInvoke;
//
//            System.out.println("******* total time in getCośTam: " + totalElapsedTime + " millis, total in invoke: " + totalElapsedInInvoke + " millis, outside: " + totalOutsideOfInvoke + " millis");
            return ret;
        } finally {
            if (DumpMemUsageModeOn) {
                LameUtils.dumpMemoryUsage("getBikEntityDetailsData: " + "end" /* I18N: no */);
            }

            //GlobalThreadStopWatch.endProcess();
        }
    }

    @Override
    public Pair<Integer, List<String>> addBikDocument(int parentNodeId, String caption, String href, String nodeKindCode) {
        Integer id = execNamedQuerySingleVal("addBikDocument", true, "id" /* I18N: no */, getLoggedUserId(), parentNodeId, caption, href, nodeKindCode);
        updateNodeForFts(id, caption, null);
        addUserToRank(id);
        addAuthorToNode(id);
        fillDefaultValuesForAttributes(id);
        return new Pair<Integer, List<String>>(id, refreshTaxonomyTreeByIdOrParent(parentNodeId));
    }

    @Override
    public int addBikDocumentAdHoc(String caption, String href, String nodeKindCode) {
        Map<String, Object> val = execNamedQuerySingleRowCFN(
                "addBikDocumentAdHoc", false, FieldNameConversion.ToJavaPropName, getLoggedUserId(), caption, href, nodeKindCode);
        int id = ((Integer) val.get("id" /* I18N: no */)).intValue();
        updateNodeForFts(id, caption, null);
        addUserToRank(id);
        addAuthorToNode(id);
        fillDefaultValuesForAttributes(id);
        return id;
    }

    @Override
    public void implementMenuConfigTree() throws LameRuntimeException {
        String confTreeCode = getConfigurationTree(BIKConstants.APP_PROP_MENU_CONF);
        if (BaseUtils.isStrEmptyOrWhiteSpace(confTreeCode)) {
            throw new LameRuntimeException("Drzewo konfiguracji nie zostało zdefiniowane");
        }

        execNamedCommand("implementMenuConfig");
    }

//    protected void updateNodeAttributesAndRegisterChangeInNode(int nodeId, Map<String, String> attrVals) {
//        boolean isNewNode = fts.updateNodeAttributes(nodeId, attrVals);
//        execNamedCommand("processManualNodeModification", nodeId, isNewNode);
//        registerChangeInNode(nodeId);
//        indexChangedNodes();
//    }
    protected void updateNodeAttributeForFts(int nodeId, String attrName, String attrVal) {
        Map<String, String> attrVals = new HashMap<String, String>();
        attrVals.put(attrName, attrVal);
//        fts.updateNodeAttributes(nodeId, attrVals);
//        registerChangeInNode(nodeId);
//        updateNodeAttributesAndRegisterChangeInNode(nodeId, attrVals);
    }

    public void addOrUpdateAttribute(String attrName, Object value, int nodeId) {
        String safeAttrName = stripHtmlTags(cleanHtml(attrName));
        String safeValue = cleanHtml((String) value);
        Integer creatorId = getLoggedUserBean().userId != null ? getLoggedUserBean().userId : getLoggedUserBean().id;
        execNamedCommand("addOrUpdateAttribute", safeAttrName, safeValue, nodeId, creatorId);
        changeRelatedNodeInfoByAttribute(attrName, safeValue, nodeId);
    }

    public List<UserBean> getBikAllUsers() {
        return createBeansFromNamedQry("getBikAllUsers", UserBean.class);
    }

    @Override
    public void addBikUser(String name, String mail, String phone, String descr, int nodeId, String loginAD) {
        String safeName = cleanHtml(name);
        String safeMail = cleanHtml(mail);
        String safePhone = cleanHtml(phone);
        String safeDescr = cleanHtml(descr);
        String safeLoginAD = cleanHtml(loginAD);
        execNamedCommand("addBikUser", safeName, safeMail, safePhone, safeDescr, nodeId, safeLoginAD);
    }

    @Override
    public void updateBikSystemUser(int id, String loginName, String password, boolean isSendMails) {
        String safeLoginName = cleanHtml(loginName);
        //ww: to się ma wywalić dla niezalogowanego!
        // albo edytujesz sobie login/pwd, albo masz do tego większe uprawnienia
        // (App/Sys)Admin
        if (id != getLoggedUserId()) {
            checkRightsToEditSysUserById(id);
        }

        execNamedCommand("updateBikSystemUser2", id, safeLoginName, password, isSendMails);
        getLoggedUserBean().isSendMails = isSendMails;
    }

    public boolean isRecalculateCustomRightRoleRutsEnabled() {
        Boolean v = (Boolean) getRequestTempStorage().get("disableRecalculateCustomRightRoleRuts");
        return v == null || !v;
    }

    @Override
    @PerformOnSingleBiksDb
    public Integer addBikSystemUserInternal(boolean performSecurityChecks, String loginName, String password,
            boolean isDisabled, Integer userId, Set<String> userRightRoleCodes, Map<Integer, Set<Pair<Integer, Integer>>> customRightRoleUserEntries, List<Integer> usersGroupsIds, boolean doNotFilterHtml, String rightsProc) throws ValidationExceptionBiks {

        String dbName = getSingleBiksDbForCurrentRequest();

        String safeLoginName = cleanHtml(loginName);

        if (performSecurityChecks) {
            checkRightsToAddOrEditsSysUsers(null);
        }
        if (multiXLoginsSubservice.multiXLoginExistsForDatabase(safeLoginName, dbName) || loginExists(safeLoginName)) {
            throw new ValidationExceptionBiks(I18n.loginJestJuzUzywanyWybierzInny.get());
        }
        if (isMultiXMode()) {
            boolean newLoginIsValidEmail = BaseUtils.isEmailValid(safeLoginName);
            if (!newLoginIsValidEmail) {
                throw new ValidationExceptionBiks(I18n.niepoprawnyAdresEmail.get());
            }
        }
        multiXLoginsSubservice.addMultiXLoginToExistingDb(safeLoginName, dbName);
        //ww: hmmm, ciekawe - kto to tutaj zostawił?
        Set<String> treeIds = null; //tutaj będzie trzeba uzupełnić

        if (performSecurityChecks) {
            fixSysAdminRightRoleOnAddOrEdit(userRightRoleCodes);
        }

        return sqlNumberToInt(execNamedQuerySingleVal("addBikSystemUserEx", false, "id" /* I18N: no */, safeLoginName, password,
                isDisabled, userId, userRightRoleCodes, treeIds, fixCustomRightRoleUserEntries(customRightRoleUserEntries), null, null, usersGroupsIds, doNotFilterHtml, rightsProc));
    }

    @Override
    public Integer addBikSystemUser(String loginName, String password, boolean isDisabled, Integer userId,
            Set<String> userRightRoleCodes, Map<Integer, Set<Pair<Integer, Integer>>> customRightRoleUserEntries, List<Integer> usersGroupsIds, boolean doNotFilterHtml) throws ValidationExceptionBiks {

        getRequestTempStorage().put("disableRecalculateCustomRightRoleRuts", true);

        Integer systemUserId = addBikSystemUserInternal(true, loginName, password, isDisabled, userId, userRightRoleCodes, customRightRoleUserEntries, usersGroupsIds, doNotFilterHtml, "int");
        List<Integer> nodeIds = execNamedQuerySingleColAsList("getAddedConfluenceItemsByUser", "node_id", loginName);
        for (Integer nodeId : nodeIds) {
            execNamedCommand("addUserToRank", systemUserId, nodeId);
        }
        return systemUserId;
    }

    protected void checkRightsToEditSysUserById(int idOfSysUserToEdit) {
        //ww: fejkowy stary user do edycji, ważne tylko że ma wypełnione
        // userRightRoleCodes
        SystemUserBean oldSub = new SystemUserBean();
        if (!disableBuiltInRoles()) {
            oldSub.userRightRoleCodes = getRightRolesForSystemUserByUserId(idOfSysUserToEdit);
        }
        checkRightsToAddOrEditsSysUsers(oldSub);
    }

    @Override
    @PerformOnSingleBiksDb
    public void updateBikSystemUser(int id, String loginName, String password, final boolean isDisabled,
            Integer userId, String systemUserName, Set<String> userRightRoleCodes, Set<Integer> treeIds,
            boolean isSendMails, Map<Integer, Set<Pair<Integer, Integer>>> customRightRoleUserEntries,
            List<Integer> usersGroupsIds, boolean doNotFilterHtml) throws ValidationExceptionBiks {

        checkRightsToEditSysUserById(id);

        String safeLoginName = cleanHtml(loginName);
        String safeSystemUserName = cleanHtml(systemUserName);

        if (isMultiXMode()) {
            //w multix nie chcemy zmieniac z tego poziomu ani loginu ani hasla

            String oldLogin = getUserLoginById(id);
            safeLoginName = oldLogin;
            password = "";

            final String loginNameF = safeLoginName;

            //multiXLoginsSubservice.changeAccessToDatabase(isDisabled, loginName, getCurrentDatabase().get());
            performOnMasterBiks(new IContinuation() {

                @Override
                public void doIt() {
                    multiXLoginsSubservice.changeAccessToDatabase(isDisabled, loginNameF, getSingleBiksDbForCurrentRequest());
                }
            });

        }
        //pk: zmiana loginu wylaczona - poniewaz z tym samym loginem polaczone sa inne bazy wulitks

//        String oldLogin = execNamedQuerySingleVal("getBikSystemUserLogin", false, null, id);
//        final boolean isLoginChanged = !BaseUtils.safeEquals(BaseUtils.safeToLowerCase(oldLogin), BaseUtils.safeToLowerCase(loginName));
//        if (isLoginChanged && (multiXLoginsSubservice.multiXLoginExistsForCurrentDatabase(loginName) || loginExists(loginName, oldLogin))) {
//            throw new ValidationExceptionBiks(I18n.loginJestJuzUzywanyWybierzInny.get());
//        }
//        if (isMultiXMode()) {
//
////            if (isLoginChanged) {
////                boolean oldLoginIsValidEmail = BaseUtils.isEmailValid(oldLogin);
////                boolean newLoginIsValidEmail = BaseUtils.isEmailValid(loginName);
////                if (oldLoginIsValidEmail && !newLoginIsValidEmail) {
////                    throw new ValidationExceptionBiks(I18n.niepoprawnyAdresEmail.get());
////                }
////            }
//        }
//        multiXLoginsSubservice.replaceMultiXLogin(loginName, oldLogin);
        fixSysAdminRightRoleOnAddOrEdit(userRightRoleCodes);

        execNamedCommand("updateBikSystemUser", id, safeLoginName, password, isDisabled, userId, safeSystemUserName, userRightRoleCodes, treeIds, isSendMails,
                fixCustomRightRoleUserEntries(customRightRoleUserEntries), usersGroupsIds, doNotFilterHtml, "int");

        markUserAsDisabled(safeLoginName, isDisabled);
        if (!isDisabled) {
            registerChangeForUser(safeLoginName);
        }
    }

    protected String getUserLoginById(int id) {
        String oldLogin = execNamedQuerySingleVal("getBikSystemUserLogin", false, null, id);
        return oldLogin;
    }

    protected void markUsersAsDisabled(Map<String, Boolean> loginToIsDisabledMap) {
        BOXIServiceImplData implData = getBOXIServiceImplDataForCurrentRequest();
        final Set<String> disabledUsers = implData.disabledUsers;
        synchronized (disabledUsers) {

            for (Entry<String, Boolean> e : loginToIsDisabledMap.entrySet()) {
                String loginName = e.getKey();
                boolean isDisabled = e.getValue();

                BaseUtils.setAddOrRemoveItem(disabledUsers, loginName, isDisabled);
            }
        }
    }

    protected void markUserAsDisabled(String loginName, boolean isDisabled) {
        BOXIServiceImplData implData = getBOXIServiceImplDataForCurrentRequest();
        final Set<String> disabledUsers = implData.disabledUsers;
        synchronized (disabledUsers) {
            BaseUtils.setAddOrRemoveItem(disabledUsers, loginName, isDisabled);
        }
    }

    @Override
    public void deleteSystemUser(Integer systemUserId) {
        //ww: akcja nie jest używana w aplikacji, ale na wszelki wypadek...
        checkRightsToEditSysUserById(systemUserId);
        execNamedCommand("deleteSystemUser", systemUserId);
        String loginName = getUserLoginById(systemUserId);
        markUserAsDisabled(loginName, true);
    }

    public Pair<Integer, List<String>> addBikTreeNodeUser(TreeNodeBean repo, UserExtBean user) {

        if (logger.isDebugEnabled()) {
            logger.debug("addBikTreeNodeUser: " + "start. user" /* I18N: no */ + "=<" + user.name + ">");
        }
        try {
            String nodeName = user.name;
            int id = createTreeNodeByCode(repo.id, BIKConstants.NODE_KIND_USER, nodeName, repo.objId, repo.treeId, repo.linkedNodeId, null);
            addBikUser(user.name, user.email, user.phoneNum, user.shortDescr, id, user.loginNameForAd);
            // Pobranie z AD danych dla tego usera
            // TF > niepotrzebne - pobieramy już wszsytkie loginy
//            getADDataForUser(user.loginNameForAd, false, null);
            updateNodeAttrEmailPhoneForFts(id, user.email, user.phoneNum);
            return new Pair<Integer, List<String>>(id, refreshTaxonomyTreeByIdOrParent(repo.id));
        } catch (Exception ex) {
            UserBean usr = bikUserByLogin(user.loginName);
            if (usr != null) {
                finishWorkUnit(false);
                return null;
            } else {
                logger.errorAndThrowNew("Error in addBikTreeNodeUser, user" /* I18N: no:err-fail-in */ + "=<" + user.name + ">", ex);
                return null;
            }
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("addBikTreeNodeUser: " + "done. user" /* I18N: no */ + "=<" + user.name + ">");
            }
        }
    }

    public void addBikEntityNote(String title, String body, int nodeId) {
        String safeTitle = cleanHtml(title);
        String safeBody = cleanHtml(body);
        execNamedCommand("addBikEntityNote", title, body, getLoggedUserId(), nodeId);
    }

    protected void updateNodeAttrEmailPhoneForFts(Integer nodeId, String email, String phoneNum) {
        Map<String, String> attrVals = new HashMap<String, String>();
        attrVals.put("email" /* I18N: no */, email);
        attrVals.put("phone_num", phoneNum);
//        fts.updateNodeAttributes(nodeId, attrVals);
//        registerChangeInNode(nodeId);
//        updateNodeAttributesAndRegisterChangeInNode(nodeId, attrVals);
    }

    public List<String> updateBikUser(int id, String oldLoginName, String loginName, String name, String password, String mail, String phone, String loginAd) {

        if (BaseUtils.isStrEmptyOrWhiteSpace(mail)) {
            mail = null;
        }
        if (BaseUtils.isStrEmptyOrWhiteSpace(phone)) {
            phone = null;
        }

        if (BaseUtils.isStrEmptyOrWhiteSpace(loginAd)) {
            loginAd = null;
        }
        try {
            Integer nodeId = execNamedQuerySingleVal(
                    "updateBikUser", false, "node_id", id, name, mail, phone, loginAd);

            updateNodeAttrEmailPhoneForFts(nodeId, mail, phone);

            // Pobranie z AD danych dla tego usera
            // TF - > niepotrzebne - pobieramy już wszsytkie loginy
//            createSocialUserAndLink(loginAd, false, null);
            return refreshTaxonomyTreeByIdOrParent(nodeId);
        } catch (Exception ex) {
            if (oldLoginName != null && !oldLoginName.equals(loginName)) {
                UserBean usr = bikUserByLogin(loginName);
                if (usr != null) {
                    finishWorkUnit(false);
                    return null;
                } else {
                    logger.errorAndThrowNew("Error in updateBikUser, user" /* I18N: no:err-fail-in */ + "=<" + name + ">", ex);
                    return null;
                }
            } else {
                logger.errorAndThrowNew("Error in updateBikUser, user" /* I18N: no:err-fail-in */ + "=<" + name + ">", ex);
                return null;
            }
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("updateBikUser: " + "done. user" /* I18N: no */ + "=<" + name + ">");
            }
        }
    }

    public Pair<Integer, List<String>> addBikBlogEntry(int parentNodeId, String content, String subject) {
        String safeContent = cleanHtml(content);
        String safeSubject = stripHtmlTags(cleanHtml(subject));
        Integer blogId = execNamedQuerySingleVal(
                "addBikBlogEntry", false, "id" /* I18N: no */, parentNodeId, safeContent, safeSubject, getLoggedUserId());
        updateNodeForFts(blogId, stripHtmlTags(safeSubject), stripHtmlTags(safeContent));
        addAuthorToNode(blogId);
        fillDefaultValuesForAttributes(blogId);
        return new Pair<Integer, List<String>>(blogId, refreshTaxonomyTreeByIdOrParent(parentNodeId));
    }

    public List<String> editBikBlogEntry(int id, String content, String subject, int nodeId) {
        String safeContent = cleanHtml(content);
        String safeSubject = stripHtmlTags(cleanHtml(subject));
        Set<Integer> idNodeList = BIKCenterUtils.extractNodeIdsFromHtml(safeContent);
        execNamedCommand("deleteNodeInBody", id);
        execNamedCommand("addNodeInBody", id, idNodeList);
        execNamedCommand("editBikBlogEntry", id, safeContent, safeSubject);
        execNamedCommand("changeBikNodeNameBlog", nodeId, safeSubject);

        updateNodeForFts(nodeId, subject, stripHtmlTags(safeContent));

        return refreshTaxonomyTreeByIdOrParent(nodeId);
    }

    @Override
    public void addBikEntry(int cuId, int id, boolean inheritToDescendants) {
        execNamedCommand("addBikEntry", cuId, id, inheritToDescendants);
    }

    @Override
    public void deleteBikLinkedEntry(int cuId, int id) {
        execNamedCommand("deleteBikLinkedEntry", cuId, id);
    }

    @Override
    public ArticleBean getBikBizDefForNodeId(int nodeId) {
        return createBeanFromNamedQry("getBikBizDefForNodeId", ArticleBean.class, true, nodeId);
    }

    @Override
    public AttachmentBean getBikDocumentForSiId(int nodeId) {
        return createBeanFromNamedQry("getBikDocumentForSiId", AttachmentBean.class, true, nodeId);
    }

    @Override
    public List<String> getBikAllArts(int treeId) {
        final List<String> res = execNamedQuerySingleColAsList("getBikAllArts", "name" /* I18N: no */, treeId);
        return res;
    }

    // name, descr - optional (null - will not change)
    // to delete descr set it to empty string ("")
    protected void updateNodeForFts(int nodeId, String name, String descr) {

        Map<String, String> attrVals = new HashMap<String, String>();
        if (name != null) {
            attrVals.put("name" /* I18N: no */, name);
        }
        if (descr != null) {
            attrVals.put("descr" /* I18N: no */, descr);
            execNamedCommand("updatePlainDescription", descr.replaceAll("%.*?%", ""), nodeId);
//            if (BaseUtils.isStrEmptyOrWhiteSpace(descr)) //            if (!descr.contains(I18n.brakWersjiKorporacyjnej.get()))
//            {
//                addUserToRank(nodeId);
//            }
        }
//        fts.updateNodeAttributes(nodeId, attrVals);
//        registerChangeInNode(nodeId);
//        updateNodeAttributesAndRegisterChangeInNode(nodeId, attrVals);
    }

    public void addBikArticle(String title, String body, Set<String> tags,
            int official, int nodeId, String version) {

        String safeTitle = stripHtmlTags(cleanHtml(title));
        String safeBody = cleanHtml(body);
        String safeVersion = cleanHtml(version);
        Set<Integer> idNodeList = BIKCenterUtils.extractNodeIdsFromHtml(safeBody);
        execNamedCommand("addBikArticle", safeTitle, safeBody, official, nodeId, getLoggedUserId(), safeVersion);
        execNamedCommand("addNodeInArticle", nodeId, idNodeList);

        updateNodeForFts(nodeId, safeTitle, stripHtmlTags(safeBody));
        fillDefaultValuesForAttributes(nodeId);
    }

    @Override
    public List<String> deleteBikArticle(int id, int nodeId) {
        String branchIds = execNamedQuerySingleVal("deleteBikArticle", true, "branch_ids", id, nodeId);
        return refreshTaxonomyTree(branchIds);//execNamedQuerySingleColAsList("getCodeLinkedTreeNode", "code", nodeId);

    }

    @PerformOnAnyDb
    protected void removeCurrentUserFromLoggedUsers() {
        final String loggedUserName = getLoggedUserName();
        String currentSingleBiksDb = getOptSingleBiksDbForCurrentRequest();
        if (loggedUserName == null || currentSingleBiksDb == null) {
            return;
        }

        Map<String, Date> loggedUsers = getBOXIServiceImplDataForCurrentRequest().loggedUsers;
        if (loggedUsers != null) {
//            getBOXIServiceImplDataForCurrentRequest().loggedUsers.remove(getLoggedUserName());
            loggedUsers.remove(loggedUserName);
            getBOXIServiceImplDataForCurrentRequest().loggedUserAppInstances.remove(loggedUserName);
        }
    }

    @PerformOnDefaultDb
    protected boolean isClassicLoginDataCorrect(final String login, final String pwd) {
        //final BOXISessionData sessionData = getSessionData();

        if (isMultiXMode()) {
            Integer id = performOnMasterBiks(new IContinuationWithReturn<Integer>() {
                @Override
                public Integer doIt() {
                    return execNamedQuerySingleVal("mltxLoginPasswordCheck", true, "id", login, pwd);
                }
            });

            return id != null;
        } else {
//            return bikSystemUserByLoginAndPassword(login, pwd, null) != null;
            Integer existsAndDisabledStatus = execNamedQuerySingleVal("getBikSystemUserExistsAndDisabledStatus", true, null, login, pwd);
            // dajemy tylko info, czy login/pwd są OK, ale nie dajemy info czy user jest zablokowany
            return existsAndDisabledStatus != null; // && existsAndDisabledStatus == 0;
        }
    }

    @Override
    @PerformOnAnyDb
    @NotRequireUserLoggedIn
    public void performUserLogout() {
        removeCurrentUserFromLoggedUsers();

        BOXISessionData sessionData = getSessionData();
        if (sessionData != null) {
            sessionData.clearForLogout();
        }
        setLastLoggecUserId(-1);
    }

    public Integer getLoggedUserId() {
        SystemUserBean loggedUser = getLoggedUserBean();
        return loggedUser != null ? loggedUser.id : null;
    }

    public boolean userExistButDisabled() {
        return getSessionData().userExistButDisabled;
    }

    public void deleteBikNote(int id) {
        execNamedCommand("deleteBikNote", id);

    }

    public void editBikEntityNote(int id, String title, String body) {
        execNamedCommand("editBikEntityNote", id, title, body);
    }

    public void deleteBikLinkedMeta(Integer cuId, int id) {
        execNamedCommand("deleteBikLinedMeta", cuId, id);
    }

    public void deleteBikLinkedAtr(final Integer nodeId, final int id, final String value) {
        execNamedCommand("deleteBikLinkedAtr", nodeId, id, value);
//        String attrName = (String) execNamedQuerySingleVal("getAttributeNameById", false, "name" /* I18N: no */, id);
//        updateNodeAttributeForFts(nodeId, attrName, "");
        calculateAttributesAndIndexingForSearch(nodeId);
    }

    private void changeRelatedNodeInfoByAttribute(String attrName, String atrLinValue, Integer nodeId) {
//TODO: docelowo budujemy wyzwalaczy aby przy zmienieniu atrybuty, automatycznie tez zostana zmienione inne atrybuty
        TreeNodeBean node = getNodeBeanById(nodeId, false);
        if (BIKConstants.METABIKS_ATTR_ORIGINAL.equalsIgnoreCase(attrName) && BIKConstants.TREE_KIND_META_BIKS.equals(node.treeKind)) {
            node.name = atrLinValue.substring(atrLinValue.indexOf(",") + 1);
            updateTreeNode(node);
            Integer originalNodeId = BaseUtils.tryParseInteger(BaseUtils.splitBySep(atrLinValue, "_").get(0));
            Map<String, AttributeBean> attrsMap = buildNodeAttributesMap(originalNodeId);
            AttributeBean attr = attrsMap.get(BIKConstants.METABIKS_ATTR_NODE_KIND_CAPTION_PL);
            if (attr != null) {
                addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_NODE_KIND_CAPTION_PL, attr.atrLinValue, nodeId);
            }
            execNamedCommand("recalculateObjId", node.treeCode);
        }
    }

    public void addBikEntityAttrAssociateRoleWithAnObj(final String name, final String value, final int nodeId) {
        String safeName = stripHtmlTags(cleanHtml(name));
        String safeValue = cleanHtml(value);

        addOrUpdateAttribute(safeName, safeValue, nodeId);

        calculateAttributesAndIndexingForSearch(nodeId);
        List<AttributeBean> abl = getAttributesByNameValueNode(safeName, safeValue, nodeId);
        for (AttributeBean ab : abl) {
            if (ab.usedInDrools) {
                runRules(ab, null);
            }
        }
    }

    public void addBikEntityAttribute(final String name, final String value, final int nodeId) {
        String safeName = stripHtmlTags(cleanHtml(name));
        String safeValue = cleanHtml(value);
        addOrUpdateAttribute(safeName, safeValue, nodeId);
//        runWithArtificialAdminInNewThread(new IContinuation() {
//
//            @Override
//            public void doIt() {
//                updateNodeAttributeForFts(nodeId, name, value);
        calculateAttributesAndIndexingForSearch(nodeId);
//            }
//        });

        if (BaseUtils.tryParseBoolean(getAppPropValueEx("showAddStatus", true)) && safeName.equals(BIKConstants.METABIKS_ATTR_STATUS_NAME) && safeValue.equals(BIKConstants.YES)) {
            execNamedCommand("updateAttributeWhenStatusIsYes", nodeId);

        }

        List<AttributeBean> abl = getAttributesByNameValueNode(safeName, safeValue, nodeId);
        for (AttributeBean ab : abl) {
            if (ab.usedInDrools) {
                runRules(ab, null);
            }
        }
    }

    public void editBikEntityAttribute(final int id, final int nodeId, final String value) {
        String safeValue = cleanHtml(value);
        execNamedCommand("editBikLinkedAttribute", id, nodeId, safeValue);
        final String attrName = (String) execNamedQuerySingleVal("getAttributeNameById", false, "name" /* I18N: no */, id);
        changeRelatedNodeInfoByAttribute(attrName, safeValue, nodeId);

        if (BaseUtils.tryParseBoolean(getAppPropValueEx("showAddStatus", true)) && attrName.equals(BIKConstants.METABIKS_ATTR_STATUS_NAME) && safeValue.equals(BIKConstants.YES)) {
            execNamedCommand("updateAttributeWhenStatusIsYes", nodeId);

        }

        //        runWithArtificialAdminInNewThread(new IContinuation() {
        //
        //            @Override
        //            public void doIt() {
        //                updateNodeAttributeForFts(nodeId, attrName, atrLinValue);
        calculateAttributesAndIndexingForSearch(nodeId);
        //            }
        //        });
        List<AttributeBean> abl = getAttributesByNameValueNode(attrName, safeValue, nodeId);
        for (AttributeBean ab : abl) {
            if (ab.usedInDrools) {
                runRules(ab, null);
            }
        }
    }

    public List<String> editBikEntityAttrAssociateRoleWithAnObj(final int id, final int nodeId, final String value, List<String> rightRolesBefore) {
        List<String> listOfRolesWithAscribedUsers = new ArrayList();
        if (!rightRolesBefore.get(0).isEmpty()) {
            List<String> rightRolesAfter = new ArrayList();
            rightRolesAfter = Arrays.asList(value.split("[|]"));
            List<String> tempRoles = new ArrayList();
            if (rightRolesBefore.get(rightRolesBefore.size() - 1).contains("\n")) {
                String str = rightRolesBefore.get(rightRolesBefore.size() - 1);
                str = str.trim();
                rightRolesBefore.set(rightRolesBefore.size() - 1, str);
            }
            tempRoles.addAll(rightRolesBefore);

            for (String str : rightRolesBefore) {
                if (rightRolesAfter.contains(str)) {
                    tempRoles.remove(str);
                }
            }

            for (String roleName : tempRoles) {
                Integer i = execNamedQuerySingleValAsInteger("checkIfRoleHasNoAssociatedUsers", false, null, roleName);
                if (i != 0) {
                    listOfRolesWithAscribedUsers.add("Do roli \"" + roleName + "\" przypisani są użytkownicy. ("
                            + String.valueOf(i) + " uż.). ");
                }
            }
        }
        if (listOfRolesWithAscribedUsers.isEmpty()) {
            execNamedCommand("editBikLinkedAttribute", id, nodeId, value);
            final String attrName = (String) execNamedQuerySingleVal("getAttributeNameById", false, "name" /* I18N: no */, id);
            changeRelatedNodeInfoByAttribute(attrName, value, nodeId);

            calculateAttributesAndIndexingForSearch(nodeId);

            List<AttributeBean> abl = getAttributesByNameValueNode(attrName, value, nodeId);
            for (AttributeBean ab : abl) {
                if (ab.usedInDrools) {
                    runRules(ab, null);
                }
            }
        }
        return listOfRolesWithAscribedUsers;
    }

    @Override
    public Pair<Integer, List<DataSourceStatusBean>> getBikDataSourceStatuses(int pageNum, int pageSize) {

        Integer rowCnt = execNamedQuerySingleValAsInteger("getBikDataSourceStatusesCnt", true, null);
        if (rowCnt == null) {
            rowCnt = 0;
        }

        return new Pair<Integer, List<DataSourceStatusBean>>(rowCnt,
                createBeansFromNamedQry("getBikDataSourceStatuses", DataSourceStatusBean.class,
                        pageNum * pageSize + 1,
                        (pageNum + 1) * pageSize
                ));
    }

    public boolean addToFvs(List<Integer> nodeId) {
        execNamedCommand("addToFvs", getLoggedUserId(), nodeId);
        return true;
    }

    public boolean removeFromFvs(int nodeId) {
        execNamedCommand("removeFromFvs", getLoggedUserId(), nodeId);
        return false;
    }

    public boolean isInFvs(int nodeId) {
        Map<String, Object> row = execNamedQuerySingleRowCFN("isInFvs", true, FieldNameConversion.ToJavaPropName, getLoggedUserId(), nodeId);
        return row != null;
    }

    public List<ObjectInFvsChangeBean> getFavouritesForUser() {
        return getFavouritesForUser(getLoggedUserId());
    }

    public void addToHist(int nodeId) {
        Integer systemUserBikUserId = getLoggedUserId();

        if (systemUserBikUserId != null) {
            execNamedCommand("addToHist", systemUserBikUserId, nodeId);
        }
    }

    public int addFrequentlyAskedQuestions(int nodeId, String question, String answer) {
        String safeAnswer = cleanHtml(answer);
        String safeQuestion = stripHtmlTags(cleanHtml(question));
        final Integer id = execNamedQuerySingleVal("addFrequentlyAskedQuestions", true, "id" /* I18N: no */, nodeId, safeQuestion, safeAnswer);
        addAuthorToNode(id);
        updateNodeForFts(id, safeQuestion, stripHtmlTags(safeAnswer));
        fillDefaultValuesForAttributes(id);

        return id;
    }

    public void updateFrequentlyAskedQuestions(int id, String question, String answer) {
        String safeAnswer = cleanHtml(answer);
        String safeQuestion = stripHtmlTags(cleanHtml(question));
        execNamedCommand("updateFrequentlyAskedQuestions", id, safeQuestion, safeAnswer);
    }

    public void deleteFrequentlyAskedQuestions(int id) {
        execNamedCommand("deleteFrequentlyAskedQuestions", id);
    }

    public List<ObjectInHistoryBean> getFromHistory() {
        return createBeansFromNamedQry("getFromHistory", ObjectInHistoryBean.class, getLoggedUserId());
    }

    protected Pair<String, Integer> createOptMenuSubGroup(String parentMenuCode, Integer visualOrder, Map<String, String> optMenuSubGroup) {
        if (optMenuSubGroup != null && !optMenuSubGroup.isEmpty()/*!BaseUtils.isStrEmptyOrWhiteSpace(optMenuSubGroup)*/) {
            final String caption = optMenuSubGroup.get(getLanguage());
            String subMenuGroupCode = execNamedQuerySingleVal("addMenuSubGroup", false, null, caption, BaseUtils.fixIdentName(BaseUtils.deAccentPolishChars(caption), "", false),
                    visualOrder, BaseUtils.isStrEmptyOrWhiteSpace(parentMenuCode) ? null : parentMenuCode, optMenuSubGroup);
            parentMenuCode = subMenuGroupCode;
            visualOrder = 0;
        }

        return new Pair<String, Integer>(parentMenuCode, visualOrder);
    }

    //ww: parentMenuCode == null -> nie dodawaj w menu
    // parentMenuCode == "" -> dodaj pod rootem (najwyższy poziom, jak MyBIKS)
    public void createTree(Map<String, String> langToName, String treeKind, String parentMenuCode, Integer parentId, Integer visualOrder, Integer isAutoObjId,
            Map<String, String> optMenuSubGroup, String optServerTreeFileName) {
        String name = langToName.get(getLanguage());
        String codePrefix = BaseUtils.fixIdentName(BaseUtils.deAccentPolishChars(name), "", false);

        final int treeId = execNamedQuerySingleValAsInteger("createTree", false, null, name, codePrefix, treeKind, langToName, isAutoObjId);

        getAdhocDao().finishWorkUnit(true);

        Integer systemUserBikUserId = getLoggedUserId();
        if (systemUserBikUserId != null && !disableBuiltInRoles()) {
            execNamedCommand("addAuthorRoleToBIKSystemUserRight", systemUserBikUserId, treeId);
        }
        //execNamedCommand("createTree", name, codePrefix, treeKind);
        if (visualOrder == null) {
            visualOrder = execNamedQuerySingleValAsInteger("getMaxVisualOrderInParentTree", true, null, parentId);
            if (visualOrder == null) {
                visualOrder = 10000000;
            }
        }
        if (parentMenuCode != null) {
            Pair<String, Integer> p = createOptMenuSubGroup(parentMenuCode, visualOrder, optMenuSubGroup);
            execNamedCommand("addTreeToMenu", treeId, p.v1, p.v2);
        }

        /*
         Dodaj drzewo do statystyk
         */
        execNamedCommand("insertTreeToBIKStatistics", treeId);
//        importTree(treeId, optServerTreeFileName);
        deleteUnnecessaryAttrValue(treeId);
    }

    @PerformOnAnyDb
    protected String getServerSupportedLocaleNamesStr() {
        String res;

        //ww: ze względu na wołanie tego poza cyklem życia requestu
//        setCurrentDatabaseFromSession();
        if (isInMultiXModeAndNoSingleBiksDbDetermined()) {
            //setCurrentDbForAdhoc(defaultDatabaseName);
            res = performOnMasterBiks(new IContinuationWithReturn<String>() {

                @Override
                public String doIt() {
                    return multiXLoginsSubservice.getServerSupportedLocaleNamesStr();
                }
            });

        } else {
            res = performOnSingleBiks(new IContinuationWithReturn<String>() {

                @Override
                public String doIt() {
                    return getSensitiveAppPropValue("languages" /* I18N: no */);
                }
            });
        }

        return res;
    }

    @Override
    @PerformOnAnyDb
    public Set<String> getServerSupportedLocaleNames() {
        Set<String> res = BaseUtils.splitUniqueBySep(
                BaseUtils.safeToLowerCase(getServerSupportedLocaleNamesStr()), ",", true);
        if (BaseUtils.isCollectionEmpty(res)) {
            res
                    = //ww: nie może być singleton, bo się nie serializuje!
                    //Collections.singleton("pl" /* i18n: no */);
                    BaseUtils.paramsAsSet("pl" /* i18n: no */);
        }
        return res;
    }

    protected String getLanguage() {
        Set<String> languages = getServerSupportedLocaleNames();
        String pl = "pl" /* I18N: no */;
        if (languages.contains(pl)) {
            return pl;
        }
        return BaseUtils.safeGetFirstItem(languages, pl); //languages.get(0).toLowerCase();
    }

    public Map<String, List<String>> getMSSQLServerAndDatabasesForBOConnection() {
        Map<String, List<String>> map = new HashMap<String, List<String>>();
        List<Map<String, Object>> servers = execNamedQueryCFN("getMSSQLServerAndDatabases", FieldNameConversion.ToJavaPropName);
        for (Map<String, Object> record : servers) {
            String server = (String) record.get("sn" /* I18N: no */);
            if (!map.containsKey(server)) {
                List<String> list = new LinkedList<String>();
                list.add((String) record.get("db" /* I18N: no */));
                map.put(server, list);
            } else {
                map.get(server).add((String) record.get("db" /* I18N: no */));
            }
        }
        return map;
    }

    public Set<String> getOracleServerForBOConnection() {
        return execNamedQuerySingleColAsSet("getOracleServers", null);
    }

    public void setMSSQLServerAndDatabasesForBOConnection(int nodeId, String serverName, String databaseName, String schemaName) {
        execNamedCommand("setMSSQLServerAndDatabases", nodeId, serverName, databaseName, schemaName);
    }

    public void setOracleServerForBOConnection(int nodeId, String serverName) {
        execNamedCommand("setOracleServer", nodeId, serverName);
    }

    protected List<TreeBean> getTrees(boolean getAllTrees) {

        Map<Integer, List<Integer>> res = new HashMap<Integer, List<Integer>>();

        List<Map<String, Object>> rows = execNamedQueryCFN("getNodeKindIdInTreeId",
                FieldNameConversion.ToLowerCase);
        Map<Integer, List<TreeBean>> treeIdToKindsToAddOnTreesAsMainBranch = new HashMap<Integer, List<TreeBean>>();

        List<TreeBean> kindsToAddOnTreesAsMainBranch = createBeansFromNamedQry("getKindsToAddOnTreesAsMainBranch", TreeBean.class);

        for (TreeBean tb : kindsToAddOnTreesAsMainBranch) {
            int treeId = tb.id;

            List<TreeBean> nodeKindIds = treeIdToKindsToAddOnTreesAsMainBranch.get(treeId);
//              List<Integer> nodeKindIds = res.get(treeId);

            if (nodeKindIds == null) {
                nodeKindIds = new ArrayList<TreeBean>();
                treeIdToKindsToAddOnTreesAsMainBranch.put(treeId, nodeKindIds);
            }
            tb.requiredAttrs = getRequiredAttrsWithVisOrder(tb.nodeKindId);
            nodeKindIds.add(tb);

        }

        for (Map<String, Object> row : rows) {
            int treeId = sqlNumberInRowToInt(row, "tree_id");
            int nodeKindId = sqlNumberInRowToInt(row, "node_kind_id");

            List<Integer> nodeKindIds = res.get(treeId);
            if (nodeKindIds == null) {
                nodeKindIds = new ArrayList<Integer>();
                res.put(treeId, nodeKindIds);
            }
            nodeKindIds.add(nodeKindId);
        }
        List<TreeBean> tnbs = createBeansFromNamedQry("getTrees", TreeBean.class, getAllTrees);
        for (TreeBean tnb : tnbs) {
            tnb.nodeKindIds = res.get(tnb.id);
            tnb.nodeKindsToAddOnTreesAsMainBranch = treeIdToKindsToAddOnTreesAsMainBranch.get(tnb.id);
//            System.out.println(tnb.showAddMainBranchBtn + " " + tnb.id);
        }
        return tnbs;
//        return createBeansFromNamedQry("getTrees", TreeBean.class, getAllTrees);
    }

    public List<NodeKindBean> getNodeKinds() {
        List<NodeKindBean> nodeKinds = createBeansFromNamedQry("getNodeKinds", NodeKindBean.class);
        for (NodeKindBean nkb : nodeKinds) {
            nkb.associatedRoles = getRolesAssociatedWithObj(nkb.code);
        }
        return nodeKinds;
    }

    public List<TreeKindBean> getTreeKinds() {
        List<TreeKindBean> list = createBeansFromNamedQry("getTreeKinds", TreeKindBean.class);
        for (TreeKindBean tkb : list) {
            tkb.additionalNodeKindsId = createBeansFromNamedQry("getNodeKinds4TreeKind", NodeKind4TreeKind.class, tkb.id);
        }
        return list;
    }

    public Set<String> getDynamicTreeKindCodes() {
        return execNamedQuerySingleColAsSet("getDynamicTreeKindCodes", null);
    }

    protected List<TreeIconBean> getTreeIcons() {
        return createBeansFromNamedQry("getTreeIcons", TreeIconBean.class);
    }

    public TreeKindBean getTreeKind(int id) {
        return createBeanFromNamedQry("getTreeKindById", TreeKindBean.class, true, id);
    }

    public NodeKindBean getNodeKind(int id) {
        return createBeanFromNamedQry("getNodeKindById", NodeKindBean.class, true, id);
    }

    public Pair<Integer, List<String>> createTreeNode(Integer parentNodeId, int nodeKindId, String name, String objId, int treeId, Integer linkedNodeId) {
        return createTreeNode(parentNodeId, nodeKindId, name, objId, treeId, linkedNodeId, true);
    }

    protected Pair<Integer, List<String>> createTreeNode(Integer parentNodeId, int nodeKindId, String name, String objId, int treeId, Integer linkedNodeId, boolean fixName) {

        String safeName = cleanHtml(name);

        String treeKind = execNamedQuerySingleVal("getTreeKindByTreeId", false, null, treeId);
        String treeCode = getTreeCodeById(treeId);
        NodeKindBean nodeKind = getNodeKind(nodeKindId);

        if (fixName && BIKConstants.TREE_KIND_META_BIKS.equalsIgnoreCase(treeKind)
                && (BIKConstants.NODE_KIND_META_NODE_KIND.equalsIgnoreCase(nodeKind.code) || BIKConstants.NODE_KIND_META_TREE_KIND.equalsIgnoreCase(nodeKind.code) || BIKConstants.NODE_KIND_META_RELATED_NODE_KIND.equals(nodeKind.code))) {
            String codePrefix = BaseUtils.fixIdentName(BaseUtils.deAccentPolishChars(safeName), "", false);
            Integer n = null;
            Integer existNodeId = null;
            do {
                existNodeId = execNamedQuerySingleValAsInteger("getNodeIdByNameAndParentId", true, null, parentNodeId, n == null ? codePrefix : codePrefix + n);
                if (n == null) {
                    n = 1;
                } else {
                    n++;
                }
            } while (existNodeId != null);
            safeName = codePrefix + (n == 1 ? "" : (n - 1));
        }
//        if (BIKConstants.NODE_KIND_META_MENU_GROUP.equals(nodeKind.code)) {
//            String parentObjId = null;
//            if (parentNodeId != null) {
//                execNamedQuerySingleVal("getObjIdById", false, null, parentNodeId);
//            }
//            Map<String, String> menuSubGroup = new HashMap<String, String>();
//            menuSubGroup.put("pl", name);
//            Pair<String, Integer> p = createOptMenuSubGroup(parentObjId, 9999, menuSubGroup);
//            objId = p.v1;
//        }
        final Integer id = execNamedQuerySingleVal("createTreeNode", true, "id" /* I18N: no */, parentNodeId, nodeKindId, safeName, objId, treeId, linkedNodeId);
        if (id == null) {
            return null;
        }
        Integer isAutoObjId = execNamedQuerySingleValAsInteger("isTreeHasAutoObjId", false, null, treeCode);
        if (isAutoObjId > 0) {
            execNamedCommand("recalculateAutoObjId", treeCode);
        }
        if (!BaseUtils.isStrEmptyOrWhiteSpace(nodeKind.defaultDesc)) {
            TreeNodeBean node = getNodeBeanById(id, false);
            NameDescBean bean = new NameDescBean();
            bean.subject = node.name;
            bean.content = nodeKind.defaultDesc;
            updateDescription(bean, node);
        }
        addAuthorToNode(id);
        updateNodeForFts(id, safeName, null);
        if (execNamedQuerySingleColAsSet("getBikTreeBlogId", "id" /* I18N: no */).contains(treeId)) {
            addBikAuthorsBlog(id, getLoggedUserId(), true);
        }
        if (linkedNodeId == null) {
            fillDefaultValuesForAttributes(id);
        }
        if (!BaseUtils.isStrEmptyOrWhiteSpace(nodeKind.sql)) {
            runScript(id, nodeKind.sql);
        }

        return new Pair<Integer, List<String>>(id, refreshTaxonomyTreeByIdOrParent(id));
    }

    public Pair<Integer, List<String>> createTreeNodeWithAttr(Integer parentNodeId, int nodeKindId, String name, String objId, int treeId, Integer linkedNodeId, Set<Integer> addingJoinedObjs, Map<String, AttributeBean> requiredAttrs) {

        String safeName = cleanHtml(name);
        Map<String, AttributeBean> safeRequiredAttrs = new HashMap<String, AttributeBean>();

        for (Map.Entry<String, AttributeBean> entry : requiredAttrs.entrySet()) {
            AttributeBean safeAttributeBean = new AttributeBean();
            safeAttributeBean.id = entry.getValue().id;
            safeAttributeBean.atrLinValue = cleanHtml(entry.getValue().atrLinValue);
            safeRequiredAttrs.put(entry.getKey(), safeAttributeBean);
        }

        Pair<Integer, List<String>> p = createTreeNode(parentNodeId, nodeKindId, safeName, objId, treeId, linkedNodeId);
        Integer creatorId = getLoggedUserBean().userId != null ? getLoggedUserBean().userId : getLoggedUserBean().id;

        if (requiredAttrs != null && !requiredAttrs.isEmpty()) {
            execNamedCommand("setRequiredAttrs", p.v1, creatorId, safeRequiredAttrs);
//            NodeKindBean nodeKind = getNodeKind(nodeKindId);

//            if (BIKConstants.NODE_KIND_META_MENU_ITEM.equals(nodeKind.code)) {
//                Map<String, String> lang2Name = new HashMap<String, String>();
//                lang2Name.put("pl", name);
//                Map<String, AttributeBean> attrMaps = buildNodeAttributesMap(p.v1);
//                AttributeBean attr = attrMaps.get(BIKConstants.METABIKS_ATTR_TREE_TYPE);
//                if (attr == null || BaseUtils.isStrEmptyOrWhiteSpace(attr.atrLinValue)) {
//                    return null;
//                }
//                String treeKind = attr.atrLinValue;
//                String parentMenuCode = execNamedQuerySingleVal("getObjIdById", false, null, parentNodeId);
//                createTree(lang2Name, treeKind, parentMenuCode, null, 9999, 1, null, null);
//            }
        }
        if (!BaseUtils.isCollectionEmpty(addingJoinedObjs)) {
            Integer srcId = p.v1;
            for (Integer dstId : addingJoinedObjs) {
                addBikJoinedObject(srcId, dstId, false);
            }
        }
        if (parentNodeId != null) {
            List<AttributeBean> abl = getAttributesByNameValueNode(null, null, p.v1);
            for (AttributeBean ab : abl) {
                if (ab.usedInDrools) {
                    runRules(ab, null);
                }
            }
        }

        return p;
    }

    // tworzy wpisy z defaultowymi wartosciami do atrybutów dodatkowych
    protected void fillDefaultValuesForAttributes(int nodeId) {
        execNamedCommand("fillDefaultValuesForAttributes", nodeId); // po tej metodzie trzeba dodac atrybuty do wyszukiwarki
        calculateAttributesAndIndexingForSearch(nodeId);
    }

    public int createTreeNodeByCode(Integer parentNodeId, String nodeKindCode, String name, String objId, int treeId, Integer linkedNodeId, String descr) {
        Integer id = execNamedQuerySingleValAsInteger("createTreeNodeByCode", false, "id" /* I18N: no */, parentNodeId, nodeKindCode, name, objId, treeId, linkedNodeId, descr);
        execNamedCommand("node_init_branch_id_node", id);
        updateNodeForFts(id, name, descr);
        fillDefaultValuesForAttributes(id);
        return id;
    }

    @Override
    public List<String> updateTreeNode(final TreeNodeBean node) {
//żeby odświeżył i w przypadku podlinkowanego wyciętego i wklejonego
        return refreshTaxonomyTree(node.branchIds + updateTreeNodeEx(node));
    }

    protected String updateTreeNodeEx(final TreeNodeBean node) {
//        if (getLoggedUserBean() == null) {
//            throw new LameRuntimeException("User not logged in");
//        }

        String safeNodeDescr = cleanHtml(node.descr);
        String safeNodeDescrPlain = cleanHtml(node.descrPlain);
        String safeNodeName = cleanHtml(node.name);

        String oldHyperLinkInAttr = node.id + "_" + node.treeCode + "," + getNodeNamesByIds(Arrays.asList(node.id)).get(node.id);
        execNamedCommand("updateTreeNode", node.id, node.parentNodeId, node.nodeKindId, safeNodeName, node.objId, node.treeId, node.linkedNodeId);
        execNamedCommand("updateMenuParentIfNecessary", node.id, node.parentNodeId);
        execNamedCommand("node_init_branch_id", node.treeId); //....

        if (BaseUtils.safeEqualsAny(node.nodeKindCode,
                BIKConstants.NODE_KIND_GLOSSARY,
                BIKConstants.NODE_KIND_GLOSSARY_NOT_CORPO) //node.nodeKindCode.equals(I18n.glossary.get() /* I18N:  */) || node.nodeKindCode.equals("GlossaryNotCorpo")
                ) {
            List<Integer> a = execNamedQuerySingleColAsList("getNodeIds", "id" /* I18N: no */, node.id);
            for (int id : a) {
                execNamedCommand("updateName", id, safeNodeName);
            }
        }

        updateNodeForFts(node.id, safeNodeName, null);

        String branchIds = execNamedQuerySingleVal("updateLinkedTreeNode", true, "branch_ids", node.id, safeNodeName);
        List<Integer> nodeIds = execNamedQuerySingleColAsList("updateHyperLinkInAttr", null, oldHyperLinkInAttr, node.id + "_" + node.treeCode + "," + safeNodeName);
        if (BIKConstants.TREE_KIND_META_BIKS.equals(node.treeKind) && BIKConstants.NODE_KIND_META_ATTRIBUTE_TYPE.equals(node.nodeKindCode)) {
            //zmieniamy nazwy wezlow, ktore maja atrybut "wzorzec"
            for (int id : nodeIds) {
                execNamedCommand("updateMetaModelNodeName", id, safeNodeName);
            }
        }
        return branchIds;
    }

    public List<String> updateTreeNodeAndChangeFvs(final TreeNodeBean node) {

        List<Integer> nodeIdsCut = BIKCenterUtils.splitBranchIdsEx(node.branchIds, true);
        execNamedCommand("addObjectToFvsChangePast", node.id, nodeIdsCut, 2, node.branchIds);

        // w bazie danych parent jest jeszcze stary, branchIds też stare
        execNamedCommand("markNodeAsCutInFvsChangeEx", node.id);

        String updateBranchIds = updateTreeNodeEx(node);
        List<Integer> nodeIdsPaste = BIKCenterUtils.splitBranchIdsEx(updateBranchIds, true);
        execNamedCommand("addObjectToFvsChangePast", node.id, nodeIdsPaste, 3, updateBranchIds);

        // w bazie danych parent jest już nowy, branchIds też nowe
        execNamedCommand("markNodeAsPastedInFvsChangeEx", node.id);

        return refreshTaxonomyTree(node.branchIds + updateBranchIds);
    }

    protected String getConditionPartForBikNodeTreeOptMode(BikNodeTreeOptMode optExtraMode) {
        if (optExtraMode == null) {
            return null;
        }
        if (optExtraMode == BikNodeTreeOptMode.ActiveDQCTestsOnly) {
            return "node_kind_id not in ( " + BaseUtils.mergeWithSepEx(getBOXIServiceImplDataForCurrentRequest().inactiveTestNodeKindId, ",") + ")";
        }
        if (optExtraMode == BikNodeTreeOptMode.NotConnectedUsersOnly) {
            return "(dbo.fn_is_unconnected_user_node_or_has_unconnected_user_subnode(id, node_kind_id, linked_node_id, branch_ids, tree_id) = 1)";
            //return "(bik_node.node_kind_id=(select id from bik_node_kind where code='User') and bik_node.id not in (select node_id from bik_user bu inner join bik_system_user bsu on bsu.user_id=bu.id) and (bik_node.linked_node_id is null or bik_node.linked_node_id not in(select node_id from bik_user bu inner join bik_system_user bsu on bsu.user_id=bu.id)) ) or exists(select bndi.id from bik_node bndi with (index (idx_bik_node_branch_ids)) inner join bik_node_kind bnkdi on bndi.node_kind_id=bnkdi.id where bndi.branch_ids like bik_node.branch_ids + '%' and bndi.id not in (select node_id from bik_user bu inner join bik_system_user bsu on bsu.user_id=bu.id) and (bndi.linked_node_id is null or bndi.linked_node_id not in(select node_id from bik_user bu inner join bik_system_user bsu on bsu.user_id=bu.id)) and bnkdi.code = 'User' and bndi" /* I18N: no:sql */ + ".is_deleted = 0)";
        }
        if (optExtraMode == BikNodeTreeOptMode.DQMDBFunction) {
            return "node_kind_id in (select id from bik_node_kind where code in ('PostgresServer', 'PostgresDatabase', 'PostgresSchema', 'PostgresFolder', 'PostgresFunction', 'OracleServer', 'OracleOwner', 'OracleFolder', 'OracleFunction', 'MSSQLServer', 'MSSQLDatabase', 'MSSQLFolder', 'MSSQLTableFunction'))";
        }
        if (optExtraMode == BikNodeTreeOptMode.DQMDatabase) {
            return "node_kind_id in (select id from bik_node_kind where code in ('PostgresServer', 'PostgresDatabase', 'OracleServer', 'OracleOwner', 'MSSQLServer', 'MSSQLDatabase'))";
        }
        if (optExtraMode == BikNodeTreeOptMode.OnlyLinkedNodes) {
            return "linked_node_id is not null";
        }
        if (optExtraMode == BikNodeTreeOptMode.SAPBOReportFoldersOnly) {
            return "node_kind_id in (select id from bik_node_kind where code in ('ReportFolder'))";
        }
        if (optExtraMode == BikNodeTreeOptMode.SAPBOReportFoldersAndReportsOnly) {
            return "node_kind_id in (select id from bik_node_kind where code in ('Webi', 'ReportFolder'))";
        }
        if (optExtraMode == BikNodeTreeOptMode.SAPBOReportConnection) {
            return "node_kind_id in (select id from bik_node_kind where code like ('%Connection%'))";
        }

        throw new LameRuntimeException("unsupported" /* I18N: no */ + " optExtraMode: " + optExtraMode);
    }

    @Override
    public List<TreeNodeBean> getOneSubtreeLevelOfTree(int treeId, Integer parentNodeId,
            boolean noLinkedNodes,
            BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, boolean calcHasNoChildren, Integer nodeKindId, String relationType, String attributeName, Integer outerTreeId) {
        addAccessedTreeInCurrentSession(treeId);

        setOptFilteringNodeActionCodeForCurrentRequest(optFilteringNodeActionCode);
//"TU";
        List<TreeNodeBean> res = createBeansFromNamedQry("getOneSubtreeLevelOfTree", TreeNodeBean.class,
                treeId, parentNodeId, noLinkedNodes, getConditionPartForBikNodeTreeOptMode(optExtraMode), calcHasNoChildren, nodeKindId, relationType, attributeName, outerTreeId);
        translateSpecialTreeNodeBeans(res, true);

        return res;
    }

    public List<TreeNodeBean> getTrimmedTree(int treeId) {
        List<TreeNodeBean> l = getTreeNodes(treeId);
        trimTree(treeId, l);
        return l;
    }

    public List<TreeNodeBean> getTreeNodes(int treeId) {
        List<TreeNodeBean> tnb = createBeansFromNamedQry("getTreeNodes", TreeNodeBean.class, treeId);
        translateSpecialTreeNodeBeans(tnb, true);
        return tnb;
    }

    private void trimTree(int treeId, List<TreeNodeBean> l) {
        UniTreeBuilder utb = new UniTreeBuilder();
        TreeAlgorithms.buildTree(utb, l);
        utb.copyLinkedSubtrees(treeId);
        Map<Integer, UniTreeNode> m = utb.getUniNodesMap();
        l.clear();
        for (Entry e : m.entrySet()) {
            l.add(((UniTreeNode) e.getValue()).tnb);
        }
    }

//    protected void deleteNodesByIds(Collection<Integer> nodeIds) {
////        fts.deleteNodes(nodeIds);
////        registerChangeInNodes(nodeIds);
////        indexChangedNodes();
//    }
    public List<String> deleteTreeNode(TreeNodeBean node) throws LameRuntimeException {
        if (BIKConstants.TREE_KIND_META_BIKS.equalsIgnoreCase(node.treeKind)) {
            List<String> list = execNamedQuerySingleColAsList("getChildBranchIds", null, node.branchIds);
            Set<Integer> ids = new HashSet<Integer>();
            ids.add(node.id);
            for (String branchId : list) {
                List<String> l = BaseUtils.splitBySep(branchId.substring(node.branchIds.length()), "|");
                for (String id : l) {
                    if (!BaseUtils.isStrEmptyOrWhiteSpace(id)) {
                        ids.add(BaseUtils.tryParseInteger(id));
                    }
                }
            }
            for (Integer id : ids) {
                if (hasReferenceToBranch(id)) {
                    throw new LameRuntimeException("Instnieje referencja do node_id=" + node.id + " lub jego podrzędnych");
                }
            }
        }
        List<String> treeToRefresh = refreshTaxonomyTree(node.branchIds);
        updateNodeForFts(node.id, "", null);
        if (node.linkedNodeId == null) {

            List<Integer> ids = sqlNumbersToInts(
                    execNamedQuerySingleColAsList("deleteTreeNode", "id" /* I18N: no */, node.branchIds, node.id));
            if (!ids.isEmpty()) {
                List<Integer> idsFromLinked = sqlNumbersToInts(
                        execNamedQuerySingleColAsList("deleteTreeNodeByIds", "id" /* I18N: no */, ids));
                if (!idsFromLinked.isEmpty()) {
//                    deleteNodesByIds(idsFromLinked);
                }
            }
//            deleteNodesByIds(ids);
            execNamedCommand("deleteLinkedTreeNode", node.id);
            execNamedCommand("deleteAllUserInNodeAndSubnodes", node.branchIds);
            if (deleteBadLinkedUsersUnderRole()) {
                treeToRefresh.add(BIKConstants.TREE_CODE_USER_ROLES);
            }
            // dodac nullowanie pola user_id w bik_system_user
            if (node.nodeKindCode.equals(BIKConstants.NODE_KIND_USER)) {
                execNamedCommand("deleteBikUserFromBikSystemUser", node.id);
            }
            calculateAttributesAndIndexingForSearch(node.id);
        } else {
            execNamedCommand("deleteMercilessTreeNode", node.id);
        }

        return treeToRefresh;
    }

    public String getDatabaseVersion() {
        return getSensitiveAppPropValue("bik_ver");
    }

    public boolean getDeveloperModeFlag() {
//        String val = getSensitiveAppPropValue("developerMode");
//        if (BaseUtils.isStrEmptyOrWhiteSpace(val)) {
//            return false;
//        }
//        val = val.toLowerCase().trim();
//        return val.equals("1") || val.equals("true");
        return parseAppPropValueToBoolean("developerMode");
    }

    @Override
    public List<TreeNodeBean> getBikAllTreeNodes() {
        setOptFilteringNodeActionCodeForCurrentRequestAddJoinedObjs();
        List<TreeNodeBean> tnb = createBeansFromNamedQry("getBikAllTreeNodes", TreeNodeBean.class);
        translateSpecialTreeNodeBeans(tnb, false);
        return tnb;
    }

    @Override
    public List<TreeNodeBean> getBikAllTreeNodesExcludingOneTree(int treeId) {
        setOptFilteringNodeActionCodeForCurrentRequestAddJoinedObjs();
//        List<TreeNodeBean> tnb = createBeansFromNamedQry("getBikAllTreeNodesExcludingOneTree", TreeNodeBean.class, treeId);
        List<TreeNodeBean> tnb = createBeansFromNamedQry("getBikAllTreeNodesExcludingOneTree", TreeNodeBean.class, -1);
        System.out.println("aa");
        translateSpecialTreeNodeBeans(tnb, false);
        return tnb;
    }

    @Override
    public List<TreeNodeBean> getBikAllRootTreeNodesExcludingOneTree(int treeId, Integer nodeKindId, String relationType, String attributeName,
            List<Integer> parentNodeIdsDirectOverride /*,boolean isHideDictionaryRootTab -tymczasowe do ukrycia słowników*/) {
        setOptFilteringNodeActionCodeForCurrentRequestAddJoinedObjs();
        setOptFilteringNodeActionCodeForCurrentRequest("ShowInTree");
//        List<TreeNodeBean> tnb = createBeansFromNamedQry("getBikAllRootTreeNodesExcludingOneTree", TreeNodeBean.class, treeId/*, isHideDictionaryRootTab-tymczasowe do ukrycia słowników*/);
        List<TreeNodeBean> tnb = createBeansFromNamedQry("getBikAllRootTreeNodesExcludingOneTree", TreeNodeBean.class,
                treeId/*-1*/, nodeKindId, relationType, attributeName, parentNodeIdsDirectOverride/*, isHideDictionaryRootTab-tymczasowe do ukrycia słowników*/);
        translateSpecialTreeNodeBeans(tnb, false);
        return tnb;
    }

    @Override
    public void updateJoinedObjsFromParent(Integer nodeId, Set<Integer> dstId2Add, Set<Integer> dstId2Delete) {
        for (Integer dstNodeId : dstId2Add) {
            addBikJoinedObject(nodeId, dstNodeId, false);
        }
        for (Integer dstNodeId : dstId2Delete) {
            deleteBikJoinedObjs(nodeId, dstNodeId);
        }
    }

    public void addBikJoinedObject(int srcNodeId, int dstNodeId,
            boolean inheritToDescendants) {
        execNamedCommand("addBikJoinedObject", srcNodeId, dstNodeId,
                inheritToDescendants);
    }

    public void deleteBikJoinedObjs(int srcNodeId, int dstNodeId) {
        execNamedCommand("deleteBikJoinedObjs", srcNodeId, dstNodeId);
    }

    protected List<MenuNodeBean> getMenuNodes() {
        return createBeansFromNamedQry("getMenuNodes", MenuNodeBean.class);
    }

    @Override
    public StartupDataBean getStartupData() {
        System.out.println("getStartupData: start");
        try {
            clearAccessedTreesInCurrentSession();

            StartupDataBean sdb = new StartupDataBean();
            //ww: na razie tak robimy, bo bez tego podniesienie wersji bazy
            // po opaleniu BIKSa nie jest widziane w BIKSie - przeładowanie
            // aplikacji nie widzi tego
            clearAppPropsCache();

            sdb.canSendMails = isSharedMailServerCfgValid();

            //Pair<String, String> p = getApplicationVersion();
            sdb.isNasActive = isNasActive();
            sdb.isConfluenceActive = isConfluenceActive();
            sdb.appVersion
                    = //p.v1;
                    APP_VERSION;
//        sdb.dbVersion = //p.v2;
//                getDatabaseVersion();
//        sdb.developerMode = getDeveloperModeFlag();
//        sdb.allowedShowDiagKinds = getSensitiveAppPropValue("allowedShowDiagKinds");
            sdb.loggedUser = getLoggedUserBean();
            sdb.biksDatabaseName = mssqlConnCfg.database;

            sdb.developerModeStr = getAppPropValueEx("developerMode", true);

            getRightRolesForOneSystemUser(sdb.loggedUser);

            sdb.userExistButDisabled = userExistButDisabled();
            String remoteUser = getSessionData().remoteUser;
            sdb.ssoUserName = !isMultiDomainMode() && remoteUser != null ? remoteUser.replaceFirst(".*\\\\", "") : remoteUser;
            sdb.nodeKinds = getNodeKinds();
            sdb.trees = getTrees(true);
            sdb.treeKinds = getTreeKinds();
            sdb.treeIcons = getTreeIcons();
//            sdb.nodeKinds = createBeansFromNamedQry("getNodeKinds", NodeKindBean.class);
//            sdb.bikRolesForNode = createBeansFromNamedQry("getBikRolesForNode", BikRoleForNodeBean.class);
            sdb.appProps = getNonSensitiveAppProps();
            sdb.disableGuestMode = getDisableGuestModeVal(sdb.appProps);
            //System.out.println("appProps=" + sdb.appProps);
            sdb.menuNodes = getMenuNodes();
//            for (MenuNodeBean mnb : sdb.menuNodes) {
//                System.out.println(mnb.name);
//            }
            sdb.attrHints = getAttributes();
            sdb.jdbcConnectionTypes = getJdbcSourceTypes();
            sdb.systemAttribute = getSystemAttributes();
            sdb.allAttributesForNodeKindId = getAllAttributesForNodeKindId();

//            sdb.attrNameTranslations = getAttrNameTranslations();
            sdb.translationsGeneric = getTranslationsGeneric(
                    BIKConstants.TRANSLATION_KIND_ADEF, BIKConstants.TRANSLATION_KIND_ADCAT);
            sdb.openReportLink = getBOOpenDocLinkMap();
            sdb.reportTreeCodes = getBOReportTreeCodes();

            sdb.attrDefAndCategory = getAttrDefAndCategory(); // getAttributesDef(true, null);
            sdb.attrsDict = getAttributeDict();
//            sdb.attrsSearchable = getAllSearchableAttributes();
//            sdb.biSystems = getBISystemsConnectionsParameters();
            sdb.inactiveTabs = getInactiveTabs();
            sdb.changedRanking = getBOXIServiceImplDataForCurrentRequest().currentUserRanking;//getStarredUser();
//            lastRankingChangedTimeStamp = sdb.changedRanking.timeStamp;
//            changedRanking = getStarredUsers();

//            sdb.changedRanking = getStarredUsers();//new KeepAliveBean();
//            lastRankingChangedTimeStamp = sdb.changedRanking.timeStamp;
//            currentUserRanking = getStarredUsers();
            sdb.welcomeMsgForUser = getWelcomeMsgForUser();
            sdb.welcomeMsgForVersion = getWelcomeMsgForVersion();
            sdb.allRightRolesByCode = getAllRightRolesByCode();
            sdb.mssqlExProps = getMssqlExProps();
            sdb.adDomains = getADConnectionParameters().domains;
//            sdb.localeLangInfo = getLocaleLangInfo();

            sdb.customRightRoles = getCustomRightRoles();
            sdb.nodeActionCodeToIdMap = getNodeActionCodeToIdMap();
            sdb.customRightRolesActionsInTreeRoots = getCustomRightRolesActionsInTreeRoots();

            sdb.treeSelectorForRegularUserCrr = execNamedQuerySingleVal("getTreeSelectorForRegularUserCrr", true, null);

            sdb.showGrantsInTestConnection = parseAppPropValueToBoolean("showGrantsInTestConnection");

//            setServiceExtraResponseData(System.currentTimeMillis() + "");
            getSessionData().lastActivityInServerTimeMillis = System.currentTimeMillis();

            sdb.i18nTranslations = getI18nTranslations();
            sdb.useSimpleMsgForUnloggedUser = parseAppPropValueToBoolean("useSimpleMsgForUnloggedUser");
            sdb.isMultiDomainMode = isMultiDomainMode();
            sdb.allRoles = getAllUserGroups();
            System.out.println("getStartupData: end");
            return sdb;
        } catch (Exception ex) {
            System.out.println("getStartupData: exception: " + ex.getMessage());
            if (logger.isErrorEnabled()) {
                logger.error("ex in getStartupData()" /* I18N: no */, ex);
            }
            throw new RuntimeException("ex in" /* I18N: no */ + " getStartupData()", ex);
        }
    }

    protected boolean isMultiDomainMode() {
        return BaseUtils.tryParseBoolean(getAppPropValueEx("isMultiDomainMode", true), false);
    }

    @Override
    public SearchFullResult newSearch(final String text, final int offset,
            final int limit, final Collection<Integer> optTreeIds,
            final Collection<Integer> optKindIdsToSearch, final Collection<Integer> optKindIdsToExclude,
            final boolean calcKindAndCounts,
            final Collection<ISearchByAttrValueCriteria> optSBAVCs,
            final String ticket,
            final String opExpr,
            final boolean useLucene) {

        insertBIKStatisticsEveryTimeOneDay("newSearch", text);

        return runCancellableDBJob(ticket, new IContinuationWithReturn<SearchFullResult>() {
            @Override
            public SearchFullResult doIt() {
//                long start = System.currentTimeMillis();
//                SearchFullResult res = fts.search(text, offset, limit, optTreeIds, optKindIdsToSearch, optKindIdsToExclude,
//                        calcKindAndCounts, optSBAVCs, opExpr, useLucene);
//
//                if (logger.isDebugEnabled()) {
//                    System.out.println("search: text = " + text + " time = " + (System.currentTimeMillis() - start) / 1000.0);
//                }
//                return res;
                throw new UnsupportedOperationException();
            }
        });
    }

    @Override
    public boolean hasFulltextCatalog() {
        return getBOXIServiceImplDataForCurrentRequest().hasFulltextCatalog;
    }

    @Override
    public String performFullSearch(String query, int allCnt, Set<Integer> optTreeIds,
            Set<Integer> optKindIdsToSearch, Set<Integer> optKindIdsToExclude,
            boolean calcKindAndCounts, List<ISearchByAttrValueCriteria> optSBAVCs,
            String ticketForFullSearch, String opExpr,
            boolean useLucene) {
//        for (int lastOffset = 0; lastOffset < allCnt; lastOffset += MAX_NUMBER_OF_RESULTS_PER_SEARCH_REQUEST) {
//            //            System.out.println("Wynik od: " + lastOffset + " do " + (lastOffset + MAX_NUMBER_OF_RESULTS_PER_SEARCH_REQUEST));
//            SearchFullResult res = fts.search(query, lastOffset, MAX_NUMBER_OF_RESULTS_PER_SEARCH_REQUEST, optTreeIds, optKindIdsToSearch, optKindIdsToExclude,
//                    calcKindAndCounts, optSBAVCs, opExpr, useLucene);
//            execNamedCommand("cacheSearchResults", ticketForFullSearch, res.foundItems);
//        }

        return ticketForFullSearch;
    }

    @Override
    public void clearSearchResultsCache(String ticketForFullSearch) {
        execNamedCommand("clearCachedSearchResults", ticketForFullSearch);
    }

    public void setIndexVerIntoBikNode(Set<Integer> nodeId, int INDEX_VER) {
        if (nodeId == null) {
            execNamedCommand("setIndexVerIntoBikNodes", INDEX_VER);
        } else {
            execNamedCommand("setIndexVerIntoBikNode", nodeId, INDEX_VER);
        }
    }

//brzydkie ify
    public Pair<Integer, List<String>> createTreeNodeBiz(TreeNodeBean repo, ArticleBean param, int action/*
             * Set<String> depsToConnect
             */, String nodeKindCode) {
        //0- robocza , 1 -korpo, 2 - inna
        int nodeId = 0;
        String version = "";
        String subject = param.subject;
        if (param.official == 0) {
            subject = param.subject + " (" + I18n.robocza.get() /* I18N:  */ + ")";
            version = I18n.robocza.get() /* I18N:  */;
        } else if (param.official == 2) {
            subject = param.subject + " (" + param.versions + ")";
            version = param.versions;
        }
//add: 0-nowa,1- istnieje,2 -pod korpo
//edit: 3- z kazdej innej na inna 4z- innej na korpo //5-z korpo na zastap 6-z korpo na nowa
        if (action == 0) {
            if ((param.official != 1) && (nodeKindCode.equals(BIKConstants.NODE_KIND_GLOSSARY))) {//robocza lub inna pod wezlem korporacyjnej gdy jej nie ma)

                int official = param.official;
                param.official = 1;
                nodeId = addBikMetapedia(repo, param, BIKConstants.NODE_KIND_GLOSSARY_NOT_CORPO, version, ""/*"Brak wersji korporacyjnej"*/);
                addAuthorToNode(nodeId);
                repo.id = nodeId;
                param.official = official;
                nodeKindCode = BIKConstants.NODE_KIND_GLOSSARY_VERSION;
            }
            nodeId = addBikMetapedia(repo, param, nodeKindCode, version, param.body);
            addAuthorToNode(nodeId);
        } else if (action == 2) {
            repo.id = (Integer) execNamedQuerySingleVal("getBikMetapediaNodeId", true, "id" /* I18N: no */, param.subject, repo.treeId);//, repo.id + ""
            nodeId = addBikMetapedia(repo, param, BIKConstants.NODE_KIND_GLOSSARY_VERSION, version, param.body);
            addAuthorToNode(nodeId);
        } else if (action == 1) {
            nodeId = (Integer) execNamedQuerySingleVal("getBikMetapediaNodeId", true, "id" /* I18N: no */, subject, repo.treeId);//getBikMetapediaNodeId , repo.id + ""
            updateBikeMetapedia(subject, param.body, param.official, nodeId, version);
        } else if (action == 3) {//kazdy z innej na inna
            //sprawdzic czy nie ma..
//            execNamedCommand("deleteTreeNodeMetapedia", subject, repo.treeId);
            Set<Integer> deletedIds
                    = execNamedQuerySingleColAsSet("deleteTreeNodeMetapedia", null, subject, repo.treeId);
            //ww: usunięte, zamiatamy w FTSie
//            deleteNodesByIds(deletedIds);
//            registerChangeInNodes(deletedIds);
            updateBikeMetapedia(subject, param.body, param.official, repo.id, version);
            nodeId = repo.id;
//            execNamedCommand("updateJoinedObjec", repo.id, repo.parentNodeId);
        } else if (action == 4) {
            execNamedCommand("deleteTreeNode", repo.branchIds, repo.id);
            //ww: usuwamy i potem update corporate, więc usuwamy też z full text search
//            deleteNodesByIds(Collections.singleton(repo.id));
//            registerChangeInNode(repo.id);
            updateBikeMetapedia(param.subject, param.body, param.official, repo.parentNodeId, version);
            execNamedCommand("updateJoinedObjecToCorpo", repo.id, repo.parentNodeId);
            nodeId = repo.parentNodeId;
            updateAuthorForDefinition(nodeId, BIKConstants.NODE_KIND_GLOSSARY_NOT_CORPO);
        } else if (action == 5 || action == 6) {
            //z korporacyjnej na inna ->
            updateBikeMetapedia(param.subject, ""/*"Brak wersji korporacyjnej"*/, 1, repo.id, "");
            execNamedCommand("changeIconByCode", repo.id, param.subject, "GlossaryNotCorpo");
            if (action == 5) { //zastap
                nodeId = (Integer) execNamedQuerySingleVal("getBikMetapediaNodeId", true, "id" /* I18N: no */, subject, repo.treeId);
                updateBikeMetapedia(subject, param.body, param.official, nodeId, version);
            } else { //nowa
                nodeId = addBikMetapedia(repo, param, BIKConstants.NODE_KIND_GLOSSARY_VERSION, version, param.body);
            }
            execNamedCommand("updateJoinedObjecFromCorpo", repo.id, nodeId);
        }
        if (param.official == 1) {
            execNamedCommand("changeIconByCode", nodeId, subject, BIKConstants.NODE_KIND_GLOSSARY);
        }
        List<String> refreshTree = refreshTaxonomyTree(repo.branchIds);
        if (deleteBadLinkedUsersUnderRole()) {
            refreshTree.add(BIKConstants.TREE_CODE_USER_ROLES);
        }
        NameDescBean nameDescBean = new NameDescBean();
        nameDescBean.content = param.body;
        nameDescBean.subject = param.subject;
        TreeNodeBean node = getBikEntityDetailsData(nodeId, null, false).node;
        updateDescription(nameDescBean, node);
        return new Pair<Integer, List<String>>(nodeId, refreshTree/* refreshTaxonomyTree(repo.branchIds)*/);
    }

    protected Set<String> getInactiveTabs() {
        Set<String> inactiveTabs = new HashSet<String>();
        BISystemsConnectionsParametersBean biSystems = getBISystemsConnectionsParameters();
        biSystems.sapboCPB.servers.addAll(biSystems.sapbo4CPB.servers);
        for (ConnectionParametersSapBoBean instance : biSystems.sapboCPB.servers) {
            if (!instance.isActive) {
                inactiveTabs.add(instance.reportTreeCode);
                inactiveTabs.add(instance.universeTreeCode);
                inactiveTabs.add(instance.connectionTreeCode);
            }
        }
        if (biSystems.teradataCPB.isActive == 0) {
            inactiveTabs.add(BIKConstants.TREE_CODE_TERADATA);
        }
        if (biSystems.dqcCPB.isActive == 0) {
            inactiveTabs.add(BIKConstants.TREE_CODE_DQC);
        }
        if (biSystems.dqmCPB.isActive == 0) {
            inactiveTabs.add(BIKConstants.TREE_CODE_DQM);
        }
        if (biSystems.sasCPB.isActive == 0) {
            inactiveTabs.add(BIKConstants.TREE_CODE_SAS_CUBES);
            inactiveTabs.add(BIKConstants.TREE_CODE_SAS_INFORMATION_MAPS);
            inactiveTabs.add(BIKConstants.TREE_CODE_SAS_LIBRARIES);
            inactiveTabs.add(BIKConstants.TREE_CODE_SAS_REPORTS);
        }
        if (biSystems.sapbwCPB.isActive == 0) {
            inactiveTabs.add(BIKConstants.TREE_CODE_SAPBW_REPORTS);
            inactiveTabs.add(BIKConstants.TREE_CODE_SAPBW_PROVIDERS);
        }
        if (biSystems.profileCPB.isActive == 0) {
            inactiveTabs.add(BIKConstants.TREE_CODE_PROFILE);
        }
        if (biSystems.confluenceCPB.isActive == 0) {
            for (String treeCode : biSystems.confluenceCPB.confluenceTreeCodes) {
                inactiveTabs.add(treeCode);
            }
        }
        boolean isMSSQLActive = false;
        for (ConnectionParametersDBServerBean bean : biSystems.mssqlCPB.servers) {
            if (bean.isActive) {
                isMSSQLActive = true;
            }
        }
        if (!isMSSQLActive) {
            inactiveTabs.add(BIKConstants.TREE_CODE_MSSQL);
        }
        boolean isOracleActive = false;
        for (ConnectionParametersDBServerBean bean : biSystems.oracleCPB) {
            if (bean.isActive) {
                isOracleActive = true;
            }
        }
        if (!isOracleActive) {
            inactiveTabs.add(BIKConstants.TREE_CODE_ORACLE);
        }
        boolean isPostgresActive = false;
        for (ConnectionParametersDBServerBean bean : biSystems.postgresCPB) {
            if (bean.isActive) {
                isPostgresActive = true;
            }
        }
        if (!isPostgresActive) {
            inactiveTabs.add(BIKConstants.TREE_CODE_POSTGRES);
        }
        return inactiveTabs;
    }

    public int addBikMetapedia(TreeNodeBean repo, ArticleBean param, String nodeKindCode, String version, String body) {
        String subject = param.subject;
        if (param.official == 0) {
            subject = param.subject + " (" + I18n.robocza.get() /* I18N:  */ + ")";
            version = I18n.robocza.get() /* I18N:  */;
        } else if (param.official == 2) {
            subject = param.subject + " (" + param.versions + ")";
            version = param.versions;
        }
        int nodeId = createTreeNodeByCode(repo.id, nodeKindCode, subject, repo.objId, repo.treeId, repo.linkedNodeId, null);

        addBikArticle(param.subject, body, param.tags, param.official, nodeId, version);
        return nodeId;
    }

    public void updateBikeMetapedia(String subject, String body, int official, int nodeId, String version) {
        String safeSubject = stripHtmlTags(cleanHtml(subject));
        String safeBody = cleanHtml(body);
        String safeVersion = cleanHtml(version);
        execNamedCommand("updateBikArtDep", safeSubject, safeBody, official, nodeId, getLoggedUserId(), safeVersion);
        updateNodeForFts(nodeId, safeSubject, stripHtmlTags(safeBody));
    }

    public List<String> refreshTaxonomyTreeByIdOrParent(int parentNodeId) {
        String branchIds = execNamedQuerySingleVal("getParentBranchIds", false, "branch_ids", parentNodeId);
        return refreshTaxonomyTree(branchIds != null ? branchIds : "");
    }

    public List<String> refreshTaxonomyTree(String branchIds) {
        List<String> parentIds = BaseUtils.splitBySep(branchIds, "|");
        List<String> getCodeLinkedTreeNode = execNamedQuerySingleColAsList("getCodeLinkedTreeNode", "code" /* I18N: no */, parentIds);
        if (getCodeLinkedTreeNode.size() > 0) {
            return new ArrayList<String>(getCodeLinkedTreeNode);
        }
        return new ArrayList<String>();
    }

    @Override
    public List<TreeNodeBean> getBikDocumentTreeNodes() {
        setOptFilteringNodeActionCodeForCurrentRequestAddJoinedObjs();
        List<TreeNodeBean> tnb = createBeansFromNamedQry("getBikDocumentTreeNodes", TreeNodeBean.class);
        translateSpecialTreeNodeBeans(tnb, true);
        return tnb;
    }

    @Override
    public List<TreeNodeBean> getBikDocumentRootTreeNodes() {
        setOptFilteringNodeActionCodeForCurrentRequestAddJoinedObjs();
        List<TreeNodeBean> tnb = createBeansFromNamedQry("getBikDocumentRootTreeNodes", TreeNodeBean.class);
        translateSpecialTreeNodeBeans(tnb, true);
        return tnb;
    }

    @Override
    public DynamicTreesBigBean getBikTreeForData() {
        DynamicTreesBigBean res = new DynamicTreesBigBean();
        //res.dynamicTrees = createBeansFromNamedQry("getBikTreeForData", TreeBean.class);
        res.allTrees = getTrees(false);
        res.menuNodes = getMenuNodes();
        return res;
    }

//    @Override
//    public void changeMenuConfigTree(String treeCode) throws LameRuntimeException {
//        Map<String, Object> rs = execNamedQuerySingleRowCFN("getConfigTreeInfo", true, null, treeCode);
//        if (rs == null) {
//            throw new LameRuntimeException("Nie ma takiego drzewa");
//        }
//        Integer autoObjIds = (Integer) rs.get("is_auto_obj_id");
//        if (autoObjIds != 1) {
//            throw new LameRuntimeException("Drzewo konfiguracji nie ma opcji automatycznego wyliczania ID obiektów");
//        }
//        String treeKind = (String) rs.get("tree_kind");
//        if (!BIKConstants.TREE_KIND_META_BIKS_MENU_CONF.equalsIgnoreCase(treeKind)) {
//            throw new LameRuntimeException("Drzewo nie ma żądanego typu");
//        }
//        setAppPropValue("menuConfigTree", treeCode);
//    }
    @Override
    public void updateBikTree(final int id, Map<String, String> langToName, String parentMenuCode, int visualOrder, int isAutoObjId,
            Map<String, String> optMenuSubGroup, String treeKind, String optServerTreeFileName) {

        Map<String, String> safeLangToName = new HashMap<String, String>();

        for (Map.Entry<String, String> entry : langToName.entrySet()) {
            safeLangToName.put(entry.getKey(), cleanHtml(entry.getValue()));
        }

        String name = safeLangToName.get(getLanguage());

        execNamedCommand("updateBikTree", id, name, safeLangToName, isAutoObjId);

        if (treeKind != null) { // tylko dla drzew dynamicznych (pochodzących z szablonu)
//            addOrUpdateSpidHistoryForCurrentUser();
            execNamedCommand("updateBikTreeKind", id, treeKind);
        }

//        fts.updateTree(id);
        Pair<String, Integer> p = createOptMenuSubGroup(parentMenuCode, visualOrder, optMenuSubGroup);

        execNamedCommand("updateTreeInMenu", id, p.v1, p.v2);
//        importTree(id, optServerTreeFileName);
        deleteUnnecessaryAttrValue(id);
    }

    public void addBikTree(String name, int nodeKindId) {
        String safeName = cleanHtml(name);
        execNamedCommand("addBikTree", safeName);
    }

    protected TreeBean getOneTree(int treeId) {
        return createBeanFromNamedQry("getOneTree", TreeBean.class, false, treeId);
    }

    public void deleteTree(Integer treeId) {
        execNamedCommand("deleteTree", treeId);
    }

    public TreeKindBean addOrUpdateTreeType(List<TreeKindBean> tkbs, Integer kindId) {
        return addOrUpdateTreeType(tkbs, kindId, true, true);
    }

    public TreeKindBean addOrUpdateTreeType(List<TreeKindBean> tkbs, Integer kindId, boolean updateNodeKindCaption, boolean createNewNodeKind) {
        TreeKindBean tkb = tkbs.get(0);
        if (kindId == null) { // dodajemy typ drzewa
            String treeCode = BaseUtils.isStrEmptyOrWhiteSpace(tkb.code) ? BaseUtils.fixIdentName(BaseUtils.deAccentPolishChars(tkb.caption), "", false) : tkb.code;
            String branchNodeKindCode = BaseUtils.isStrEmptyOrWhiteSpace(tkb.branchCode) ? BaseUtils.fixIdentName(BaseUtils.deAccentPolishChars(tkb.branchCaption), "", false) : tkb.branchCode;
            String leafNodeKindCode = BaseUtils.isStrEmptyOrWhiteSpace(tkb.leafCode) ? BaseUtils.fixIdentName(BaseUtils.deAccentPolishChars(tkb.leafCaption), "", false) : tkb.leafCode;
            kindId = execNamedQuerySingleValAsInteger("createTreeType", false, "id" /* I18N: no */, treeCode, branchNodeKindCode, leafNodeKindCode, createNewNodeKind, tkb.caption, tkb.branchCaption, tkb.leafCaption, tkb.branchIcon, tkb.leafIcon, tkb.allowLinking, tkbs);
        } else { // edytujemy typ drzewa
            tkb.id = kindId;
            execNamedCommand("modifyTreeType", tkb.id, tkb.caption, tkb.branchCaption, tkb.leafCaption, tkb.branchIcon, tkb.leafIcon, tkb.allowLinking, tkbs, updateNodeKindCaption);
        }
        return getTreeKind(kindId);
    }

//    public TreeKindBean addOrUpdateTreeKind(Integer kindId, String caption, String branchCaption, String leafCaption, String branchIcon, String leafIcon, Integer allowLinking, String lang) {
//        if (kindId == null) { // dodajemy typ drzewa
//            String treeCode = BaseUtils.fixIdentName(BaseUtils.deAccentPolishChars(caption), "", false);
//            String branchNodeKindCode = BaseUtils.fixIdentName(BaseUtils.deAccentPolishChars(branchCaption), "", false);
//            String leafNodeKindCode = BaseUtils.fixIdentName(BaseUtils.deAccentPolishChars(leafCaption), "", false);
//            kindId = execNamedQuerySingleValAsInteger("createTreeKind", false, "id" /* I18N: no */, treeCode, branchNodeKindCode, leafNodeKindCode, caption, branchCaption, leafCaption, branchIcon, leafIcon, allowLinking, lang);
//        } else { // edytujemy typ drzewa
//            execNamedCommand("modifyTreeKind", kindId, caption, branchCaption, leafCaption, branchIcon, leafIcon, allowLinking, lang);
//        }
//        return getTreeKind(kindId);
//    }
//
//    public TreeKindBean addOrUpdateTreeKind(Integer kindId, String caption, String branchCaption, String leafCaption, String branchIcon, String leafIcon, Integer allowLinking, String lang) {
//        if (kindId == null) { // dodajemy typ drzewa
//            String treeCode = BaseUtils.fixIdentName(BaseUtils.deAccentPolishChars(caption), "", false);
//            String branchNodeKindCode = BaseUtils.fixIdentName(BaseUtils.deAccentPolishChars(branchCaption), "", false);
//            String leafNodeKindCode = BaseUtils.fixIdentName(BaseUtils.deAccentPolishChars(leafCaption), "", false);
//            kindId = execNamedQuerySingleValAsInteger("createTreeKind", false, "id" /* I18N: no */, treeCode, branchNodeKindCode, leafNodeKindCode, caption, branchCaption, leafCaption, branchIcon, leafIcon, allowLinking, lang);
//        } else { // edytujemy typ drzewa
//            execNamedCommand("modifyTreeKind", kindId, caption, branchCaption, leafCaption, branchIcon, leafIcon, allowLinking, lang);
//        }
//        return getTreeKind(kindId);
//    }
    public NodeKindBean addOrUpdateNodeKind(Integer kindId, String code, String caption, String iconName, String treeKind, Integer isFolder, String childrenKinds, String childrenAttributes, Integer isRegistry, String jasperReport) {
        if (kindId == null) { // dodajemy typ noda
            String nodeKindCode = BaseUtils.isStrEmptyOrWhiteSpace(code) ? BaseUtils.fixIdentName(BaseUtils.deAccentPolishChars(caption), "", false) : code;
            kindId = execNamedQuerySingleValAsInteger("createNodeKind", false, "id", nodeKindCode, caption, iconName, treeKind, isFolder, childrenKinds, childrenAttributes, isRegistry, jasperReport);
        } else { // edytujemy typ noda
            //default dla uploadable
            execNamedCommand("modifyNodeKind", kindId, caption, iconName, treeKind, isFolder, childrenKinds, childrenAttributes, isRegistry, jasperReport);
        }
        return getNodeKind(kindId);
    }

    public NodeKindBean addOrUpdateNodeKind(Integer kindId, String code, String caption, String iconName, String treeKind, Integer isFolder, String childrenKinds, String childrenAttributes, String jasperReport) {
        return addOrUpdateNodeKind(kindId, code, caption, iconName, treeKind, isFolder, childrenKinds, childrenAttributes, 0, jasperReport);
    }

    @Override
    public void sendMail(String subject, String body, String... recipients) {
        sendMailWithCC(subject, body, recipients, new String[]{});
    }

    public void sendMailWithCC(String subject, String body, String[] recipients, String[] cc) {
        sendMailWithCCAndFiles(subject, body, null, recipients, cc);
//        try {
//            ThreadedMailSender mailSender = getBOXIServiceImplDataForCurrentRequest().mailSender;
//            mailSender.sendMailWithCC(recipients, cc, subject, body, null);
////            SendMail.sendMessage(getSharedMailServerCfg(), recipients, subject, body, null);
//        } catch (Exception ex) {
//            throw new RuntimeException(ex);
//        }
    }

    public void sendMailWithFiles(String subject, String body, Map<String, String> embeddedFiles, String... recipients) {
        try {
            execNamedCommand("addMailLogMsg", "Mail start");
            ThreadedMailSender mailSender = getBOXIServiceImplDataForCurrentRequest().mailSender;
            mailSender.sendMailWithCC(recipients, new String[]{}, subject, body, embeddedFiles);
            execNamedCommand("addMailLogMsg", "Mail end");
        } catch (Exception ex) {
            execNamedCommand("addMailLogMsg", ex.getMessage());
            throw new RuntimeException(ex);
        }
    }

    public void sendMailWithCCAndFiles(String subject, String body, Map<String, String> embeddedFiles, String[] recipients, String[] cc) {
        try {
            ThreadedMailSender mailSender = getBOXIServiceImplDataForCurrentRequest().mailSender;
            mailSender.sendMailWithCC(recipients, cc, subject, body, embeddedFiles);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public int updateMetapediaArt(int id, String subject, String body, int official, int nodeId,
            int action) {
        execNamedCommand("updateBikArtDep", subject, body, official, nodeId, getLoggedUserId(), action);

        return 0;
    }

    //    usuwanie z powiązań na zakładce Społeczności BI
    public Integer deleteJoinedObjectWithRoleFromUser(int nodeId, int roleInKindId, int inheritToDescendants, boolean isAuxiliary, int userNodeId) {
//        execNamedCommand("deleteJoinedObjectWithRoleFromUser", nodeId, roleInKindId, inheritToDescendants, isAuxiliary, userNodeId);
        Integer i = execNamedQuerySingleVal("deleteJoinedObjectWithRoleFromUser", true, null, nodeId, roleInKindId, inheritToDescendants, isAuxiliary, userNodeId);
        return i;//deleteBadLinkedUsersUnderRole();
    }

    public boolean addUserInNode(int nodeId, Integer userId, int roleForNodeId, int inheritToDescendants, boolean isAuxiliary, JoinTypeRoles joinTypeRoles) {
        //ww: zakomentować
//
        if (joinTypeRoles == JoinTypeRoles.Primary || isAuxiliary/* joinTypeRoles == JoinTypeRoles.Auxiliary*/) {
            execNamedCommand("addUserInNode", nodeId, userId, roleForNodeId, inheritToDescendants, isAuxiliary);
        } else {
            execNamedCommand("addUserInNodeEx", nodeId, userId, roleForNodeId, inheritToDescendants, isAuxiliary, joinTypeRoles.ordinal());
        }
        //ww: zamiast powyższego - tylko to
//        execNamedCommand("addUserInNode", nodeId, userId, roleInKindId, inheritToDescendants, isAuxiliary);
        return deleteBadLinkedUsersUnderRole();
    }

    public synchronized void addVote(int nodeId, int value) {
        execNamedCommand("addVote", nodeId, getLoggedUserId(), value);
    }

    public synchronized void deleteLoggedUserVoteForNode(int nodeId) {
        execNamedCommand("deleteVote", nodeId, getLoggedUserId());
    }

    @Override
    public List<TreeNodeBean> getAllTreeNodes(Integer treeId) {

        List<TreeNodeBean> tnb = createBeansFromNamedQry("getAllTreeNodes", TreeNodeBean.class, treeId);
        return tnb;
    }

    @Override
    public List<TreeNodeBean> getAllTreeNodesToExport(Integer treeId, String branchId) {

        setOptFilteringNodeActionCodeForCurrentRequest("Rename");// tu powinna być jakaś akcja którą mogą wszystcy użytkownicy poza zwykłym (ShowInTree nie pasowało)
        List<TreeNodeBean> tnb = createBeansFromNamedQry("getAllTreeNodesToExport", TreeNodeBean.class, treeId, branchId);
        return tnb;
    }

    public List<TreeNodeBean> getBikAllJoinableTreeNodes() {
        setOptFilteringNodeActionCodeForCurrentRequestAddJoinedObjs();
        List<TreeNodeBean> tnb = createBeansFromNamedQry("getBikAllJoinableTreeNodes", TreeNodeBean.class
        );
        translateSpecialTreeNodeBeans(tnb,
                false);
        return tnb;
    }

    public List<TreeNodeBean> getBikAllJoinableRootTreeNodes(Set<String> treeKind, boolean noLinkedNodes, Integer nodeKindId, String relationType, Integer treeId) {
        setOptFilteringNodeActionCodeForCurrentRequestAddJoinedObjs();
        List<TreeNodeBean> tnb = createBeansFromNamedQry("getBikAllJoinableRootTreeNodes", TreeNodeBean.class,
                getDynTreeKindsWhenTreeKindIsDynamic(treeKind), noLinkedNodes, nodeKindId, relationType, treeId);
        translateSpecialTreeNodeBeans(tnb,
                false);

        return tnb;
    }

    protected Collection<String> getDynTreeKindsWhenTreeKindIsDynamic(Collection<String> treeKinds) {
        Set<String> treeKindsSet = new HashSet<String>();
        for (String treeKind : treeKinds) {
            if (treeKind.equals(BIKConstants.TREE_KIND_DYNAMIC_TREE)) {
                for (TreeKindBean tkb : getTreeKinds()) {
                    treeKindsSet.add(tkb.code);
                }
            } else {
                treeKindsSet.add(treeKind);
            }
        }
        return treeKindsSet;
    }

    protected Collection<String> getDynTreeKindsWhenTreeKindIsDynamic(String treeKind) {
        return getDynTreeKindsWhenTreeKindIsDynamic(Collections.singleton(treeKind));
    }

    public List<TreeNodeBean> getBikRolesTreeNodes(boolean onlyTopRoles) {
        List<TreeNodeBean> tnb = createBeansFromNamedQry("getBikRolesTreeNodes", TreeNodeBean.class,
                onlyTopRoles);
        translateSpecialTreeNodeBeans(tnb,
                true);
        return tnb;
    }

    //ww->pane: kto jest autorem? kto użył tego w getBikEntityDetailsData?
//ww: sam nie wiem o co mi chodziło :-( ... ;-)
    public UserBean
            bikUserByNodeId(int nodeId) {
        UserBean user = createBeanFromNamedQry("bikUserByNodeId", UserBean.class, true, nodeId);
        return user;
    }

    public UserExtBean
            bikExtUserByNodeId(int nodeId) {
        UserExtBean user = createBeanFromNamedQry("bikUserByNodeId", UserExtBean.class, true, nodeId);
        return user;
    }

    public UserBean
            bikUserByLogin(String loginName) {
        return createBeanFromNamedQry("bikUserByLogin", UserBean.class, true, loginName);
    }

    public BlogBean
            getBikEntryByNodeId(int nodeId) {
        return createBeanFromNamedQry("getBikEntryByNodeId", BlogBean.class, true, nodeId);
    }

    public List<BikRoleForNodeBean> getBikRolesForNode() {
        return createBeansFromNamedQry("getBikRolesForNode", BikRoleForNodeBean.class
        );
    }

    public void addBikAuthorsBlog(int nodeId, int userId, boolean isAdmin) {
        execNamedCommand("addBikAuthorsBlog", nodeId, userId, isAdmin);
    }

    public void deleteBikAuthorsBlog(int nodeId, int userId) {
        execNamedCommand("deleteBikAuthorsBlog", nodeId, userId);
    }

    public void updateBikAuthorsBlogToAdmin(int nodeId, int userId) {
        execNamedCommand("updateBikAuthorsBlogToAdmin", nodeId, userId, getLoggedUserId());
    }

    public NewDefinitionDataBean getNewDefinitionData(int treeId) {
        NewDefinitionDataBean res = new NewDefinitionDataBean();
//        res.defNames = getBikParentNodeIdArts(nodeId);//getBikAllArts();
//        res.bizDefs = getBizDefVersInDepartments(nodeId);
        res.defAndVersion = getBikAllDefAndVersion(treeId);
        return res;
    }

    @Override
    protected Map<String, IBeanConnectionThreadHandle> getCancellableExecutionsInSession() {
        return getSessionData().cancellableExecutions;
    }

    protected List<IBIKPumpTaskNew> createPumpListAndAdd(IBIKPumpTaskNew... pumps) {
        List<IBIKPumpTaskNew> pumpList = new LinkedList<IBIKPumpTaskNew>();
        pumpList.addAll(Arrays.asList(pumps));
        return pumpList;
    }

    protected List<IBIKPumpTaskNew> setOptInstanceName(String instanceName, List<IBIKPumpTaskNew> pumps) {
        for (IBIKPumpTaskNew pump : pumps) {
            pump.setOptPumpInstanceName(instanceName);
        }
        return pumps;
    }

    @Override
    public void runTeradataPump() {
        getPumpExecutor().addPumpToJobExecutorQueue(createPumpListAndAdd(new TeradataPump()));
    }

    @Override
    public void runTeradataDataModelPump() {
        getPumpExecutor().addPumpToJobExecutorQueue(createPumpListAndAdd(new TeradataDataModel()));
        // napchanie drzewek nowymi danymi i odswiezenie zakladki admin
    }

    @Override
    public void runErwinDataModelPump() {
        getPumpExecutor().addPumpToJobExecutorQueue(createPumpListAndAdd(new ErwinPump()));
    }

    @Override
    public void runOraclePump(String instanceName) {
        getPumpExecutor().addPumpToJobExecutorQueue(setOptInstanceName(instanceName, createPumpListAndAdd(new OraclePump())));
    }

    @Override
    public void runPostgresPump(String instanceName) {
        getPumpExecutor().addPumpToJobExecutorQueue(setOptInstanceName(instanceName, createPumpListAndAdd(new PostgreSQLPump())));
    }

    @Override
    public void runPlainFilePump(String instanceName) {
        getPumpExecutor().addPumpToJobExecutorQueue(setOptInstanceName(instanceName, createPumpListAndAdd(new PlainFilePump())));
    }

    @Override
    public void runJdbcPump(String instanceName) {
        getPumpExecutor().addPumpToJobExecutorQueue(setOptInstanceName(instanceName, createPumpListAndAdd(new JdbcPump())));
    }

    @Override
    public void runFileSystemPump(String instanceName) {
        getPumpExecutor().addPumpToJobExecutorQueue(setOptInstanceName(instanceName, createPumpListAndAdd(new FileSystemPump())));
    }

    @Override
    public void runDynamicAxPump(String instanceName) {
        getPumpExecutor().addPumpToJobExecutorQueue(setOptInstanceName(instanceName, createPumpListAndAdd(new DynamicAxPump())));
    }

    @Override
    public void runProfilePump() {
        getPumpExecutor().addPumpToJobExecutorQueue(createPumpListAndAdd(new ProfilePump()));
    }

    @Override
    public void runMSSQLPump(String instanceName) {
        getPumpExecutor().addPumpToJobExecutorQueue(setOptInstanceName(instanceName, createPumpListAndAdd(new MsSqlPump())));
    }

    @Override
    public void runDQCPump() {
        getPumpExecutor().addPumpToJobExecutorQueue(createPumpListAndAdd(new DQCPump()));
    }

    @Override
    public void runADPump() {
        getPumpExecutor().addPumpToJobExecutorQueue(createPumpListAndAdd(new ADPump()));
    }

    @Override
    public void runSAPBWPump() {
        getPumpExecutor().addPumpToJobExecutorQueue(createPumpListAndAdd(new SapBWPump()));
    }

    @Override
    public void runConfluencePump() {
        getPumpExecutor().addPumpToJobExecutorQueue(createPumpListAndAdd(new ConfluencePump()));
    }

    @Override
    public void runSapBOPump(String instanceName, String source) {
//        BOXIServiceImplData serviceImplData = getBOXIServiceImplDataForCurrentRequest();
        ThreadedPumpExecutor pumpExecutor = getPumpExecutor();
        if (source.equals(PumpConstants.SOURCE_NAME_SAPBO)) {
            pumpExecutor.addPumpToJobExecutorQueue(setOptInstanceName(instanceName, createPumpListAndAdd(new SapBOPump())));
        }
        if (source.equals(PumpConstants.SOURCE_NAME_SAPBO4)) {
            pumpExecutor.addPumpToJobExecutorQueue(setOptInstanceName(instanceName, createPumpListAndAdd(new SapBO4Pump())));
        }
    }

    @Override
    public void runSapBODesignerPump(String instanceName, String source) {
//        BOXIServiceImplData serviceImplData = getBOXIServiceImplDataForCurrentRequest();
        ThreadedPumpExecutor pumpExecutor = getPumpExecutor();
        if (source.equals(PumpConstants.SOURCE_NAME_SAPBO)) {
            pumpExecutor.addPumpToJobExecutorQueue(setOptInstanceName(instanceName, createPumpListAndAdd(new DesignerPump())));
        }
        if (source.equals(PumpConstants.SOURCE_NAME_SAPBO4)) {
            pumpExecutor.addPumpToJobExecutorQueue(setOptInstanceName(instanceName, createPumpListAndAdd(new Designer4Pump())));
        }
    }

    @Override
    public void runSapBOIDTPump(String instanceName) {
//        BOXIServiceImplData serviceImplData = getBOXIServiceImplDataForCurrentRequest();
        ThreadedPumpExecutor pumpExecutor = getPumpExecutor();
        pumpExecutor.addPumpToJobExecutorQueue(setOptInstanceName(instanceName, createPumpListAndAdd(new IDTPump())));
    }

    @Override
    public void runSapBOReportPump(String instanceName, String source) {
//        BOXIServiceImplData serviceImplData = getBOXIServiceImplDataForCurrentRequest();
        ThreadedPumpExecutor pumpExecutor = getPumpExecutor();
        if (source.equals(PumpConstants.SOURCE_NAME_SAPBO)) {
            pumpExecutor.addPumpToJobExecutorQueue(setOptInstanceName(instanceName, createPumpListAndAdd(new ReportPump())));
        }
        if (source.equals(PumpConstants.SOURCE_NAME_SAPBO4)) {
            pumpExecutor.addPumpToJobExecutorQueue(setOptInstanceName(instanceName, createPumpListAndAdd(new Report4Pump())));
        }
    }

    @Override
    public void runSapBOPumpAll(String instanceName, String source) {
//        BOXIServiceImplData serviceImplData = getBOXIServiceImplDataForCurrentRequest();
        ThreadedPumpExecutor pumpExecutor = getPumpExecutor();
        if (source.equals(PumpConstants.SOURCE_NAME_SAPBO)) {
            pumpExecutor.addPumpToJobExecutorQueue(setOptInstanceName(instanceName, createPumpListAndAdd(new SapBOPump(), new DesignerPump(), new ReportPump())));
        }
        if (source.equals(PumpConstants.SOURCE_NAME_SAPBO4)) {
            pumpExecutor.addPumpToJobExecutorQueue(setOptInstanceName(instanceName, createPumpListAndAdd(new SapBO4Pump(), new Designer4Pump(), new IDTPump(), new Report4Pump())));
        }
    }

    @Override
    public void runPermissionsPump(String sqlName) {
        getPumpExecutor().addPumpToJobExecutorQueue(setOptInstanceName(sqlName, createPumpListAndAdd(new AnySqlPump())));
    }

    @Override
    public void runAll() {
        BISystemsConnectionsParametersBean systems = getBISystemsConnectionsParameters();
        List<IBIKPumpTaskNew> pumpToStart = new LinkedList<IBIKPumpTaskNew>();
        // wazna jest kolejnosc wrzucania na liste pumpToStart!!!
        if (systems.teradataCPB.isActive == 1) {
            pumpToStart.add(new TeradataPump());
        }
        for (ConnectionParametersDBServerBean mssqlbean : systems.mssqlCPB.servers) {
            if (mssqlbean.isActive) {
                pumpToStart.add(new MsSqlPump());
                break;
            }
        }
        for (ConnectionParametersDBServerBean oraclebean : systems.oracleCPB) {
            if (oraclebean.isActive) {
                pumpToStart.add(new OraclePump());
                break;
            }
        }
        for (ConnectionParametersDBServerBean postgresbean : systems.postgresCPB) {
            if (postgresbean.isActive) {
                pumpToStart.add(new PostgreSQLPump());
                break;
            }
        }
        for (ParametersAnySqlBean anySqlCPB : systems.anySqlsPB) {
            if (anySqlCPB.isActive) {
                pumpToStart.add(new AnySqlPump());
                break;
            }
        }

//        if(systems.anySqlsPB.isActive/*==1*/){
//             pumpToStart.add(new AnySqlPump());
//        }
        for (ConnectionParametersJdbcBean jdbcbean : systems.jdbcCPB) {
            if (jdbcbean.isActive) {
                pumpToStart.add(new JdbcPump());
                break;
            }
        }
        // TF: do odkomentowania w przypadku implementacji konektora
//        for (ConnectionParametersFileSystemBean jdbcbean : systems.fileSystemCPB) {
//            if (jdbcbean.isActive) {
//                pumpToStart.add(new FileSystemPump());
//                break;
//            }
//        }

        for (ConnectionParametersPlainFileBean cppfb : systems.plainFileCPB) {
            if (cppfb.isActive) {
                pumpToStart.add(new PlainFilePump(cppfb.name));
//                break;
            }
        }
        for (ConnectionParametersSapBoBean boBean : systems.sapboCPB.servers) {
            if (boBean.isActive) {
                SapBOPump javaSDK = new SapBOPump();
                javaSDK.setOptPumpInstanceName(boBean.name);
                DesignerPump designerSDK = new DesignerPump();
                designerSDK.setOptPumpInstanceName(boBean.name);
                ReportPump reportSDK = new ReportPump();
                reportSDK.setOptPumpInstanceName(boBean.name);
                pumpToStart.add(javaSDK);
                pumpToStart.add(designerSDK);
                pumpToStart.add(reportSDK);
            }
        }
        for (ConnectionParametersSapBoBean boBean : systems.sapbo4CPB.servers) {
            if (boBean.isActive) {
                SapBO4Pump javaSDK = new SapBO4Pump();
                javaSDK.setOptPumpInstanceName(boBean.name);
                Designer4Pump designerSDK = new Designer4Pump();
                designerSDK.setOptPumpInstanceName(boBean.name);
                IDTPump idtSDK = new IDTPump();
                idtSDK.setOptPumpInstanceName(boBean.name);
                Report4Pump reportSDK = new Report4Pump();
                reportSDK.setOptPumpInstanceName(boBean.name);
                pumpToStart.add(javaSDK);
                pumpToStart.add(designerSDK);
                pumpToStart.add(idtSDK);
                pumpToStart.add(reportSDK);
            }
        }
        if (systems.dqcCPB.isActive == 1) {
            pumpToStart.add(new DQCPump());
        }
        if (systems.adCPB.isActive == 1) {
            pumpToStart.add(new ADPump());
        }
        if (systems.sapbwCPB.isActive == 1) {
            pumpToStart.add(new SapBWPump());
        }
        if (systems.profileCPB.isActive == 1) {
            pumpToStart.add(new ProfilePump());
        }
        if (systems.confluenceCPB.isActive == 1) {
            pumpToStart.add(new ConfluencePump());
        }
        getPumpExecutor().addPumpToJobExecutorQueue(pumpToStart);
    }

    @Override
    public Pair<Integer, String> runSapBoTestConnection(int id) {
        return new SapBOTestConnection(getMssqlConnLoadCfg(), id).testConnection();
    }

    @Override
    public Pair<Integer, String> runTeradataTestConnection() {
        return new TeradataTestConnection(getMssqlConnLoadCfg()).testConnection();
    }

    @Override
    public Pair<Integer, String> runDQCTestConnection() {
        return new DQCTestConnection(getMssqlConnLoadCfg()).testConnection();
    }

    @Override
    public Pair<Integer, String> runADTestConnection() {
        return new ADTestConnection(getMssqlConnLoadCfg()).testConnection();
    }

    @Override
    public Pair<Integer, String> runSASTestConnection() {
        return new SASTestConnection(getMssqlConnLoadCfg()).testConnection();
    }

    @Override
    public Pair<Integer, String> runSapBWTestConnection() {
        return new SapBWTestConnection(getMssqlConnLoadCfg()).testConnection();
    }

    @Override
    public Pair<Integer, String> runConfluenceTestConnection() {
        return new ConfluenceTestConnection(getMssqlConnLoadCfg()).testConnection();
    }

    @Override
    public Pair<Integer, String> runMsSqlTestConnection(int id, String optObjsForGrantsTesting) {
        return new MsSqlTestConnection(getMssqlConnLoadCfg(), id, optObjsForGrantsTesting).testConnection();
    }

    @Override
    public Pair<Integer, String> runOracleTestConnection(int id, String optObjsForGrantsTesting) {
        return new OracleTestConnection(getMssqlConnLoadCfg(), id, optObjsForGrantsTesting).testConnection();
    }

    @Override
    public Pair<Integer, String> runPostgresTestConnection(int id, String optObjsForGrantsTesting) {
        return new PostgreSQLTestConnection(getMssqlConnLoadCfg(), id, optObjsForGrantsTesting).testConnection();
    }

    @Override
    public Pair<Integer, String> runProfileTestConnection() {
        return new ProfileTestConnection(getMssqlConnLoadCfg()).testConnection();
    }

    @Override
    public Pair<Integer, String> runJdbcTestConnection(int id) {
        return new JdbcTestConnection(getMssqlConnLoadCfg(), id).testConnection();
    }

    @Override
    public Pair<Integer, String> runFileSystemTestConnection(int id) {
        return new FileSystemTestConnection(getMssqlConnLoadCfg(), id).testConnection();
    }

    @Override
    public Pair<Integer, String> runPlainFileTestConnection(int id) {
        return new PlainFileTestConnection(getMssqlConnLoadCfg(), id).testConnection();
    }

    @Override
    public List<String> getActiveSystemInstance(String source) {
        // rozwiazanie tymczasowe, docelowo przydala by sie struktura trzymajaca w sobie, dla konektora nazwe jej tabeli konfiguracyjnej
        String dataConfigTable = null;
        if (BaseUtils.safeEquals(PumpConstants.SOURCE_NAME_JDBC, source)) {
            source = null;
            dataConfigTable = "bik_jdbc";
        } else if (BaseUtils.safeEquals(PumpConstants.SOURCE_NAME_FILE_SYSTEM, source)) {
            source = null;
            dataConfigTable = "bik_file_system";
        } else if (BaseUtils.safeEquals(PumpConstants.SOURCE_NAME_SAPBO, source) || BaseUtils.safeEquals(PumpConstants.SOURCE_NAME_SAPBO4, source)) {
            dataConfigTable = "bik_sapbo_server";
        } else if (BaseUtils.safeEquals(PumpConstants.SOURCE_NAME_PLAIN_FILE, source)) {
            source = null;
            dataConfigTable = "bik_plain_file";
        } else if (BaseUtils.safeEquals(PumpConstants.SOURCE_NAME_BIKS_SQL, source)) {
            source = null;
            dataConfigTable = "bik_any_sql";
        } else { // dla innych - bazy danych
            dataConfigTable = "bik_db_server";
        }
        return execNamedQuerySingleColAsList("getActiveSystemInstance", "instance", source, dataConfigTable);
    }

    public String setUserAvatar(String avatar) {
        execNamedCommand("setUserAvatar", getLoggedUserId(), avatar);
        if (avatar == null) {
            avatar = getUserAvatar();
        }

        getLoggedUserBean().avatar = avatar;
        return avatar;
    }

    protected String getUserAvatar() {
        String avatarFieldForAd = getAvatarFieldForAd();
        return execNamedQuerySingleVal("getUserAvatar", true, "avatar" /* I18N: no */, getLoggedUserId(), avatarFieldForAd);
    }

    public Pair<Integer, List<String>> promoteMetapediaArt(TreeNodeBean repo, int id, String subject, String body, int official, int nodeId,
            int action, String versions/*
     * Set<String> depsToConnect
     *//*
     * , String branchIds
     */) {

        if (official == 1) {
            execNamedCommand("changeIconByCode", nodeId, subject, BIKConstants.NODE_KIND_GLOSSARY);
            updateMetapediaArt(id, subject, body, official, nodeId, /*
                     * depsToConnect
                     */ action);
        } else {
            updateMetapediaArt(id, subject, "" /*I18n.brakWersjiKorporacyjnej.get()*//*"Brak wersji korporacyjnej"*/, 1, nodeId, /*
                     * depsToConnect
                     */ action);
            execNamedCommand("changeIconByCode", nodeId, subject, BIKConstants.NODE_KIND_GLOSSARY_NOT_CORPO);
            ArticleBean param = new ArticleBean();
            param.body = body;
            param.subject = subject;
            param.versions = versions;
            param.official = official;
            nodeId = addBikMetapedia(repo, param, BIKConstants.NODE_KIND_GLOSSARY_VERSION, /*
                     * subject,
                     */ versions, param.body);
            addAuthorToNode(nodeId);

        }
        return new Pair<Integer, List<String>>(nodeId, refreshTaxonomyTreeByIdOrParent(nodeId));
    }

    public BISystemsConnectionsParametersBean getBISystemsConnectionsParameters() {
        BISystemsConnectionsParametersBean cpb = new BISystemsConnectionsParametersBean();

        cpb.sapboCPB = getSapBo31ExConnectionParameters();
        cpb.sapbo4CPB = getSapBo4ExConnectionParameters();
        cpb.teradataCPB = getTeradataConnectionParameters();
        cpb.dqcCPB = getDQCConnectionParameters();
        cpb.oracleCPB = getOracleConnectionParameters();
        cpb.postgresCPB = getPostgresConnectionParameters();
        cpb.mssqlCPB = getMsSqlConnectionParameters();
        cpb.jdbcCPB = getAllJdbcConnectionParameters();
        cpb.fileSystemCPB = getFileSystemConnectionParameters();
        cpb.plainFileCPB = getPlainFileConnectionParameters();
        cpb.dynamicAxCPB = getDynamicAxConnectionParameters();
        cpb.adCPB = getADConnectionParameters();
        cpb.sasCPB = getSASConnectionParameters();
        cpb.sapbwCPB = getSapBWConnectionParameters();
        cpb.profileCPB = getProfileConnectionParameters();
        cpb.erwinCPB = getErwinConnectionParameters();
        cpb.confluenceCPB = getConfluenceConnectionParameters();
        cpb.dqmCPB = getDQMConnectionParameters();
        cpb.anySqlsPB = getPermissionsConnectionParameters();

        return cpb;
    }

    protected <B> void setAdminBean(B bean, String prefix) {
        Map<String, Object> map = LameUtils.namedPropsBeanToMap(new NamedPropsBeanEx(bean));
        final String prefixWithDot = prefix + ".";
        for (String key : map.keySet()) {
            execNamedCommand("updateAdminValue", prefixWithDot + key, map.get(key));
        }
    }

    protected Map<Integer, String> getBOOpenDocLinkMap() {
        Map<Integer, String> map = new HashMap<Integer, String>();
        List<ConnectionParametersSapBoBean> cfgs = getSapBoAllConnectionParameters();
        for (ConnectionParametersSapBoBean cfg : cfgs) {
            map.put(cfg.reportTreeId, cfg.openReportTemplate);
        }
        return map;
    }

    protected Set<String> getBOReportTreeCodes() {
        return execNamedQuerySingleColAsSet("getBOReportTreeCodes", null);
    }

    public void setBODesignerPath(String path, String source) {
        if (source.equals(PumpConstants.SOURCE_NAME_SAPBO)) {
            execNamedCommand("updateAdminValue", "sapbo.path", path);
        }
        if (source.equals(PumpConstants.SOURCE_NAME_SAPBO4)) {
            execNamedCommand("updateAdminValue", "sapbo4.path", path);
        }
    }

    public void setReportSDKPath(String path) {
        execNamedCommand("updateAdminValue", "sapbo4.reportPath", path);
    }

    public void setPath(String path, String pathName) {
        execNamedCommand("updateAdminValue", pathName, path);

    }

    protected List<ConnectionParametersSapBoBean> getSapBoAllConnectionParameters() {
        return createBeansFromNamedQry("getBOConnectionParameters", ConnectionParametersSapBoBean.class, null, null);
    }

    protected ConnectionParametersSapBoExBean getSapBo31ExConnectionParameters() {
        ConnectionParametersSapBoExBean conn = new ConnectionParametersSapBoExBean();
        conn.servers = createBeansFromNamedQry("getBOConnectionParameters", ConnectionParametersSapBoBean.class, PumpConstants.SOURCE_NAME_SAPBO, null);
        conn.exProp = new SapBoExPropConfigBean();
        conn.exProp.path = execNamedQuerySingleVal("getAdminSingleValue", true, "value" /* I18N: no */, "sapbo.path");
        return conn;
    }

    public ConnectionParametersSapBo4ExBean getSapBo4ExConnectionParameters() {
        ConnectionParametersSapBo4ExBean conn = new ConnectionParametersSapBo4ExBean();
        conn.servers = createBeansFromNamedQry("getBOConnectionParameters", ConnectionParametersSapBoBean.class, PumpConstants.SOURCE_NAME_SAPBO4, null);
        conn.exProp = readAdminBean("sapbo4", SapBo4ExPropConfigBean.class);
//        conn.exProp = new SapBo4ExPropConfigBean();
//        conn.exProp.path = execNamedQuerySingleVal("getAdminSingleValue", true, "value" /* I18N: no */, "sapbo4.path");
//        conn.exProp.reportPath = execNamedQuerySingleVal("getAdminSingleValue", true, "value" /* I18N: no */, "sapbo4.reportPath");
//        conn.exProp.idtPath = execNamedQuerySingleVal("getAdminSingleValue", true, "value" /* I18N: no */, "sapbo4.idtPath");
//        conn.exProp.clientToolsPath = execNamedQuerySingleVal("getAdminSingleValue", true, "value" /* I18N: no */, "sapbo4.clientToolsPath");
        return conn;
    }

    protected ConnectionParametersSapBoBean
            getSapBoConnectionParametersForInstance(Integer instanceId) {
        return createBeanFromNamedQry("getBOConnectionParameters", ConnectionParametersSapBoBean.class, false, null, instanceId);
    }

    protected ConnectionParametersTeradataBean
            getTeradataConnectionParameters() {
        return readAdminBean("teradata" /* I18N: no */, ConnectionParametersTeradataBean.class
        );
    }

    protected ConnectionParametersDQMBean
            getDQMConnectionParameters() {
        ConnectionParametersDQMBean bean = readAdminBean("dqm" /* I18N: no */, ConnectionParametersDQMBean.class
        );
//        bean.servers = createBeansFromNamedQry("getDBParameters", ConnectionParametersDBServerBean.class, PumpConstants.SOURCE_NAME_MSSQL, null);
        bean.connections = createBeansFromNamedQry("getDQMConnectionParameters", ConnectionParametersDQMConnectionsBean.class);
//        bean.exProp = readAdminBean("mssql", MsSqlExPropConfigBean.class);
        return bean;
//        return readAdminBean("dqm" /* I18N: no */, ConnectionParametersDQMBean.class);
    }

    protected ConnectionParametersSASBean
            getSASConnectionParameters() {
        return readAdminBean("sas" /* I18N: no */, ConnectionParametersSASBean.class
        );
    }

    protected ConnectionParametersSapBWBean
            getSapBWConnectionParameters() {
        return readAdminBean("sapbw" /* I18N: no */, ConnectionParametersSapBWBean.class
        );
    }

    protected ConnectionParametersProfileBean
            getProfileConnectionParameters() {
        return readAdminBean("profile" /* I18N: no */, ConnectionParametersProfileBean.class
        );
    }

    protected ConnectionParametersErwinBean
            getErwinConnectionParameters() {
        return readAdminBean("erwin" /* I18N: no */, ConnectionParametersErwinBean.class
        );
    }

    protected List<ParametersAnySqlBean>
            getPermissionsConnectionParameters() {
//        return readAdminBean("permissions" /* I18N: no */, ParametersAnySqlBean.class
//        );
        return createBeansFromNamedQry("getAnySqls", ParametersAnySqlBean.class);
//        return null;
    }

    protected ConnectionParametersMsSqlBean getMsSqlConnectionParameters() {
        ConnectionParametersMsSqlBean bean = new ConnectionParametersMsSqlBean();
        bean.servers = createBeansFromNamedQry("getDBParameters", ConnectionParametersDBServerBean.class, PumpConstants.SOURCE_NAME_MSSQL, null);
        bean.exProp = readAdminBean("mssql", MsSqlExPropConfigBean.class);
        return bean;
    }

    protected List<ConnectionParametersDBServerBean> getOracleConnectionParameters() {
        return createBeansFromNamedQry("getDBParameters", ConnectionParametersDBServerBean.class, PumpConstants.SOURCE_NAME_ORACLE, null);
    }

    protected List<ConnectionParametersDBServerBean> getPostgresConnectionParameters() {
        return createBeansFromNamedQry("getDBParameters", ConnectionParametersDBServerBean.class, PumpConstants.SOURCE_NAME_POSTGRESQL, null);
    }

    protected void setVisibleTree(Object treeCode, Integer isActive) {
        execNamedCommand("setVisibleTree", treeCode, isActive == 0);
    }

    protected void setVisibleTree(Object treeCode, boolean isActive) {
        execNamedCommand("setVisibleTree", treeCode, !isActive);
    }

    public void setSapBoConnectionParameters(ConnectionParametersSapBoBean bean) {
//        setAdminBean(bean, "sapbo");
        execNamedCommand("setSapBoConnectionParameters", bean);
        setVisibleTree(bean.reportTreeCode, bean.isActive);
        setVisibleTree(bean.universeTreeCode, bean.isActive);
        setVisibleTree(bean.connectionTreeCode, bean.isActive);
    }

    public void setTeradataConnectionParameters(ConnectionParametersTeradataBean bean) {
        setAdminBean(bean, "teradata" /* I18N: no */);
        setVisibleTree(BIKConstants.TREE_CODE_TERADATA, bean.isActive);
    }

    public void setDQCConnectionParameters(ConnectionParametersDQCBean bean) {
        setAdminBean(bean, "dqc" /* I18N: no */);
        setVisibleTree(BIKConstants.TREE_CODE_DQC, bean.isActive);
        if (bean.isActive == 1 && bean.checkTestActivity == 1) {
            getBOXIServiceImplDataForCurrentRequest().dqcTestRequestsReceiver = new DQCTestRequestsReceiver(new MssqlConnectionConfig(bean.server, bean.instance, bean.database, bean.user, bean.password));
        }
    }

    public void setADConnectionParameters(ConnectionParametersADBean bean) {
        setAdminBean(bean, "ad" /* i18n: no */);
    }

    public void setMsSqlConnectionParameters(ConnectionParametersDBServerBean bean) {
        setDBConfig(bean);
        setVisibleTree(BIKConstants.TREE_CODE_MSSQL, isAnyDbServerActive(getMsSqlConnectionParameters().servers));
    }

    protected void setDbActivityInBikNode(ConnectionParametersDBServerBean bean) {
        if (bean.isActive) {
            activateDBServer(bean);
        } else {
            deactivateDBServer(bean);
        }
    }

    public void setMsSqlExPropParameters(MsSqlExPropConfigBean bean) {
        setAdminBean(bean, "mssql" /* i18n: no */);
        setExPropToTranslateTable(bean);
    }

    protected void setExPropToTranslateTable(MsSqlExPropConfigBean bean) {
        Set<String> locales = getServerSupportedLocaleNames();
        execNamedCommand("cleanMssqlExPropTranslates");
        Map<String, String> localMap = new HashMap<String, String>();
        for (String localName : locales) {
            localMap.clear();
            Field[] fields = bean.getClass().getFields();
            for (Field field : fields) {
                try {
                    String val = (String) field.get(bean);
                    Map<String, String> exPropList = PumpUtils.getExPropList(val, localName);
                    localMap.putAll(exPropList);
//                    System.out.println("local: " + localName + ", field: " + field.getName() + ", map: " + exPropList);
                } catch (Exception ex) {
                    System.out.println("Error in getFields: " + ex.getMessage());
                }
            }
            execNamedCommand("saveMssqlExPropTranslateForLanguage", localMap, localName);
        }
//        addOrUpdateSpidHistoryForCurrentUser();
        execNamedCommand("indexMssqlExPropTranslate");
    }

    public void setOracleConnectionParameters(ConnectionParametersDBServerBean bean) {
        setDBConfig(bean);
        setVisibleTree(BIKConstants.TREE_CODE_ORACLE, isAnyDbServerActive(getOracleConnectionParameters()));
    }

    public void setPostgresConnectionParameters(ConnectionParametersDBServerBean bean) {
        setDBConfig(bean);
        setVisibleTree(BIKConstants.TREE_CODE_POSTGRES, isAnyDbServerActive(getPostgresConnectionParameters()));
    }

    public void setProfileConnectionParameters(ConnectionParametersProfileBean bean) {
        setAdminBean(bean, "profile" /* I18N: no */);
        setVisibleTree(BIKConstants.TREE_CODE_PROFILE, bean.isActive);
    }

    public void setSASConnectionParameters(ConnectionParametersSASBean bean) {
        setAdminBean(bean, "sas" /* I18N: no */);
        Set<String> treesCode = new HashSet<String>();
        treesCode.add(BIKConstants.TREE_CODE_SAS_REPORTS);
        treesCode.add(BIKConstants.TREE_CODE_SAS_CUBES);
        treesCode.add(BIKConstants.TREE_CODE_SAS_INFORMATION_MAPS);
        treesCode.add(BIKConstants.TREE_CODE_SAS_LIBRARIES);
        setVisibleTree(treesCode, bean.isActive);
    }

    public void setSapBWConnectionParameters(ConnectionParametersSapBWBean bean) {
        setAdminBean(bean, "sapbw" /* I18N: no */);
        Set<String> treesCode = new HashSet<String>();
        treesCode.add(BIKConstants.TREE_CODE_SAPBW_REPORTS);
        treesCode.add(BIKConstants.TREE_CODE_SAPBW_PROVIDERS);
        setVisibleTree(treesCode, bean.isActive);
    }

    public void setConfluenceConnectionParameters(ConnectionParametersConfluenceBean bean) {
        setAdminBean(bean, "confluence" /* I18N: no */);
        List<String> confluenceTreeCodes = getConfluenceTreeCodes();
        for (String tree : confluenceTreeCodes) {
            setVisibleTree(tree, bean.isActive);
        }
        if (bean.isActive == 1) {
            getBOXIServiceImplDataForCurrentRequest().confluenceProxyConnector = ConfluenceProxy.makeProxy(new ConfluenceConnector(new ConfluenceConfigBean(bean.isActive, bean.url, bean.user, bean.password, bean.checkChildrenCount)));
        }
    }

    public void setConnectionParametersErwinBean(ConnectionParametersErwinBean bean) {
        setAdminBean(bean, "erwin" /* I18N: no */);
        setVisibleTree(BIKConstants.TREE_CODE_TERADATADATAMODEL, bean.isActive);
    }

    public void setPermissionsConnectionParameters(ParametersAnySqlBean bean) {
        setAdminBean(bean, "permissions" /* i18n: no */);
    }

    protected boolean isAnyDbServerActive(List<? extends ConnectionParametersDBServerBean> servers) {
        boolean isActive = false;
        for (ConnectionParametersDBServerBean server : servers) {
            if (server.isActive) {
                isActive = true;
                break;
            }
        }
        return isActive;
    }

    protected void setDBConfig(ConnectionParametersDBServerBean bean) {
        ConnectionParametersDBServerBean oldConfig = getDBParametersForId(bean.id);
        execNamedCommand("setDBConfig", bean.id, bean.name, bean.server, bean.instance, bean.user, bean.password, bean.port, bean.isActive, bean.databaseFilter, bean.objectFilter);
        if (oldConfig.isActive != bean.isActive || !BaseUtils.safeEquals(oldConfig.databaseFilter, bean.databaseFilter)) {
            bean.treeId = oldConfig.treeId; // bo do klienta nie przekazujemy tree_id
            bean.databaseList = oldConfig.databaseList; // bo do klienta nie przekazujemy databaseList
            setDbActivityInBikNode(bean);
        }
    }

    public ConnectionParametersDBServerBean addDBConfig(String sourceName, String configName, boolean needSeparateSchedule, String treeCode) {
        int id = sqlNumberToInt(execNamedQuerySingleVal("addDBConfig", true, "id" /* I18N: no  */, sourceName, configName, needSeparateSchedule, treeCode));
        return getDBParametersForId(id);
    }

    public ConnectionParametersDBServerBean
            getDBParametersForId(Integer serverId) {
        return createBeanFromNamedQry("getDBParameters", ConnectionParametersDBServerBean.class, false, null, serverId);
    }

    public ConnectionParametersDBServerBean
            getOneDQMConnectionParameters(Integer connectionId) {
        return createBeanFromNamedQry("getOneDQMConnectionParameters", ConnectionParametersDBServerBean.class, false, connectionId);
    }

    @Override
    public ConnectionParametersSapBoBean addBOConfig(String sourceName, String configName) {
        Integer instanceId = execNamedQuerySingleValAsInteger("addBOConfig", true, "id" /* i18n: no */, sourceName, configName);
        return getSapBoConnectionParametersForInstance(instanceId);
    }

    @Override
    public void deleteDBConfig(int configId, String sourceName, String serverName) {
//        boolean isMSSQL = sourceName.equals(PumpConstants.SOURCE_NAME_MSSQL);
//        if (isMSSQL) {
        ConnectionParametersDBServerBean bean = createBeanFromNamedQry("getDBParameters", ConnectionParametersDBServerBean.class, false, null, configId);
        deactivateDBServer(bean);
//        }

        execNamedCommand(
                "deleteDBConfig", "bik_db_server", configId, sourceName, serverName);
    }

    public void deleteBOConfig(int configId) {
        execNamedCommand("deleteBOConfig", configId);
    }

    public Integer getLastSourceStatus(String source) {
        return (Integer) execNamedQuerySingleVal("getLastSourceStatus", true, "status" /* i18n: no:column-name */, source);
    }

    public List<ConfluenceObjectBean> getConfluenceRootObjects() {
        return getConfluenceProxyConnector().getSpaces();
    }

    public List<ConfluenceObjectBean> getConfluenceChildrenObjects(String parentId, String parentKind) {
        return getConfluenceProxyConnector().getChildrenObjects(parentId, parentKind);
    }

    public List<ConfluenceObjectBean> getConfluenceFiltredObjects(final String val, String ticket) {

//        System.out.println("ww:conf: getConfluenceFiltredObjects starts for val=" + val + ", ticket=" + ticket);
        try {
            return runCancellableDBJob(ticket, new IContinuationWithReturn<List<ConfluenceObjectBean>>() {
                public List<ConfluenceObjectBean> doIt() {
                    return getConfluenceProxyConnector().search(val);
                }
            });
        } finally {
//            System.out.println("ww:conf: getConfluenceFiltredObjects ends for val=" + val + ", ticket=" + ticket);
        }
    }

    protected void updateConfluenceNodeForFTS(int nodeId, ConfluenceObjectBean cob) {
        updateNodeForFts(nodeId, cob.name, stripHtmlTags(cob.content));
    }

    protected List<List<ConfluenceObjectBean>> getContentForObjects(Map<String, String> objectsIds) {
        // convert to ConfluenceGroupBean
        List<ConfluenceGroupBean> selectedObjects = new ArrayList<ConfluenceGroupBean>();
        for (Entry<String, String> entry : objectsIds.entrySet()) {
            selectedObjects.add(new ConfluenceGroupBean(entry.getKey(), entry.getValue(), null));
        }
        final IConfluenceConnector confluenceProxyConnector = getConfluenceProxyConnector();
        // pobieranie obiektów z Confluecne
        return confluenceProxyConnector.getContentForObjects(selectedObjects);
    }

    @Override
    public void saveConfluenceObjects(Map<String, String> objectsIds, Integer parentId, int treeId) {
        List<List<ConfluenceObjectBean>> contentForObjects = getContentForObjects(objectsIds);
//        execNamedCommand("addConfluenceObjects", parentId, contentForObjects);
        // dodanie obiektów do BIKSa

        saveConfluenceObjects(parentId, treeId, contentForObjects);
    }

    protected IConfluenceConnector getConfluenceProxyConnector() {
        return getBOXIServiceImplDataForCurrentRequest().confluenceProxyConnector;
    }

    protected IBIAConnector getBIAProxyConnector() {
        return getBOXIServiceImplDataForCurrentRequest().biaProxyConnector;
    }

    @Override
    public void refreshConfluenceObject(int nodeId) {
        ConfluenceObjectBean bean = createBeanFromNamedQry("getConfluenceExtradata", ConfluenceObjectBean.class, true, nodeId);
        ConfluenceObjectBean beanAfterUpdate = getConfluenceProxyConnector().getContentForOneObject(bean);

        refreshConfluenceObject(nodeId, beanAfterUpdate);
    }

    //ww: zwraca listę id przodków - posortowaną od najstarszego czyli
// generalnie to co branch_ids, ale bez ostatniego id (własnego) = nodeId
    @Override
    public List<Integer> getBikNodeAncestorIds(int nodeId) {
        return execNamedQuerySingleColAsList("getBikNodeAncestorIds", "node_id", nodeId);
    }

    // zwraca w wyniku mapę: nodeId (z nodeIds) -> id-korzenia, id-pod-korzeniem, ..., id-rodzica-dla-nodeId
    // ważne, że wyniki są ustawione w kolejność (na liście) od korzenia do rodzica danego węzła nodeId
    // jeżeli węzeł nodeId (z kolekcji nodeIds) jest sam korzeniem, to dla niego nie będzie nic
    // w wynikowej mapie (tzn. res.get(rootNodeId) == null)
    @Override
    public Map<Integer, List<Integer>> getBikNodeAncestorIdsMulti(Collection<Integer> nodeIds) {
        Map<Integer, List<Integer>> res = new HashMap<Integer, List<Integer>>();

        if (BaseUtils.isCollectionEmpty(nodeIds)) {
            return res;
        }

        List<Map<String, Object>> rows = execNamedQueryCFN("getBikNodeAncestorIdsMulti",
                FieldNameConversion.ToLowerCase, nodeIds);

        for (Map<String, Object> row : rows) {
            int nodeId = sqlNumberInRowToInt(row, "id");
            int parentId = sqlNumberInRowToInt(row, "node_id");

            List<Integer> parentIds = res.get(nodeId);
            if (parentIds == null) {
                parentIds = new ArrayList<Integer>();
                res.put(nodeId, parentIds);
            }
            parentIds.add(parentId);
        }

        return res;
    }

    @Override
    public List<TreeNodeBean> getSubtreeLevelsByParentIds(Collection<Integer> parentNodeIds,
            BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, String optRelationType, String optAttributeName, Integer nodeKindId) {
        setOptFilteringNodeActionCodeForCurrentRequest(optFilteringNodeActionCode);

        List<TreeNodeBean> res = createBeansFromNamedQry("getSubtreeLevelsByParentIds",
                TreeNodeBean.class, parentNodeIds, getConditionPartForBikNodeTreeOptMode(optExtraMode), optRelationType, optAttributeName, nodeKindId);
        translateSpecialTreeNodeBeans(res,
                false);
        return res;
    }

    protected List<TreeNodeBean> getBikFilteredNodesInner(final String val, final Integer optSubTreeRootNodeId,
            final Object treeIdOrIds, final boolean noLinkedNodes, final BikNodeTreeOptMode optExtraMode, String ticket, final Integer optNodeKindId, final String optRelationType, final String optAttributeName) {

//        addAccessedTreeInCurrentSession();
        return runCancellableDBJob(ticket, new IContinuationWithReturn<List<TreeNodeBean>>() {
            @Override
            @SuppressWarnings("unchecked")
            public List<TreeNodeBean> doIt() {
//                List<TreeNodeBean> tnbs = createBeansFromNamedQry("getBikFilteredNodes", TreeNodeBean.class,
//                        val, optSubTreeRootNodeId, treeIdOrIds, noLinkedNodes,
//                        getConditionPartForBikNodeTreeOptMode(optExtraMode));

                List<Integer> treeIds = new ArrayList<Integer>();
                if (optSubTreeRootNodeId != null) {
                    treeIds.add((Integer) execNamedQuerySingleVal("getTreeIdByNodeId", false, null, optSubTreeRootNodeId));
                } else if (treeIdOrIds instanceof Collection) {
                    treeIds.addAll((Collection) treeIdOrIds);
                } else {
                    treeIds.add((Integer) treeIdOrIds);
                }

                List<Document> docs = searchService.searchInTreeByNodeName(val, treeIds);
                Set<Integer> nodeIds = new HashSet<Integer>();
                for (Document doc : docs) {
                    nodeIds.add(BaseUtils.tryParseInteger(doc.get(BiksSearchConstant.DOC_ID)));
                }
                if (!BaseUtils.isCollectionEmpty(nodeIds)) {
                    nodeIds = execNamedQuerySingleColAsSet("filterNodeIdsByAdditionalConditions", null, nodeIds, noLinkedNodes ? "linked_node_id is null" : null, getConditionPartForBikNodeTreeOptMode(optExtraMode));
                }

                if (!BaseUtils.isCollectionEmpty(nodeIds)) {
                    List<String> branchIds = execNamedQuerySingleColAsList("getBranchIdsToRoot", null, nodeIds, optSubTreeRootNodeId);
                    nodeIds.clear();
                    for (String branchId : branchIds) {
                        List<String> ids = BaseUtils.splitBySep(branchId, "|");
                        for (String id : ids) {
                            Integer nodeId = BaseUtils.tryParseInteger(id);
                            if (nodeId != null) {
                                nodeIds.add(nodeId);
                            }
                        }
                    }
                }
                List<TreeNodeBean> tnbs = new ArrayList<TreeNodeBean>();

                if (!BaseUtils.isCollectionEmpty(nodeIds)) {
                    tnbs = createBeansFromNamedQry("getBikFilteredNodesAfterSearchByLucene", TreeNodeBean.class, nodeIds, optNodeKindId, optRelationType, optAttributeName);
                }

                List<TreeNodeBean> res = new ArrayList<TreeNodeBean>();
                for (TreeNodeBean pn : tnbs) {
                    boolean add2Res = false;
                    if (!pn.hasProperGrantForNodeFilteringAction) {
                        for (TreeNodeBean cn : tnbs) {
                            if (cn.branchIds.startsWith(pn.branchIds) && cn.hasProperGrantForNodeFilteringAction) {
                                add2Res = true;
                                break;
                            }
                        }
                    } else {
                        add2Res = true;
                    }
                    if (add2Res) {
                        res.add(pn);
                    }
                }

                translateSpecialTreeNodeBeans(res, false);
                return res;
            }
        });
    }

    @Override
    public List<TreeNodeBean> getBikFilteredNodes(int treeId, String val, final Integer optSubTreeRootNodeId,
            BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, String ticket, Integer optNodeKindId, String optRelationType, String optAttributeName) {

        addAccessedTreeInCurrentSession(treeId);

        setOptFilteringNodeActionCodeForCurrentRequest(optFilteringNodeActionCode);

        List<TreeNodeBean> res;

        if (BaseUtils.safeEquals(val, "")) {
            res = createBeansFromNamedQry("getOneSubtreeLevelOfTree", TreeNodeBean.class, treeId, null, false,
                    getConditionPartForBikNodeTreeOptMode(optExtraMode), true, null, null, null, null);
//
//             List<TreeNodeBean> res = createBeansFromNamedQry("getOneSubtreeLevelOfTree", TreeNodeBean.class,
//                treeId, parentNodeId, noLinkedNodes, getConditionPartForBikNodeTreeOptMode(optExtraMode), calcHasNoChildren, nodeKindId, relationType, attributeName, outerTreeId);

            translateSpecialTreeNodeBeans(res,
                    false);
        } else {
            res
                    = //createBeansFromNamedQry("getBikFilteredNodes", TreeNodeBean.class, val, treeId, false);
                    getBikFilteredNodesInner(val, optSubTreeRootNodeId, treeId, false, optExtraMode, ticket, optNodeKindId, optRelationType, optAttributeName);
        }
        return res;
    }

    @Override
    public List<SystemUserGroupBean> getUserGroupFilteredNodes(final String val, String ticket) {
        return runCancellableDBJob(ticket, new IContinuationWithReturn<List<SystemUserGroupBean>>() {
            @Override
            public List<SystemUserGroupBean> doIt() {
                return createBeansFromNamedQry("getUserGroupFilteredNodes", SystemUserGroupBean.class, val);
            }
        });
    }

    @Override
    public List<TreeNodeBean> getBikFilteredNodesWithCondidtion(String val, final Integer optSubTreeRootNodeId,
            Collection<Integer> treeIds, boolean noLinkedNodes, BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, String ticket, Integer optNodeKindId, String optRelationType, String optAttributeName) {
        setOptFilteringNodeActionCodeForCurrentRequest(optFilteringNodeActionCode);

        return //createBeansFromNamedQry("getBikFilteredNodes", TreeNodeBean.class, val, treeIds, noLinkedNodes);
                getBikFilteredNodesInner(val, optSubTreeRootNodeId, treeIds, noLinkedNodes,
                        optExtraMode, ticket, optNodeKindId, optRelationType, optAttributeName);
//                List<Document> docs = searchService.searchInTreeByNodeName(val, optSubTreeRootNodeId,  );
//                List<Integer> nodeIds = new ArrayList<Integer>();
//                for (Document doc : docs) {
//                    nodeIds.add(BaseUtils.tryParseInteger(doc.get(BiksSearchConstant.DOC_ID)));
//                }
//                nodeIds = execNamedQuerySingleColAsList("getNodeIdsToRoot", null, nodeIds);
//                List<TreeNodeBean> tnbs = createBeansFromNamedQry("getBikFilteredNodesAfterSearchByLucene", null, nodeIds);

    }

    protected void translateSpecialTreeNodeBeans(List<TreeNodeBean> list, boolean addToAccessedTrees) {

        //ww: to tylko do testów
//        FoxyGWTServiceServlet.sleepInRequestForThreadsSuspiciousBehaviour(5);
        Set<Integer> treeIds = null;

        if (addToAccessedTrees) {
            treeIds = new HashSet<Integer>();
        }

        for (TreeNodeBean tnb : list) {

            if (addToAccessedTrees) {
                treeIds.add(tnb.treeId);
            }

            Object translationObject = BIKConstants.SPECIAL_NODE_KIND_TRANSLATIONS.get(tnb.nodeKindCode);
            if (translationObject == null) {
                continue;
            }
            if (translationObject instanceof I18nMessage) {
                tnb.name = ((I18nMessage) translationObject).get();
            } else if (translationObject instanceof Map) {
                @SuppressWarnings("unchecked")
                I18nMessage msg = ((Map<String, I18nMessage>) translationObject).get(tnb.name);
                if (msg != null) {
                    tnb.name = msg.get();
                }
            }
        }

        if (addToAccessedTrees) {
            for (Integer treeId : treeIds) {
                addAccessedTreeInCurrentSession(treeId);
            }
        }
    }

    public List<SapBoUniverseColumnBean> getAllAboutTable(int tableId) {
        return createBeansFromNamedQry("getAllAboutTable", SapBoUniverseColumnBean.class, tableId);
    }

    public List<AppPropBean> getNonSensitiveAppProps() {
        //return createBeansFromNamedQry("getNonSensitiveAppProps", AppPropBean.class);
        loadAppPropsToCacheIfNeeded();
        return getBOXIServiceImplDataForCurrentRequest().nonSensitiveAppPropsCache;
    }

    protected void clearCaches() {
        //ww: gdy będziemy więcej rzeczy cache'ować, to tutaj wyczyścimy je
        // wszystkie na raz (jeżeli to będzie potrzebne), czyli
        // każda cache'owana rzecz ma mieć swoją oddzielną metodę clearXXXCache
        // a tutaj wołamy po kolei te wszystkie metody
        clearAppPropsCache();
    }

    public List<AppPropBean> setEditableAppProps(Map<String, String> modifiedOrNewProps) {
        //for (Entry<String, String> e : modifiedOrNewProps.entrySet()) {
        execNamedCommand("setEditableAppProps", modifiedOrNewProps);
        //}
        //clearCaches();
        clearAppPropsCache();

        return getNonSensitiveAppProps();
    }

    public LinkedHashMap<String, Integer> getBikAllDefAndVersion(int treeId) {
        List<Map<String, Object>> keywordRows = execNamedQueryCFN(
                "getBikAllDefAndVersion", FieldNameConversion.ToJavaPropName, treeId);
        LinkedHashMap<String, Integer> hashMap = new LinkedHashMap<String, Integer>();
        for (Map<String, Object> k : keywordRows) {
            hashMap.put((String) k.get("name" /* I18N: no */).toString().toLowerCase(), (sqlNumberToInt(k.get("official" /* I18N: no */))));
        }
        return hashMap;
    }

    public List<AttributeAndTheirKindsBean> getBikAttributeAndTheirKinds() {
        return createBeansFromNamedQry("getBikAttributeAndTheirKinds", AttributeAndTheirKindsBean.class
        );
    }

    public List<NoteBean> getNotesByUser() {
        return createBeansFromNamedQry("bikSystemUserEntityNotes", NoteBean.class, getLoggedUserId()/*
         * getLoggedUserId()
         */);
    }

    public void addNewRole(String caption, int attrCategoryId) {
        execNamedCommand("addNewRole", caption, attrCategoryId);
    }

    public void updateRole(int id, String caption, int attrCategoryId) {
        execNamedCommand("updateRole", id, caption, attrCategoryId);
    }

    public void deleteRole(int id) {
        execNamedCommand("deleteRole", id);
    }

    public void deleteCategory(int id) {
//        List<Map<String, Object>> roles = execNamedQueryCFN("getRolesInCategory", FieldNameConversion.ToJavaPropName, id);
//        for (Map<String, Object> row : roles) {
//            int roleId = sqlNumberInRowToInt(row, "id");
//            deleteRole(roleId);
//        }
//        List<Map<String, Object>> attrs = execNamedQueryCFN("getAttributesInCategory", FieldNameConversion.ToJavaPropName, id);
//        for (Map<String, Object> row : attrs) {
//            int attrId = sqlNumberInRowToInt(row, "id" /* I18N: no */);
//            deleteAttributeDict(attrId);
//        }
        execNamedCommand("deleteCategory", id);
//        updateAddAttrsForSearch();
    }

    private String cleanHtml(String html) {
        SystemUserBean sub = getLoggedUserBean();
        if (html == null || html.isEmpty() || sub.doNotFilterHtml || sub.userRightRoleCodes.contains("Administrator")) {
            return html;
        } else {
            String x = Jsoup.clean(html, "http://localhost", myWhiteList);
            org.jsoup.nodes.Document d = Jsoup.parse(x);
            Elements imgs = d.select("img[src]");
            if (imgs != null) {
                for (Element img : imgs) {
                    if (!img.attr("src").startsWith("fsb?")) {
                        img.removeAttr("src");
                    }
                }
            }
            return d.body().html();
        }
    }

    public void updateDescription(NameDescBean nameDesc, TreeNodeBean node) {
        String safeSubject = stripHtmlTags(cleanHtml(nameDesc.subject));
        String safeContent = cleanHtml(nameDesc.content);
        String strippedContent = stripHtmlTags(safeContent);
        execNamedCommand("updateDescription", safeContent, node.id);
        updateNodeForFts(node.id, null, strippedContent);
        TreeNodeBean fakeNodeBean = new TreeNodeBean(node.id, node.parentNodeId, node.nodeKindId, safeContent, node.nodeKindCaption, node.nodeKindCode, safeSubject, node.objId, node.treeId, node.linkedNodeId, node.branchIds, node.treeCode, node.treeName, node.treeKind);
        updateTreeNode(fakeNodeBean);
    }

    protected void stripHtmlTagsFromRichTextFieldsOfOneTable(String tableName,
            String richTextFieldName, String nodeIdFieldName, String attrName) {
        List<Map<String, Object>> rows = execNamedQueryCFN("getRichTextValuesOfOneTable",
                FieldNameConversion.ToLowerCase, tableName,
                richTextFieldName, nodeIdFieldName, attrName);

        for (Map<String, Object> row : rows) {
            int nodeId = sqlNumberInRowToInt(row, "node_id");
            String value = (String) row.get("value" /* I18N: no */);

            updateNodeForFts(nodeId, null, stripHtmlTags(cleanHtml(value)));
//            System.out.println("stripHtmlTagsFromRichTextFieldsOfOneTable");
//            execNamedCommand("insertOrUpdateAttrVal", nodeId, stripHtmlTags(value),
//                    "descr");
        }
    }

    public void updateDescriptionForMetadata(NameDescBean nameDesc, TreeNodeBean node) {
        String safeSubject = stripHtmlTags(cleanHtml(nameDesc.subject));
        String safeContent = cleanHtml(nameDesc.content);
        String strippedContent = stripHtmlTags(safeContent);
        execNamedCommand("updateDescriptionForMetadata", safeContent, node.id);
        updateNodeForFts(node.id, null, stripHtmlTags(safeContent));
        TreeNodeBean fakeNodeBean = new TreeNodeBean(node.id, node.parentNodeId, node.nodeKindId, safeContent, node.nodeKindCaption, node.nodeKindCode, safeSubject, node.objId, node.treeId, node.linkedNodeId, node.branchIds, node.treeCode, node.treeName, node.treeKind);
        updateTreeNode(fakeNodeBean);
    }

    public Pair<Integer, Boolean> addLinkUser(int linkedId, String name, int parentNodeId, int treeId) {
//        Integer treeId = execNamedQuerySingleVal("getTreeId", false, "id", BIKConstants.TREE_CODE_USER_ROLES);
//        execNamedCommand("addLinkedUser", linkedId, name, parentNodeId, BIKConstants.NODE_KIND_USER, treeId);
//        execNamedCommand("node_init_branch_id", treeId); // odświeżam dla całego drzewa, można dla 1 węzła ale drzewko jest bardzo małe więc to nie problem
        List<Map<String, Object>> userIdAndIsNew = execNamedQueryCFN("addLinkedUser",
                FieldNameConversion.ToLowerCase, linkedId, name, parentNodeId, BIKConstants.NODE_KIND_USER, treeId);
        Pair<Integer, Boolean> userIdIsNew = null;// = new Pair<Integer, Boolean>();
        for (Map<String, Object> idAndNameRow : userIdAndIsNew) {
            int id = (Integer) idAndNameRow.get("id" /* I18N: no */);
            boolean isNew = (Integer) idAndNameRow.get("is_new") > 0;
            userIdIsNew = new Pair<Integer, Boolean>(id, isNew);
        }
        return userIdIsNew;
    }

    public FrequentlyAskedQuestionsBean
            getFrequentlyAskedQuestions(int nodeId) {
        return createBeanFromNamedQry("getFrequentlyAskedQuestions", FrequentlyAskedQuestionsBean.class, true, nodeId);
    }

    public void verticalizeNodeAttrs() {
        execInAutoCommitMode(new IContinuation() {
            public void doIt() {
//                addOrUpdateSpidHistoryForCurrentUser();
                execNamedCommand("verticalizeNodeAttrs");
            }
        });
    }

    public List<AttrHintBean> getAttributes() {
        return createBeansFromNamedQry("getAttributes", AttrHintBean.class
        );
    }

    public void updateAttrHint(String name, String hint, int isBuiltIn) {
        execNamedCommand("updateAttrHint", name, hint == null ? "" : hint, isBuiltIn);
    }

    public void changeInheritance(int idBikJoinedObj, int inherit) {
        execNamedCommand("changeInheritance", idBikJoinedObj, inherit);
    }

    public int getCountOfLinkedElements(int idAtr) {
        return (Integer) execNamedQuerySingleVal("getCountOfLinkedElements", true, "count" /* I18N: no */, idAtr);
    }

    public Set<Integer> getAlreadyJoinedUserIds() {
        return execNamedQuerySingleColAsSet("getAlreadyJoinedUserIds", null);
    }

    public List<String> getAncestorNodePath(String branchIds) {
        //ww: uwaga! to nie jest dobre podejście, bo id jest int-em, a tu mamy listę stringów!
        // patrz niżej...
        //List<String> nodeIds = BaseUtils.splitBySep(branchIds, "|");/*BIKCenterUtils.extractNodeIdsFromHtml(body);*/
        List<Integer> nodeIds = BIKCenterUtils.splitBranchIdsEx(branchIds, false);
        List<String> ancestorNamePath = new ArrayList<String>();
        if (!nodeIds.isEmpty()) {
            //ww: o - tu przekazywana była lista stringów i potem wewnątrz
            // getAncestorNodePath był id in (lista-stringów) co potencjalnie
            // może być wolne! bo może nie iść po indeksie (konwersja varchar na int
            // albo odwrotnie).
            ancestorNamePath = execNamedQuerySingleColAsList("getNodeNamesByIds", "name" /* I18N: no */, nodeIds);
        }
        return ancestorNamePath;
    }

    @Override
    public List<TreeNodeBean> getBikUserTreeNodes(Integer optUpSelectedNodeId) {
        List<TreeNodeBean> tnb = createBeansFromNamedQry("getBikUserTreeNodes", TreeNodeBean.class, optUpSelectedNodeId);
        translateSpecialTreeNodeBeans(tnb,
                true);
        return tnb;
    }

    @Override
    public List<TreeNodeBean> getNotConnectedUsers() {
        setOptFilteringNodeActionCodeForCurrentRequestAddJoinedObjs();
        List<TreeNodeBean> tnb = createBeansFromNamedQry("getNotConnectedUsers", TreeNodeBean.class
        );
        translateSpecialTreeNodeBeans(tnb,
                false);
        return tnb;
    }

    @Override
    public List<TreeNodeBean> getBikUserRootTreeNodes(Integer optUpSelectedNodeId) {
        setOptFilteringNodeActionCodeForCurrentRequestAddJoinedObjs();

        List<TreeNodeBean> tnb = createBeansFromNamedQry("getBikUserRootTreeNodes", TreeNodeBean.class, optUpSelectedNodeId);
        translateSpecialTreeNodeBeans(tnb,
                true);
        return tnb;
    }

    public List<AttributeBean> getAttrDefAndCategory() {
        return createBeansFromNamedQry("getAttrDefAndCategory", AttributeBean.class);
    }

    public List<AttributeBean> getAttrDefBuiltAndCategory() {
        return createBeansFromNamedQry("getAttrDefBuiltAndCategory", AttributeBean.class
        );
    }

    public Map<Integer, List<Integer>> getAttributeDict() {
        List<AttributeBean> abs = createBeansFromNamedQry("getAttributeDict", AttributeBean.class
        );
        Map<Integer, List<Integer>> nkad = new HashMap<Integer, List<Integer>>();

        for (AttributeBean ab : abs) {
            List<Integer> l = nkad.get(ab.nodeKindId);
            if (l == null) {
                l = new ArrayList<Integer>();
                nkad.put(ab.nodeKindId, l);
            }
            l.add(ab.attrDefId);
        }

        return nkad;
    }

    /*
     * method_name varchar(1000) not null, parameters varchar(max) not null,
     * start_time datetime not null, duration_millis int not null default -1,
     * ip_addr varchar(1000) not null, session_id varchar(1000) not null,
     * exception_msg
     */
    public int insertServiceRequest(String methodName, String parameters, String ipAddr,
            String sessionId, String optExtraInfo) {
        int res = sqlNumberToInt(execNamedQuerySingleVal("insertServiceRequest", false, null,
                methodName, parameters, ipAddr, sessionId, optExtraInfo));
        finishWorkUnit(true);
        return res;
    }

    public void updateServiceRequest(int reqId, String optExtraInfo, String optExceptionMsg) {
        execNamedCommand("updateServiceRequest", reqId, optExtraInfo, optExceptionMsg);
        finishWorkUnit(true);
    }

    public boolean isServletRequestLoggingEnabled() {
        return !isInMultiXModeAndNoSingleBiksDbDetermined()
                && BaseUtils.tryParseInt(getAppPropValueEx("serviceRequestLogLevel", true, "0")) > 0;
    }

    public List<RoleForNodeBean> getRoleForNode() {
        return createBeansFromNamedQry("getRoleForNode", RoleForNodeBean.class);
    }

    public List<String> getAncestorNodePathByNodeId(int nodeId) {
        return execNamedQuerySingleColAsList("getAncestorNodePathByNodeId", "name" /* I18N: no */, nodeId);
    }

    public void updateItemInCategory(int itemId, int categoryId, String name, String attrType, String valueOpt, boolean displayAsNumber, boolean usedInDrools) {
        String safeName = stripHtmlTags(cleanHtml(name));
        String safeAttrType = cleanHtml(attrType);
        String safeValueOpt = cleanHtml(valueOpt);
        execNamedCommand("updateItemInCategoryEx", itemId, categoryId, safeName, safeAttrType, safeValueOpt, displayAsNumber, usedInDrools);
    }

    public void updateJoinedObjsForNode(int nodeId, boolean noLinkedNodes, Map<Integer, JoinedObjMode> directOverride, Map<Integer, Pair<JoinedObjMode, Boolean>> subNodesOverride) {
//        for (Entry<Integer, JoinedObjMode> e : directOverride.entrySet()) {
//            execNamedCommand("insertJoinedObjMode", nodeId, e.getKey(), e.getValue().ordinal(), 0);
//        }
//        for (Entry<Integer, Pair<JoinedObjMode, Boolean>> e : subNodesOverride.entrySet()) {
//            Integer mode = e.getValue().v1 != null ? e.getValue().v1.ordinal() : null;
//            execNamedCommand("insertJoinedObjMode", nodeId, e.getKey(), mode, e.getValue().v2 ? 2 : 1);
//        }

//        String sqlTxt = getAdhocDao().provideNamedSqlText("updateJoinedObjsForNode", nodeId, directOverride, subNodesOverride);
////        System.out.println("updateJoinedObjsForNode: sqlTxt:\n" + sqlTxt);
//        if (logger.isInfoEnabled()) {
//            logger.info("updateJoinedObjsForNode: sqlTxt:\n" + sqlTxt);
//        }
        execNamedCommand("updateJoinedObjsForNode", nodeId, noLinkedNodes, directOverride, subNodesOverride);
    }

    public Integer addOrUpdateCategory(Integer id, String name) {
        String safeName = stripHtmlTags(cleanHtml(name));
        if (id == null) {
            return execNamedQuerySingleValAsInteger("addCategory", false, null, safeName);
        } else {
            return execNamedQuerySingleValAsInteger("updateCategory", false, null, id, safeName);
        }
    }

    public Integer addAttributeDict(String itemName, int categoryId, String typeAttr, String attrValueList, boolean displayAsNumber, boolean usedInDrools) {
        String safeItemName = stripHtmlTags(cleanHtml(itemName));
        String safeTypeAttr = stripHtmlTags(cleanHtml(typeAttr));
        String safeAttrValueList = stripHtmlTags(cleanHtml(attrValueList));
        return execNamedQuerySingleValAsInteger("addAttributeDictEx", false, null, safeItemName, categoryId, safeTypeAttr, safeAttrValueList, displayAsNumber, usedInDrools);
    }

    protected void updateAddAttrsForSearch() {
//        addOrUpdateSpidHistoryForCurrentUser();
//        execNamedCommand("updateAddAttrsForSearch");
    }

    public void addOrDeleteAttributeDictForKind(Map<Integer, Integer> atrIdAndCnt, Set<Integer> selectedNodeKindIds) {
        for (Entry<Integer, Integer> atr : atrIdAndCnt.entrySet()) {
            if (atr.getValue() == selectedNodeKindIds.size()) {
                execNamedCommand("addAttributeDictForKind", atr.getKey(), selectedNodeKindIds);
            } else if (atr.getValue() == 0) {
                execNamedCommand("deleteAttributeDictForKind", atr.getKey(), selectedNodeKindIds);
            }
        }

//        updateAddAttrsForSearch();
    }

    public void addOrDeleteAttributeTypeForKind(Integer attrTypeId, Set<Integer> nodeKindIds, Boolean doDelete) {
        if (!doDelete) {
            execNamedCommand("addAttributeDictForKind", attrTypeId, nodeKindIds);
        } else {
            execNamedCommand("deleteAttributeDictForKind", attrTypeId, nodeKindIds);
        }
    }

    public void modifySystemAttrVisibleForKind(Map<Integer, Integer> atrIdAndCnt, Set<Integer> selectedNodeKindIds) {
        for (Entry<Integer, Integer> atr : atrIdAndCnt.entrySet()) {
            if (atr.getValue() != 2) { // 2 = checkbox przerywany. Dla niego nie zmieniamy nic
                execNamedCommand("updateSystemAttributeVisibleForNodeKind", selectedNodeKindIds, atr.getKey(), atr.getValue());
            }
        }
    }

    @Override
    public void deleteAttributeDict(int atrId) {
        execNamedCommand("deleteAttributeDict", atrId);

//        updateAddAttrsForSearch();
    }

    public <T extends BeanWithIntIdBase> Map<Integer, T> getNodeDataByIds(String namedSqlName, Class<T> cls, Collection<Integer> nodeIds) {

        if (BaseUtils.isCollectionEmpty(nodeIds)) {
            return new HashMap<Integer, T>();
        }

        List<T> beans = createBeansFromNamedQry(namedSqlName, cls, nodeIds);
        return BaseUtils.makeBeanMap(beans);
    }

    @Override
    public Map<Integer, String> getNodeNamesByIds(Collection<Integer> nodeIds) {

        Map<Integer, String> res = new LinkedHashMap<Integer, String>();

        if (BaseUtils.isCollectionEmpty(nodeIds)) {
            return res;
        }

        List<Map<String, Object>> idAndNameRows = execNamedQueryCFN("getNodeNamesByIds",
                FieldNameConversion.ToLowerCase, nodeIds);

        for (Map<String, Object> idAndNameRow : idAndNameRows) {
            int id = (Integer) idAndNameRow.get("id" /* I18N: no */);
            String name = (String) idAndNameRow.get("name" /* I18N: no */);
            res.put(id, name);
        }

        return res;
    }

    @Override
    public IndexCrawlStatus getFullTextCrawlCompleteStatus(boolean useLucene) {
        final BOXIServiceImplData boxiServiceImplDataForCurrentRequest = getBOXIServiceImplDataForCurrentRequest();

        if (!useLucene) {
            useLucene = boxiServiceImplDataForCurrentRequest.useLuceneSearch;
        } else {
            useLucene = boxiServiceImplDataForCurrentRequest.similarNodesWizard != null;
        }

        if (useLucene) {

            if (!boxiServiceImplDataForCurrentRequest.similarNodesWizard.hasIndex()) {
                return IndexCrawlStatus.NoIndex;
            }

            IndexCrawlStatus res = boxiServiceImplDataForCurrentRequest.similarNodesWizard.isIndexingInProgress()
                    ? IndexCrawlStatus.CrawlInProgress : IndexCrawlStatus.CrawlComplete;
            return res;
        }

        Map<String, Object> row = execNamedQuerySingleRowCFN("getFullTextCrawlCompleteStatus", true,
                FieldNameConversion.ToLowerCase);

        if (row == null) {
            return IndexCrawlStatus.NoIndex;
        }

        boolean isCrawlDone = (Boolean) row.get("is_crawl_done");
        int isChangeTrackingEnabled = (Integer) row.get("is_change_tracking_enabled");

        if (isChangeTrackingEnabled == 0) {
            return IndexCrawlStatus.NoChangeTracking;
        }

        return isCrawlDone ? IndexCrawlStatus.CrawlComplete : IndexCrawlStatus.CrawlInProgress;
    }

    @Override
    public boolean isDBJobCanceled() {
        return super.isDBJobCanceled();
    }

    public List<VoteBean> getVotesByUser() {
        return createBeansFromNamedQry("getVoteForUser", VoteBean.class, getLoggedUserId());
    }

    public boolean isOldPasswordTrue(String password, int id) {
        Object flagObj = execNamedQuerySingleVal("isOldPasswordTrue", false, "flag" /* I18N: no */, password, id);
        return ((Integer) flagObj) != 0;
    }

    public void setNodeVisualOrder(int nodeId, int visualOrder) {
        execNamedCommand("setNodeVisualOrder", nodeId, visualOrder);
        execNamedCommand("updateMenuOrderIfNecessary", nodeId, null);
//        registerChangeInNode(nodeId);
    }

    public boolean updateUserRolesForNode(int nodeId, boolean noLinkedNodes, Map<Integer, JoinedObjMode> directOverride, Map<Integer, Pair<JoinedObjMode, Boolean>> subNodesOverride, int roleId, Set<String> treeKinds, boolean isAuxiliary, JoinTypeRoles joinTypeRole, Collection<Integer> alreadyJoinedNode) {
        execNamedCommand("updateUserRolesForNode", nodeId, noLinkedNodes, directOverride, subNodesOverride, roleId, getDynTreeKindsWhenTreeKindIsDynamic(treeKinds), isAuxiliary, joinTypeRole.ordinal(), alreadyJoinedNode);
        boolean isAddLinkedUserUnderRoles = addLinkedUserUnderRoles();
        boolean isDeleteBadLinkedUsersUnderRole = deleteBadLinkedUsersUnderRole();
        return isAddLinkedUserUnderRoles || isDeleteBadLinkedUsersUnderRole;

    }

    public void resetVisualOrderForNodes(Integer parentId, int treeId) {
        execNamedCommand("resetVisualOrderForNodes", parentId, treeId);
        execNamedCommand("updateMenuOrderIfNecessary", parentId, treeId);
        registerChangeInTree(treeId);
    }

    @PerformOnSingleBiksDb
    protected void addCurrentUserToLoggedUsers() {
//        String loggedUserName = getLoggedUserName();
//
//        if (loggedUserName != null) {
//            getBOXIServiceImplDataForCurrentRequest().loggedUsers.put(loggedUserName, new Date());
//        }
        //@MethodNeedsFurtherInvestigation
        addUserNameToLoggedUsers(getLoggedUserName());
    }

//    private void dummyDump(String txt) {
//        System.out.println(txt);
//    }
    @Override
    @PerformOnAnyDb
    @NotRequireUserLoggedIn
    public String keepSessionAlive(String dummy) {
        final String optSingleBiksDb = getOptSingleBiksDbForCurrentRequest();

//        if (getOptSingleBiksDbForCurrentRequest() == null) {
//            return BIKConstants.APP_VERSION;
//        }
        boolean notLoggedIn = optSingleBiksDb == null;
        //            dummyDump(dummy);
        final long currentTimeMillis = System.currentTimeMillis();

        Long lastActivityInServerTimeMillis = getSessionData().lastActivityInServerTimeMillis;

//            dummyDump("session last act=" + lastActivityInServerTimeMillis);
        if (BaseUtils.strHasPrefix(dummy, ":", false)) {
            Long currentActivityPassedMillis = BaseUtils.tryParseLong(BaseUtils.dropPrefixOrGiveNull(dummy, ":"), null);

//                dummyDump("currentActivityPassedMillis=" + currentActivityPassedMillis);
            if (currentActivityPassedMillis != null) {

                long inServerTimeMillis = currentTimeMillis - currentActivityPassedMillis;

                if (lastActivityInServerTimeMillis == null || lastActivityInServerTimeMillis < inServerTimeMillis) {
                    lastActivityInServerTimeMillis = inServerTimeMillis;
                    getSessionData().lastActivityInServerTimeMillis = lastActivityInServerTimeMillis;
                }
            }
        }

        if (optSingleBiksDb != null) {
//            dummyDump("session last act#2=" + lastActivityInServerTimeMillis);
            int autoLogoutAfterMins = BaseUtils.tryParseInt(getAppPropValueEx("autoLogoutAfterMins", true));

//            dummyDump("autoLogoutAfterMins=" + autoLogoutAfterMins);
            if (autoLogoutAfterMins > 0 && lastActivityInServerTimeMillis != null) {

                int passedMins = (int) ((currentTimeMillis - lastActivityInServerTimeMillis) / 1000 / 60);
//                dummyDump("passedMins=" + passedMins);
                if (passedMins >= autoLogoutAfterMins) {
                    notLoggedIn = true;
                    performUserLogout();
//                    dummyDump("performUserLogout() done!");
                }
            }
        }

//        maybeSetModifiedTreesInfoInResponse();
//        System.out.println("keepSessionAlive: instance=" + getFoxyAppInstanceId() + ", dummy=" + dummy);
//        String clientUserName = dummy; //BaseUtils.getLastItems(dummy, BIKConstants.KEEP_SESSION_ALIVE_REQUEST_PARAM_SEP, 1);
        if (!notLoggedIn) {
            LoggedUserOnClientVsServerStatus luocvss = calculateLuocvss();

            if (luocvss != LoggedUserOnClientVsServerStatus.DisabledUser) {
                // wyszło, że jest zalogowany i nie jest wyłączony - sprawdźmy,
                // czy wiemy, na jakiej bazie SingleBiks działa
                if (optSingleBiksDb != null) {
                    addCurrentUserToLoggedUsers();
                }
            }
        }

        return BIKConstants.APP_VERSION;
//        return BIKCenterUtils.encodeStatusInVersionAfterLastDotForKeepSessionAlive(BIKConstants.APP_VERSION, luocvss);
    }

    protected BaseUtils.MapProjector<String, String> getCodeMapProj = new BaseUtils.MapProjector<String, String>("code" /* i18n: no */);
    protected BaseUtils.MapProjector<String, String> getTxtMapProj = new BaseUtils.MapProjector<String, String>("txt" /* i18n: no */);

    //ww: wynikiem jest mapa z kindów na mapy tłumaczeń
//    protected Map<String, Map<String, String>> getTranslationsGeneric(String... kinds) {
//        //ww: bez tego pyszczy NetBeans - wołanie z varargs podwójne by było - a tak - to ma być parametr pojedyńczy!
//        Object kindsInner = kinds;
//        List<Map<String, Object>> rows = execNamedQueryCFN("getTranslationsGeneric",
//                FieldNameConversion.ToJavaPropName, kindsInner);
//
//        Map<String, Map<String, String>> res = //new HashMap<String, Map<String, String>>();
//                BaseUtils.makeHashMapOfSize(kinds.length);
//
//        //ww: zrobimy puste mapy wewnętrzne, żeby potem nie było zakoczenia nullem
//        // gdy brak wartości dla kindu
//        for (String kind : kinds) {
//            Map<String, String> mapOfKind = new HashMap<String, String>();
//            res.put(kind, mapOfKind);
//        }
//
//        for (Map<String, Object> row : rows) {
//            String kind = (String) row.get("kind" /* i18n: no */);
//            String code = (String) row.get("code" /* i18n: no */);
//            String txt = (String) row.get("txt" /* i18n: no */);
//
//            //ww: taka mapa jest, bo ją powyżej zakładamy w oddzielnej pętli
//            res.get(kind).put(code, txt);
//        }
//
//        return res;
//    }
    private Map<Integer, List<AttributeBean>> getAllAttributesForNodeKindId() {
        List<AttributeBean> result = createBeansFromNamedQry("getAllAttributesForNodeKindId", AttributeBean.class
        );
        Map<Integer, List<AttributeBean>> map = new HashMap<Integer, List<AttributeBean>>();
        for (AttributeBean ab : result) {
            if (map.containsKey(ab.nodeKindId)) {
                map.get(ab.nodeKindId).add(ab);
            } else {
                List<AttributeBean> list = new ArrayList<AttributeBean>();
                list.add(ab);
                map.put(ab.nodeKindId, list);
            }
        }
        return map;
    }

    private Map<Integer, List<AttributeBean>> getSystemAttributes() {
        List<AttributeBean> result = createBeansFromNamedQry("getSystemAttributes", AttributeBean.class
        );
        Map<Integer, List<AttributeBean>> map = new HashMap<Integer, List<AttributeBean>>();
        for (AttributeBean ab : result) {
            if (map.containsKey(ab.nodeKindId)) {
                map.get(ab.nodeKindId).add(ab);
            } else {
                List<AttributeBean> list = new ArrayList<AttributeBean>();
                list.add(ab);
                map.put(ab.nodeKindId, list);
            }
        }
        return map;
    }

    //ww->startupdata: a dlaczego oddzielne zapytanie w adhoc do tego,
// nie można użyć np. getAppPropValue ???
    public Pair<String, String> getAppVersion() {
        //ww: zostawione dla kompatybilności biksa ze starszą wersją w przeglądarce
        // aby pokazało się info o złej wersji
        return versionInfoService.getAppVersion();
//        String version = execNamedQuerySingleVal("getVersionAppProps", true, I18n.val.get() /* I18N:  */);
//        return new Pair<String, String>(APP_VERSION, version);
    }

    @Override
    public Pair<Map<String, AttributeBean>, Map<String, List<TreeNodeBean>>>
            getBikAttributesNameAndType(Integer nodeId, Integer nodeKindId, String relationType, Integer treeId, Map<String, List<Integer>> usedLink) {
        Map<String, AttributeBean> attributeaNameAndType = new HashMap<String, AttributeBean>();
        List<AttributeBean> attributeNames = createBeansFromNamedQry("getBikAttributesNameAndType", AttributeBean.class, nodeId);
        for (AttributeBean ab : attributeNames) {
            if (ab.typeAttr.equalsIgnoreCase(AttributeType.selectOneJoinedObject.name())) {
                ab.valueOpt = BaseUtils.mergeWithSepEx(execNamedQuerySingleColAsList("getJoinedObjDstNodeIdAndName", null, nodeId), ";").trim();
            }
            attributeaNameAndType.put(ab.atrName, ab);
        }

        Map<String, List<TreeNodeBean>> map = new HashMap<String, List<TreeNodeBean>>();

        if (!BaseUtils.isMapEmpty(usedLink)) {
            for (Entry<String, List<Integer>> e : usedLink.entrySet()) {
                if (!BaseUtils.isCollectionEmpty(e.getValue())) {
                    List<TreeNodeBean> tnbs = createBeansFromNamedQry("getBikNodeUsedInHyperlinkIn", TreeNodeBean.class, e.getValue());
                    map.put(e.getKey(), tnbs);
                }
            }
        }
        Pair<Map<String, AttributeBean>, Map<String, List<TreeNodeBean>>> attrsAndNodesByKind
                = new Pair<Map<String, AttributeBean>, Map<String, List<TreeNodeBean>>>(attributeaNameAndType, map);
        return attrsAndNodesByKind;
    }

    @Override
    public HelpBean getHelpDescr(String helpKey, String caption) {
        return getMultipleHelpDescr(helpKey, caption, I18nMessage.getCurrentLocaleName());
    }

    @Override
    public HelpBean getMultipleHelpDescr(String helpKey, String caption, String lang) {
        HelpBean hb;

        if (caption != null) {
            hb = createBeanFromNamedQry("getHelpDescr", HelpBean.class, true, helpKey, caption, lang);
        } else {
            hb = createBeanFromNamedQry("getHelpDescrAndCaption", HelpBean.class, true, helpKey, lang);
        }
        return hb;
    }

    @Override
    public List<HelpBean> getHelpHints() {
        return createBeansFromNamedQry("getHelpHints", HelpBean.class
        );
    }

    public void addOrUpdateBikHelp(HelpBean hb) {
        hb.langToLowerCase();
        HelpBean safeHb = new HelpBean();
        safeHb.helpKey = hb.helpKey;
        safeHb.caption = cleanHtml(hb.caption);
        safeHb.textHelp = cleanHtml(hb.textHelp);
        safeHb.lang = hb.lang;
        execNamedCommand("addOrUpdateBikHelp", safeHb);
    }

    public void updateBikHelpHint(HelpBean hb) {
        hb.langToLowerCase();
        HelpBean safeHb = new HelpBean();
        safeHb.helpKey = hb.helpKey;
        safeHb.caption = cleanHtml(hb.caption);
        safeHb.textHelp = cleanHtml(hb.textHelp);
        safeHb.lang = hb.lang;
        execNamedCommand("updateBikHelpHint", hb);
    }

    // usuwanie z powiązań uzytkonik w kazdej innej zakładce niż  Społeczności BI
    public boolean deleteUserInNode(int nodeId, Integer userId, int roleInKindId) {
        execNamedCommand("deleteUserInNode", nodeId, userId, roleInKindId);
        return deleteBadLinkedUsersUnderRole();
    }

    public Map<Integer, Integer> getDescendantsCntInMainRoles(int nodeId) {
        List<Map<String, Object>> rows = execNamedQueryCFN("getDescendantsCntInMainRoles",
                FieldNameConversion.ToLowerCase, nodeId);

        Map<Integer, Integer> roleCnt = new HashMap<Integer, Integer>();
        for (Map<String, Object> row : rows) {
            roleCnt.put(sqlNumberToInt(row.get("role_for_node_id")), sqlNumberToInt(row.get("role_cnt")));
        }
        return roleCnt;
    }

    public List<NewsBean> getNewsByUser() {
        return getNewsByUser(getLoggedUserId());
    }

    public NewsBean markNewsAsReadAndGetNew(int newsId) {
        execNamedCommand("markNewsAsRead", newsId, getLoggedUserId());

        return createBeanFromNamedQry("getOneNewsByUser", NewsBean.class, true, newsId, getLoggedUserId());
    }

    @Override
    public NewsBean addNews(NewsBean news) {
        NewsBean safeNews = new NewsBean();
        safeNews.id = news.id;
        safeNews.title = stripHtmlTags(cleanHtml(news.title));
        safeNews.text = cleanHtml(news.text);
        safeNews.authorName = cleanHtml(news.authorName);
        safeNews.authorId = getLoggedUserId();
        safeNews.dateAdded = news.dateAdded;
        safeNews.isRead = news.isRead;
        if (isSendEmailNotifications()) {
            getMailSender().sendMailAdhoc(safeNews);

        }
        return createBeanFromNamedQry("addNews", NewsBean.class, true, safeNews);
    }

    public void addNewsForUser(NewsBean news, int onlyForUserId) {
        NewsBean safeNews = new NewsBean();
        safeNews.id = news.id;
        safeNews.title = stripHtmlTags(cleanHtml(news.title));
        safeNews.text = cleanHtml(news.text);
        safeNews.authorName = cleanHtml(news.authorName);
        safeNews.authorId = getLoggedUserId();
        safeNews.dateAdded = news.dateAdded;
        safeNews.isRead = news.isRead;
        execNamedCommand("addNewsExt", safeNews, onlyForUserId);
    }

    public void addNewsForUser(String title, String text, int onlyForUserId) {
        NewsBean safeNews = new NewsBean();
        safeNews.title = stripHtmlTags(cleanHtml(title));
        safeNews.text = cleanHtml(text);
        execNamedCommand("addNewsExt", safeNews, onlyForUserId);
    }

    public void updateNews(NewsBean news) {
        NewsBean safeNews = new NewsBean();
        safeNews.id = news.id;
        safeNews.title = stripHtmlTags(cleanHtml(news.title));
        safeNews.text = cleanHtml(news.text);
        safeNews.authorName = cleanHtml(news.authorName);
        safeNews.authorId = news.authorId;
        safeNews.dateAdded = news.dateAdded;
        safeNews.isRead = news.isRead;
        execNamedCommand("updateNews", safeNews);
    }

    @Override
    public void deleteNews(int newsId) {
        execNamedCommand("deleteNews", newsId);
    }

    protected boolean hasAllRequiredColumns(Integer treeId, List<String> colNames) {
        boolean res = true;
        Set<String> allColsName = new HashSet<String>(colNames);

        if (!allColsName.contains(PumpConstants.GENERIC_NODE_OBJID)) {
            res = false;
            add2TreeImportLog(treeId, "Nie ma kolumny " + PumpConstants.GENERIC_NODE_OBJID);
        }
        if (!allColsName.contains(PumpConstants.GENERIC_NODE_PARENT_OBJID)) {
            res = false;
            add2TreeImportLog(treeId, "Nie ma kolumny " + PumpConstants.GENERIC_NODE_PARENT_OBJID);

        }
        if (!allColsName.contains(PumpConstants.GENERIC_NODE_NODE_KIND_CODE)) {
            res = false;
            add2TreeImportLog(treeId, "Nie ma kolumny " + PumpConstants.GENERIC_NODE_NODE_KIND_CODE);
        }
        if (!allColsName.contains(PumpConstants.GENERIC_NODE_NAME)) {
            res = false;
            add2TreeImportLog(treeId, "Nie ma kolumny " + PumpConstants.GENERIC_NODE_NAME);
        }
        if (!allColsName.contains(PumpConstants.GENERIC_NODE_DESCR)) {
            res = false;
            add2TreeImportLog(treeId, "Nie ma kolumny " + PumpConstants.GENERIC_NODE_DESCR);
        }
        if (!allColsName.contains(PumpConstants.GENERIC_NODE_VISUAL_ORDER)) {
            res = false;
            add2TreeImportLog(treeId, "Nie ma kolumny " + PumpConstants.GENERIC_NODE_VISUAL_ORDER);
        }
        return res;
    }

    protected boolean hasUniqueObjIds(Integer treeId, Map<String, Integer> objIdsCntMap) {
        boolean res = true;
        for (Entry<String, Integer> e : objIdsCntMap.entrySet()) {
            if (e.getValue() > 1) {
                res = false;
                add2TreeImportLog(treeId, "OBJ_ID: " + e.getKey() + " jest powtórzony ");
            }
        }
        return res;
    }

    protected Map<String, Integer> readAllObjIds(Integer treeId, List<Map<String, String>> nodeInfoList) {
        Map<String, Integer> objIdsCntMap = new HashMap<String, Integer>();
        for (Map<String, String> e : nodeInfoList) {
            String objId = e.get(PumpConstants.GENERIC_NODE_OBJID);
            if (BaseUtils.isStrEmptyOrWhiteSpace(objId)) {
                add2TreeImportLog(treeId, PumpConstants.GENERIC_NODE_OBJID + " nie moe być pusty");
                return null;
            }
            int cnt = objIdsCntMap.containsKey(objId) ? objIdsCntMap.get(objId) : 0;
            objIdsCntMap.put(objId, cnt + 1);
        }

        return objIdsCntMap;
    }

    protected boolean hasAllAvailableParentObjId(Integer treeId, Map<String, Integer> objIdsCntMap, List<Map<String, String>> nodeInfoList) {
        boolean res = true;
        for (Map<String, String> m : nodeInfoList) {
            String parentObjId = m.get(PumpConstants.GENERIC_NODE_PARENT_OBJID);
            if (!BaseUtils.isStrEmptyOrWhiteSpace(parentObjId) && !objIdsCntMap.containsKey(parentObjId)) {
                res = false;
                add2TreeImportLog(treeId, PumpConstants.GENERIC_NODE_PARENT_OBJID + ": " + parentObjId + " nie istnieje");
            }
        }
        return res;
    }

    protected boolean hasFeasibleNodeKind(Integer treeId, List<Map<String, String>> nodeInfoList) {
        boolean res = true;
        Set<String> allNodeKindCodes = execNamedQuerySingleColAsSet("getAvailableNodeKind4Tree", "code", treeId);
        for (Map<String, String> m : nodeInfoList) {
            String nodeKind = m.get(PumpConstants.GENERIC_NODE_NODE_KIND_CODE);
            if (BaseUtils.isStrEmptyOrWhiteSpace(nodeKind)) {
                res = false;
                add2TreeImportLog(treeId, PumpConstants.GENERIC_NODE_NODE_KIND_CODE + " nie może być pusty");
            }
            if (!allNodeKindCodes.contains(nodeKind)) {
                res = false;
                add2TreeImportLog(treeId, PumpConstants.GENERIC_NODE_NODE_KIND_CODE + ": " + nodeKind + " nie należy do tego typu drzewa");
            }
        }
        return res;
    }

    protected boolean hasDeasibleAttribute(Integer treeId, List<Map<String, String>> nodeInfoList, List<String> allColNames) {
        boolean res = true;
        Set<String> attrNames = new HashSet<String>(allColNames);
        attrNames.remove(PumpConstants.GENERIC_NODE_OBJID);
        attrNames.remove(PumpConstants.GENERIC_NODE_PARENT_OBJID);
        attrNames.remove(PumpConstants.GENERIC_NODE_NODE_KIND_CODE);
        attrNames.remove(PumpConstants.GENERIC_NODE_NAME);
        attrNames.remove(PumpConstants.GENERIC_NODE_DESCR);
        attrNames.remove(PumpConstants.GENERIC_NODE_VISUAL_ORDER);
        List<Map<String, Object>> attrAndTypes = execNamedQueryCFN("getAttrNameAndType4Tree", FieldNameConversion.ToLowerCase, treeId);
        Map<String, String> attrName2TypeMap = new HashMap<String, String>();
        for (Map<String, Object> m : attrAndTypes) {
            attrName2TypeMap.put(BaseUtils.safeToString(m.get("attr_name")), BaseUtils.safeToString(m.get("type_attr")));
        }

        for (String attrName : attrNames) {
            if (!attrName2TypeMap.containsKey(attrName)) {
                add2TreeImportLog(treeId, "Nie istnieje atrybutu: " + attrName + ". Zostanie utworzony");
            }
        }

        return res;
    }

    protected boolean isFeasibleImportFile(Integer treeId, String filePath) {
        //ct - brzydki sprawdzań
        String csvContent;
        try {
            InputStream sourceStream = new FileInputStream(filePath);
            csvContent = LameUtils.loadAsString(sourceStream, "*Cp1250,ISO-8859-2,UTF-8");
        } catch (Exception ex) {
            add2TreeImportLog(treeId, "Nie można odczytać pliku");
            return false;
        }
        CSVReader reader = new CSVReader(csvContent, CSVReader.CSVParseMode.CONSIDER_QUOTES, ';', true);
        List<Map<String, String>> nodeInfoList = reader.readFullTableAsMaps();
        if (!hasAllRequiredColumns(treeId, reader.getColNames())) {
            return false;
        }
        Map<String, Integer> objIdsCntMap = readAllObjIds(treeId, nodeInfoList);
        if (objIdsCntMap == null) {
            return false;
        }
        if (!hasUniqueObjIds(treeId, objIdsCntMap)) {
            return false;
        }
        if (!hasAllAvailableParentObjId(treeId, objIdsCntMap, nodeInfoList)) {
            return false;
        }
        if (!hasFeasibleNodeKind(treeId, nodeInfoList)) {
            return false;
        }
        if (!hasDeasibleAttribute(treeId, nodeInfoList, reader.getColNames())) {
            return false;
        }

        return true;
    }

    @Override
    public void importCSVDataToTree(final String treeCode, final String filePath) {
//        INamedSqlsDAO<Object> metadataPumpAdhocDao = getMetadataPumpAdhocDao();
//        metadataPumpAdhocDao.execInAutoCommitMode(new IContinuation() {
//
//            @Override
//            public void doIt() {
//                try {
//                    String path = BaseUtils.ensureStrHasSuffix(dirForUpload.replace("/", "\\"), "\\", true) + filePath;
//                    int treeId = getTreeIdByTreeCode(treeCode);
//                    if (isFeasibleImportFile(treeId, path)) {
//                        Set<Integer> tmp = new HashSet<Integer>();
//                        tmp.add(treeId);
//                        calculateObjIds(treeId, tmp);
//                        CSVImporter csvImporter = new CSVImporter(getMetadataPumpAdhocDao(), treeCode);
//                        String csvContent = LameUtils.loadAsString(new FileInputStream(path), "*Cp1250,ISO-8859-2,UTF-8");
//                        CSVReader reader = new CSVReader(csvContent, CSVReader.CSVParseMode.CONSIDER_QUOTES, ';', true);
//                        Pair<Integer, String> pumpResult = csvImporter.pumpTree(reader.readFullTableAsMaps());
//                        add2TreeImportLog(treeId, "Indeksowanie zawartości drzewa...");
//                        registerChangeInTreeAndUpdateSearchIndex(treeId);
//                        if (PumpConstants.LOG_LOAD_STATUS_DONE != pumpResult.v1) {
//                            throw new LameRuntimeException(pumpResult.v2);
//                        }
//                    } else {
//                        add2TreeImportLog(treeId, "Nie można importotwać drzewa");
//                        throw new LameRuntimeException("Plik zawiera błędy");
//                    }
//                } catch (FileNotFoundException ex) {
//                    throw new LameRuntimeException(ex.getMessage());
//                }
//            }
//        });
    }

    public int getTreeIdByTreeCode(String treeCode) {
        return execNamedQuerySingleValAsInteger("getTreeIdByTreeCode", false, null, treeCode);
    }

    private void registerChangeInTreeAndUpdateSearchIndex(int treeId) {
        registerChangeInTree(treeId);
//        indexChangedNodes();
    }

    public Pair<Integer, String> getLastInstanceForRaport(String reportId, int nodeId) {
        String loggedUserName = BIKCenterUtils.trimRemoteUserByDomain(getLoggedUserName());
        Integer instanceId = execNamedQuerySingleValAsInteger("getBOserverIdForTreeId", false, "id" /* I18N: no */, nodeId);
        ConnectionParametersSapBoBean paramBO = getSapBoConnectionParametersForInstance(instanceId);
        Integer userID = execNamedQuerySingleVal("getBOUserID", true, "id" /* I18N: no */, loggedUserName, nodeId);
        SapBOReportInstancePump instancePump = new SapBOReportInstancePump(reportId, paramBO.checkRights ? userID : null);
        instancePump.setBOConnectionParameters(paramBO.server, paramBO.userName, paramBO.password);
        Pair<Integer, String> result = instancePump.pump();
        if (result == null) {
            return null; // brak dostępu
        }
        Boolean isRefreshOnOpen = isReportRefreshOnOpen(nodeId);
        if (isRefreshOnOpen != null && isRefreshOnOpen.booleanValue()) {
            return new Pair<Integer, String>(null, "Refresh on open" /* I18N: no */);
        }
        return result;
    }

    public Integer getDescendantsCntInMainRoleByUserRoleAndTreeKind(int userId, int roleForNodeId, Set<String> treeKindsForSelected) {
        return execNamedQuerySingleVal("getDescendantsCntInMainRoleByUserRoleAndTreeKind", true, "role_cnt", userId, roleForNodeId, getDynTreeKindsWhenTreeKindIsDynamic(treeKindsForSelected));
    }

    public void insertBIKStatisticsEveryTimeOneDay(String counterName, String parametr) {
        execNamedCommand("insertBIKStatisticsEveryTimeOneDay", counterName, getLoggedUserId(), parametr);
    }

    public Boolean isReportRefreshOnOpen(int reportNodeId) {
        return execNamedQuerySingleVal("getRefreshOnOpenStatus", true, "" + "result" /* I18N: no */, reportNodeId);
    }

    public List<ObjectInFvsChangeBean> getChangedDescendantsObjectInFvs(int nodeId, int userId) {
        List<ObjectInFvsChangeBean> l = createBeansFromNamedQry("getChangedDescendantsObjectInFvs", ObjectInFvsChangeBean.class, nodeId, userId);
        for (ObjectInFvsChangeBean o : l) {
            o.ancestorNodePath = getAncestorNodePath(o.branchIds);
        }
        return l;
    }

    public void insertBIKStatisticsTabAndNode(String counterName, String description, String parametr) {
        execNamedCommand("insertBIKStatisticsTabAndNode", counterName, description, getLoggedUserId(), parametr);
    }

    public boolean isInsertStatisticsForAllNodes() {
        String appPropValue = getAppPropValue("insertStatisticsForAllNodes");
        return BaseUtils.tryParseBoolean(appPropValue);
    }

    public void insertBIKStatisticsExtTabAndNode(int nodeId, String branchIds) {
        boolean isInsertStatisticsForAllNodes = isInsertStatisticsForAllNodes();
        //muszę sprawdzić po tronie serwera czy użytkownik jest zalogowany, nie wystarczy
        //tylko sorawdzić tego po stronie GWT - ponieważ przeglądarka może długo nic nie robić
        Integer systemUserBikUserId = getLoggedUserId();
        if (systemUserBikUserId == null) {
            return;
        }
        if (!isInsertStatisticsForAllNodes) {
            Map<String, Object> val = execNamedQuerySingleRowCFN(
                    "insertBIKStatisticsExtNodePrev", true, FieldNameConversion.ToJavaPropName, branchIds);

            if (val != null) {
                int nodeIdVal = (Integer) val.get("id" /* I18N: no */);
                String branchIdsVal = (String) val.get("branchIds");
                execNamedCommand("insertBIKStatisticsExtTabAndNode", nodeIdVal, branchIdsVal, systemUserBikUserId);
            }
        } else {
            execNamedCommand("insertAllBIKStatistics", nodeId, systemUserBikUserId);

        }
    }

    public List<StatisticsBean> getStatisticsDict() {
        //return createBeansFromNamedQry("getAllStatisticsDict",StatisticsBean.class );
        return createBeansFromNamedQry("getAllStatisticsExtDict", StatisticsBean.class
        );
    }

    public void deleteAllChangedObjectInFvsInFvs(int nodeId) {
        execNamedCommand("deleteAllChangedObjectInFvsInFvs", nodeId, getLoggedUserId());
    }

    public void deleteChangedObjectInFvs(int nodeId) {
        execNamedCommand("deleteChangedObjectInFvs", nodeId, getLoggedUserId());
    }

    public void deleteAllChangedObjectForUser() {
        execNamedCommand("deleteAllChangedObjectForUser", getLoggedUserId());
    }

    public void copyAllRolesUser(int userIdFrom, int userIdTo, Boolean isMoveMain, Boolean isCopyAuxiliary) {
        execNamedCommand("copyAllRolesUser", userIdFrom, userIdTo, isMoveMain, isCopyAuxiliary);
    }

    public List<String> getAllUserLogged() {
        BOXIServiceImplData serviceImplData = getBOXIServiceImplDataForCurrentRequest();

        List<String> s = new ArrayList<String>();

        for (Entry<String, Date> a : serviceImplData.loggedUsers.entrySet()) {
            if (new Date().getTime() - a.getValue().getTime() > 50000) {//(120000)
                s.add(a.getKey());
            }
        }
        for (String l : s) {
            serviceImplData.loggedUsers.remove(l);
        }

        return new ArrayList<String>(serviceImplData.loggedUsers.keySet());
    }

    public List<TutorialBean> getTutorial() {
        return createBeansFromNamedQry("getTutorial", TutorialBean.class, getLoggedUserId());
    }

    public void markTutorialAsRead(int tutorialId) {
        execNamedCommand("markTutorialAsRead", tutorialId, getLoggedUserId());
    }

    public List<NodeAuthorBean> getRankUsers() {
        return createBeansFromNamedQry("getRankUsers", NodeAuthorBean.class, getAvatarFieldForAd());
    }

    public void addUserToRank(int nodeId) {
        execNamedCommand("addUserToRank", getLoggedUserId(), nodeId);

        ChangedRankingBean crb = getChangedRanking();
        if (crb != null) {
            getBOXIServiceImplDataForCurrentRequest().currentUserRanking = crb;//getStarredUsers();
//            lastRankingChangedTimeStamp = System.currentTimeMillis();
        }
    }

    protected ChangedRankingBean getChangedRanking() {
        ChangedRankingBean crb = getStarredUsers();
        Set<Integer> starredBikUser = crb.starredBikUser;//execNamedQuerySingleColAsSet("getStarredUser", "user_id", getStarredUserCount());
        Set<Integer> changedRankingstarredBikUser = getBOXIServiceImplDataForCurrentRequest().currentUserRanking.starredBikUser;
        if (starredBikUser.size() != changedRankingstarredBikUser.size() || !starredBikUser.containsAll(changedRankingstarredBikUser)) {
            return crb;
        }
        return null;
    }

//    protected boolean isChangedRanking() {
//        Set<Integer> starredBikUser = execNamedQuerySingleColAsSet("getStarredUser", "user_id", getStarredUserCount());
//        Set<Integer> changedRankingstarredBikUser = currentUserRanking.starredBikUser;
//        if (starredBikUser.size() != changedRankingstarredBikUser.size() || !starredBikUser.containsAll(changedRankingstarredBikUser)) {
//            return true;
//        }
//        return false;
//    }
    @Override
    @PerformOnAnyDb
    public ChangedRankingBean getChangedRanking(long timeStamp) {

        //ww: jeżeli kogoś wyloguje po stronie serwera i mamy tryb MultiX, to
        // może następować próba wołania tego, ale się nie uda - może polecieć babol
        // tutaj temu zapobiegamy, zwracamy NULL, co klient obsłuży sobie dobrze
        if (getOptSingleBiksDbForCurrentRequest() == null) {
            return null;
        }

        BOXIServiceImplData serviceImplData = getBOXIServiceImplDataForCurrentRequest();
//        ChangedRankingBean cb;
        if (serviceImplData.currentUserRanking.timeStamp > timeStamp) {
//            cb =
            return serviceImplData.currentUserRanking;

        } else {
//            cb = new ChangedRankingBean();
//            cb.timeStamp = timeStamp;
            return null;
        }
//        return cb;
    }

    public List<HomePageBean> getHomePageHints() {
        return createBeansFromNamedQry("getHomePageHints", HomePageBean.class
        );
    }

    protected boolean innerTabOfMyBIKSHasNewItemsForLoggedUser(String queryName) {
        //Jeśli będą elementy o statusie Nowy, to zostanie zwrócona 1, wpp nie będzie wyniku.
        Integer hasNewItemsFlag = execNamedQuerySingleVal(queryName, true, "has_new_items", getLoggedUserId());
        return hasNewItemsFlag != null;
    }

    protected boolean favouritesTabOfMyBIKSHasNewItemsForLoggedUser() {
        return innerTabOfMyBIKSHasNewItemsForLoggedUser("favouritesTabOfMyBIKSHasNewItemsForLoggedUser");
    }

    protected boolean newsTabOfMyBIKSHasNewItemsForLoggedUser() {
        return innerTabOfMyBIKSHasNewItemsForLoggedUser("newsTabOfMyBIKSHasNewItemsForLoggedUser");
    }

    @Override
    public Set<String> getShortCodesForMyBIKSTabsWithNewItems() {
        Set<String> res = new HashSet<String>();

        if (favouritesTabOfMyBIKSHasNewItemsForLoggedUser()) {
            res.add(BIKConstants.MYBIKSTAB_SHORT_CODE_FAVOURITES);
        }
        if (newsTabOfMyBIKSHasNewItemsForLoggedUser()) {
            res.add(BIKConstants.MYBIKSTAB_SHORT_CODE_NEWS);
        }

        return res;
    }

    public void setSchedule(List<SchedulePumpBean> beans) {
        execNamedCommand("setSchedule", beans);
        getPumpExecutor().setScheduler(getSchedule());
    }

    protected String getWelcomeMsgForUser() {
        final SystemUserBean loggedUserBean = getLoggedUserBean();

        if (loggedUserBean == null) {
            return null;
        }
        // narazie ten sam tekst dla eksperta i admina , bo brak oddzielnego tekstu dla admina.
//        return BIKCenterUtils.userHasRightRoleWithCode(getLoggedUserBean(), "Expert" /* I18N: no */)
//                || BIKCenterUtils.userHasRightRoleWithCode(getLoggedUserBean(), "Administrator" /* I18N: no */)
//                ? getSensitiveAppPropValue("welcomeMsgExpertUser") : getSensitiveAppPropValue("welcomeMsgSimpleUser");
        return BIKRightRoles.isSysUserEffectiveExpert(loggedUserBean)
                ? getSensitiveAppPropValue("welcomeMsgExpertUser") : getSensitiveAppPropValue("welcomeMsgSimpleUser");
    }

    protected String getWelcomeMsgForVersion() {
        return parseAppPropValueToBoolean("isDemoVersion") ? getSensitiveAppPropValue("myBIKSDemoMsg") : getSensitiveAppPropValue("myBIKSGeneralMsg");
    }

    public List<String> getSearchHints() {
        return execNamedQuerySingleColAsList("getSearchHints", "name" /* I18N: no */);
    }

    public List<RightRoleBean> getAllRightRoles() {
        return createBeansFromNamedQry("getAllRightRoles", RightRoleBean.class);
    }

    public Map<String, RightRoleBean> getAllRightRolesByCode() {

        Map<String, RightRoleBean> allRightRoleCodes = new LinkedHashMap<String, RightRoleBean>();
        if (disableBuiltInRoles()) {
            return allRightRoleCodes;
        }

        List<RightRoleBean> l = getAllRightRoles();
        for (RightRoleBean rrb : l) {
            allRightRoleCodes.put(rrb.code, rrb);
        }
        return allRightRoleCodes;
    }

    protected boolean disableBuiltInRoles() {
        String disableBuiltInRoles = getAppPropValue(BIKConstants.APP_PROP_DISABLE_BUILT_IN_ROLES);
        return disableBuiltInRoles != null && (disableBuiltInRoles.equals("1") || disableBuiltInRoles.equals("true"));
    }

    protected List<Pair<Integer, Pair<Integer, Integer>>> getAllRightRolesOfUsersByIds() {
        List<Pair<Integer, Pair<Integer, Integer>>> allRightRolesOfUsersByIds = new ArrayList<Pair<Integer, Pair<Integer, Integer>>>();
        List<Map<String, Object>> listOfRightRolesOfUsersByIds = execNamedQueryCFN("getRightRolesOfUsersByIds", FieldNameConversion.ToJavaPropName);
        for (Map<String, Object> m : listOfRightRolesOfUsersByIds) {
            //allRightRolesOfUsersByIds.add(new Pair<Integer, Integer>((Integer) m.get("userId"), (Integer) m.get("rightId")));
            //Pair<Integer, Integer>
            allRightRolesOfUsersByIds.add(new Pair<Integer, Pair<Integer, Integer>>((Integer) m.get("userId"), new Pair<Integer, Integer>((Integer) m.get("roleId"), (Integer) m.get("treeId"))));
        }
        return allRightRolesOfUsersByIds;
    }

    protected void updateDisabledUsersCache(Collection<SystemUserBean> users) {
        if (users == null) {
            return;
        }

        Map<String, Boolean> dm = BaseUtils.makeHashMapOfSize(users.size());
        for (SystemUserBean sub : users) {
            dm.put(sub.loginName, sub.isDisabled);
        }

        markUsersAsDisabled(dm);
    }

    protected List<SystemUserBean> getAllBikSystemUsersWithRightRoles(String textOfFilter, Boolean isUserAccountDisabled,
            Boolean hasCommunityUser, RightRoleBean selectedRightRole, Set<Integer> selectedAuthorTreeIDs,
            Integer optCustomRightRoleId, boolean showUserInRoleFromGroup) {
        String avatarFiled = getAvatarFieldForAd();
        textOfFilter = BaseUtils.isStrEmptyOrWhiteSpace(textOfFilter) ? null : textOfFilter;
        List<SystemUserBean> allBikSystemUsers;
        if (selectedRightRole != null) {
            selectedRightRole = selectedRightRole.id == -1 ? null : selectedRightRole;
            allBikSystemUsers
                    = createBeansFromNamedQry("getAllBikSystemUserWithRightRoleFilter", SystemUserBean.class,
                            avatarFiled, textOfFilter, isUserAccountDisabled, hasCommunityUser, selectedRightRole, selectedAuthorTreeIDs, optCustomRightRoleId, showUserInRoleFromGroup);
        } else {
            allBikSystemUsers = createBeansFromNamedQry("getAllBikSystemUserWithFilter", SystemUserBean.class,
                    avatarFiled, textOfFilter, isUserAccountDisabled, hasCommunityUser, selectedAuthorTreeIDs, optCustomRightRoleId, showUserInRoleFromGroup);
        }

        updateDisabledUsersCache(allBikSystemUsers);

        Map<Integer, SystemUserBean> mapOfSystemUsers = BaseUtils.makeBeanMap(allBikSystemUsers);
        List<RightRoleBean> allRightRoles = getAllRightRoles();
        Map<Integer, RightRoleBean> mapOfAllRightRoles = BaseUtils.makeBeanMap(allRightRoles);

        List<Pair<Integer, Pair<Integer, Integer>>> rightRolesOfUsers = getAllRightRolesOfUsersByIds();
        for (Pair<Integer, Pair<Integer, Integer>> p : rightRolesOfUsers) {
            SystemUserBean sub = mapOfSystemUsers.get(p.v1);
            RightRoleBean rrb = mapOfAllRightRoles.get(p.v2.v1);
            // add creator and non public user tree ids
            if (sub != null && rrb != null) {
                if (rrb.code.equals("Creator")) {
                    Integer creatorTreeID = p.v2.v2;

                    if (!disableBuiltInRoles()) {
                        if (sub.userRightRoleCodes == null) {
                            sub.userRightRoleCodes = new LinkedHashSet<String>();
                        }
                        sub.userRightRoleCodes.add(rrb.code);
                    }
                    if (creatorTreeID != null) {
                        if (sub.userCreatorTreeIDs == null) {
                            sub.userCreatorTreeIDs = new LinkedHashSet<Integer>();
                        }
                        sub.userCreatorTreeIDs.add(creatorTreeID);
                    }
                }
                if (rrb.code.equals("Author")) {
                    Integer authorTreeID = p.v2.v2;

                    if (!disableBuiltInRoles()) {
                        if (sub.userRightRoleCodes == null) {
                            sub.userRightRoleCodes = new LinkedHashSet<String>();
                        }
                        sub.userRightRoleCodes.add(rrb.code);
                    }
                    if (authorTreeID != null) {
                        if (sub.userAuthorTreeIDs == null) {
                            sub.userAuthorTreeIDs = new LinkedHashSet<Integer>();
                        }
                        sub.userAuthorTreeIDs.add(authorTreeID);
                    }
                }

                if (rrb.code.equals("NonPublicUser")) {
                    Integer nonPublicUserTreeID = p.v2.v2;

                    if (!disableBuiltInRoles()) {
                        if (sub.userRightRoleCodes == null) {
                            sub.userRightRoleCodes = new LinkedHashSet<String>();
                        }
                        sub.userRightRoleCodes.add(rrb.code);
                    }
                    if (nonPublicUserTreeID != null) {
                        if (sub.nonPublicUserTreeIDs == null) {
                            sub.nonPublicUserTreeIDs = new LinkedHashSet<Integer>();
                        }
                        sub.nonPublicUserTreeIDs.add(nonPublicUserTreeID);
                    }
                }
            }
        }

        if (!mapOfSystemUsers.isEmpty()) {
            List<CustomRightRoleUserEntryBean> crruebs = createBeansFromNamedQry("getCustomRightRoleUserEntries",
                    CustomRightRoleUserEntryBean.class, mapOfSystemUsers.keySet());

            for (CustomRightRoleUserEntryBean crrueb : crruebs) {
                SystemUserBean sub = mapOfSystemUsers.get(crrueb.userId);
                Set<TreeNodeBranchBean> ents;
                if (sub.customRightRoleUserEntries == null) {
                    sub.customRightRoleUserEntries = new HashMap<Integer, Set<TreeNodeBranchBean>>();
                    ents = null;
                } else {
                    ents = sub.customRightRoleUserEntries.get(crrueb.roleId);
                }
                boolean noEnts = ents == null;
                if (noEnts) {
                    ents = new HashSet<TreeNodeBranchBean>();
                }
                ents.add(new TreeNodeBranchBean(crrueb.treeId, crrueb.nodeId, crrueb.branchIds));
                if (noEnts) {
                    sub.customRightRoleUserEntries.put(crrueb.roleId, ents);
                }
            }

            List<CustomRightRoleUserEntryBean> crruebsDomain = createBeansFromNamedQry("getCustomRightRoleUserFromRoleInGroup",
                    CustomRightRoleUserEntryBean.class, mapOfSystemUsers.keySet());
            for (CustomRightRoleUserEntryBean cd : crruebsDomain) {
                SystemUserBean sub = mapOfSystemUsers.get(cd.userId);
                if (sub.customRightRoleUserEntriesFromGroup == null) {
                    sub.customRightRoleUserEntriesFromGroup = new HashSet<Integer>();
                }
                sub.customRightRoleUserEntriesFromGroup.add(cd.roleId);
            }
        }
        return allBikSystemUsers;
    }

    public List<String> deleteTreeNodeWithTheUserRole(int id, String branchIds) {
//        registerChangeInNode(id);
        List<String> treeToRefresh = refreshTaxonomyTree(branchIds);
        List<Integer> ids = sqlNumbersToInts(execNamedQuerySingleColAsList("deleteTreeNodeWithTheUserRole", "id" /* I18N: no */, id, branchIds));
//        deleteNodesByIds(ids);
//        registerChangeInNodes(ids);
        return treeToRefresh;
    }

    public List<String> updateTreeNodeWithTheUserRole(int id, String name) {

        execNamedCommand("updateTreeNodeWithTheUserRole", id, name);

        updateNodeForFts(id, name, null);

        String branchIds = execNamedQuerySingleVal("updateLinkedTreeNode", true, "branch_ids", id, name);
        return refreshTaxonomyTree(branchIds);

    }

    public Pair<Pair<Integer, Integer>, List<String>> createTreeNodeWithTheUserRole(Integer parentNodeId, int nodeKindId, String name, String objId, int treeId) {
        Map<String, Object> row = execNamedQuerySingleRowCFN("createTreeNodeWithTheUserRole", true, FieldNameConversion.ToLowerCase,
                parentNodeId, nodeKindId, name, objId, treeId);

        Integer id = (Integer) row.get("id" /* I18N: no */);
        updateNodeForFts(id, name, null);
        fillDefaultValuesForAttributes(id);
        return new Pair<Pair<Integer, Integer>, List<String>>(new Pair<Integer, Integer>(id, (Integer) row.get("role_id")), refreshTaxonomyTreeByIdOrParent(id));

    }

    protected SystemUserBean bikSystemUserByLoginAndPassword(String login, String pwd, String avatarField) {
        SystemUserBean sub = createBeanFromNamedQry("bikSystemUserByLoginAndPassword", SystemUserBean.class, true,
                login, pwd, avatarField);
        getRightRolesForOneSystemUser(sub);
        return sub;
    }

    public List<SystemUserBean> getAllBikSystemUserWithFilter(String ticket, final String textOfFilter, final Boolean isUserAccountDisabled,
            final Boolean hasCommunityUser, final RightRoleBean selectedRightRole, final Set<Integer> selectedAuthorTreeIDs,
            final Integer optCustomRightRoleId, final boolean showUserInRoleFromGroup) {
        return runCancellableDBJob(ticket, new IContinuationWithReturn<List<SystemUserBean>>() {
            public List<SystemUserBean> doIt() {
                return getAllBikSystemUsersWithRightRoles(textOfFilter, isUserAccountDisabled, hasCommunityUser, selectedRightRole,
                        selectedAuthorTreeIDs, optCustomRightRoleId, showUserInRoleFromGroup);
            }
        });
    }

    @Override
    public List<TreeNodeBean> getBikUserRolesTreeNodes(Integer optUpSelectedNodeId, String showOnlyRoleByCode, boolean useLazyLoading) {
        setOptFilteringNodeActionCodeForCurrentRequestAddJoinedObjs();
        List<TreeNodeBean> tnb = createBeansFromNamedQry("getBikUserRolesTreeNodes", TreeNodeBean.class, optUpSelectedNodeId, showOnlyRoleByCode,
                //                BIKCenterUtils.userHasRightRoleWithCode(getLoggedUserBean(), "Administrator" /* I18N: no */),
                BIKRightRoles.isSysUserEffectiveAppAdmin(getLoggedUserBean()),
                useLazyLoading);
        translateSpecialTreeNodeBeans(tnb,
                true);
        return tnb;
    }

    protected boolean addLinkedUserUnderRoles() {
        return sqlNumberToInt(execNamedQuerySingleVal("addLinkedUserUnderRoles", true, null)) > 0;
    }

    public boolean deleteBadLinkedUsersUnderRole() {
        Integer cnt = sqlNumberToInt(execNamedQuerySingleVal("deleteBadLinkedUsersUnderRole", true, null));
        if (cnt == null) {
            cnt = 0;
        }
        return cnt > 0;
    }

    public List<DataQualityGroupBean> getDqcTestGroupExtradata(int nodeId) {
        ConnectionParametersDQCBean adParam = getDQCConnectionParameters();
        DQCTestRequestsReceiver dqcReceiver = getBOXIServiceImplDataForCurrentRequest().dqcTestRequestsReceiver;
        if (adParam.isActive == 1 && adParam.checkTestActivity == 1 && dqcReceiver != null) {
            boolean isSuccess = false;

            try {
                List<DataQualityGroupBean> tests = createBeansFromNamedQry("getDqcTestGroupExtradataOnline", DataQualityGroupBean.class, nodeId);
                List<DataQualityGroupBean> listToReturn = new ArrayList<DataQualityGroupBean>();
                Set<Integer> dailyTests = dqcReceiver.getDailyTests();
                for (DataQualityGroupBean test : tests) {
                    List<pl.bssg.metadatapump.dqc.DQCRequestBean> requestsForTest = dqcReceiver.getRequestsForTest(test.idTest);
                    int i = 0;
                    for (pl.bssg.metadatapump.dqc.DQCRequestBean requestBean : requestsForTest) {
                        DataQualityGroupBean newBean = new DataQualityGroupBean();
                        newBean.idTest = test.idTest;
                        newBean.link = test.link;
                        newBean.name = test.name;
                        newBean.nodeId = test.nodeId;
                        newBean.testHint = test.testHint;
                        newBean.treeCode = test.treeCode;
                        newBean.requestHint = requestBean.endTimestamp != null ? requestBean.endTimestamp : requestBean.startTimestamp;
                        if ((i == (requestsForTest.size() - 1)) && dailyTests.contains(test.idTest)) { // sprawdzanie czy sie wykonal - czy jest aktywny
                            Date now = new Date();
                            if (requestBean.endTimestamp != null && SimpleDateUtils.getDay(now) == SimpleDateUtils.getDay(requestBean.endTimestamp) && SimpleDateUtils.getMonth(now) == SimpleDateUtils.getMonth(requestBean.endTimestamp) && SimpleDateUtils.getYear(now) == SimpleDateUtils.getYear(requestBean.endTimestamp)) {
                                newBean.type = test.type;
                            } else {
                                newBean.type = BIKConstants.NODE_KIND_DQC_TEST_INACTIVE;
                            }
                        } else {
                            newBean.type = test.type;
                        }
                        newBean.isSuccess = requestBean.isSuccess;
                        i++;
                        listToReturn.add(newBean);
                    }
                }
                isSuccess = true;
                return listToReturn;
            } catch (Exception e) {
                System.out.println("Error in getting requests from DQC database: " + e.getMessage());

                return createBeansFromNamedQry("getDqcTestGroupExtradata", DataQualityGroupBean.class, nodeId);
            } finally {
                dqcReceiver.finishWorkUnit(isSuccess);

            }
        }
        return createBeansFromNamedQry("getDqcTestGroupExtradata", DataQualityGroupBean.class, nodeId);
    }

    public void resetPumpThread() {
//        try {
//            Thread tmp = pumpThread;
        BOXIServiceImplData serviceImplData = getBOXIServiceImplDataForCurrentRequest();
        serviceImplData.pumpExecutor.cancelStatementIfRunning();
        serviceImplData.pumpThread.interrupt();
        serviceImplData.pumpExecutor = new ThreadedPumpExecutor(urlForPublicAccess, dirForUpload, doAfterVerticalizeInPumps);
        serviceImplData.pumpExecutor.setConnectionConfig(getMssqlConnLoadCfg());//tConnectionConfig(mssqlConnCfg, getConnection(), getAdhocDao());
        serviceImplData.pumpExecutor.setScheduler(getSchedule());
        serviceImplData.pumpThread = new Thread(serviceImplData.pumpExecutor);
        serviceImplData.pumpThread.start();
        checkDataLoadingStatusAndUpdateIncorrectLogs();
//            Thread.sleep(5000);
//            System.out.println("tmp.is alive ?" + tmp.isAlive());
//        } catch (InterruptedException ex) {
//            Logger.getLogger(BOXIServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

    protected void addAuthorToNode(int nodeId) {
        Integer userId = getOptLoggedSocialUserId();
        Integer creatorId = getLoggedUserBean().id;
//        SystemUserBean sub = getLoggedUserBean();
//        Integer userId = sub != null ? sub.userId : null;
        execNamedCommand("addAuthorToNode", nodeId, userId, creatorId);
    }

    protected Integer getOptLoggedSocialUserId() {
        SystemUserBean sub = getLoggedUserBean();
        return sub != null ? sub.userId : null;
    }

    protected void updateAuthorForDefinition(int nodeId, String glossaryDef) {
        Integer userId = getOptLoggedSocialUserId();
//        SystemUserBean sub = getLoggedUserBean();
//        Integer userId = sub != null ? sub.userId : null;
        if (userId != null) {
//            String sqlTxt = getAdhocDao().provideNamedSqlText("updateAuthorForDefinition", nodeId, userId, glossaryDef);
//            System.out.println("updateAuthorForDefinition: sqlTxt:\n" + sqlTxt);
            execNamedCommand("updateAuthorForDefinition", nodeId, userId, glossaryDef);
        }
    }

    public Set<String> getIcons() {
        return execNamedQuerySingleColAsSet("getIcons", "name" /* I18N: no */);
    }

    public void changeToBranchOrLeaf(int nodeId, boolean changeBranchToleaf) {
        if (changeBranchToleaf) {
            execNamedCommand("changeBranchToLeaf", nodeId);
        } else {
            execNamedCommand("changeLeafToBranch", nodeId);
        }
    }

    //ww: zostawiam po to, żeby było widać jak wybrać aktualne locale
//    @Deprecated
//    protected String getLocaleLangInfo() {
//        String res;
//        String localeLang = I18nMessage.getCurrentLocaleName();
//
//        String localeLangFromAdhoc = execNamedQuerySingleVal("getLocaleLangFromAdhoc", false, null);
//
//        if (BaseUtils.safeEquals(localeLang, localeLangFromAdhoc)) {
//            res = I18n.jestOKZgodne.get() /* I18N:  */ + " localeLangs! localeLang=[" + localeLang + "]";
//        } else {
//            res = I18n.nieSaZgodneLocaleLangsWynikZImpl.get() /* I18N:  */ + "=[" + localeLang + "], " + I18n.wynikZAdhoc.get() /* I18N:  */ + "=["
//                    + localeLangFromAdhoc + "]";
//        }
//        return res;
//    }
    public Map<String, TreeKindBean> getTranslatedTreeKinds(int treeKindId) {
        Map<String, TreeKindBean> m = new HashMap<String, TreeKindBean>();
        List<TreeKindBean> tkbs = createBeansFromNamedQry("getTranslatedTreeKinds", TreeKindBean.class, treeKindId);

        for (TreeKindBean tkb : tkbs) {
            m.put(tkb.lang.toLowerCase(), tkb);
        }
        return m;
    }

    public Map<String, String> getTranslatedTrees(int treeId) {
        Map<String, String> m = new HashMap<String, String>();
        List<TreeBean> tkbs = createBeansFromNamedQry("getTranslatedTrees", TreeBean.class, treeId);

        for (TreeBean tkb : tkbs) {
            m.put(tkb.lang, tkb.nameInLang);
        }
        return m;
    }

    public void updateTutorial(int id, String title, String text) {
        execNamedCommand("updateTutorial", id, title, text);
    }

    public boolean getShowSSOLoginDialog() {
        return parseAppPropValueToBoolean("showSSOLoginDialog");
    }

    public boolean getDisableGuestModeVal(List<AppPropBean> appProps) {
        for (AppPropBean appProp : appProps) {
            if (appProp.name.equals("disableGuestMode")) {
                return BaseUtils.tryParseBoolean(appProp.val, false);
            }
        }
        return false;
    }

    public boolean isNasActive() {
        String nasServer = execNamedQuerySingleVal("getAdminSingleValue", true, "value" /* I18N: no */, "lisateradata.url");
        String nasDb = execNamedQuerySingleVal("getAdminSingleValue", true, "value" /* I18N: no */, "lisateradata.database");
        String teraUser = execNamedQuerySingleVal("getAdminSingleValue", true, "value" /* I18N: no */, "teradata.user");
        String teraPass = execNamedQuerySingleVal("getAdminSingleValue", true, "value" /* I18N: no */, "teradata.password");

        boolean isActive = !(nasServer == null || nasServer.isEmpty()
                || nasDb == null || nasDb.isEmpty()
                || teraUser == null || teraUser.isEmpty()
                || teraPass == null || teraPass.isEmpty());

        //System.out.println("isNasActive: active?=" + isActive + ", nasServer=[" + nasServer + "], nasDb=[" + nasDb + "], teraUser=[" + teraUser + "], teraPass=[" + teraPass + "]");
        return isActive;
    }

    public ErwinDiagramDataBean getErwinListTables(String areaId) {
        ErwinDiagramDataBean eddb = new ErwinDiagramDataBean();
        eddb.erwinListTables = createBeansFromNamedQry("getErwinTables", ErwinTableBean.class, areaId);
        eddb.erwinTableColumns = createBeansFromNamedQry("getErwinTableColumns", ErwinTableColumnBean.class, areaId);
        eddb.erwinRelationships = createBeansFromNamedQry("getErwinRelationships", ErwinRelationshipBean.class, areaId);
        eddb.erwinShapes = createBeansFromNamedQry("getErwinShapes", ErwinShapeBean.class, areaId);

        return eddb;
    }

    public List<ErwinDataModelProcessObjectsRel> getDataModelProcessObjectsRel(int nodeId, String areaName) {
        return createBeansFromNamedQry("getDataModelProcessObjectsRel", ErwinDataModelProcessObjectsRel.class, nodeId, areaName);
    }

    public Integer getNodeId(String objId, String treeCode) {
        return execNamedQuerySingleValAsInteger("getNodeId", true, "id" /* I18N: no */, objId, treeCode);
    }

    protected Map<String, Map<String, String>> getMssqlExProps() {
        MsSqlExPropConfigBean confBean = readAdminBean("mssql", MsSqlExPropConfigBean.class
        );
        Map<String, Map<String, String>> typeMap = new HashMap<String, Map<String, String>>();

        typeMap.put(BIKConstants.NODE_KIND_MSSQL_DATABASE, getMssqlOneTypeMap(confBean.exPropDatabase));
        typeMap.put(BIKConstants.NODE_KIND_MSSQL_TABLE, getMssqlOneTypeMap(confBean.exPropTable));
        typeMap.put(BIKConstants.NODE_KIND_MSSQL_VIEW, getMssqlOneTypeMap(confBean.exPropView));
        typeMap.put(BIKConstants.NODE_KIND_MSSQL_PROCEDURE, getMssqlOneTypeMap(confBean.exPropProcedure));
        typeMap.put(BIKConstants.NODE_KIND_MSSQL_FUNCTION, getMssqlOneTypeMap(confBean.exPropScalarFunction));
        typeMap.put(BIKConstants.NODE_KIND_MSSQL_TABLE_FUNCTION, getMssqlOneTypeMap(confBean.exPropTableFunction));
        typeMap.put(BIKConstants.NODE_KIND_MSSQL_COLUMN, getMssqlOneTypeMap(confBean.exPropColumn));
        typeMap.put(BIKConstants.NODE_KIND_MSSQL_COLUMN_FK, getMssqlOneTypeMap(confBean.exPropColumn));
        typeMap.put(BIKConstants.NODE_KIND_MSSQL_COLUMN_PK, getMssqlOneTypeMap(confBean.exPropColumn));
        return typeMap;
    }

    protected Map<String, String> getMssqlOneTypeMap(String typeConfig) {
        return PumpUtils.getExPropList(typeConfig, I18nMessage.getCurrentLocaleName());
    }

    protected boolean isConfluenceActive() {
        return getConfluenceConnectionParameters().isActive == 1;
    }

    public void publishArticleToConfluence(String biksUrlBase, String articleName,
            String articleBody, String confluenceLogin, String confluencePassword,
            String parentObjId, String space, Integer parentNodeId, int treeId) {
        if (!BaseUtils.isStrEmptyOrWhiteSpace(articleBody)) {
            articleBody = DeferredConfluenceObjAnchorReplacer.fixUnclosedImgHrBr(articleBody);
            List<IDeferredRegExprReplacer<?, ?>> replacers = new ArrayList<IDeferredRegExprReplacer<?, ?>>();
            replacers.add(new UrlAbsolutizingDeferredReplacer(biksUrlBase));
//        replacers.add(new DeferredConfluenceDwhLinkReplacer(importedChecker));
            articleBody = DeferredStringConverter.convert(replacers, articleBody);
        }

//        System.out.println("articleBody:\n" + articleBody);
        ConfluenceObjectBean createdPage = getConfluenceProxyConnector().publishArticleToConfluence(articleName, articleBody, confluenceLogin, confluencePassword, parentObjId, space);
        Map<String, String> map = new HashMap<String, String>();
        map.put(createdPage.id, createdPage.kind);
        saveConfluenceObjects(map, parentNodeId, treeId);
    }

    @Override
    public List<NodeKindBean> getNodeKindsIconsForTree(int treeId, String treeKind) {
        return createBeansFromNamedQry("getNodeKindsIconsForTree", NodeKindBean.class, treeId, treeKind);
    }

    @Override
    public void setNodeKindsIconsForTree(int treeId, List<NodeKindBean> icons) {
        execNamedCommand("setNodeKindsIconsForTree", treeId, icons);
    }

    protected void activateDBServer(ConnectionParametersDBServerBean bean) {
//        String databases = execNamedQuerySingleVal("getDatabasesForServer", false, null, bean.id);
        execNamedCommand("fixDatabasesNodesAfterDeactivate", bean.id);
        execNamedCommand("activateDBServer", PumpUtils.getServerNameWithoutSuffix(bean.name), getDatabasesFromConfigBean(bean), bean.treeId);
    }

    protected void deactivateDBServer(ConnectionParametersDBServerBean bean) {
//        String databases = execNamedQuerySingleVal("getDatabasesForServer", false, null, bean.id);
        execNamedCommand("fixDatabasesNodesAfterDeactivate", bean.id);
        execNamedCommand("deactivateDBServer", PumpUtils.getServerNameWithoutSuffix(bean.name), getDatabasesFromConfigBean(bean), bean.treeId);
    }

    protected List<String> getDatabasesFromConfigBean(ConnectionParametersDBServerBean bean) {
        String stringToSplit = bean.databaseFilter;
        if (BaseUtils.isStrEmptyOrWhiteSpace(bean.databaseFilter)) {
            stringToSplit = bean.databaseList;
        }
        return BaseUtils.splitBySep(stringToSplit, ",", true);
    }

    @Override
    public Integer findAnticipatedNodeOfTree(int treeId, Integer optAncestorNodeId, String anticipatedNodeName) {
        return execNamedQuerySingleValAsInteger("findAnticipatedNodeOfTree", true, null, treeId, optAncestorNodeId, anticipatedNodeName);
    }

    @Override
    public Collection<? extends IFoxyTransactionalPerformer> getTransactionalPerformers() {
        // jeżeli jesteśmy podłączeni do MasterBIKSa (to jest aktualna baza)
        // to nie mamy dodatkowych TransactionalPerformersów
        if (getOptSingleBiksDbForCurrentRequest() == null) {
            return null;
        }

        if (isMultiXMode()) {
            return null;
        }
        // jeżeli w wątku obsługi requestu zostanie użyty adhocDao z pompek, to się zawsze sfinalizuje transakcja
        // w sensie commit lub rollback
        final ThreadedPumpExecutor pumpExecutor = getPumpExecutor();
        return pumpExecutor != null ? Collections.singleton(pumpExecutor.getPumpAdhocDao()) : null;

    }

    private List<AttrSearchableBean> getAllSearchableAttributes() {
        List<AttrSearchableBean> res = createBeansFromNamedQry("getAllSearchableAttributes", AttrSearchableBean.class
        );
        return res;
    }

    @Override
    public List<AttrSearchableBean> getAttributesBySelectedTreeNode(Collection<Integer> treeIds, Set<Integer> kindsToSearchFor) {
//        throw new UnsupportedOperationException("Nie używany");
        //System.out.println(treeIds.size() + " " + kindsToSearchFor.size());
        if (BaseUtils.isCollectionEmpty(treeIds) && BaseUtils.isCollectionEmpty(kindsToSearchFor)) {
            return new ArrayList<AttrSearchableBean>();
        }
        //List<AttrSearchableBean> res = createBeansFromNamedQry("getAllSearchableAttributes", AttrSearchableBean.class);
        List<AttrSearchableBean> res = createBeansFromNamedQry("getAttributesBySelectedTreeNode", AttrSearchableBean.class, treeIds, kindsToSearchFor);
        return res;
    }

    @Override
    public Integer updateConfluenceNode(TreeNodeBean node) {
        Map<String, String> objectsIds = new HashMap<String, String>();
        objectsIds.put(node.objId, node.nodeKindCode);
        List<List<ConfluenceObjectBean>> contentForObjects = getContentForObjects(objectsIds);
        Map<String, ConfluenceObjectBean> confluenceNodes = new LinkedHashMap<String, ConfluenceObjectBean>();
        //convert from to Map
        for (List<ConfluenceObjectBean> list : contentForObjects) {
            for (ConfluenceObjectBean article : list) {
                confluenceNodes.put(article.id, article);
            }
        }

        List<Integer> bikNodes = new ArrayList<Integer>();
        bikNodes.add(node.id);
        if (updateOneBikNodeFromConfluence(node, confluenceNodes)) {
            //nie bylo kasowany w calosci
            int i = 0;

            while (i < bikNodes.size()) {
                //lista podrzednych wezlow
                TreeNodeBean parent = createBeanFromNamedQry("getNodeById", TreeNodeBean.class, true, bikNodes.get(i));
                List<TreeNodeBean> res = createBeansFromNamedQry("getNodeByParentId", TreeNodeBean.class, bikNodes.get(i++));
                for (TreeNodeBean bean : res) {
                    bikNodes.add(bean.id);
                    updateOneBikNodeFromConfluence(bean, confluenceNodes);
                }
            }
//convert to List
            List<ConfluenceObjectBean> nodesToAdd = new ArrayList<ConfluenceObjectBean>();
            for (Entry<String, ConfluenceObjectBean> e : confluenceNodes.entrySet()) {
                addOneBikNodeFromConfluence(node.treeId, e.getValue());
            }
            return node.id;
        } else {
            TreeNodeBean res = null;

            if (node.parentNodeId != null) {
                res = createBeanFromNamedQry("getNodeById", TreeNodeBean.class, true, node.parentNodeId);
            }
            if (res != null) {
                return res.id;
            } else {
                return null;
            }
        }
    }

    private void refreshConfluenceObject(int nodeId, ConfluenceObjectBean beanAfterUpdate) {
        execNamedCommand("updateOneConfluenceObject", nodeId, beanAfterUpdate);
        updateConfluenceNodeForFTS(nodeId, beanAfterUpdate);

        ConfluenceUtils.updateJoinedObjsForLinksToBikNodes(getMetadataPumpAdhocDao(), urlForPublicAccess,
                Collections.singletonMap(nodeId, beanAfterUpdate.content));
        //...        tu chyba brakuje kodu         execNamedCommand("confluenceJoinContentWithUsers");
        // TF - > autor powyższego   - nie brakuje, bo autor nie może się zmienić
        ConfluenceUtils.addAuthorsToRanking(getMetadataPumpAdhocDao());
    }

    private boolean updateOneBikNodeFromConfluence(TreeNodeBean node, Map<String, ConfluenceObjectBean> confluenceNodes) {
        ConfluenceObjectBean article = confluenceNodes.get(node.objId);
        confluenceNodes.remove(node.objId);
        if (article == null) {
            //jest kasowany w Confluence
            //sprawdz czy ten wezel jest pierwszy raz kasowany
//            TreeNodeBean root = node;
//            TreeNodeBean last = node;
//            while (root != null && confluenceNodes.get(root.objId) == null) {
//                last = root;
//                root = tree.get(root.id);
//            }
//            if (root == null || last == node) {
//                deleteTreeNode(node);
            execNamedQuerySingleVal("deleteSingleNode", true, null, node.id);

            return false;
//            }
        } else {
            refreshConfluenceObject(node.id, article);
        }
        return true;

    }

    private void saveConfluenceObjects(Integer parentId, Integer treeId, List<List<ConfluenceObjectBean>> contentForObjects) {
        List<TreeNodeBean> tnbs = createBeansFromNamedQry("addConfluenceObjectsGetNodes",
                TreeNodeBean.class,
                parentId, treeId, contentForObjects);
        Map<String, String> objIdLogin = new HashMap<String, String>();
        for (List<ConfluenceObjectBean> list : contentForObjects) {
            for (ConfluenceObjectBean cob : list) {
                objIdLogin.put(cob.id, cob.author);
            }
        }

//        Map<String, String> objIdToEmail = new HashMap<String, String>();
//        for (List<ConfluenceObjectBean> list : contentForObjects) {
//            for (ConfluenceObjectBean cob : list) {
//                objIdToEmail.put(cob.id, cob.authorEmail);
//            }
//        }
//        Map<String, Integer> emailToUserId = new HashMap<String, Integer>();
//        List<UserBean> allUser = getBikAllUsers();
//        for (UserBean userBean : allUser) {
//            emailToUserId.put(userBean.email, userBean.id);
//        }
//        Map<String, Integer> bikLoginToSysUserId = new HashMap<String, Integer>();
//        List<SystemUserBean> allBikSystemUser = getAllBikSystemUser();
//        for (SystemUserBean sysUser : allBikSystemUser) {
//            bikLoginToSysUserId.put(sysUser.loginName, sysUser.id);
//        }
        Map<Integer, String> nodeIdToDescrMap = new HashMap<Integer, String>();
        for (TreeNodeBean tnb : tnbs) {
            updateNodeForFts(tnb.id, tnb.name, stripHtmlTags(tnb.descr));
            nodeIdToDescrMap.put(tnb.id, tnb.descr);
        }

        ConfluenceUtils.updateJoinedObjsForLinksToBikNodes(getMetadataPumpAdhocDao(),
                urlForPublicAccess, nodeIdToDescrMap);

        // łączenie autorów artykułów z BIKSowymi użytkowninikami
        execNamedCommand(
                "confluenceJoinContentWithUsers");
        // dodaję autorów do rankingu
        ConfluenceUtils.addAuthorsToRanking(getMetadataPumpAdhocDao());
    }

    private void addOneBikNodeFromConfluence(Integer treeId, ConfluenceObjectBean article) {
        Integer parentId = execNamedQuerySingleValAsInteger("getInsertedNodeIdByParentId", false, "id", article.parentId);
        List<ConfluenceObjectBean> list = new ArrayList<ConfluenceObjectBean>();
        list.add(article);
        List<List<ConfluenceObjectBean>> contentForObjects = new ArrayList<List<ConfluenceObjectBean>>();
        contentForObjects.add(list);

        saveConfluenceObjects(parentId, treeId, contentForObjects);
    }

    protected synchronized void calculateAttributesAndIndexingForSearch(int nodeId) {
        execNamedCommand("calculateAttributesIfNeeded", nodeId);
//        addOrUpdateSpidHistoryForCurrentUser();
//        execNamedCommand("verticalizeNodeAttrsForAttribute", nodeId);
    }

    @Override
    public List<MyObjectsBean> getMyObjects() {
        return getObjectsForUser(getLoggedUserBean().loginName);
    }

    @Override
    public List<UserObjectBean> getUsersObjects() {
        List<UserObjectBean> list = new ArrayList<UserObjectBean>();
        List<UserBean> users = getBikAllUsers();
        for (UserBean user : users) {
            List<MyObjectsBean> objectsForUser = getObjectsForUser(user.loginName);
            list.add(new UserObjectBean(user, objectsForUser));
        }
        return list;
    }

    protected List<MyObjectsBean> getObjectsForUser(String loginName) {
        List<String> roles = BaseUtils.splitBySep(getAppPropValue(BIKConstants.APP_PROP_MY_OBJECTS_ROLES), ",", true);
        List<String> trees = BaseUtils.splitBySep(getAppPropValue(BIKConstants.APP_PROP_MY_OBJECTS_TREE_CODE), ",", true);
        List<String> nodeKinds = BaseUtils.splitBySep(getAppPropValue(BIKConstants.APP_PROP_MY_OBJECTS_NODE_KINDS), ",", true);
        List<String> attributes = BaseUtils.splitBySep(getAppPropValue(BIKConstants.APP_PROP_MY_OBJECTS_ATTRIBUTES), "|", true);
        List<MyObjectsBean> objects = new ArrayList<MyObjectsBean>();

        for (String role : roles) {
            List<ChildrenBean> myObj = createBeansFromNamedQry("getMyObjects", ChildrenBean.class, loginName, role, trees, nodeKinds);
            for (ChildrenBean object : myObj) {
                object.attributes = createBeansFromNamedQry("bikEntityAttribute", AttributeBean.class, object.nodeId, attributes);
            }

            objects.add(
                    new MyObjectsBean(role, myObj));
        }
        return objects;
    }

    @Override
    public void registerNodeActionCodes(Set<String> codes) {
        if (!BaseUtils.isCollectionEmpty(codes)) {
            execNamedCommand("registerNodeActionCodes", codes);

        }
    }

    protected List<CustomRightRoleBean> getCustomRightRoles() {
        return createBeansFromNamedQry("getCustomRightRoles", CustomRightRoleBean.class);
    }

    @SuppressWarnings("unchecked")
    protected Map<String, Integer> getNodeActionCodeToIdMap() {
        List<Map<String, Object>> rows = execNamedQueryCFN("getNodeActionCodeToIdMap", FieldNameConversion.ToLowerCase);
        return BaseUtils.projectCollOfMapToMapVals2(rows, "code", "id");
    }

    public Set<Integer> getTreeIdsForCustomRightRoles() {

        return getRequestCachedValue("getTreeIdsForCustomRightRoles", new IContinuationWithReturn<Set<Integer>>() {

            @Override
            public Set<Integer> doIt() {
                return execNamedQuerySingleColAsSet("getTreeIdsForCustomRightRoles", null);
            }
        });

        //        Map<String, Object> rts = getRequestTempStorage();
        //
        //        Set<Integer> tifcrr = (Set<Integer>) rts.get("getTreeIdsForCustomRightRoles");
        //
        //        if (tifcrr == null) {
        //            tifcrr = execNamedQuerySingleColAsSet("getTreeIdsForCustomRightRoles", null);
        //            rts.put("getTreeIdsForCustomRightRoles", tifcrr);
        //        }
        //
        //        return tifcrr;
    }

    protected Map<Integer, Set<Integer>> getCustomRightRolesActionsInTreeRoots() {
        List<Map<String, Object>> rows = execNamedQueryCFN("getCustomRightRolesActionsInTreeRoots", FieldNameConversion.ToLowerCase);

        Map<Integer, Set<Integer>> res = new HashMap<Integer, Set<Integer>>();

        for (Map<String, Object> row : rows) {
            int treeId = sqlNumberInRowToInt(row, "tree_id");
            int actionId = sqlNumberInRowToInt(row, "action_id");

            Set<Integer> actionIds = res.get(treeId);
            if (actionIds == null) {
                actionIds = new HashSet<Integer>();
                res.put(treeId, actionIds);
            }
            actionIds.add(actionId);
        }
        return res;
    }

    @Override
    public Set<TreeNodeBranchBean> getRutEntries() {
        return new HashSet<TreeNodeBranchBean>(createBeansFromNamedQry("getRutEntries", TreeNodeBranchBean.class
        ));
    }

    @Override
    public void updateRutEntries(Set<Pair<Integer, Integer>> newRutEntries) {
//         execNamedCommand("updateRutEntries", newRutEntries);
        Set<String> userLogins = execNamedQuerySingleColAsSet("updateRutEntries", null, newRutEntries);

        for (String userLogin : userLogins) {
            registerChangeForUser(userLogin);
        }
    }

    public int getMaxDataQualityLastRequests() {
        String s = execNamedQuerySingleVal("getAdminSingleValue", true, null, "dqm.lastErrorRequests");
        return BaseUtils.tryParseInteger(s, 7);
    }

    protected void addNodeModificationToLog(int id, int treeId, String name, boolean isDeleted, String branchIds) {
        BOXIServiceImplData implData = getBOXIServiceImplDataForCurrentRequest();
        final Map<Integer, Map<Long, NodeModificationLogEntryBean>> nodeModificationLog = implData.nodeModificationLog;

        synchronized (nodeModificationLog) {
            Map<Long, NodeModificationLogEntryBean> m = nodeModificationLog.get(treeId);

            if (m == null) {
                m = new HashMap<Long, NodeModificationLogEntryBean>();
                nodeModificationLog.put(treeId, m);
            }

            m.put(System.currentTimeMillis(), new NodeModificationLogEntryBean(id, name, isDeleted, branchIds));
        }
    }

    protected void addNodeModificationToLog(int id) {

    }

    @Override
    public List<NodeModificationLogEntryBean> getModifiedNodesFromLog(int treeId, long timeStamp) {
        return null;
    }

    protected void addAccessedTreeInCurrentSession(int treeId) {

        // cicho-loguj-cicho - na wszelki wypadek, gdyby to zostało wywałane z
        // wątku, który nie obsługuje requestu HTTP (np. z konektora)
        try {
            BOXIServiceImplData implData = getBOXIServiceImplDataForCurrentRequest();
            final Map<Integer, Pair<Long, String>> treeIdToModificationTimeStampMap = implData.treeIdToModificationTimeStampMap;
            synchronized (treeIdToModificationTimeStampMap) {
//                String dbName = getCurrentDatabase().get();
                String instanceId = getFoxyAppInstanceId();

                BOXISessionData sessionData = getSessionData();

                Map<Integer, Long> accessedTreeIds = sessionData.accessedTreesOnDb.get(instanceId);
                if (accessedTreeIds == null) {
                    accessedTreeIds = new HashMap<Integer, Long>();
                    sessionData.accessedTreesOnDb.put(instanceId, accessedTreeIds);
                }

                Long prevMs = accessedTreeIds.get(treeId);
                final long serviceMethodInvokeCurrentMillis = getServiceMethodInvokeCurrentMillis();

                if (prevMs == null || prevMs > serviceMethodInvokeCurrentMillis) {
                    accessedTreeIds.put(treeId, serviceMethodInvokeCurrentMillis);
                }
            }
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("exception in addAccessedTreeInCurrentSession(" + treeId + ")", ex);
            }
        }
    }

    protected void clearAccessedTreesInCurrentSession() {
        BOXIServiceImplData implData = getBOXIServiceImplDataForCurrentRequest();
        final Map<Integer, Pair<Long, String>> treeIdToModificationTimeStampMap = implData.treeIdToModificationTimeStampMap;
        synchronized (treeIdToModificationTimeStampMap) {
//            String dbName = getCurrentDatabase().get();
            String instanceId = getFoxyAppInstanceId();
            BOXISessionData sessionData = getSessionData();
            sessionData.accessedTreesOnDb.remove(instanceId);
        }
    }

    protected void registerChangeInTree(int treeId) {
        registerChangeInTrees(null, Collections.singleton(treeId));
    }

    @Override
    public void registerChangeInTrees(Long serverTimeStamp, Set<Integer> treeIds) {

        if (serverTimeStamp == null) {
            serverTimeStamp = System.currentTimeMillis();
        }

        BOXIServiceImplData implData = getBOXIServiceImplDataForCurrentRequest();
        final Map<Integer, Pair<Long, String>> treeIdToModificationTimeStampMap = implData.treeIdToModificationTimeStampMap;
        synchronized (treeIdToModificationTimeStampMap) {

            String foxyAppInstanceId = "BIKS";
            try {
                foxyAppInstanceId = getFoxyAppInstanceId();
            } catch (Exception ex) {
                logger.debug("Using app instance as: BIKS");
            }
//            if (BaseUtils.isStrEmptyOrWhiteSpace(foxyAppInstanceId)) {
//                foxyAppInstanceId = BaseUtils.generateRandomToken(64);
//            }
            Pair<Long, String> p = new Pair<Long, String>(serverTimeStamp, foxyAppInstanceId);
            for (Integer treeId : treeIds) {
                if (treeId != null) {
                    Pair<Long, String> oldP = treeIdToModificationTimeStampMap.put(treeId, p);

                    if (oldP != null) {
                        ThreadLocal<Map<Integer, Pair<Long, String>>> overwrittenMods = getOverwrittenMods();
                        if (overwrittenMods.get() == null) {
                            overwrittenMods.set(new HashMap<Integer, Pair<Long, String>>());
                        }
                        overwrittenMods.get().put(treeId, oldP);
                    }
                }
            }
        }
    }

//    protected void registerChangeInNodes(Iterable<Integer> nodeIds) {
//        Set<Integer> treeIds = getTreeIdsByNodeIds(nodeIds);
//        registerChangeInTrees(System.currentTimeMillis(), treeIds);
//    }
//    public void registerChangeInNode(int nodeId) {
//        registerChangeInNodes(Collections.singleton(nodeId));
//    }
    protected Set<Integer> getTreeIdsByNodeIds(Iterable<Integer> nodeIds) {
        if (nodeIds.iterator().hasNext()) {
            Set<Integer> res = execNamedQuerySingleColAsSet("getTreeIdsByNodeIds", null, nodeIds);
            return res;
        } else {
            return new HashSet<Integer>();
        }
    }

    protected void registerChangeForUser(String loginName) {
        BOXIServiceImplData implData = getBOXIServiceImplDataForCurrentRequest();

        implData.userDataInvalidatedTimeMillis.put(loginName, System.currentTimeMillis());

        Map<String, Date> m = implData.loggedUserAppInstances.get(loginName);
        if (m == null) {
            return;
        }

        synchronized (implData.instanceIdsForFullRefresh) {
//            dummyDump("loginName=" + loginName + ", invalidated sessions: " + m.keySet());
            implData.instanceIdsForFullRefresh.addAll(m.keySet());
        }
    }

    @PerformOnSingleBiksDb
    public void getTreeToExportViaCollector(final int treeId, final ILameCollector<Map<String, Object>> collector) {
//        getAdhocDao().execNamedQueryExCFN("getTreeToExport", false, false, collector, FieldNameConversion.ToUpperCase, treeId);

        final Set<String> allAttrNamesInTree = execNamedQuerySingleColAsSet("getAttributesNamesOfNodesInTree", null, treeId);

        getAdhocDao().execNamedQueryExCFN("getAttributesValuesOfNodesInTree", new IParametrizedContinuation<Iterator<Map<String, Object>>>() {

            @Override
            public void doIt(final Iterator<Map<String, Object>> attrIter) {

                getAdhocDao().execNamedQueryExCFN("getTreeToExport", new IParametrizedContinuation<Iterator<Map<String, Object>>>() {

                    @Override
                    public void doIt(final Iterator<Map<String, Object>> nodeIter) {

                        Map<String, Object> nodeRow;
                        Map<String, Object> attrRow = null;

                        String nodeObjId;
                        String attrObjId = null;

                        int nodeNodeId;
                        int attrNodeId = 0;

                        while (nodeIter.hasNext()) {
                            nodeRow = nodeIter.next();

                            nodeObjId = (String) nodeRow.get("OBJ_ID");
                            nodeNodeId = sqlNumberInRowToInt(nodeRow, "ID"); //(Integer) nodeRow.get("ID");

                            Map<String, String> attrVals = new HashMap<String, String>();

                            if (attrRow == null && attrIter.hasNext()) {
                                attrRow = attrIter.next();
                                attrObjId = (String) attrRow.get("OBJ_ID");
                                attrNodeId = sqlNumberInRowToInt(attrRow, "NODE_ID"); //(Integer) attrRow.get("NODE_ID");
                            }

                            while (attrRow != null) {

                                // <0 gdy nodeIter jest przed attrIter (wtedy nie doczytujemy wartości attr w tym przebiegu)
                                //  0 gdy są na tym samym obiekcie (należy doczytać wartości attr)
                                // >0 gdy nodeIter jest za attrIter (trzeba pominąć wiersz attr, a może więcej wierszy i znów sprawdzić)
                                int compareRes;

                                // sprawdzamy kiedy nodeIter jest dalej od attrItera
//                                if (nodeNodeId == attrNodeId) {
//                                    compareRes = 0;
//                                } else if (nodeObjId == null && attrObjId != null) {
//                                    compareRes = -1;
//                                } else if (nodeObjId != null && attrObjId == null) {
//                                    compareRes = 1;
//                                } else if (BaseUtils.safeEquals(nodeObjId, attrObjId)) {
//                                    // to nie może być to samo - mimo, że mają ten sam objId
//                                    // bo wcześniej by zostało złapane przez porównanie nodeNodeId z attrNodeId
//                                    compareRes = (nodeNodeId < attrNodeId) ? -1 : 1;
//                                } else {
//                                    // tutaj ani nodeObjId, ani attrObjId nie mogą być nullami
//                                    // to byśmy wcześniej wyłapali
//                                    compareRes = nodeObjId.compareTo(attrObjId);
//                                }
                                if (nodeNodeId == attrNodeId) {
                                    compareRes = 0;
                                } else {
                                    compareRes = (nodeNodeId < attrNodeId) ? -1 : 1;
                                }

                                if (compareRes < 0) {
                                    break;
                                }

                                if (compareRes == 0) {
                                    String attrName = (String) attrRow.get("ATR_NAME");
                                    String attrVal = (String) attrRow.get("VALUE_OPT");
                                    attrVals.put(attrName, attrVal);
                                }

                                // czytamy kolejny wiersz z attr
                                if (attrIter.hasNext()) {
                                    attrRow = attrIter.next();
                                    attrObjId = (String) attrRow.get("OBJ_ID");
                                    attrNodeId = sqlNumberInRowToInt(attrRow, "NODE_ID"); //(Integer) attrRow.get("NODE_ID");
                                } else {
                                    attrRow = null;
                                }
                            }

                            nodeRow.remove("ID");

                            for (String attrName : allAttrNamesInTree) {
                                String attrVal = attrVals.get(attrName);
                                nodeRow.put(attrName, attrVal);
                            }

                            collector.add(nodeRow);
                        }
                    }
                }, FieldNameConversion.ToUpperCase, treeId);
            }
        }, FieldNameConversion.ToUpperCase, treeId);
    }

    @PerformOnSingleBiksDb
    public List<Map<String, Object>> getTreeToExport(int treeId, List<String> branchIds) {
        List<Map<String, Object>> returnList = new ArrayList<Map<String, Object>>();
        if (BaseUtils.isCollectionEmpty(branchIds)) {
            branchIds.add("");
        }
        List<Integer> ansectorIds = new ArrayList<Integer>();
        for (String branchId : branchIds) {
            List<String> idAsStrs = BaseUtils.splitBySep(branchId, "|");
            for (String s : idAsStrs) {
                Integer nodeId = BaseUtils.tryParseInteger(s);
                if (nodeId != null) {
                    ansectorIds.add(nodeId);
                }
            }
        }
        List<TreeNodeBean> rows = createBeansFromNamedQry("getTreeToExport", TreeNodeBean.class, treeId, branchIds, ansectorIds);
        for (TreeNodeBean oneBean : rows) {
            Map<String, Object> map = new LinkedHashMap<String, Object>();
            map.put(PumpConstants.GENERIC_NODE_OBJID, BaseUtils.ensureObjIdEndWith(oneBean.objId, "|"));
            map.put(PumpConstants.GENERIC_NODE_PARENT_OBJID, BaseUtils.ensureObjIdEndWith(oneBean.parentObjId, "|"));
            map.put(PumpConstants.GENERIC_NODE_NAME, oneBean.name);
            map.put(PumpConstants.GENERIC_NODE_NODE_KIND_CODE, oneBean.nodeKindCode);
            map.put(PumpConstants.GENERIC_NODE_DESCR, oneBean.descr);
            map.put(PumpConstants.GENERIC_NODE_VISUAL_ORDER, oneBean.visualOrder + "");

            List<AttributeBean> attributeNames = getAttributesForNode(oneBean.id, null);
            for (AttributeBean att : attributeNames) {
                if (!AttributeType.hyperlinkInMulti.name().equals(att.typeAttr)
                        && !AttributeType.permissions.name().equals(att.typeAttr)
                        && !AttributeType.hyperlinkInMono.name().equals(att.typeAttr)) {
//                    List<String> tmp = BaseUtils.splitBySep(att.atrLinValue, "_");
//                    Integer nodeId = BaseUtils.tryParseInt(tmp.get(0));
//                    att.atrLinValue = execNamedQuerySingleVal("getObjIdByNodeNamePath", false, null, nodeId) + "_" + tmp.get(1);
                    map.put(att.atrName, att.atrLinValue);
                }
            }
            returnList.add(map);
        }
        return returnList;
    }

    @Override
    @PerformOnDefaultDb
    @NotRequireUserLoggedIn
    public Pair<String, SystemUserBean> performUserLoginEx(String login, String password, String domain) {

        // to jest bezpieczne (nie walnie babolem) dla niezalogowanego
        // czyścimy!
        performUserLogout();

        BOXISessionData sessionData = getSessionData();

        boolean maybeCreateBikUser;
        String ssoDomain = null;

        // trzeba rozpoznać oczekiwany tryb logowania
        // 1. nic jest podane (same nulle)  -> domenowe standardowe (zwykłe SSO)
        // 2. podane login i password       -> klasyczne
        // 3. wszystko podane (żadny nulli) -> domenowe rozszerzone (SSO z domeną i hasłem)
        if (login == null && password == null && domain == null) {
            // remoteUser z sesji na pewno jest dobry - hasło sprawdzone
            // przez system operacyjny, może jest zablokowany itp.
            // ale w tym miejscu tego jeszcze nie wiemy
            // jedynie w trybie MultiX można sprawdzić czy istnieje taki
            // użytkownik - bo musi istnieć (nie można zakładać nowego)
            // to wyjdzie potem - okaże się, że ten login nie ma dostępnych baz
            String remoteUserLC = sessionData.remoteUser.toLowerCase();
            Pair<String, String> loginAndDomain = BaseUtils.splitString(remoteUserLC, "\\");
            if (!isMultiDomainMode() && loginAndDomain.v2 != null) { // loginy bez domen w nazwie
                login = loginAndDomain.v2;
            } else { // pełny login z domeną
                login = remoteUserLC;
            }
            if (loginAndDomain.v2 != null && loginAndDomain.v1 != null) { // jest domena w remoteUser
                ssoDomain = loginAndDomain.v1;
            }
            maybeCreateBikUser = true;
        } else if (login != null && password != null && domain == null) {
            // sprawdzenie, czy użytkownik i hasło są ok
            // w przypadku MultiX - na bazie MasterBiks
            // w przypadku SingleBiks - na bazie domyślnej
            // czyli -> zawsze działamy na bazie domyślnej, ale adnotacja metody
            // nam to zapewnia - nie trzeba nic przełączać
            if (!isClassicLoginDataCorrect(login, password)) {
                return null;
            }
            maybeCreateBikUser = false;
        } else if (login != null && password != null && domain != null) {
            // logowanie domenowe rozszerzone (SSO w określonej domenie)
            if (!new ADLoginAuthenticationChecker(domain, login, password).checkAuthentication()) {
                return null;
            }
            // a jeżeli się udało zweryfikować login/hasło/domenę, to i tak
            // zaraz wyjdzie czy są dostępne bazy (czyli konto założone)
            // czy nie
            login = domain + "\\" + login;
            ssoDomain = domain;
            maybeCreateBikUser = true;
        } else {
            throw new LameRuntimeException("performUserLogin: incorrect call, unsupported login mode, login="
                    + (login == null ? "" : "not ") + "null, password="
                    + (password == null ? "" : "not ") + "null, domain="
                    + (domain == null ? "" : "not ") + "null");
        }

        String singleBiksDb;

        // teraz trzeba określić do jakiej bazy singleBiks się ten użytkownik zalogował
        // w trybie MultiX sprawdzamy czy jest określona baza w URLu
        // w trybie SingleBiks jest tylko jedna opcja - udany login, to zwracamy pusty string ("")
        if (isMultiXMode()) {
            // zgarniamy dostępne bazy dla loginu - zapiszemy to w sesji
            // w wyniku może być pusta lista dostępnych baz (ale nie null)
            final Set<String> accessibleDbNames = multiXLoginsSubservice.loadDbNames(login);

            if (accessibleDbNames.isEmpty()) {
                return null;
            }

            sessionData.setAccessibleDbNames(accessibleDbNames);

            String requestedDbName = (String) getRequestTempStorage().get(MultixConstants.DB_NAME_PARAM);

            if (requestedDbName != null && accessibleDbNames.contains(requestedDbName)) {
                // baza w URLu jest osiągalna, czyli dobra - więc taki będzie wynik
                singleBiksDb = requestedDbName;
                final String loginF = login;

                Integer existsAndDisabledStatus = performOnSingleBiksExpliciteDb(requestedDbName, new IContinuationWithReturn<Integer>() {

                    @Override
                    public Integer doIt() {
                        return execNamedQuerySingleVal("getBikSystemUserExistsAndDisabledStatus", true, null, loginF, null);
                    }
                });

                if (existsAndDisabledStatus == null || existsAndDisabledStatus != 0) {
                    singleBiksDb = multiXLoginsSubservice.getDefaultDbNameForMultiXLogin(login);
                }

            } else {
                // bazy w URLu nie ma, albo nie jest dobra (nieosiągalna),
                // zatem zwracamy w wyniku bazę domyślną
                singleBiksDb = multiXLoginsSubservice.getDefaultDbNameForMultiXLogin(login);
            }
        } else {
            singleBiksDb = defaultDatabaseName;
        }

        // jeżeli doszliśmy tutaj, to singleBiksDb != null
        setSingleBiksDbForCurrentRequest(singleBiksDb);

        SystemUserBean sysUser = null;
        if (!isMultiXMode() && maybeCreateBikUser) {
            sysUser = getOrCreateSystemUserBeanByLogin(login, ssoDomain);
        } else {
            setCurrentDbForAdhoc(singleBiksDb);
            String avatarField = getAvatarFieldForAd();
            sysUser = bikSystemUserByLogin(login, avatarField);
        }

        systemUsersSubservice.setLoggedUserBeanInSessionData(sysUser);

        if (sysUser.isDisabled) {
            //return null;
            // logowanie OK (poprawne dane podane), ale użytkownik jest
            // zablokowany (disabled)
            sysUser = null;
            sessionData.userExistButDisabled = true;
        } else {
            sessionData.loginName = login;
        }

        return new Pair<String, SystemUserBean>(singleBiksDb, sysUser);
    }

    @Override
    @PerformOnDefaultDb
    @NotRequireUserLoggedIn
    public Pair<String, Boolean> performUserLogin(String login, String password, String domain) {

        Pair<String, SystemUserBean> p = performUserLoginEx(login, password, domain);

        if (p == null) {
            return null;
        }

        return new Pair<String, Boolean>(p.v1, p.v2 != null);
    }

    @Override
    @PerformOnAnyDb
    public String getLoggedUserName() {
        return super.getLoggedUserName();
    }

    @Override
    @PerformOnAnyDb
    public void maybeSetModifiedTreesInfoInResponse() {

        try {

//            if (isInMultiXModeAndNoSingleBiksDbDetermined()) {
//                return;
//            }
            if (checkClientLoggedUserNameVsServerAndMaybeSetInfoInResponse()) {
                return;
            }

            // dalej potrzebna jest baza SingleBiks - jak jej nie mamy - kończymy
            if (getOptSingleBiksDbForCurrentRequest() == null) {
                return;
            }

            int treeTabAutoRefreshLevel = BaseUtils.tryParseInt(getAppPropValueEx("treeTabAutoRefreshLevel", true));

            if (treeTabAutoRefreshLevel != 2 && treeTabAutoRefreshLevel != 3) {
                return;
            }

            BOXIServiceImplData implData = getBOXIServiceImplDataForCurrentRequest();
            String instId = getFoxyAppInstanceId();

            boolean needFullRefresh;
            synchronized (implData.instanceIdsForFullRefresh) {
                needFullRefresh = implData.instanceIdsForFullRefresh.remove(instId);
            }

            Set<Integer> modifiedTreeIds = new HashSet<Integer>();
            final Map<Integer, Pair<Long, String>> treeIdToModificationTimeStampMap = implData.treeIdToModificationTimeStampMap;
            synchronized (treeIdToModificationTimeStampMap) {
//                    String dbName = getCurrentDatabase().get();
                BOXISessionData sessionData = getSessionData();
                Map<Integer, Long> accessedTreeIds = sessionData.accessedTreesOnDb.get(instId);

                if (accessedTreeIds != null) {

//                    for (Entry<Integer, Pair<Long, String>> e : treeIdToModificationTimeStampMap.entrySet()) {
                    for (Map.Entry<Integer, Long> e : accessedTreeIds.entrySet()) {

//                        long modificationTs = e.getValue().v1;
//                        String modifyingInstId = e.getValue().v2;
//                        final Integer treeId = e.getKey();
                        final Integer treeId = e.getKey();
                        Pair<Long, String> p = treeIdToModificationTimeStampMap.get(treeId);

                        Long modificationTs = p == null ? null : p.v1;
                        String modifyingInstId = p == null ? null : p.v2;

//                        Long timeStampOfAccess = accessedTreeIds.get(treeId);
                        Long timeStampOfAccess = e.getValue();

                        boolean sameInst = BaseUtils.safeEquals(modifyingInstId, instId);

                        if (sameInst) {
                            Pair<Long, String> pp = getOverwrittenMods().get().get(treeId);
                            if (pp != null) {
                                if (!BaseUtils.safeEquals(pp.v2, instId)) {
                                    sameInst = false;
                                    modificationTs = pp.v1;
                                }
                            }
                        }

                        if (needFullRefresh
                                || (modificationTs != null && timeStampOfAccess != null && modificationTs >= timeStampOfAccess && !sameInst)) {
                            if (timeStampOfAccess != null) {
                                modifiedTreeIds.add(treeId);
                            }
                        }
                    }

                    accessedTreeIds.keySet().removeAll(modifiedTreeIds);
                }
                String modifiedAsStr = BaseUtils.mergeWithSepEx(modifiedTreeIds, ",");

//                System.out.println("maybeSetModifiedTreesInfoInResponse: modifiedAsStr=" + modifiedAsStr);
                setServiceExtraResponseData(modifiedAsStr);
            }

        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("exception in maybeSetModifiedTreesInfoInResponse", ex);
            }
        }
    }

    @PerformOnAnyDb
    protected boolean checkClientLoggedUserNameVsServerAndMaybeSetInfoInResponse() {
        LoggedUserOnClientVsServerStatus luocvss = calculateLuocvss();

        boolean badUser = luocvss != LoggedUserOnClientVsServerStatus.SameUser;

        if (badUser) {
            final String serd = ":" + luocvss.ordinal();
//            System.out.println("checkClientLoggedUserNameVsServerAndMaybeSetInfoInResponse: serd=" + serd);
            setServiceExtraResponseData(serd);
        }

        return badUser;
    }

//    @PerformOnSingleBiksDb
    @PerformOnAnyDb
    protected LoggedUserOnClientVsServerStatus calculateLuocvss() {
        String clientUserName = getClientLoggedUserName();
        String loggedUserName = getLoggedUserName();
        // uwaga - porównanie bez uwzględniania wielkości liter!
        boolean isSameUser = BaseUtils.safeEqualsStr(loggedUserName, clientUserName, true);
        LoggedUserOnClientVsServerStatus luocvss;
        if (isSameUser) {

            if (getOptSingleBiksDbForCurrentRequest() == null) {
                // tu możemy tylko stwierdzić, że to ten sam
                luocvss = LoggedUserOnClientVsServerStatus.SameUser;
            } else {
                final Set<String> disabledUsers = getBOXIServiceImplDataForCurrentRequest().disabledUsers;
                synchronized (disabledUsers) {
                    luocvss = disabledUsers.contains(clientUserName)
                            ? LoggedUserOnClientVsServerStatus.DisabledUser
                            : LoggedUserOnClientVsServerStatus.SameUser;
                }
            }
        } else {
            luocvss = loggedUserName == null ? LoggedUserOnClientVsServerStatus.UnloggedUser
                    : LoggedUserOnClientVsServerStatus.DifferentUser;
        }

        if (logger.isDebugEnabled()) {
            logger.debug("clientUserName=" + clientUserName + ", loggedUserName=" + loggedUserName + ", luocvss=" + luocvss
                    + ", singleBiksDb=" + getOptSingleBiksDbForCurrentRequest());
        }

//        dummyDump("clientUserName=" + clientUserName + ", loggedUserName=" + loggedUserName + ", luocvss=" + luocvss);
        return luocvss;
    }

    @PerformOnSingleBiksDb
    protected void checkRightsToAddOrEditsSysUsers(SystemUserBean optUserToEdit) {
        // czy tu potrzebe to sprawdzanie?
        if (!disableBuiltInRoles() && !BIKRightRoles.canSysUserAddOrEditSysUsers(getLoggedUserBean(), optUserToEdit)) {
            throw new ValidationExceptionBiks("You are not allowed to add or edit system users!");
        }
    }

    @PerformOnAnyDb
    protected static Map<Integer, Set<Pair<Integer, Integer>>> fixCustomRightRoleUserEntries(Map<Integer, Set<Pair<Integer, Integer>>> customRightRoleUserEntries) {
        if (customRightRoleUserEntries == null) {
            return null;
        }

        Map<Integer, Set<Pair<Integer, Integer>>> res = new HashMap<Integer, Set<Pair<Integer, Integer>>>(customRightRoleUserEntries);

        for (Map.Entry<Integer, Set<Pair<Integer, Integer>>> e : customRightRoleUserEntries.entrySet()) {
            if (BaseUtils.isCollectionEmpty(e.getValue())) {
                res.remove(e.getKey());
            }
        }

        return res;
    }

    @PerformOnSingleBiksDb
    protected void fixSysAdminRightRoleOnAddOrEdit(Set<String> userRightRoleCodes) {
        //ww->authors: fix na dodawanie SysAdmina przez AppAdmina
        // nie sprawdzane przy nowych rolach
        if (!BIKRightRoles.isSysUserSysAdmin(getLoggedUserBean())) {
            BIKRightRoles.clearSysAdminRightRole(userRightRoleCodes);
        }
    }

    @PerformOnSingleBiksDb
    protected boolean loginExists(String loginName, String oldLogin) {
        if (loginName != null && loginName.equals(oldLogin)) {
            return false;
        }
        int ret = execNamedQuerySingleValAsInteger("countExistingLogins", false, null, loginName);
        return ret > 0;
    }

    @PerformOnSingleBiksDb
    protected boolean loginExists(String loginName) {
        return loginExists(loginName, null);
    }

    @PerformOnSingleBiksDb
    @Override
    public SystemUserBean getOrCreateSystemUserBeanByLogin(String loginName, String optSsoDomain) {
        //String avatarField = systemUsersSubservice.getAvatarFieldForAd();
        //SystemUserBean user = systemUsersSubservice.bikSystemUserByLogin(loginName, avatarField);
        String avatarField = getAvatarFieldForAd();
        SystemUserBean user = bikSystemUserByLogin(loginName, avatarField);

        if (user == null) {
            multiXLoginsSubservice.addMultiXLoginToExistingDb(loginName);

            user = new SystemUserBean();
            user.loginName = loginName;
            boolean createDisabledUser = BaseUtils.tryParseBoolean((String) execNamedQuerySingleVal("getAppPropValue", false, "val", BOXIServiceImpl.CREATE_DISABLED_USERS_IN_SSO_PROP_NAME), false);
            user.isDisabled = createDisabledUser;
            user.userName = loginName;
            user.ssoDomain = optSsoDomain;
            user.ssoLogin = loginName.replaceFirst(".*\\\\", "");
            //string grantRights pokazuje skąd byli nadane uprawnienia "AD" - przy zasilaniu z AD, "kon" - kontekstu, "int" - interfejs
            String grantRights = "AD";
            user.id = sqlNumberToInt(execNamedQuerySingleVal("addBikSystemUserEx", false, "id" /* I18N: no */, loginName, null, createDisabledUser, null, null, null, null, user.ssoLogin, optSsoDomain, new ArrayList<Integer>(), false, grantRights));
            user.userRightRoleCodes = null;

            // jeśli konektor do AD jest włączony, pobierz dane dla tego loginu i stwórz użytkownika społecznościowego
            //systemUsersSubservice.createSocialUserFromADData(loginName);
            createSocialUserFromADData(user.ssoLogin, optSsoDomain, user.id);
            execNamedCommand("addUserToDomainGroup", user.id, user.ssoDomain);
        }
        return user;
    }

    @Override
    public void destroy() {
        destroySharedServiceObj();
        super.destroy();
    }

    @Override
    public void forSerializationOnly() throws ServiceMethodCallException {
        // no-op
    }

    @SuppressWarnings("unchecked")
    protected Map<String, String> getI18nTranslations() {
        List<Map<String, Object>> rows = execNamedQueryCFN("getI18nTranslations", FieldNameConversion.ToLowerCase);
        return BaseUtils.projectCollOfMapToMapVals2(rows, "code", "txt");
    }

    @Override
    public SystemUserGroupBean getSystemUsersGroup(int groupId) {
        SystemUserGroupBean usersGroupBean = createBeanFromNamedQry("getUsersGroup", SystemUserGroupBean.class, false, groupId);

        List<CustomRightRoleUserEntryBean> crruebs = createBeansFromNamedQry("getCustomRightRoleUserGroup",
                CustomRightRoleUserEntryBean.class, groupId);

        for (CustomRightRoleUserEntryBean crrueb : crruebs) {
            Set<TreeNodeBranchBean> ents;
            if (usersGroupBean.customRightRoleUserGroup == null) {
                usersGroupBean.customRightRoleUserGroup = new HashMap<Integer, Set<TreeNodeBranchBean>>();
                ents = null;
            } else {
                ents = usersGroupBean.customRightRoleUserGroup.get(crrueb.roleId);
            }
            boolean noEnts = ents == null;
            if (noEnts) {
                ents = new HashSet<TreeNodeBranchBean>();
            }
            ents.add(new TreeNodeBranchBean(crrueb.treeId, crrueb.nodeId, crrueb.branchIds));
            if (noEnts) {
                usersGroupBean.customRightRoleUserGroup.put(crrueb.roleId, ents);
            }
        }

        usersGroupBean.usersName = getUsersInGroup().get(groupId);
//        usersGroupBean.groupIn = getGroupInGroup("getInGroup").get(groupId);//A zawiera B
        usersGroupBean.inGroup = getGroupInGroup("getGroupIn").get(groupId);//B należy do A

        return usersGroupBean;
    }

    private Map<Integer, List<SystemUserGroupBean>> getGroupInGroup(String sql) {
        List<SystemUserGroupBean> ubs = createBeansFromNamedQry(sql, SystemUserGroupBean.class
        );
        Map<Integer, List<SystemUserGroupBean>> inGroupMap = new HashMap<Integer, List<SystemUserGroupBean>>();
        for (SystemUserGroupBean ub : ubs) {
            int id = ub.id;
            if (!inGroupMap.containsKey(id)) {
                inGroupMap.put(id, new ArrayList<SystemUserGroupBean>());
            }
            inGroupMap.get(id).add(ub);
        }
        return inGroupMap;
    }

    private Map<Integer, List<String>> getUsersInGroup() {
        List<SystemUserInGroupBean> usersInGroup = createBeansFromNamedQry("getUsersInGroup", SystemUserInGroupBean.class
        );
        Map<Integer, List<String>> map = new HashMap<Integer, List<String>>();
        for (SystemUserInGroupBean ub : usersInGroup) {
            int groupId = ub.groupId;
            if (!map.containsKey(groupId)) {
                map.put(groupId, new ArrayList<String>());
            }

            map.get(groupId).add(ub.loginName + " (" + ub.userName + ")");

        }
        return map;
    }

    @Override
    public void updateSystemUserGroup(int groupId, Map<Integer, Set<Pair<Integer, Integer>>> customRightRoleUserGroup) {
        execNamedCommand("addOrUpdateCustomRightRoleUserGroup", groupId, fixCustomRightRoleUserEntries(customRightRoleUserGroup));
    }

    @Override
    public void updateProfileInRole(int groupId, Map<Integer, Set<Pair<Integer, Integer>>> customRightRoleUserGroup) {
        //saving role profile
        execNamedCommand("addOrUpdateCustomRightRoleNewRightSys", groupId, fixCustomRightRoleUserEntries(customRightRoleUserGroup));
    }

    @Override
    public ConnectionParametersJdbcBean addJdbcConfig(String sourceName, String serverName) {
        Integer instanceId = execNamedQuerySingleValAsInteger("addJdbcConfig", true, "id" /* i18n: no */, sourceName, serverName);
        return getJdbcConnectionParametersForInstanceId(instanceId);
    }

    @Override
    public ConnectionParametersPlainFileBean addPlainFileConfig(String serverName) {
        Integer instanceId = execNamedQuerySingleValAsInteger("addPlainFileConfig", true, "id" /* i18n: no */, serverName);
        return getPlainFileConnectionParametersForInstanceId(instanceId);

    }

    private ConnectionParametersJdbcBean getJdbcConnectionParametersForInstanceId(Integer instanceId) {
        return createBeanFromNamedQry("getJdbcConfigForInstanceId", ConnectionParametersJdbcBean.class, false, instanceId);
    }

    private ConnectionParametersFileSystemBean
            getFileSystemConnectionParametersForInstanceId(Integer instanceId) {
        return createBeanFromNamedQry("getFileSystemConfigForInstanceId", ConnectionParametersFileSystemBean.class, false, instanceId);
    }

    private ConnectionParametersDynamicAxBean
            getDynamicAxConnectionParametersForInstanceId(Integer instanceId) {
        return createBeanFromNamedQry("getDynamicAxConfigForInstanceId", ConnectionParametersDynamicAxBean.class, false, instanceId);
    }

    protected List<ConnectionParametersJdbcBean> getAllJdbcConnectionParameters() {
        return createBeansFromNamedQry("getAllJdbcConfig", ConnectionParametersJdbcBean.class
        );
    }

    protected List<ConnectionParametersFileSystemBean> getFileSystemConnectionParameters() {
        return createBeansFromNamedQry("getFileSystemConfigs", ConnectionParametersFileSystemBean.class
        );
    }

    protected List<ConnectionParametersPlainFileBean> getPlainFileConnectionParameters() {
        return createBeansFromNamedQry("getPlainFileConfigs", ConnectionParametersPlainFileBean.class
        );
    }

    protected List<ConnectionParametersDynamicAxBean> getDynamicAxConnectionParameters() {
        return new ArrayList<ConnectionParametersDynamicAxBean>();
//        return createBeansFromNamedQry("getDynamicAxConfigs", ConnectionParametersDynamicAxBean.class);
    }

    @Override
    public void setJdbcConnectionParameters(ConnectionParametersJdbcBean bean) {
        execNamedCommand("setJdbcConfig", bean.id, bean.name, bean.jdbcUrl, bean.user, bean.password, bean.jdbcSourceId, bean.query, bean.queryJoined, bean.isActive, bean.folderPath, bean.adhocSql);
        setVisibleTree(bean.treeCode, (bean.isActive && BaseUtils.isStrEmptyOrWhiteSpace(bean.folderPath)));
    }

    @Override
    public void deleteJdbcConfig(String sourceName, int id, String serverName) {
        ConnectionParametersJdbcBean bean = getJdbcConnectionParametersForInstanceId(id);
        setVisibleTree(bean.treeCode, false);
        execNamedCommand("deleteDBConfig", "bik_jdbc", id, sourceName, serverName);
    }

    @Override
    public List<SystemUserGroupBean> getGroupRootObjects() {
        return createBeansFromNamedQry("getGroupRootObjects", SystemUserGroupBean.class
        );
    }

    @Override
    public List<SystemUserGroupBean> getGroupChildrenObjects(String objId, int id) {
        return createBeansFromNamedQry("getGroupChildrenObjects", SystemUserGroupBean.class, objId, id);
    }

    protected List<ConnectionParametersJdbcBean> getJdbcSourceTypes() {
        return createBeansFromNamedQry("getJdbcSourceTypes", ConnectionParametersJdbcBean.class
        );
    }

    @Override
    public ConnectionParametersFileSystemBean addFileSystemConfig(String serverName) {
        Integer instanceId = execNamedQuerySingleValAsInteger("addFileSystemConfig", true, "id" /* i18n: no */, serverName);
        return getFileSystemConnectionParametersForInstanceId(instanceId);
    }

    @Override
    public ConnectionParametersDynamicAxBean addDynamicAxConfig(String serverName) {
        Integer instanceId = execNamedQuerySingleValAsInteger("addDynamicAxConfig", true, "id" /* i18n: no */, serverName);
        return getDynamicAxConnectionParametersForInstanceId(instanceId);
    }

    @Override
    public void setFileSystemConnectionParameters(ConnectionParametersFileSystemBean bean) {
        if (bean.fileSince != null && !BaseUtils.isStrEmptyOrWhiteSpace(bean.fileSince)) {
            Date date = DateUtils.parse(new SimpleDateFormat("yyyy-MM-dd"), bean.fileSince);
            if (date == null) {
                throw new FoxyRuntimeException("Date is not valid");
            }
        }
        bean.path = bean.path.replace("\\", "/");
        execNamedCommand("setFileSystemConfig", bean);
        setVisibleTree(bean.treeCode, bean.isActive);
    }

    @Override
    public void deleteFileSystemConfig(String sourceName, int id, String serverName) {
        ConnectionParametersFileSystemBean bean = getFileSystemConnectionParametersForInstanceId(id);
        setVisibleTree(bean.treeCode, false);
        execNamedCommand("deleteDBConfig", "bik_file_system", id, sourceName, serverName);
    }

    @Override
    public void deletePlainFileConfig(String sourceName, int id, String serverName) {
        ConnectionParametersPlainFileBean bean = getPlainFileConnectionParametersForInstanceId(id);
        if (bean.treeId != null) {
            String treeCode = getTreeCodeById(bean.treeId);
            setVisibleTree(treeCode, false);
        }
        execNamedCommand("deleteDBConfig", "bik_plain_file", id, sourceName, serverName);
    }

    @Override
    public List<TreeNodeBean> getRootTreeNodes4ApplyForExtendedAccess(Set<String> treeCodes) {
        if (BaseUtils.isCollectionEmpty(treeCodes)) {
            return new ArrayList<TreeNodeBean>();

        }
        List<TreeNodeBean> tnb = createBeansFromNamedQry("getRootTreeNodes4ApplyForExtendedAccess", TreeNodeBean.class, treeCodes);
        translateSpecialTreeNodeBeans(tnb,
                false);
        return tnb;
    }

    @Override
    public void calculateAttributeUsages() {
//        getPumpExecutor().addPumpToJobExecutorQueue(createPumpListAndAdd(new AttributeUsagePump()));
        INamedSqlsDAO<Object> metadataPumpAdhocDao = getMetadataPumpAdhocDao();
        metadataPumpAdhocDao.execInAutoCommitMode(new IContinuation() {

            @Override
            public void doIt() {
                AttributeUsageCalculator attrPump = new AttributeUsageCalculator(getMetadataPumpAdhocDao());
                attrPump.startPump();
            }
        });
    }

    @Override
    public List<TreeNodeBean> getAttributeUsageTreeNodes() {
        Integer treeId = execNamedQuerySingleValAsInteger("getTreeId", false, "id", "AttributeUsage");
        return getOneSubtreeLevelOfTree(treeId, null, false, null, null, true, null, null, null, null);
        //        return createBeansFromNamedQry("getTreeNodesForTree", TreeNodeBean.class, null, "AttributeUsage");
    }

    @Override
    public List<AttributeValueInNodeBean> getAttributeUsingNodes(TreeNodeBean nodeBean) {
        List<String> tmp = new ArrayList<String>();
        if (nodeBean != null) {
            tmp = BaseUtils.splitBySep(nodeBean.objId, "|");
        }
        List<AttributeValueInNodeBean> usedInNodes = new ArrayList<AttributeValueInNodeBean>();
        if (tmp.size() > 2) {
            String categoryName = tmp.get(0);
            String attrName = tmp.get(1);
            String attrValue = tmp.get(2);
            List<AttributeValueInNodeBean> l = createBeansFromNamedQry("getUsingNodesByAttribute", AttributeValueInNodeBean.class, categoryName, attrName, attrValue);
            for (AttributeValueInNodeBean attrValueInNode : l) {
                List<String> values = BaseUtils.splitBySep(attrValueInNode.attrValue, ",");
                for (int i = 0; i < values.size(); i++) {
                    values.set(i, values.get(i).trim());
                }
                if (values.indexOf(attrValue) >= 0) {
                    usedInNodes.add(attrValueInNode);
                }
            }
        }
        return usedInNodes;
    }

    // upublicznienie dla interfejsu IBOXIServiceForEDDBPropsReader
    @Override
    public BOXIServiceImplData getBOXIServiceImplDataForCurrentRequest() {
        return super.getBOXIServiceImplDataForCurrentRequest();
    }

    @Override
    public void setCrrLabelsOnNode(Set<String> labels, int nodeId) {
        execNamedCommand("setCrrLabelsOnNode", BaseUtils.mergeWithSepEx(labels, ","), nodeId);
    }

    public Set<String> getCrrLabelCodes() {
        return execNamedQuerySingleColAsSet("getCrrLabels", "name");
    }

    public List<Map<String, Object>> getSearchResultsToExport(String searchRequestToken, List<String> exportOptions) {
        List<Map<String, Object>> searchResults = execNamedQueryCFN("getSearchResultsCached", null, searchRequestToken);
        Map<Integer, String> ancestorNames = null;
        Set<Integer> allNodeIds = new HashSet<Integer>();
        for (Map<String, Object> sr : searchResults) {
            BIKCenterUtils.splitBranchIdsIntoColl(BaseUtils.safeToString(sr.get("branch_ids")), true, allNodeIds);
        }

        Map<String, String> translatedAttrNames = getTranslationsGeneric(BIKConstants.TRANSLATION_KIND_ADEF).get(BIKConstants.TRANSLATION_KIND_ADEF);

        List<Map<String, Object>> content = new ArrayList<Map<String, Object>>();
        for (Map<String, Object> sr : searchResults) {
            Map<String, Object> item = new LinkedHashMap<String, Object>();
            if (exportOptions.indexOf(BIKConstants.EXPORT_OPTIONS_NAME) >= 0) {
                String nodeName = BaseUtils.safeToString(sr.get("name"));
                item.put(I18n.nazwa.get(), nodeName);
            }
            if (exportOptions.indexOf(BIKConstants.EXPORT_OPTIONS_NODE_KIND) >= 0) {
                item.put(I18n.typWezla.get(), sr.get("node_kind_caption"));
            }
            if (exportOptions.indexOf(BIKConstants.EXPORT_OPTIONS_DESCR) >= 0) {
                item.put(I18n.opis.get(), sr.get("descr_plain"));
            }
            if (exportOptions.indexOf(BIKConstants.EXPORT_OPTIONS_OBJ_PATH) >= 0) {
                if (ancestorNames == null) {
                    ancestorNames = allNodeIds.isEmpty() ? new HashMap<Integer, String>() : getNodeNamesByIds(allNodeIds);
                }
                String nodeName = BaseUtils.safeToString(sr.get("name"));
                ancestorNames.put(BaseUtils.tryParseInt(BaseUtils.safeToString(sr.get("node_id"))), nodeName);
                String branchIds = BaseUtils.safeToString(sr.get("branch_ids"));
                String objPath = BaseUtils.safeToString(sr.get("menu_path")) + " » " + BIKCenterUtils.buildAncestorPathByNodeNames(branchIds, ancestorNames, null);
                item.put(I18n.sciezkaDoObiektu.get(), objPath);
            }
            if (exportOptions.indexOf(BIKConstants.EXPORT_OPTIONS_EVALUATION) >= 0) {
                int voteCnt = BaseUtils.tryParseInt(BaseUtils.safeToString(sr.get("vote_cnt")));
                int voteSum = BaseUtils.tryParseInt(BaseUtils.safeToString(sr.get("vote_sum")));
                int allVoteCnt = voteCnt + voteSum;
                String voteSummary = (allVoteCnt != 0 ? ("" + voteSum + " " + I18n.z_noSingleLetter.get() /* I18N:  */ + " " + allVoteCnt) : "(" + I18n.brakOcen2.get() /* I18N:  */ + ")");
                item.put(I18n.oceny.get(), voteSummary);
            }
            if (exportOptions.indexOf(BIKConstants.EXPORT_OPTIONS_APPEARANCE) >= 0) {
//                List<String> attrNames = BaseUtils.splitBySep(BaseUtils.safeToString(sr.get("appearance")), ",");
//                Pair<String, String> attrListShortAndFull = BIKCenterUtils.getAttrListShortAndFull(attrNames);
                List<String> attrNames = BaseUtils.splitBySep(BaseUtils.safeToString(sr.get("appearance")), ",");
                StringBuilder appearance = new StringBuilder();
                for (String attrName : attrNames) {
                    appearance.append(BaseUtils.nullToDef(translatedAttrNames.get(attrName), attrName));
                }
                item.put(I18n.wystepowanie.get(), appearance.toString());
            }
            content.add(item);
        }
        return content;
    }

    @Override
    public List<SystemUserBean> getADUsers(String optfilter) {
        return createBeansFromNamedQry("getADUsers", SystemUserBean.class, optfilter);
    }

    public Integer getUserIdByLogin(String username) {
        return execNamedQuerySingleValAsInteger("getUserIdByLogin", true, null, username);
    }

    public List<ObjectInFvsChangeBean> getFavouritesForUser(Integer uid) {
        List<ObjectInFvsChangeBean> l = new ArrayList<ObjectInFvsChangeBean>();

        for (ObjectInFvsChangeBean o : createBeansFromNamedQry("getFavouritesForUser", ObjectInFvsChangeBean.class, uid)) {
            o.ancestorNodePath = getAncestorNodePath(o.branchIds);

            l.add(o);
        }
        return l;
    }

    public List<NewsBean> getNewsByUser(Integer uid) {
        List<NewsBean> l = createBeansFromNamedQry("getNewsByUser", NewsBean.class, uid);
        return l;
    }

    public List<NewsBean> getPublicNews() {
        return createBeansFromNamedQry("getPublicNews", NewsBean.class
        );
    }

    @PerformOnSingleBiksDb
    @Override
    public Map<Integer, Float> getOptSimilarNodes(int nodeId) {

        Map<Integer, Float> similarNodes = null;

        BOXIServiceImplData dataImpl = getBOXIServiceImplDataForCurrentRequest();

        if (!dataImpl.suggestSimilarResults) {
            return similarNodes;
        }

        final ISimilarNodesWizard similarNodesWizard = dataImpl.similarNodesWizard;

        int maxResults = getSuggestSimilarMaxResults();
        int dupBuff = BaseUtils.tryParseInteger(getAppPropValue("suggestSimilar.dupBuff"), 20);
        double minScore = BaseUtils.tryParseDouble(getAppPropValue("suggestSimilar.minScore"));

        similarNodes = similarNodesWizard.findSimilarNodeIds(nodeId, minScore, maxResults + dupBuff,
                new SearchWizardBucketManagerJoinedObjsTabKindEx(getDynamicTreeKindCodes()));
        if (similarNodes != null && similarNodes.isEmpty()) {
            similarNodes = null;
        }

        return similarNodes;
    }

    @Override
    public List<FvsChangeExBean> getFavouritesExForUser() {
        return createBeansFromNamedQry("getFavouritesExForUser", FvsChangeExBean.class, getLoggedUserId());
    }

//    @Override
//    public List<ObjectInFvsChangeExBean> getChangedDescendantsObjectInFvsEx(int nodeId, int userId) {
//
//        List<ObjectInFvsChangeExBean> changedDescendantsObjectInFvsEx = createBeansFromNamedQry("getChangedDescendantsObjectInFvsEx", ObjectInFvsChangeExBean.class, nodeId, userId);
//        for (ObjectInFvsChangeExBean oin : changedDescendantsObjectInFvsEx) {
//            if (!BaseUtils.isStrEmpty(oin.changedAttrs)) {
//                List<String> a = execNamedQuerySingleColAsList("getChangedAttrsNameInFvs", null, oin.changedAttrs);
//                oin.changedAttrs = BaseUtils.mergeWithSepEx(a, ", ");
//            }
//        }
//        return changedDescendantsObjectInFvsEx;
//    }
    // TF > jest to w klasie wyżej!
//    @Override
//    public ConnectionParametersBIAdminBean getBIAdminConnectionParameters() {
//        return readAdminBean("biadmin" /* I18N: no */, ConnectionParametersBIAdminBean.class);
//    }
    @Override
    public void setBIAdminConnectionParameters(ConnectionParametersBIAdminBean bean) {
        setAdminBean(bean, "biadmin" /* I18N: no */);
        if (bean.isActive == 1) {
            getBOXIServiceImplDataForCurrentRequest().biaProxyConnector = BIAProxy.makeProxy(new BIAConnector(bean.host, bean.user, bean.password));
        }
    }

    @Override
    public void setDroolsConfig(DroolsConfigBean bean) {
        bean.drlFolder = bean.drlFolder.replace("\\", "/");
        setAdminBean(bean, "drools");
    }

    @Override
    public List<SAPBOScheduleBean> getBIASchedules(SAPBOScheduleBean filter) {
        List<Integer> conList = filter.connectionsNodeId;
        if (!BaseUtils.isCollectionEmpty(conList)) {
            filter.reportsIdFromConnections = execNamedQuerySingleColAsList("getReportsJoinedConnections", "obj_id", filter.connectionsNodeId);
            if (BaseUtils.isCollectionEmpty(filter.reportsIdFromConnections)) {
                filter.reportsIdFromConnections.add("\'-1\'");
            }
        }
        List<SAPBOScheduleBean> schedules = getBIAProxyConnector().getBIASchedules(filter);

        if (BaseUtils.isCollectionEmpty(schedules)) {
            return schedules;
        }

        return getBIASchedulesOtherInfFromJoinObjs(schedules);
    }

    protected List<SAPBOScheduleBean> getBIASchedulesOtherInfFromJoinObjs(List<SAPBOScheduleBean> schedules) {

        List<String> objIds = new ArrayList<String>();
        for (SAPBOScheduleBean sb : schedules) {
            objIds.add(sb.parentId.toString());

        }

        List<SAPBOScheduleOtherInfFromJoinObjBean> scheduleEx = createBeansFromNamedQry("getBIASchedulesOtherInfFromJoinObjs", SAPBOScheduleOtherInfFromJoinObjBean.class, objIds);
        Map<String, SAPBOScheduleOtherInfFromJoinObjBean> res = new LinkedHashMap<String, SAPBOScheduleOtherInfFromJoinObjBean>();
        for (SAPBOScheduleOtherInfFromJoinObjBean sbe : scheduleEx) {
            SAPBOScheduleOtherInfFromJoinObjBean sb;
            if (res.containsKey(sbe.objId)) {
                sb = res.get(sbe.objId);
            } else {
                sb = new SAPBOScheduleOtherInfFromJoinObjBean();
//                sb.path = sbe.path;
                sb.modifyTimeReport = sbe.modifyTimeReport;
                sb.universes = new ArrayList<String>();
                sb.connections = new ArrayList<String>();
                sb.databaseEngines = new HashMap<String, String>();
                sb.modifyTimeUniverses = new HashMap<String, String>();
            }
            if (sbe.universe != null) {
                sb.universes.add(sbe.universe);
            }

            if (sbe.modifyTimeUniverse != null) {
                sb.modifyTimeUniverses.put(SimpleDateUtils.toCanonicalString(sbe.modifyTimeUniverse), " (" + sbe.universe + ")");
                sb.modifyTimeUniverse = sbe.modifyTimeUniverse;
            }

            if (sbe.connection != null) {
                sb.connections.add(sbe.connection);
            }
            if (sbe.databaseEngine != null) {
                sb.databaseEngines.put(sbe.databaseEngine, " (" + sbe.connection + ")");
                sb.databaseEngine = sbe.databaseEngine;
            }

            res.put(sbe.objId, sb);

        }

        for (SAPBOScheduleBean sb : schedules) {
            SAPBOScheduleOtherInfFromJoinObjBean s = res.get(sb.parentId.toString());
            if (s != null) {
//                sb.path = s.path;
//                sb.path = sb.location;
                sb.modifyTimeReport = s.modifyTimeReport;
                sb.modifyTimeUniverse = BaseUtils.isMapEmpty(s.modifyTimeUniverses) ? "" : (s.modifyTimeUniverses.size() == 1 ? SimpleDateUtils.toCanonicalString(s.modifyTimeUniverse) : BaseUtils.mapToString(s.modifyTimeUniverses, " ", ",<br/> ", false, false));
                sb.universes = BaseUtils.isCollectionEmpty(s.universes) ? "" : BaseUtils.mergeWithSepEx(s.universes, ",<br/> ");
                sb.connections = BaseUtils.isCollectionEmpty(s.connections) ? "" : BaseUtils.mergeWithSepEx(s.connections, ",<br/> ");
                sb.databaseEngine = BaseUtils.isMapEmpty(s.databaseEngines) ? "" : (s.databaseEngines.size() == 1 ? s.databaseEngine : BaseUtils.mapToString(s.databaseEngines, " ", ",<br/> ", false, false));
            }
        }
        return schedules;
    }

    @Override
    public List<SAPBOObjectBean> getBIAObjects(SAPBOObjectBean filter) {
//        List<SAPBOObjectBean> list =
        return getBIAProxyConnector().getBIAObjects(filter);
        // adds path and icon
//        if (!BaseUtils.isCollectionEmpty(list)) {
//            for (SAPBOObjectBean obj : list) {
//                obj.location = (String) execNamedQuerySingleVal("getBOObjLocation", true, null, String.valueOf(obj.id));
//            }
//        }
//        return list;
    }

    @Override
    public List<SAPBOObjectBean> getBIAUsers() {
        return getBIAProxyConnector().getBIAUsers();
    }

    @Override
    public void pauseBIAInstances(List<Integer> instancesToPause) {
        getBIAProxyConnector().pauseInstances(instancesToPause);
    }

    @Override
    public void resumeBIAInstances(List<Integer> instancesToResume) {
        getBIAProxyConnector().resumeInstances(instancesToResume);
    }

    @Override
    public void deleteBIAInstances(List<Integer> instancesToDelete) {
        getBIAProxyConnector().deleteInstances(instancesToDelete);
    }

    @Override
    public void rescheduleBIAInstances(List<Integer> instancesToReschedule, SAPBOScheduleBean params) {
        getBIAProxyConnector().rescheduleInstances(instancesToReschedule, params);
    }

    @Override
    public void changeBIANameAndDescr(Set<SAPBOObjectBean> objs, Integer manageType) {
        getBIAProxyConnector().changeBIANameAndDescr(objs, manageType);
    }

    @Override
    public void changeOwnerBIAObjects(List<Integer> objs, SAPBOObjectBean newOwner) {
        getBIAProxyConnector().changeBIAOwner(objs, newOwner);
    }

    @Override
    public void stopBIAQuery() {
        getBIAProxyConnector().cancelQuering();
    }

    @Override
    public ISimilarNodesWizard getSearchWizard() {
        return getBOXIServiceImplDataForCurrentRequest().similarNodesWizard;
    }

    protected boolean isSendEmailNotifications() {
        return BaseUtils.tryParseBoolean(getSensitiveAppPropValue("sendEmailNotifications"));
    }

    @Override
    public List<FvsChangeExBean> getSuggestedElementsForLoggedUser() {
        return createBeansFromNamedQry("getSuggestedElements", FvsChangeExBean.class, getLoggedUserId());

    }

    @Override
    public List<TreeNodeBean> getBikTreeInBIAdmin(BikNodeTreeOptMode extraMode) {
        List<TreeNodeBean> tnb;

        if (extraMode.equals(BikNodeTreeOptMode.SAPBOReportConnection)) {
            tnb = createBeansFromNamedQry("getBikConnectionTree", TreeNodeBean.class
            );
        } else {
            tnb = createBeansFromNamedQry("getBikReportsTree", TreeNodeBean.class
            );
        }
        translateSpecialTreeNodeBeans(tnb, true);
        return tnb;
    }

    @Override
    public void setBIAdminArchiveConfig(BIArchiveConfigBean bean) {
        bean.savedFolder = bean.savedFolder.replace('\\', '/');
        setAdminBean(bean, "biadmin.archive");
        if (/*bean.isActive != null &&*/bean.isActive == 1) {
            getBOXIServiceImplDataForCurrentRequest().biaArchive.schedule(getBIAdminConfig(), getBIAdminArchiveConfig());
        }
    }

    @Override
    public String runBIArchiveTestConnection() {
        BIArchiveConfigBean bean = getBIAdminArchiveConfig();
//        Date startDate = SimpleDateUtils.parseFromCanonicalString(bean.startDate + " " + bean.startHour);
//        if (startDate == null || BaseUtils.tryParseInteger(bean.limit) == null) {
//            return I18n.nieprawidlowyFormat.get();
//        }
//        if (BaseUtils.isStrEmptyOrWhiteSpace(bean.lcmCliPath)
//                || BaseUtils.isStrEmptyOrWhiteSpace(bean.jobCuid)
//                || BaseUtils.isStrEmptyOrWhiteSpace(bean.savedFolder)) {
//            return I18n.wszystkiePolaSaWymagane.get();
//        }
        if (bean.isRemoteSaving == 1) {
            String path = PumpFileUtils.fixPathIfNessecary(bean.savedFolder);
            try {
                SmbFile smbFile = new SmbFile(path, new NtlmPasswordAuthentication(bean.domain, bean.user, bean.pw));
                if (smbFile == null || !smbFile.isDirectory()) {
                    throw new FoxyRuntimeException("Target is not a folder.");
                }
            } catch (Exception ex) {
                throw new FoxyRuntimeException(ex.getMessage());
            }
        } else {
            File dir = new File(bean.savedFolder);
            if (!dir.exists()) {
                throw new FoxyRuntimeException("Target is not a folder.");
            }
        }
        if (bean.useGit == 1) {
            try {
                File localRepo = new File(bean.savedFolder);
                Git git = Git.open(localRepo);
                git.status().call();
//                Git git = Git.cloneRepository()
//                        .setURI(bean.gitRepository)
//                        .setDirectory(localRepo)
//                        .setCredentialsProvider(new UsernamePasswordCredentialsProvider(bean.gitUsername, bean.gitPw))
//                        .call();
                git.close();
                return I18n.sukces.get();
            } catch (Exception e) {
//                e.printStackTrace();
                throw new FoxyRuntimeException(e);
            }
        }
        return I18n.sukces.get();
    }

    @Override
    public Pair<Integer, List<BIAArchiveLogBean>> getBIAArchiveLogs(int pageNum, int pageSize) {
        Integer cnt = execNamedQuerySingleValAsInteger("getBIAArchiveLogsCnt", true, null);
        if (cnt == null) {
            cnt = 0;

        }
        List<BIAArchiveLogBean> logList = createBeansFromNamedQry("getBIAArchiveLogs", BIAArchiveLogBean.class, pageNum * pageSize + 1, (pageNum + 1) * pageSize);
        return new Pair<Integer, List<BIAArchiveLogBean>>(cnt, logList);
    }

    @Override
    public List<ObjectInFvsChangeBean> getSimilarFavouriteWithSimilarityScore(int nodeId) {
        SystemUserBean loggedUserBean = getLoggedUserBean();
        Map<Integer, Float> similarNodes = getOptSimilarNodes(nodeId);
        if (similarNodes == null || loggedUserBean == null) {
            return null;

        } else {
            List<ObjectInFvsChangeBean> similarFavouriteWithSimilarityScore = createBeansFromNamedQry("similarFavouriteWithSimilarityScore", ObjectInFvsChangeBean.class, similarNodes, loggedUserBean.id);
            for (ObjectInFvsChangeBean fvs : similarFavouriteWithSimilarityScore) {
                fvs.ancestorNodePath = execNamedQuerySingleColAsList("getNodeNamesByIds", "name" /* I18N: no */, BIKCenterUtils.splitBranchIdsEx(fvs.branchIds, false));
            }
            return similarFavouriteWithSimilarityScore;
        }
    }

    @Override
    public void setBIAuditDatabaseConnectionParameters(ConnectionParametersAuditBean bean) {
        setAdminBean(bean, "biadmin.audit" /* I18N: no */);
    }

    @Override
    public List<AuditBean> getBIAAuditInformation() {
        ConnectionParametersAuditBean conParameters = getBIAuditDatabaseConnectionParameters();
        if (conParameters.isActive == 0) {
            return new ArrayList<AuditBean>();

        }
        List<AuditBean> auditBeans = createBeansFromNamedQry("getBIAReportsAudit", AuditBean.class
        );
        SAPBOAuditConnector con = new SAPBOAuditConnector(getBIAuditDatabaseConnectionParameters());

        return con.getData(auditBeans);
    }

// TF: poniższe metody przeniesione z EntityDetailsDataBeanPropsReader - zapytanie trwało za długo. Teraz uruchamiamy je leniwie
    @Override
    public Pair<List<UserInNodeBean>, Map<Integer, String>> getUsersInNode(int nodeId, String branchIds) {
        TabWidgetsDataProvider dp = new TabWidgetsDataProvider(getAdhocDao(), this);
        return dp.getUsersInNode(nodeId, branchIds);
    }

    @Override
    public List<BlogBean> getBlogs(int nodeId, Integer linkingParentId) {
        TabWidgetsDataProvider dp = new TabWidgetsDataProvider(getAdhocDao(), this);
        return dp.getBlogs(nodeId, linkingParentId);
    }

    @Override
    public List<BlogBean> getUserBlogs(int nodeId) {
        TabWidgetsDataProvider dp = new TabWidgetsDataProvider(getAdhocDao(), this);
        return dp.getUserBlogs(nodeId);
    }

    @Override
    public List<AttachmentBean> getAttachments(int nodeId, Integer linkingParentId) {
        TabWidgetsDataProvider dp = new TabWidgetsDataProvider(getAdhocDao(), this);
        return dp.getAttachments(nodeId, linkingParentId);
    }

    @Override
    public List<NoteBean> getNotes(int nodeId) {
        TabWidgetsDataProvider dp = new TabWidgetsDataProvider(getAdhocDao(), this);
        return dp.getNotes(nodeId);
    }

    @Override
    public List<JoinedObjBean> getJoinedObjs(int nodeId, Integer linkingParentId) {
        TabWidgetsDataProvider dp = new TabWidgetsDataProvider(getAdhocDao(), this);
//        List<JoinedObjBean> res = dp.getJoinedObjs(nodeId, linkingParentId);
        List<JoinedObjBean> res = dp.getBikJoinedObjsInnerFromHyperlink(nodeId, linkingParentId);
        return res;
    }

    @Override
    public Pair<Integer, String> runBIAAuditTestConnection() {
        return new BIAAuditDatabaseTestConnection(getBIAuditDatabaseConnectionParameters()).testConnection();
    }

    @Override
    public String getBIAInstanceTreeCode(String typeName) {
        return execNamedQuerySingleVal("getBIAInstanceTreeCode", false, "code", typeName);
    }

    @Override
    public ConnectionParametersDQMConnectionsBean addDQMConnections(String name) {
        ConnectionParametersDQMConnectionsBean cp = new ConnectionParametersDQMConnectionsBean();
        cp.name = name;
        cp.id = sqlNumberToInt(execNamedQuerySingleVal("addDQMConnections", true, "id" /* I18N: no  */, name));

//        cp.id = execNamedCommand("addDQMConnections", name);
        cp.sourceType = PumpConstants.SOURCE_NAME_ORACLE;
        return cp;
    }

    @Override
    public void deleteDQMConnections(int id) {
        execNamedCommand("deleteDQMConnections", id);
    }

    @Override
    public void updateDQMConnections(ConnectionParametersDQMConnectionsBean cpDQM) {
        execNamedCommand("updateDQMConnections", cpDQM);
    }

    @Override
    public Pair<Integer, String> runDQMTestConnection(int id, String optObjsForGrantsTesting) {
        return new DQMConnectionsTestConnection(getMssqlConnLoadCfg(), id, optObjsForGrantsTesting).testConnection();
    }

    @Override
    public int addJoinedObjAttribute(String name, String type, String valueOptList, String valueOptTo, boolean usedInDrools) {
        return sqlNumberToInt(execNamedQuerySingleVal("addJoinedObjAttribute", true, "id" /* I18N: no  */, name, type, valueOptList, valueOptTo, usedInDrools));
//      return  execNamedCommand("addJoinedObjAttribute", name, type, valueOptList);
    }

    @Override
    public List<AttributeBean> getJoinedObjAttribute() {
        return createBeansFromNamedQry("getJoinedObjAttribute", AttributeBean.class
        );
    }

    @Override
    public void deleteJoinedObjAttribute(int attrId) {
        /* dodać usuwanie z powiązań */
        execNamedCommand("deleteJoinedObjAttribute", attrId);
    }

    public void updateJoinedObjAttribute(int itemId, String name, String type, String valueOptList, String valueOptTo, boolean usedInDrools) {
        execNamedCommand("updateJoinedObjAttribute", itemId, name, type, valueOptList, valueOptTo, usedInDrools);
    }

    public List<JoinedObjAttributeLinkedBean> getJoinedAttributesLinked(int joinedObjId) {
        return createBeansFromNamedQry("getJoinedAttributeLinked", JoinedObjAttributeLinkedBean.class, joinedObjId);
    }

    @Override
    public Pair<List<JoinedObjAttributeLinkedBean>, Map<String, AttributeBean>> getJoinedAttributesLinkedAndoinedObjAttributes(Integer joinedObjId) {
        List<AttributeBean> joinedObjAttribute = getJoinedObjAttribute();
        Map<String, AttributeBean> joinedObjAttributeMap = new HashMap<String, AttributeBean>();
        for (AttributeBean at : joinedObjAttribute) {
            joinedObjAttributeMap.put(at.atrName, at);
        }
        List<JoinedObjAttributeLinkedBean> joinedAttributeLinked = new ArrayList<JoinedObjAttributeLinkedBean>();

        if (joinedObjId != null) {
            joinedAttributeLinked = createBeansFromNamedQry("getJoinedAttributeLinked", JoinedObjAttributeLinkedBean.class, joinedObjId);
        }

        return new Pair<List<JoinedObjAttributeLinkedBean>, Map<String, AttributeBean>>(joinedAttributeLinked, joinedObjAttributeMap);
    }

    @Override
    public void addJoinedAttributeLinked(int joinedObjId, List<JoinedObjAttributeLinkedBean> joal) {
        execNamedCommand("addJoinedAttributeLinked", joinedObjId, joal);

    }

    private ConnectionParametersPlainFileBean getPlainFileConnectionParametersForInstanceId(Integer instanceId) {
        return createBeanFromNamedQry("getPlainFileConfigForInstanceId", ConnectionParametersPlainFileBean.class, false, instanceId);
    }

    public List<Map<String, Object>> getNodeInfoByAttribute(String attrName, String attrValue) {
        return execNamedQueryCFN("getNodeInfoByAttribute", FieldNameConversion.ToLowerCase, attrName, attrValue);
    }

    public void setPlainFileConnectionParameters(ConnectionParametersPlainFileBean bean) {
//        bean.sourceFilePath = bean.sourceFilePath.replace('\\', '/');
        bean.sourceFolderPath = bean.sourceFolderPath.replace('\\', '/');
        execNamedCommand("setPlainFileConfig", bean);
        if (bean.treeId != null) {
            setVisibleTree(getTreeCodeById(bean.treeId), bean.isActive);
        }
    }

    private String getTreeCodeById(Integer treeId) {
        return execNamedQuerySingleVal("getTreeCodeById", false, null, treeId);
    }

    private String getTreeNameById(Integer treeId) {
        return execNamedQuerySingleVal("getTreeNameById", false, null, treeId);
    }

    public List<Integer> getBindedTreeId(Integer treeId, List<String> branchIds) {
        return execNamedQuerySingleColAsList("getBindedTreeId", null, treeId, branchIds);
    }

    public List<Map<String, Object>> getRelatedNodeToExport(int treeId, List<String> branchIds, Integer bindedTreeId) {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        //dowiazania
        List<TreeNodeBean> linkedNodeList = createBeansFromNamedQry("getNodeWithLinkedNodeId", TreeNodeBean.class, treeId, branchIds, bindedTreeId);
        for (TreeNodeBean node : linkedNodeList) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put(PumpConstants.GENERIC_NODE_OBJID, execNamedQuerySingleVal("getObjIdByNodeNamePath", false, null, node.parentNodeId));
            map.put(PumpConstants.GENERIC_NODE_LINKED_OBJID, execNamedQuerySingleVal("getObjIdByNodeNamePath", false, null, node.linkedNodeId));
            map.put(PumpConstants.GENERIC_TYPE, PumpConstants.LINKING);

            breakObjId(map, PumpConstants.GENERIC_NODE_OBJID, PumpConstants.GENERIC_SRC_PARENT_OBJID, PumpConstants.GENERIC_SRC_NAME);
            breakObjId(map, PumpConstants.GENERIC_NODE_LINKED_OBJID, PumpConstants.GENERIC_DST_PARENT_OBJID, PumpConstants.GENERIC_DST_NAME);

            map.remove(PumpConstants.GENERIC_NODE_OBJID);
            map.remove(PumpConstants.GENERIC_NODE_LINKED_OBJID);
            list.add(map);
        }
        //powiazania
        List<JoinedObjBasicInfoBean> joinedNodeList = createBeansFromNamedQry("getAllJoinedObjs4Tree", JoinedObjBasicInfoBean.class, treeId, branchIds, bindedTreeId);
        for (JoinedObjBasicInfoBean node : joinedNodeList) {
            Map<String, Object> map = new HashMap<String, Object>();
            //src,dst
            map.put(PumpConstants.GENERIC_TYPE, PumpConstants.ASSOCIATION);

            map.put(PumpConstants.GENERIC_NODE_OBJID, execNamedQuerySingleVal("getObjIdByNodeNamePath", false, null, node.srcId));
            map.put(PumpConstants.GENERIC_NODE_JOINED_OBJID, execNamedQuerySingleVal("getObjIdByNodeNamePath", false, null, node.dstId));
            map.put(PumpConstants.GENERIC_IS_INHERIT, node.inheritToDescendants ? "1" : "0");
            Integer joinedObjId = execNamedQuerySingleValAsInteger("getJoinedObjIdBySrcAndDst", false, null, node.srcId, node.dstId);
            //atrybuty
            List<JoinedObjAttributeLinkedBean> attrs = getJoinedAttributesLinked(joinedObjId);
            for (JoinedObjAttributeLinkedBean att : attrs) {
                if (att.dstId == node.dstId) {
                    if (att.isMain) {
                        map.put(PumpConstants.GENERIC_MAIN_ATTRIBUTE, att.name);
                    }
                    map.put(att.name + PumpConstants.GENERIC_ATTRIBUTE_SRC_SUFFIX, att.value);
                    map.put(att.name + PumpConstants.GENERIC_ATTRIBUTE_DST_SUFFIX, att.valueTo);
                }
            }

            breakObjId(map, PumpConstants.GENERIC_NODE_OBJID, PumpConstants.GENERIC_SRC_PARENT_OBJID, PumpConstants.GENERIC_SRC_NAME);
            breakObjId(map, PumpConstants.GENERIC_NODE_JOINED_OBJID, PumpConstants.GENERIC_DST_PARENT_OBJID, PumpConstants.GENERIC_DST_NAME);
            map.remove(PumpConstants.GENERIC_NODE_OBJID);
            map.remove(PumpConstants.GENERIC_NODE_JOINED_OBJID);
            list.add(map);
        }

        List<Map<String, Object>> rs = execNamedQueryCFN("getAllHyperLinkIn", null, treeId, branchIds);
        for (Map<String, Object> record : rs) {
            Map<String, Object> map = new HashMap<String, Object>();
            Integer dstNodeId = (Integer) record.get("dst_node_id");

            Integer dstTreeId = execNamedQuerySingleValAsInteger("getTreeIdByNodeId", true, null, dstNodeId);
            if (dstTreeId != null && dstTreeId.intValue() == bindedTreeId.intValue()) {
                map.put(PumpConstants.GENERIC_NODE_OBJID, (String) record.get("src_obj_id"));
                map.put(PumpConstants.GENERIC_NODE_LINKED_OBJID, (String) record.get("dst_obj_id"));
                map.put(PumpConstants.GENERIC_TYPE, PumpConstants.HYPERLINK);
                map.put(PumpConstants.GENERIC_HYPER_LINK_IN_ATTR_NAME, (String) record.get("hyper_link_in_attr_name"));

                breakObjId(map, PumpConstants.GENERIC_NODE_OBJID, PumpConstants.GENERIC_SRC_PARENT_OBJID, PumpConstants.GENERIC_SRC_NAME);
                breakObjId(map, PumpConstants.GENERIC_NODE_LINKED_OBJID, PumpConstants.GENERIC_DST_PARENT_OBJID, PumpConstants.GENERIC_DST_NAME);

                map.remove(PumpConstants.GENERIC_NODE_OBJID);
                map.remove(PumpConstants.GENERIC_NODE_LINKED_OBJID);
                list.add(map);
            }
        }
        return list;
    }

//    protected void runWithArtificialAdminInNewThread(final IContinuation cont) {
//        final Integer loggedUserId = getLoggedUserId();
//        artificialAdminThread.execute(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    setAcceptAritificial(true);
//                    if (loggedUserId != null) {
//                        setLastLoggecUserId(loggedUserId);
//                    }
//                    getAdhocDao().execInAutoCommitMode(new IContinuation() {
//
//                        @Override
//                        public void doIt() {
//                            cont.doIt();
//                        }
//                    });
//                    getAdhocDao().finishWorkUnit(true);
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                    getAdhocDao().finishWorkUnit(false);
//                    throw new LameRuntimeException(ex.getMessage());
//                } finally {
//                    setAcceptAritificial(false);
//                    setLastLoggecUserId(-1);
//                }
//            }
//        });
//    }
    @Override
    public void setTreeImportStatus(int treeId, TreeImportLogStatus status) {
        execNamedCommand("setTreeImportStatus", treeId, status.getCode());
    }

//    protected void importTree(final Integer treeId, final String optServerTreeFileName) {
//        runWithArtificialAdminInNewThread(new IContinuation() {
//
//            @Override
//            public void doIt() {
//                try {
//                    if (optServerTreeFileName != null) {
//                        setTreeImportStatus(treeId, TreeImportLogStatus.RUNNING);
//                        add2TreeImportLog(treeId, "Import rozpoczęty");
//                        String treeCode = execNamedQuerySingleVal("getTreeCodeById", false, null, treeId);
//                        importCSVDataToTree(treeCode, optServerTreeFileName);
//                        add2TreeImportLog(treeId, "Zakończono z sukcesem");
//                        setTreeImportStatus(treeId, TreeImportLogStatus.SUCCESS);
//                    }
//                } catch (Exception e) {
////                    e.printStackTrace();
//                    add2TreeImportLog(treeId, "UWAGA: Błąd się pojawił, zobacz log serwera po więcej szczegółów. ");
//                    setTreeImportStatus(treeId, TreeImportLogStatus.ERROR);
//                } finally {
//                    deleteTmpFile(optServerTreeFileName);
//                }
//            }
//        });
//    }
    @Override
    public void deleteTmpFiles(Collection<String> tmpFileName2Delete) {
        for (String serverFileName : tmpFileName2Delete) {
            deleteTmpFile(serverFileName);
        }
    }

    private void importLinkedOrJoinedObj4Node(Integer treeId, Integer nodeId, String objIdStr, Integer bindedTreeId, Map<String, Integer> attrNames, Map<String, String> rowInfo) {
        //Dowiazanie
        String linkedObjIdStr = BaseUtils.trimLeftAndRight(rowInfo.get(PumpConstants.GENERIC_NODE_LINKED_OBJID));
        Integer linkedNodeId = execNamedQuerySingleValAsInteger("getNodeIdByNodeNamePath", true, null, bindedTreeId, linkedObjIdStr);

        if (linkedNodeId != null) {
            TreeNodeBean linkedNode = getNodeBeanById(linkedNodeId, true);
            add2TreeImportLog(treeId, "Dowiązania z " + objIdStr + " do " + linkedObjIdStr);

            createTreeNode(nodeId, linkedNode.nodeKindId, linkedNode.name, linkedObjIdStr, treeId, linkedNodeId);
//            execNamedCommand("importLinkedNodeInfo", nodeId, linkedNodeId);
        }

        //Powiazanie
        String dstObjIdStr = BaseUtils.trimLeftAndRight(rowInfo.get(PumpConstants.GENERIC_NODE_JOINED_OBJID));
        Integer dstId = execNamedQuerySingleValAsInteger("getNodeIdByNodeNamePath", true, null, bindedTreeId, dstObjIdStr);
        String mainAttribute = BaseUtils.trimLeftAndRight(rowInfo.get(PumpConstants.GENERIC_MAIN_ATTRIBUTE));
        if (dstId != null) {
            add2TreeImportLog(treeId, "Powiązanie pomiędzy " + objIdStr + " i " + dstObjIdStr);

            String isInheritStr = BaseUtils.trimLeftAndRight(rowInfo.get(PumpConstants.GENERIC_IS_INHERIT));
            Boolean isInherit = BaseUtils.tryParseInteger(isInheritStr, 0) > 0;
            addBikJoinedObject(nodeId, dstId, isInherit);
            Integer joinedObjId = execNamedQuerySingleValAsInteger("getJoinedObjIdBySrcAndDst", false, null, nodeId, dstId);

            //atrybuty
            List<JoinedObjAttributeLinkedBean> joal = new ArrayList<JoinedObjAttributeLinkedBean>();
            for (String attrName : attrNames.keySet()) {
                JoinedObjAttributeLinkedBean bean = new JoinedObjAttributeLinkedBean();
//                bean.id =
                bean.dstId = dstId;
                bean.name = attrName;
                bean.value = BaseUtils.trimLeftAndRight(rowInfo.get(attrName + PumpConstants.GENERIC_ATTRIBUTE_SRC_SUFFIX));
                bean.valueTo = BaseUtils.trimLeftAndRight(rowInfo.get(attrName + PumpConstants.GENERIC_ATTRIBUTE_DST_SUFFIX));
                bean.attributeId = attrNames.get(attrName);
                bean.isMain = attrName.equalsIgnoreCase(mainAttribute);
                if (!BaseUtils.isStrEmptyOrWhiteSpace(bean.value) || !BaseUtils.isStrEmptyOrWhiteSpace(bean.valueTo)) {
                    joal.add(bean);
                }
            }
            addJoinedAttributeLinked(joinedObjId, joal);
        }
    }

    private Map<String, Integer> extractJoinedAttrNames(List<String> attrColNames) {
        Map<String, Integer> attrNames = new HashMap<String, Integer>();
        attrColNames.remove(PumpConstants.GENERIC_NODE_OBJID);
        attrColNames.remove(PumpConstants.GENERIC_NODE_LINKED_OBJID);
        attrColNames.remove(PumpConstants.GENERIC_NODE_JOINED_OBJID);
        attrColNames.remove(PumpConstants.GENERIC_IS_INHERIT);
        attrColNames.remove(PumpConstants.GENERIC_MAIN_ATTRIBUTE);

        List<AttributeBean> allJoinedAttrs = getJoinedObjAttribute();
        for (AttributeBean attr : allJoinedAttrs) {
            attrNames.put(attr.atrName, attr.id);
        }
        for (String attrColName : attrColNames) {
            String attrName = null;
            if (attrColName.endsWith(PumpConstants.GENERIC_ATTRIBUTE_SRC_SUFFIX)) {
                attrName = attrColName.substring(0, attrColName.lastIndexOf(PumpConstants.GENERIC_ATTRIBUTE_SRC_SUFFIX));
            } else if (attrColName.endsWith(PumpConstants.GENERIC_ATTRIBUTE_DST_SUFFIX)) {
                attrName = attrColName.substring(0, attrColName.lastIndexOf(PumpConstants.GENERIC_ATTRIBUTE_DST_SUFFIX));
            }
            if (attrName != null && !attrNames.containsKey(attrName)) {
                attrNames.put(attrName, addJoinedObjAttribute(attrName, AttributeType.shortText.name(), "", "", false));
            }
        }
        return attrNames;
    }

    private void deleteUnnecessaryAttrValue(int treeId) {
        execNamedCommand("deleteUnnecessaryAttrValue", treeId);
    }

    @Override
    public String resetDrools() {
        DroolsConfigBean droolsConfig = getDroolsConfig();

        synchronized (this) {
            if (droolsConfig.isActive > 0) {
                String res = threadedDroolsEngine.tryCompile(droolsConfig);
                if ("OK".equals(res)) {
                    threadedDroolsEngine.schedule(droolsConfig);
                }
                return res;
            } else {
                return "Drools is not used";
            }
        }
    }

    public INamedSqlsDAO<Object> getAdhocDao() {
        return super.getAdhocDao();
    }

    //Tylko w Drools - eksperyment
    @Deprecated
    public void pump(String pumpCode, String pumpTreeCode, String objId) {
        try {
            if (PumpConstants.SOURCE_NAME_PLAIN_FILE.equalsIgnoreCase(pumpCode)) {
                ConfigPlainFilePumpBean config = readConfigFromTreePumpConfig(ConfigPlainFilePumpBean.class, pumpCode, pumpTreeCode, objId);
                if (config.isActive) {
                    BikPlainFilePump pump = new BikPlainFilePump(this, config);
                    pump.startPump();
                }
            } else {
                System.out.println("Nie poprawny pumpCode: " + pumpCode);
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
            throw new LameRuntimeException("Exception in pump " + ex.getMessage());
        }
    }

    public List<AttributeBean> getAttributes(Integer nodeId) {
        List<AttributeBean> res = getAttributesForNode(nodeId, null);
        List<AttributeBean> metaAttrs = createBeansFromNamedQry("getLoadedAttributes", AttributeBean.class, nodeId);
        res.addAll(metaAttrs);
        return res;
    }

    public List<AttributeBean> getAttributesForNode(int nodeId, List<String> attrToFIlter) {
        return createBeansFromNamedQry("bikEntityAttribute", AttributeBean.class, nodeId, attrToFIlter);
    }

    public List<AttributeBean> getAttributesByNameValueNode(String name, String value, Integer nodeId) {
        List<AttributeBean> attrs = createBeansFromNamedQry("getAttributesNameValueNode", AttributeBean.class, name, value, nodeId);
        return attrs;
    }

    public List<AttributeBean> getAttributesDef(boolean withMetaModel, String name) {
        List<AttributeBean> attrs = createBeansFromNamedQry("getAttributesDef", AttributeBean.class, withMetaModel, name);
        return attrs;
    }

//    =======
//    public List<AttributeBean> getAttributesbyNameValueNode(String name, String value, Integer nodeId) {
//        List<AttributeBean> attrs = createBeansFromNamedQry("getAttributesNameValueNode", AttributeBean.class, name, value, nodeId);
//        return attrs;
//    }
//
//    public List<AttributeBean> getAttributesDef(boolean withMetaModel, String name) {
//        List<AttributeBean> attrs = createBeansFromNamedQry("getAttributesDef", AttributeBean.class, withMetaModel, name);
//        return attrs;
//    }
//    >>>>>>> .r10382
    public Integer getNodeIdWithAutoObjId(String treeCode, String objId) {
        return execNamedQuerySingleVal("getNodeIdWithAutoObjId", true, null, BaseUtils.ensureObjIdEndWith(objId, "|"), treeCode);
    }

    protected <T> T readConfigFromTreePumpConfig(Class<T> beanClass, String pumpCode, String pumpTreeCode, String objId) throws InstantiationException, IllegalAccessException {
        Integer nodeId = getNodeIdWithAutoObjId(pumpTreeCode, objId);
        if (nodeId == null) {
            throw new LameRuntimeException("Brak konfiguracji: " + objId);
        }
        String attrPrefix = pumpCode.replaceAll(" ", "");
        Map<String, Object> m = new HashMap<String, Object>();

        for (Field field : beanClass.getFields()) {
            String attrName = attrPrefix + "@" + field.getName();
            String value = execNamedQuerySingleVal("getNodeAttribute", true, null, nodeId, attrName);
            if (value != null) {
                m.put(field.getName(), value);
            }
        }
        return BeanMaker.createOneBean(m, beanClass);
    }

    @Override
    public Pair<Integer, List<DroolsLogBean>> getDroolsLogs(int pageNum, int pageSize) {
        Integer cnt = execNamedQuerySingleValAsInteger("getDroolsLogsCnt", true, null);
        if (cnt == null) {
            cnt = 0;

        }
        List<DroolsLogBean> logList = createBeansFromNamedQry("getDroolsLogs", DroolsLogBean.class, pageNum * pageSize + 1, (pageNum + 1) * pageSize);
        return new Pair<Integer, List<DroolsLogBean>>(cnt, logList);
    }

    public void setLastChangedId(Integer lastChangedId) {
        this.lastChangedId.set(lastChangedId);
    }

    public void setLastCheckedChangeId(Integer lastCheckedChangeId) {
        this.lastCheckedChangeId.set(lastCheckedChangeId);
    }

    public List<NodeHistoryChangeBean> getNodeAttributeChangeList(Integer nodeId, String attrName) {
        return createBeansFromNamedQry("getNodeAttributeChangeList", NodeHistoryChangeBean.class, nodeId, attrName, lastChangedId.get(), lastCheckedChangeId.get());
    }

    @Override
    public boolean checkTreeAttributesCompatibilitiy(String treeKind, String serverTreeFileName) {
        return true;
    }

    public String runCmd(String cmd) {
        try {
            Runtime rt = Runtime.getRuntime();
            cmd = "cmd /c " + cmd;
            Process proc = rt.exec(cmd);
            BufferedReader input = new BufferedReader(new InputStreamReader(proc.getInputStream()));

            String line = null;
            StringBuilder log = new StringBuilder();
            while ((line = input.readLine()) != null) {
                log.append("\n").append(line);
//                System.out.println(line);
            }
//            System.out.println((System.currentTimeMillis() - startTime.getTime()) / 1000.0);
            proc.waitFor();
            proc.destroy();
            return log.toString();
        } catch (Throwable ex) {
            ex.printStackTrace();
            throw new LameRuntimeException("Error in rumCmd ");
        }
    }

    @Override
    public boolean checkJoinedObjsAttributesCompatibilitiy(Map<Integer, String> bindingTreeIdWithFile) {
        return true;
    }

    @Override
    public void importLinksAndJoinedObjs2Tree(final Integer treeId, final Map<Integer, String> bindingTreeIdWithFile) {
//        runWithArtificialAdminInNewThread(new IContinuation() {
//
//            @Override
//            public void doIt() {
//                try {
////                    calculateObjIds(treeId, bindingTreeIdWithFile.keySet());
//                    for (Entry<Integer, String> e : bindingTreeIdWithFile.entrySet()) {
//                        add2TreeImportLog(treeId, "Importowanie dowiązań/powiązań z drzewem " + getTreeCodeById(e.getKey()));
//                        Integer bindedTreeId = e.getKey();
//                        String serverBindingFileName = e.getValue();
//                        List<Map<String, String>> nodeInfoList = null;
//                        String path = BaseUtils.ensureStrHasSuffix(dirForUpload.replace("/", "\\"), "\\", true) + serverBindingFileName;
//                        String csvContent;
//                        try {
//                            csvContent = LameUtils.loadAsString(new FileInputStream(path), "*Cp1250,ISO-8859-2,UTF-8");
//                        } catch (FileNotFoundException ex) {
//                            throw new LameRuntimeException(ex.getMessage());
//                        }
//                        CSVReader reader = new CSVReader(csvContent, CSVReader.CSVParseMode.CONSIDER_QUOTES, ';', true);
//
//                        nodeInfoList = reader.readFullTableAsMaps();
//                        Map<String, Integer> attrNames = extractJoinedAttrNames(reader.getColNames());
//                        for (Map<String, String> map : nodeInfoList) {
//                            String objIdStr = BaseUtils.trimLeftAndRight(map.get(PumpConstants.GENERIC_NODE_OBJID));
//                            Integer nodeId = execNamedQuerySingleValAsInteger("getNodeIdByNodeNamePath", true, null, treeId, objIdStr);
//                            if (nodeId != null) {
//                                importLinkedOrJoinedObj4Node(treeId, nodeId, objIdStr, bindedTreeId, attrNames, map);
//                            }
//                        }
//                    }
//                } finally {
//                    deleteTmpFiles(bindingTreeIdWithFile.values());
//                    add2TreeImportLog(treeId, "Zakończono");
//                }
//            }
//        });
    }

    @Override
    public void calculateObjIds(Integer treeId, Set<Integer> allTreeIds) {
        add2TreeImportLog(treeId, "Ponownie wyliczyć ścieżki obiektów");
        execNamedCommand("calculateObjPaths4Trees", allTreeIds);
    }

    private void add2TreeImportLog(final Integer treeId, final String msg) {
        getAdhocDao().execInAutoCommitMode(new IContinuation() {

            @Override
            public void doIt() {
                execNamedCommand("add2TreeImportLog", treeId, msg);
            }
        });
    }

    @Override
    public List<String> getImportTreeLog(Integer treeId) {
        List<String> res = new ArrayList<String>();

        List<Map<String, Object>> tmp = execNamedQueryCFN("getImportTreeLog", FieldNameConversion.ToLowerCase, treeId);
        for (Map<String, Object> record : tmp) {
            String s = BaseUtils.safeToString(record.get("start_time")) + ": ";
            s += BaseUtils.safeToString(record.get("description"));
            res.add(s);
        }
        return res;
    }

    @SuppressWarnings("unchecked")
    public void generateDoc4Nodes(int numbering, List<Integer> ids, String pref, int level, int maxDepth, IParametrizedContinuation onGetContentSuccess) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cnt = 0;
        for (Integer nodeId : ids) {
            cnt++;
            TreeNodeBean node = getNodeBeanById(nodeId, false);
//            sb = BaseUtils.appendNTimes(sb, "\t", level);
//            sb = sb.append("# ").append(pref + cnt + ".").append(" ");
            sb = sb.append("<h2>").append(numbering == 0 ? "" : pref + cnt + ". ").append(node.name).append("</h2>").append("\n");
            String descr = node.descr;
            if (BaseUtils.isStrEmptyOrWhiteSpace(descr)) {
                descr = node.descrPlain;
            }
            descr = parseSpecialTags(nodeId, descr, false);
            if (!BaseUtils.isStrEmptyOrWhiteSpace(descr)) {
//                sb.append("<b>Opis:</b>\n");
                sb.append(descr).append("\n<br/>");

            }
            List<AttributeBean> attrs = createBeansFromNamedQry("getAttributeValueOfNode", AttributeBean.class, node.linkedNodeId != null ? node.linkedNodeId : node.id);
            if (!BaseUtils.isCollectionEmpty(attrs)) {
//                sb.append("<b>Atrybuty:</b>\n").append("<br/>\n");
                sb.append("<ul>\n");
                for (AttributeBean attr : attrs) {
                    String s = "";
                    if (attr.typeAttr.equals(AttributeType.hyperlinkInMulti.name()) || attr.typeAttr.equals(AttributeType.hyperlinkInMono.name())
                            || attr.typeAttr.equals(AttributeType.permissions.name())) {
                        List<String> values = BaseUtils.splitBySep(attr.valueOpt, "|");
                        s = s + "\n<br/>";
                        for (String v : values) {
                            int p = v.indexOf(",");
                            if (p >= 0) {
                                s += "&nbsp;&nbsp;  " + (v.substring(p + 1) + "\n<br/>");
                            }
                        }
                        if (s.endsWith(",")) {
                            s = s.substring(0, s.length() - 1);
                        }
                    } else {
                        s = BaseUtils.safeToStringNotNull(attr.valueOpt);
                    }
                    String attrName = attr.atrName.indexOf('.') < 0 ? attr.atrName : attr.atrName.split("\\.")[1];
                    sb.append("<li><b>").append(attrName).append(":</b> ").append(s).append("</li>\n");
                }
                sb.append("</ul>\n");
            }

            //powiazania
            Map<String, List<String>> joinedAttrsMap = new HashMap<String, List<String>>();
            List<String> withoutAttrs = new ArrayList<String>();
            List<JoinedObjBasicInfoBean> joinedNodeList = createBeansFromNamedQry("getJoinedObjs4Node", JoinedObjBasicInfoBean.class, node.id);
            for (JoinedObjBasicInfoBean obj : joinedNodeList) {
                Integer joinedObjId = execNamedQuerySingleValAsInteger("getJoinedObjIdBySrcAndDst", false, null, obj.srcId, obj.dstId);
                //atrybuty
                List<JoinedObjAttributeLinkedBean> joinedAttrs = getJoinedAttributesLinked(joinedObjId);
                if (BaseUtils.isCollectionEmpty(joinedAttrs)) {
                    withoutAttrs.add(obj.dstName);
                } else {
                    for (JoinedObjAttributeLinkedBean joinedAttr : joinedAttrs) {
                        if (joinedAttr.dstId == obj.dstId) {
                            List<String> l = joinedAttrsMap.get(obj.dstName);
                            if (l == null) {
                                l = new ArrayList<String>();
                            }
                            StringBuilder s = new StringBuilder();
                            if (joinedAttr.isMain) {
                                s.append("<b>");
                            }
                            s.append(joinedAttr.value).append(" (").append(joinedAttr.name).append(")\n");
                            if (joinedAttr.isMain) {
                                s.append("</b>");
                            }
                            l.add(s.toString());
                            joinedAttrsMap.put(obj.dstName, l);
                        }
                    }
                }
            }

            if (!BaseUtils.isCollectionEmpty(withoutAttrs)
                    || joinedAttrsMap.size() > 0) {
                sb.append("<b>Powiązane z:</b>\n").append("<br/>\n");
            }

            if (!BaseUtils.isCollectionEmpty(withoutAttrs)) {
                sb.append("<ul>\n");
                for (String s : withoutAttrs) {
                    sb.append("<li>").append(s).append("</li>\n");
                }
                sb.append("</ul>\n");
            }

            if (joinedAttrsMap.size()
                    > 0) {
//                sb.append("<b>Powiązania z atrybutami:</b>\n").append("<br/>");
                sb.append("<ul>\n");
                for (Entry<String, List<String>> e : joinedAttrsMap.entrySet()) {
//                    sb.append("<li>\n");
                    sb.append("<ul>\n");
                    for (String p : e.getValue()) {
                        sb.append("<li>").append(e.getKey() + ": ").append(p).append("</li>\n");
                    }
//                    sb.append("---" + e.getKey() + "---").append("\n");
                    sb.append("</ul>\n");
//                    sb.append("</li>\n");
                }
                sb.append("</ul>");
            }

//            if (!BaseUtils.isStrEmptyOrWhiteSpace(descr)) {
//                sb = sb.append(Html2Md.convertHtml(descr, "UTF-8")).append("\n");
//            }
            //Osoby
            Pair<List<UserInNodeBean>, Map<Integer, String>> usersInNode = getUsersInNode(nodeId, node.branchIds);

            if (!BaseUtils.isCollectionEmpty(usersInNode.v1)) {
//                sb.append("<b>Osoby:</b>\n").append("<br/>\n");
                sb.append("<ul>\n");
                for (UserInNodeBean bean : usersInNode.v1) {
                    List<Integer> nodeIdsFromBranch = BIKCenterUtils.splitBranchIdsEx(bean.userInRoleBranchIds, false);
                    List<String> nodeNames = new ArrayList<String>();
                    for (Integer id : nodeIdsFromBranch) {
                        if (bean.userInRoleNodeId != id) {
                            String nodeName = execNamedQuerySingleVal("nodeNamesById", true, null, id);
                            nodeNames.add(nodeName);
                        }
                    }
                    String pathStr = BaseUtils.mergeWithSepEx(nodeNames, BIKGWTConstants.RAQUO_STR_SPACED);
                    String role = pathStr + (bean.isAuxiliary ? " (" + I18n.pomocniczy.get() + ")" : "") // " (" + (!b.isAuxiliary ? I18n.glowny.get() /* I18N:  */ : I18n.pomocniczy.get() /* I18N:  */) + ")"
                            + (bean.inheritToDescendants /*== 0*/ ? " *" : "");

                    sb.append("<li>").append(bean.userName).append(" - ").append(role).append("</li>\n");
                }
                sb.append("</ul>\n");
            }

            onGetContentSuccess.doIt(sb.toString());
            sb.setLength(0);
            getNodesDFS(numbering, numbering == 0 ? "" : pref + cnt + ".", level + 1, maxDepth, node.linkedNodeId == null ? nodeId : node.linkedNodeId, onGetContentSuccess);
        }
    }

    protected String generateTitlePage(String title) {
        StringBuilder sb = new StringBuilder();
//        String treeName = execNamedQuerySingleVal("getTreeNameById", false, null, treeId);
        sb.append("<div style='height:1000px'><div style='margin-top:200px; text-align:center; height:70%;'><h1>").append(title).append("</h1></div>");
        sb.append("<div style='vartical-align:text-bottom; text-align:center;'><h3>").append(getLoggedUserName()).append("</h3>").append("BIKS - " + DateUtils.dateTimeFormat(new Date(), "yyyy-MM-dd hh:mm:ss")).append("</div></div>");

        return sb.toString();
    }

    public void createDocument4TreeProducer(int treeId, List<Integer> nodeIds2Export, int maxDepth, int numbering, IParametrizedContinuation onGetContentSuccess) throws IOException {
        if (maxDepth <= 0) {
            return;
        }
//        List<Integer> ids = execNamedQuerySingleColAsList("getFirstRankNodeIdsOfTree", null, treeId);
        String treeName = execNamedQuerySingleVal("getTreeNameById", false, null, treeId);
        onGetContentSuccess.doIt(generateTitlePage(treeName));
//        generateDoc4Nodes(numbering, ids, "", 0, maxDepth, onGetContentSuccess);
        nodeIds2Export = changeLinkedNodeIds2Original(nodeIds2Export);
        generateDoc4Nodes(numbering, nodeIds2Export, "", 0, maxDepth, onGetContentSuccess);
        //        StringBuilder sb = new StringBuilder();
        //        String pref = "";
        //        int cnt = 0;
        //        for (Integer nodeId : ids) {
        //            cnt++;
        //            TreeNodeBean node = getNodeBeanById(nodeId, false);
        //            sb = sb.append("# ").append(pref + cnt + ".").append(node.name).append("\n");
        //            String descr = node.descr;
        //            if (BaseUtils.isStrEmptyOrWhiteSpace(descr)) {
        //                descr = node.descrPlain;
        //            }
        //            if (!BaseUtils.isStrEmptyOrWhiteSpace(descr)) {
        //                sb.append(Html2Md.convertHtml(descr, "UTF-8")).append("\n");
        //            }
        //            onGetContentSuccess.doIt(sb.toString());
        //            sb.setLength(0);
        //            getNodesDFS(pref + cnt + ".", 1, maxDepth, node.linkedNodeId == null ? nodeId : node.linkedNodeId, onGetContentSuccess);
        //        }
    }

    protected void getNodesDFS(int numbering, String pref, int level, int maxDepth, int parentNodeId, IParametrizedContinuation onGetContentSuccess) throws IOException {
        if (level >= maxDepth) {
            return;
        }
        List<Integer> ids = execNamedQuerySingleColAsList("getChildNodeIdsOfTree", "node_id", parentNodeId);
        generateDoc4Nodes(numbering, ids, pref, level, maxDepth, onGetContentSuccess);
    }

    protected void getNodesDFS2(int numbering, String pref, int level, int maxDepth, int parentNodeId, boolean showNewLine, boolean showAuthor, boolean isPDFFormat, IParametrizedContinuation onGetContentSuccess) throws IOException {
        if (level >= maxDepth) {
            return;
        }
        List<TreeNodeBean> ids = createBeansFromNamedQry("getChildNodeIdsOfTreePrint", TreeNodeBean.class, parentNodeId
        );

        if (BaseUtils.isCollectionEmpty(ids)) {
            return;
        }
        generateDoc4NodesPrint(numbering, ids, pref, level, maxDepth, showNewLine, showAuthor, isPDFFormat, onGetContentSuccess);
    }

    public void setDqmService(DQMServiceImpl dqmService) {
        this.dqmService = dqmService;
    }

    public void updateMetaIconAttribute() {
        execNamedCommand("updateMetaIconAttribute");
    }

    public DQMServiceImpl getDQMService() {
        return dqmService;
    }

    public Integer addSystemUserGroup(String name, Integer parentId) {
        String safeName = cleanHtml(name);
        return execNamedQuerySingleVal("addSystemUserGroup", true, "id" /* I18N: no */, safeName, parentId);
    }

    public Integer addSystemRole(String name, String desc, Integer parentId) {
        String safeName = cleanHtml(name);
        return execNamedQuerySingleVal("addSystemRole", true, "id" /* I18N: no */, safeName, desc, parentId);
    }

    public void addSystemUserToGroup(String groupName, List<Integer> userIds) {
        execNamedCommand("addSystemUserToGroup", groupName, userIds);
    }

    public List<String> saveTree2Disk(int treeId, List<Integer> nodeIds2Export) {
        try {
            List<String> listFiles = new ArrayList<String>();
            TreeExportMetadata metadata = new TreeExportMetadata();
            List<String> branchIds = new ArrayList<String>();
            if (BaseUtils.isCollectionEmpty(nodeIds2Export)) {
                branchIds.add("");
            } else {
                branchIds = execNamedQuerySingleColAsList("getSelectedBrandIds", null, nodeIds2Export);
            }

            List<Map<String, Object>> treeContent = getTreeToExport(treeId, branchIds);

            String tmpPath = BaseUtils.ensureDirSepPostfix(dirForUpload) + "tmp/exportTree/" + metadata.treeCode + "/";

            listFiles.add(updateBaseMetadata(metadata, treeId, treeContent, tmpPath));
            listFiles.addAll(updateRelatedMetadata(metadata, treeId, branchIds, tmpPath));
            listFiles.add(updateAdditionalInfo(metadata, treeId, tmpPath));

            ObjectMapper jsonMapper = new ObjectMapper();
            jsonMapper.enable(SerializationFeature.INDENT_OUTPUT);
            List<TreeExportMetadata> l = new ArrayList<TreeExportMetadata>();
            l.add(metadata);
            jsonMapper.writeValue(new FileOutputStream(tmpPath + metadata.treeCode + ".metadata"), l);
            listFiles.add(tmpPath + metadata.treeCode + ".metadata");
            return listFiles;
        } catch (Exception ex) {
            throw new LameRuntimeException(ex);
        }
    }

    private void write2Csv(List<Map<String, Object>> content, String folderName, String fileName) throws IOException {
        write2Csv(content, folderName, fileName, null);
    }

    private int breakTreeObjId(List<Map<String, Object>> content, String field2Break, String parentColNamePref, String additionalNameCol) {
        int maxDepth = -1;
        for (Map<String, Object> node : content) {
            int depth = breakObjId(node, field2Break, parentColNamePref, additionalNameCol);
            if (maxDepth < depth) {
                maxDepth = depth;
            }
        }
        for (Map<String, Object> node : content) {
            node.remove(field2Break);
        }
        return maxDepth;
    }

    private String updateBaseMetadata(TreeExportMetadata metadata, int treeId, List<Map<String, Object>> treeContent, String dstFolder) throws IOException {
        metadata.treeCode = getTreeCodeById(treeId);
        metadata.baseFileName = LameUtils.sanitizeFileName(getTreeNameById(treeId) + ".csv");

        Set<String> attrColNames = new HashSet<String>();
        for (Map<String, Object> node : treeContent) {
            for (String attrName : node.keySet()) {
                attrColNames.add(attrName);
            }
        }
        attrColNames.remove(PumpConstants.GENERIC_NODE_OBJID);
        attrColNames.remove(PumpConstants.GENERIC_NODE_PARENT_OBJID);
        attrColNames.remove(PumpConstants.GENERIC_NODE_NAME);
        attrColNames.remove(PumpConstants.GENERIC_NODE_NODE_KIND_CODE);
        attrColNames.remove(PumpConstants.GENERIC_NODE_DESCR);
        attrColNames.remove(PumpConstants.GENERIC_NODE_VISUAL_ORDER);

        int depth = breakTreeObjId(treeContent, PumpConstants.GENERIC_NODE_PARENT_OBJID, PumpConstants.GENERIC_NODE_PARENT_OBJID, null);
        metadata.parentObjIds = new ArrayList<String>();
        for (int i = 0; i < depth - 1; i++) {
            metadata.parentObjIds.add(PumpConstants.GENERIC_NODE_PARENT_OBJID + i);
        }
        for (Map<String, Object> map : treeContent) {
            map.remove(PumpConstants.GENERIC_NODE_OBJID);
        }
        metadata.name = PumpConstants.GENERIC_NODE_NAME;
        metadata.nodeKindCode = PumpConstants.GENERIC_NODE_NODE_KIND_CODE;
        metadata.desc = PumpConstants.GENERIC_NODE_DESCR;
        metadata.visualOrder = PumpConstants.GENERIC_NODE_VISUAL_ORDER;
        metadata.attributes = new HashMap<String, String>();
        for (String attrName : attrColNames) {
            metadata.attributes.put(attrName, attrName);
        }
        List<String> headerList = new ArrayList<String>();
        headerList.add(metadata.name);
        headerList.addAll(metadata.parentObjIds);
        headerList.add(metadata.nodeKindCode);
        headerList.add(metadata.desc);
        headerList.add(metadata.visualOrder);
        headerList.addAll(metadata.attributes.keySet());

        write2Csv(treeContent, dstFolder, metadata.baseFileName, headerList);
        return dstFolder + metadata.baseFileName;
    }

    private List<String> updateRelatedMetadata(TreeExportMetadata metadata, int treeId, List<String> branchIds, String dstFolder) throws IOException {
        metadata.relatedFiles = new ArrayList<TreeExportRelatedFileMetaData>();

        List<String> listFiles = new ArrayList<String>();
        String treeName = getTreeNameById(treeId);
        //powiazania-dowiazania-linkWew
        List<Integer> bindedTreeIds = getBindedTreeId(treeId, branchIds);
        for (Integer bindedTreeId : bindedTreeIds) {
            TreeExportRelatedFileMetaData info = new TreeExportRelatedFileMetaData();
            info.relatedTreeCode = getTreeCodeById(bindedTreeId);
            info.relatedFileName = LameUtils.sanitizeFileName(treeName + "_" + getTreeNameById(bindedTreeId) + ".csv");
            metadata.relatedFiles.add(info);
//                metadata.put("@" + bindedTreeCode, fileName);
            listFiles.add(dstFolder + info.relatedFileName);
            List<String> headerList = preapreHeaderList4RelatedTree2Export(treeId, branchIds, bindedTreeId);
            List<Map<String, Object>> content = getRelatedNodeToExport(treeId, branchIds, bindedTreeId);
            Set<String> mandatorySet = new HashSet<String>(headerList);
            for (Map<String, Object> map : content) {
                for (String colName : map.keySet()) {
                    boolean contained = false;
                    for (String key : mandatorySet) {
                        if (key.equalsIgnoreCase(colName)) {
                            contained = true;
                            break;
                        }
                    }
                    if (!contained) {
                        mandatorySet.add(colName);
                    }
                }
            }
            mandatorySet.removeAll(headerList);
            headerList.addAll(mandatorySet);
            write2Csv(content, dstFolder, info.relatedFileName, headerList);
        }
        return listFiles;
    }

    @Override
    public Integer renameUserGroup(SystemUserGroupBean sub) {
        String safeName = cleanHtml(sub.name);
        return execNamedQuerySingleVal("renameUserGroup", true, "id" /* I18N: no */, sub.id, safeName);
    }

    @Override
    public void deleteUserGroup(int id) {
        execNamedCommand("deleteUserGroup", id);
    }

    @Override
    public List<Integer> getUsersGroups(int userId) {
        return execNamedQuerySingleColAsList("getUsersGroups", "id", userId);
    }

    @Override
    public void copyPermissionFromUser(int userId, int groupId) {
        execNamedCommand("copyPermissionFromUser", userId, groupId);
    }

    @Override
    public List<TreeBean> getImportableTrees() {
        return createBeansFromNamedQry("getImportableTrees", TreeBean.class
        );
    }

    private void write2Csv(List<Map<String, Object>> content, String folderName, String fileName, List<String> headerList) throws IOException {
        File folder = new File(folderName);
        if (!folder.exists()) {
            folder.mkdirs();
        }
        CSVWriter writer = new CSVWriter(headerList, content);
        String text = '\uFEFF' + writer.getText();
        FileOutputStream fo = new FileOutputStream(BaseUtils.ensureDirSepPostfix(folderName) + fileName);
        fo.write(text.getBytes(StandardCharsets.UTF_8));
        fo.close();
    }

    public List<AttributeBean> getManagmentAttrubutes() {
        return createBeansFromNamedQry("getManagmentAttrubutes", AttributeBean.class);
    }

    public void updateAttributes(AttributeBean ab) {
        execNamedCommand("updateAttributes", ab);

    }

    @Override
    public List<UserExtradataBean> getADLogin(String query) {
        if (query == null) {
            query = "";

        }
        return createBeansFromNamedQry("getADLogin", UserExtradataBean.class, query);
    }

    @Override
    public UserExtradataBean
            getInfoFromADByLogin(String loginAd) {
        return createBeanFromNamedQry("getInfoFromADByLogin", UserExtradataBean.class, true, loginAd);
    }

    private List<String> preapreHeaderList4RelatedTree2Export(int treeId, List<String> branchIds, Integer bindedTreeId) {
        Integer treeDepth = execNamedQuerySingleValAsInteger("getTreeDepth", true, null, treeId, branchIds);
        if (treeDepth == null) {
            treeDepth = 0;
        }
        List<String> bindedBranchIds = new ArrayList<String>();
        bindedBranchIds.add("");
        Integer bindedTreeDepth = execNamedQuerySingleValAsInteger("getTreeDepth", true, null, bindedTreeId, bindedBranchIds);
        if (bindedTreeDepth == null) {
            bindedTreeDepth = 0;
        }
        List<String> headerList = new ArrayList<String>();
        headerList.add(PumpConstants.GENERIC_SRC_NAME);
        for (int i = 0; i < treeDepth - 1; i++) {
            headerList.add(PumpConstants.GENERIC_SRC_PARENT_OBJID + i);
        }
        headerList.add(PumpConstants.GENERIC_DST_NAME);
        for (int i = 0; i < bindedTreeDepth - 1; i++) {
            headerList.add(PumpConstants.GENERIC_DST_PARENT_OBJID + i);
        }

        return headerList;
    }

    private int breakObjId(Map<String, Object> node, String field2Break, String parentColNamePref, String additionalNameCol) {
        String parentObjId = BaseUtils.safeToString(node.get(field2Break));
        List<String> parentPath = BaseUtils.splitBySep(parentObjId, "|");

        int limit = parentPath.size() - 1;
        if (!BaseUtils.isStrEmptyOrWhiteSpace(additionalNameCol)) {
            limit--;
            if (node.get(additionalNameCol) != null) {
                throw new LameRuntimeException("Atrybut nie może mieć nazwy " + additionalNameCol);
            }
            node.put(additionalNameCol, parentPath.get(limit));
        }
        for (int i = 0; i < limit; i++) {
            String colName = parentColNamePref + i;
            if (node.get(colName) != null) {
                throw new LameRuntimeException("Atrybut nie może mieć nazwy " + colName);
            }
            node.put(colName, parentPath.get(i));
        }

        return limit + 1;
    }

    @Override
    public Set<Integer> checkAutoObj4Tree(Set<Integer> allTreeIds) {
        return execNamedQuerySingleColAsSet("checkAutoObj4Tree", null, allTreeIds);
    }

    @Override
    public void importTreeWithLinkedAndJoinedObjs(final int treeId, Integer isIncremental, String serverFileName2ImportTree, String serverMetadataFileName, Map<Integer, String> bindingTreeWithServerFiles) {
        try {
//            setTreeImportStatus(treeId, TreeImportLogStatus.RUNNING);

            PlainFilePump pump = new PlainFilePump();
            pump.setEmbedded(true);
            ConnectionParametersPlainFileBean config = new ConnectionParametersPlainFileBean();
            config.isActive = true;
            config.isRemote = false;
            config.isIncremental = isIncremental;
            Pair<String, TreeExportMetadata> p = moveImportedFiles2TmpFolder4Tree(treeId, serverFileName2ImportTree, serverMetadataFileName, bindingTreeWithServerFiles);
            config.sourceFolderPath = p.v1;
            pump.setMetadata(p.v2);
            pump.setConfig(config);
            pump.setPumpLogger(new IPumpLogger() {

                @Override
                public void pumpLog(String str, Integer proc) {
                    add2TreeImportLog(treeId, str);
                }

                /*
                 Nie uzywaj tego
                 */
                @Override
                public MssqlConnectionConfig getConnectionConfig() {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
            });
            getPumpExecutor().addPumpToJobExecutorQueue(createPumpListAndAdd(pump));

//            setTreeImportStatus(treeId, TreeImportLogStatus.SUCCESS);
        } catch (Throwable ex) {
            add2TreeImportLog(treeId, "Nie spodziewany błąd: " + ex.getMessage());
//            setTreeImportStatus(treeId, TreeImportLogStatus.ERROR);
            ex.printStackTrace();
        }
    }

    private Pair<String, TreeExportMetadata> moveImportedFiles2TmpFolder4Tree(int treeId, String serverFileName2ImportTree, String serverMetadataFileName, Map<Integer, String> bindingTreeWithServerFiles) throws IOException {
        String treeCode = getTreeCodeById(treeId);
        String tmpFolder = BaseUtils.ensureDirSepPostfix(dirForUpload) + "tmp/importTree/" + treeCode + "/";
        File folder = new File(tmpFolder);
        if (!folder.exists()) {
            folder.mkdirs();
        }
        for (File file : folder.listFiles()) {
            file.delete();
        }

        if (!BaseUtils.isStrEmptyOrWhiteSpace(serverFileName2ImportTree)) {
            LameUtils.moveFileToDir(BaseUtils.ensureDirSepPostfix(dirForUpload) + serverFileName2ImportTree, tmpFolder);
        }

        ObjectMapper jsonMapper = new ObjectMapper();
        TreeExportMetadata metadata = new TreeExportMetadata();

        if (!BaseUtils.isStrEmptyOrWhiteSpace(serverMetadataFileName)) {
            TreeExportMetadata[] importConfigs = jsonMapper.readValue(new FileInputStream(BaseUtils.ensureDirSepPostfix(dirForUpload) + serverMetadataFileName), TreeExportMetadata[].class
            );
            if (importConfigs.length
                    != 1) {
                add2TreeImportLog(treeId, "Uwaga: plik .metadata należy mieć tylko jedną konfigurację");
            }
            if (importConfigs.length
                    == 0) {
                add2TreeImportLog(treeId, "Błąd: brak konfiguracji lub nie poprawny format");
                throw new LameRuntimeException("Błąd: brak konfiguracji lub nie poprawny format");
            }
            metadata = importConfigs[0];
        }
//        } else {
//            metadata.name = PumpConstants.GENERIC_NODE_NAME;
//            String csvContent = LameUtils.loadAsString(new FileInputStream(BaseUtils.ensureDirSepPostfix(tmpFolder) + serverFileName2ImportTree), "*Cp1250,ISO-8859-2,UTF-8");
//            CSVReader reader = new CSVReader(csvContent, CSVReader.CSVParseMode.CONSIDER_QUOTES, ';', true);
//            List<String> headerList = reader.readRow();
//            metadata.attributes = new HashMap<String, String>();
//            for (String colName : headerList) {
//                if (PumpConstants.GENERIC_NODE_NODE_KIND_CODE.equals(colName)) {
//                    metadata.nodeKindCode = PumpConstants.GENERIC_NODE_NODE_KIND_CODE;
//                }
//                if (!PumpConstants.GENERIC_NODE_NAME.equals(colName)
//                        && !PumpConstants.GENERIC_NODE_DESCR.equals(colName)
//                        && !PumpConstants.GENERIC_NODE_VISUAL_ORDER.equals(colName)
//                        && !colName.startsWith(PumpConstants.GENERIC_NODE_PARENT_OBJID)) {
//                    metadata.attributes.put(colName, colName);
//                }
//            }
//        }
        metadata.treeCode = treeCode;
        metadata.baseFileName = serverFileName2ImportTree;

        metadata.relatedFiles = new ArrayList<TreeExportRelatedFileMetaData>();
        for (Entry<Integer, String> e : bindingTreeWithServerFiles.entrySet()) {
            TreeExportRelatedFileMetaData relatedConfig = new TreeExportRelatedFileMetaData();
            Integer bindedTreeId = e.getKey();
            relatedConfig.relatedTreeCode = getTreeCodeById(bindedTreeId);;
            relatedConfig.relatedFileName = e.getValue();
            LameUtils.moveFileToDir(BaseUtils.ensureDirSepPostfix(dirForUpload) + e.getValue(), tmpFolder);
            metadata.relatedFiles.add(relatedConfig);
        }
        jsonMapper.enable(SerializationFeature.INDENT_OUTPUT);
        List<TreeExportMetadata> l = new ArrayList<TreeExportMetadata>();
        l.add(metadata);
        jsonMapper.writeValue(new FileOutputStream(tmpFolder + metadata.treeCode + ".metadata"), l);

        return new Pair<String, TreeExportMetadata>(tmpFolder, metadata);
    }

    private String updateAdditionalInfo(TreeExportMetadata metadata, int treeId, String dstFolder) throws IOException {
        List<Map<String, Object>> addtionalInfo = execNamedQueryCFN("getTreeExportAdditionalInfo", null, treeId);
        String fileName = "BIK_export_additional_info_" + metadata.treeCode + ".csv";
        List<String> headerList = new ArrayList<String>();
        headerList.add("TREE_CODE");
        headerList.add("NODE_KIND_CODE");
        headerList.add("ATTRIBUTE_NAME");
        headerList.add("ATTRIBUTE_TYPE");
        headerList.add("CATEGORY");
        headerList.add("VALUE");
        write2Csv(addtionalInfo, dstFolder, fileName, headerList);
        return BaseUtils.ensureDirSepPostfix(dstFolder) + fileName;
    }

    @Override
    public BiksSearchResult search(String searchMode, Integer searchId, String text, Set<Integer> selectedTreeIds, Set<String> selectedAttributeName, List<String> selectedOtherFilter, Set<Integer> nodeKindIds, int pageNum, int pageSize) throws LameRuntimeException {
//        text = text.trim().toLowerCase();
        BiksSearchResult res = new BiksSearchResult();

        if (searchId == null) {
            execNamedCommand("clearSearchHistory");
            List<Document> searchResult = searchService.search(text, selectedTreeIds, selectedAttributeName, selectedOtherFilter, nodeKindIds, searchMode, false);
            searchId = execNamedQuerySingleVal("getSearchId", false, null, text, searchMode);
            List<Integer> nodeIds = new ArrayList<Integer>();
            for (Document doc : searchResult) {
                String docId = doc.get(BiksSearchConstant.DOC_ID);
                nodeIds.add(BaseUtils.tryParseInteger(docId));
            }
            nodeIds = filtrSearchResultsByRole(nodeIds);
            nodeIds = filtrMetaBiksSearchResults(nodeIds);
            save2SearchCache(searchId, nodeIds);
        }
        res.items = getSearchResultsFromCache(searchId, pageNum, pageSize, nodeKindIds, searchMode.equals(BIKConstants.SEARCH_MODE_NORMAL) ? selectedAttributeName : null);

        res.searchId = searchId;
        res.treeCnt = countTotalResults(searchId, nodeKindIds);
        for (Integer treeId : selectedTreeIds) {
            if (!res.treeCnt.containsKey(treeId)) {
                res.treeCnt.put(treeId, 0);
            }
        }
        res.query = text;

//        for (Integer treeId : selectedTreeIds) {
//            Set<Integer> s = new HashSet<Integer>();
//            s.add(treeId);
//            Integer cnt = searchService.countResult(text, s, pageNum, pageSize);
//            res.treeCnt.put(treeId, cnt);
//        }
        return res;
    }

    public void automaticFillAttributesForNode(Set<TreeNodeBranchBean> tnb, AttributeBean atr, boolean addAttribute) {
        execNamedCommand("automaticFillAttributes", tnb, atr, addAttribute);
    }

    @Override
    public void automaticUpdateJoinedObjsForNodeInner(int srcId, Set<TreeNodeBranchBean> tnbs) {

        execNamedCommand("automaticUpdateJoinedObjsForNodeInner", srcId, tnbs);
    }

    @Override
    public void automatiAddJoinedAttributeLinked(int nodeId, Set<TreeNodeBranchBean> tnbs, List<JoinedObjAttributeLinkedBean> joal) {
        execNamedCommand("automaticUpdateJoinedObjsForNodeInner", nodeId, tnbs);
        execNamedCommand("automatiAddJoinedAttributeLinked", nodeId, tnbs, joal);
    }

    @Override
    public void automaticDeleteAttributesForNode(Set<TreeNodeBranchBean> tnb, AttributeBean atr) {
        execNamedCommand("automaticDeleteAttributesForNode", tnb, atr);
    }

    public List<AttributeBean> getRequiredAttrs(int nodeKindId) {
        List<AttributeBean> abs = createBeansFromNamedQry("getRequiredAttrs", AttributeBean.class, nodeKindId);
        return abs;
    }

    public List<AttributeBean> getRequiredAttrsWithVisOrder(int nodeKindId) {
        List<AttributeBean> abs = createBeansFromNamedQry("getRequiredAttrsWithVisOrder", AttributeBean.class, nodeKindId);
        return abs;
    }

    public Pair< List<AttributeBean>, List<JoinedObjAttributeLinkedBean>> addAttributeValuesMonitoriedByDrools() {
        List<AttributeBean> allNodeAttrs = createBeansFromNamedQry("getAllNodeAttributeValuesMonitoriedByDrools", AttributeBean.class
        );
        List<JoinedObjAttributeLinkedBean> allJoinedObjAttrs = createBeansFromNamedQry("getAllJoinedObjAttributeValuesMonitoriedByDrools", JoinedObjAttributeLinkedBean.class);

        return new Pair< List<AttributeBean>, List<JoinedObjAttributeLinkedBean>>(allNodeAttrs, allJoinedObjAttrs);
    }

    @SuppressWarnings("unchecked")
    public void generateAndRunRules(Integer nodeId) {
        try {
            EntityDetailsDataBean eddb = getBikEntityDetailsData(nodeId, null, false);
            if (BaseUtils.isCollectionEmpty(eddb.attributes)) {
                execNamedCommand("addDroolsLogMsg", "Podany węzeł nie posiada szablon");
                return;
            }
            String template = null;
            String parameters = "";
            for (AttributeBean attr : eddb.attributes) {
                if (BIKConstants.DROOLS_TEMPLATE_ATTRIBUTE_NAME.equals(attr.atrName)) {
                    template = attr.atrLinValue;
                } else if (BIKConstants.DROOLS_PARAM_ATTRIBUTE_NAME.equals(attr.atrName)) {
                    parameters = attr.atrLinValue;
                }
            }
            if (BaseUtils.isStrEmptyOrWhiteSpace(template)) {
                execNamedCommand("addDroolsLogMsg", "Podany węzeł nie posiada szablon");
                return;
            }
            ObjectDataCompiler compiler = new ObjectDataCompiler();

            ObjectMapper mapper = new ObjectMapper();
            TypeReference<HashMap<String, Object>> typeRef = new TypeReference<HashMap<String, Object>>() {
            };
            Map<String, Object> data = mapper.readValue(parameters, typeRef);

            String drl = compiler.compile(Arrays.asList(data), new ByteArrayInputStream(template.getBytes()));
            String tmpFolder = BaseUtils.ensureDirSepPostfix(dirForUpload) + "tmp/drools-template/" + BaseUtils.generateRandomToken(10) + "/";
            LameUtils.ensureDirExists(tmpFolder);

            LameUtils.txtToFileWithEncoding(tmpFolder + nodeId + ".drl", "UTF-8", drl);

            execNamedCommand("addDroolsLogMsg", "Generowany .drl był zapisany w: " + tmpFolder);
            DroolsConfigBean droolsConfig = new DroolsConfigBean();
            droolsConfig.drlFolder = tmpFolder;

            DroolsEngine engine = getDroolsEngine();
            engine.setService(this);
            DroolsEngineWorker worker = new DroolsEngineWorker(this, getAdhocDao(), engine, droolsConfig);
            worker.doit();
        } catch (Exception ex) {
            execNamedCommand("addDroolsLogMsg", LameUtils.printExceptionAsString(ex));
        }
    }

    public void runRules(AttributeBean ab, TreeNodeBean tnb) {
        try {
            DroolsConfigBean droolsConfig = new DroolsConfigBean();
            droolsConfig.drlFolder = BaseUtils.ensureDirSepPostfix(cfg.dirForUpload) + "actions";
            DroolsEngine engine = getDroolsEngine();
            engine.setService(this);
            DroolsEngineWorker worker = new DroolsEngineWorker(this, getAdhocDao(), engine, droolsConfig);
            worker.setAb(ab);
            worker.setTnb(tnb);
            worker.doit();
        } catch (Exception ex) {
            execNamedCommand("addDroolsLogMsg", LameUtils.printExceptionAsString(ex));
        }
    }

//       public void RunRules(AttributeBean ab, TreeNodeBean tnb) {
//        try {
//            DroolsConfigBean droolsConfig = new DroolsConfigBean();
//            droolsConfig.drlFolder = BaseUtils.ensureDirSepPostfix(cfg.dirForUpload) + "actions";
//            DroolsEngine engine = getDroolsEngine();
//            engine.setService(this);
//            DroolsEngineWorker worker = new DroolsEngineWorker(this, getAdhocDao(), engine, droolsConfig);
//            worker.ab = ab;
//            worker.tnb = tnb;
//            worker.doit();
//        } catch (Exception ex) {
//            execNamedCommand("addDroolsLogMsg", LameUtils.printExceptionAsString(ex));
//        }
//    }
//
    protected DroolsEngine getDroolsEngine() {
        return droolsEngine;
    }

    @SuppressWarnings("unchecked")
    private INamedSqlsDAO<Object> getAdhocDao4Report(String reportFolder, String reportName) {
        INamedSqlsDAO<Object> adhocDao = adhocDao4Report.get(reportName);
        if (adhocDao == null) {
            IBeanConnectionEx connection = new MssqlConnection(cfg.connConfig);
            adhocDao = FoxyAppUtils.createNamedSqlsDAOFromFile(connection, reportFolder + reportName + ".xml");
            adhocDao4Report.put(reportName, adhocDao);
        }
        return adhocDao;
    }

    @Override
    public String compileReport(int nodeId, String reportURL) throws LameRuntimeException {
        String url;
        if (!reportURL.contains("?")) {
            url = reportURL + "?node_id=" + nodeId + "&user_id=" + getLoggedUserId();
        } else {
            url = reportURL.substring(0, reportURL.indexOf("?") + 1) + "node_id=" + nodeId + "&user_id=" + getLoggedUserId() + "&" + reportURL.substring(reportURL.indexOf("?") + 1);
        }
        String address = getAppPropValue("jasperServer.reportEndPoint") + url;
//        CloseableHttpClient httpclient = HttpClients.createDefault();
//        System.out.println("Getting report from: " + address);
//        HttpGet httpGet = new HttpGet(address);
        String jasperUsername = getAppPropValue("jasperServer.username");
        String jasperPassword = getAppPropValue("jasperServer.password");
        String auth = jasperUsername + ":" + jasperPassword;
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("ISO-8859-1")));
        String authHeader = "Basic " + new String(encodedAuth);
//        httpGet.setHeader(HttpHeaders.AUTHORIZATION, authHeader);

        try {
            URL obj = new URL(address);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            con.setRequestProperty("User-Agent", "MSIE 8.0");
            con.setRequestProperty("Authorization", authHeader);

            int responseCode = con.getResponseCode();
            //        try {
//            CloseableHttpResponse response = httpclient.execute(httpGet);
            StringBuilder sb = new StringBuilder();
//                HttpEntity entity = response.getEntity();
//                BufferedReader br = new BufferedReader(new InputStreamReader(entity.getContent(), "UTF8"));

            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF8"));
            String output;
            while ((output = br.readLine()) != null) {
                sb.append(output);
            }
//            } finally {
//                response.close();
//            }
            return sb.toString();
        } catch (Exception ex) {
            throw new LameRuntimeException(ex);
        }
    }

    public byte[] getJasperReports(int nodeId, String reportURL, String exp) throws LameRuntimeException {
        String url;
        reportURL = reportURL + "." + exp;
        if (!reportURL.contains("?")) {
            url = reportURL + "?node_id=" + nodeId + "&user_id=" + getLoggedUserId();
        } else {
            url = reportURL.substring(0, reportURL.indexOf("?") + 1) + "node_id=" + nodeId + "&user_id=" + getLoggedUserId() + "&" + reportURL.substring(reportURL.indexOf("?") + 1);
        }
        String address = getAppPropValue("jasperServer.reportEndPoint") + url;
        String jasperUsername = getAppPropValue("jasperServer.username");
        String jasperPassword = getAppPropValue("jasperServer.password");
        String auth = jasperUsername + ":" + jasperPassword;
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("ISO-8859-1")));
        String authHeader = "Basic " + new String(encodedAuth);

        try {
            URL obj = new URL(address);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            con.setRequestProperty("User-Agent", "MSIE 8.0");
            con.setRequestProperty("Authorization", authHeader);

            InputStream in = con.getInputStream();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buf = new byte[131072];
            int n = 0;
            while (-1 != (n = in.read(buf))) {
                out.write(buf, 0, n);
            }
            out.close();
            in.close();
            byte[] response = out.toByteArray();
            return response;
        } catch (Exception ex) {
            throw new LameRuntimeException(ex);
        }
    }

    @Override
    @Deprecated
    @SuppressWarnings("unchecked")
    public String compileJasper(Integer nodeId, String reportName, String params) {
        try {
            String reportFolder = BaseUtils.ensureDirSepPostfix(dirForUpload) + "report/";
            String reportFilePath = reportFolder + reportName + ".jasper";
            INamedSqlsDAO<Object> adhocDao = getAdhocDao4Report(reportFolder, reportName);
            ReportDataProvider dp = new ReportDataProvider(adhocDao, nodeId, reportName, reportFolder);
//            List<Map<String, Object>> data = execNamedQueryCFN("getChildNodesIdAndName", FieldNameConversion.ToLowerCase, nodeId);
//            params.put("message", new SampleBiksNodeDS(execNamedQueryCFN("getChildNodesIdAndName", FieldNameConversion.ToLowerCase, nodeId)));
//            params.put("node_id", nodeId);

            JasperPrint jasperPrint = JasperFillManager.fillReport(reportFilePath, dp.provideDataForReport(params), new JREmptyDataSource());
            final Map<String, String> images = new HashMap<String, String>();

//            JRPdfExporter exporter = new JRPdfExporter();
//            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
//            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(File.createTempFile("output.", ".pdf", new File("C:/TEMP/"))));
//            SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
//            configuration.setMetadataAuthor("Petter");  //why not set some config as we like
//            exporter.setConfiguration(configuration);
//            exporter.exportReport();
            Exporter exporterHTML = new HtmlExporter();
            SimpleHtmlExporterConfiguration conf = new SimpleHtmlExporterConfiguration();
            conf.setHtmlHeader("");
            conf.setHtmlFooter("");
            exporterHTML.setConfiguration(conf);

            SimpleExporterInput exporterInput = new SimpleExporterInput(jasperPrint);
            exporterHTML.setExporterInput(exporterInput);

//            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            StringBuilder sb = new StringBuilder("\uFEFF");
            SimpleHtmlExporterOutput simpleHtmlExporterOutput = new SimpleHtmlExporterOutput(sb);
            simpleHtmlExporterOutput.setImageHandler(new HtmlResourceHandler() {

                @Override
                public void handleResource(String id, byte[] data) {
//                    images.put(id, "data:image/jpg;base64," + Base64.encodeBytes(data));
                }

                @Override
                public String getResourcePath(String id) {
                    return images.get(id);
                }
            });

            exporterHTML.setExporterOutput(simpleHtmlExporterOutput);

            exporterHTML.exportReport();

            return sb.toString();
//            String res = new String(""
//            outputStream.toByteArray()
//            );
//            return res;
//            return "";
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new LameRuntimeException(ex.getMessage());
        }
    }

    private String parseSpecialTags(Integer nodeId, String descr, boolean isPDFFormat) {
        if (BaseUtils.isStrEmptyOrWhiteSpace(descr)) {
            return descr;
        }
        Set<String> reports = BaseUtils.extractReportTags(descr);
        for (final String reportTag : reports) {
//            final String report = reportTag.substring(0, reportTag.indexOf('('));
//            String params = reportTag.substring(reportTag.indexOf('(') + 1, reportTag.lastIndexOf(')'));
            String placeHolder = BaseUtils.REPORT_TAG_PREFIX + reportTag + BaseUtils.REPORT_TAG_SUFFIX;
            if (isPDFFormat) {
                descr = descr.replace(placeHolder, "");
            } else {
                try {
//                String result = compileJasper(nodeId, report, params);
                    String result = compileReport(nodeId, reportTag);
//                    String placeHolder = BaseUtils.REPORT_TAG_PREFIX + reportTag + BaseUtils.REPORT_TAG_SUFFIX;
                    descr = descr.replace(placeHolder, result);
                } catch (Exception ex) {
                    //nie mozemy parsowac -> ignorujemy
                }
            }
        }

        return descr;
    }

    @Override
    public void buildMenuConfigTree(boolean deleteOldTree) throws LameRuntimeException {
//        String menuConfigTreeCode = getMenuConfigurationTree();

        execNamedCommand("buildMenuConfigTree", deleteOldTree ? 1 : 0);
    }

    @Override
    @SuppressWarnings("unchecked")
    public String getNodeAssociationScript(Integer nodeId) {
        try {
            if (adhocDaoJs == null) { //not threadsafe
                IBeanConnectionEx connection = new MssqlConnection(cfg.connConfig);
                adhocDaoJs = FoxyAppUtils.createNamedSqlsDAOFromFile(connection, BaseUtils.ensureDirSepPostfix(dirForUpload) + "js/showAssociation.xml");
            }
            String script = LameUtils.loadAsString(BaseUtils.ensureDirSepPostfix(dirForUpload) + "js/showAssociation.jst");
            Map<String, Object> m = LameUtils.readJsonAsMap(LameUtils.loadAsString(BaseUtils.ensureDirSepPostfix(dirForUpload) + "js/showAssociation.ds"));

            for (String placeHolder : m.keySet()) {
                String queryName = (String) m.get(placeHolder);
                List<Map<String, Object>> data = adhocDaoJs.execNamedQueryCFN(queryName, FieldNameConversion.ToLowerCase, nodeId);
                script = script.replace("%{" + placeHolder + "}%", LameUtils.writeListMapAsJson(data));
            }

            return script;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new LameRuntimeException(ex.getMessage());
        }
    }

    @Override
    public String getConfigurationTree(String appProp) {
        return getAppPropValue(appProp);
    }

//    @Override
//    public String getMenuConfigurationTree() {
//        return execNamedQuerySingleVal("getMenuConfigTree", true, null);
//    }
    @Override
    public void changeConfigurationTree(String appProp, String treeCode) throws LameRuntimeException {
        List<TreeBean> trees = getTrees(true);
        for (TreeBean tree : trees) {
            if (tree.code.equals(treeCode)) {
                if (tree.isAutoObjId == 0) {
                    throw new LameRuntimeException("brak auto_obj_id");
                }
            }
        }
        setAppPropValue(appProp, treeCode);
    }

    protected void buildConfigTree(String confTreeCode, boolean deleteOldTree) {
        if (BaseUtils.isStrEmptyOrWhiteSpace(confTreeCode)) {
            throw new LameRuntimeException("Drzewo konfiguracji nie zostało zdefiniowane");
        }
//        int confTreeId = getTreeIdByTreeCode(confTreeCode);
//        String objId = BaseUtils.ensureObjIdEndWith(BIKConstants.OBJ_ID_ATTRIBUTE_DEF, "|");
//        Integer branchNodeKindId = execNamedQuerySingleVal("getBranchNodeKindId", true, null, confTreeCode, 1);
//        Integer leafNodeKindId = execNamedQuerySingleVal("getBranchNodeKindId", true, null, confTreeCode, 0);
//        if (branchNodeKindId == null) {
//            throw new LameRuntimeException("Drzewo konfiguracji nie ma gałęzi");
//        }
//        if (leafNodeKindId == null) {
//            leafNodeKindId = branchNodeKindId;
//        }
//        if (deleteOldTree) {
//            execNamedCommand("hideNodeInTree", confTreeCode);
//        }
        Map<String, Integer> nkMap = new HashMap<String, Integer>();
        List<NodeKindBean> allNodeKinds = getNodeKinds();
        for (NodeKindBean nk : allNodeKinds) {
            nkMap.put(nk.code, nk.id);
        }
//        ensureAllMetaAttributesAreCreatedAndAssignedToConfigTree(confTreeCode);

//        execNamedCommand("prepareNodeKind4TreeKind");
        execNamedCommand("buildMetamodel", deleteOldTree ? 1 : 0);
        deleteEmptyMetaAttributes(true);
//        Map<Integer, Integer> attrs2NodeIds = buildConfigTreeAttributesBranch(confTreeCode, nkMap);
//        Map<Integer, Integer> nodeKinds2NodeIds = buildConfigTreeNodeKindsBranch(confTreeCode, nkMap, attrs2NodeIds);
//        buildConfigTreeTreeKindBranch(confTreeCode, nkMap, nodeKinds2NodeIds);

    }

    private void deleteEmptyMetaAttributes(boolean isHidden) {
        execNamedCommand("deleteEmptyMetaAttributes", isHidden ? 1 : 0);
    }
//    @Override
//    public void buildTmpConfigTree() {
//        buildConfigTree(BIKConstants.METABIKS_TREE_CODE_TMP, true);
//    }

    @Override
    public void buildConfigTree(boolean deleteOldTree) throws LameRuntimeException {
        String confTreeCode = getConfigurationTree(BIKConstants.APP_PROP_SYS_CONF);
        buildConfigTree(confTreeCode, deleteOldTree);
    }

    protected boolean hasReferenceToBranch(Integer confNodeId) {
        Integer cnt = execNamedQuerySingleValAsInteger("countHyperLinkIn2Node", true, null, confNodeId);
        if (cnt > 0) {
            return true;
        }
        return false;
    }

//    private void implementDeleteAttributeDict(List<Map<String, Object>> rs) {
//        for (Map<String, Object> m : rs) {
//            String confNodeKind = (String) m.get("config_node_kind_code");
//            if (BIKConstants.NODE_KIND_META_ATTRIBUTE_TYPE.equalsIgnoreCase(confNodeKind)) {
//                String attrName = (String) m.get("node_name");
//                Integer attrId = execNamedQuerySingleValAsInteger("getAttributeIdByName", false, null, attrName);
//                deleteAttributeDict(attrId);
//            }
//        }
//    }
//
//    private void implementDeleteAttributeCatalog(List<Map<String, Object>> rs) {
//        for (Map<String, Object> m : rs) {
//            String confNodeKind = (String) m.get("config_node_kind_code");
//            if (BIKConstants.NODE_KIND_META_ATTRIBUTES_CATALOG.equalsIgnoreCase(confNodeKind)) {
//                String categoryName = (String) m.get("node_name");
//                Integer categroyId = execNamedQuerySingleValAsInteger("getAttributeCategoryIdByName", false, null, categoryName);
//                deleteCategory(categroyId);
//            }
//        }
//    }
//    private void implementDeleteNodeKind(List<Map<String, Object>> rs) {
//        for (Map<String, Object> m : rs) {
//            String confNodeKind = (String) m.get("config_node_kind_code");
//            if (BIKConstants.NODE_KIND_META_NODE_KIND.equalsIgnoreCase(confNodeKind)) {
//                String nodeKind = (String) m.get("node_name");
//                execNamedCommand("deleteNodeKind", nodeKind);
//            }
//        }
//    }
//    private void implementDeleteTreeKind(List<Map<String, Object>> rs) {
//        for (Map<String, Object> m : rs) {
//            String confNodeKind = (String) m.get("config_node_kind_code");
//            if (BIKConstants.NODE_KIND_META_TREE_KIND.equalsIgnoreCase(confNodeKind)) {
//                String treeKind = (String) m.get("node_name");
//                execNamedCommand("deleteTreeKind", treeKind);
//            }
//        }
//    }
    private void ensureNoReference2Delete(Set<Integer> attrCategoryIds2Delete, Set<Integer> attrDefIds2Delete, Set<Integer> nodeKindIds2Delete, Set<Integer> treeKindIds2Delete) {
        String confTreeCode = getConfigurationTree(BIKConstants.APP_PROP_SYS_CONF);
        List<AttributeBean> attrDefAndCategory = getAttrDefAndCategory();
        Set<Integer> nodeIds = new HashSet<Integer>();
        for (AttributeBean bean : attrDefAndCategory) {
            if (attrDefIds2Delete.contains(bean.attrDefId)) {
                Integer nodeId = getNodeIdWithAutoObjId(confTreeCode, BIKConstants.METABIKS_OBJ_ID_ATTRIBUTE_DEF + "|" + bean.atrCategory + "|" + bean.atrName + "|");
                if (nodeId != null) {
                    nodeIds.add(nodeId);
                }
            }
        }
        for (AttributeBean bean : attrDefAndCategory) {
            if (attrCategoryIds2Delete.contains(bean.categoryId)) {
                Integer nodeId = getNodeIdWithAutoObjId(confTreeCode, BIKConstants.METABIKS_OBJ_ID_ATTRIBUTE_DEF + "|" + bean.atrCategory + "|");
                if (nodeId != null) {
                    List<TreeNodeBean> tnb = getChildrenNodeBeansByParentId(nodeId);
                    for (TreeNodeBean n : tnb) {
                        nodeIds.add(n.id);
                    }
                }
            }
        }
        for (Integer id : nodeKindIds2Delete) {
            NodeKindBean nk = getNodeKind(id);
            Integer nodeId = getNodeIdWithAutoObjId(confTreeCode, BIKConstants.METABIKS_OBJ_ID_NODE_KIND_DEF + "|" + nk.code + "|");
            if (nodeId != null) {
                nodeIds.add(nodeId);
            }
        }
        for (Integer id : treeKindIds2Delete) {
            TreeKindBean tk = getTreeKind(id);
            Integer nodeId = getNodeId(BIKConstants.METABIKS_OBJ_ID_TREE_KIND_DEF + "|" + tk.code + "|", confTreeCode);
            if (nodeId != null) {
                nodeIds.add(nodeId);
            }
        }
        for (Integer nodeId : nodeIds) {
            if (hasReferenceToBranch(nodeId)) {
                throw new LameRuntimeException("Jest element podlinkowany do node_id=" + nodeId);
            }
        }
    }

    protected void implementDeletion(Set<Integer> attrCategoryIds2Delete, Set<Integer> attrDefIds2Delete, /*Set<Integer> attrIds2Delete,*/ Set<Integer> nodeKindIds2Delete, Set<Integer> treeKindIds2Delete) {
        for (Integer categroyId : attrCategoryIds2Delete) {
            deleteCategory(categroyId);
        }
        for (Integer atrDefId : attrDefIds2Delete) {
            deleteAttributeDict(atrDefId);
        }
//        for (Integer attrId : attrIds2Delete) {
//            execNamedCommand("deleteAttribute", attrId);
//        }
        execNamedCommand("deleteAllAttributes");
        for (Integer nodeKindId : nodeKindIds2Delete) {
            execNamedCommand("deleteNodeKind", nodeKindId);
        }
        for (Integer treeKindId : treeKindIds2Delete) {
            execNamedCommand("deleteTreeKind", treeKindId);
        }
    }

    protected void implementAddingAndUpdating(String confTreeCode, Map<Integer, Integer> attrCategoryConfMap, Map<Integer, Integer> attrDefConfMap, Map<Integer, Integer> nodeKindConfMap, Map<Integer, Integer> treeKindConfMap) {
        implementAttributes(confTreeCode, attrCategoryConfMap, attrDefConfMap);
        getAdhocDao().finishWorkUnit(true);
        implementNodeKinds(confTreeCode, nodeKindConfMap);
//        implementTreeKind(confTreeCode);
        getAdhocDao().finishWorkUnit(true);
        implementTreeKinds(confTreeCode, nodeKindConfMap, treeKindConfMap);
    }

    @Override
    public void implementConfigTree(Map<Integer, Integer> attrCategoryConfMap, Set<Integer> attrCategoryIds2Delete, Map<Integer, Integer> attrDefConfMap, Set<Integer> attrDefIds2Delete, /*Map<Integer, Integer> attrConfMap, Set<Integer> attrIds2Delete, */ Map<Integer, Integer> nodeKindConfMap, Set<Integer> nodeKindIds2Delete, Map<Integer, Integer> treeKindConfMap, Set<Integer> treeKindIds2Delete) throws LameRuntimeException {
//    public void implementConfigTree(Map<Integer, Integer> attrDictConfMap, Map<Integer, Integer> attrConfMap, Map<Integer, Integer> nodeKindConfMap, Map<Integer, Integer> treeKindConfMap) throws LameRuntimeException {
//        getAdhocDao().execInAutoCommitMode(doAfterVerticalizeInPumps);
        String confTreeCode = getConfigurationTree(BIKConstants.APP_PROP_SYS_CONF);
        if (BaseUtils.isStrEmptyOrWhiteSpace(confTreeCode)) {
            throw new LameRuntimeException("Drzewo konfiguracji nie zostało zdefiniowane");
        }
//        int confTreeId = getTreeIdByTreeCode(confTreeCode);
//        String tmpConfigTreeCode = BIKConstants.METABIKS_TREE_CODE_TMP;
//        List<Map<String, Object>> rs = execNamedQueryCFN("getDeletedConfigElements", null, confTreeCode, tmpConfigTreeCode);

        ensureConfigTreeHasEssentialStruture(confTreeCode);
        ensureConfigTreeDoesNotHaveExternalLinks(confTreeCode);
        ensureNoReference2Delete(attrCategoryIds2Delete, attrDefIds2Delete, nodeKindIds2Delete, treeKindIds2Delete);

        implementDeletion(attrCategoryIds2Delete, attrDefIds2Delete, /*attrIds2Delete,*/ nodeKindIds2Delete, treeKindIds2Delete);
        implementAddingAndUpdating(confTreeCode, attrCategoryConfMap, attrDefConfMap, nodeKindConfMap, treeKindConfMap);
    }

    @Override
    public void implementConfigTree() throws LameRuntimeException {
        String confTreeCode = getConfigurationTree(BIKConstants.APP_PROP_SYS_CONF);
        if (BaseUtils.isStrEmptyOrWhiteSpace(confTreeCode)) {
            throw new LameRuntimeException("Drzewo konfiguracji nie zostało zdefiniowane");
        }

        deleteEmptyMetaAttributes(false);
        ensureConfigTreeHasEssentialStruture(confTreeCode);
        ensureConfigTreeDoesNotHaveExternalLinks(confTreeCode);

        execNamedCommand("implementConfigTree");
        deleteEmptyMetaAttributes(true);
    }

    private void implementTreeKinds(String confTreeCode, Map<Integer, Integer> nodeKindConfMap, Map<Integer, Integer> treeKindConfMap) {
        Integer treeKindsRootNodeId = getNodeId(BaseUtils.ensureObjIdEndWith(BIKConstants.METABIKS_OBJ_ID_TREE_KIND_DEF, "|"), confTreeCode);
        if (treeKindsRootNodeId == null) {
            throw new LameRuntimeException("Nieprawidłowa struktura drzewa konfiguracji");
        }
        List<TreeNodeBean> treeKindsInConfTree = getChildrenNodeBeansByParentId(treeKindsRootNodeId);
        for (TreeNodeBean tkConfig : treeKindsInConfTree) {
            Integer treeKindId = treeKindConfMap.get(tkConfig.id);
            Map<String, AttributeBean> attrDesc = buildNodeAttributesMap(tkConfig.id);
            AttributeBean attr = attrDesc.get(BIKConstants.METABIKS_ATTR_ALLOW_LINKING);
            Integer allowLinking = (attr == null || BIKConstants.NO.equalsIgnoreCase(attr.atrLinValue)) ? 0 : 1;

            String branchCaption = "";
            String branchCode = "";
            String leafCaption = "";
            String leafCode = "";
            String branchIcon = "";
            String leafIcon = "";
            String lang = "pl";

            String caption = tkConfig.name;
//            attr = attrDesc.get(BIKConstants.METABIKS_ATTR_NODE_KIND_CAPTION_EN);
//            if (attr != null) {
//                caption = attr.atrLinValue;
//                execNamedCommand("updateBikTranslationByLang", tkConfig.name, caption, "en", "tkind");
//            }
            attr = attrDesc.get(BIKConstants.METABIKS_ATTR_NODE_KIND_CAPTION_PL);
            if (attr != null) {
                caption = attr.atrLinValue;
                execNamedCommand("updateBikTranslationByLang", tkConfig.name, caption, "pl", "tkind");
            }

            List<TreeNodeBean> nodeKindsInTreeKind = getChildrenNodeBeansByParentId(tkConfig.id);
//            Map<Integer, List<Integer>> relationsMap = new HashMap<Integer, List<Integer>>();
            Map<Integer, Integer> nodeKindNodeId2NodeKindId = new HashMap<Integer, Integer>();
            Set<Integer> mainBranches = new HashSet<Integer>();
            Set<Integer> leaves = new HashSet<Integer>();
            for (TreeNodeBean tnb : nodeKindsInTreeKind) {
                Map<String, AttributeBean> nodeKindAttrs = buildNodeAttributesMap(tnb.id);
                if (nodeKindAttrs.containsKey(BIKConstants.METABIKS_ATTR_ORIGINAL)) {
                    AttributeBean nodeKindAttr = nodeKindAttrs.get(BIKConstants.METABIKS_ATTR_BRANCH_TYPE_IN_TREE);
                    if (nodeKindAttr == null || BaseUtils.isStrEmptyOrWhiteSpace(nodeKindAttr.atrLinValue)) {
                        throw new LameRuntimeException("Nie wskazano node_kind: " + tnb.name + " w definicji drzewa " + tkConfig.name);
                    }

                    AttributeBean originalAttr = nodeKindAttrs.get(BIKConstants.METABIKS_ATTR_ORIGINAL);
                    Integer originalNodeKindNodeId = BaseUtils.tryParseInteger(BaseUtils.splitBySep(originalAttr.atrLinValue, "_").get(0));
                    String originalNodeKindCode = BaseUtils.splitBySep(originalAttr.atrLinValue, ",").get(1);

//                    execNamedCommand("updateTreeKind4NodeKind", originalNodeKindCode, tkConfig.name);
                    Map<String, AttributeBean> originalNodeKindAttrs = buildNodeAttributesMap(originalNodeKindNodeId);
                    AttributeBean originalCaptionAttr = originalNodeKindAttrs.get(BIKConstants.METABIKS_ATTR_NODE_KIND_CAPTION_PL);

                    AttributeBean nodeIconAttr = originalNodeKindAttrs.get(BIKConstants.METABIKS_ATTR_ICON_NAME);
                    if (nodeIconAttr == null || BaseUtils.isStrEmptyOrWhiteSpace(nodeIconAttr.atrLinValue)) {
                        throw new LameRuntimeException("Nie wskazano ikony dla node_kind" + tnb.name + " w definicji drzewa " + tkConfig.name);
                    }

                    Integer nodeKindId = nodeKindConfMap.get(originalNodeKindNodeId);
                    nodeKindNodeId2NodeKindId.put(tnb.id, nodeKindId);
                    boolean isLeaf = nodeKindAttr.atrLinValue.contains(BIKConstants.LEAF);
                    boolean isBranch = nodeKindAttr.atrLinValue.contains(BIKConstants.BRANCH);
                    boolean isMainBranch = nodeKindAttr.atrLinValue.contains(BIKConstants.MAIN_BRANCH);
                    if (isMainBranch) {
                        mainBranches.add(nodeKindId);
                    }
                    if (isBranch || isMainBranch) {
                        branchCaption = originalCaptionAttr.atrLinValue;
                        branchCode = originalNodeKindCode;
                        branchIcon = nodeIconAttr.atrLinValue;
                    }
                    if (isLeaf) {
                        leafCaption = originalCaptionAttr.atrLinValue;
                        leafCode = originalNodeKindCode;
                        leafIcon = nodeIconAttr.atrLinValue;
                        leaves.add(nodeKindId);
                    }

//                    List<Integer> dstNodeKindsList = relationsMap.get(nodeKindId);
//                    if (dstNodeKindsList == null) {
//                        dstNodeKindsList = new ArrayList<Integer>();
//                    }
//                    relationsMap.put(nodeKindId, dstNodeKindsList);
                } else {
                    throw new LameRuntimeException("Nie było linku do oryginalnego node_kind " + tnb.name + " w drzewie " + tkConfig.name);
                }
            }

            TreeKindBean tkb = new TreeKindBean();
            tkb.code = tkConfig.name;
            tkb.caption = caption;
            tkb.branchCaption = branchCaption;
            tkb.branchCode = branchCode;
            tkb.branchIcon = branchIcon;
            tkb.leafCaption = leafCaption;
            tkb.leafCode = leafCode;
            tkb.leafIcon = leafIcon;
            tkb.allowLinking = allowLinking;
            tkb.lang = lang;

            tkb = addOrUpdateTreeType(Arrays.asList(tkb), treeKindId, false, false); //sztuczny tree_kind aby można pobrać tree_kind_code
            treeKindId = tkb.id;
            treeKindConfMap.put(tkConfig.id, treeKindId);
            tkConfig.name = tkb.code;
            updateTreeNode(tkConfig);
            for (TreeNodeBean tnb : nodeKindsInTreeKind) {
                Map<String, AttributeBean> nodeKindAttrs = buildNodeAttributesMap(tnb.id);
                if (nodeKindAttrs.containsKey(BIKConstants.METABIKS_ATTR_ORIGINAL)) {
                    AttributeBean nodeKindAttr = nodeKindAttrs.get(BIKConstants.METABIKS_ATTR_BRANCH_TYPE_IN_TREE);
                    if (nodeKindAttr == null || BaseUtils.isStrEmptyOrWhiteSpace(nodeKindAttr.atrLinValue)) {
                        throw new LameRuntimeException("Nie wskazano typu node_kind: " + tnb.name + " w definicji drzewa " + tkConfig.name);
                    }

                    AttributeBean originalAttr = nodeKindAttrs.get(BIKConstants.METABIKS_ATTR_ORIGINAL);
                    String originalNodeKindCode = BaseUtils.splitBySep(originalAttr.atrLinValue, ",").get(1);
                    if (originalNodeKindCode.contains("|")) {
                        originalNodeKindCode = originalNodeKindCode.substring(0, originalNodeKindCode.indexOf("|"));
                    }

                    execNamedCommand("updateTreeKind4NodeKind", originalNodeKindCode, tkConfig.name);
                }
            }
            execNamedCommand("deleteAllNodeKind4TreeKind", treeKindId);

            for (Integer nodeKindId : leaves) {
                execNamedCommand("updateNodeKind4TreeKind", treeKindId, nodeKindId, null, null);
            }
            for (Integer nodeKindId : mainBranches) {
                execNamedCommand("updateNodeKind4TreeKind", treeKindId, null, nodeKindId, null);
            }
            for (TreeNodeBean srcBean : nodeKindsInTreeKind) {
                Integer srcNodeKindId = nodeKindNodeId2NodeKindId.get(srcBean.id);
                List<TreeNodeBean> dstConfigBeans = getChildrenNodeBeansByParentId(srcBean.id);
                for (TreeNodeBean dstBean : dstConfigBeans) {
                    Map<String, AttributeBean> relationAttrDesc = buildNodeAttributesMap(dstBean.id);
                    if (relationAttrDesc.containsKey(BIKConstants.METABIKS_ATTR_ORIGINAL) && relationAttrDesc.containsKey(BIKConstants.METABIKS_ATTR_RELATION_TYPE)) {
                        AttributeBean originalAttr = relationAttrDesc.get(BIKConstants.METABIKS_ATTR_ORIGINAL);
                        Integer originalNodeKindNodeId = BaseUtils.tryParseInteger(BaseUtils.splitBySep(originalAttr.atrLinValue, "_").get(0));
                        Integer dstNodeKindId = nodeKindConfMap.get(originalNodeKindNodeId);

//                        List<Integer> dstNodeKindsList = relationsMap.get(srcNodeKindId);
//                        dstNodeKindsList.add(dstNodeKindId);
                        AttributeBean relationType = relationAttrDesc.get(BIKConstants.METABIKS_ATTR_RELATION_TYPE);
                        execNamedCommand("updateNodeKind4TreeKind", treeKindId, srcNodeKindId, dstNodeKindId, relationType.atrLinValue);
                    } else {
                        throw new LameRuntimeException("Nie było linku do oryginalnego node_kind lub nie podano typ relacji: " + srcBean.name);
                    }
                }
            }
        }
    }

    protected Map<String, AttributeBean> buildNodeAttributesMap(Integer nodeId) {
        Map<String, AttributeBean> res = new HashMap<String, AttributeBean>();
        List<AttributeBean> attributes = getAttributesByNameValueNode(null, null, nodeId);
        for (AttributeBean bean : attributes) {
            res.put(bean.atrName, bean);
        }
        if (res.containsKey(BIKConstants.METABIKS_ATTR_ORIGINAL)) {
            updateAttributeFromOriginalIfNecessary(res, BIKConstants.METABIKS_ATTR_OVERWRITE_TYPE_ATTR, BIKConstants.METABIKS_ATTR_TYPE_ATTR);
            updateAttributeFromOriginalIfNecessary(res, BIKConstants.METABIKS_ATTR_VALUE_OPT, BIKConstants.METABIKS_ATTR_VALUE_OPT);
        }
        return res;
    }

    private void updateAttributeFromOriginalIfNecessary(Map<String, AttributeBean> attrsMap, String attrName, String originalAttrName) {
        if (!attrsMap.containsKey(attrName)) {
            return;
        }
        AttributeBean ab = attrsMap.get(attrName);
        if (BaseUtils.isStrEmptyOrWhiteSpace(ab.atrLinValue)) {
            AttributeBean orignalAb = attrsMap.get(BIKConstants.METABIKS_ATTR_ORIGINAL);
            Integer originalNodeId = BaseUtils.tryParseInteger(orignalAb.atrLinValue.substring(0, orignalAb.atrLinValue.indexOf("_")));
            Map<String, AttributeBean> originalAttrsMap = buildNodeAttributesMap(originalNodeId);
            if (originalAttrsMap.containsKey(originalAttrName)) {
                ab.atrLinValue = originalAttrsMap.get(originalAttrName).atrLinValue;
            }
        }
        attrsMap.put(attrName, ab);
    }

    private String getRequiredAttributeInSet(Map<String, AttributeBean> attributesMap, String attrName) {
        if (!attributesMap.containsKey(attrName)) {
            throw new LameRuntimeException("Brak wymaganego atrybutu " + attrName);
        }
        return getAttributeInSet(attributesMap, attrName, null);
    }

    private void implementAttributes(String confTreeCode, Map<Integer, Integer> attrDictConfMap, Map<Integer, Integer> attrDefConfMap) {
        //Kategorie atrybutów
        Integer attrRootNodeId = getNodeId(BaseUtils.ensureObjIdEndWith(BIKConstants.METABIKS_OBJ_ID_ATTRIBUTE_DEF, "|"), confTreeCode);
        List<TreeNodeBean> catalogs = getChildrenNodeBeansByParentId(attrRootNodeId);
//        Set<String> allCatalogNames = new HashSet<String>();
//        for (TreeNodeBean bean : catalogs) { //nazwa powinna być unikalna
//            if (allCatalogNames.contains(bean.name)) {
//                throw new LameRuntimeException("Nazwy kategorii atrybutów nie mogą powtórzyć się: " + bean.name);
//            } else {
//                allCatalogNames.add(bean.name);
//            }
//        }
//        List<Map<String, Object>> rs = execNamedQueryCFN("getCategory", FieldNameConversion.ToLowerCase);
//        for (Map<String, Object> m : rs) {
//            String catalogName = (String) m.get("name");
//            if (!allCatalogNames.contains(catalogName)) {
////                deleteCategory((Integer) m.get("id")); // kasuj te, które nie zostaną wdrozone - narazie nic nie kasujemy
//            } else {
//                allCatalogNames.remove(catalogName);
//            }
//        }
//        for (String name : allCatalogNames) {
//            addOrUpdateCategory(null, name); //dodac nowe kategorie
//        }
        for (TreeNodeBean bean : catalogs) {
            Integer catalogId = addOrUpdateCategory(attrDictConfMap.get(bean.id), bean.name);
            attrDictConfMap.put(bean.id, catalogId);
        }
        //Atrybuty
        Set<String> allAttributeNames = new HashSet<String>();
        for (TreeNodeBean bean : catalogs) {
            List<Map<String, Object>> rs = execNamedQueryCFN("getCategoryByName", FieldNameConversion.ToLowerCase, bean.name);
            if (BaseUtils.isCollectionEmpty(rs) || rs.size() != 1) {
                throw new LameRuntimeException("Kategoria atrybutów nie istnieje lub jest inna kategoria o tej samej nazwie");
            }

            List<TreeNodeBean> metaAttributesInCategory = getChildrenNodeBeansByParentId(bean.id);
            for (TreeNodeBean metaAttr : metaAttributesInCategory) {
                if (allAttributeNames.contains(metaAttr.name)) {
                    throw new LameRuntimeException("Nazwa atrybutu nie jest unikalna" + metaAttr.name);
                } else {
                    allAttributeNames.add(metaAttr.name);
                }
            }
        }
//        for (AttributeBean bean : getAttrDefAndCategory()) { //wszystkie attrybuty w systemie
//            String attrDefName = bean.atrName;
//            if (!allAttributeNames.contains(attrDefName)) {
////                deleteAttributeDict(bean.attrDefId); // narazie nic nie kasujemy
//            }
//        }

        for (TreeNodeBean bean : catalogs) {
            List<Map<String, Object>> rs = execNamedQueryCFN("getCategoryByName", FieldNameConversion.ToLowerCase, bean.name);
            Integer categoryId = (Integer) rs.get(0).get("id");
            List<TreeNodeBean> metaAttributesInCategory = getChildrenNodeBeansByParentId(bean.id);

            for (TreeNodeBean metaAttr : metaAttributesInCategory) {
                Map<String, AttributeBean> attributesMap = buildNodeAttributesMap(metaAttr.id);
                String attrType = getRequiredAttributeInSet(attributesMap, BIKConstants.METABIKS_ATTR_TYPE_ATTR);
                String attrValueList = getAttributeInSet(attributesMap, BIKConstants.METABIKS_ATTR_VALUE_OPT, null);
                String attrHint = getAttributeInSet(attributesMap, BIKConstants.METABIKS_ATTR_ATTR_DESC, null);
                boolean displayAsNumer = "true".equalsIgnoreCase(getAttributeInSet(attributesMap, BIKConstants.METABIKS_ATTR_DISPLAY_AS_NUMBER, "false"));
                boolean usedInDrools = "true".equalsIgnoreCase(getAttributeInSet(attributesMap, BIKConstants.METABIKS_ATTR_USED_IN_DROOLS, "false"));

                if (attrDefConfMap.get(metaAttr.id) == null) {
                    Integer attrId = addAttributeDict(metaAttr.name, categoryId, attrType, attrValueList, displayAsNumer, usedInDrools);
                    attrDefConfMap.put(metaAttr.id, attrId);
                } else {
                    updateItemInCategory(attrDefConfMap.get(metaAttr.id), categoryId, metaAttr.name, attrType, attrValueList, displayAsNumer, usedInDrools);
                }
                updateAttrHint(metaAttr.name, attrHint, 0);
            }
        }
    }

    private String getAttributeInSet(Map<String, AttributeBean> attributesMap, String attrName, String defaultValue) {
        return attributesMap.containsKey(attrName) ? attributesMap.get(attrName).atrLinValue : defaultValue;
    }

//    private void implementTreeKind(String confTreeCode) {
//        Integer nodeKindRootNodeId = getNodeId(BaseUtils.ensureObjIdEndWith(BIKConstants.METABIKS_OBJ_ID_TREE_KIND_DEF, "|"), confTreeCode);
//        if (nodeKindRootNodeId == null) {
//            throw new LameRuntimeException("Nie prawidłowa struktura drzewa konfiguracji");
//        }
//        List<TreeNodeBean> treeKinds = getChildrenNodeBeansByParentId(nodeKindRootNodeId);
//        Set<String> treeKindNames = new HashSet<String>();
//        for (TreeNodeBean treeKindBean : treeKinds) {
//            if (treeKindNames.contains(treeKindBean.name)) {
//                throw new LameRuntimeException("Nazwa definicji obiektu musi być unikalna");
//            } else {
//                treeKindNames.add(treeKindBean.name);
//            }
//        }
//        Map<Integer, Integer> updatedNodeKindIds = new HashMap<Integer, Integer>();
//        for (TreeNodeBean treeKindNodeBean : treeKinds) {
//            Map<String, AttributeBean> alltreeKindAttrs = buildNodeAttributesMap(treeKindNodeBean.id);
//            AttributeBean allowLinkingAttr = alltreeKindAttrs.get(BIKConstants.METABIKS_ATTR_ALLOW_LINKING);
//            Integer allowLinking = allowLinkingAttr == null || BIKConstants.NIE.equalsIgnoreCase(allowLinkingAttr.atrLinValue) ? 0 : 1;
//            List<Integer> treeKindIds = execNamedQuerySingleColAsList("getTreeKindIdsByCaption", null, treeKindNodeBean.name);
//            if (!BaseUtils.isCollectionEmpty(treeKindIds) && treeKindIds.size() > 1) {
//                throw new LameRuntimeException("Nie ma zgody z bazą");
//                //są 2 tree_kind o takiej samej nazwie w bazie,
//                //natomiast może jeden z nich był ukryty, nie chemy pokazywac klientom ze nie fizycznie usuwamy tree_kind, dlatego jest ten ogólny komunikat
//            }
//            Integer treeKindId = BaseUtils.isCollectionEmpty(treeKindIds) ? null : treeKindIds.get(0);
//            String caption = treeKindNodeBean.name;
//            String branchCaption = "";
//            String leafCaption = "";
//            String branchIcon = "";
//            String leafIcon = "";
//            String lang = "pl";
//            TreeKindBean tkb = new TreeKindBean();
//            tkb.caption = treeKindNodeBean.name;
//            tkb.branchCaption = branchCaption;
//            tkb.branchIcon = branchIcon;
//            tkb.leafCaption = leafCaption;
//            tkb.leafIcon = leafIcon;
//            tkb.allowLinking = allowLinking;
//            tkb.lang = lang;
//            tkb = addOrUpdateTreeType(Arrays.asList(tkb), treeKindId); //sztuczny tree_kind w celu pobierania tree_kind_code
//            treeKindId = tkb.id;
//
//            List<TreeNodeBean> nodeKindsInTreeKind = getChildrenNodeBeansByParentId(treeKindNodeBean.id);
//
//            for (TreeNodeBean nodeKindInTreeKind : nodeKindsInTreeKind) {
//                Map<String, AttributeBean> nkitkAttrs = buildNodeAttributesMap(nodeKindInTreeKind.id);
//                if (!nkitkAttrs.containsKey(BIKConstants.METABIKS_ATTR_ORIGINAL)) {
//                    throw new LameRuntimeException("Nie ma definicji node_kind:" + nodeKindInTreeKind.name);
//                }
//                AttributeBean orgNodeKindAttr = nkitkAttrs.get(BIKConstants.METABIKS_ATTR_ORIGINAL);
//                String typeInTree = BIKConstants.DODATKOWY;
//                if (nkitkAttrs.containsKey(BIKConstants.METABIKS_ATTR_BRANCH_TYPE_IN_TREE)) {
//                    AttributeBean attr = nkitkAttrs.get(BIKConstants.METABIKS_ATTR_BRANCH_TYPE_IN_TREE);
//                    typeInTree = attr.atrLinValue;
//                }
//
//                Integer originalNodeKindNode = BaseUtils.tryParseInteger(BaseUtils.splitBySep(orgNodeKindAttr.atrLinValue, "_").get(0));
//                Integer nodeKindId = implementOriginalNodeKindNode(tkb.code, nodeKindInTreeKind.name, typeInTree, originalNodeKindNode, branchCaption, branchIcon, leafCaption, leafIcon);
//                updatedNodeKindIds.put(nodeKindInTreeKind.id, nodeKindId);
//                if (BIKConstants.DODATKOWY.equalsIgnoreCase(typeInTree)) {
//                    //na szybko ustawie wszytko jako galaz
//                    execNamedCommand("addNodeKind4TreeKind", treeKindId, nodeKindId);
//                }
//            }
//
//            //TO-DO
//            //kasowanc z bik_node_kind_4_tree_kind wpisy, ktore nie pojawiaja w metaBIKS
//            //zakladam co najmniej jeden branch i leaf byl dodany
//            addOrUpdateTreeKind(treeKindId, caption, branchCaption, leafCaption, branchIcon, leafIcon, allowLinking, lang);
//        }
//        for (TreeNodeBean treeKindNodeBean : treeKinds) {
//            List<TreeNodeBean> nodeKindsInTreeKind = getChildrenNodeBeansByParentId(treeKindNodeBean.id);
//            for (TreeNodeBean nodeKindInTreeKind : nodeKindsInTreeKind) {
//                List<JoinedObjBean> allowedJoinedObjs = getJoinedObjs(nodeKindInTreeKind.id, null);
//                Integer srcNodeKindId = updatedNodeKindIds.get(nodeKindInTreeKind.id);
//                for (JoinedObjBean bean : allowedJoinedObjs) {
//                    if (confTreeCode.equals(bean.tabId)) {
//                        List<Integer> nodeKindIds = execNamedQuerySingleColAsList("getNodeKindIdsByCaption", null, bean.dstName);
//                        if (!BaseUtils.isCollectionEmpty(nodeKindIds) && nodeKindIds.size() != 1) {
//                            throw new LameRuntimeException("node_kind nie był przypisane do zadnego tree_kind lub Nie ma zgody z bazą");
//                        }
//                        Integer dstNodeKindId = nodeKindIds.get(0);
//                        execNamedCommand("saveAllowedJoinedNodeKinds", srcNodeKindId, dstNodeKindId);
//                    }
//                }
//            }
//        }
//    }
    protected Integer addNodeKind4TreeKindInConfigTree(String treeKindObjId, Integer treeKindNodeId, String treeKindBranchId, String confTreeCode, Integer nodeKindId, Integer configNodeKindId) {
        NodeKindBean nodeKind = getNodeKind(nodeKindId);
        String branchNodeKindObjId = BaseUtils.ensureObjIdEndWith(treeKindObjId + nodeKind.code, "|");
        Integer nodeKindNodeId = getOrUpdateSimpleNode(treeKindNodeId, treeKindBranchId, branchNodeKindObjId, confTreeCode, configNodeKindId, nodeKind.code, null);
        String originalNodeKindObjId = BaseUtils.ensureObjIdEndWith(BaseUtils.mergeNotEmptyParamsWithSep("|", BIKConstants.METABIKS_OBJ_ID_NODE_KIND_DEF, nodeKind.code), "|");
        Integer originalAttributeNodeId = getNodeId(originalNodeKindObjId, confTreeCode);
        addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_ORIGINAL, originalAttributeNodeId + "_" + confTreeCode + "," + nodeKind.code, nodeKindNodeId);
//        addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_BRANCH_TYPE_IN_TREE, branchTypeInTree, nodeKindNodeId);

        return nodeKindNodeId;
    }

    private void buildConfigTreeTreeKindBranch(String confTreeCode, Map<String, Integer> nkMap, Map<Integer, Integer> nodeKinds2NodeIds) {
        String treeKindRootObjId = BaseUtils.ensureObjIdEndWith(BIKConstants.METABIKS_OBJ_ID_TREE_KIND_DEF, "|");
        Integer treeKindRootNodeId = getOrUpdateSimpleNode(null, "", treeKindRootObjId, confTreeCode, nkMap.get(BIKConstants.NODE_KIND_META_DEFINITION_GROUP), BIKConstants.METABIKS_OBJ_ID_TREE_KIND_DEF, null);

        String treeKindRootBranchId = BaseUtils.ensureObjIdEndWith("" + treeKindRootNodeId, "|");
        List<TreeKindBean> allTreeKinds = getTreeKinds();
        for (TreeKindBean bean : allTreeKinds) {
            if (!BIKConstants.TREE_KIND_META_BIKS.equalsIgnoreCase(bean.code)
                    && !BIKConstants.TREE_KIND_META_BIKS_MENU_CONF.equalsIgnoreCase(bean.code)) {
                String treeKindObjId = BaseUtils.ensureObjIdEndWith(treeKindRootObjId + bean.code, "|");
                Integer treeKindNodeId = getOrUpdateSimpleNode(treeKindRootNodeId, treeKindRootBranchId, treeKindObjId, confTreeCode, nkMap.get(BIKConstants.NODE_KIND_META_TREE_KIND), bean.code, null, false);
                String treeKindBranchId = BaseUtils.ensureObjIdEndWith(treeKindRootBranchId + treeKindNodeId, "|");
                addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_TREE_KIND_CAPTION_PL, bean.treeKindCaption, treeKindNodeId);
                addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_ALLOW_LINKING, bean.allowLinking == 0 ? BIKConstants.NO : BIKConstants.YES, treeKindNodeId);

                Map<Integer, Integer> allNodeKindsInTreeDef = addNodeKind4TreeKind(confTreeCode, bean, treeKindNodeId, treeKindBranchId, treeKindObjId, nkMap, nodeKinds2NodeIds);
                Set<Integer> mainBranches = new HashSet<Integer>();
                Set<Integer> branches = new HashSet<Integer>();
                Set<Integer> leaves = new HashSet<Integer>();

                for (NodeKind4TreeKind nk4tk : bean.additionalNodeKindsId) {
                    if (nk4tk.srcNodeKindId == null) {
                        if (nk4tk.dstNodeKindId != null) {
                            mainBranches.add(allNodeKindsInTreeDef.get(nk4tk.dstNodeKindId));
                        }
                    } else if (nk4tk.dstNodeKindId == null) {
                        leaves.add(allNodeKindsInTreeDef.get(nk4tk.srcNodeKindId));
                    } else if (BIKConstants.CHILD.equalsIgnoreCase(nk4tk.relationType)) {
                        branches.add(allNodeKindsInTreeDef.get(nk4tk.srcNodeKindId));
                    }
                }
                List<TreeNodeBean> allNodeKindsInTreeList = getChildrenNodeBeansByParentId(treeKindNodeId);
                for (TreeNodeBean nb : allNodeKindsInTreeList) {
                    List<String> nodeKindInTree = new ArrayList<String>();
                    if (mainBranches.contains(nb.id)) {
                        nodeKindInTree.add(BIKConstants.MAIN_BRANCH);
                    }
                    if (branches.contains(nb.id)) {
                        nodeKindInTree.add(BIKConstants.BRANCH);
                    }
                    if (leaves.contains(nb.id)) {
                        nodeKindInTree.add(BIKConstants.LEAF);
                    }
                    addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_BRANCH_TYPE_IN_TREE, BaseUtils.mergeWithSepEx(nodeKindInTree, "+"), nb.id);
                }
            }
        }
    }

//    private void ensureAllMetaAttributesAreCreatedAndAssignedToConfigTree(String confTreeCode) {
//        List<Map<String, Object>> rs = execNamedQueryCFN("getCategoryByName", FieldNameConversion.ToLowerCase, BIKConstants.METABIKS_META_ATTR_CATEGORY);
//        if (BaseUtils.isCollectionEmpty(rs)) {
//            addOrUpdateCategory(null, BIKConstants.METABIKS_META_ATTR_CATEGORY);
//        } else if (rs.size() > 1) {
//            throw new LameRuntimeException("Są dwa kategorie do meta-atrybutów");
//        }
//        rs = execNamedQueryCFN("getCategoryByName", FieldNameConversion.ToLowerCase, BIKConstants.METABIKS_META_ATTR_CATEGORY);
//        Integer metaCatalogId = (Integer) rs.get(0).get("id");
//        List<AttributeBean> attributes = getAttrDefAndCategory();
//        Map<String, AttributeBean> allMetaAttrsMap = new HashMap<String, AttributeBean>();
//        for (AttributeBean bean : attributes) {
//            allMetaAttrsMap.put(bean.atrName, bean);
//        }
//
//        addOrUpdateMetaAttribute(allMetaAttrsMap, BIKConstants.METABIKS_ATTR_ICON_NAME, metaCatalogId, AttributeType.combobox.name(), BaseUtils.mergeWithSepEx(getIcons(), ","), false, false);
//        addOrUpdateMetaAttribute(allMetaAttrsMap, BIKConstants.METABIKS_ATTR_IS_FOLDER, metaCatalogId, AttributeType.comboBooleanBox.name(), BaseUtils.mergeWithSep(new String[]{BIKConstants.YES, BIKConstants.NO}, ","), false, false);
//        addOrUpdateMetaAttribute(allMetaAttrsMap, BIKConstants.METABIKS_ATTR_CHILDREN_KINDS, metaCatalogId, AttributeType.shortText.name(), null, false, false);
//        addOrUpdateMetaAttribute(allMetaAttrsMap, BIKConstants.METABIKS_ATTR_ATTR_CHILDREN_KINDS, metaCatalogId, AttributeType.shortText.name(), null, false, false);
//        addOrUpdateMetaAttribute(allMetaAttrsMap, BIKConstants.METABIKS_ATTR_TYPE_ATTR, metaCatalogId, AttributeType.combobox.name(), BaseUtils.mergeWithSepEx(Arrays.asList(AttributeType.values()), ","), false, false);
//        addOrUpdateMetaAttribute(allMetaAttrsMap, BIKConstants.METABIKS_ATTR_NODE_KIND_CAPTION_PL, metaCatalogId, AttributeType.shortText.name(), null, false, false);
//        addOrUpdateMetaAttribute(allMetaAttrsMap, BIKConstants.METABIKS_ATTR_VALUE_OPT, metaCatalogId, AttributeType.shortText.name(), null, false, false);
//        addOrUpdateMetaAttribute(allMetaAttrsMap, BIKConstants.METABIKS_ATTR_DISPLAY_AS_NUMBER, metaCatalogId, AttributeType.comboBooleanBox.name(), BaseUtils.mergeWithSep(new String[]{BIKConstants.YES, BIKConstants.NO}, ","), false, false);
//        addOrUpdateMetaAttribute(allMetaAttrsMap, BIKConstants.METABIKS_ATTR_USED_IN_DROOLS, metaCatalogId, AttributeType.comboBooleanBox.name(), BaseUtils.mergeWithSep(new String[]{BIKConstants.YES, BIKConstants.NO}, ","), false, false);
//        addOrUpdateMetaAttribute(allMetaAttrsMap, BIKConstants.METABIKS_ATTR_ORIGINAL, metaCatalogId, AttributeType.hyperlinkInMulti.name(), null, false, false);
//        addOrUpdateMetaAttribute(allMetaAttrsMap, BIKConstants.METABIKS_ATTR_DEFAULT_VALUE, metaCatalogId, AttributeType.shortText.name(), null, false, false);
//        addOrUpdateMetaAttribute(allMetaAttrsMap, BIKConstants.METABIKS_ATTR_IS_REQUIRED, metaCatalogId, AttributeType.comboBooleanBox.name(), BaseUtils.mergeWithSep(new String[]{BIKConstants.YES, BIKConstants.NO}, ","), false, false);
//        addOrUpdateMetaAttribute(allMetaAttrsMap, BIKConstants.METABIKS_ATTR_ALLOW_LINKING, metaCatalogId, AttributeType.comboBooleanBox.name(), BaseUtils.mergeWithSep(new String[]{BIKConstants.YES, BIKConstants.NO}, ","), false, false);
//        addOrUpdateMetaAttribute(allMetaAttrsMap, BIKConstants.METABIKS_ATTR_BRANCH_TYPE_IN_TREE, metaCatalogId, AttributeType.combobox.name(), BaseUtils.mergeWithSep(new String[]{BIKConstants.MAIN_BRANCH + "+" + BIKConstants.BRANCH, BIKConstants.MAIN_BRANCH + "+" + BIKConstants.LEAF, BIKConstants.BRANCH, BIKConstants.LEAF}, ","), false, false);
//        addOrUpdateMetaAttribute(allMetaAttrsMap, BIKConstants.METABIKS_ATTR_RELATION_TYPE, metaCatalogId, AttributeType.combobox.name(), BaseUtils.mergeWithSep(new String[]{BIKConstants.ASSOCIATION, BIKConstants.LINKING, BIKConstants.CHILD, BIKConstants.HYPERLINK}, ","), false, false);
//
//        addRequiredMetaAttrs2ConfigTree(confTreeCode);
//    }
    private Map<Integer, Integer> buildConfigTreeAttributesBranch(String confTreeCode, Map<String, Integer> nkMap) {
        String attrDefRootObjId = BaseUtils.ensureObjIdEndWith(BIKConstants.METABIKS_OBJ_ID_ATTRIBUTE_DEF, "|");
        Integer attrDefRootNodeId = getOrUpdateSimpleNode(null, "", attrDefRootObjId, confTreeCode, nkMap.get(BIKConstants.NODE_KIND_META_DEFINITION_GROUP), BIKConstants.METABIKS_OBJ_ID_ATTRIBUTE_DEF, null);
        String attrDefRootBranchId = BaseUtils.ensureObjIdEndWith("" + attrDefRootNodeId, "|");
        List<AttributeBean> attributes = getAttrDefAndCategory();
        Map<String, List<AttributeBean>> attrCatalogsMap = new HashMap<String, List<AttributeBean>>();
        for (AttributeBean bean : attributes) {
            List<AttributeBean> attrList = attrCatalogsMap.get(bean.atrCategory);
            if (attrList == null) {
                attrList = new ArrayList<AttributeBean>();
            }
            if (!BaseUtils.isStrEmptyOrWhiteSpace(bean.atrName)) {
                attrList.add(bean);
            }
            attrCatalogsMap.put(bean.atrCategory, attrList);
        }

        Map<Integer, Integer> res = new HashMap<Integer, Integer>();
        for (String attrCategoryName : attrCatalogsMap.keySet()) {
            if (!BIKConstants.METABIKS_META_ATTR_CATEGORY.equalsIgnoreCase(attrCategoryName)) {
                //tworenie katalogów
                String attrCategoryObjId = BaseUtils.ensureObjIdEndWith(attrDefRootObjId + attrCategoryName, "|");
                Integer attrCategoryNodeId = getOrUpdateSimpleNode(attrDefRootNodeId, attrDefRootBranchId, attrCategoryObjId, confTreeCode, nkMap.get(BIKConstants.NODE_KIND_META_ATTRIBUTES_CATALOG), attrCategoryName, null);
                String attrCategoryBranchId = BaseUtils.ensureObjIdEndWith(attrDefRootBranchId + attrCategoryNodeId, "|");
                List<AttributeBean> attrList = attrCatalogsMap.get(attrCategoryName);
                for (AttributeBean bean : attrList) {
                    String attrObjId = BaseUtils.ensureObjIdEndWith(attrCategoryObjId + bean.atrName, "|");
                    Integer attrNodeId = getOrUpdateSimpleNode(attrCategoryNodeId, attrCategoryBranchId, attrObjId, confTreeCode, nkMap.get(BIKConstants.NODE_KIND_META_ATTRIBUTE_TYPE), bean.atrName, null);
                    addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_TYPE_ATTR, bean.typeAttr, attrNodeId);
                    addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_ATTR_DESC, bean.hint, attrNodeId);
                    addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_VALUE_OPT, bean.valueOpt, attrNodeId);
                    addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_DISPLAY_AS_NUMBER, bean.displayAsNumber ? BIKConstants.YES : BIKConstants.NO, attrNodeId);
                    addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_USED_IN_DROOLS, bean.usedInDrools ? BIKConstants.YES : BIKConstants.NO, attrNodeId);

                    res.put(bean.id, attrNodeId);
                }
            }
        }
        return res;
    }

    private void addAttributes2NodeKind(Integer nodeKindId, String nodeKindObjId, String nodeKindBranchId, Integer nodeKindMetaNodeId, String confTreeCode, Map<String, Integer> nkMap, Map<Integer, Integer> attrs2NodeIds) {
        List<Map<String, Object>> attrListRs = execNamedQueryCFN("getAvailableAttribute4NodeKind", null, nodeKindId);
        for (Map<String, Object> prop : attrListRs) {
            Integer attrDefId = (Integer) prop.get("attr_def_id");
            String attrName = (String) prop.get("attr_name");
            String nodeKindAttributeObjId = BaseUtils.ensureObjIdEndWith(nodeKindObjId + attrName, "|");
            Integer nodeKindAttributeNodeId = getOrUpdateSimpleNode(nodeKindMetaNodeId, nodeKindBranchId, nodeKindAttributeObjId, confTreeCode, nkMap.get(BIKConstants.NODE_KIND_META_ATTR_4_NODE_KIND), attrName, null);
            //                category_name, ad.name as attr_name, default_value, is_required, coalesce(attr.type_attr, ad.type_attr) as type_attr, coalesce(attr.value_opt, ad.value_opt) as value_opt, ad.display_as_number, ad.in_drools
//                String attrCategoryName = (String) prop.get("category_name");
//                String originalAttributeObjId = BaseUtils.ensureObjIdEndWith(BaseUtils.mergeNotEmptyParamsWithSep("|", BIKConstants.METABIKS_OBJ_ID_ATTRIBUTE_DEF, attrCategoryName, attrName), "|");
//                Integer originalAttributeNodeId = getNodeId(originalAttributeObjId, confTreeCode);
            Integer originalAttributeNodeId = attrs2NodeIds.get(attrDefId);
            addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_ORIGINAL, originalAttributeNodeId + "_" + confTreeCode + "," + attrName, nodeKindAttributeNodeId);

            String defaultValue = (String) prop.get("default_value");
            addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_DEFAULT_VALUE, defaultValue, nodeKindAttributeNodeId);

            Integer isRequired = (Integer) prop.get("is_required");
            addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_IS_REQUIRED, isRequired == 1 ? BIKConstants.YES : BIKConstants.NO, nodeKindAttributeNodeId);

            Integer isPublic = (Integer) prop.get("is_public");
            addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_IS_PUBLIC, isPublic == 1 ? BIKConstants.YES : BIKConstants.NO, nodeKindAttributeNodeId);

            Integer isEmpty = (Integer) prop.get("is_empty");
            addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_IS_EMPTY, isEmpty == 1 ? BIKConstants.YES : BIKConstants.NO, nodeKindAttributeNodeId);

            String attrType = prop.get("type_attr") == null ? null : (String) prop.get("type_attr");
            addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_OVERWRITE_TYPE_ATTR, attrType, nodeKindAttributeNodeId);

            String valueOpt = prop.get("value_opt") == null ? null : (String) prop.get("value_opt");
            addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_VALUE_OPT, valueOpt, nodeKindAttributeNodeId);

            /*
             String visualOrder = prop.get("visual_order") == null ? "0" : prop.get("visual_order").toString();
             addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_VISUAL_ORDER, visualOrder, nodeKindAttributeNodeId);*/
        }
    }

    private Map<Integer, Integer> buildConfigTreeNodeKindsBranch(String confTreeCode, Map<String, Integer> nkMap, Map<Integer, Integer> attrs2NodeIds) {
        String nodeKindRootObjId = BaseUtils.ensureObjIdEndWith(BIKConstants.METABIKS_OBJ_ID_NODE_KIND_DEF, "|");
        Integer nodeKindRootNodeId = getOrUpdateSimpleNode(null, "", nodeKindRootObjId, confTreeCode, nkMap.get(BIKConstants.NODE_KIND_META_DEFINITION_GROUP), BIKConstants.METABIKS_OBJ_ID_NODE_KIND_DEF, null);
        String nodeKindRootBranchId = BaseUtils.ensureObjIdEndWith("" + nodeKindRootNodeId, "|");
        Map<Integer, Integer> res = new HashMap<Integer, Integer>();
        //Typy dynamiczne
        List<Map<String, Object>> rs = execNamedQueryCFN("getNodeKindInfo", null);
        for (Map<String, Object> m : rs) {
            Integer nodeKindId = (Integer) m.get("node_kind_id");
            String nodeKindName = (String) m.get("node_kind_code");
            String nodeKindObjId = BaseUtils.ensureObjIdEndWith(nodeKindRootObjId + nodeKindName, "|");
            Integer nodeKindMetaNodeId = getOrUpdateSimpleNode(nodeKindRootNodeId, nodeKindRootBranchId, nodeKindObjId, confTreeCode, nkMap.get(BIKConstants.NODE_KIND_META_NODE_KIND), nodeKindName, null, false);
            res.put(nodeKindId, nodeKindMetaNodeId);
            String nodeKindBranchId = BaseUtils.ensureObjIdEndWith(nodeKindRootBranchId + nodeKindMetaNodeId, "|");

            String nodeKindCaption = (String) m.get("node_kind_caption");
            addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_NODE_KIND_CAPTION_PL, nodeKindCaption, nodeKindMetaNodeId);

            String nodeKindIcon = (String) m.get("icon_name");
            addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_ICON_NAME, nodeKindIcon, nodeKindMetaNodeId);

//            Integer isFolder = (Integer) m.get("is_folder");
//            addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_IS_FOLDER, "1".equals(BaseUtils.safeToStringDef(isFolder, "1")) ? BIKConstants.YES : BIKConstants.NO, nodeKindMetaNodeId);
            String childrenKinds = (String) m.get("children_kinds");
            addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_CHILDREN_KINDS, childrenKinds, nodeKindMetaNodeId);

            String childrenAttrNames = (String) m.get("children_attr_names");
            addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_ATTR_CHILDREN_KINDS, childrenAttrNames, nodeKindMetaNodeId);

            String reportName = (String) m.get("jasper_reports");
            addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_REPORTS, reportName, nodeKindMetaNodeId);

            Integer isRegistry = (Integer) m.get("is_registry");
            addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_REGISTRY, "1.".equals(BaseUtils.safeToStringDef(isRegistry, "0")) ? BIKConstants.YES : BIKConstants.NO, nodeKindMetaNodeId);

            addAttributes2NodeKind(nodeKindId, nodeKindObjId, nodeKindBranchId, nodeKindMetaNodeId, confTreeCode, nkMap, attrs2NodeIds);
            updateHelpAttribute4NodeKind(nodeKindMetaNodeId, nodeKindName);
        }
        //Typy wbudowane
        rs = execNamedQueryCFN("getBuildInNodeKinds", FieldNameConversion.ToLowerCase, res.keySet());
        for (Map<String, Object> m : rs) {
            Integer nodeKindId = (Integer) m.get("node_kind_id");
            String nodeKindName = (String) m.get("node_kind_code");
            String nodeKindObjId = BaseUtils.ensureObjIdEndWith(nodeKindRootObjId + nodeKindName, "|");
            Integer nodeKindMetaNodeId = getOrUpdateSimpleNode(nodeKindRootNodeId, nodeKindRootBranchId, nodeKindObjId, confTreeCode, nkMap.get(BIKConstants.NODE_KIND_META_BUILT_IN_NODE_KIND), nodeKindName, null);
            String nodeKindBranchId = BaseUtils.ensureObjIdEndWith(nodeKindRootBranchId + nodeKindMetaNodeId, "|");

            addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_NODE_KIND_CAPTION_PL, (String) m.get("node_kind_caption"), nodeKindMetaNodeId);

            res.put(nodeKindId, nodeKindMetaNodeId);
            addAttributes2NodeKind(nodeKindId, nodeKindObjId, nodeKindBranchId, nodeKindMetaNodeId, confTreeCode, nkMap, attrs2NodeIds);
            updateHelpAttribute4NodeKind(nodeKindMetaNodeId, nodeKindName);
        }
        return res;
    }

//    private void addRequiredMetaAttrs2ConfigTree(String confTreeCode) {
//        List<AttributeBean> attrDefAndCategory = getAttrDefAndCategory();
//        Map<String, AttributeBean> allAttrsMap = new HashMap<String, AttributeBean>();
//        for (AttributeBean bean : attrDefAndCategory) {
//            allAttrsMap.put(bean.atrName, bean);
//        }
//        Set<Integer> nodeKindIds = execNamedQuerySingleColAsSet("getConfigTreeNodeKindIs", "node_kind_id", confTreeCode);
//        for (String metaAttr : BIKConstants.METABIKS_REQUIRED_META_ATTRS) {
//            AttributeBean bean = allAttrsMap.get(metaAttr);
//            addOrDeleteAttributeTypeForKind(bean.attrDefId, nodeKindIds, false);
//        }
//    }
    private void addOrUpdateMetaAttribute(Map<String, AttributeBean> allMetaAttrsMap, String metaAttrName, Integer metaCatalogId, String attrType, String valueOpt, boolean displayAsNumber, boolean useInDrools) {
        if (!allMetaAttrsMap.containsKey(metaAttrName)) {
            addAttributeDict(metaAttrName, metaCatalogId, attrType, valueOpt, displayAsNumber, useInDrools);
        } else {
            AttributeBean attrBean = allMetaAttrsMap.get(metaAttrName);
            updateItemInCategory(attrBean.id, metaCatalogId, metaAttrName, attrType, valueOpt, displayAsNumber, useInDrools);
        }
    }

    private Integer getOrUpdateSimpleNode(Integer parentNodeId, String parentBranchIds, String objId, String treeCode, Integer nodeKindId, String nodeName, Integer linkingNodeId) {
        return getOrUpdateSimpleNode(parentNodeId, parentBranchIds, objId, treeCode, nodeKindId, nodeName, linkingNodeId, true);
    }

    private Integer getOrUpdateSimpleNode(Integer parentNodeId, String parentBranchIds, String objId, String treeCode, Integer nodeKindId, String nodeName, Integer linkingNodeId, boolean fixName) {
        objId = BaseUtils.ensureObjIdEndWith(objId, "|");
        int treeId = getTreeIdByTreeCode(treeCode);
        Integer nodeId = getNodeId(objId, treeCode);
        if (nodeId == null) {
            createTreeNode(parentNodeId, nodeKindId, nodeName, objId, treeId, linkingNodeId, fixName);
        } else {
            TreeNodeBean node = new TreeNodeBean(nodeId, parentNodeId, nodeKindId, nodeName, objId, treeId, linkingNodeId);
            node.branchIds = BaseUtils.ensureObjIdEndWith(parentBranchIds + nodeId, "|");
            updateTreeNode(node);
        }
        return getNodeId(objId, treeCode);
    }

//    private Integer implementOriginalNodeKindNode(String treeKind, String nodeKindName, String nodeKindTypeInTree, Integer originalNodeKindNode, String branchCaption, String branchIcon, String leafCaption, String leafIcon) {
//        Map<String, AttributeBean> originalAttrs = buildNodeAttributesMap(originalNodeKindNode);
//        AttributeBean attr = originalAttrs.get(BIKConstants.METABIKS_ATTR_ICON_NAME);
//        if (attr == null) {
//            throw new LameRuntimeException("node_kind " + nodeKindName + " nie ma zdefiniowanej ikony");
//        }
//        String iconName = attr.atrLinValue;
//        if (BaseUtils.isStrEmptyOrWhiteSpace(branchCaption) && BIKConstants.BRANCH.equalsIgnoreCase(nodeKindTypeInTree)) {
//            branchCaption = nodeKindName;
//            branchIcon = iconName;
//        } else if (BaseUtils.isStrEmptyOrWhiteSpace(leafCaption) && BIKConstants.LEAF.equalsIgnoreCase(nodeKindTypeInTree)) {
//            leafCaption = nodeKindName;
//            leafIcon = iconName;
//        }
//        List<Integer> nodeKindIds = execNamedQuerySingleColAsList("getNodeKindIdsByCaption", null, nodeKindName);
//        if (!BaseUtils.isCollectionEmpty(nodeKindIds) && nodeKindIds.size() > 1) {
//            throw new LameRuntimeException("Nie ma zgody z bazą");
//        }
//        Integer nodeKindId = BaseUtils.isCollectionEmpty(nodeKindIds) ? null : nodeKindIds.get(0);
//        attr = originalAttrs.get(BIKConstants.METABIKS_ATTR_IS_FOLDER);
//        Integer isFolder = 0;
//        if (attr != null) {
//            isFolder = BaseUtils.tryParseInteger(attr.atrLinValue, 0);
//        }
//        String childrenKinds = "";
//        attr = originalAttrs.get(BIKConstants.METABIKS_ATTR_CHILDREN_KINDS);
//        if (attr != null) {
//            childrenKinds = attr.atrLinValue;
//        }
//        String childrenAttrs = "";
//        attr = originalAttrs.get(BIKConstants.METABIKS_ATTR_ATTR_CHILDREN_KINDS);
//        if (attr != null) {
//            childrenAttrs = attr.atrLinValue;
//        }
//        NodeKindBean nkb = addOrUpdateNodeKind(nodeKindId, nodeKindName, iconName, treeKind, isFolder, childrenKinds, childrenAttrs);
//        return nkb.id;
//    }
//    protected List<BeanWithIntIdAndNameBase> sortBeanIdNameListByName(List<BeanWithIntIdAndNameBase> list) {
//        Collections.sort(list, new Comparator<BeanWithIntIdAndNameBase>() {
//
//            @Override
//            public int compare(BeanWithIntIdAndNameBase o1, BeanWithIntIdAndNameBase o2) {
//                return o1.name.compareTo(o2.name);
//            }
//        });
//
//        return list;
//    }
    @Override
    public Pair<List<BeanWithIntIdAndNameBase>, List<BeanWithIntIdAndNameBase>> getAttributeDictOverView() throws LameRuntimeException {
        List<BeanWithIntIdAndNameBase> confList = getMetaConfigListIdAndName(BIKConstants.NODE_KIND_META_ATTRIBUTES_CATALOG);
        List<BeanWithIntIdAndNameBase> sysList = createBeansFromNamedQry("getAttributeCatagories", BeanWithIntIdAndNameBase.class);

//        List<Map<String, Object>> rs = execNamedQueryCFN("getDeletedConfigElements", null, getConfigurationTree(), BIKConstants.METABIKS_TREE_CODE_TMP);
        return new Pair<List<BeanWithIntIdAndNameBase>, List<BeanWithIntIdAndNameBase>>(confList, sysList);
    }

    @Override
    public Pair<List<BeanWithIntIdAndNameBase>, List<BeanWithIntIdAndNameBase>> getAttribute4NodeKindOverView() throws LameRuntimeException {
//        List<BeanWithIntIdAndNameBase> confList = getMetaConfigListIdAndName(BIKConstants.NODE_KIND_META_ATTR_4_NODE_KIND);
        List<BeanWithIntIdAndNameBase> sysList = createBeansFromNamedQry("getAllAttributeWithNodeKind", BeanWithIntIdAndNameBase.class);

        String confTreeCode = getConfigurationTree(BIKConstants.APP_PROP_SYS_CONF);
        Integer nodeKindsRootNodeId = getNodeId(BaseUtils.ensureObjIdEndWith(BIKConstants.METABIKS_OBJ_ID_NODE_KIND_DEF, "|"), confTreeCode);
        List<TreeNodeBean> nodeKindDefNodes = getChildrenNodeBeansByParentId(nodeKindsRootNodeId);
        List<BeanWithIntIdAndNameBase> confList = new ArrayList<BeanWithIntIdAndNameBase>();
        for (TreeNodeBean nk : nodeKindDefNodes) {
            List<TreeNodeBean> list = getChildrenNodeBeansByParentId(nk.id);
            for (TreeNodeBean tnb : list) {
                BeanWithIntIdAndNameBase bean = new BeanWithIntIdAndNameBase();
                bean.id = tnb.id;
                Map<String, AttributeBean> attrsMap = buildNodeAttributesMap(tnb.id);
                AttributeBean attr = attrsMap.get(BIKConstants.METABIKS_ATTR_ORIGINAL);
                if (attr == null || BaseUtils.isStrEmptyOrWhiteSpace(attr.atrLinValue)) {
                    throw new LameRuntimeException("Nie ma oryginalego atrybutu dla atrybut " + tnb.name + " w " + nk.name);
                }
                String attrDefName = BaseUtils.splitBySep(attr.atrLinValue, ",").get(1);
                if (attrDefName.contains("|")) {
                    attrDefName = attrDefName.substring(0, attrDefName.indexOf("|"));
                }
                bean.name = nk.name + ":" + attrDefName;
                confList.add(bean);
            }
        }
        return new Pair<List<BeanWithIntIdAndNameBase>, List<BeanWithIntIdAndNameBase>>(confList, sysList);
    }

    @Override
    public Pair<List<BeanWithIntIdAndNameBase>, List<BeanWithIntIdAndNameBase>> getAttributeDefOverView() throws LameRuntimeException {
        List<BeanWithIntIdAndNameBase> confList = getMetaConfigListIdAndName(BIKConstants.NODE_KIND_META_ATTRIBUTE_TYPE);
        List<BeanWithIntIdAndNameBase> sysList = createBeansFromNamedQry("getAttributeIdAndName", BeanWithIntIdAndNameBase.class);
        //        String confTreeCode = getConfigurationTree();
//        String attrDefRootObjId = BaseUtils.ensureObjIdEndWith(BIKConstants.METABIKS_OBJ_ID_ATTRIBUTE_DEF, "|");
//        Integer attrDefRootNodeId = getNodeId(attrDefRootObjId, confTreeCode);
//        if (attrDefRootNodeId == null) {
//            throw new LameRuntimeException("Nie prawidłowa struktura drzewa konfiguracji");
//        }

//        List<TreeNodeBean> confCatalogs = getChildrenNodeBeansByParentId(attrDefRootNodeId);
//        for (TreeNodeBean confCatalog : confCatalogs) {
//            List<TreeNodeBean> confAttrs = getChildrenNodeBeansByParentId(confCatalog.id);
//            for (TreeNodeBean confAttr : confAttrs) {
//                BeanWithIntIdAndNameBase bean = new BeanWithIntIdAndNameBase();
//                bean.id = confAttr.id;
//                bean.name = confAttr.name;
//                confList.add(bean);
//            }
//        }
//        List<AttrHintBean> attributes = getAttributes();
//        for (AttrHintBean attr : attributes) {
//            if (!attr.isBuiltIn && !attr.isDeleted && !attr.name.startsWith(BIKConstants.METABIKS_META_ATTR_PREFIX)) {
//                BeanWithIntIdAndNameBase bean = new BeanWithIntIdAndNameBase();
//                bean.id = attr.id;
//                bean.name = attr.name;
//                sysList.add(bean);
//            }
//        }
        return new Pair<List<BeanWithIntIdAndNameBase>, List<BeanWithIntIdAndNameBase>>(confList, sysList);
    }

    private List<BeanWithIntIdAndNameBase> getMetaConfigListIdAndName(String metaNodeKindCode) {
//        List<BeanWithIntIdAndNameBase> confList = new ArrayList<BeanWithIntIdAndNameBase>();
//        String confTreeCode = getConfigurationTree();
//        String attrDefRootObjId = BaseUtils.ensureObjIdEndWith(rootObjId, "|");
//        Integer attrDefRootNodeId = getNodeId(attrDefRootObjId, confTreeCode);
//        if (attrDefRootNodeId == null) {
//            throw new LameRuntimeException("Nie prawidłowa struktura drzewa konfiguracji");
//        }
//        List<TreeNodeBean> confCatalogs = getChildrenNodeBeansByParentId(attrDefRootNodeId);
//        for (TreeNodeBean confCatalog : confCatalogs) {
//            if (!BIKConstants.NODE_KIND_META_BUILT_IN_NODE_KIND.equals(confCatalog.nodeKindCode)) {
//                BeanWithIntIdAndNameBase bean = new BeanWithIntIdAndNameBase();
//                bean.id = confCatalog.id;
//                bean.name = confCatalog.name;
//                List<AttributeBean> rs = getAttributesByNameValueNode(BIKConstants.METABIKS_ATTR_NODE_KIND_CAPTION_PL, null, bean.id);
//                if (!BaseUtils.isCollectionEmpty(rs) && rs.size() == 1) {
//                    AttributeBean attr = rs.get(0);
//                    bean.name = confCatalog.name + ":" + attr.atrLinValue;
//                }
//                confList.add(bean);
//            }
//        }
        String namePrefix = "metaBiks";
//        if (BIKConstants.NODE_KIND_META_ATTRIBUTES_CATALOG.equals(metaNodeKindCode)) {
//            namePrefix = BIKConstants.METABIKS_META_ATTR_CATEGORY;
//        } else if (BIKConstants.NODE_KIND_META_ATTRIBUTE_TYPE.equals(metaNodeKindCode)) {
//            namePrefix = BIKConstants.METABIKS_META_ATTR_PREFIX;
//        } else if (BIKConstants.NODE_KIND_META_NODE_KIND.equals(metaNodeKindCode)) {
//            namePrefix = "metaBiks";
//        } else if (BIKConstants.NODE_KIND_META_TREE_KIND.equals(metaNodeKindCode)) {
//            namePrefix = "metaBIKS";
//        }
        List<BeanWithIntIdAndNameBase> confList = createBeansFromNamedQry("getMetaConfigNodeIdAndName", BeanWithIntIdAndNameBase.class, metaNodeKindCode, namePrefix);
        return confList;
    }

    @Override
    public Pair<List<BeanWithIntIdAndNameBase>, List<BeanWithIntIdAndNameBase>> getNodeKindsOverView() throws LameRuntimeException {
        List<BeanWithIntIdAndNameBase> confList = getMetaConfigListIdAndName(BIKConstants.NODE_KIND_META_NODE_KIND);
        List<BeanWithIntIdAndNameBase> sysList = createBeansFromNamedQry("getNodeKindIdAndName", BeanWithIntIdAndNameBase.class);
//        List<Map<String, Object>> rs = execNamedQueryCFN("getNodeKindInfo", null);
//        for (Map<String, Object> m : rs) {
//            BeanWithIntIdAndNameBase bean = new BeanWithIntIdAndNameBase();
//            bean.id = (Integer) m.get("node_kind_id");
//            bean.name = (String) m.get("node_kind_code") + ":" + (String) m.get("node_kind_caption");
//            sysList.add(bean);
//        }

        return new Pair<List<BeanWithIntIdAndNameBase>, List<BeanWithIntIdAndNameBase>>(confList, sysList);
    }

    @Override
    public Pair<List<BeanWithIntIdAndNameBase>, List<BeanWithIntIdAndNameBase>> getTreeKindsOverView() throws LameRuntimeException {
        List<BeanWithIntIdAndNameBase> confList = getMetaConfigListIdAndName(BIKConstants.NODE_KIND_META_TREE_KIND);
        List<BeanWithIntIdAndNameBase> sysList = createBeansFromNamedQry("getTreeKindsIdAndName", BeanWithIntIdAndNameBase.class);
//        List<TreeKindBean> rs = getTreeKinds();
//        for (TreeKindBean m : rs) {
//            if (!BIKConstants.TREE_KIND_META_BIKS.equalsIgnoreCase(m.code) && !BIKConstants.TREE_KIND_META_BIKS_MENU_CONF.equalsIgnoreCase(m.code)) {
//                BeanWithIntIdAndNameBase bean = new BeanWithIntIdAndNameBase();
//                bean.id = m.id;
//                bean.name = m.code + ":" + m.treeKindCaption;
//                sysList.add(bean);
//            }
//        }

        return new Pair<List<BeanWithIntIdAndNameBase>, List<BeanWithIntIdAndNameBase>>(confList, sysList);
    }

    private void implementNodeKinds(String confTreeCode, Map<Integer, Integer> confMap) {
        String nodeKindsRootObjId = BaseUtils.ensureObjIdEndWith(BIKConstants.METABIKS_OBJ_ID_NODE_KIND_DEF, "|");
        Integer nodeKindsRootNodeId = getNodeId(nodeKindsRootObjId, confTreeCode);

        List<TreeNodeBean> nodeKindsList = getChildrenNodeBeansByParentId(nodeKindsRootNodeId);
        for (TreeNodeBean bean : nodeKindsList) {
            String nodeKindCode = bean.name;
            Integer nodeKindId = null;
            Map<String, AttributeBean> nodeKindAttrs = buildNodeAttributesMap(bean.id);
            AttributeBean attr = nodeKindAttrs.get(BIKConstants.METABIKS_ATTR_NODE_KIND_CAPTION_PL);
            String nodeKindCaption = bean.name;
            if (attr != null) {
                nodeKindCaption = attr.atrLinValue;
                execNamedCommand("updateBikTranslationByLang", bean.name, nodeKindCaption, "pl", "kind");
            }
            HelpBean hb = new HelpBean();
            hb.caption = nodeKindCaption;
            hb.helpKey = nodeKindCode + ":author";
            hb.lang = "pl";
            attr = nodeKindAttrs.get(BIKConstants.METABIKS_ATTR_HELP_AUTHOR);
            if (attr != null && !BaseUtils.isStrEmptyOrWhiteSpace(attr.atrLinValue)) {
                hb.textHelp = attr.atrLinValue;
                addOrUpdateBikHelp(hb);
            }
            hb.helpKey = nodeKindCode + ":user";
            attr = nodeKindAttrs.get(BIKConstants.METABIKS_ATTR_HELP_USER);
            if (attr != null && !BaseUtils.isStrEmptyOrWhiteSpace(attr.atrLinValue)) {
                hb.textHelp = attr.atrLinValue;
                addOrUpdateBikHelp(hb);
            }
            if (BIKConstants.NODE_KIND_META_BUILT_IN_NODE_KIND.equals(bean.nodeKindCode)) {
                nodeKindId = execNamedQuerySingleValAsInteger("getNodeKindIdByCode", true, null, bean.name);
                if (nodeKindId != null) {
                    confMap.put(bean.id, nodeKindId);
                }
            } else {
                nodeKindId = confMap.get(bean.id);
                //            AttributeBean attr = nodeKindAttrs.get(BIKConstants.METABIKS_ATTR_NODE_KIND_CAPTION_EN);
//            if (attr != null) {
//                nodeKindCaption = attr.atrLinValue;
//                execNamedCommand("updateBikTranslationByLang", bean.name, nodeKindCaption, "en", "kind");
//            }

                attr = nodeKindAttrs.get(BIKConstants.METABIKS_ATTR_ICON_NAME);
                if (attr == null) {
                    throw new LameRuntimeException("node_kind " + nodeKindCode + " nie ma zdefiniowanej ikony");
                }
                String iconName = attr.atrLinValue;

//            attr = nodeKindAttrs.get(BIKConstants.METABIKS_ATTR_IS_FOLDER);
                Integer isFolder = 1;
//            if (attr != null) {
//                isFolder = BIKConstants.YES.equalsIgnoreCase(attr.atrLinValue) ? 1 : 0;
//            }
                String childrenKinds = "";
                attr = nodeKindAttrs.get(BIKConstants.METABIKS_ATTR_CHILDREN_KINDS);
                if (attr != null) {
                    childrenKinds = attr.atrLinValue;
                }
                if (BaseUtils.isStrEmptyOrWhiteSpace(childrenKinds)) {
                    childrenKinds = null;
                }
                String childrenAttrs = "";
                attr = nodeKindAttrs.get(BIKConstants.METABIKS_ATTR_ATTR_CHILDREN_KINDS);
                if (attr != null) {
                    childrenAttrs = attr.atrLinValue;
                }
                if (BaseUtils.isStrEmptyOrWhiteSpace(childrenAttrs)) {
                    childrenAttrs = null;
                }
                Integer isRegistry = 0;
                attr = nodeKindAttrs.get(BIKConstants.METABIKS_ATTR_REGISTRY);
                if (attr != null) {
                    isRegistry = BIKConstants.YES.equalsIgnoreCase(attr.atrLinValue) ? 1 : 0;
                }
                String jasperReport = null;
                attr = nodeKindAttrs.get(BIKConstants.METABIKS_ATTR_REPORTS);
                if (attr != null) {
                    jasperReport = attr.atrLinValue;
                }

                NodeKindBean nkb = addOrUpdateNodeKind(nodeKindId, nodeKindCode, nodeKindCaption, iconName, "", isFolder, childrenKinds, childrenAttrs, isRegistry, jasperReport);
                nodeKindId = nkb.id;
                confMap.put(bean.id, nodeKindId);
                bean.name = nkb.code;
                updateTreeNode(bean);
            }
            List<TreeNodeBean> attrsInNode = getChildrenNodeBeansByParentId(bean.id); //bik_attributes;
            for (TreeNodeBean attrInNode : attrsInNode) {
                assignAttribute2NodeKindByConfigTree(nodeKindId, attrInNode.id);
            }
        }
    }

    private void assignAttribute2NodeKindByConfigTree(Integer nodeKindId, Integer nodeIdInConfigTree) {
        Map<String, AttributeBean> attrDesc = buildNodeAttributesMap(nodeIdInConfigTree);
        AttributeBean attr = attrDesc.get(BIKConstants.METABIKS_ATTR_ORIGINAL);
        if (attr == null) {
            throw new LameRuntimeException("Nie ma zdifiniowanego atrybutu");
        }
        Integer attrDefNodeId = BaseUtils.tryParseInteger(BaseUtils.splitBySep(attr.atrLinValue, "_").get(0));
        String attrDefName = BaseUtils.splitBySep(attr.atrLinValue, ",").get(1);
        if (attrDefName.contains("|")) {
            attrDefName = attrDefName.substring(0, attrDefName.indexOf("|"));
        }

        attr = attrDesc.get(BIKConstants.METABIKS_ATTR_DEFAULT_VALUE);
        String defaultValue = null;
        if (attr != null) {
            defaultValue = attr.atrLinValue;
        }
        attr = attrDesc.get(BIKConstants.METABIKS_ATTR_TYPE_ATTR);
        String typeAttr = null;
        if (attr != null) {
            typeAttr = attr.atrLinValue;
        }
        attr = attrDesc.get(BIKConstants.METABIKS_ATTR_VALUE_OPT);
        String valueOpt = null;
        if (attr != null) {
            valueOpt = attr.atrLinValue;
        }

//        attr = attrDesc.get(BIKConstants.METABIKS_ATTR_VISUAL_ORDER);
//        String visualOrder = "0";
//        if (attr != null && !BaseUtils.isStrEmptyOrWhiteSpace(attr.atrLinValue)) {
//            visualOrder = attr.atrLinValue;
//        }
        attr = attrDesc.get(BIKConstants.METABIKS_ATTR_IS_REQUIRED);
        Integer isRequired = 0;
        if (attr != null && BIKConstants.YES.equals(attr.atrLinValue)) {
            isRequired = 1;
        }
        attr = attrDesc.get(BIKConstants.METABIKS_ATTR_IS_PUBLIC);
        Integer isPublic = 0;
        if (attr != null && BIKConstants.YES.equals(attr.atrLinValue)) {
            isPublic = 1;
        }
        attr = attrDesc.get(BIKConstants.METABIKS_ATTR_IS_EMPTY);
        Integer isEmpty = 0;
        if (attr != null && BIKConstants.YES.equals(attr.atrLinValue)) {
            isEmpty = 1;
        }

        attrDesc = buildNodeAttributesMap(attrDefNodeId);
        if (BaseUtils.isStrEmptyOrWhiteSpace(typeAttr) && attrDesc.containsKey(BIKConstants.METABIKS_ATTR_TYPE_ATTR)) {
            attr = attrDesc.get(BIKConstants.METABIKS_ATTR_TYPE_ATTR);
            typeAttr = attr.atrLinValue;
        }
        if (BaseUtils.isStrEmptyOrWhiteSpace(valueOpt) && attrDesc.containsKey(BIKConstants.METABIKS_ATTR_VALUE_OPT)) {
            attr = attrDesc.get(BIKConstants.METABIKS_ATTR_VALUE_OPT);
            valueOpt = attr.atrLinValue;
        }
        List<Map<String, Object>> rs = execNamedQueryCFN("getAttributeDefByName", FieldNameConversion.ToLowerCase, attrDefName);
        if (rs.size() != 1) {
            throw new LameRuntimeException("Są 2 atrybuty o nazwie:" + attrDefName + " lub atrybut nie istnieje");
        }
        Map<String, Object> m = rs.get(0);
        Integer attrDefId = (Integer) m.get("attr_def_id");

        addOrDeleteAttributeTypeForKind(attrDefId, new HashSet<Integer>(Arrays.asList(nodeKindId)), false);
        execNamedCommand("setAttributeProperty", "default_value", attrDefId, nodeKindId, defaultValue);
        execNamedCommand("setAttributeProperty", "is_required", attrDefId, nodeKindId, isRequired);
        execNamedCommand("setAttributeProperty", "is_public", attrDefId, nodeKindId, isPublic);
        execNamedCommand("setAttributeProperty", "is_empty", attrDefId, nodeKindId, isEmpty);
        execNamedCommand("setAttributeProperty", "type_attr", attrDefId, nodeKindId, typeAttr);
        execNamedCommand("setAttributeProperty", "value_opt", attrDefId, nodeKindId, valueOpt);
//        execNamedCommand("setAttributeProperty", "visual_order", attrDefId, nodeKindId, visualOrder);
    }

    public List<TreeNodeBean> getNodesByLinkedKind(int srcNodeKindId, String relationType, int treeId) {

        setOptFilteringNodeActionCodeForCurrentRequestAddJoinedObjs();
        List<TreeNodeBean> tnb = createBeansFromNamedQry("getNodesByLinkedKind", TreeNodeBean.class, srcNodeKindId, relationType, treeId);

        translateSpecialTreeNodeBeans(tnb, false);
        return tnb;
    }

    @Override
    public List<TreeNodeBean> getBikAllJoinableNodeListByLinkedKind(int srcNodeKindId, int treeId, String relationType) {
        setOptFilteringNodeActionCodeForCurrentRequestAddJoinedObjs();
        List<TreeNodeBean> tnb = createBeansFromNamedQry("getBikAllJoinableNodeListByLinkedKind", TreeNodeBean.class, srcNodeKindId, treeId, relationType);
        translateSpecialTreeNodeBeans(tnb, false);
        return tnb;
    }

    public List<TreeNodeBean> getKindsForLinkNodeKind(Integer srcNodeKindId, int treeId, String relationType) {

//        getRequiredAttrs
        List<TreeNodeBean> nodeKinds = createBeansFromNamedQry("getKindsForLinkNodeKind", TreeNodeBean.class, srcNodeKindId, treeId, relationType);
        for (TreeNodeBean nk : nodeKinds) {
            nk.requiredAttrs = getRequiredAttrsWithVisOrder(nk.nodeKindId);
        }
//        return createBeansFromNamedQry("getKindsForLinkNodeKind", TreeNodeBean.class, srcNodeKindId, treeId, relationType);
        return nodeKinds;

    }

    @Override
    public List<TreeNodeBean> getInnerJoinKindsForLinkNodeKindAndHyperlink(Integer nodeKindIdOuter, Integer nodeKindIdInner, int reeIdOuter, int treeIdInner, Pair<String, String> relationTypes, String optAttributeName) {
        List<TreeNodeBean> nodeKinds = createBeansFromNamedQry("getInnerJoinKindsForLinkNodeKindAndHyperlink", TreeNodeBean.class, nodeKindIdOuter, nodeKindIdInner, reeIdOuter, treeIdInner, relationTypes.v1, relationTypes.v2, optAttributeName);
        for (TreeNodeBean nk : nodeKinds) {
            nk.requiredAttrs = getRequiredAttrsWithVisOrder(nk.nodeKindId);
        }
//        return createBeansFromNamedQry("getKindsForLinkNodeKind", TreeNodeBean.class, srcNodeKindId, treeId, relationType);
        return nodeKinds;
    }

    private void ensureConfigTreeHasEssentialStruture(String confTreeCode) throws LameRuntimeException {
        Integer isAutoObjId = execNamedQuerySingleValAsInteger("isTreeHasAutoObjId", false, null, confTreeCode);
        if (isAutoObjId == 0) {
            throw new LameRuntimeException("Drzewo Meta BIKS musi mieć zaznaczoną opcję auto_obj");
        }
        List<String> allObjIdsList = execNamedQuerySingleColAsList("recalculateAutoObjId", null, confTreeCode);
        for (int i = 1; i < allObjIdsList.size(); i++) {
            if (allObjIdsList.get(i).equals(allObjIdsList.get(i - 1))) {
                throw new LameRuntimeException("Powtórzony ID obiektu: " + allObjIdsList.get(i));
            }
        }
        Integer cnt = execNamedQuerySingleVal("countRootObj", false, null, "Definicje atrybutów|");
        if (cnt != 1) {
            throw new LameRuntimeException("Brak definicji atrybutów");
        }
        cnt = execNamedQuerySingleVal("countRootObj", false, null, "Definicje obiektów|");
        if (cnt != 1) {
            throw new LameRuntimeException("Brak definicji obiektów");
        }
        cnt = execNamedQuerySingleVal("countRootObj", false, null, "Definicje drzew|");
        if (cnt != 1) {
            throw new LameRuntimeException("Brak definicji drzew");
        }
        List<Object> objId = new ArrayList<Object>();
        objId.addAll(execNamedQuerySingleColAsSet("metaModelCheckFirstLevelElement", null, "Definicje atrybutów|", BIKConstants.NODE_KIND_META_ATTRIBUTES_CATALOG, null));
        objId.addAll(execNamedQuerySingleColAsSet("metaModelCheckFirstLevelElement", null, "Definicje obiektów|", BIKConstants.NODE_KIND_META_NODE_KIND, BIKConstants.NODE_KIND_META_BUILT_IN_NODE_KIND));
        objId.addAll(execNamedQuerySingleColAsSet("metaModelCheckFirstLevelElement", null, "Definicje drzew|", BIKConstants.NODE_KIND_META_TREE_KIND, null));
        if (!BaseUtils.isCollectionEmpty(objId)) {
            throw new LameRuntimeException(BaseUtils.mergeWithSepEx(objId, ",\n") + " nie ma kompatybilnego typu");
        }
        objId.addAll(execNamedQuerySingleColAsSet("metaModelCheckParentAndChild", null, BIKConstants.NODE_KIND_META_ATTRIBUTES_CATALOG, BIKConstants.NODE_KIND_META_ATTRIBUTE_TYPE));
        objId.addAll(execNamedQuerySingleColAsSet("metaModelCheckParentAndChild", null, BIKConstants.NODE_KIND_META_NODE_KIND, BIKConstants.NODE_KIND_META_ATTR_4_NODE_KIND));
        objId.addAll(execNamedQuerySingleColAsSet("metaModelCheckParentAndChild", null, BIKConstants.NODE_KIND_META_TREE_KIND, BIKConstants.NODE_KIND_META_NODE_KIND_4_TREE_KIND));
        objId.addAll(execNamedQuerySingleColAsSet("metaModelCheckParentAndChild", null, BIKConstants.NODE_KIND_META_NODE_KIND_4_TREE_KIND, BIKConstants.NODE_KIND_META_RELATED_NODE_KIND));
//        objId.addAll(execNamedQuerySingleColAsSet("metaModelCheckParentAndChild", null, BIKConstants.NODE_KIND_META_NODE_KIND, BIKConstants.NODE_KIND_META_PRINT));

        objId.remove(BIKConstants.NODE_KIND_META_PRINT);
        if (!BaseUtils.isCollectionEmpty(objId)) {
            throw new LameRuntimeException(BaseUtils.mergeWithSepEx(objId, ",\n") + " nie ma kompatybilnego typu");
        }
        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_ATTRIBUTES_CATALOG, BIKConstants.METABIKS_ATTR_ATTR_CAT_NAME);

        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_ATTRIBUTE_TYPE, BIKConstants.METABIKS_ATTR_VALUE_OPT);
        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_ATTRIBUTE_TYPE, BIKConstants.METABIKS_ATTR_ATTR_NAME);
        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_ATTRIBUTE_TYPE, BIKConstants.METABIKS_ATTR_ATTR_DESC);
        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_ATTRIBUTE_TYPE, BIKConstants.METABIKS_ATTR_TYPE_ATTR);
        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_ATTRIBUTE_TYPE, BIKConstants.METABIKS_ATTR_USED_IN_DROOLS);
        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_ATTRIBUTE_TYPE, BIKConstants.METABIKS_ATTR_DISPLAY_AS_NUMBER);

        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_BUILT_IN_NODE_KIND, BIKConstants.METABIKS_ATTR_NODE_KIND_CAPTION_PL);
        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_BUILT_IN_NODE_KIND, BIKConstants.METABIKS_ATTR_HELP_AUTHOR);
        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_BUILT_IN_NODE_KIND, BIKConstants.METABIKS_ATTR_HELP_USER);

        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_NODE_KIND, BIKConstants.METABIKS_ATTR_ATTR_CHILDREN_KINDS);
        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_NODE_KIND, BIKConstants.METABIKS_ATTR_CHILDREN_KINDS);
        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_NODE_KIND, BIKConstants.METABIKS_ATTR_ICON_NAME);
        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_NODE_KIND, BIKConstants.METABIKS_ATTR_NODE_KIND_CAPTION_PL);
        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_NODE_KIND, BIKConstants.METABIKS_ATTR_REGISTRY);
        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_NODE_KIND, BIKConstants.METABIKS_ATTR_HELP_AUTHOR);
        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_NODE_KIND, BIKConstants.METABIKS_ATTR_HELP_USER);
//        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_NODE_KIND, BIKConstants.METABIKS_ATTR_REPORTS);

        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_ATTR_4_NODE_KIND, BIKConstants.METABIKS_ATTR_VALUE_OPT);
        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_ATTR_4_NODE_KIND, BIKConstants.METABIKS_ATTR_IS_EMPTY);
        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_ATTR_4_NODE_KIND, BIKConstants.METABIKS_ATTR_IS_REQUIRED);
        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_ATTR_4_NODE_KIND, BIKConstants.METABIKS_ATTR_IS_PUBLIC);
        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_ATTR_4_NODE_KIND, BIKConstants.METABIKS_ATTR_TYPE_ATTR);
        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_ATTR_4_NODE_KIND, BIKConstants.METABIKS_ATTR_DEFAULT_VALUE);
        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_ATTR_4_NODE_KIND, BIKConstants.METABIKS_ATTR_ORIGINAL);

        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_TREE_KIND, BIKConstants.METABIKS_ATTR_TREE_KIND_CAPTION_PL);
        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_TREE_KIND, BIKConstants.METABIKS_ATTR_ALLOW_LINKING);

        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_NODE_KIND_4_TREE_KIND, BIKConstants.METABIKS_ATTR_BRANCH_TYPE_IN_TREE);
        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_NODE_KIND_4_TREE_KIND, BIKConstants.METABIKS_ATTR_ORIGINAL);

        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_RELATED_NODE_KIND, BIKConstants.METABIKS_ATTR_RELATION_TYPE);
        checkMetaModelRequiredAttribute(BIKConstants.NODE_KIND_META_RELATED_NODE_KIND, BIKConstants.METABIKS_ATTR_ORIGINAL);

        checkMetaModelChildHasParentAsLeaf();
        checkMetaModelParentWithoutChild();
        checkMetaModelAddAllNodeKind4TreeKind();
        checkMetaModelTreeKindHasBranch();
        checkMetaModelTreeKindHasLeaf();
        checkMetaModelHasRepeatedName(BIKConstants.NODE_KIND_META_ATTRIBUTES_CATALOG, BIKConstants.METABIKS_ATTR_ATTR_CAT_NAME);
        checkMetaModelHasRepeatedName(BIKConstants.NODE_KIND_META_ATTRIBUTE_TYPE, BIKConstants.METABIKS_ATTR_ATTR_NAME);
        checkMetaModelHasRepeatedName(BIKConstants.NODE_KIND_META_NODE_KIND, BIKConstants.METABIKS_ATTR_NODE_KIND_CAPTION_PL);
        checkMetaModelHasRepeatedName(BIKConstants.NODE_KIND_META_TREE_KIND, BIKConstants.METABIKS_ATTR_TREE_KIND_CAPTION_PL);
//        Set<String> allObjIdsSet = new HashSet<String>();
//        for (String objId : allObjIdsList) {
//            if (!allObjIdsSet.contains(objId)) {
//                allObjIdsSet.add(objId);
//            } else {
//                throw new LameRuntimeException("Powtórzony ID obiektu: " + objId);
//            }
//        }
//
//        if (getNodeId(BaseUtils.ensureObjIdEndWith(BIKConstants.METABIKS_OBJ_ID_ATTRIBUTE_DEF, "|"), confTreeCode) == null
//                || getNodeId(BaseUtils.ensureObjIdEndWith(BIKConstants.METABIKS_OBJ_ID_NODE_KIND_DEF, "|"), confTreeCode) == null
//                || getNodeId(BaseUtils.ensureObjIdEndWith(BIKConstants.METABIKS_OBJ_ID_TREE_KIND_DEF, "|"), confTreeCode) == null) {
//            throw new LameRuntimeException("Nieprawidłowa struktura drzewa konfiguracji");
//        }
//
//        List<Integer> queue = new ArrayList<Integer>();
//        queue.add(getNodeId(BaseUtils.ensureObjIdEndWith(BIKConstants.METABIKS_OBJ_ID_ATTRIBUTE_DEF, "|"), confTreeCode));
//        queue.add(getNodeId(BaseUtils.ensureObjIdEndWith(BIKConstants.METABIKS_OBJ_ID_NODE_KIND_DEF, "|"), confTreeCode));
//        queue.add(getNodeId(BaseUtils.ensureObjIdEndWith(BIKConstants.METABIKS_OBJ_ID_TREE_KIND_DEF, "|"), confTreeCode));
//        if (queue.size() != 3) {
//            throw new LameRuntimeException("Drzewo nie ma wystarczającej liczby grup definicji");
//        }
//        Set<String> treeKindNode = new HashSet<String>();
//        Map<String, Integer> treeKindNodeHasMainBranch = new HashMap<String, Integer>();
//        Map<String, Integer> treeKindNodeHasBranch = new HashMap<String, Integer>();
//        Map<String, Integer> treeKindNodeHasLeaf = new HashMap<String, Integer>();
//        Map<String, Integer> branchInTreeKind = new HashMap<String, Integer>();
//        Set<Integer> nodeKind4TreeKindAsLeaf = new HashSet<Integer>();
//        Map<String, Set<String>> nodeKindInTreeKinds = new HashMap<String, Set<String>>();
//        for (int i = 0; i < queue.size(); i++) {
//            Integer nodeId = queue.get(i);
//
//            Map<String, AttributeBean> attrsMap = buildNodeAttributesMap(nodeId);
//            TreeNodeBean node = getNodeBeanById(nodeId, false);
//
//            ensureConfigTreeNodeHasEssentialAttributes(node, attrsMap);
//            if (BIKConstants.NODE_KIND_META_TREE_KIND.equalsIgnoreCase(node.nodeKindCode)) {
//                treeKindNode.add(node.objId);
//                if (!nodeKindInTreeKinds.containsKey(node.objId)) {
//                    nodeKindInTreeKinds.put(node.objId, new HashSet<String>());
//                }
//            } else if (node.objId.startsWith(BIKConstants.METABIKS_OBJ_ID_TREE_KIND_DEF)) {
//                List<String> ids = BaseUtils.splitBySep(node.objId, "|");
//                String treeKindObjId = BaseUtils.ensureObjIdEndWith(BaseUtils.mergeWithSepEx(ids.subList(0, 2), "|"), "|");
//                Set<String> nkInTk = nodeKindInTreeKinds.get(treeKindObjId);
//                if (BIKConstants.NODE_KIND_META_NODE_KIND_4_TREE_KIND.equalsIgnoreCase(node.nodeKindCode)) {
//                    AttributeBean attr = attrsMap.get(BIKConstants.METABIKS_ATTR_ORIGINAL);
//                    String originalNodeKindCode = BaseUtils.splitBySep(attr.atrLinValue, ",").get(1);
//                    if (originalNodeKindCode.contains("|")) {
//                        originalNodeKindCode = originalNodeKindCode.substring(0, originalNodeKindCode.indexOf("|"));
//                    }
//                    nkInTk.add(originalNodeKindCode);
//                    nodeKindInTreeKinds.put(treeKindObjId, nkInTk);
//                    attr = attrsMap.get(BIKConstants.METABIKS_ATTR_BRANCH_TYPE_IN_TREE);
//                    if (!BaseUtils.isStrEmptyOrWhiteSpace(attr.atrLinValue)) {
//                        if (attr.atrLinValue.contains(BIKConstants.MAIN_BRANCH)) {
//                            treeKindNodeHasMainBranch.put(treeKindObjId, treeKindNodeHasMainBranch.containsKey(treeKindObjId) ? treeKindNodeHasMainBranch.get(treeKindObjId) + 1 : 1);
//                        }
//                        if (attr.atrLinValue.equals(BIKConstants.LEAF)) {
//                            nodeKind4TreeKindAsLeaf.add(nodeId);
//                            treeKindNodeHasLeaf.put(treeKindObjId, treeKindNodeHasLeaf.containsKey(treeKindObjId) ? treeKindNodeHasLeaf.get(treeKindObjId) + 1 : 1);
//                        }
//                        if (attr.atrLinValue.contains(BIKConstants.BRANCH) && !branchInTreeKind.containsKey(node.objId)) {
//                            branchInTreeKind.put(node.objId, 0);
//                        }
//                    }
//                } else if (BIKConstants.NODE_KIND_META_RELATED_NODE_KIND.equalsIgnoreCase(node.nodeKindCode)) {
//                    AttributeBean attr = attrsMap.get(BIKConstants.METABIKS_ATTR_ORIGINAL);
//                    String originalNodeKindCode = BaseUtils.splitBySep(attr.atrLinValue, ",").get(1);
//                    if (originalNodeKindCode.contains("|")) {
//                        originalNodeKindCode = originalNodeKindCode.substring(0, originalNodeKindCode.indexOf("|"));
//                    }
//                    attr = attrsMap.get(BIKConstants.METABIKS_ATTR_RELATION_TYPE);
//                    if (!BaseUtils.isStrEmptyOrWhiteSpace(attr.atrLinValue) && attr.atrLinValue.contains(BIKConstants.CHILD)) {
//                        treeKindNodeHasBranch.put(treeKindObjId, treeKindNodeHasMainBranch.containsKey(treeKindObjId) ? treeKindNodeHasMainBranch.get(treeKindObjId) + 1 : 1);
//                        String parentObjId = BaseUtils.ensureObjIdEndWith(BaseUtils.mergeWithSepEx(ids.subList(0, 3), "|"), "|");
//                        if (branchInTreeKind.get(parentObjId) == null) {
//                            throw new LameRuntimeException(parentObjId + " nie ma wypełnionego atrybutu: typ relacji");
//                        }
//                        branchInTreeKind.put(parentObjId, branchInTreeKind.get(parentObjId) + 1);
//                        if (!nkInTk.contains(originalNodeKindCode)) {
//                            throw new LameRuntimeException(originalNodeKindCode + " jeszcze nie było dodane do drzewa " + treeKindObjId);
//                        }
//                        if (nodeKind4TreeKindAsLeaf.contains(node.parentNodeId)) {
//                            throw new LameRuntimeException(node.objId + " nie może mieć ojca jako liść w drzewie");
//                        }
//                    }
//                }
//            }
//            List<TreeNodeBean> children = getChildrenNodeBeansByParentId(node.id);
//            for (TreeNodeBean child : children) {
//                queue.add(child.id);
//            }
//        }
//        for (String nodeKindInTree : branchInTreeKind.keySet()) {
//            if (branchInTreeKind.get(nodeKindInTree) == 0) {
//                throw new LameRuntimeException("Gałąź " + nodeKindInTree + "nie ma dziecka");
//            }
//        }
//        for (String treeObjId : treeKindNode) {
//            if (!treeKindNodeHasMainBranch.containsKey(treeObjId)) {
//                throw new LameRuntimeException("Drzewo: " + treeObjId + " nie ma zdefiniowanego typu głównej gałęzi");
//            }
//            if (!treeKindNodeHasLeaf.containsKey(treeObjId)) {
//                throw new LameRuntimeException("Drzewo: " + treeObjId + " nie ma zdefiniowanego typu liści");
//            }
//            if (!treeKindNodeHasBranch.containsKey(treeObjId)) {
//                throw new LameRuntimeException("Drzewo: " + treeObjId + " nie ma zdefiniowanego typu gałęzi lub nie ma podanego dziecka dla gałęzi");
//            }
//        }
    }

    private List<String> getRequiredAttrsName4NodeInMetaBiks(String nodeKindCode) {
        List<String> requireAttrs = new ArrayList<String>();
        if (BIKConstants.NODE_KIND_META_ATTRIBUTES_CATALOG.equalsIgnoreCase(nodeKindCode)) {
            requireAttrs = Arrays.asList(BIKConstants.METABIKS_ATTR_ATTR_CAT_NAME);
        } else if (!BIKConstants.NODE_KIND_META_DEFINITION_GROUP.equalsIgnoreCase(nodeKindCode)
                && !BIKConstants.NODE_KIND_META_ATTRIBUTES_CATALOG.equalsIgnoreCase(nodeKindCode)
                && !BIKConstants.NODE_KIND_META_BUILT_IN_NODE_KIND.equals(nodeKindCode)) {
            if (BIKConstants.NODE_KIND_META_ATTRIBUTE_TYPE.equalsIgnoreCase(nodeKindCode)) {
                requireAttrs = Arrays.asList(BIKConstants.METABIKS_ATTR_VALUE_OPT,
                        BIKConstants.METABIKS_ATTR_TYPE_ATTR,
                        BIKConstants.METABIKS_ATTR_ATTR_DESC,
                        BIKConstants.METABIKS_ATTR_USED_IN_DROOLS,
                        BIKConstants.METABIKS_ATTR_DISPLAY_AS_NUMBER,
                        BIKConstants.METABIKS_ATTR_ATTR_NAME);
            } else if (BIKConstants.NODE_KIND_META_BUILT_IN_NODE_KIND.equalsIgnoreCase(nodeKindCode)) {
                requireAttrs = Arrays.asList(BIKConstants.METABIKS_ATTR_NODE_KIND_CAPTION_PL);
            } else if (BIKConstants.NODE_KIND_META_NODE_KIND.equalsIgnoreCase(nodeKindCode)) {
                requireAttrs = Arrays.asList(BIKConstants.METABIKS_ATTR_ATTR_CHILDREN_KINDS,
                        BIKConstants.METABIKS_ATTR_CHILDREN_KINDS,
                        //                        BIKConstants.METABIKS_ATTR_IS_FOLDER,
                        BIKConstants.METABIKS_ATTR_ICON_NAME,
                        BIKConstants.METABIKS_ATTR_NODE_KIND_CAPTION_PL,
                        BIKConstants.METABIKS_ATTR_REGISTRY,
                        BIKConstants.METABIKS_ATTR_REPORTS);
            } else if (BIKConstants.NODE_KIND_META_ATTR_4_NODE_KIND.equalsIgnoreCase(nodeKindCode)) {
                requireAttrs = Arrays.asList(BIKConstants.METABIKS_ATTR_VALUE_OPT,
                        /*BIKConstants.METABIKS_ATTR_VISUAL_ORDER,*/
                        BIKConstants.METABIKS_ATTR_ORIGINAL,
                        BIKConstants.METABIKS_ATTR_OVERWRITE_TYPE_ATTR,
                        BIKConstants.METABIKS_ATTR_DEFAULT_VALUE,
                        BIKConstants.METABIKS_ATTR_IS_REQUIRED,
                        BIKConstants.METABIKS_ATTR_IS_PUBLIC,
                        BIKConstants.METABIKS_ATTR_IS_EMPTY);
            } else if (BIKConstants.NODE_KIND_META_TREE_KIND.equalsIgnoreCase(nodeKindCode)) {
                requireAttrs = Arrays.asList(BIKConstants.METABIKS_ATTR_ALLOW_LINKING,
                        BIKConstants.METABIKS_ATTR_TREE_KIND_CAPTION_PL);
            } else if (BIKConstants.NODE_KIND_META_NODE_KIND_4_TREE_KIND.equalsIgnoreCase(nodeKindCode)) {
                requireAttrs = Arrays.asList(BIKConstants.METABIKS_ATTR_ORIGINAL,
                        BIKConstants.METABIKS_ATTR_BRANCH_TYPE_IN_TREE);
            } else if (BIKConstants.NODE_KIND_META_RELATED_NODE_KIND.equalsIgnoreCase(nodeKindCode)) {
                requireAttrs = Arrays.asList(BIKConstants.METABIKS_ATTR_ORIGINAL,
                        BIKConstants.METABIKS_ATTR_RELATION_TYPE);
            } else {
                throw new LameRuntimeException("Nie zgadza się typ węzła: " + nodeKindCode);
            }
        }
        return requireAttrs;
    }

    private void ensureConfigTreeNodeHasEssentialAttributes(TreeNodeBean nodeBean, Map<String, AttributeBean> attrsMap) {
        List<String> requiredAttrs = getRequiredAttrsName4NodeInMetaBiks(nodeBean.nodeKindCode);
        for (String attrName : requiredAttrs) {
            if (!attrsMap.containsKey(attrName)) {
                if (!canAddAttributeAsNull(nodeBean, attrsMap, attrName)) {
                    throw new LameRuntimeException("Nie dodano atrybut: " + attrName + " dla " + nodeBean.objId);
                }
            }
        }
    }

    private boolean canAddAttributeAsNull(TreeNodeBean node, Map<String, AttributeBean> attrsMap, String attrName) {
        if (BIKConstants.METABIKS_ATTR_CHILDREN_KINDS.equalsIgnoreCase(attrName)
                || BIKConstants.METABIKS_ATTR_ATTR_CHILDREN_KINDS.equals(attrName)
                || BIKConstants.METABIKS_ATTR_OVERWRITE_TYPE_ATTR.equals(attrName)
                || BIKConstants.METABIKS_ATTR_DEFAULT_VALUE.equals(attrName)) {
            addOrUpdateAttribute(attrName, null, node.id);
            return true;
        } else if (BIKConstants.METABIKS_ATTR_VALUE_OPT.equalsIgnoreCase(attrName)) {
            if (BIKConstants.NODE_KIND_META_ATTRIBUTE_TYPE.equals(node.nodeKindCode)) {
                AttributeBean typeAttr = attrsMap.get(BIKConstants.METABIKS_ATTR_TYPE_ATTR);
                if (typeAttr != null && (!AttributeType.combobox.name().equals(typeAttr.atrLinValue)
                        && !AttributeType.checkbox.name().equals(typeAttr.atrLinValue))
                        && !AttributeType.comboBooleanBox.name().equals(typeAttr.atrLinValue)
                        && !AttributeType.selectOneJoinedObject.name().equals(typeAttr.atrLinValue)) {
                    addOrUpdateAttribute(attrName, null, node.id);
                    return true;
                }
            } else if (BIKConstants.NODE_KIND_META_ATTR_4_NODE_KIND.equals(node.nodeKindCode)) {
                addOrUpdateAttribute(attrName, null, node.id);
                return true;
            }
        }
        return false;
    }

    private Map<Integer, Integer> addNodeKind4TreeKind(String confTreeCode, TreeKindBean bean, Integer treeKindNodeId, String treeKindBranchId, String treeKindObjId, Map<String, Integer> nkMap, Map<Integer, Integer> nodeKinds2NodeIds) {
        Map<Integer, Integer> allNodeKindsInTreeDef = new HashMap<Integer, Integer>();
        for (NodeKind4TreeKind nk4tk : bean.additionalNodeKindsId) {
            if (nk4tk.srcNodeKindId == null) {
                if (nk4tk.dstNodeKindId != null) {
                    //Główny gałąż
                    Integer dstNodeIdInTreeDef = allNodeKindsInTreeDef.get(nk4tk.dstNodeKindId);
                    if (dstNodeIdInTreeDef == null) {
                        dstNodeIdInTreeDef = addNodeKind4TreeKindInConfigTree(treeKindObjId, treeKindNodeId, treeKindBranchId, confTreeCode, nk4tk.dstNodeKindId, nkMap.get(BIKConstants.NODE_KIND_META_NODE_KIND_4_TREE_KIND));
                        allNodeKindsInTreeDef.put(nk4tk.dstNodeKindId, dstNodeIdInTreeDef);
                    }
                }
            } else if (nk4tk.dstNodeKindId == null) {
                //Liść
                Integer srcNodeIdInTreeDef = allNodeKindsInTreeDef.get(nk4tk.srcNodeKindId);
                if (srcNodeIdInTreeDef == null) {
                    srcNodeIdInTreeDef = addNodeKind4TreeKindInConfigTree(treeKindObjId, treeKindNodeId, treeKindBranchId, confTreeCode, nk4tk.srcNodeKindId, nkMap.get(BIKConstants.NODE_KIND_META_NODE_KIND_4_TREE_KIND));
                    allNodeKindsInTreeDef.put(nk4tk.srcNodeKindId, srcNodeIdInTreeDef);
                }
            } else if (BIKConstants.CHILD.equalsIgnoreCase(nk4tk.relationType)) {
                //Potomek-Dziecko
                Integer srcNodeIdInTreeDef = allNodeKindsInTreeDef.get(nk4tk.srcNodeKindId);
                Integer dstNodeIdInTreeDef = allNodeKindsInTreeDef.get(nk4tk.dstNodeKindId);

                if (srcNodeIdInTreeDef == null) {
                    srcNodeIdInTreeDef = addNodeKind4TreeKindInConfigTree(treeKindObjId, treeKindNodeId, treeKindBranchId, confTreeCode, nk4tk.srcNodeKindId, nkMap.get(BIKConstants.NODE_KIND_META_NODE_KIND_4_TREE_KIND));
                    allNodeKindsInTreeDef.put(nk4tk.srcNodeKindId, srcNodeIdInTreeDef);
                }
                if (dstNodeIdInTreeDef == null) {
                    dstNodeIdInTreeDef = addNodeKind4TreeKindInConfigTree(treeKindObjId, treeKindNodeId, treeKindBranchId, confTreeCode, nk4tk.dstNodeKindId, nkMap.get(BIKConstants.NODE_KIND_META_NODE_KIND_4_TREE_KIND));
                    allNodeKindsInTreeDef.put(nk4tk.dstNodeKindId, dstNodeIdInTreeDef);
                }
                Integer originalSrcNodeKindNodeId = nodeKinds2NodeIds.get(nk4tk.srcNodeKindId);
                Integer originalDstNodeKindNodeId = nodeKinds2NodeIds.get(nk4tk.dstNodeKindId);
                Map<Integer, String> m = getNodeNamesByIds(Arrays.asList(originalSrcNodeKindNodeId, originalDstNodeKindNodeId));

                String parentNodeKindBranchIdInTreeDef = BaseUtils.ensureObjIdEndWith(BaseUtils.ensureObjIdEndWith(treeKindBranchId, "|") + srcNodeIdInTreeDef, "|");
                String parentNodeKindBranchObjIdInTreeDef = BaseUtils.ensureObjIdEndWith(BaseUtils.ensureObjIdEndWith(treeKindObjId, "|") + m.get(originalSrcNodeKindNodeId), "|");
                Integer childInRelation = getOrUpdateSimpleNode(srcNodeIdInTreeDef, parentNodeKindBranchIdInTreeDef, parentNodeKindBranchObjIdInTreeDef + m.get(originalDstNodeKindNodeId), confTreeCode, nkMap.get(BIKConstants.NODE_KIND_META_RELATED_NODE_KIND), m.get(originalDstNodeKindNodeId), null);
                addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_ORIGINAL, originalDstNodeKindNodeId + "_" + confTreeCode + "," + m.get(originalDstNodeKindNodeId), childInRelation);
                addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_RELATION_TYPE, BIKConstants.CHILD, childInRelation);
            } else {
                //Dowiązanie - powiązanie
                Integer srcNodeIdInTreeDef = allNodeKindsInTreeDef.get(nk4tk.srcNodeKindId);
                if (srcNodeIdInTreeDef == null) {
                    srcNodeIdInTreeDef = addNodeKind4TreeKindInConfigTree(treeKindObjId, treeKindNodeId, treeKindBranchId, confTreeCode, nk4tk.srcNodeKindId, nkMap.get(BIKConstants.NODE_KIND_META_NODE_KIND_4_TREE_KIND));
                }
                Integer originalSrcNodeKindNodeId = nodeKinds2NodeIds.get(nk4tk.srcNodeKindId);
                Integer originalDstNodeKindNodeId = nodeKinds2NodeIds.get(nk4tk.dstNodeKindId);
                Map<Integer, String> m = getNodeNamesByIds(Arrays.asList(originalSrcNodeKindNodeId, originalDstNodeKindNodeId));
                String parentNodeKindBranchIdInTreeDef = BaseUtils.ensureObjIdEndWith(BaseUtils.ensureObjIdEndWith(treeKindBranchId, "|") + srcNodeIdInTreeDef, "|");
                String parentNodeKindBranchObjIdInTreeDef = BaseUtils.ensureObjIdEndWith(BaseUtils.ensureObjIdEndWith(treeKindObjId, "|") + m.get(originalSrcNodeKindNodeId), "|");
                Integer childInRelation = getOrUpdateSimpleNode(srcNodeIdInTreeDef, parentNodeKindBranchIdInTreeDef, parentNodeKindBranchObjIdInTreeDef + m.get(originalDstNodeKindNodeId), confTreeCode, nkMap.get(BIKConstants.NODE_KIND_META_RELATED_NODE_KIND), m.get(originalDstNodeKindNodeId), null);

                addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_ORIGINAL, originalDstNodeKindNodeId + "_" + confTreeCode + "," + m.get(originalDstNodeKindNodeId), childInRelation);
                addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_RELATION_TYPE, nk4tk.relationType, childInRelation);
            }
        }
        return allNodeKindsInTreeDef;
    }

    private void ensureConfigTreeDoesNotHaveExternalLinks(String confTreeCode) {
        List<AttributeBean> list = createBeansFromNamedQry("getAttribute4Tree", AttributeBean.class, confTreeCode, AttributeType.hyperlinkInMulti.name());
        if (list.size() > 0) {
            for (AttributeBean bean : list) {
                String linkedTreeCode = BaseUtils.splitBySep(BaseUtils.splitBySep(bean.atrLinValue, ",").get(0), "_").get(1);
                if (!confTreeCode.equals(linkedTreeCode)) {
                    throw new LameRuntimeException("Drzewo typu Meta BIKS nie może mieć wewnętrznego linka do innego typu drzewa niż tylko do siebie: " + list.get(0).nodeObjId);
                }
            }
        }
    }

    private void updateHelpAttribute4NodeKind(Integer nodeKindMetaNodeId, String nodeKindCode) {
        String helpKey = nodeKindCode + ":author";
        String textHelp = BaseUtils.safeToStringNotNull(execNamedQuerySingleVal("getHelpText", true, null, helpKey));
        addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_HELP_AUTHOR, textHelp, nodeKindMetaNodeId);

        helpKey = nodeKindCode + ":user";
        textHelp = BaseUtils.safeToStringNotNull(execNamedQuerySingleVal("getHelpText", true, null, helpKey));
        addOrUpdateAttribute(BIKConstants.METABIKS_ATTR_HELP_USER, textHelp, nodeKindMetaNodeId);
    }

    public void grantRightsFromContextForUser(Map<SystemUserBean, List<RightRoleBean>> map, int treeId, Integer nodeId, String rightsProc) {
        for (Map.Entry<SystemUserBean, List<RightRoleBean>> mapPair : map.entrySet()) {
            for (RightRoleBean r : mapPair.getValue()) {
                execNamedCommand("grantRightsFromContextForUser", mapPair.getKey().id, r.id, treeId, nodeId, rightsProc);
            }
        }
        execNamedCommand("recalculateCustomRightRoleViewBranchGrants");
    }

    public void grantRightsFromContextForUserNewRightsSys(List<SystemUserBean> usersList, int nodeId, int roleId, String rightsProc) {
        ArrayList<SystemUserBean> tempUsersList = new ArrayList<SystemUserBean>();
        tempUsersList.addAll(usersList);
        for (SystemUserBean sub : tempUsersList) {
            if (sub.userHasRightsInCurrentNode) {
                usersList.remove(sub);
                execNamedCommand("removeRightsOfUserInCurrentNode", nodeId, roleId, sub.id);
            }
        }
        for (SystemUserBean sub : usersList) {
            execNamedCommand("grantRightsFromContextForUserNewRightsSys", sub.id, nodeId, roleId, rightsProc);
        }
        execNamedCommand("recalculateCustomRightRoleViewBranchGrants");
    }

    public List<SystemUserGroupBean> getAllUserGroups() {
        List<SystemUserGroupBean> usersGroupBeans = createBeansFromNamedQry("getAllGroups", SystemUserGroupBean.class);
        return usersGroupBeans;
    }

    public List<SystemUserGroupBean> getRolesAssociatedWithObj(String nodeKindCode) {
        List<SystemUserGroupBean> usersGroupBeans = createBeansFromNamedQry("getRolesAssociatedWithObj", SystemUserGroupBean.class, nodeKindCode);
        return usersGroupBeans;
    }

    public Map<RightRoleBean, List<SystemUserBean>> getAssignedToNodeUsers(int treeId, Integer nodeId) {
        List<SystemUserBean> assignedUsersList;
        assignedUsersList
                = createBeansFromNamedQry("getAssignedToNodeUsers", SystemUserBean.class, treeId, nodeId);

        List<RightRoleBean> allRightRoles = getAllRightRoles();
        Map<Integer, RightRoleBean> mapOfAllRightRoles = BaseUtils.makeBeanMap(allRightRoles);

        List<Pair<Integer, Pair<Integer, Integer>>> rightRolesOfUsers = getAllRightRolesOfUsersByIds();
        Map<RightRoleBean, List<SystemUserBean>> roleToUserList = new HashMap<RightRoleBean, List<SystemUserBean>>();

        if (!assignedUsersList.isEmpty()) {
            for (Integer rrI : mapOfAllRightRoles.keySet()) {
                List<SystemUserBean> users = new ArrayList<SystemUserBean>();
                for (SystemUserBean sub : assignedUsersList) {
                    for (Pair<Integer, Pair<Integer, Integer>> p : rightRolesOfUsers) {
                        if (p.v1 != null && p.v1.equals(sub.userId) && rrI.equals(p.v2.v1) && p.v2.v2 != null && treeId == (p.v2.v2)) {
                            users.add(sub);
                        }
                    }
                }
                if (!users.isEmpty()) {
                    roleToUserList.put(mapOfAllRightRoles.get(rrI), users);
                }
            }
        }
        return roleToUserList;
    }

    public List<RightRoleBean> getAllUserRoles() {
        List<RightRoleBean> allRoles;
        allRoles = getAllRightRoles();
        return allRoles;
    }

    public void grantRightsFromContextForGroup(Map<RightRoleBean, List<SystemUserGroupBean>> rightToGroupMap, int treeId, Integer nodeId, String rightsProc) {
        for (Map.Entry<RightRoleBean, List<SystemUserGroupBean>> mapPair : rightToGroupMap.entrySet()) {
            for (SystemUserGroupBean sugb : mapPair.getValue()) {
                execNamedCommand("grantRightsFromContextForGroup", mapPair.getKey().id, sugb.id, treeId, nodeId, rightsProc);
            }
        }
        execNamedCommand("recalculateCustomRightRoleViewBranchGrants");
    }

    public Map<SystemUserBean, List<RightRoleBean>> getUsersMappedToRoles(String textOfFilter, Boolean isUserAccountDisabled,
            Boolean hasCommunityUser, RightRoleBean selectedRightRole, Set<Integer> selectedAuthorTreeIDs,
            Integer optCustomRightRoleId, boolean showUserInRoleFromGroup) {
        String avatarFiled = getAvatarFieldForAd();
        textOfFilter = BaseUtils.isStrEmptyOrWhiteSpace(textOfFilter) ? null : textOfFilter;
        List<SystemUserBean> allBikSystemUsers;
        Map<SystemUserBean, List<RightRoleBean>> usersMappedToRole = new HashMap<SystemUserBean, List<RightRoleBean>>();

        if (selectedRightRole != null) {
            selectedRightRole = selectedRightRole.id == -1 ? null : selectedRightRole;
            allBikSystemUsers
                    = createBeansFromNamedQry("getAllBikSystemUserWithRightRoleFilter", SystemUserBean.class,
                            avatarFiled, textOfFilter, isUserAccountDisabled, hasCommunityUser, selectedRightRole, selectedAuthorTreeIDs, optCustomRightRoleId, showUserInRoleFromGroup);
        } else {
            allBikSystemUsers = createBeansFromNamedQry("getAllBikSystemUserWithFilter", SystemUserBean.class,
                    avatarFiled, textOfFilter, isUserAccountDisabled, hasCommunityUser, selectedAuthorTreeIDs, optCustomRightRoleId, showUserInRoleFromGroup);
        }

        updateDisabledUsersCache(allBikSystemUsers);

        Map<Integer, SystemUserBean> mapOfSystemUsers = BaseUtils.makeBeanMap(allBikSystemUsers);
        List<RightRoleBean> allRightRoles = getAllRightRoles();
        Map<Integer, RightRoleBean> mapOfAllRightRoles = BaseUtils.makeBeanMap(allRightRoles);

        List<Pair<Integer, Pair<Integer, Integer>>> rightRolesOfUsers = getAllRightRolesOfUsersByIds();
        for (Pair<Integer, Pair<Integer, Integer>> p : rightRolesOfUsers) {
            SystemUserBean sub = mapOfSystemUsers.get(p.v1);
            RightRoleBean rrb = mapOfAllRightRoles.get(p.v2.v1);
            if (!BaseUtils.isMapEmpty(usersMappedToRole) && usersMappedToRole.containsKey(sub)) {
                usersMappedToRole.get(sub).add(rrb);
            } else {
                List<RightRoleBean> rightsList = new ArrayList<RightRoleBean>();
                rightsList.add(rrb);
                usersMappedToRole.put(sub, rightsList);
            }
            // add creator and non public user tree ids
            if (sub != null && rrb != null) {
                if (rrb.code.equals("Creator")) {
                    Integer creatorTreeID = p.v2.v2;

                    if (!disableBuiltInRoles()) {
                        if (sub.userRightRoleCodes == null) {
                            sub.userRightRoleCodes = new LinkedHashSet<String>();
                        }
                        sub.userRightRoleCodes.add(rrb.code);
                    }
                    if (creatorTreeID != null) {
                        if (sub.userCreatorTreeIDs == null) {
                            sub.userCreatorTreeIDs = new LinkedHashSet<Integer>();
                        }
                        sub.userCreatorTreeIDs.add(creatorTreeID);
                    }
                }
                if (rrb.code.equals("Author")) {
                    Integer authorTreeID = p.v2.v2;

                    if (!disableBuiltInRoles()) {
                        if (sub.userRightRoleCodes == null) {
                            sub.userRightRoleCodes = new LinkedHashSet<String>();
                        }
                        sub.userRightRoleCodes.add(rrb.code);
                    }
                    if (authorTreeID != null) {
                        if (sub.userAuthorTreeIDs == null) {
                            sub.userAuthorTreeIDs = new LinkedHashSet<Integer>();
                        }
                        sub.userAuthorTreeIDs.add(authorTreeID);
                    }
                }

                if (rrb.code.equals("NonPublicUser")) {
                    Integer nonPublicUserTreeID = p.v2.v2;

                    if (!disableBuiltInRoles()) {
                        if (sub.userRightRoleCodes == null) {
                            sub.userRightRoleCodes = new LinkedHashSet<String>();
                        }
                        sub.userRightRoleCodes.add(rrb.code);
                    }
                    if (nonPublicUserTreeID != null) {
                        if (sub.nonPublicUserTreeIDs == null) {
                            sub.nonPublicUserTreeIDs = new LinkedHashSet<Integer>();
                        }
                        sub.nonPublicUserTreeIDs.add(nonPublicUserTreeID);
                    }
                }
            }
        }

        if (!mapOfSystemUsers.isEmpty()) {
            List<CustomRightRoleUserEntryBean> crruebs = createBeansFromNamedQry("getCustomRightRoleUserEntries",
                    CustomRightRoleUserEntryBean.class, mapOfSystemUsers.keySet());

            for (CustomRightRoleUserEntryBean crrueb : crruebs) {
                SystemUserBean sub = mapOfSystemUsers.get(crrueb.userId);
                Set<TreeNodeBranchBean> ents;
                if (sub.customRightRoleUserEntries == null) {
                    sub.customRightRoleUserEntries = new HashMap<Integer, Set<TreeNodeBranchBean>>();
                    ents = null;
                } else {
                    ents = sub.customRightRoleUserEntries.get(crrueb.roleId);
                }
                boolean noEnts = ents == null;
                if (noEnts) {
                    ents = new HashSet<TreeNodeBranchBean>();
                }
                ents.add(new TreeNodeBranchBean(crrueb.treeId, crrueb.nodeId, crrueb.branchIds));
                if (noEnts) {
                    sub.customRightRoleUserEntries.put(crrueb.roleId, ents);
                }
            }

            List<CustomRightRoleUserEntryBean> crruebsDomain = createBeansFromNamedQry("getCustomRightRoleUserFromRoleInGroup",
                    CustomRightRoleUserEntryBean.class, mapOfSystemUsers.keySet());
            for (CustomRightRoleUserEntryBean cd : crruebsDomain) {
                SystemUserBean sub = mapOfSystemUsers.get(cd.userId);
                if (sub.customRightRoleUserEntriesFromGroup == null) {
                    sub.customRightRoleUserEntriesFromGroup = new HashSet<Integer>();
                }
                sub.customRightRoleUserEntriesFromGroup.add(cd.roleId);
            }
        }
        return usersMappedToRole;
    }

    public Integer isDeletedNode(Integer nodeId) {

//        return execNamedCommand("isDeletedNode", nodeId);
        return execNamedQuerySingleValAsInteger("isDeletedNode", true, null, nodeId);
//        return execNamedCommand("getDeleteDateFromHistory", nodeId);
    }

    public Set<String> getCurrentUserCustomRightRoleInGroup(int userId) {
        Set<String> res = execNamedQuerySingleColAsSet("getCurrentUserCustomRightRoleInGroup", "code", userId);
        return res;
    }

    private void checkMetaModelRequiredAttribute(String nodeKind, String metaAttrName) throws LameRuntimeException {
        List<Object> objId = new ArrayList<Object>();

        objId.addAll(execNamedQuerySingleColAsSet("metaModelCheckRequiredAttribute", null, nodeKind, metaAttrName));
        if (!BaseUtils.isCollectionEmpty(objId)) {
            throw new LameRuntimeException(BaseUtils.mergeWithSepEx(objId, ",\n") + " nie ma atrybutu: " + metaAttrName);
        }
    }

    private void checkMetaModelChildHasParentAsLeaf() throws LameRuntimeException {
        List<Object> objId = new ArrayList<Object>();

        objId.addAll(execNamedQuerySingleColAsSet("metaModelCheckChildHasParentAsLeaf", null));
        if (!BaseUtils.isCollectionEmpty(objId)) {
            throw new LameRuntimeException(BaseUtils.mergeWithSepEx(objId, ",\n") + " ma element nadrzędny jako liść");
        }
    }

    private void checkMetaModelParentWithoutChild() throws LameRuntimeException {
        List<Object> objId = new ArrayList<Object>();

        objId.addAll(execNamedQuerySingleColAsSet("checkMetaModelParentWithoutChild", null));
        if (!BaseUtils.isCollectionEmpty(objId)) {
            throw new LameRuntimeException(BaseUtils.mergeWithSepEx(objId, ",\n") + " nie ma dziecka");
        }
    }

    private void checkMetaModelAddAllNodeKind4TreeKind() throws LameRuntimeException {
        List<Object> objId = new ArrayList<Object>();

        objId.addAll(execNamedQuerySingleColAsSet("metaModelCheckAddAllNodeKind4TreeKind", null));
        if (!BaseUtils.isCollectionEmpty(objId)) {
            throw new LameRuntimeException(BaseUtils.mergeWithSepEx(objId, ",\n") + " nie było dodano jako typ w drzewie");
        }
    }

    private void checkMetaModelTreeKindHasBranch() throws LameRuntimeException {
        List<Object> objId = new ArrayList<Object>();

        objId.addAll(execNamedQuerySingleColAsSet("metaModelCheckTreeKindHasBranch", null));
        if (!BaseUtils.isCollectionEmpty(objId)) {
            throw new LameRuntimeException(BaseUtils.mergeWithSepEx(objId, ",\n") + " nie ma gałęzi");
        }
    }

    private void checkMetaModelTreeKindHasLeaf() throws LameRuntimeException {
        List<Object> objId = new ArrayList<Object>();

        objId.addAll(execNamedQuerySingleColAsSet("metaModelCheckTreeKindHasLeaf", null));
        if (!BaseUtils.isCollectionEmpty(objId)) {
            throw new LameRuntimeException(BaseUtils.mergeWithSepEx(objId, ",\n") + " nie ma liści");
        }
    }

    private List<Integer> filtrSearchResultsByRole(List<Integer> nodeIds) {
        List<Integer> res = new ArrayList<Integer>();
        int i = 0;
        while (i < nodeIds.size()) {
            List<Integer> l = execNamedQuerySingleColAsList("filtrSearchResultsByRole", "id", nodeIds.subList(i, Math.min(nodeIds.size(), i + DEFAULT_PACK_SIZE)));
            res.addAll(l);
            i += DEFAULT_PACK_SIZE;
        }
        return res;
    }

    private List<Integer> filtrMetaBiksSearchResults(List<Integer> nodeIds) {
        List<Integer> res = new ArrayList<Integer>();
        int i = 0;
        while (i < nodeIds.size()) {
            List<Integer> l = execNamedQuerySingleColAsList("filtrMetaBiksSearchResults", "id", nodeIds.subList(i, Math.min(nodeIds.size(), i + DEFAULT_PACK_SIZE)));
            res.addAll(l);
            i += DEFAULT_PACK_SIZE;
        }
        return res;
    }

    private Map<Integer, Integer> countTotalResults(Integer searchId, Set<Integer> nodeKindIds) {
        Map<Integer, Integer> res = new HashMap<Integer, Integer>();
        List<Map<String, Object>> l = execNamedQueryCFN("countTotalResults", FieldNameConversion.ToLowerCase, searchId, nodeKindIds);
        for (Map<String, Object> m : l) {
            res.put((Integer) m.get("tree_id"), (Integer) m.get("cnt"));
        }
        return res;
    }

    private List<TreeNodeBean> getSearchResultsFromCache(Integer searchId, int pageNum, int pageSize, Set<Integer> nodeKindIds, Set<String> selectedAttributeName) {
        return createBeansFromNamedQry("getSearchResultsFromCache", TreeNodeBean.class, searchId, pageNum * pageSize + 1, (pageNum + 1) * pageSize, nodeKindIds, selectedAttributeName);
    }

    private void save2SearchCache(Integer searchId, List<Integer> nodeIds) {
        int i = 0;
        while (i < nodeIds.size()) {
            execNamedCommand("save2SearchCache", searchId, nodeIds.subList(i, Math.min(nodeIds.size(), i + DEFAULT_PACK_SIZE)));
            i += DEFAULT_PACK_SIZE;
        }
    }

    private void checkMetaModelHasRepeatedName(String nodeKind, String attrName) {
        List<Map<String, Object>> list = execNamedQueryCFN("checkMetaModelHasRepeatedName", FieldNameConversion.ToLowerCase, nodeKind, attrName);
        if (list.size() > 0) {
            throw new LameRuntimeException("Powtórzony atrybut: " + attrName + " np.: " + list.get(0).get("value"));
        }
    }

    @Override
    public void addSubmenu(Integer parentMenuId, String name) {
        List<String> attrs = new ArrayList<String>();
        attrs.add(BIKConstants.METAMENU_OBJ_ID);
        List<AttributeBean> list = getAttributesForNode(parentMenuId, attrs);
        String parentMenuCode = null;
        for (AttributeBean bean : list) {
            if (BIKConstants.METAMENU_OBJ_ID.equalsIgnoreCase(bean.atrName)) {
                parentMenuCode = bean.atrLinValue;
            }
        }
        String sectionCode = BaseUtils.safeToStringDef(parentMenuCode, "");
        if (!BaseUtils.isStrEmptyOrWhiteSpace(parentMenuCode)) {
            sectionCode += ":";
        }
        sectionCode += BaseUtils.fixIdentName(BaseUtils.deAccentPolishChars(name), "", false);
        execNamedQuerySingleVal("addMenuSubGroup", false, null, name, sectionCode, 0, parentMenuCode, null);
        buildMenuConfigTree(true);
    }

    @Override
    public boolean canAccess(Method mtc) {
        return true;
    }

    @Override
    public boolean userNotLoggedInError(Method implMethod) {
        NotRequireUserLoggedIn notRequireLoggedUser = implMethod.getAnnotation(NotRequireUserLoggedIn.class);
        if (notRequireLoggedUser == null) {
            return getLoggedUserBean() == null;
        } else {
            return false;
        }
    }

    @Override
    public Exception getExceptionForNotLoggedUser() {
        throw new LameRuntimeException("User is not logged in");
    }

    @Override
    public void runScript(Integer nodeId, String script) {
        script = cleanHtml(stripHtmlTags(script));
        execNamedCommand("runScript", nodeId, script);
    }

    private List<Integer> changeLinkedNodeIds2Original(List<Integer> nodeIds2Export) {
        List<Integer> res = new ArrayList();
        for (Integer nodeId : nodeIds2Export) {
            Integer linkedNodeId = execNamedQuerySingleValAsInteger("getLinkedNodeId", true, null, nodeId);
            res.add(linkedNodeId == null ? nodeId : linkedNodeId);
        }
        return res;
    }

    protected String getTreeDocumentExportFolderPath() {
        return BaseUtils.ensureDirSepPostfix(BaseUtils.ensureDirSepPostfix(cfg.dirForUpload) + "treeexport");
    }

    protected String getJaspeReportsExportFolderPath() {
        return BaseUtils.ensureDirSepPostfix(BaseUtils.ensureDirSepPostfix(cfg.dirForUpload) + "reports");
    }

    public ParametersAnySqlBean addAnySql(String sourceName, String serverName) {
        Integer sqlId = execNamedQuerySingleValAsInteger("addAnySql", true, "id" /* i18n: no */, sourceName, serverName);
        return getAnySqlForSqlId(sqlId);

    }

    private ParametersAnySqlBean getAnySqlForSqlId(Integer sqlId) {
        return createBeanFromNamedQry("getAnySqlForSqlId", ParametersAnySqlBean.class, false, sqlId);
    }

    public void setAnySqlParameters(ParametersAnySqlBean bean) {
        execNamedCommand("setAnySqlParameters", bean.id, bean.name, bean.query, bean.isActive);
//        setVisibleTree(bean.treeCode, (bean.isActive && BaseUtils.isStrEmptyOrWhiteSpace(bean.folderPath)));
    }

    public void deleteAnySql(String sourceName, int id, String serverName) {
//            ParametersAnySqlBean bean = getAnySqlForSqlId(id);
//        setVisibleTree(bean.treeCode, false);
        execNamedCommand("deleteDBConfig", "bik_any_sql", id, sourceName, serverName);
    }

    @Override
    public Pair<Integer, List<MailLogBean>> getMailLogs(int pageNum, int pageSize) {
        Integer cnt = execNamedQuerySingleValAsInteger("getMailLogsCnt", true, null);
        if (cnt == null) {
            cnt = 0;

        }
        List<MailLogBean> logList = createBeansFromNamedQry("getMailLogs", MailLogBean.class, pageNum * pageSize + 1, (pageNum + 1) * pageSize);
        return new Pair<Integer, List<MailLogBean>>(cnt, logList);
    }

    public void updateRoleDescription(NameDescBean nameDesc, Integer id) {
        String safeContent = cleanHtml(nameDesc.content);
        execNamedCommand("updateRoleDescription", safeContent, id);
    }

    public String getRoleDescr(Integer id) {
        return BaseUtils.safeToStringNotNull(execNamedQuerySingleVal("getRoleDescr", true, null, id));
    }

    public List<SystemUserBean> getAllUsersWithRightsInCurrentNode(int node_id, int group_id) {
        List<SystemUserBean> allUsers = createBeansFromNamedQry("getAllUsers", SystemUserBean.class);
        List<SystemUserBean> tempUsersInNode = createBeansFromNamedQry("getCustomRightsNewRightSys", SystemUserBean.class, node_id, group_id);

        for (SystemUserBean sub : allUsers) {
            for (SystemUserBean subInNode : tempUsersInNode) {
                if (sub.id == subInNode.id) {
                    sub.userHasRightsInCurrentNode = true;
                }
            }
        }
        return allUsers;
    }

    public List<TreeNodeBean> getAttributeOrKindObjectToConfigPrintTemplate(Integer nodeId, Integer nodeIdObjectKind, String nodeKindCode, String nodeKindCodeFromAttrLis, String parentName) {
        List<String> nkc = BaseUtils.splitBySep(nodeKindCodeFromAttrLis, ",");
        return createBeansFromNamedQry("getAttributeOrKindObjectToConfigPrintTemplate", TreeNodeBean.class, nodeId, nodeIdObjectKind, nodeKindCode, nkc, parentName);
    }
//276179

    public Integer createTreeNodesAttribute(Integer parentNodeId, List<Integer> directOverride, int nodeKindId) {

        List<TreeNodeBean> tnbs = createBeansFromNamedQry("getDirectOverride", TreeNodeBean.class, directOverride);
        Integer creatorId = getLoggedUserBean().userId != null ? getLoggedUserBean().userId : getLoggedUserBean().id;

        Integer metaBiksBuiltInNodeKind = execNamedQuerySingleValAsInteger("getNodeKindIdByCode", true, null, BIKConstants.NODE_KIND_META_PRINT_BUILT_IN_KIND);

        Integer attributeId = (Integer) execNamedQuerySingleVal("getPrintAttributeId", true, null /* I18N: no */, nodeKindId);
        Integer nodeIdfirtsObject = null;
        for (TreeNodeBean tnb : tnbs) {

            if (tnb.nodeKindCode.equals(BIKConstants.NODE_KIND_META_BUILT_IN_NODE_KIND)) {
                nodeKindId = metaBiksBuiltInNodeKind;
                attributeId = (Integer) execNamedQuerySingleVal("getPrintAttributeId", true, null /* I18N: no */, nodeKindId);
            }

            Pair<Integer, List<String>> p = createTreeNode(parentNodeId,
                    nodeKindId, tnb.name, null, tnb.treeId, null/*tnb.id*/, false);
            if (nodeIdfirtsObject == null) {
                nodeIdfirtsObject = p.v1;
            }
            AttributeBean ab = new AttributeBean();
            ab.id = attributeId;
            ab.atrLinValue = tnb.id + "_" + getTreeCodeById(tnb.treeId) + "," + tnb.name;
//            <f:set var="" params="id, creatorId, abId,atrLinValue">

            execNamedCommand("setPatternAttrToPrintAttrOrObject", p.v1, creatorId, ab);
        }
        return nodeIdfirtsObject;
//        return null;
//        execNamedCommand("createTreeNodes", parentNodeId, directOverride)
    }

    public void createDocument4TreeProducerPrint(int treeId, List<Integer> nodeIds2Export, int maxDepth, int numbering, String nodeKindCode, Integer templateNodeId, Integer maxPrintLevel,
            boolean showNewLine, boolean showAuthor, boolean isPDFFormat, IParametrizedContinuation onGetContentSuccess) throws IOException {
        if (maxDepth <= 0) {
            return;
        }
        List<TreeNodeBean> tbns = new ArrayList<TreeNodeBean>();

//        List<Integer> ids = execNamedQuerySingleColAsList("getFirstRankNodeIdsOfTree", null, treeId);
//        onGetContentSuccess.doIt(generateTitlePage(treeId));
//        generateDoc4Nodes(numbering, ids, "", 0, maxDepth, onGetContentSuccess);
        nodeIds2Export = changeLinkedNodeIds2Original(nodeIds2Export);
        Integer nodeId = nodeIds2Export.get(0);
        TreeNodeBean node = getNodeBeanById(nodeId, false);
        onGetContentSuccess.doIt(generateTitlePage(node.name));
        for (Integer i : nodeIds2Export) {
            TreeNodeBean tnb = new TreeNodeBean();
            tnb.id = i;
            tnb.nodeKindCode = nodeKindCode;
            tnb.templateNodeId = templateNodeId;
            tnb.maxPrintLevel = maxPrintLevel;
            tbns.add(tnb);
        }
        generateDoc4NodesPrint(numbering, tbns/*nodeIds2Export*/, "", 0, maxDepth, showNewLine, showAuthor, isPDFFormat/*, nodeKindCode, template*/, onGetContentSuccess
        );

    }

    @SuppressWarnings("unchecked")
    public void generateDoc4NodesPrint(int numbering, List<TreeNodeBean> ids, String pref, int level, int maxDepth, boolean showNewLine, boolean showAuthor, boolean isPDFFormat,/* String nodeKindCode, String template,*/ IParametrizedContinuation onGetContentSuccess) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cnt = 0;
        for (TreeNodeBean tnb : ids) {
            Integer nodeId = tnb.id;
            cnt++;
            TreeNodeBean node = getNodeBeanById(nodeId, false);

            String newLineStyle = showNewLine ? " style=\"page-break-before:always\"" : "";
            sb = sb.append("<h2");
            sb.append(newLineStyle);
            sb.append(">").append(numbering == 0 ? "" : pref + cnt + ". ").append(node.name).append("</h2>").append("\n");

//            sb.append("<div style=\"page-break-before:always\">some text befroe</div>\n"
            //                  + "<div style=\"page-break-after:always\">some textafter</div> \n"
            //                + "<div style=\"page-break-inside:avoid\">some textavoid</div> ");
            String descr = node.descr;
            if (BaseUtils.isStrEmptyOrWhiteSpace(descr)) {
                descr = node.descrPlain;
            }

            descr = parseSpecialTags(nodeId, descr, isPDFFormat);

            if (!BaseUtils.isStrEmptyOrWhiteSpace(descr)) {
//                sb.append("<b>Opis:</b>\n");
                sb.append(descr).append("\n<br/>");

            }
            List<AttributeBean> attrs;
            boolean isBasic = tnb.maxPrintLevel == null || tnb.templateNodeId == null;/* tnb.defaultTemplate.equals(BIKConstants.BASIC_TEMPLATE );*/

//              List<AttributeBean> attrs = createBeansFromNamedQry("getAttributeValueOfNode", AttributeBean.class, node.linkedNodeId != null ? node.linkedNodeId : node.id);
            if (!isBasic) {
                attrs = createBeansFromNamedQry("getPatterAndValueAttrPrintX", AttributeBean.class, node.linkedNodeId != null ? node.linkedNodeId : node.id, 1, tnb.nodeKindCode, tnb.templateNodeId);
            } else {
                attrs = createBeansFromNamedQry("getAttributeValueOfNodeWithSort", AttributeBean.class, node.linkedNodeId != null ? node.linkedNodeId : node.id, tnb.nodeKindCode);

            }
            if (!BaseUtils.isCollectionEmpty(attrs)) {
//                sb.append("<b>Atrybuty:</b>\n").append("<br/>\n");
                sb.append("<ul>\n");

                for (AttributeBean attr : attrs) {
                    String s = "";
                    String attrName = attr.atrName.indexOf('.') < 0 ? attr.atrName : attr.atrName.split("\\.")[1];
                    if (attr.typeAttr.equals(AttributeType.hyperlinkInMulti.name()) || attr.typeAttr.equals(AttributeType.hyperlinkInMono.name())
                            || attr.typeAttr.equals(AttributeType.permissions.name())) {
                        List<String> values = BaseUtils.splitBySep(attr.valueOpt, "|", true);
                        List<Integer> nodeKindToPrint = execNamedQuerySingleColAsList("getPrintNodeKindId", null, attrName, tnb.nodeKindCode, tnb.templateNodeId, 2);

                        List<Integer> filterNodeByPrintKind = null;
                        List<Integer> getFilterNodeRightRestrictionByPrintKind = new ArrayList<Integer>();

                        if (!BaseUtils.isCollectionEmpty(nodeKindToPrint)) {
                            filterNodeByPrintKind = execNamedQuerySingleColAsList("getFilterNodeByPrintKind", null, attr.valueOpt, nodeKindToPrint);
                            getFilterNodeRightRestrictionByPrintKind = execNamedQuerySingleColAsList("getFilterNodeRightRestrictionByPrintKind", null, attr.valueOpt, nodeKindToPrint);
                        }

//                        Map<Integer, String> nodeIdToValue = new HashMap();
//                        if (!BaseUtils.isCollectionEmpty(values)) {
//                            for (String v : values) {
//                                if (BaseUtils.isStrEmptyOrWhiteSpace(v)) {
//                                    continue;
//                                }
//                                int un = v.indexOf("_");
//                                Integer nodeIdAttr = BaseUtils.tryParseInteger(v.substring(0, un));
//                                nodeIdToValue.put(nodeIdAttr, v);
//
//                            }
//                        }
//                        values.clear();
////                        Map<Integer, String> sortNodeIdToValue = new HashMap();
//                        for (Integer i : getFilterNodeRightRestrictionByPrintKind) {
//                            values.add(nodeIdToValue.get(i));
//                        }
                        if (!BaseUtils.isCollectionEmpty(values)) {
                            s = s + "\n<br/>";
                            for (String v : values) {
                                if (BaseUtils.isStrEmptyOrWhiteSpace(v)) {
                                    continue;
                                }
                                int un = v.indexOf("_");
                                if (un == -1) {
                                    s += "&nbsp;&nbsp;  " + (v + "\n<br/>");
                                } else {
                                    Integer nodeIdAttr = BaseUtils.tryParseInteger(v.substring(0, un));
                                    if (BaseUtils.isCollectionEmpty(nodeKindToPrint)
                                            || (BaseUtils.collectionContainsFix(filterNodeByPrintKind, nodeIdAttr))) {

                                        int p = v.indexOf(",");
                                        if (p >= 0) {
                                            s += "&nbsp;&nbsp;  " + (v.substring(p + 1) + "\n<br/>");
                                            String returnz = "";
                                            if (tnb.maxPrintLevel != null && tnb.templateNodeId != null /* !tnb.defaultTemplate.equals(BIKConstants.BASIC_TEMPLATE)*/ && BaseUtils.collectionContainsFix(getFilterNodeRightRestrictionByPrintKind, nodeIdAttr)) {
                                                s += generateDoc4NodesPrintInner(returnz, v, attrName, 2, tnb.nodeKindCode, tnb.templateNodeId, tnb.maxPrintLevel, attr.templateBranch);
                                            }
                                        }
                                    }
                                }
                            }
                            if (s.endsWith("\n<br/>")) {
                                s = s.substring(0, s.length() - 5);
                            }
                        }
                    } else {
                        s = BaseUtils.safeToStringNotNull(attr.valueOpt);
                    }
//                    String attrName = attr.atrName.indexOf('.') < 0 ? attr.atrName : attr.atrName.split("\\.")[1];
                    sb.append("<b>").append(attrName).append(":</b> ").append(s).append("<br/>\n");
                }
                sb.append("</ul>\n");
            }

            //powiazania
            Map<String, List<String>> joinedAttrsMap = new HashMap<String, List<String>>();
            List<String> withoutAttrs = new ArrayList<String>();
            List<JoinedObjBasicInfoBean> joinedNodeList = createBeansFromNamedQry("getJoinedObjs4Node", JoinedObjBasicInfoBean.class, node.id);
            for (JoinedObjBasicInfoBean obj : joinedNodeList) {
                Integer joinedObjId = execNamedQuerySingleValAsInteger("getJoinedObjIdBySrcAndDst", false, null, obj.srcId, obj.dstId);
                //atrybuty
                List<JoinedObjAttributeLinkedBean> joinedAttrs = getJoinedAttributesLinked(joinedObjId);
                if (BaseUtils.isCollectionEmpty(joinedAttrs)) {
                    withoutAttrs.add(obj.dstName);
                } else {
                    for (JoinedObjAttributeLinkedBean joinedAttr : joinedAttrs) {
                        if (joinedAttr.dstId == obj.dstId) {
                            List<String> l = joinedAttrsMap.get(obj.dstName);
                            if (l == null) {
                                l = new ArrayList<String>();
                            }
                            StringBuilder s = new StringBuilder();
                            if (joinedAttr.isMain) {
                                s.append("<b>");
                            }
                            s.append(joinedAttr.value).append(" (").append(joinedAttr.name).append(")\n");
                            if (joinedAttr.isMain) {
                                s.append("</b>");
                            }
                            l.add(s.toString());
                            joinedAttrsMap.put(obj.dstName, l);
                        }
                    }
                }
            }

            if (!BaseUtils.isCollectionEmpty(withoutAttrs)
                    || joinedAttrsMap.size() > 0) {
                sb.append("<b>Powiązane z:</b>\n").append("<br/>\n");
            }

            if (!BaseUtils.isCollectionEmpty(withoutAttrs)) {
                sb.append("<ul>\n");
                for (String s : withoutAttrs) {
                    sb.append("<li>").append(s).append("</li>\n");
                }
                sb.append("</ul>\n");
            }

            if (joinedAttrsMap.size()
                    > 0) {
//                sb.append("<b>Powiązania z atrybutami:</b>\n").append("<br/>");
                sb.append("<ul>\n");
                for (Entry<String, List<String>> e : joinedAttrsMap.entrySet()) {
//                    sb.append("<li>\n");
                    sb.append("<ul>\n");
                    for (String p : e.getValue()) {
                        sb.append("<li>").append(e.getKey() + ": ").append(p).append("</li>\n");
                    }
//                    sb.append("---" + e.getKey() + "---").append("\n");
                    sb.append("</ul>\n");
//                    sb.append("</li>\n");
                }
                sb.append("</ul>");
            }

//            if (!BaseUtils.isStrEmptyOrWhiteSpace(descr)) {
//                sb = sb.append(Html2Md.convertHtml(descr, "UTF-8")).append("\n");
//            }
            //Osoby
            if (showAuthor) {
                Pair<List<UserInNodeBean>, Map<Integer, String>> usersInNode = getUsersInNode(nodeId, node.branchIds);

                if (!BaseUtils.isCollectionEmpty(usersInNode.v1)) {
                    sb.append("<ul>\n");
                    for (UserInNodeBean bean : usersInNode.v1) {
                        List<Integer> nodeIdsFromBranch = BIKCenterUtils.splitBranchIdsEx(bean.userInRoleBranchIds, false);
                        List<String> nodeNames = new ArrayList<String>();
                        for (Integer id : nodeIdsFromBranch) {
                            if (bean.userInRoleNodeId != id) {
                                String nodeName = execNamedQuerySingleVal("nodeNamesById", true, null, id);
                                nodeNames.add(nodeName);
                            }
                        }
                        String pathStr = BaseUtils.mergeWithSepEx(nodeNames, BIKGWTConstants.RAQUO_STR_SPACED);
                        String role = pathStr + (bean.isAuxiliary ? " (" + I18n.pomocniczy.get() + ")" : "")
                                + (bean.inheritToDescendants ? " *" : "");

                        sb.append("<li>").append(bean.userName).append(" - ").append(role).append("</li>\n");
                    }
                    sb.append("</ul>\n");
                }
            }
            onGetContentSuccess.doIt(sb.toString());
            sb.setLength(0);
            getNodesDFS2(numbering, numbering == 0 ? "" : pref + cnt + ".", level + 1, maxDepth, node.linkedNodeId == null ? nodeId : node.linkedNodeId, showNewLine, showAuthor, isPDFFormat, onGetContentSuccess);
        }
    }

    protected String generateDoc4NodesPrintInner(String returnz, String v, String atrName, int poziom, String nodeKindCode, Integer templateNodeId, Integer maxPrintLevel, String templateBranch) {
        String empty = "";
        String attrName = atrName.indexOf('.') < 0 ? atrName : atrName.split("\\.")[1];
        Integer nodeIdAttr = Integer.parseInt(v.substring(0, v.indexOf("_")));
        List<AttributeBean> attrs2 = createBeansFromNamedQry("getPatterAndValueAttrPrintX2", AttributeBean.class, nodeIdAttr, poziom, attrName, nodeKindCode, templateNodeId, templateBranch);
        if (BaseUtils.isCollectionEmpty(attrs2)) {
            poziom = 2;
        }

        for (int ii = 1; ii < poziom; ii++) {
            empty += "&nbsp;&nbsp;&nbsp;";
        }
        for (AttributeBean attr2 : attrs2) {
            StringBuilder sb = new StringBuilder();
//            String s = "";
            String addbr = "";
            if (attr2.typeAttr.equals(AttributeType.hyperlinkInMulti.name()) || attr2.typeAttr.equals(AttributeType.hyperlinkInMono.name())
                    || attr2.typeAttr.equals(AttributeType.permissions.name())) {
                List<String> values = BaseUtils.splitBySep(attr2.valueOpt, "|");

                if (values.size() > 1 || (values.size() == 1 && (values.get(0).indexOf("_") != -1))) {

                    List<Integer> nodeKindToPrint = execNamedQuerySingleColAsList("getPrintNodeKindId", null, attr2.atrName, nodeKindCode, templateNodeId, poziom + 2);
                    List<Integer> filterNodeByPrintKind = null;
                    List<Integer> getFilterNodeRightRestrictionByPrintKind = new ArrayList<Integer>();
                    if (!BaseUtils.isCollectionEmpty(nodeKindToPrint)) {
                        filterNodeByPrintKind = execNamedQuerySingleColAsList("getFilterNodeByPrintKind", null, attr2.valueOpt, nodeKindToPrint);
                        getFilterNodeRightRestrictionByPrintKind = execNamedQuerySingleColAsList("getFilterNodeRightRestrictionByPrintKind", null, attr2.valueOpt, nodeKindToPrint);
                    }
                    for (String val : values) {
                        if (BaseUtils.isStrEmptyOrWhiteSpace(val)) {
                            continue;
                        }
                        int un = val.indexOf("_");
                        Integer nodeIdAttr2 = BaseUtils.tryParseInteger(val.substring(0, un));

                        if (BaseUtils.isCollectionEmpty(nodeKindToPrint)
                                || (nodeIdAttr2 != null && BaseUtils.collectionContainsFix(filterNodeByPrintKind, nodeIdAttr2))) {

                            int p = val.indexOf(",");
                            if (p >= 0) {
                                String attrValue = val.substring(p + 1);
                                if (!BaseUtils.isStrEmptyOrWhiteSpace(attrValue)) {
                                    sb.append(empty).append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").append(attrValue).append("\n<br/>");
                                    if (BaseUtils.collectionContainsFix(getFilterNodeRightRestrictionByPrintKind, nodeIdAttr2)) {
                                        poziom = poziom + 2;
                                        sb.append(generateDoc4NodesPrintInner("", val, attr2.atrName, poziom, nodeKindCode, templateNodeId, maxPrintLevel, attr2.templateBranch));
                                    }
                                } else {
                                    sb.append(empty).append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                }
                                poziom = poziom - 2;
                            }
                        }

                    }
                    poziom = 2;
                    addbr = "\n<br/> ";
                } else {
                    addbr = "";
                    sb.append(BaseUtils.safeToStringNotNull(attr2.valueOpt)).append("\n<br/>");
                }
            } else {
                addbr = "";
                sb.append(BaseUtils.safeToStringNotNull(attr2.valueOpt)).append("\n<br/>");
            }

            returnz += empty + "&nbsp;&nbsp;&nbsp;<b>" + attr2.atrName + ":&nbsp;</b>" + addbr/*"</b><br/> "*/ + sb.toString();//+ "\n<br/>";
        }
        return returnz;
    }

    public Map<String, PrintTemplateBean> getPrintTemplates(String nodeKindCode) {

        List<PrintTemplateBean> printTmp = createBeansFromNamedQry("getPrintTemplates", PrintTemplateBean.class, nodeKindCode);

        Map<String, PrintTemplateBean> ptbMap = new HashMap<String, PrintTemplateBean>();

        for (PrintTemplateBean pt : printTmp) {
            ptbMap.put(pt.template, pt);
        }

        if (ptbMap.get(BIKConstants.BASIC_TEMPLATE) == null) {
            PrintTemplateBean basic = new PrintTemplateBean();
            basic.maxLevel = null;
            basic.showAuthor = false;
            basic.showNewLine = true;
            basic.showPageNumber = true;
            basic.template = BIKConstants.BASIC_TEMPLATE;
            ptbMap.put(basic.template, basic);
        }
        return ptbMap;
    }

    public void resetVisualOrder(Integer parentId, int treeId) {
        TreeNodeBean tbs = createBeanFromNamedQry("getPrintParentKind", TreeNodeBean.class, true, parentId, treeId);
        if (tbs.nodeKindCode.equals(BIKConstants.NODE_KIND_META_PRINT)) {
            String branchId = tbs.branchIds.replace(tbs.parentNodeId + "|", "").replace(parentId + "|", "");
            tbs = createBeanFromNamedQry("getPrintParentKindOpt", TreeNodeBean.class, true, branchId, treeId);

        }
        execNamedCommand("resetVisualOrder", tbs.name, treeId, parentId);
    }

    public String nodeNamesById(Integer nodeId) {
        return execNamedQuerySingleVal("nodeNamesById", true, null, nodeId);
    }

    public void setStatus(int nodeId, Map<String, AttributeBean> statusAttrs) {

        Integer creatorId = getLoggedUserBean().userId != null ? getLoggedUserBean().userId : getLoggedUserBean().id;
        if (statusAttrs != null && !statusAttrs.isEmpty()) {
            execNamedCommand("setRequiredAttrs", nodeId, creatorId, statusAttrs);
        }

    }
}
