﻿exec sp_check_version '1.1.9.11';
go

delete from aaa_teradata_object
go

alter table aaa_teradata_object drop constraint aaa_teradata_object_uniq
go

alter table aaa_teradata_object drop column parent_id
go

alter table aaa_teradata_object alter column extra_info varchar(max) null
go

alter table aaa_teradata_object alter column branch_names varchar(1000) null
go

alter table aaa_teradata_object add parent_branch_names varchar(max) null
go

alter table aaa_teradata_object add request_text varchar(max) null
go	

-- usuwanie foreign keya
declare @fk_name sysname
select @fk_name = foreignKey_name from dbo.vw_ww_foreignkeys
where Table_Name = 'bik_teradata' and name = 'parent_teradata_id'
exec ('alter table bik_teradata drop constraint ' + @fk_name)
go

declare @fk_name sysname
select @fk_name = foreignKey_name from dbo.vw_ww_foreignkeys
where Table_Name = 'aaa_teradata_object' and name = 'data_load_log_id'
exec ('alter table aaa_teradata_object drop constraint ' + @fk_name)
go

alter table aaa_teradata_object
add foreign key(data_load_log_id) references bik_data_load_log(id)
go

alter table bik_teradata drop column parent_teradata_id
go

create table aaa_teradata_index(
	id int not null identity(1,1) primary key,
	table_branch_names varchar(max) not null,
	name varchar(max) null,
	type varchar(255) not null,
	unique_flag varchar(255) not null,
	column_name varchar(max) not null,
	column_position int not null
);
go

----

create table bik_sapbo_report_extradata(
	node_id int not null references bik_node(id) unique,
	name varchar(max) null,
	author varchar(max) null,
	description varchar(max) null,
	refresh_on_open bit null,
	last_refresh_duration int null,
	last_saved_by varchar(max) null
);
go

create table bik_sapbo_user(
	si_id int not null primary key,
	name varchar(max) null
);
go

insert into bik_admin(param,value)
values('sapbo.checkRights', '1')


exec sp_update_version '1.1.9.11', '1.1.9.12';
go