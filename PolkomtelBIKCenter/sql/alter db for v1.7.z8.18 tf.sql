﻿exec sp_check_version '1.7.z8.18';
go

if (select val from bik_app_prop where name = 'biks_id') = '7CB602FD-4C29-4C5D-A3A9-82E814EFADD2' -- PLK
begin
	if not exists (select * from bik_app_prop where name = 'availableConnectors' and val like '%DQM%')
	begin
		update bik_app_prop set val = val + ',DQM' where name = 'availableConnectors'
	end
	
	if not exists(select * from bik_node_action where code = '#admin:dqm:schedule')
	begin
		insert into bik_node_action(code)
		values ('#admin:dqm:schedule')
		
		insert into bik_node_action(code)
		values ('#admin:dqm:logs')
		
		insert into bik_node_action_in_custom_right_role (action_id, role_id)
		select (select id from bik_node_action where code = '#admin:dqm:schedule'), id 
		from bik_custom_right_role where code in ('Administrator', 'AppAdmin')
		
				
		insert into bik_node_action_in_custom_right_role (action_id, role_id)
		select (select id from bik_node_action where code = '#admin:dqm:logs'), id 
		from bik_custom_right_role where code in ('Administrator', 'AppAdmin')
		
		
		exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;
	end;
end
		

exec sp_update_version '1.7.z8.18', '1.7.z9';
go
