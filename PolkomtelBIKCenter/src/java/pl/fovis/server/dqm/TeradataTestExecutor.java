/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.dqm;

import commonlib.LameUtils;
import pl.bssg.metadatapump.common.ConnectionParametersDBServerBean;
import pl.fovis.common.dqm.DQMMultiSourceBean;
import pl.trzy0.foxy.serverlogic.db.TeradataConnection;
import pl.trzy0.foxy.serverlogic.db.TeradataConnectionConfig;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class TeradataTestExecutor extends AbstractDatabaseTestExecutor {

    private static final ILameLogger logger = LameUtils.getMyLogger();

    public TeradataTestExecutor(IBIKSDQMSubservice service) {
        super(service);
    }

    @Override
    public boolean initConnection(DQMTestBean test) {
        this.test = test;
        return initConnectionInner(test.dbServerId, test.connectionId);
    }

    protected boolean initConnectionInner(Integer dbServerId, Integer connectionId) {
        try {
            ConnectionParametersDBServerBean serverConfig = getDatabaseConfig(dbServerId, connectionId);
            this.mc = new TeradataConnection(new TeradataConnectionConfig(serverConfig.server, serverConfig.databaseName, serverConfig.user, serverConfig.password, "ANSI", "UTF8", serverConfig.port != null ? String.valueOf(serverConfig.port) : null));
            mc.setDbStatementExecutionWarnThresholdMillis(-1);
            mc.setAutoCommit(true);
            mc.execQrySingleValInteger("select top 1 1 from DBC.TablesV", false);
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in initConnection", e);
            }
            return false;
        }
        return true;
    }

    @Override
    protected String getCallTestFunctionQuery(String functionName) {
        // nieuzywane
        return null;
    }

    @Override
    public boolean initConnection(DQMMultiSourceBean source) {
        this.source = source;
        return initConnectionInner(source.dbServerId, source.connectionId);
    }

    @Override
    public String getSourceName() {
        return "TeradataDBMS";
    }
}
