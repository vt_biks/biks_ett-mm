/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.dialogs;

import com.google.gwt.user.client.rpc.AsyncCallback;
import pl.fovis.foxygwtcommons.FoxyClientSingletons;
import pl.fovis.foxygwtcommons.i18n.I18n;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author pmielanczuk
 */
public abstract class StandardAsyncCallbackForDialog<T> implements AsyncCallback<T> {

    //ww: nie jest final aby można było nadpisać
    public static String STANDARD_FAILURE_MESSAGE = I18n.wystapilBladPodczasWykonywaAkcji.get() /* I18N:  */ + ".";
    protected IParametrizedContinuation<String> onCompleteCont;
    protected String onSuccessMsg;
    protected String onFailureMsg;

    public StandardAsyncCallbackForDialog(IParametrizedContinuation<String> onCompleteCont,
            String onSuccessMsg, String onFailureMsg) {
        this.onCompleteCont = onCompleteCont;
        this.onSuccessMsg = onSuccessMsg;
        this.onFailureMsg = onFailureMsg;
    }

    public StandardAsyncCallbackForDialog(IParametrizedContinuation<String> onCompleteCont,
            String onSuccessMsg) {
        this(onCompleteCont, onSuccessMsg, STANDARD_FAILURE_MESSAGE);
    }

    public StandardAsyncCallbackForDialog(IParametrizedContinuation<String> onCompleteCont) {
        this(onCompleteCont, null, STANDARD_FAILURE_MESSAGE);
    }

    @Override
    public void onFailure(Throwable caught) {
        onCompleteCont.doIt(onFailureMsg);
    }

    protected abstract void refreshViewAfterSuccess(T result);

    protected String getOnSuccessMsg() {
        return onSuccessMsg;
    }

    protected void showSuccessMsg(T result) {
        String osm = getOnSuccessMsg();
        if (osm != null) {
            FoxyClientSingletons.showInfo(osm);
        }
    }

    protected boolean isNotificationModal() {
        return FoxyClientSingletons.isInfoModal();
    }

    protected boolean isRefreshViewAsync() {
        return true;
    }

    @Override
    public void onSuccess(T result) {
        onCompleteCont.doIt(null);

        boolean shouldDoRefreshFirst = isNotificationModal() && isRefreshViewAsync();

        if (!shouldDoRefreshFirst) {
            showSuccessMsg(result);
        }
        refreshViewAfterSuccess(result);
        if (shouldDoRefreshFirst) {
            showSuccessMsg(result);
        }
    }
}
