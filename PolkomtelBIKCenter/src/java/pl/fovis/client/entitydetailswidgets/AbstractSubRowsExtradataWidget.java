/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author tflorczak
 */
public abstract class AbstractSubRowsExtradataWidget extends EntityDetailsWidgetFlatBase {

    protected VerticalPanel mainVp;
    protected ISubRowsDisplayBroker broker;

    protected abstract String getOptHeaderCaption();

    protected abstract ISubRowsDisplayBroker getSubRowsDisplayBroker();

    protected void setupHeaderWidgetStyle(Widget header) {
        // no-op
    }

    @Override
    public void displayData() {
        mainVp.clear();

        if (broker == null) {
            return;
        }

        boolean hasContent = broker.getRowCount() > 0;
        if (hasContent) {

            String optHeaderCaption = getOptHeaderCaption();
            if (optHeaderCaption != null) {
                final Widget headerWidget = createHeader(optHeaderCaption);
                mainVp.add(headerWidget);
                setupHeaderWidgetStyle(headerWidget);
            }

            mainVp.add(new GenericSubRowsTableWidget(broker).buildWidget());
            mainVp.add(createPostSpacing());
        }
    }

    @Override
    public Widget buildWidgets() {
        mainVp = new VerticalPanel();
        mainVp.setWidth("100%");
        return mainVp;
    }

    @Override
    public boolean hasContent() {
        return broker != null && broker.getRowCount() > 0;
    }

    @Override
    protected void extractAndAssignExtraPropsFromFetchedData() {
        broker = getSubRowsDisplayBroker();
    }
}
