/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.oracle;

/**
 *
 * @author tflorczak
 */
public class OracleIndexBean {

    public String name;
    public String server;
    public String owner;
    public String tableName;
    public String columnName;
    public int columnPosition;
    public String descend;
    public boolean isUnique;
    public String type;
    public boolean isPrimaryKey;

    public OracleIndexBean(String name, String server, String owner, String tableName, String columnName, int columnPosition, String descend, boolean isUnique, String type, boolean isPrimaryKey) {
        this.name = name;
        this.server = server;
        this.owner = owner;
        this.tableName = tableName;
        this.columnName = columnName;
        this.columnPosition = columnPosition;
        this.descend = descend;
        this.isUnique = isUnique;
        this.type = type;
        this.isPrimaryKey = isPrimaryKey;
    }
}
