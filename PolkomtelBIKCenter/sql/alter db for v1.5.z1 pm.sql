﻿------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- 1. poprawka do sp_check_version
--    - zerwie połączenie lub rzuci szybciej błędem
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

exec sp_check_version '1.5.z1';
go

------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_check_version]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_check_version]
GO

create procedure [dbo].[sp_check_version] (@ver varchar(255)) as
begin	
	declare @version varchar(255)
	select @version = val from bik_app_prop where name = 'bik_ver';
	if(@version != @ver)
	begin
	    declare @txt nvarchar(4000) = N'!!! Wrong version number: ' + @ver + 
	      '. Current version is ' + @version + '. !!!'
	    
	    while 1 = 1 begin
	    print @txt
	    
--/*
		raiserror (@txt,
				20,
				-1) with LOG;
--*/

		raiserror(@txt,
				15,
				-1) with nowait;
				
		print 'you must manually stop execution of this script!!!'
		print 'proszę przerwać dalsze wykonanie tego skryptu ręcznie!!!'
		
		select @txt
		select 'you must manually stop execution of this script!!!'
		select 'proszę przerwać dalsze wykonanie tego skryptu ręcznie!!!'
		
		WAITFOR DELAY '00:00:10';
	    end
	end	
end
GO

------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

exec sp_update_version '1.5.z1', '1.5.z1.1';
go
