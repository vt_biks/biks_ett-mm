package pl.trzy0.foxy.commons.tags;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import simplelib.BaseUtils;
import simplelib.BaseUtils.IdentityProjector;
import simplelib.Pair;

/**
 *
 * @author wezyr
 */
public class TagForestBuilder<T> {

    //private static ILameLogger logger = LameLoggerFactory.getLogger(TagForestBuilder.class);
    private ITagLibrary<T> library;

    public TagForestBuilder(ITagLibrary<T> library) {
        this.library = library;
    }

    protected void postProcessing(List<T> res, T tag, List<T> subTags) {
    }

    private Pair<List<T>, TagData> innerBuildFoxyTagForest(T parentTag, Iterator<TagData> tagStream) {
        List<T> tags = null;

        while (true) {
            TagData tagData = tagStream.hasNext() ? tagStream.next() : null;
            if (tagData == null || tagData.getLevelDelta() < 0) {
                return new Pair<List<T>, TagData>(tags, tagData);
            }

            if (tags == null) {
                tags = new ArrayList<T>();
            }

            T tag = library.createTag(parentTag, tagData);

            List<T> subTags;
            if (tagData.getLevelDelta() > 0) {
                Pair<List<T>, TagData> inner = innerBuildFoxyTagForest(tag, tagStream);
                if (inner.v2 == null) {
                    throw new FoxyTagsRuntimeException("no closing tag for " +
                            tagData.getName() + ", end of file reached");
                }

                if (!BaseUtils.safeEquals(inner.v2.getName(), tagData.getName())) {
                    throw new FoxyTagsRuntimeException("unmatched opening tag: " +
                            tagData.getName() + ", found closing tag: " + inner.v2.getName() +
                            ", fileName=" + tagData.sourceFileName);
                }

                subTags = inner.v1;
            } else {
                subTags = null;
            }

            library.setSubTags(tag, subTags);
            postProcessing(tags, tag, subTags);

            tags.add(tag);
        }
    }

    public List<T> buildFoxyTagForest(Iterator<TagData> tagStream) {
        List<TagData> tags = BaseUtils.projectToList(tagStream, new IdentityProjector<TagData>());

        int idx = tags.size() - 1;

        while (idx >= 0) {
            TagData tag = tags.get(idx);
            if (tag.isTextTag() && BaseUtils.isStrEmptyOrWhiteSpace(tag.getName())) {
                tags.remove(idx);
            } else {
                break;
            }

            idx--;
        }

        Pair<List<T>, TagData> inner = innerBuildFoxyTagForest(null, tags.iterator());

        if (inner.v2 != null) {
            throw new FoxyTagsRuntimeException("found closing tag: " +
                    inner.v2.getName() + " without matching opening tag");
        }

        List<T> res = inner.v1 != null ? inner.v1 : new ArrayList<T>();

        return res;

        //List<T> res = new ArrayList<T>();
        //
        ////IFoxyTag previousTag = null;
        //
        //while (tagStream.hasNext()) {
        //    TagData tagData = tagStream.next();
        //    H aux = library.initializeNewTag(tagData);
        //    List<T> subTags;
        //    if (tagData.getLevelDelta() > 0) {
        //        subTags = buildFoxyTagForest(new SubStreamParser(tagData, tagStream, library));
        //    } else {
        //        subTags = null;
        //    }
        //
        //    T tag = library.createTag(tagData, subTags, aux);
        //
        //    postProcessing(res, tag, subTags);
        //    library.finalizeNewTag(tagData, aux);
        //
        //    res.add(tag);
        //    //previousTag = tag;
        //}
        //
        ////previousTag = null;
        //return res;
    }

    protected ITagLibrary<T> getLibrary() {
        return library;
    }
}
