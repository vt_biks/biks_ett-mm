exec sp_check_version '1.0.37';

-- Usuniecie wpisów z metapedii odnoszących się do folderów (co jest błędem) - jest tylko 1 taki przypadek
DELETE FROM bik_metapedia_linked where metapedia_id in (select id from bik_metapedia where body in (select body from bik_metapedia a join bik_node b on a.node_id=b.id where b.node_kind_id<>(select id from bik_node_kind where code='Tezaurus')))  
DELETE FROM bik_metapedia where body in (select body from bik_metapedia a join bik_node b on a.node_id=b.id where b.node_kind_id<>(select id from bik_node_kind where code='Tezaurus'))

-------------------------- INFO -----------------------
-------------------------------------------------------
-------------------------------------------------------
-------------------------------------------------------
-- PONIEWAZ TO SA TYLKO 2 LINIJKI TO MOZNA JE SKOPIOWAC DO WLASNEGO (NOWEGO) SKRYPTU, A MOJ USUNAC
-- DLACZEGO? NIE MA SENSU TWORZYC BARDZO DUZO MALYCH (I PRAWIE NIC NIE WNOSZACYCH) SKRYPTOW
-------------------------------------------------------
-------------------------------------------------------
-------------------------------------------------------
-------------------------------------------------------
update bik_node
set
  is_deleted = 1
from
  bik_node n inner join bik_user u on n.id = u.node_id
where
  u.activ = 0

exec sp_update_version '1.0.37', '1.0.38';
go
