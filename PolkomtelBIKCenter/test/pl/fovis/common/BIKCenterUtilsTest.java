/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import commonlib.LameUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author wezyr
 */
public class BIKCenterUtilsTest extends BIKCenterUtilsTestBase {

    static {
        LameUtils.initFactories();
    }

    public BIKCenterUtilsTest() {
        // tylko po to, aby nie wcięło static importa
        if (false) {
            assertEquals(0, 0);
        }
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of extractNodeIdsFromHtml method, of class BIKCenterUtils.
     */
    @Test
    public void testExtractNodeIdsFromHtml() {
        innerTestExtractNodeIdsFromHtml();
    }

    @Test
    public void testRemoveHrefsFromHtml() {
        innerTestRemoveHrefsFromHtml();
    }

    @Test
    public void testComparator() {
        innerTestCompareValues();
    }

    private static void testForNormalizeTextForFTS(String txt) {
        System.out.println("normalizeTextForFTS(\"" + txt + "\", true) --> [" + BIKCenterUtils.normalizeTextForFTS(txt, true) + "]");
        System.out.println("normalizeTextForFTSAndOptLike(\"" + txt + "\") --> "
                + BIKCenterUtils.normalizeTextForFTSAndOptLike(txt));
    }

    @Test
    public void testNormalizeTextForFTSAndOptLike() {
        System.out.println("testNormalizeTextForFTSAndOptLike");
        testForNormalizeTextForFTS("*");
        testForNormalizeTextForFTS("\"korporacyjne foldery\"");
        testForNormalizeTextForFTS(" * ");
        testForNormalizeTextForFTS("   ");
        testForNormalizeTextForFTS("wtf?*");
        testForNormalizeTextForFTS("wtf*");
        testForNormalizeTextForFTS("  wtf*  ");
        testForNormalizeTextForFTS("  wtf *");
        testForNormalizeTextForFTS("  wtf* abc* xyz * ju*mi* \"too\" 'late'* ");
        testForNormalizeTextForFTS("id_konto");
        testForNormalizeTextForFTS("id_kont*");
        testForNormalizeTextForFTS("_id_*");
        testForNormalizeTextForFTS("id_konto select do.bm");
        testForNormalizeTextForFTS("SELECT weekly.typ_serwisu, weekly.tydzien, weekly.data_startu_tygodnia, weekly.do_mb, weekly.liczba_uzytkownikow");
    }
}
