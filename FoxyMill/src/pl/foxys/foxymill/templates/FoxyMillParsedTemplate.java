/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill.templates;

import java.util.List;
import java.util.Map;
import pl.foxys.foxymill.FoxyMillUtils;
import pl.foxys.foxymill.IRenderable;
import pl.foxys.foxymill.renderctx.IOutputDevice;
import pl.foxys.foxymill.renderctx.IRenderCtx;
import pl.foxys.foxymill.renderctx.RenderCtx;
import pl.foxys.foxymill.renderctx.StringOutputDevice;
import pl.trzy0.foxy.commons.templates.IParsedTemplate;

/**
 *
 * @author wezyr
 */
class FoxyMillParsedTemplate implements IParsedTemplate {

    private IRenderable renderable;

    public FoxyMillParsedTemplate(List<? extends IRenderable> tags) {
        this(FoxyMillUtils.wrapAsRenderable(tags, null));
    }

    public FoxyMillParsedTemplate(IRenderable renderable) {
        this.renderable = renderable;
    }

    public void render(StringBuilder sb, Map<String, Object> vars, Object model) {
        IOutputDevice outDev = new StringOutputDevice(sb);
        IRenderCtx ctx = RenderCtx.makeRenderCtx(model, vars, outDev);
        renderable.render(ctx);
    }
}
