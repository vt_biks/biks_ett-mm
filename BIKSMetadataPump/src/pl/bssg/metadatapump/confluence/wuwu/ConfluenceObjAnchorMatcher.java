/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.confluence.wuwu;

import java.util.LinkedHashMap;
import java.util.Map;
import simplelib.BaseUtils;
import simplelib.IRegExpMatcher;
import simplelib.IStringMatcher;

/**
 *
 * @author pmielanczuk
 */
public class ConfluenceObjAnchorMatcher implements IStringMatcher {

    protected IRegExpMatcher m;
    protected String href;
//    protected long resId;
//    protected String resType;
    protected LinkedHashMap<String, String> attrMap;

    public ConfluenceObjAnchorMatcher(String s) {
        this.m = DeferredConfluenceObjAnchorReplacer.ANCHOR_PATT.matcher(s);
    }

    @Override
    public boolean find() {
        return innerFind(-1);
    }

    protected static String dropFirstAndLastChar(String s) {
        if (s == null || s.length() < 2) {
            return "";
        }
        return s.substring(1, s.length() - 1);
    }

    protected static Long extractOptObjId(Map<String, String> map) {
        return BaseUtils.tryParseLong(dropFirstAndLastChar(map.get("data-linked-resource-id")), null);
    }

    protected boolean innerFind(int optStartPos) {
        while (true) {
            boolean found;
            if (optStartPos < 0) {
                found = m.find();
            } else {
                found = m.find(optStartPos);
            }
            if (!found) {
                return false;
            }
            optStartPos = -1;
            attrMap = DeferredConfluenceObjAnchorReplacer.extractAttrNamesAndVals(
                    dropFirstAndLastChar(m.group(0)), false, true);
            href = attrMap.get("href");
//            Long optResId = extractOptObjId(attrMap);
//            if (BaseUtils.isStrEmptyOrWhiteSpace(href) || optResId == null) {
//                continue;
//            }
//            resId = optResId;
            if (BaseUtils.isStrEmptyOrWhiteSpace(href)) {
                continue;
            }
            return true;
        }
    }

    @Override
    public boolean find(int startPos) {
        return innerFind(startPos);
    }

    @Override
    public int start() {
        return m.start();
    }

    @Override
    public int end() {
        return m.end();
    }

    public LinkedHashMap<String, String> getAttrMap() {
        return attrMap;
    }
}
