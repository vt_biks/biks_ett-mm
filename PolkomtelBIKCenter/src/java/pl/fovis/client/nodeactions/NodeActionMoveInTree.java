/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import pl.fovis.common.MoveNodeInTreeMode;

/**
 *
 * @author pmielanczuk
 */
public class NodeActionMoveInTree extends NodeActionMoveInTreeBase {

    protected String caption;
    protected MoveNodeInTreeMode moveMode;

    public NodeActionMoveInTree(String caption, MoveNodeInTreeMode moveMode) {
        this.caption = caption;
        this.moveMode = moveMode;
    }

    @Override
    protected MoveNodeInTreeMode getMoveMode() {
        return moveMode;
    }

    public String getCaption() {
        return caption;
    }
}
