/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.DoubleClickEvent;
import com.google.gwt.event.dom.client.DoubleClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import pl.fovis.foxygwtcommons.i18n.I18n;

/**
 *
 * @author wezyr
 */
public abstract class BasePickerDialog<T> extends BaseActionOrCancelDialog {

//    protected ListGrid grid;
    protected ListBox listBox;
//    protected IFoxyListGrid<Integer> grid;
    protected Map<Map<String, Object>, T> dataItemMap = new LinkedHashMap<Map<String, Object>, T>();
    protected IParametrizedContinuation<T> cont;
    protected boolean callContForCancelToo;
    protected List<T> items;
    protected Integer initiallySelectedId;
    protected TextBox textBox;
    protected String[] gridFields;
    protected List<Map<String, Object>> gridRecords;
    protected List<String> displayedFieldNames;
    protected String firstDisplayedFieldName;
    private Label lblSearch;
    private Label lblListBox;
//    protected CheckBox inheritToDescendantsCb;

//    public boolean getInheritToDescendantsCb(){
//        return inheritToDescendantsCb.getValue();
//    }
    protected void displayItems(String searchTxt) {
        String searchTxtLower = searchTxt == null ? null : searchTxt.toLowerCase();

        listBox.clear();

        gridRecords = new ArrayList<Map<String, Object>>();//new ListGridRecord[items.size()];

        int i = 0;
        for (Map<String, Object> record : dataItemMap.keySet()) {
            boolean recordMatches = BaseUtils.isStrEmpty(searchTxt);

            if (!recordMatches) {
                for (String fieldName : displayedFieldNames) {
                    String fieldVal = BaseUtils.safeToStringDef(record.get(fieldName), "");
                    String kLower = fieldVal.toLowerCase();

                    if (kLower.indexOf(searchTxtLower) != -1) {
                        recordMatches = true;
                        break;
                    }
                }
            }

//            Window.alert("row #" + i + ": row=" + record + ", matches=" + recordMatches
//                    + ", search: " + searchTxtLower
//                    + ", gridFields=" + BaseUtils.mergeWithSep(gridFields, ","));

            if (recordMatches) {
                gridRecords.add(record);
                String val = BaseUtils.safeToString(record.get(firstDisplayedFieldName));
//                Window.alert("firstDisplayedFieldName: " + firstDisplayedFieldName
//                        + ", val=" + val + ", record=" + record);
                listBox.addItem(val);
            }

            i++;
        }

//        grid.setData(gridRecords);
//        if (!gridRecords.isEmpty()) {
//            grid.selectFirstRecord();
//        }
    }

    protected Map<String, Object> getSelectedRecord() {
        int idx = listBox.getSelectedIndex();
        return idx == -1 ? null : gridRecords.get(idx);
    }

    protected abstract String getListBoxLabelCaption();

    @Override
    protected void buildMainWidgets() {
        Grid grid = new Grid(2, 2);
        grid.setCellSpacing(4);

        lblSearch = new Label(I18n.szukaj.get() /* I18N:  */+ ":");
        lblListBox = new Label(getListBoxLabelCaption());
        actionBtn.setEnabled(false);
        textBox = new TextBox();
        textBox.setWidth("500px");
        textBox.addKeyUpHandler(new KeyUpHandler() {

            public void onKeyUp(KeyUpEvent event) {
                displayItems(textBox.getText());
                actionBtn.setEnabled(false);
            }
        });

        // main.add(textBox);

//        grid = TreeAndListCreator.createListGrid();
////        grid = new ListGrid();
//        grid.setWidth(getGridWidth() + "px");
//        grid.setHeight(getGridHeight() + "px");
//        grid.setShowAllRecords(true);

        listBox = new ListBox();
        listBox.setVisibleItemCount(7);
        listBox.setWidth("500px");
        gridFields = createFields();

        grid.setWidget(0, 0, lblSearch);
        grid.setWidget(0, 1, textBox);
        grid.setWidget(1, 0, lblListBox);
        grid.setWidget(1, 1, listBox);

        lblSearch.setStyleName("BPDlblSearch");
        lblListBox.setStyleName("BPDlblListBox");
        textBox.setStyleName("BPDtextBox");
        listBox.setStyleName("BPDlistBox");

        displayedFieldNames = new ArrayList<String>();
        for (String field : gridFields) {
            String fieldName = BaseUtils.splitString(field, "=").v1;
            if (!BaseUtils.safeEquals(getIdFieldName(), fieldName)) {
                if (firstDisplayedFieldName == null) {
                    firstDisplayedFieldName = fieldName;
                }
                displayedFieldNames.add(fieldName);
            }
        }

//        grid.setFields(gridFields);
//        grid.setIdField(getIdFieldName());
//        grid.defineFieldHidden(getIdFieldName(), true);
//
//        grid.setEmptyMessage("");
        for (int i = 0; i < items.size(); i++) {
            T dataItem = items.get(i);

            Map<String, Object> record = new HashMap<String, Object>();
            setupListGridRecord(dataItem, record);
            dataItemMap.put(record, dataItem);
        }

        displayItems(null);

//        grid.addDoubleClickHandler(new IParametrizedContinuation<Map<String, Object>>() {
//
//            public void doIt(Map<String, Object> param) {
//                actionDoubleClicked(param);
////                doAction();
//            }
//        });

        listBox.addDoubleClickHandler(new DoubleClickHandler() {

            public void onDoubleClick(DoubleClickEvent event) {
                if (getSelectedRecord() != null) {
                    actionDoubleClicked(getSelectedRecord());
                }
            }
        });
        listBox.addClickHandler(new ClickHandler() {

            public void onClick(ClickEvent event) {
                if (getSelectedRecord() != null) {
                    actionBtn.setEnabled(true);
                }
            }
        });

//        grid.addCellDoubleClickHandler(new CellDoubleClickHandler() {
//
//            public void onCellDoubleClick(CellDoubleClickEvent event) {
//                actionClicked();
//            }
//        });

//        if (items.size() > 0) {
//
//            //            Record r = initiallySelectedId!= null ? grid.getDataAsRecordList().find("id", initiallySelectedId) : null;
//            if (initiallySelectedId != null) {
////            if (r != null) {
//                //String r = Integer.toString(initiallySelectedId);
//                grid.selectRecord(//r
//                        initiallySelectedId);//tu było selectSingleRecord
//            } else {
//                grid.selectFirstRecord();//tu było selectSingleRecord
//            }
//        }
//
//        main.add(grid.buildWidget());
//        main.add(listBox);
//        HorizontalPanel hp = new HorizontalPanel();
//        hp.add(inheritToDescendantsCb = new CheckBox());
//        hp.add(new Label("Pokazuj w obiektach podrzędnych"));
        main.add(grid);
//        main.add(hp);
    }

    @Override
    protected void doAction() {
//        T dataItem = dataItemMap.get(grid.getSelectedRecord());
        T dataItem = dataItemMap.get(getSelectedRecord());
        cont.doIt(dataItem);
    }

    @Override
    protected void doCancel() {
        if (callContForCancelToo) {
            cont.doIt(null);
        }
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.wybierz.get() /* I18N:  */;
    }

    protected abstract void setupListGridRecord(T item, Map<String, Object> record);

//    protected abstract ListGridField[] createFields();
    protected abstract String[] createFields();

    protected abstract String getIdFieldName();

//    protected ListGridField[] createFields(String... namesAndCaptions) {
//        ListGridField[] res = new ListGridField[namesAndCaptions.length];
//
//        int i = 0;
//        for (String nameAndCaption : namesAndCaptions) {
//            Pair<String, String> p = BaseUtils.splitString(nameAndCaption, "=");
//            ListGridField field = new ListGridField(p.v1, BaseUtils.nullToDef(p.v2, p.v1));
//            res[i++] = field;
//        }
//
//        return res;
//    }
    protected String[] createFields(String... namesAndCaptions) {
        return namesAndCaptions;
//        String[] res = new String[namesAndCaptions.length];
//
//        int i = 0;
//        for (String nameAndCaption : namesAndCaptions) {
//            String field = nameAndCaption;
//            res[i++] = field;
//        }
//
//        return res;
    }

    protected int getGridWidth() {
        return 200;
    }

    protected int getGridHeight() {
        return 150;
    }

    protected abstract String getDialogCaption();

    protected void actionDoubleClicked(Map<String, Object> data) {
        if (doActionBeforeClose()) {
            //dialog.hide();
            hideDialog();
            T dataItem = dataItemMap.get(data);
            cont.doIt(dataItem);
        }
    }

    public void pickOneItem(List<T> items, Integer initiallySelectedId,
            final IParametrizedContinuation<T> cont,
            final boolean callContForCancelToo) {

        this.cont = cont;
        this.callContForCancelToo = callContForCancelToo;
        this.items = items;
        this.initiallySelectedId = initiallySelectedId;

        buildAndShowDialog();
    }
//    public static <T, D extends BasePickerDialog<T>> void pickIt(Class<D> dc, List<T> items, Integer initiallySelectedId,
//            final IParametrizedContinuation<T> cont,
//            final boolean callContForCancelToo) {
//        D dialog;
//        try {
//            dialog = dc.newInstance();
//        } catch (Exception ex) {
//            throw new RuntimeException("error creating dialog object", ex);
//        }
//        dialog.pickOneItem(items, initiallySelectedId, cont, callContForCancelToo);
//    }
}
