/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import commonlib.LameUtils;
import org.junit.*;
import pl.trzy0.foxy.commons.FoxyCommonUtils;
/**
 *
 * @author pmielanczuk
 */
public class GWTUtilsTest {
    
    public GWTUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        LameUtils.initFactories();
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of clearFormattingForRichTextArea method, of class GWTUtils.
     */
    @Test
    public void testClearFormatting() {
        System.out.println("clear formatting: " + FoxyCommonUtils.clearFormattingForRichTextArea("<h2><span name='xxx'>ala <b>ma ko</b>ta</span></h2>"));
    }
}
