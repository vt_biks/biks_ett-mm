exec sp_check_version '1.4.y1.15';
go

----------------------------------------------------
----------------------------------------------------
--     uzupelnienie drzewa o (byc moze) brakujacy weze�
--     korzen drzewa slownikow LISY
----------------------------------------------------
----------------------------------------------------

declare @treeId int
select @treeId = id from bik_tree where code ='DictionaryDWH'
declare @DWHDictionaries int
select @DWHDictionaries = id from bik_node_kind where code='DWHDictionaries'

if not exists (select top 1 id from bik_node where tree_id = @treeId and obj_id = 'DWHDictionaryRoot' )
insert into bik_node (parent_node_id,node_kind_id,name,tree_id,obj_id) values (null, @DWHDictionaries,'S�owniki DWH',@treeId,'DWHDictionaryRoot');
go

exec sp_update_version '1.4.y1.15', '1.4.y1.16';
go
