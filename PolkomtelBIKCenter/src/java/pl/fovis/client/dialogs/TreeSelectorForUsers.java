/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.TreeNodeBean;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author beata
 */
public class TreeSelectorForUsers extends TreeSelectorDialog/*TreeSelectorDialogBase*/ {

//    protected int thisNodeId;
//    protected String treeKind;
    protected boolean isInheritToDescendantsCheckBoxVisible;
    protected Integer optUpSelectedNodeId;

    @Override
    protected boolean isInheritToDescendantsCheckBoxVisible() {
        return isInheritToDescendantsCheckBoxVisible;
    }

    @Override
    protected void callServiceToGetData(AsyncCallback<List<TreeNodeBean>> callback) {
        BIKClientSingletons.getService().getBikUserRootTreeNodes(optUpSelectedNodeId, callback);
    }

    public void buildAndShowDialog(int thisNodeId, //String treeKind,
            IParametrizedContinuation<TreeNodeBean> saveCont,
            IJoinedObjSelectionManager<Integer> alreadyJoinedManager, boolean isInheritToDescendantsCheckBoxVisible, Integer optUpSelectedNodeId) {
//        this.thisNodeId = thisNodeId;
//        this.treeKind = treeKind;
        this.isInheritToDescendantsCheckBoxVisible = isInheritToDescendantsCheckBoxVisible;
        this.optUpSelectedNodeId = optUpSelectedNodeId;
        super.buildAndShowDialog(thisNodeId, saveCont, alreadyJoinedManager);
    }

    @Override
    protected void doAfterDoubleClickOnTree(Map<String, Object> param) {
        //no-op
    }

    @Override
    protected boolean isActionButtonEnabled() {
        return !(selectedNode != null && (selectedNode.nodeKindCode.equals(BIKGWTConstants.NODE_KIND_ALL_USERS_FOLDER)
                || selectedNode.nodeKindCode.equals(BIKGWTConstants.NODE_KIND_USERS_ROLE)));
        //ww:mltx powyższy warunek jest dziwny, odpowiada chyba poniższemu, czyli akcja jest włączona
        // gdy brak wybranego węzła?
//        return selectedNode == null ||
//                !selectedNode.nodeKindCode.equals(BIKGWTConstants.NODE_KIND_ALL_USERS_FOLDER)
//                && !selectedNode.nodeKindCode.equals(BIKGWTConstants.NODE_KIND_USERS_ROLE);
    }

    @Override
    protected boolean mustHideLinkedNodes() {
        return false;
    }

    @Override
    protected Collection<Integer> getTreeIdsForFiltering() {
        return BIKClientSingletons.getProperTreeIdsWithTreeKind(BIKGWTConstants.TREE_KIND_USER_ROLES);
        //return " tree_id in (select id from bik_tree where tree_kind in ( '" + BIKGWTConstants.TREE_CODE_USERS + "'))";
    }
}
