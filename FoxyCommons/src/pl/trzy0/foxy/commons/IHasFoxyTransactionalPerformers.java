/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons;

import java.util.Collection;

/**
 *
 * @author pmielanczuk
 */
public interface IHasFoxyTransactionalPerformers {

    public Collection<? extends IFoxyTransactionalPerformer> getTransactionalPerformers();
}
