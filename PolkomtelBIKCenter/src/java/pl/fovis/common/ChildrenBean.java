/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author tflorczak
 */
public class ChildrenBean implements Serializable {

    public String name;
    public String nodePath;
    public String description;
    public String icon;
    public String treeCode;
    public int nodeId;
    public int treeId;
    public String branchIds;
    public List<AttributeBean> attributes;
    public Integer nodeKindId;
}
