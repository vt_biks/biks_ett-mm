/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.BikNodeTreeOptMode;
import pl.fovis.common.TreeNodeBean;
import simplelib.BaseUtils;

/**
 *
 * @author tflorczak
 */
public class TreeSelectorForNotConnectedUsers extends TreeSelectorForUsers {

    @Override
    protected void callServiceToGetData(AsyncCallback<List<TreeNodeBean>> callback) {
        BIKClientSingletons.getService().getNotConnectedUsers(callback);
    }

    @Override
    protected BikNodeTreeOptMode getTreeOptExtraMode() {
        return BikNodeTreeOptMode.NotConnectedUsersOnly;
    }

    @Override
    protected boolean calcHasNoChildren() {
        return false;
    }

    @Override
    protected void optFixBeanPropsAfterLoad(TreeNodeBean tnb) {
        //ww: Tomek potwierdza, że jest PRO ;-)
        super.optFixBeanPropsAfterLoad(tnb);
        tnb.hasNoChildren = BaseUtils.safeEquals(tnb.nodeKindCode, BIKGWTConstants.NODE_KIND_USER);
    }
}
