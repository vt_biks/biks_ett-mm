/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.i18nsupport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import simplelib.BaseUtils;
import simplelib.IRegExpMatcher;
import simplelib.IRegExpPattern;

/**
 *
 * @author pmielanczuk
 */
public class I18nParametrizedMessages {
    //    static {
//        LameUtils.initFactories();
//    }

    protected static class MsgSegment {

        int startOrIdx;
        int endOrZero;

        public MsgSegment(int startOrIdx, int endOrZero) {
            this.startOrIdx = startOrIdx;
            this.endOrZero = endOrZero;
        }
    }
    protected static final IRegExpPattern msgSegPatt = BaseUtils.getRegExpMatcherFactory().compile("[{]([_]?[0-9]+)[}]", false, false);
    protected static final Map<String, List<MsgSegment>> cache = new HashMap<String, List<MsgSegment>>();

    protected static List<MsgSegment> makeSegments(String format) {
        List<MsgSegment> res = cache.get(format);
        if (res != null) {
            return res;
        }

        res = new ArrayList<MsgSegment>();
        IRegExpMatcher m = msgSegPatt.matcher(format);
        int pos = 0;
        while (m.find()) {
            String grp = m.group(1);
            if (grp.charAt(0) == '_') {
                res.add(new MsgSegment(pos, m.start() + 1));
                res.add(new MsgSegment(m.start() + 2, m.end()));
            } else {
                res.add(new MsgSegment(pos, m.start()));
                res.add(new MsgSegment(Integer.valueOf(grp), 0));
            }
            pos = m.end();
        }
        int len = format.length();
        if (pos < len) {
            res.add(new MsgSegment(pos, len));
        }

        cache.put(format, res);

        return res;
    }

    public static String format(String format, Object... args) {
        List<MsgSegment> segments = makeSegments(format);
        StringBuilder sb = new StringBuilder();
        for (MsgSegment seg : segments) {
            if (seg.endOrZero == 0) {
                sb.append(args[seg.startOrIdx]);
            } else {
                sb.append(format.substring(seg.startOrIdx, seg.endOrZero));
            }
        }
        return sb.toString();
    }

    public static String format(I18nMessage msg, Object... args) {
        return format(msg.get(), args);
    }
}
