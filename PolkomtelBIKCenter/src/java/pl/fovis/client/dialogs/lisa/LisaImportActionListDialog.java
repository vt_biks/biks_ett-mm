/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.lisa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.LisaConstants;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.common.lisa.LisaImportActionBean;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.fovis.foxygwtcommons.grid.BeanGrid;
import pl.fovis.foxygwtcommons.grid.BeanGridPager;
import pl.fovis.foxygwtcommons.grid.IBeanDisplay;
import pl.fovis.foxygwtcommons.grid.IBeanProvider;
import simplelib.BaseUtils;

/**
 *
 * @author ctran
 */
public class LisaImportActionListDialog extends BaseActionOrCancelDialog {

    protected IBeanProvider<LisaImportActionBean> beanProvider;
    protected List<LisaDictColumnBean> pkColumnsList;

    @Override
    protected String getDialogCaption() {
        return I18n.listaAkcji.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return null;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void buildMainWidgets() {
        final Map<String, Integer> colName2IdMap = new HashMap<String, Integer>();
        List<String> colNames = new ArrayList<String>();
        colNames.add(I18n.nrLinii.get());
        int id = 0;
        for (LisaDictColumnBean column : pkColumnsList) {
            colNames.add(column.nameInDb);
            colName2IdMap.put(column.nameInDb, id++);
        }
        BeanGrid actionGrid = new BeanGrid(beanProvider, new IBeanDisplay<LisaImportActionBean>() {

            @Override
            public Object getHeader(String colName) {
                return colName;
            }

            @Override
            public Object getValue(LisaImportActionBean bean, String colName) {
                if (I18n.nrLinii.get().equals(colName)) {
                    return (bean.lineNr >= 0 ? bean.lineNr + 2 : "x"); //linie sa numerowane od 1 plus naglowki -> +2
                }
                if (bean.pkValue == null) {
                    return null;
                }
                int id = colName2IdMap.get(colName);
                List<String> pkValueList = BaseUtils.splitBySep(bean.pkValue, LisaConstants.PK_VALUE_SEPERATOR);
                return pkValueList.get(id);
            }
        }, LisaConstants.GRID_PAGE_SIZE, colNames.toArray(new String[colNames.size()]));

        main.add(actionGrid);
        BeanGridPager<LisaDictColumnBean> bgp = new BeanGridPager<LisaDictColumnBean>(actionGrid);
        bgp.setUknownPageCountMode(true);
        main.add(bgp);
    }

    @Override
    protected void doAction() {
    }

    public void buildAndShowDialog(IBeanProvider<LisaImportActionBean> beanProvider, List<LisaDictColumnBean> pkColumnsList) {
        this.beanProvider = beanProvider;
        this.pkColumnsList = pkColumnsList;
        super.buildAndShowDialog();
    }

}
