/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

/**
 *
 * @author pmielanczuk
 */
public enum MoveNodeInTreeMode {
    MakeFirst, MoveUp, MoveDown, MakeLast;
}
