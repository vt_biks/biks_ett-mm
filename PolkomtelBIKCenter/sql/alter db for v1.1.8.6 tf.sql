﻿exec sp_check_version '1.1.8.6';
go

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_delete_bik_joined_objs_by_kinds_fast]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_delete_bik_joined_objs_by_kinds_fast
go

create procedure sp_delete_bik_joined_objs_by_kinds_fast @kinds_one_str varchar(max), @kinds_two_str varchar(max) as
begin
  set nocount on

  declare @timestamp_start_total datetime = current_timestamp
  declare @diags_level int = 1 -- wartość 0 oznacza brak logowania, 1 i więcej - jest logowanie
  declare @rc int
  declare @timestamp_start datetime 
  declare @timestamp_end datetime
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: start' +
      ', @kinds_one_str = ' + @kinds_one_str + 
      ', @kinds_two_str = ' + @kinds_two_str
  end

  -- declare @kinds_one varchar(max) = 'Universe', @kinds_two varchar(max) = 'TeradataSchema'
  declare @codes_one table (code varchar(255) not null primary key)
  declare @codes_two table (code varchar(255) not null primary key)

  insert into @codes_one (code)
  select distinct str
  from dbo.fn_split_by_sep(@kinds_one_str, ',', 7)

  insert into @codes_two (code)
  select distinct str
  from dbo.fn_split_by_sep(@kinds_two_str, ',', 7)

  declare @kind_one_id table (id int not null primary key)
  declare @kind_two_id table (id int not null primary key)

  insert into @kind_one_id (id)
  select distinct k.id
  from @codes_one x inner join bik_node_kind k on x.code = k.code
  where substring(x.code, 1, 1) <> '@'
  
  insert into @kind_two_id (id)
  select distinct k.id
  from @codes_two x inner join bik_node_kind k on x.code = k.code
  where substring(x.code, 1, 1) <> '@'

  declare @tree_one_id table (id int not null primary key)
  declare @tree_two_id table (id int not null primary key)

  delete from @codes_one where substring(code, 1, 1) <> '@'
  delete from @codes_two where substring(code, 1, 1) <> '@'

  update @codes_one set code = substring(code, 2, len(code))
  update @codes_two set code = substring(code, 2, len(code))

  insert into @tree_one_id (id)
  select distinct t.id
  from @codes_one x inner join bik_tree t on x.code = t.code

  insert into @tree_two_id (id)
  select distinct t.id
  from @codes_two x inner join bik_tree t on x.code = t.code

  declare @tree_id_to_kind_id table (tree_id int not null, node_kind_id int not null)
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: before insert into @tree_id_to_kind_id'
  end

  set @timestamp_start = current_timestamp
  
  insert into @tree_id_to_kind_id (tree_id, node_kind_id)
  select distinct tree_id, node_kind_id
  from bik_node where is_deleted = 0 and linked_node_id is null

  set @rc = @@rowcount
  set @timestamp_end = current_timestamp
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: after insert into @tree_id_to_kind_id, @rc='
      + cast(@rc as varchar(20)) + ', time taken: ' + cast(datediff(ms, @timestamp_start, @timestamp_end) / 1000.0 as varchar(20)) + ' s'
  end

  insert into @kind_one_id (id)
  select t2k.node_kind_id
  from @tree_one_id t inner join @tree_id_to_kind_id t2k on t.id = t2k.tree_id
  left join @kind_one_id k on k.id = t2k.node_kind_id
  where k.id is null

  insert into @kind_two_id (id)
  select t2k.node_kind_id
  from @tree_two_id t inner join @tree_id_to_kind_id t2k on t.id = t2k.tree_id
  left join @kind_two_id k on k.id = t2k.node_kind_id
  where k.id is null

  declare @joined_ids_to_del table (id int not null primary key)

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: before insert into @joined_ids_to_del'
  end

  set @timestamp_start = current_timestamp

  insert into @joined_ids_to_del (id)
  select jo.id 
  from bik_joined_objs jo inner join bik_node n_one on jo.src_node_id = n_one.id
  inner join bik_node n_two on jo.dst_node_id = n_two.id
  inner join @kind_one_id k1 on n_one.node_kind_id = k1.id
  inner join @kind_two_id k2 on n_two.node_kind_id = k2.id
  where jo.type = 1
  union -- bez all, bo mogą być duplikaty, a chcemy je właśnie wyeliminować
  select jo.id 
  from bik_joined_objs jo inner join bik_node n_one on jo.dst_node_id = n_one.id
  inner join bik_node n_two on jo.src_node_id = n_two.id
  inner join @kind_one_id k1 on n_one.node_kind_id = k1.id
  inner join @kind_two_id k2 on n_two.node_kind_id = k2.id
  where jo.type = 1

  set @rc = @@rowcount
  set @timestamp_end = current_timestamp
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: after insert into @joined_ids_to_del, @rc='
      + cast(@rc as varchar(20)) + ', time taken: ' + cast(datediff(ms, @timestamp_start, @timestamp_end) / 1000.0 as varchar(20)) + ' s'
  end
  
  --select count(*) from @joined_ids_to_del

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: before final delete'
  end

  set @timestamp_start = current_timestamp
  
  delete from bik_joined_objs where id in (select id from @joined_ids_to_del)
  
  set @rc = @@rowcount
  set @timestamp_end = current_timestamp
  declare @timestamp_end_total datetime = current_timestamp
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: after final delete, @rc='
      + cast(@rc as varchar(20)) + ', time taken: ' + cast(datediff(ms, @timestamp_start, @timestamp_end) / 1000.0 as varchar(20)) + ' s'
      + ', TOTAL TIME TAKEN: ' + cast(datediff(ms, @timestamp_start_total, @timestamp_end_total) / 1000.0 as varchar(20)) + ' s'
  end
end
go

/*

exec sp_delete_bik_joined_objs_by_kinds_fast 'TeradataView,TeradataTable,TeradataSchema,Webi,ReportQuery', 'Universe,DataConnection,Measure,Dimension,Detail,Filter,Webi,ReportQuery,UniverseAliasTable,UniverseDerivedTable,UniverseTable'


2012-08-24 16:54:09.845: sp_delete_bik_joined_objs_by_kinds_fast: start, @kinds_one_str = TeradataView,TeradataTable,TeradataSchema,Webi,ReportQuery, @kinds_two_str = Universe,DataConnection,Measure,Dimension,Detail,Filter,Webi,ReportQuery,UniverseAliasTable,UniverseDerivedTable,UniverseTable
2012-08-24 16:54:09.849: sp_delete_bik_joined_objs_by_kinds_fast: before insert into @tree_id_to_kind_id
2012-08-24 16:54:10.079: sp_delete_bik_joined_objs_by_kinds_fast: after insert into @tree_id_to_kind_id, @rc=57, time taken: 0.230000 s
2012-08-24 16:54:10.079: sp_delete_bik_joined_objs_by_kinds_fast: before insert into @joined_ids_to_del
2012-08-24 16:54:16.131: sp_delete_bik_joined_objs_by_kinds_fast: after insert into @joined_ids_to_del, @rc=212483, time taken: 6.053000 s
2012-08-24 16:54:16.131: sp_delete_bik_joined_objs_by_kinds_fast: before final delete
2012-08-24 16:54:22.554: sp_delete_bik_joined_objs_by_kinds_fast: after final delete, @rc=212483, time taken: 6.423000 s, TOTAL TIME TAKEN: 12.710000 s

*/

-- poprzednia wersja w pliku: alter db for v1.0.88.1 tf.sql
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_move_bik_joined_objs_tmp]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_move_bik_joined_objs_tmp
go

create procedure [dbo].[sp_move_bik_joined_objs_tmp] (@oneGroup varchar(max), @twoGroup varchar(max))
as
begin
	if exists(select 1 from sysobjects where id = object_id('tmp_unique_bik_joined_objs'))
	drop table tmp_unique_bik_joined_objs
	
	create table [dbo].[tmp_unique_bik_joined_objs](
	[src_node_id] [int]  NULL,
	[dst_node_id] [int]  NULL,
	[type] [int]  NULL,
	[inherit_to_descendants] [int] not null default 0
	);
	
	-- wykonanie usuwania
	exec sp_delete_bik_joined_objs_by_kinds_fast @oneGroup, @twoGroup

	insert into tmp_unique_bik_joined_objs(src_node_id, dst_node_id, type, inherit_to_descendants)
	select distinct *
	from tmp_bik_joined_objs
	
	insert into bik_joined_objs(src_node_id, dst_node_id, type, inherit_to_descendants)
	select u.src_node_id, u.dst_node_id, u.type,u.inherit_to_descendants from tmp_unique_bik_joined_objs u left join bik_joined_objs bjo on bjo.src_node_id=u.src_node_id
    and bjo.dst_node_id=u.dst_node_id
    where bjo.id is null
    
    update bik_joined_objs
    set type=1
    from tmp_unique_bik_joined_objs u
    where     
    bik_joined_objs.src_node_id=u.src_node_id 
    and bik_joined_objs.dst_node_id=u.dst_node_id 
    and bik_joined_objs.type = 0
	
	if exists(select 1 from sysobjects where id = object_id('tmp_bik_joined_objs'))
	drop table tmp_bik_joined_objs
	
	if exists(select 1 from sysobjects where id = object_id('tmp_unique_bik_joined_objs'))
	drop table tmp_unique_bik_joined_objs
end
go

-- poprzednia wersja w pliku: alter db for v1.1.1.4 tf.sql
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_connections_for_dqc]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_insert_into_bik_joined_objs_connections_for_dqc
go

create procedure sp_insert_into_bik_joined_objs_connections_for_dqc as
begin

-------------- Test <-> Teradata
exec sp_prepare_bik_joined_objs_tmp

	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
    select distinct test.id, ter.id, 1, 0
    from bik_dqc_test_parameters dtp
    join bik_node test on test.obj_id='t:' + convert(varchar(max), dtp.test_id) and test.is_deleted=0 and test.linked_node_id is null
    join bik_node ter on ter.obj_id=convert(varchar(max), dtp.parameter_name) and ter.is_deleted=0 and ter.linked_node_id is null
    --left join bik_joined_objs obj on obj.src_node_id=test.id and obj.dst_node_id=ter.id
    --where obj.id is null

    insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
    select distinct ter.id, test.id, 1, 0
    from bik_dqc_test_parameters dtp
    join bik_node test on test.obj_id='t:' + convert(varchar(max), dtp.test_id) and test.is_deleted=0 and test.linked_node_id is null
    join bik_node ter on ter.obj_id=convert(varchar(max), dtp.parameter_name) and ter.is_deleted=0 and ter.linked_node_id is null
    --left join bik_joined_objs obj on obj.src_node_id=ter.id and obj.dst_node_id=test.id
    --where obj.id is null

exec sp_move_bik_joined_objs_tmp 'DQCTestSuccess,DQCTestFailed,DQCTestInactive', '@Teradata'

----------------- Test <-> BO

create table #tmp_jo (teradata int, dqc int);
create table #tmp_to_insert (src int, dst int);

declare @teradataTree int;
select @teradataTree=id from bik_tree where code='Teradata'
declare @dqcTree int;
select @dqcTree=id from bik_tree where code='DQC'
declare @reportsTree int;
select @reportsTree=id from bik_tree where code='Reports'
declare @universeTree int;
select @universeTree=id from bik_tree where code='ObjectUniverses'
declare @connectionTree int;
select @connectionTree=id from bik_tree where code='Connections'

insert into #tmp_jo(teradata,dqc)
select src.id, dst.id from bik_joined_objs jo
join bik_node src on src.id=jo.src_node_id
join bik_node dst on dst.id=jo.dst_node_id
where src.tree_id = @teradataTree and src.is_deleted = 0 and src.linked_node_id is null
and dst.tree_id=@dqcTree and dst.is_deleted = 0 and dst.linked_node_id is null
and jo.type=1 -- tylko automatyczne

insert into #tmp_to_insert(src,dst)
select distinct bn.id, tmp.dqc from #tmp_jo tmp
join bik_joined_objs jo on tmp.teradata=jo.dst_node_id 
join bik_node bn on bn.id=jo.src_node_id
where bn.is_deleted = 0 and linked_node_id is null and bn.id <> tmp.dqc
and bn.tree_id in (@reportsTree, @universeTree, @connectionTree)
and jo.type=1 -- tylko automatyczne

exec sp_prepare_bik_joined_objs_tmp

	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
	select src, dst, 1, 0
	from #tmp_to_insert tmp
	--left join bik_joined_objs a on a.src_node_id=src and a.dst_node_id=dst
	--where a.id is null

	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
	select dst, src, 1, 0
	from #tmp_to_insert tmp
	--left join bik_joined_objs a on a.src_node_id=dst and a.dst_node_id=src
	--where a.id is null

exec sp_move_bik_joined_objs_tmp 'DQCTestSuccess,DQCTestFailed,DQCTestInactive', '@Reports, @ObjectUniverses, @Connections'

drop table #tmp_to_insert
drop table #tmp_jo

end
go

-- poprzednia wersja w pliku: alter db for v1.0.88.6 tf.sql
-- zmiana nazwy procki w celu zachowania konwencji nazewniczej: tutaj usuwanie starej, a dalej zalożenie z nową nazwą
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_connections_for_universe_objects]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_insert_connections_for_universe_objects
go
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_universe_objects]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_insert_into_bik_joined_objs_universe_objects
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_universe_objects]
as
begin
	
	declare @uniKind int;
    select @uniKind = id from bik_node_kind where code='Universe'

    declare @dcKind int;
    select @dcKind = id from bik_node_kind where code='DataConnection'

    create table #tmpBranch (branch_id varchar(max), node_id int);

    insert into #tmpBranch(branch_id,node_id)
    select bn.branch_ids, bn2.id from bik_node bn
    join bik_joined_objs jo on jo.src_node_id=bn.id
    join bik_node bn2 on jo.dst_node_id=bn2.id
    and bn2.node_kind_id=@dcKind
    and bn2.linked_node_id is null
    and bn2.is_deleted = 0
    where bn.node_kind_id=@uniKind
    and bn.linked_node_id is null
    and bn.is_deleted = 0

    exec sp_prepare_bik_joined_objs_tmp

		insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
		select bn.id, a.node_id, 1 from #tmpBranch a join bik_node bn on bn.branch_ids like a.branch_id+'%'
		and bn.is_deleted=0
		and bn.linked_node_id is null
		where node_kind_id <> @uniKind
		
		insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
		select bn2.id, bn.id, 1 from bik_node bn join bik_node bn2 on bn2.branch_ids like bn.branch_ids+'%'
		and bn2.is_deleted=0
		and bn2.linked_node_id is null
		and bn.is_deleted=0
		and bn.linked_node_id is null
		and bn.node_kind_id=@uniKind
		and bn2.node_kind_id <> @uniKind

		delete from #tmpBranch
		drop table #tmpBranch

    exec sp_move_bik_joined_objs_tmp 'Universe,DataConnection', 'UniverseClass,Measure,Dimension,Detail,Filter,UniverseAliasTable,UniverseDerivedTable,UniverseTable,UniverseTablesFolder'
	
end
go

-----
------
-------
--------

-- poprzednia wersja w pliku: "alter db for v1.1.6.6 tf.sql"
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_metadata_connections]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_joined_objs_metadata_connections]
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_metadata_connections]
as
begin

	exec sp_prepare_bik_joined_objs_tmp

    declare @teradataKind int;
    select @teradataKind=id from bik_tree where code='Teradata'
    declare @schemaKind int;
    select @schemaKind=id from bik_node_kind where code='TeradataSchema'
    declare @tabKind int;
    select @tabKind=id from bik_node_kind where code='TeradataTable'
    declare @viewKind int;
    select @viewKind=id from bik_node_kind where code='TeradataView'
    declare @connection_kind int;
    select @connection_kind=id from bik_node_kind where code='DataConnection'
    declare @query_kind int;
    select @query_kind=id from bik_node_kind where code='ReportQuery'

    ----------   universe  -->  zapytania   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type, inherit_to_descendants)
    select bn.id as src_node_id, bnu.id as dst_node_id, 1 as type, 1 as inherit_to_descendants
    from bik_sapbo_query sbq
    join bik_node bn on sbq.node_id=bn.id
    join APP_UNIVERSE uni on sbq.universe_cuid=uni.SI_CUID
    join bik_node bnu on bnu.obj_id=convert(varchar(30),uni.si_id) and
    bnu.is_deleted=0 and bnu.node_kind_id=(select id from bik_node_kind where code='Universe')
	where bn.is_deleted=0 

    ----------   connection  -->  zapytania   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bn.id as src_node_id, bnc.id as dst_node_id, 1 as type
    from bik_sapbo_query sbq
    join bik_node bn on sbq.node_id=bn.id
    join APP_UNIVERSE uni on sbq.universe_cuid=uni.SI_CUID
    join bik_node bnu on bnu.obj_id=convert(varchar(30),uni.si_id) and
    bnu.is_deleted=0 and bnu.node_kind_id=(select id from bik_node_kind where code='Universe')
    join bik_node bnc on bnc.obj_id=convert(varchar(30),uni.SI_DATACONNECTION__1) and
    bnc.is_deleted=0 and bnc.node_kind_id=(select id from bik_node_kind where code='DataConnection')
	where bn.is_deleted=0 

    ----------   universe  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bnu.id as src_node_id, sbq.report_node_id as dst_node_id, 1 as type
    from aaa_query sbq
    join APP_UNIVERSE uni on sbq.universe_cuid=uni.SI_CUID
    join bik_node bnu on bnu.obj_id=convert(varchar(30),uni.si_id) and
    bnu.is_deleted=0 and bnu.node_kind_id=(select id from bik_node_kind where code='Universe')

    ----------   raport  -->  universe   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct sbq.report_node_id as src_node_id, bnu.id as dst_node_id, 1 as type
    from aaa_query sbq
    join APP_UNIVERSE uni on sbq.universe_cuid=uni.SI_CUID
    join bik_node bnu on bnu.obj_id=convert(varchar(30),uni.si_id) and
    bnu.is_deleted=0 and bnu.node_kind_id=(select id from bik_node_kind where code='Universe')

    ----------   raport  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct sbq.report_node_id as src_node_id, bnc.id as dst_node_id, 1 as type
    from aaa_query sbq
    join APP_UNIVERSE uni on sbq.universe_cuid=uni.SI_CUID
    join bik_node bnu on bnu.obj_id=convert(varchar(30),uni.si_id) and
    bnu.is_deleted=0 and bnu.node_kind_id=(select id from bik_node_kind where code='Universe')
    join bik_node bnc on bnc.obj_id=convert(varchar(30),uni.SI_DATACONNECTION__1) and
    bnc.is_deleted=0 and bnc.node_kind_id=(select id from bik_node_kind where code='DataConnection')

	----------   connection  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bnc.id as src_node_id, sbq.report_node_id as dst_node_id, 1 as type
    from aaa_query sbq
    join APP_UNIVERSE uni on sbq.universe_cuid=uni.SI_CUID
    join bik_node bnu on bnu.obj_id=convert(varchar(30),uni.si_id) and
    bnu.is_deleted=0 and bnu.node_kind_id=(select id from bik_node_kind where code='Universe')
    join bik_node bnc on bnc.obj_id=convert(varchar(30),uni.SI_DATACONNECTION__1) and
    bnc.is_deleted=0 and bnc.node_kind_id=(select id from bik_node_kind where code='DataConnection')

    ----------   teradata Table/View  -->  obiekt   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct a.object_node_id, bn.id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)

    ----------   teradata Schema  -->  obiekt   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct a.object_node_id, bn.id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id=@schemaKind

    ----------   universum  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bntf.parent_node_id, bn.id, 1 from bik_sapbo_universe_table b
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    where b.type='Teradata' and b.is_derived=0

    ----------   teradata Table/View  -->  universum   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bntf.parent_node_id, 1 from bik_sapbo_universe_table b
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    where b.type='Teradata' and b.is_derived=0

    ----------   universum  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bntf.parent_node_id, bn.id, 1 from bik_sapbo_universe_table b
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id=@schemaKind
    where b.type='Teradata' and b.is_derived=0

    ----------   teradata Schema  -->  universum   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bntf.parent_node_id, 1 from bik_sapbo_universe_table b
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id=@schemaKind
    where b.type='Teradata' and b.is_derived=0

    ----------   connection  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn2.id, bn.id, 1 from bik_sapbo_universe_table b
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id=@schemaKind
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_joined_objs jo on jo.src_node_id=bntf.parent_node_id
    join bik_node bn2 on jo.dst_node_id=bn2.id
    and bn2.is_deleted=0
    and bn2.node_kind_id=@connection_kind
    where b.type='Teradata' and b.is_derived=0 and bn2.id is not null

    ----------   teradata Schema  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn2.id, 1 from bik_sapbo_universe_table b
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id=@schemaKind
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_joined_objs jo on jo.src_node_id=bntf.parent_node_id
    join bik_node bn2 on jo.dst_node_id=bn2.id
    and bn2.is_deleted=0
    and bn2.node_kind_id=@connection_kind
    where b.type='Teradata' and b.is_derived=0 and bn2.id is not null

    ----------   teradata Table/View  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn2.id, 1 from bik_sapbo_universe_table b
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@tabKind, @viewKind)
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_joined_objs jo on jo.src_node_id=bntf.parent_node_id
    join bik_node bn2 on jo.dst_node_id=bn2.id
    and bn2.is_deleted=0
    and bn2.node_kind_id=@connection_kind
    where b.type='Teradata' and b.is_derived=0 and bn2.id is not null

    ----------   connection  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn2.id, bn.id, 1 from bik_sapbo_universe_table b
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@tabKind, @viewKind)
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_joined_objs jo on jo.src_node_id=bntf.parent_node_id
    join bik_node bn2 on jo.dst_node_id=bn2.id
    and bn2.is_deleted=0
    and bn2.node_kind_id=@connection_kind
    where b.type='Teradata' and b.is_derived=0 and bn2.id is not null 

    ----------   teradata Table/View  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn4.parent_node_id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    join bik_node bn3 on bn3.linked_node_id=a.object_node_id
    and bn3.is_deleted=0
    join bik_node bn4 on bn3.parent_node_id=bn4.id
    and bn4.node_kind_id=@query_kind
    and bn4.is_deleted=0

    ----------   raport  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.parent_node_id, bn.id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    join bik_node bn3 on bn3.linked_node_id=a.object_node_id
    and bn3.is_deleted=0
    join bik_node bn4 on bn3.parent_node_id=bn4.id
    and bn4.node_kind_id=@query_kind
    and bn4.is_deleted=0

    ----------   teradata Schema  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn4.parent_node_id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id = @schemaKind
    join bik_node bn3 on bn3.linked_node_id=a.object_node_id
    and bn3.is_deleted=0
    join bik_node bn4 on bn3.parent_node_id=bn4.id
    and bn4.node_kind_id=@query_kind
    and bn4.is_deleted=0

    ----------   raport  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.parent_node_id, bn.id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id = @schemaKind
    join bik_node bn3 on bn3.linked_node_id=a.object_node_id
    and bn3.is_deleted=0
    join bik_node bn4 on bn3.parent_node_id=bn4.id
    and bn4.node_kind_id=@query_kind
    and bn4.is_deleted=0

    ----------   zapytanie  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.id, bn.id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id=@schemaKind
    join bik_node bn3 on bn3.linked_node_id=a.object_node_id
    and bn3.is_deleted=0
    join bik_node bn4 on bn3.parent_node_id=bn4.id
    and bn4.node_kind_id=@query_kind
    and bn4.is_deleted=0

    ----------   zapytanie  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.id, bn.id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    join bik_node bn3 on bn3.linked_node_id=a.object_node_id
    and bn3.is_deleted=0
    join bik_node bn4 on bn3.parent_node_id=bn4.id
    and bn4.node_kind_id=@query_kind
    and bn4.is_deleted=0

	----------   obiekt  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct ids.object_node_id, bn.parent_node_id, 1 from aaa_ids_for_report ids join bik_node bn on ids.query_node_id=bn.id
	
	----------   UniverseTable  -->  teradata Table/View   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bn.id, 1 from bik_sapbo_universe_table bsut 
	join bik_node bn on bsut.name_for_teradata=bn.obj_id 
	and bn.is_deleted=0
	and bn.linked_node_id is null

	----------   teradata Table/View  -->  UniverseTable   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bsut.node_id, 1 from bik_sapbo_universe_table bsut 
	join bik_node bn on bsut.name_for_teradata=bn.obj_id 
	and bn.is_deleted=0
	and bn.linked_node_id is null

	----------   UniverseTable  -->  teradata Schema   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bn.id, 1 from bik_sapbo_universe_table bsut 
	join bik_node bn on bsut.schema_name=bn.obj_id 
	and bn.is_deleted=0
	and bn.linked_node_id is null
	
	----------   UniverseAliasTable  -->  UniverseTable   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bsut1.node_id, 1 from bik_sapbo_universe_table bsut 
	join bik_sapbo_universe_table bsut1 on bsut.original_table = bsut1.their_id
	join bik_node bn on bn.id=bsut.node_id
	join bik_node bn2 on bn2.id=bsut1.node_id
	where bsut.is_alias=1 and bn2.parent_node_id=bn.parent_node_id

	-- usuniecie alias table - univ table
	exec sp_delete_bik_joined_objs_by_kinds_fast 'UniverseTable', 'UniverseAliasTable'
	
    exec sp_move_bik_joined_objs_tmp 'TeradataView,TeradataTable,TeradataSchema,Webi,ReportQuery', 'Universe,DataConnection,Measure,Dimension,Detail,Filter,Webi,ReportQuery,UniverseAliasTable,UniverseDerivedTable,UniverseTable'
	
end;
go

-----------------
-- poprzednia wersja w pliku: alter db for v1.1.6.6 tf.sql
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_mssql_connections]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_joined_objs_mssql_connections]
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_mssql_connections]
as
begin

	exec sp_prepare_bik_joined_objs_tmp
    
    declare @connection_kind int;
    select @connection_kind=id from bik_node_kind where code='DataConnection'    
    declare @universe_kind int;
    select @universe_kind=id from bik_node_kind where code='Universe'
    declare @query_kind int;
    select @query_kind=id from bik_node_kind where code='ReportQuery'

	---------- universe table --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ut.node_id, bn.id, 1 from bik_sapbo_universe_table ut 
	join bik_node bn on bn.obj_id=ut.mssql_table_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	
	---------- universe table --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ut.node_id, bn.id, 1 from bik_sapbo_universe_table ut 
	join bik_node bn on bn.obj_id=ut.database_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	
	---------- object --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ot.object_node_id, bn.id, 1 from bik_sapbo_universe_table ut 
	join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	join bik_node bn on bn.obj_id=ut.mssql_table_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	
	---------- object --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ot.object_node_id, bn.id, 1 from bik_sapbo_universe_table ut 
	join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	join bik_node bn on bn.obj_id=ut.database_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	
	---------- query --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.id, bn.id, 1 from bik_sapbo_universe_table ut 
	join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	join bik_node bn on bn.obj_id=ut.mssql_table_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id=@query_kind
	and bnparent.linked_node_id is null
	
	---------- query --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.id, bn.id, 1 from bik_sapbo_universe_table ut 
	join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	join bik_node bn on bn.obj_id=ut.database_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id=@query_kind
	and bnparent.linked_node_id is null
	
	---------- report --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.parent_node_id, bn.id, 1 from bik_sapbo_universe_table ut 
	join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	join bik_node bn on bn.obj_id=ut.mssql_table_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id=@query_kind
	and bnparent.linked_node_id is null
	
	---------- report --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.parent_node_id, bn.id, 1 from bik_sapbo_universe_table ut 
	join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	join bik_node bn on bn.obj_id=ut.database_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id=@query_kind
	and bnparent.linked_node_id is null	

	---------- universe --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 from bik_sapbo_universe_table ut 
	join bik_node bn on bn.obj_id=ut.mssql_table_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_joined_objs obj on obj.src_node_id=ut.node_id
	and obj.type = 1
	join bik_node bndst on bndst.id=obj.dst_node_id
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- universe --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 from bik_sapbo_universe_table ut 
	join bik_node bn on bn.obj_id=ut.database_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_joined_objs obj on obj.src_node_id=ut.node_id
	and obj.type = 1
	join bik_node bndst on bndst.id=obj.dst_node_id
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- connection --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 from bik_sapbo_universe_table ut 
	join bik_node bn on bn.obj_id=ut.mssql_table_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_joined_objs obj on obj.src_node_id=ut.node_id
	and obj.type = 1
	join bik_node bndst on bndst.id=obj.dst_node_id
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- connection --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 from bik_sapbo_universe_table ut 
	join bik_node bn on bn.obj_id=ut.database_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_joined_objs obj on obj.src_node_id=ut.node_id
	and obj.type = 1
	join bik_node bndst on bndst.id=obj.dst_node_id
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null


	
	---------- mssql table --> universe table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, ut.node_id, 1 from bik_sapbo_universe_table ut 
	join bik_node bn on bn.obj_id=ut.mssql_table_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	
	---------- mssql table --> report -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bnparent.parent_node_id, 1 from bik_sapbo_universe_table ut 
	join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	join bik_node bn on bn.obj_id=ut.mssql_table_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id=@query_kind
	and bnparent.linked_node_id is null
	
	---------- mssql table --> universe -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 from bik_sapbo_universe_table ut 
	join bik_node bn on bn.obj_id=ut.mssql_table_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_joined_objs obj on obj.src_node_id=ut.node_id
	and obj.type = 1
	join bik_node bndst on bndst.id=obj.dst_node_id
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- mssql table --> connection -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 from bik_sapbo_universe_table ut 
	join bik_node bn on bn.obj_id=ut.mssql_table_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_joined_objs obj on obj.src_node_id=ut.node_id
	and obj.type = 1
	join bik_node bndst on bndst.id=obj.dst_node_id
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- mssql DB --> connection -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 from bik_sapbo_universe_table ut 
	join bik_node bn on bn.obj_id=ut.database_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_joined_objs obj on obj.src_node_id=ut.node_id
	and obj.type = 1
	join bik_node bndst on bndst.id=obj.dst_node_id
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null

	---------- mssql DB --> universe -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 from bik_sapbo_universe_table ut 
	join bik_node bn on bn.obj_id=ut.database_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_joined_objs obj on obj.src_node_id=ut.node_id
	and obj.type = 1
	join bik_node bndst on bndst.id=obj.dst_node_id
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- mssql DB --> report -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bnparent.parent_node_id, 1 from bik_sapbo_universe_table ut 
	join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	join bik_node bn on bn.obj_id=ut.database_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id=@query_kind
	and bnparent.linked_node_id is null		
	
    exec sp_move_bik_joined_objs_tmp '@MSSQL', '@Reports, @ObjectUniverses, @Connections'
    
end;
go

exec sp_update_version '1.1.8.6', '1.1.8.7';
go