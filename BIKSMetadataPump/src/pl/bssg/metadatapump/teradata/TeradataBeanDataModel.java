/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.teradata;

/**
 *
 * @author pgajda
 */
public class TeradataBeanDataModel extends TeradataBean {
    public String databaseName;
    public String tableName;
    
    public TeradataBeanDataModel(int dataLoadLogId, String type, String name, String extraInfo, String branchNames, String parentBranchNames, String requestText,
                        String databaseName, String tableName ){
        super( dataLoadLogId, type, name, extraInfo, branchNames, parentBranchNames, requestText);
        this.databaseName = databaseName;
        this.tableName = tableName;
    }
}
