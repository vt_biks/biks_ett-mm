/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikwidgets.TreeOfRoles;
import pl.fovis.common.UseInNodeSelectionBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author beata
 * z poziomu uzytkownika
 */
public class RolesDialog extends BaseActionOrCancelDialog {

    protected IParametrizedContinuation<UseInNodeSelectionBean> saveCont;
    protected int nodeId;
    protected TreeOfRoles treeRoles;

    @Override
    protected String getDialogCaption() {
        return I18n.wybierzRole.get() /* I18N:  */;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.dalej.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {
        VerticalPanel rightVp = new VerticalPanel();

        main.setWidth("500px");
        rightVp.setStyleName("EntityDetailsPane selectRoleInDialogPane");
        rightVp.setWidth("100%");

        treeRoles = new TreeOfRoles();
        Widget treeRolesWidget = treeRoles.buildWidgets();

        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(treeRolesWidget,
                BIKClientSingletons.ORIGINAL_SELECT_ROLE_IN_DIALOG_SCROLL_HEIGHT);

        rightVp.add(treeRolesWidget);
        main.add(rightVp);

    }

    protected void configureButtons() {
        actionBtn.setEnabled(true);
    }

    @Override
    protected void doAction() {
        UseInNodeSelectionBean unb = new UseInNodeSelectionBean();
        unb.roleForNodeId = treeRoles.getIdOfSelectedRole();
        unb.roleForNodeName = treeRoles.getNameOfSelectedRole();

        saveCont.doIt(unb);
        //boolean isReplaceAll = false;//// !!rozpoznac do czego
        //saveCont.doIt(new Pair<UseInNodeSelectionBean, Boolean>(unb, isReplaceAll));
    }

    @Override
    protected boolean doActionBeforeClose() {

        if (treeRoles.getSelectedNode() == null) {
            Window.alert(I18n.wybierzRole.get() /* I18N:  */);
            return false;
        }

        return true;
    }

    public void buildAndShowDialog(int nodeId, IParametrizedContinuation<UseInNodeSelectionBean> saveCont) {
        this.saveCont = saveCont;
        this.nodeId = nodeId;
        super.buildAndShowDialog();
    }
}
