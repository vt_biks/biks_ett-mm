/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.lisa;

import java.io.Serializable;

/**
 * Wyjatek dedykowany dla modulu NAS
 * @author tbzowka
 */
public class TeradataException extends RuntimeException implements Serializable {

    private Type type = Type.OTHER_PROBLEM;

    public TeradataException(String errorMsg) {
        super(errorMsg);
    }

    public TeradataException(Boolean isBlocking) {
        if (!isBlocking) {
            type = Type.WRONG_CREDENTIALS;
        }
    }

    public TeradataException() {
    }

    public boolean isBlockingProblem() {
        return Type.OTHER_PROBLEM.equals(type);
    }

    public void setIsBlocking(boolean b) {
        if (b) {
            type = Type.OTHER_PROBLEM;
        } else {
            type = Type.WRONG_CREDENTIALS;
        }
    }

    enum Type implements Serializable {

        WRONG_CREDENTIALS, OTHER_PROBLEM
    }
}
