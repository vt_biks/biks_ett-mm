/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages.biadmin;

import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.LogsPageBase;
import pl.fovis.client.entitydetailswidgets.NodeAwareBeanExtractorBase;
import pl.fovis.common.BIAArchiveLogBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.Pair;

/**
 *
 * @author ctran
 */
public class BIAArchiveLogPage extends LogsPageBase<BIAArchiveLogBean> {

    public BIAArchiveLogPage() {
        super(20);
    }

    @Override
    protected String getHeaderString() {
        return I18n.logiZOstatnichWykonanArchiwizacji.get();
    }

    @Override
    protected void callServiceToGetData(StandardAsyncCallback<Pair<Integer, List<BIAArchiveLogBean>>> callback) {
        BIKClientSingletons.getService().getBIAArchiveLogs(pwh.pageNum, pwh.pageSize, callback);
    }

    @Override
    protected NodeAwareBeanExtractorBase<BIAArchiveLogBean> getDataExtractor() {
        return new BIAArchiveLogBeanExtractor();
    }

//    protected BeanGrid<BIAArchiveLogBean> logsGrid;
//    protected VerticalPanel vp;
//
//    @Override
//    protected Widget buildWidgets() {
//
//        vp = new VerticalPanel();
//        getLogs();
//        ScrollPanel scPanel = new ScrollPanel(vp);
////        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(scPanel, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT);
//
//        return scPanel;
//    }
//
//    public void getLogs() {
//        final BIAArchiveLogDisplay lisaLogDisplay = new BIAArchiveLogDisplay();
//
//        logsGrid = new BeanGrid<BIAArchiveLogBean>(new IBeanProvider<BIAArchiveLogBean>() {
//            @Override
//            public void getBeans(final int offset, final int limit, Iterable<String> sortCols, Iterable<String> descendingCols, final IParametrizedContinuation<Pair<? extends Iterable<BIAArchiveLogBean>, Integer>> showBeansCont) {
//
//                BIKClientSingletons.getService().getBIAArchiveLogs(offset, limit, new StandardAsyncCallback<List<BIAArchiveLogBean>>() {
//                    @Override
//                    public void onSuccess(List<BIAArchiveLogBean> result) {
//                        Pair<List<BIAArchiveLogBean>, Integer> p = new Pair<List<BIAArchiveLogBean>, Integer>();
//                        p.v1 = result;
//                        p.v2 = result.size();
//
//                        showBeansCont.doIt(p);
//                    }
//                });
//            }
//        }, lisaLogDisplay, LisaConstants.GRID_PAGE_SIZE, BIAArchiveLogDisplay.COLUMN_JOB_CUID, BIAArchiveLogDisplay.COLUMN_START_TIME, BIAArchiveLogDisplay.COLUMN_END_TIME, BIAArchiveLogDisplay.COLUMN_STATUS, BIAArchiveLogDisplay.COLUMN_FILE_CNT);
//
//        logsGrid.setHeaderStyleName("lisaDictHeader");
//        final BeanGridPager<BIAArchiveLogBean> bgp = new BeanGridPager<BIAArchiveLogBean>(logsGrid);
//        bgp.setUknownPageCountMode(true);
//        HorizontalPanel hp = new HorizontalPanel();
//
//        hp.add(NewLookUtils.newCustomPushButton(I18n.odswiez.get(), new ClickHandler() {
//
//            @Override
//            public void onClick(ClickEvent event) {
//                bgp.navigate(0);
//            }
//        }));
//        hp.add(bgp);
//        vp.add(hp);
//        vp.add(logsGrid);
//    }
//
//    public String getId() {
//        return "admin:biadmin:archiveManager:logs";
//    }
//
//    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
//    }
//
//    private class BIAArchiveLogDisplay implements IBeanDisplay<BIAArchiveLogBean> {
//
//        public static final String COLUMN_JOB_CUID = "jobCuid";
//        public static final String COLUMN_START_TIME = "startTime" /* i18n: no:column-name */;
//        public static final String COLUMN_END_TIME = "endTime";
//        public static final String COLUMN_STATUS = "status";
//        public static final String COLUMN_FILE_CNT = "fileCnt";
//        private final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
//        private final DateTimeFormat dtf = DateTimeFormat.getFormat(DATE_FORMAT);
////        private final INamedTypedPropsBeanBroker<BIAArchiveLogBean> beanPropsBroker = BIKClientSingletons.beanCreator.getBroker(BIAArchiveLogBean.class);
//
//        @Override
//        public Object getHeader(String colName) {
//            if (colName.equals(COLUMN_JOB_CUID)) {
//                return I18n.jobCuid.get() /* i18n:  */;
//            } else if (colName.equals(COLUMN_START_TIME)) {
//                return I18n.starttime.get() /* i18n:  */;
//            } else if (colName.equals(COLUMN_END_TIME)) {
//                return I18n.endtime.get() /* i18n:  */;
//            } else if (colName.equals(COLUMN_STATUS)) {
//                return I18n.status.get() /* i18n:  */;
//            } else if (colName.equals(COLUMN_FILE_CNT)) {
//                return I18n.liczbaPlikow.get();
//            }
//            return colName;
//
//        }
//
//        @Override
//        public Object getValue(BIAArchiveLogBean bean, String colName) {
//            if (colName.equals(COLUMN_JOB_CUID)) {
//                return bean.jobCuid /* i18n:  */;
//            } else if (colName.equals(COLUMN_START_TIME) && bean.startTime != null) {
//                return dtf.format(bean.startTime) /* i18n:  */;
//            } else if (colName.equals(COLUMN_END_TIME) && bean.endTime != null) {
//                return dtf.format(bean.endTime) /* i18n:  */;
//            } else if (colName.equals(COLUMN_STATUS)) {
//                return bean.status /* i18n:  */;
//            } else if (colName.equals(COLUMN_FILE_CNT)) {
//                return bean.fileCnt /* i18n:  */;
//            }
//            return "";
//        }
//    }
    @Override
    public String getId() {
        return "admin:biadmin:archiveManager:log";
    }

    @Override
    public boolean isPageEnabled() {
        return true;
//    private class BIAArchiveLogDisplay implements IBeanDisplay<BIAArchiveLogBean> {
//
//        public static final String COLUMN_JOB_CUID = "jobCuid";
//        public static final String COLUMN_START_TIME = "startTime" /* i18n: no:column-name */;
//        public static final String COLUMN_END_TIME = "endTime";
//        public static final String COLUMN_STATUS = "status";
//        public static final String COLUMN_FILE_CNT = "fileCnt";
//        private final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
//        private final DateTimeFormat dtf = DateTimeFormat.getFormat(DATE_FORMAT);
////        private final INamedTypedPropsBeanBroker<BIAArchiveLogBean> beanPropsBroker = BIKClientSingletons.beanCreator.getBroker(BIAArchiveLogBean.class);
//
//        @Override
//        public Object getHeader(String colName) {
//            if (colName.equals(COLUMN_JOB_CUID)) {
//                return I18n.cuidProcesu.get() /* i18n:  */;
//            } else if (colName.equals(COLUMN_START_TIME)) {
//                return I18n.dataRozpoczecia.get() /* i18n:  */;
//            } else if (colName.equals(COLUMN_END_TIME)) {
//                return I18n.dataZakonczenia.get() /* i18n:  */;
//            } else if (colName.equals(COLUMN_STATUS)) {
//                return I18n.status.get() /* i18n:  */;
//            } else if (colName.equals(COLUMN_FILE_CNT)) {
//                return I18n.zarchiwizowanoPlikow.get();
//            }
//            return colName;
//
//        }
//
//        @Override
//        public Object getValue(BIAArchiveLogBean bean, String colName) {
//            if (colName.equals(COLUMN_JOB_CUID)) {
//                return bean.jobCuid /* i18n:  */;
//            } else if (colName.equals(COLUMN_START_TIME) && bean.startTime != null) {
//                return dtf.format(bean.startTime) /* i18n:  */;
//            } else if (colName.equals(COLUMN_END_TIME) && bean.endTime != null) {
//                return dtf.format(bean.endTime) /* i18n:  */;
//            } else if (colName.equals(COLUMN_STATUS)) {
//                return bean.status /* i18n:  */;
//            } else if (colName.equals(COLUMN_FILE_CNT)) {
//                return bean.fileCnt /* i18n:  */;
//            }
//            return "";
//        }
    }
}
