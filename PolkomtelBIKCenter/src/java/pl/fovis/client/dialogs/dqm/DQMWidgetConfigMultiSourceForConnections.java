/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.dqm;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.PushButton;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.ConnectionParametersDQMConnectionsBean;

import pl.fovis.common.dqm.DQMMultiSourceBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author bfechner
 */
public class DQMWidgetConfigMultiSourceForConnections extends DQMWidgetConfigMultiSourceBase {

    protected Map<Integer, ConnectionParametersDQMConnectionsBean> connections;

    public DQMWidgetConfigMultiSourceForConnections(Map<Integer, ConnectionParametersDQMConnectionsBean> connections) {
        this.connections = connections;
    }

    @Override
    protected void createHeader() {
        sourceTable.setHTML(0, 0, "<b>" + I18n.dqmPolaczenie.get() + "</b>");
//        sourceTable.setHTML(0, 1, "<b>" + I18n.system.get() + "</b>");
//        sourceTable.setHTML(0, 2, "<b>" + I18n.bazaDanychLubPlik.get() + "</b>");
        sourceTable.setHTML(0, 3, "<b>" + I18n.edytuj.get() + "</b>");
        sourceTable.setHTML(0, 4, "<b>" + I18n.usun.get() + "</b>");
    }

    @Override
    protected void insertSourceRow(final int row, final DQMMultiSourceBean bean) {
        sourceTable.setHTML(row, 0, bean.connectionName);
//        sourceTable.setHTML(row, 1, bean.serverName);
//        sourceTable.setHTML(row, 2, !BaseUtils.isStrEmptyOrWhiteSpace(bean.databaseName) ? bean.databaseName : bean.resultFileName);
        PushButton editQueryBtn = new PushButton(I18n.edytuj.get());
        NewLookUtils.makeCustomPushButton(editQueryBtn);
        editQueryBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {

                if (BIKClientSingletons.useDqmConnectionMode()) {
                    new AddOrEditDQMSourceDialogForConnections().buildAndShowDialog(nodeId, bean, connections, new IParametrizedContinuation<DQMMultiSourceBean>() {

                        @Override
                        public void doIt(DQMMultiSourceBean param) {
                            redisplayGrid();
                        }
                    });
                }
            }
        });

        sourceTable.setWidget(row, 3, editQueryBtn);
        sourceTable.setWidget(row, 4, createDeleteButton(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                multiSources.remove(bean);
                redisplayGrid();
            }
        }, "images/trash_gray.gif"));

    }

    protected PushButton addSourceBtn() {
        PushButton addSourceBtn = new PushButton(I18n.dodajZrodlo.get());
        NewLookUtils.makeCustomPushButton(addSourceBtn);
        addSourceBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {

                new AddOrEditDQMSourceDialogForConnections().buildAndShowDialog(nodeId, null, connections, new IParametrizedContinuation<DQMMultiSourceBean>() {

                    @Override
                    public void doIt(DQMMultiSourceBean param) {
                        multiSources.add(param);
                        redisplayGrid();
                    }
                });

            }
        });
        return addSourceBtn;
    }

}
