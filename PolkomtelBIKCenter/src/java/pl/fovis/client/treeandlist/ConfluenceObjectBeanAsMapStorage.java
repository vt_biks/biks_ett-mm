/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.treeandlist;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineHTML;
import java.util.HashMap;
import java.util.Map;
import pl.bssg.metadatapump.common.ConfluenceObjectBean;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.foxygwtcommons.treeandlist.ITreeGridCellWidgetBuilder;
import pl.fovis.foxygwtcommons.treeandlist.MapTreeRowAndBeanStorageBase;
import static pl.fovis.foxygwtcommons.treeandlist.MapTreeRowAndBeanStorageBase.ID_FIELD_NAME;

/**
 *
 * @author tflorczak
 */
public abstract class ConfluenceObjectBeanAsMapStorage extends MapTreeRowAndBeanStorageBase<String, ConfluenceObjectBean> {

    public static final String PARENT_CONF_ID_FIELD_NAME = "parentId";
//    public static final String CHECKBOX_FIELD_NAME = "checkbox";
    protected boolean hasCheckBox;

    public ConfluenceObjectBeanAsMapStorage(ITreeGridCellWidgetBuilder<ConfluenceObjectBean> optCellWidgetBuilder) {
        super(ID_FIELD_NAME, PARENT_CONF_ID_FIELD_NAME, optCellWidgetBuilder);
    }

    public ConfluenceObjectBeanAsMapStorage(ITreeGridCellWidgetBuilder<ConfluenceObjectBean> optCellWidgetBuilder, boolean hasCheckBox) {
        super(ID_FIELD_NAME, PARENT_CONF_ID_FIELD_NAME, optCellWidgetBuilder);
        this.hasCheckBox = hasCheckBox;
    }

    @Override
    protected Map<String, Object> convertBeanToRow(final ConfluenceObjectBean bean) {
        Map<String, Object> row = new HashMap<String, Object>();
        row.put(ID_FIELD_NAME, bean.id);
        row.put(PARENT_ID_FIELD_NAME, bean.parentId);
        if (hasCheckBox) {
            final CheckBox cb = new CheckBox();
            cb.setHTML(BIKClientSingletons.getHtmlForNodeKindIconWithText(bean.kind, BIKClientSingletons.getTreeCodeById(bean.treeId), bean.name));
            cb.setStyleName("cellInner");
            cb.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    selectionChanged(bean.id, bean.kind, cb.getValue());
                }
            });
            row.put(NAME_FIELD_NAME, cb);
        } else {
            InlineHTML name = new InlineHTML(BIKClientSingletons.getHtmlForNodeKindIconWithText(bean.kind, BIKClientSingletons.getTreeCodeById(bean.treeId), bean.name));
            row.put(NAME_FIELD_NAME, name);
        }
//        putColValueWithConvertion(row, bean, NAME_FIELD_NAME, bean.name);
        putColValueWithConvertion(row, bean, PRETTY_KIND_FIELD_NAME, BIKClientSingletons.getNodeKindCaptionByCode(bean.kind));
//        row.put(ICON_FIELD_NAME, "images" /* I18N: no */ + "/" + BIKClientSingletons.getNodeKindIconNameByCode(bean.kind) + "." + "gif" /* I18N: no */);
        return row;
    }

    protected void selectionChanged(String id, String kind, boolean isChecked) {
        // NO OP
    }
}
