/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.db.setdbperformance;

import commonlib.GlobalThreadStopWatch;

/**
 *
 * @author pmielanczuk
 */
public class QueryStep extends Step {

    public static long totalRowCnt = 0;
    String dbName;
    String queryTxt;

    public QueryStep(String dbName, String queryTxt) {
        this.dbName = dbName;
        this.queryTxt = queryTxt;
    }

    public void run() {
//        if (true) {
//            return;
//        }
        GlobalThreadStopWatch.startProcess("step run", true);
        try {
            if (dbName != null) {
                GlobalThreadStopWatch.startProcessStage("set db to: " + dbName);
                DaoSingleton.setDatabase(dbName);
            }
            GlobalThreadStopWatch.startProcessStage("run query");
            totalRowCnt += DaoSingleton.runQuery(queryTxt).size();
//        totalRowCnt += DaoSingleton.runQuery("select 1").size();
        } finally {
            GlobalThreadStopWatch.endProcess();
        }
    }
}
