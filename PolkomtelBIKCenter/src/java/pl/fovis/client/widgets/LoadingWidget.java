/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.widgets;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;

/**
 *
 * @author tflorczak
 */
public class LoadingWidget extends HorizontalPanel {

    protected String text;

    public LoadingWidget(String text) {
        this.text = text;
    }

    public HorizontalPanel buildWidget() {
        add(new Label(text));
        Image iconImg = new Image("images/loader.gif");
        iconImg.setSize("16px", "16px");
        add(iconImg);
        setSpacing(10);
        return this;
    }
}
