/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.db.setdbperformance;

import java.util.List;
import java.util.Map;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.serverlogic.db.MssqlConnection;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;

/**
 *
 * @author pmielanczuk
 */
public class DaoSingleton {

    public static IBeanConnectionEx<Object> conn = new MssqlConnection(
            new MssqlConnectionConfig("bssg-dell4", "SQLEXPRESS", "MasterBIKS8", "sa", "MSsql123"));

    public static List<Map<String, Object>> runQuery(String queryTxt) {
        return conn.execQry(queryTxt);
    }

    public static void execCmd(String cmdTxt) {
        conn.execCommand(cmdTxt);
    }

    public static void setDatabase(String dbName) {
        conn.setCurrentDatabase(dbName);
    }

    public static void finalizeTran(boolean doCommit) {
        conn.finalizeTran(doCommit);
    }
}
