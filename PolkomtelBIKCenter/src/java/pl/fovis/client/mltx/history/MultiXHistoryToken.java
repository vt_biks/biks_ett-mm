/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.mltx.history;

import java.util.HashMap;
import java.util.Map;
import pl.fovis.client.mltx.MultiRegistrationPage;
import pl.fovis.client.mltx.MultixEmailConfirmedPage;
import pl.fovis.client.mltx.MultixLoginPage;
import pl.fovis.client.mltx.MultixResePasswordSendPage;
import pl.fovis.client.mltx.MultixResetPasswordPage;
import pl.fovis.client.mltx.MultixStatisticsPage;
import pl.fovis.common.mltx.MultixConstants;

/**
 *
 * @author pku
 */
public enum MultiXHistoryToken {

    REGISTRATION("Registration", MultiRegistrationPage.getHistoryAction()),
    EMAIL_CONFIRMED(MultixConstants.CONFIRMATION_TOKEN, MultixEmailConfirmedPage.getHistoryAction()),
    INVITE_CONFIRMED(MultixConstants.INVITE_TOKEN, MultixEmailConfirmedPage.getHistoryActionInvite()),
    PASSWORD_RESET_SEND("ResetSend", MultixResePasswordSendPage.getHistoryAction()),
    PASSWORD_RESET(MultixConstants.RESET_TOKEN, MultixResetPasswordPage.getHistoryAction()),
    LOGIN("Login", MultixLoginPage.getHistoryAction()),
    MULTIX_STATISTICS("MultixStatistics", MultixStatisticsPage.getHistoryAction());
    public String hname;
    public HistoryAction action;
    private final static Map<String, MultiXHistoryToken> map = new HashMap<String, MultiXHistoryToken>();

    static {
        for (MultiXHistoryToken t : MultiXHistoryToken.values()) {
            map.put(t.hname, t);
        }
    }

    MultiXHistoryToken(String name, HistoryAction action) {
        this.hname = name;
        this.action = action;
    }

    public static MultiXHistoryToken getTokenByName(String name) {
        MultiXHistoryToken t = map.get(name);
        if (t == null) {
            return LOGIN;
        }
        return t;
    }
}
