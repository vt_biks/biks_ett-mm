/*
 * PostgresCommandBatcher.java
 *
 * Created on 10 sierpień 2006, 16:19
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package pl.trzy0.foxy.serverlogic.simpledblib;

import simplelib.LameRuntimeException;
import commonlib.LameUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public class PostgresCommandBatcher {
    
    static ILameLogger logger = LameLoggerFactory.getLogger(PostgresCommandBatcher.class);
    //LameLogger.getClassLogger();
    
    public interface BatchCommand {
        void run() throws SQLException;
        //void writeAsBytes(OutputStream outStrm) throws IOException;
        String runSeparatedText();
    }
    
    static private final int MAX_PENDING_COMMANDS = 5000;
    static private final int RUN_COMMANDS_AT_ONCE = 1000;
    private int totalCount = 0;
    
    private Connection conn;
    private Statement stmt = null;
    private PreparedStatement prepStmt = null;
    private ArrayList<BatchCommand> pendingCommands;
//    private String fileName;
    private boolean appendToFile = false;
    private int commandPatternCount = 0;
    
    /** Creates a new instance of PostgresCommandBatcher */
    public PostgresCommandBatcher(Connection conn//, String fileName
            ) throws SQLException {
        this.conn = conn;
        if (conn != null)
            this.stmt = conn.createStatement();
//        this.fileName = fileName;
        this.pendingCommands = new ArrayList<BatchCommand>();
    }
    
    public void add(BatchCommand bc) throws SQLException//, IOException
    {
        String runSeparatedText = bc.runSeparatedText();
        boolean isSeparated = runSeparatedText != null;
        
        long milliStart;
        
        if (isSeparated) {
            milliStart = System.currentTimeMillis();
            runPending(true);
            if (logger.isInfoEnabled())
                logger.info("running separated: " + runSeparatedText);
        } else
            milliStart = 0;
        
        pendingCommands.add(bc);
        
        if (isSeparated || pendingCommands.size() >= MAX_PENDING_COMMANDS)
            runPending(isSeparated);
        
        if (isSeparated) {
            long milliEnd = System.currentTimeMillis();
            if (logger.isInfoEnabled())
                logger.info("running separated done, time taken: " + (milliEnd - milliStart) / 1000.0 + " seconds");
        }
    }
    
//    private void storePending() throws IOException {
//        OutputStream os = new FileOutputStream(fileName, appendToFile);
//        for (int i = 0; i < pendingCommands.size(); i++) {
//            //os.write(pendingCommands.get(i).getRawData());
//            pendingCommands.get(i).writeAsBytes(os);
//        }
//
//        os.close();
//        appendToFile = true;
//    }
    
    public void runPending() throws SQLException//, IOException
    {
        runPending(false);
    }
    
    public void runPending(boolean beQuite) throws SQLException//, IOException
    {
        if (conn != null)
            execPending(beQuite);
//        if (fileName != null)
//            storePending();
        
        if (pendingCommands.size() >= MAX_PENDING_COMMANDS)
            pendingCommands.trimToSize();
        
        pendingCommands.clear();
    }
    
    private void execPending(boolean beQuite) throws SQLException {
        commandBuffer.clear();
        if (!beQuite)
            if (logger.isInfoEnabled())
                logger.info("runPending: start, command count = " + pendingCommands.size());
        boolean autoCommit = conn.getAutoCommit();
        conn.setAutoCommit(false);
        try {
            
            int oneBatchIdx = 0, pendingSize = pendingCommands.size();
            
            while (oneBatchIdx < pendingSize) {
                if (!beQuite)
                    if (logger.isInfoEnabled())
                        logger.info("runPending: clearBatch, oneBatchIdx = " + oneBatchIdx);
                
                stmt.clearBatch();
                        
                if (!beQuite)
                    if (logger.isInfoEnabled())
                        logger.info("runPending: run commands loop, oneBatchIdx = " + oneBatchIdx);
                for (int i = 0; i < RUN_COMMANDS_AT_ONCE && oneBatchIdx < pendingSize; i++) {
                    BatchCommand bc = pendingCommands.get(oneBatchIdx);
                    pendingCommands.set(oneBatchIdx, null);
                    if (bc != null)
                        bc.run();
                    oneBatchIdx++;
                    totalCount++;
                    if (totalCount % 30000 == 0)
                        LameUtils.runGc();
                }
                
                if (!beQuite)
                    if (logger.isInfoEnabled())
                        logger.info("runPending: exec batch, oneBatchIdx = " + oneBatchIdx);
                
                try {
                    //System.out.println("--------- before execute batch");
                    stmt.executeBatch();
                } catch(Exception ex) {
                    if (!conn.getAutoCommit()) {
                        conn.rollback();
                        if (!beQuite)
                            if (logger.isInfoEnabled())
                                logger.info("runPending: manual rollback, oneBatchIdx = " + oneBatchIdx);
                    }
                    logger.error("error in one of commands\n" + LameUtils.mergeWithSepEx(commandBuffer, "\n"));
                    throw new LameRuntimeException(ex);
                }
                
                if (!conn.getAutoCommit()) {
                    conn.commit();
                    if (!beQuite)
                        if (logger.isInfoEnabled())
                            logger.info("runPending: manual commit, oneBatchIdx = " + oneBatchIdx);
                } else
                    if (!beQuite)
                        if (logger.isInfoEnabled())
                            logger.info("runPending: autocommitted, oneBatchIdx = " + oneBatchIdx);
            }
            
        } finally {
            conn.setAutoCommit(autoCommit);
        }
        
        if (!beQuite)
            if (logger.isInfoEnabled())
                logger.info("runPending: done");
    }
    
    private List<String> commandBuffer = new ArrayList<String>();
    
    public void runCommand(String command) throws SQLException {
        //logger.info("add command: " + command);
        //System.out.println("add command: " + command);
        commandBuffer.add(command);
        stmt.addBatch(command);
    }
    
    public int registerCommandPattern() {
        return ++commandPatternCount;
    }
    
//    public static void batchFromFile(String fileName, Connection conn,
//            String tableToReplace, String newTable) throws IOException, SQLException {
//        FileInputStream fis = new FileInputStream(fileName);
//        PrimAsBytesReader r = new PrimAsBytesReader(fis);
//        List<CommandSerializer> css = new ArrayList<CommandSerializer>();
//        PostgresCommandBatcher cb = new PostgresCommandBatcher(conn, null);
//
//        int cntr = 0;
//
//        for (;; cntr++) {
//            if (cntr % 1000 == 0)
//                logger.info("batchFromFile: cntr = " + cntr);
//            Integer csi = (Integer) r.readObject();
//            if (csi == null)
//                break;
//            if (csi < 0) {
//                if (-csi - 1 != css.size())
//                    throw new StreamCorruptedException("unproper new command serializer index " + csi + ", css.size(): " + css.size());
//
//                String commandString = (String) r.readObject();
//                if (tableToReplace != null)
//                    commandString = commandString.replace(tableToReplace, newTable);
//
//                css.add(new CommandSerializer(cb, commandString, (String) r.readObject()));
//                csi = -csi;
//            }
//            if (csi < 1 || csi > css.size())
//                throw new StreamCorruptedException("unproper command serializer index " + csi + ", css.size(): " + css.size());
//            CommandSerializer cs = css.get(csi - 1);
//            Object[] args = r.readObjects(cs.argCount());
//            cs.addCommand(args);
//        }
//        cb.runPending();
//    }
    
    private class PlainBatchCommand implements BatchCommand {
        
        protected String commandText;
        protected boolean runSeparated;
        
        protected PlainBatchCommand(String commandText, boolean runSeparated) {
            this.commandText = commandText;
            this.runSeparated = runSeparated;
        }
        
        public void run() throws SQLException {
            runCommand(commandText);
        }
        
//        public void writeAsBytes(OutputStream outStrm) throws IOException {
//            PrimAsBytesWriter w = new PrimAsBytesWriter(outStrm);
//            w.writeObject(0);
//            w.writeObject(commandText);
//        }
        
        public String runSeparatedText() {
            return runSeparated ? commandText : null;
        }
    }
    
    public void addCommand(String commandText, boolean runSeparated) throws SQLException//, IOException
    {
        add(new PlainBatchCommand(commandText, runSeparated));
    }
    
    public void addCommand(String commandText) throws SQLException//, IOException
    {
        //System.out.println("addCommand: " + commandText);
        addCommand(commandText, false);
    }
}
