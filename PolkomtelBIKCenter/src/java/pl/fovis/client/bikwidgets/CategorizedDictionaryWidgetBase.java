/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.AddOrUpdateItemNameDialog;
import pl.fovis.client.dialogs.AttrOrRoleEditDialog;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import simplelib.BeanWithIntIdBase;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author beata
 */
public abstract class CategorizedDictionaryWidgetBase<ITEM extends BeanWithIntIdBase> {

    protected VerticalPanel vp;
    protected VerticalPanel main;
    protected Map<Integer, ITEM> items = new HashMap<Integer, ITEM>();
    protected Map<Integer, String> category = new HashMap<Integer, String>();
    protected List<Integer> itemsc = new ArrayList<Integer>();
    protected Map<Integer, List<Integer>> itemsInCategory = new HashMap<Integer, List<Integer>>();

    protected abstract int getCategoryIdForItem(ITEM item);

    protected abstract String getCategoryNameForItem(ITEM item);

    protected abstract String getItemNameForItem(ITEM item);

    protected boolean isItemUsedInDrools(ITEM item) {
        if (item instanceof AttributeBean) {
            AttributeBean i = (AttributeBean) item;
            return i.usedInDrools;
        } else {
            return false;
        }
    }

    protected int getItemIdForItem(ITEM item) {
        return item.id;
    }

    protected String getItemTypeForItem(ITEM item) {
        return null;
    }

    public Widget buildWidgets() {
        vp = new VerticalPanel();

        main = new VerticalPanel();
        main.setWidth("100%");

        PushButton saveButton = saveBtn();
        if (saveButton != null) {
            main.add(saveButton);
        }
//        BikCustomButton createAddCategoryButton = createAddCategoryButton();
//        if (createAddCategoryButton != null) {
//            main.add(createAddCategoryButton);
//        }
        Label rulesLb = new Label(I18n.wybierzAtrybut.get() /* I18N:  */ + ":");
        rulesLb.setStyleName("AttrHeadLbl");
        rulesLb.addStyleName("rightTreeLbl");
        main.add(rulesLb);
        safeAddIntoContainer(main, createAddCategoryButton(), false);
        main.add(vp);
        vp.addStyleName("selectRolesScroll");
        return main;
    }

    protected void createButtonsForCategory(HorizontalPanel wrapper, final int categoryId, final String categoryName) {
        safeAddIntoContainer(wrapper, createEditButton(new IContinuation() {
            public void doIt() {
                showEditCategoryDialog(categoryId, categoryName);
            }
        }), true);
        safeAddIntoContainer(wrapper, createDeleteButtonForCategory(categoryId), true);
        safeAddIntoContainer(wrapper, createAddItemButton(categoryId), true);
    }

    protected void createEditAndDeleteButtons(HorizontalPanel wrapper, final int itemId, final String attrName) {
        safeAddIntoContainer(wrapper, createEditButton(new IContinuation() {
            public void doIt() {
                showEditItemDialog(itemId, attrName);
            }
        }), true);
        safeAddIntoContainer(wrapper, createDeleteButton(itemId, attrName), true);
    }

    protected void displayFetchedData(List<ITEM> result) {
        items.clear();
        category.clear();
        vp.clear();
        HorizontalPanel hp;
        String lastItemCatName = "";
        for (ITEM it : result) {
            hp = new HorizontalPanel();

            int categoryId = getCategoryIdForItem(it);
            String categoryName = getCategoryNameForItem(it);
            if (!BIKConstants.METABIKS_META_ATTR_CATEGORY.equalsIgnoreCase(categoryName)) {
                int itemId = getItemIdForItem(it);
                String itemName = getItemNameForItem(it);
                boolean usedInDrools = isItemUsedInDrools(it);
                final String itemTypeForItem = getItemTypeForItem(it);
                String itemnTypeOpt = itemTypeForItem != null ? " [" + itemTypeForItem + "]" : "";

                if (!lastItemCatName.equals(categoryName)) {
                    createOneCategoryWidgets(vp, categoryId, categoryName);
                    vp.add(hp);
                    category.put(categoryId, categoryName);
                    itemsc = new ArrayList<Integer>();
                    itemsInCategory.put(categoryId, itemsc);

                }
                if (!itemName.isEmpty()) {
                    hp = new HorizontalPanel();
                    items.put(itemId, it);
                    createOneItemInCategoryWidgets(vp, itemId, itemName, itemnTypeOpt, usedInDrools);
                    vp.add(hp);
                    itemsc.add(itemId);
                }
                lastItemCatName = categoryName;
            }
        }
    }

    protected int getCategoryIdForItemId(int itemId) {
        ITEM item = items.get(itemId);
        return getCategoryIdForItem(item);
    }

    protected String getItemNameForItemId(int itemId) {
        ITEM item = items.get(itemId);
        return getItemNameForItem(item);
    }

    public abstract void displayData();

    protected Set<String> getItemsNames() {
        Set<String> res = new LinkedHashSet<String>();
        for (ITEM item : items.values()) {
            String itemCaption = getItemNameForItem(item);
            res.add(itemCaption);
//            res.add(getTranslatedItemCaption(itemCaption));
        }
        Window.alert("getItemsNames: " + res);
        return res;
    }

    protected abstract void showAddItemInCategoryDialog(final int categoryId);

    protected void showEditCategoryDialog(final int categoryId, String categoryName) { //edytuj kategorie
        new AddOrUpdateItemNameDialog().buildAndShowDialog(I18n.edytujKategorie.get() /* I18N:  */, categoryName, new ArrayList<String>(category.values()), new IParametrizedContinuation<String>() {
                    public void doIt(String param) {
                        BIKClientSingletons.getService().addOrUpdateCategory(categoryId, param, new StandardAsyncCallback<Integer>("Failed in update Category name" /* I18N: no:err-fail-in */ + ".") {
                            public void onSuccess(Integer result) {
                                refreshData();
                                BIKClientSingletons.showInfo(I18n.edytowanoKategorie.get() /* I18N:  */);
                            }
                        });
                    }
                });
    }

    protected void showEditItemDialog(final int itemId, String attrOrRoleName) {
        int categoryId = getCategoryIdForItemId(itemId);
        new AttrOrRoleEditDialog().buildAndShowDialog(category, categoryId, attrOrRoleName,
                getItemsNames(/*new ArrayList<ITEM>(items.values())*/), new IParametrizedContinuation<Pair<Integer, String>>() {
                    public void doIt(Pair<Integer, String> param) {
                        updateItemInCategory(itemId, param.v2, param.v1);
                        refreshData();
                    }
                });
    }

    protected abstract void updateItemInCategory(int itemId, String name, int categoryId);

    protected abstract Widget createOptCheckboxForCategoryWidget(int catId);

    protected abstract Widget createOptCheckboxForItemWidget(int attrId);

    protected void createOneCategoryWidgets(Panel container, final int categoryId, final String categoryName) {
        HorizontalPanel wrapper = new HorizontalPanel();
        wrapper.setStyleName("AttrHeadLbl");

        Widget cb = createOptCheckboxForCategoryWidget(categoryId);
        if (cb != null) {
            wrapper.add(cb);
        }
        Label lbl = new Label(categoryName);
        wrapper.add(lbl);
        createButtonsForCategory(wrapper, categoryId, categoryName);
        container.add(wrapper);
    }

//    protected String getTranslatedItemCaption(String itemCaption) {
//        return itemCaption;
//    }
    protected void createOneItemInCategoryWidgets(Panel container, final int itemId, final String attrName, String typeOpt, boolean usedInDrools) {
        HorizontalPanel wrapper = new HorizontalPanel();
        wrapper.setStyleName("AttrLbl");

        Widget cb = createOptCheckboxForItemWidget(itemId);
        if (cb != null) {
            wrapper.add(cb);
        }
//        Label lbl = new Label(attrName + typeOpt);
        HTML lbl = usedInDrools ? new HTML("<b>" + attrName + typeOpt + "</b>") : new HTML(attrName + typeOpt);
        wrapper.add(lbl);
        createEditAndDeleteButtons(wrapper, itemId, attrName);
        safeAddIntoContainer(wrapper, createOptEditHintAttrButton(attrName), true);
        container.add(wrapper);
    }

    protected void safeAddIntoContainer(Panel container, Widget w, boolean showOnlyOnHover) {
        if (w != null) {
            container.add(w);
            if (showOnlyOnHover) {
                BIKClientSingletons.setupDisplayOnHover(w);
            }
        }
    }

    protected abstract void showDeleteItemConfirmDialog(final int itemId);

    protected abstract String getAddItemButtonCaption();

    protected PushButton createEditButton(final IContinuation showEditDialogCont) {

        PushButton editButton = new PushButton(new Image("images/edit_gray.png" /* I18N: no */), new Image("images/edit_yellow.png" /* I18N: no */));
        editButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                showEditDialogCont.doIt();
            }
        });
        editButton.setStyleName("Attr" /* I18N: no */ + "-editBtn");
        editButton.setTitle(I18n.edytuj.get() /* I18N:  */);
        return editButton;
    }

    protected BikCustomButton createAddItemButton(final int categoryId) {
        return new BikCustomButton(getAddItemButtonCaption(), "CategorizedDict-add",
                new IContinuation() {
                    public void doIt() {
                        showAddItemInCategoryDialog(categoryId);
                    }
                });
    }

    protected PushButton createDeleteButton(final int attrOrRoleId, final String name) {
        PushButton deleteBtn = createDeleteButtonClean();
        deleteBtn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                showDeleteItemConfirmDialog(attrOrRoleId);
            }
        });
        return deleteBtn;
    }

    protected BikCustomButton createAddCategoryButton() {
        return new BikCustomButton(I18n.dodajKategorie.get() /* I18N:  */, "CategorizedDict-add",
                new IContinuation() {
                    public void doIt() {
                        showAddCategoryDialog();
                    }
                });
    }

    protected void showAddCategoryDialog() { //dodaj kategorie
        new AddOrUpdateItemNameDialog().buildAndShowDialog(I18n.dodajKategorie.get() /* I18N:  */, null, new ArrayList<String>(category.values()), new IParametrizedContinuation<String>() {
                    public void doIt(String param) {
                        BIKClientSingletons.getService().addOrUpdateCategory(null, param, new StandardAsyncCallback<Integer>("Failed in addOrUpdate Category name" /* I18N: no:err-fail-in */ + ".") {
                            public void onSuccess(Integer result) {
                                refreshData();
                                BIKClientSingletons.showInfo(I18n.dodanoKategorie.get() /* I18N:  */);
                            }
                        });
                    }
                });
    }

    protected PushButton createDeleteButtonForCategory(final int categoryId) {
        PushButton deleteBtn = createDeleteButtonClean();
        deleteBtn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                new ConfirmDialog().buildAndShowDialog(I18n.czyNaPewnoUsunacKategoriUsuniesz.get() /* I18N:  */ + "!",
                        new IContinuation() {
                            public void doIt() {

                                BIKClientSingletons.getService().deleteCategory(categoryId, new StandardAsyncCallback<Void>("Failure on" /* I18N: no */ + " deleteCategory") {
                                    public void onSuccess(Void result) {
                                        BIKClientSingletons.showInfo(I18n.usunietoKategorie.get() /* I18N:  */);
                                        refreshData();
                                    }
                                });
                            }
                        });
            }
        });
        return deleteBtn;
    }

    protected abstract void refreshData();

    protected PushButton saveBtn() {
        return null;
    }

    public PushButton createOptEditHintAttrButton(String name) {
        return null;
    }

    protected PushButton createDeleteButtonClean() {
        PushButton deleteBtn = new PushButton(new Image("images/trash_gray.gif" /* I18N: no */), new Image("images/trash_red.png" /* I18N: no */));
        deleteBtn.addStyleName("zeroBtn");
        deleteBtn.setWidth("16px");
        deleteBtn.setTitle(I18n.usun.get() /* I18N:  */);
        return deleteBtn;
    }

}
