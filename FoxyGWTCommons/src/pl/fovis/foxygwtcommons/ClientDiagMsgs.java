/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DialogBox.Caption;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.trzy0.foxy.commons.FoxyCommonConsts;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.LameBag;
import simplelib.SimpleDateUtils;
import simplelib.StackTraceUtil;

/**
 *
 * @author wezyr
 */
// i18n-default: no
public class ClientDiagMsgs {

    public static int maxDiagMsgsLines = 1000;
    private static List<String> lastDiagMsgs = new ArrayList<String>();
    protected static Set<String> declaredAllowedShowDiagKinds = new HashSet<String>();
    protected static Set<String> declaredDisallowedShowDiagKinds = new HashSet<String>();
    protected static Set<String> allowedShowDiagKinds = new HashSet<String>();
    protected static Set<String> disallowedShowDiagKinds = new HashSet<String>();
    protected static LameBag<String> allowedKindsCounters = new LameBag<String>();
    public static final char SHOW_DIAG_SUBKIND_SEP = ':';
    private static boolean clientDiagMsgsEnabled = false;
    private static DialogBox dialogBox;
    private static DiagMsgsWidget diagMsgsWidget;
    public static final String APP_MSG_KIND = "<APP>";
    protected static IParametrizedContinuation<Map<String, String>> savePropsCont;

//    protected static boolean isSpecialKind(String kind) {
//        return BaseUtils.safeEquals(kind, APP_MSG_KIND);
//    }
    public static void showDiagMsg(String kind, String msg) {
        //Window.alert(msg);
        if (!isShowDiagEnabledByKind(kind)) {
            String errMsg = "showDiagMsg not allowed for kind" /* I18N: no */ + ": " + kind + "\n" + ""
                    + "with msg" /* I18N: no */ + ":\n\n" + msg;
            Window.alert(errMsg);
            throw new RuntimeException(errMsg);
        }

        if (!clientDiagMsgsEnabled) {
            return; // tutaj mogą dojść typy APP_MSG_KIND, a wtedy wychodzimy po cichu
        }

        trimDiagMsgsLines();

        String formattedMsg = SimpleDateUtils.toCanonicalStringWithMillis(new Date()) + ": ["
                + kind + "] " + msg;

        lastDiagMsgs.add(formattedMsg);

        allowedKindsCounters.add(kind);

        updateDialogIfVisible();
    }

    public static void setAllowedShowDiagKinds(Collection<String> kinds) {
        declaredAllowedShowDiagKinds = new HashSet<String>();
        allowedShowDiagKinds = new HashSet<String>();
        if (kinds != null) {
            declaredAllowedShowDiagKinds.addAll(kinds);
            allowedShowDiagKinds.addAll(kinds);
        }
        //allowedShowDiagKinds.add(APP_MSG_KIND);
    }

    public static void setDisallowedShowDiagKinds(Collection<String> kinds) {
        declaredDisallowedShowDiagKinds = new HashSet<String>();
        disallowedShowDiagKinds = new HashSet<String>();
        if (kinds != null) {
            declaredDisallowedShowDiagKinds.addAll(kinds);
            disallowedShowDiagKinds.addAll(kinds);
        }
    }

    public static void setClientDiagMsgsEnabled(boolean val) {
        clientDiagMsgsEnabled = val;
    }

    protected static boolean isShowDiagEnabledByKind(String kind) {
        if (disallowedShowDiagKinds.contains(kind)) {
            return false;
        }

        if (allowedShowDiagKinds.contains(kind)) {
            return true;
        }

        int kindLen = kind.length();

        if (isImpliedAsSubkind(disallowedShowDiagKinds, kindLen, kind)) {
            return false;
        }

        if (isSubkind(APP_MSG_KIND, kindLen, kind, allowedShowDiagKinds)) {
            return true;
        }

        if (isImpliedAsSubkind(allowedShowDiagKinds, kindLen, kind)) {
            return true;
        }

        disallowedShowDiagKinds.add(kind);
        updateDialogIfVisible();
        return false;
    }

    public static boolean isShowDiagEnabled(String kind) {
        if (!clientDiagMsgsEnabled) {
            return false;
        }

        return isShowDiagEnabledByKind(kind);
    }

    protected static boolean isImpliedAsSubkind(Collection<String> kinds, int kindLen, String kind) {
        for (String definedKind : kinds) {
            if (isSubkind(definedKind, kindLen, kind, kinds)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isSubkind(String definedKind, int kindLen, String kind, Collection<String> kinds) {
        int allowedShowDiagKindLen = definedKind.length();
        if ((allowedShowDiagKindLen == 1 && definedKind.charAt(0) == '*')
                || (kindLen >= allowedShowDiagKindLen + 2
                && kind.startsWith(definedKind)
                && kind.charAt(allowedShowDiagKindLen) == SHOW_DIAG_SUBKIND_SEP)) {

            kinds.add(kind);
            updateDialogIfVisible();

            return true;
        } else {
            return false;
        }
    }

//    private static boolean isAllowedAsSubkind(String allowedShowDiagKind, int kindLen, String kind) {
//        boolean res = isSubkind(allowedShowDiagKind, kindLen, kind);
//
//        if (res) {
//            allowedShowDiagKinds.add(kind);
//            updateDialogIfVisible();
//        }
//        return res;
////
////        int allowedShowDiagKindLen = allowedShowDiagKind.length();
////        if ((allowedShowDiagKindLen == 1 && allowedShowDiagKind.charAt(0) == '*')
////                || (kindLen >= allowedShowDiagKindLen + 2
////                && kind.startsWith(allowedShowDiagKind)
////                && kind.charAt(allowedShowDiagKindLen) == SHOW_DIAG_SUBKIND_SEP)) {
////            return true;
////        }
////        return false;
//    }
    private static void updateDialogIfVisible() {
        if (diagMsgsWidget != null) {
            diagMsgsWidget.setDeclaredAllowedKinds(declaredAllowedShowDiagKinds);
            diagMsgsWidget.setDeclaredDisallowedKinds(declaredDisallowedShowDiagKinds);
            diagMsgsWidget.setDiagMsgs(lastDiagMsgs);
            //diagMsgsWidget.setAllowedKinds(allowedShowDiagKinds);
            diagMsgsWidget.setAllowedKindsInUseCounters(allowedKindsCounters);
            diagMsgsWidget.setDisallowedKindsInUse(disallowedShowDiagKinds);
        }
    }

    public static void clearDiagMsgs() {
        lastDiagMsgs.clear();
        allowedKindsCounters.clear();
        updateDialogIfVisible();
    }

    public static void showDiagMsgsInDialog() {
        if (!clientDiagMsgsEnabled) {
            return;
        }

        if (dialogBox == null) {
            Caption caption = new DialogCaptionWithCloseButton("diagMsgs", new IContinuation() {

                public void doIt() {
                    diagMsgsWidget = null;
                    dialogBox.hide();
                    dialogBox = null;
                }
            });
            dialogBox = new DialogBox(false, false, caption);
//            FlexTable mainDialogBody = new FlexTable();
//            mainDialogBody.setText(0, 0, "diagMsgs:");
//
//            TextArea msgsLines = new TextArea();
//            String msgsAsTxt = BaseUtils.mergeWithSepEx(lastDiagMsgs, "\n");
//            msgsLines.setText(msgsAsTxt);
//
//            mainDialogBody.setWidget(0, 1, msgsLines);
            diagMsgsWidget = new DiagMsgsWidget(savePropsCont);
            updateDialogIfVisible();
            diagMsgsWidget.setWidth((Window.getClientWidth() - 60) + "px");
            dialogBox.setWidget(diagMsgsWidget);

//            dialogBox.setSize((Window.getClientWidth() - 100) + "px",
//                    (Window.getClientHeight() - 100) + "px");
//            dialogBox.setSize("100%",
//                    "100%");
            dialogBox.show();
        }
    }

    private static void trimDiagMsgsLines() {
        while (lastDiagMsgs.size() > maxDiagMsgsLines && !lastDiagMsgs.isEmpty()) {
            lastDiagMsgs.remove(0);
        }
    }

    public static void setSavePropsCont(IParametrizedContinuation<Map<String, String>> savePropsCont) {
        ClientDiagMsgs.savePropsCont = savePropsCont;
    }

    public static void showException(String msg, Throwable ex) {
        String txt = msg + "\n" + StackTraceUtil.getCustomStackTrace(ex);
        System.out.println("showException: " + txt);
        if (ClientDiagMsgs.isShowDiagEnabled(FoxyCommonConsts.DIAG_MSG_KIND$GLOBAL_UNCAUGHT_EXCEPTIONS)) {
            ClientDiagMsgs.showDiagMsg(FoxyCommonConsts.DIAG_MSG_KIND$GLOBAL_UNCAUGHT_EXCEPTIONS, txt);
        }
    }
}
