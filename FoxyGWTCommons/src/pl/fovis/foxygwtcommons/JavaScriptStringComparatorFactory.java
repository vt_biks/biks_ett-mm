/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import simplelib.SimpleStringComparator;
import simplelib.BaseUtils;

/**
 *
 * @author tflorczak
 */
public class JavaScriptStringComparatorFactory {

    private static boolean isFactorySet = false;

    public static void setFactory() {
        if (!isFactorySet) {
            isFactorySet = true;
            BaseUtils.setStringComparator(SimpleStringComparator.getInstance());
        }
    }
}
