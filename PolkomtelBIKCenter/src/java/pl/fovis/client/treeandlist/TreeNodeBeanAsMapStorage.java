/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.treeandlist;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HTML;
import java.util.HashMap;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.EntityDetailsPaneDialog;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.fovis.foxygwtcommons.treeandlist.ITreeGridCellWidgetBuilder;
import pl.fovis.foxygwtcommons.treeandlist.MapTreeRowAndBeanStorageBase;

/**
 *
 * @author wezyr
 */
public class TreeNodeBeanAsMapStorage extends MapTreeRowAndBeanStorageBase<Integer, TreeNodeBean> {

    protected boolean isWidgetRunFromDialog;

    public TreeNodeBeanAsMapStorage(ITreeGridCellWidgetBuilder<TreeNodeBean> optCellWidgetBuilder,
            boolean isWidgetRunFromDialog) {
        super(ID_FIELD_NAME, PARENT_ID_FIELD_NAME, optCellWidgetBuilder);
        this.isWidgetRunFromDialog = isWidgetRunFromDialog;
    }

    @Override
    protected Map<String, Object> convertBeanToRow(final TreeNodeBean node) {
        Map<String, Object> row = new HashMap<String, Object>();
        row.put(ID_FIELD_NAME, node.id);
        row.put(PARENT_ID_FIELD_NAME, node.parentNodeId);
        //row.put(NAME_FIELD_NAME, node.name);
        putColValueWithConvertion(row, node, NAME_FIELD_NAME, node.name);
        //row.put(PRETTY_KIND_FIELD_NAME, BIKClientSingletons.getNodeKindCaptionByCode(node.nodeKindCode));
        putColValueWithConvertion(row, node, PRETTY_KIND_FIELD_NAME, BIKClientSingletons.getNodeKindCaptionByCode(node.nodeKindCode));

        row.put(ICON_FIELD_NAME, "images" /* I18N: no */ + "/" + BIKClientSingletons.getNodeKindIconNameByCode(node.nodeKindCode, node.treeCode) + "." + "gif" /* I18N: no */);
        if (node.linkedNodeId != null || node.isFromLinkedSubtree) {
            if (node.linkedNodeId != null && BIKClientSingletons.isStarredUserNode(node.linkedNodeId)) {
                row.put(ADDITIONAL_ICON_FIELD_NAME, "images/arrowWithStar.png" /* I18N: no */);
            } else {
                row.put(ADDITIONAL_ICON_FIELD_NAME, "images/arrows.png" /* I18N: no */);
            }
        } else if (BIKClientSingletons.isStarredUserNode(node.id)) {
            row.put(ADDITIONAL_ICON_FIELD_NAME, "images/star.png" /* I18N: no */);
        } else {
            row.put(ADDITIONAL_ICON_FIELD_NAME, null);
        }
        if (node != null && node.id > 0) {
            row.put(VIEW_NODE_FIELD_NAME, BIKClientSingletons.createActionButton("loupe" /* I18N: no */, I18n.podglad.get() /* I18N:  */, new ClickHandler() {
                        public void onClick(ClickEvent event) {
                            EntityDetailsPaneDialog edpd = new EntityDetailsPaneDialog();
                            edpd.buildAndShowDialog(node.id, node.linkedNodeId == null ? null : node.parentNodeId, node.treeCode);

//                        EntityDetailsPaneDialog edpd = new EntityDetailsPaneDialog();
//                        edpd.buildAndShowDialog(node.id, node.linkedNodeId == null ? null : node.parentNodeId, node.linkedNodeId, node.treeCode);
                        }
                    }));

            row.put(FOLLOW_FIELD_NAME, BIKClientSingletons.createActionButton("link_arrows", I18n.przejdz.get() /* I18N:  */, new ClickHandler() {
                        public void onClick(ClickEvent event) {
                            if (isWidgetRunFromDialog) {
                                BaseActionOrCancelDialog.hideAllDialogs();
                            }
                            BIKClientSingletons.getTabSelector().submitNewTabSelection(node.treeCode, node.id, true);
                        }
                    }));
        }
        HTML emptyHTML = new HTML("&" + "nbsp" /* I18N: no */ + ";");
        emptyHTML.addStyleName("followBtn");
        row.put(EMPTY_COLUMN_FIELD_NAME, emptyHTML);
        return row;
    }
}
