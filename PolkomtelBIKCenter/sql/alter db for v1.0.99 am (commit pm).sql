﻿exec sp_check_version '1.0.99';
go

------------------------------------------------------------------------------------------------------
alter table bik_role_for_node
	add attr_category_id int null references bik_attr_category(id);
go
------------------------------------------------------------------------------------------------------
declare @attr_category_id int;
select @attr_category_id = id
from bik_attr_category
where name = 'Ogólne'

update bik_role_for_node
set attr_category_id = @attr_category_id
go
------------------------------------------------------------------------------------------------------
alter table bik_role_for_node
	alter column attr_category_id int not null
go
------------------------------------------------------------------------------------------------------
alter table bik_user_in_node
	add role_for_node_id int null references bik_role_for_node(id)
go
------------------------------------------------------------------------------------------------------
update bik_user_in_node
set role_for_node_id =  bik_role_in_node_kind.role_id
from bik_user_in_node join bik_role_in_node_kind on bik_user_in_node.role_in_kind_id = bik_role_in_node_kind.id
go
------------------------------------------------------------------------------------------------------
alter table bik_user_in_node
	alter column role_for_node_id int not null
go
------------------------------------------------------------------------------------------------------
/*
declare @fk_name sysname
select @fk_name = foreignKey_name from dbo.vw_ww_foreignkeys
where Table_Name = 'bik_user_in_node' and name = 'role_in_kind_id'
exec ('alter table bik_user_in_node drop constraint ' + @fk_name)
go
alter table bik_user_in_node
	drop column role_in_kind_id
go
drop table bik_role_in_node_kind;
go

alter table bik_user_in_node
	add constraint UQ_user_id_node_id_role_for_node_id   unique(user_id, node_id, role_for_node_id)
go
*/
alter table bik_user_in_node
	add inherit_to_descendants int not null default 0
go


------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
exec sp_update_version '1.0.99', '1.0.99.1';
go
