/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.gwtservlet;

import java.util.LinkedHashSet;
import java.util.Set;
import pl.fovis.foxygwtcommons.i18nsupport.LocaleNegotiationResultBean;
import pl.fovis.foxygwtcommons.i18nsupport.ServerSideI18nSupportingService;
import pl.trzy0.foxy.serverlogic.IFoxySessionPrimitivesProvider;
import simplelib.BaseUtils;

/**
 *
 * @author pmielanczuk
 */
public abstract class ServerSideI18nSupportingServiceImplBase implements ServerSideI18nSupportingService {

    private final boolean isDumpInfoEnabled = false;

    protected IFoxySessionPrimitivesProvider sessionPrimitivesProvider;

    public ServerSideI18nSupportingServiceImplBase(IFoxySessionPrimitivesProvider sessionPrimitivesProvider) {
        this.sessionPrimitivesProvider = sessionPrimitivesProvider;
    }

    private void dumpInfo(String info) {
        if (isDumpInfoEnabled) {
            System.out.println("setSessionLocaleName: " + info);
        }
    }

    @Override
    public LocaleNegotiationResultBean setSessionLocaleName(String localeName, Set<String> clientSupportedLocaleNames) {
//                    String acceptLanguage = BaseUtils.safeToLowerCase(getThreadLocalRequest().getHeader("Accept-Language"));
        String acceptLanguage = BaseUtils.safeToLowerCase(sessionPrimitivesProvider.getRequestHeader("Accept-Language"));
//        acceptLanguage = BaseUtils.safeToLowerCase(acceptLanguage);
        if (isDumpInfoEnabled) {
            dumpInfo("localeName=" + localeName);
            dumpInfo("clientSupportedLocaleNames=" + clientSupportedLocaleNames);
            dumpInfo("acceptLanguage=" + acceptLanguage);
        }
        Set<String> serverSupportedLocaleNames = getServerSupportedLocaleNames();
        if (isDumpInfoEnabled) {
            dumpInfo("serverSupportedLocaleNames=" + serverSupportedLocaleNames);
        }
        if (BaseUtils.isStrEmptyOrWhiteSpace(localeName)) {
            // klient nie sugeruje języka - trzeba wybrać jakiś, najlepiej
            // taki, który jest preferowany w przeglądarce i wspierany
            // na serwerze i kliencie jednocześnie
            Set<String> commonLocales = new LinkedHashSet<String>(clientSupportedLocaleNames);
            if (serverSupportedLocaleNames != null) {
                commonLocales.retainAll(serverSupportedLocaleNames);
            }
            if (isDumpInfoEnabled) {
                dumpInfo("commonLocales=" + commonLocales);
            }
            if (commonLocales.isEmpty()) {
                // słabość czuję, brak wspólnych języków, trudno
                // w taki razie dla klienta weźmy najlepszy wg przeglądarki
                // z tych, jakie sam klient obsługuje
                localeName = BaseUtils.safeGetFirstItem(clientSupportedLocaleNames, "?");
                localeName = BaseUtils.getBestAcceptedAndSupportedLanguage(acceptLanguage, clientSupportedLocaleNames, localeName);
            } else {
                // jest przynajmniej jeden wspólny (pierwszy wspólny będzie domyślny)
                localeName = BaseUtils.safeGetFirstItem(commonLocales, null);
                localeName = BaseUtils.getBestAcceptedAndSupportedLanguage(acceptLanguage, commonLocales, localeName);
            }
            if (isDumpInfoEnabled) {
                dumpInfo("after fixing localeName=" + localeName);
            }
        }
        String localeForServer;
        if (serverSupportedLocaleNames != null) {
            // sprawdzamy, czy serwer obsługuje język wybrany dla (lub zasugerowany przez) klienta
            if (!serverSupportedLocaleNames.contains(localeName)) {
                // no mamy pecha - serwer nie obsługuje, zatem wybierzemy dla
                // serwera język sugerowany przez przeglądarkę
                localeForServer = BaseUtils.getBestAcceptedAndSupportedLanguage(acceptLanguage, serverSupportedLocaleNames, BaseUtils.safeGetFirstItem(serverSupportedLocaleNames, "?"));
            } else {
                localeForServer = localeName;
            }
        } else {
            localeForServer = "?"; // serwerowi wszystko jedno
        }
        if (isDumpInfoEnabled) {
            dumpInfo("after localeForServer=" + localeForServer);
        }
//        HttpSession session = getThreadLocalSession();
//        session.setAttribute(FOXY_LOCALE_LANG_NAME_ATTR_KEY, localeForServer);
        sessionPrimitivesProvider.setSessionAttribute(FoxyGWTServiceServlet.FOXY_LOCALE_LANG_NAME_ATTR_KEY, localeForServer);
        if (isDumpInfoEnabled) {
            dumpInfo("session id=" + sessionPrimitivesProvider.getSessionId());
        }
        LocaleNegotiationResultBean res = new LocaleNegotiationResultBean();
        res.clientLocaleName = localeName;
        res.serverLocaleName = localeForServer;
        res.serverSupportedLocaleNames = serverSupportedLocaleNames;
        return res;
    }

}
