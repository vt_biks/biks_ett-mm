/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.BIKClientSingletons;

/**
 *
 * @author bfechner
 */
public class UserRankPage extends BikPageMinimalBase {

    protected VerticalPanel vp;

    @Override
    protected Widget buildWidgets() {
        vp = new VerticalPanel();
        show(vp);
        return vp;
    }

    @Override
    public String getId() {
        return "Rank" /* i18n: no */;
    }

    @Override
    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
        show(vp);
    }

    public void show(final VerticalPanel vp) {
        UserRankPageTab mr = new UserRankPageTab();
        mr.show(vp);
    }

    @Override
    public boolean isPageEnabled() {
        return super.isPageEnabled() && !BIKClientSingletons.isDisableRankingPage();
    }
}
