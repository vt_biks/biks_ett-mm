/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import simplelib.BaseUtils;

/**
 *
 * @author wezyr
 */
public class RepoEntity implements Serializable {

    public String siId;
    public String siParentid;
    public String siName;
    public String siKind;
    public String baId;

    public RepoEntity() {
    }

    public RepoEntity(String siId, String siParentid, String siName, String siKind) {
        this.siId = siId;
        this.siParentid = siParentid;
        this.siName = siName;
        this.siKind = siKind;
    }

    public RepoEntity(int siId, Integer siParentid, String siName, String siKind) {
        this.siId = Integer.toString(siId);
        this.siParentid = BaseUtils.safeToString(siParentid);
        this.siName = siName;
        this.siKind = siKind;
    }

    public RepoEntity(int siId, String siParentid, String siName, String siKind, String baId) {
        this.siId = Integer.toString(siId);
        this.siParentid = BaseUtils.safeToString(siParentid);
        this.siName = siName;
        this.siKind = siKind;
        this.baId = baId;
    }
}
