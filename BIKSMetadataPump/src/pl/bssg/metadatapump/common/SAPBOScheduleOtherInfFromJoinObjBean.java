/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.common;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author bfechner
 */
public class SAPBOScheduleOtherInfFromJoinObjBean {

    public String objId;
//    public String path;
    public Date modifyTimeReport;
    public Date modifyTimeUniverse;
    public Map<String, String> modifyTimeUniverses;
    public String universe;
    public List<String> universes;
    public String connection;
    public List<String> connections;
    public String databaseEngine;
    public Map<String, String> databaseEngines;

}
