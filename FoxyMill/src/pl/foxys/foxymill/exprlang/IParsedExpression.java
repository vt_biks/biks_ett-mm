/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.foxys.foxymill.exprlang;

import pl.foxys.foxymill.renderctx.IRenderCtx;

/**
 *
 * @author wezyr
 */
public interface IParsedExpression {
    public Object eval(//IRenderContext
            IRenderCtx renderCtx);
}
