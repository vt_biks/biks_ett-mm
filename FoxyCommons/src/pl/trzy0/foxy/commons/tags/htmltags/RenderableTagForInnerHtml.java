/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.commons.tags.htmltags;

import java.util.Collection;
import java.util.Map;

/**
 *
 * @author wezyr
 */
public class RenderableTagForInnerHtml implements IRenderableTag {

    private String innerHtml;
    
    public RenderableTagForInnerHtml(String innerHtml) {
        this.innerHtml = innerHtml;
    }
    
    public String getTagName() {
        return innerHtml;
    }

    public Map<String, String> getTagAttrs() {
        return null;
    }

    public boolean isTextTag() {
        return true;
    }

    public boolean isClosedInPlace() {
        return false;
    }

    public Collection<IRenderableTag> getRenderableSubTags() {
        return null;
    }
}
