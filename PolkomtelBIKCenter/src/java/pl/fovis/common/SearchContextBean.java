/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.common.sbavc.ISearchByAttrValueCriteria;

/**
 *
 * @author ctran
 */
public class SearchContextBean {

    public String query;
    public Set<Integer> treeIds;
    public Map<Integer, Integer> searchKindAndCounts;
    public Set<Integer> kindsToSearchFor;
    public Set<Integer> kindIdsToExclude;
    public boolean calcKindCounts;
    public List<ISearchByAttrValueCriteria> sbavcs;
    public int allCnt;
}
