/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.RadioButton;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.MenuNodeBean;
import pl.fovis.common.TreeBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;
import simplelib.IContinuation;

/**
 *
 * @author pmielanczuk
 */
public class ApplyForExtendedAccessTreeOfTrees extends TreeOfTrees {

    protected final Integer NODE_ACTION_ID_SHOW_IN_TREE = BIKClientSingletons.getNodeActionIdByCode(BIKConstants.NODE_ACTION_CODE_SHOW_IN_TREE);
    protected final Integer NODE_ACTION_ID_NEW_NESTABLE_PARENT = BIKClientSingletons.getNodeActionIdByCode("NewNestableParent");
    protected final String NODE_ACTION_APPLY_FOR_EXTENDED_ACCESS = "#ApplyForExtendedAccess"; // + przerobić wyświetlanie drzewka!
    protected final boolean isEffectiveExpertLoggedIn = BIKClientSingletons.isEffectiveExpertLoggedInEx(NODE_ACTION_APPLY_FOR_EXTENDED_ACCESS);
    protected final List<CheckBox> treeCheckboxes = new ArrayList<CheckBox>();
    protected final List<RadioButton> roAccessRbs = new ArrayList<RadioButton>();
    protected final List<RadioButton> rwAccessRbs = new ArrayList<RadioButton>();
    protected final List<MenuNodeBean> menuNodeBeans = new ArrayList<MenuNodeBean>();
    protected final IContinuation setProperMailToPartCont;
    protected int row = 0;

    public ApplyForExtendedAccessTreeOfTrees(IContinuation setProperMailToPartCont) {
        this.setProperMailToPartCont = setProperMailToPartCont;
    }

    @Override
    protected Collection<TreeBean> getTrees() {

        List<TreeBean> res = new ArrayList<TreeBean>();

        if (BIKClientSingletons.isAppAdminLoggedIn(BIKConstants.ACTION_OTHER_ACTIONS_APP_ADMIN)) {
            return res;
        }

        Collection<TreeBean> allTrees = BIKClientSingletons.getAllTrees();

        for (TreeBean tb : allTrees) {

            boolean isRegularUserTree = BIKClientSingletons.isRegularUserTree(tb);
            final int treeId = tb.id;

            if (isRegularUserTree) {
                if (isEffectiveExpertLoggedIn || BIKClientSingletons.isLoggedUserAuthorOfTree(treeId) || BIKClientSingletons.isLoggedUserCreatorOfTree(treeId)) {
                    continue;
                }

                if (tb.nodeKindId == null && BIKClientSingletons.isNodeActionGrantedInTreeRoot(treeId, NODE_ACTION_ID_SHOW_IN_TREE)) {
                    continue;
                }

            } else {
                if (BIKClientSingletons.isNodeActionGrantedInTreeRoot(treeId, NODE_ACTION_ID_NEW_NESTABLE_PARENT)) {
                    continue;
                }
            }

            res.add(tb);
        }

        return res;
    }

    @Override
    protected boolean mustAddUltimateRootToMenuTree() {
        return false;
    }

    @Override
    protected List<String> getFieldsToDisplay() {
        List<String> res = super.getFieldsToDisplay();
        res.add("rbRo");
        res.add("rbRw");
        return res;
    }

    @Override
    protected Object createValueForNameColumn(final MenuNodeBean mnb) {
        if (mnb.treeId != null) {
            return super.createValueForNameColumn(mnb);
        }

        InlineHTML res = new InlineHTML("<b>" + BaseUtils.encodeForHTMLTag(mnb.name) + "</b>");
        res.setStyleName("cellInner");

        return res;
    }

    @Override
    protected Map<String, Object> convertBeanToRow(MenuNodeBean mnb) {
        Map<String, Object> res = super.convertBeanToRow(mnb);

        Object cbObj = res.get("name");
        if (!(cbObj instanceof CheckBox)) {
            return res;
        }

        final CheckBox cb = (CheckBox) cbObj;
        final RadioButton rbRo = new RadioButton("rb" + row, I18n.tylkoOdczyt.get());
        final RadioButton rbRw = new RadioButton("rb" + row, I18n.modyfikacjaDanych.get());
        rbRo.setVisible(false);
        rbRw.setVisible(false);

        final Integer treeId = mnb.treeId;
        final boolean needsReadOnlyAccessInTreeRoot = treeId != null && !BIKClientSingletons.isNodeActionGrantedInTreeRoot(treeId, NODE_ACTION_ID_SHOW_IN_TREE);
        final TreeBean tb = treeId != null ? BIKClientSingletons.getTreeBeanById(treeId) : null;
        final boolean needsModifyAccessInTreeRoot = tb != null && tb.nodeKindId != null;

        cb.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                rbRo.setVisible(needsReadOnlyAccessInTreeRoot && cb.getValue());
                rbRw.setVisible(needsModifyAccessInTreeRoot && cb.getValue());

                setProperMailToPartCont.doIt();
            }
        });

        ClickHandler ch = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                setProperMailToPartCont.doIt();
            }
        };

        rbRo.addClickHandler(ch);
        rbRw.addClickHandler(ch);

        if (needsReadOnlyAccessInTreeRoot) {
            rbRo.setValue(true);
        } else {
            rbRw.setValue(true);
        }

        treeCheckboxes.add(cb);
        roAccessRbs.add(rbRo);
        rwAccessRbs.add(rbRw);
        menuNodeBeans.add(mnb);

        res.put("rbRo", rbRo);
        res.put("rbRw", rbRw);

        row++;

        return res;
    }

    public int getOptionCount() {
        return treeCheckboxes.size();
    }

    public boolean isOptionSelected(int idx) {
        return treeCheckboxes.get(idx).getValue();
    }

    public String getOptionName(int idx) {
        List<String> revPathNames = new ArrayList<String>();

        MenuNodeBean mnb = getOptionMenuNodeBean(idx);

        while (mnb != null) {
            revPathNames.add(mnb.name);
            mnb = storage.getBeanById(mnb.parentNodeId);
        }

        Collections.reverse(revPathNames);

        return BaseUtils.mergeWithSepEx(revPathNames, BIKConstants.RAQUO_STR_SPACED);

//        return treeCheckboxes.get(idx).getText();
    }

    public boolean isReadOnlyAccessForOption(int idx) {
        return roAccessRbs.get(idx).getValue();
    }

    public MenuNodeBean getOptionMenuNodeBean(int idx) {
        return menuNodeBeans.get(idx);
    }
}
