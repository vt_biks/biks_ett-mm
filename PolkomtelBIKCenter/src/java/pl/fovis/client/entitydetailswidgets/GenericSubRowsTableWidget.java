/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Widget;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuationWithReturn;

/**
 *
 * @author tflorczak
 */
public class GenericSubRowsTableWidget {

    protected ISubRowsDisplayBroker broker;

    public GenericSubRowsTableWidget(ISubRowsDisplayBroker broker) {
        this.broker = broker;
    }

    public Widget buildWidget() {
        FlexTable main = new FlexTable();
        main.setStyleName("gridJoinedObj");

        int attrCount = broker.getAttrCount();

        String[] columns = new String[attrCount];
        Integer[] widths = new Integer[attrCount];

        boolean noColNames = true;

        for (int i = 0; i < attrCount; i++) {
            String attrCaption = broker.getAttrCaption(i);
            noColNames = noColNames && BaseUtils.isStrEmpty(attrCaption);

            columns[i] = BaseUtils.nullToDef(attrCaption, "");
            widths[i] = broker.getAttrWidth(i);
        }

        EntityDetailsWidgetFlatBase.createHeaders(main, columns);
        EntityDetailsWidgetFlatBase.setColumnWidth(main, widths);

        int rowCount = broker.getRowCount();

        IParametrizedContinuationWithReturn<Integer, String> rowStyleCalc = broker.getRowStyleCalc();

        for (int i = 0; i < rowCount; i++) {
            Widget[] widgets = broker.makeWidgetsForRow(i);
            EntityDetailsWidgetFlatBase.createRow(main, i + 1, rowStyleCalc, widgets);
        }

        main.setWidth("100%");
        return main;
    }

//        //
//        checkIfDescriptionIsNeeded();
//        checkShouldShowSomeHeader();
//        //
//        int numberOfAttributes = attrDicList.size();
//        String[] columns = new String[numberOfAttributes + (showDescription ? 2 : 1)];
//        int tmpIdx = 0;
//        columns[tmpIdx++] = I18n.nazwaObiektu.get();
//        int numberOfColumn = numberOfAttributes + (showDescription ? 4 : 3);
//        int[] widths = new int[numberOfColumn];
//        int colIdx = 0;
//        boolean hasNoAttributes = BaseUtils.isCollectionEmpty(attrDicList);
//        int attributesWidths = BaseUtils.isCollectionEmpty(attrDicList) ? 0 : 60;
//        widths[colIdx++] = 2; // ikona
//        if (showDescription) {
//            columns[tmpIdx++] = I18n.opis.get();
//            if (hasNoAttributes) {
//                widths[colIdx++] = 51; // nazwa
//                widths[colIdx++] = 45; // description
//            } else {
//                widths[colIdx++] = 26; // nazwa
//                widths[colIdx++] = 20; // description
//                attributesWidths = 50;
//            }
//        } else {
//            widths[colIdx++] = 36 + (hasNoAttributes ? 60 : 0); // nazwa
//        }
//        for (String attr : attrDicList) {
//            columns[tmpIdx++] = attr;
//            widths[colIdx++] = attributesWidths / attrDicList.size();
//        }
//        widths[colIdx++] = 2; // strzlki do przejscia
//        EntityDetailsWidgetFlatBase.createHeaders(main, columns);
//        EntityDetailsWidgetFlatBase.setColumnWidth(main, widths);
//        int indexCount = 1;
//        for (ChildrenBean child : children) {
//            List<AttributeBean> attrs = child.attributes;
//            Widget[] widgets = new Widget[numberOfColumn];
//            int widgetIndex = 0;
//            widgets[widgetIndex++] = BikIcon.createIcon("images/" + child.icon + ".gif" /* I18N: no */, "RelatedObj-ObjIcon");
//            widgets[widgetIndex++] = BIKClientSingletons.createLinkAsAnchor(child.name, child.treeCode, child.nodeId);
//            if (showDescription) {
//                widgets[widgetIndex++] = new Label(child.description);
//            }
//            Map<String, AttributeBean> attrsMap = new HashMap<String, AttributeBean>();
//            if (attrs != null) {
//                for (AttributeBean attr : attrs) {
//                    attrsMap.put(attr.atrName, attr);
//                }
//            }
//            for (String attr : attrDicList) {
//                AttributeBean attrBean = attrsMap.get(attr);
//                boolean displayAsNumber = attrBean != null && attrBean.displayAsNumber;
//                if (attrBean != null && attrBean.typeAttr.equals(AddOrEditAdminAttributeWithTypeDialog.AttributeType.comboBooleanBox.name())) {
//                    widgets[widgetIndex++] = BikIcon.createIcon(attrBean.valueOpt.startsWith(attrBean.atrLinValue) ? "images/check2.png" : "images/delete.png", "RelatedObj-ObjIcon");
//                } else {
//                    String trimmedVal = attrBean == null || attrBean.atrLinValue == null ? "" : attrBean.atrLinValue.trim();
//                    if (BIKClientSingletons.isRenderAttributesAsHTMLMode() && BaseUtils.strHasPrefix(trimmedVal, RENDER_AS_HTML_VAL_PREFIX, false) && BaseUtils.strHasSuffix(trimmedVal, RENDER_AS_HTML_VAL_SUFFIX, false)) {
//                        if (displayAsNumber) {
//                            trimmedVal = BaseUtils.chunkHTMLWithSepSpace(trimmedVal);
//                        }
//                        widgets[widgetIndex++] = new HTML(getHTMLInnerText(trimmedVal));
//                    } else {
//                        if (displayAsNumber) {
//                            trimmedVal = BaseUtils.chunkStringWithSepSpace(trimmedVal);
//                        }
//                        widgets[widgetIndex++] = new Label(trimmedVal);
//                    }
//                }
//            }
//            widgets[widgetIndex++] = BIKClientSingletons.createActionButton("link_arrows", I18n.przejdz.get() /* I18N:  */, EntityDetailsWidgetFlatBase.createFollowCH(child.treeCode, child.nodeId));
//            EntityDetailsWidgetFlatBase.createRow(main, indexCount++, widgets);
//        }
//        main.setWidth("100%");
//        return main;
//    }
//
//    protected void checkShouldShowSomeHeader() {
//        Map<String, Boolean> attrMap = new HashMap<String, Boolean>();
//        for (String attr : attrDicList) {
//            attrMap.put(attr, Boolean.FALSE);
//        }
//        for (ChildrenBean row : children) {
//            List<AttributeBean> attributes = row.attributes;
//            if (attributes == null) {
//                continue;
//            }
//            for (AttributeBean attribute : attributes) {
//                if (attrDicList.contains(attribute.atrName) && !BaseUtils.isStrEmptyOrWhiteSpace(attribute.atrLinValue)) {
//                    attrMap.put(attribute.atrName, Boolean.TRUE);
//                }
//            }
//        }
//        for (Map.Entry<String, Boolean> entrySet : attrMap.entrySet()) {
//            if (!entrySet.getValue()) {
//                attrDicList.remove(entrySet.getKey());
//            }
//        }
//    }
//
//    protected void checkIfDescriptionIsNeeded() {
//        if (showDescription) {
//            showDescription = false;
//            for (ChildrenBean child : children) {
//                if (child != null && !BaseUtils.isHTMLTextEmpty(child.description)) {
//                    showDescription = true;
//                    return;
//                }
//            }
//        }
//    }
}
