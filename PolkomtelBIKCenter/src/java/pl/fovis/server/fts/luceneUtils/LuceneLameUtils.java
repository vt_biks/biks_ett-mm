/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts.luceneUtils;

import commonlib.LameUtils;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.MultiFields;
import org.apache.lucene.index.Term;
import pl.fovis.common.SearchWizardHitResult;
import simplelib.BaseUtils;
import simplelib.ILameCollector;
import simplelib.logging.ILameLogger;

/**
 *
 * @author pmielanczuk
 */
public class LuceneLameUtils {

    private static final ILameLogger logger = LameUtils.getMyLogger();

    public static boolean wildcardWordMatches(String wildcardWord, String word) {
        WildcardWord wcw = WildcardWord.splitWildcardWord(wildcardWord);
        if (wcw == null) {
            return wildcardWord.equals(word);
        }
        return wcw.matches(word);
    }

    public static Set<String> splitIntoWords(String words) {
        return BaseUtils.splitUniqueBySep(BaseUtils.trimAndCompactSpaces(words), " ", true);
    }

    @SuppressWarnings("deprecation")
    public static boolean queryWordsMatch(String queryWords, String words) {
        Set<String> qwSet = splitIntoWords(queryWords);
        QueryWords qw = QueryWords.make(qwSet);
        Set<String> wordsSet = splitIntoWords(words);
        return qw.matches(wordsSet);
    }

    public static Set<String> calcOccurenceInFields(Analyzer analyzer, QueryWords queryWords, Document doc, Set<String> optFieldsToIgnore) {
        try {
//            QueryWords qw = makeQueryWords(queryWords);
            Set<String> occurenceFields = new HashSet<String>();
            for (IndexableField indFld : doc.getFields()) {
                String fieldName = indFld.name();
                if (optFieldsToIgnore != null && optFieldsToIgnore.contains(fieldName)) {
                    continue;
                }
                String fieldVal = indFld.stringValue(); //.toLowerCase();
                Set<String> fieldWords = parseTextByAnalyzerUnique(analyzer, fieldVal, fieldName);

                if (queryWords.matches(fieldWords)) {
                    occurenceFields.add(fieldName);
                }
            }
            return occurenceFields;
        } catch (Exception ex) {
            throw new RuntimeException("error in calcOccurenceInFields", ex);
        }
    }

    public static Set<String> parseTextByAnalyzerUnique(Analyzer analyzer, String text, String fieldName) throws IOException {
        return parseTextByAnalyzer(analyzer, text, fieldName, new HashSet<String>());
    }

    public static QueryWords parseTextByAnalyzerToQueryWords(Analyzer analyzer, String text, String fieldName) throws IOException {
        QueryWords res = new QueryWords();

        Set<String> words = splitIntoWords(text);

        StringBuilder sb = new StringBuilder();
        List<WildcardWord> wildcardWords = new ArrayList<WildcardWord>();

        for (String word : words) {
            WildcardWord wcw = WildcardWord.splitWildcardWord(word);

            if (wcw != null) {
                wildcardWords.add(wcw);
            } else {
                if (sb.length() != 0) {
                    sb.append(" ");
                }
                sb.append(word);
            }
        }

        res.wildcardWords = wildcardWords;
        res.plainWords = parseTextByAnalyzerEx(analyzer, sb.toString(), fieldName);

        return res;
    }

    public static List<String> parseTextByAnalyzer(Analyzer analyzer, String text, String fieldName) throws IOException {
        return parseTextByAnalyzer(analyzer, text, fieldName, new ArrayList<String>());
    }

    public static <C extends Collection<String>> C parseTextByAnalyzer(Analyzer analyzer, String text, String fieldName, final C res) throws IOException {
        parseTextByAnalyzer(analyzer, text, fieldName, new ILameCollector<WordFromAnalyzer>() {

            @Override
            public void add(WordFromAnalyzer item) {
                res.add(item.termText);
            }
        });
        return res;
    }

    public static List<WordWithOtherForms> parseTextByAnalyzerEx(Analyzer analyzer, final String text, String fieldName) throws IOException {
        final List<WordWithOtherForms> res = new ArrayList<WordWithOtherForms>();

        ILameCollector<WordFromAnalyzer> collector = new ILameCollector<WordFromAnalyzer>() {

            Map<String, WordWithOtherForms> collectedWords = new HashMap<String, WordWithOtherForms>();

            @Override
            public void add(WordFromAnalyzer item) {
                String baseWord = text.substring(item.startPos, item.endPos);
                WordWithOtherForms w = collectedWords.get(baseWord);
                if (w == null) {
                    w = new WordWithOtherForms(baseWord);
                    collectedWords.put(baseWord, w);
                    res.add(w);
                }
                w.words.add(item.termText);
            }
        };

        parseTextByAnalyzer(analyzer, text, fieldName, collector);

        return res;
    }

    public static <C extends ILameCollector<WordFromAnalyzer>> C parseTextByAnalyzer(Analyzer analyzer, String text, String fieldName, C collector) throws IOException {

        Reader r = new StringReader(text);
        TokenStream ts = analyzer.tokenStream(fieldName, r);
        try {
//            int tokenCount = 0;
            // for every token
            CharTermAttribute termAtt = ts.addAttribute(CharTermAttribute.class);
            OffsetAttribute offsAttr = ts.addAttribute(OffsetAttribute.class);

            ts.reset();
            while (ts.incrementToken()) {
                String word = termAtt.toString();
//                String originalWord = text.substring(offsAttr.startOffset(), offsAttr.endOffset());
//                System.out.println("  word: " + word + ", start: " + offsAttr.startOffset() + ", end: " + offsAttr.endOffset()
//                        + (!word.equals(originalWord) ? ", original word: " + originalWord : ""));
//                tokenCount++;
//                if (tokenCount > maxNumTokensParsed) {
//                    break;
//                }
//                if (isNoiseWord(word)) {
//                    continue;
//                }
                collector.add(new WordFromAnalyzer(word, offsAttr.startOffset(), offsAttr.endOffset()));
            }
            ts.end();
        } finally {
            ts.close();
        }

        return collector;
    }

    public static Map<Integer, Float> convertSearchHitsToMap(List<SearchWizardHitResult> hits) {
        Map<Integer, Float> res = new LinkedHashMap<Integer, Float>();
        if (hits != null) {
            for (SearchWizardHitResult hit : hits) {
                res.put(hit.nodeId, hit.score);
            }
        }
        return res;
    }

    public static Set<String> fieldsWithWords(IndexReader ir, Collection<WordWithOtherForms> words, Set<String> fieldsToIgnoreForOccurences) {

        Set<String> res = new HashSet<String>();

        Collection<String> fields = MultiFields.getIndexedFields(ir);
        for (String fieldName : fields) {
            if (fieldsToIgnoreForOccurences != null && fieldsToIgnoreForOccurences.contains(fieldName)) {
                continue;
            }

            boolean hasAllWords = true;
            try {
                for (WordWithOtherForms word : words) {

                    boolean hasAny = false;
                    for (String w : word.words) {
                        int freq = ir.docFreq(new Term(fieldName, w));
                        if (freq > 0) {
                            hasAny = true;
                            break;
                        }
                    }

                    if (!hasAny) {
                        hasAllWords = false;
                        break;
                    }
                }
            } catch (IOException ex) {
                throw new RuntimeException("error in fieldsWithWords", ex);
            }

            if (hasAllWords) {
                res.add(fieldName);
            }
        }

        return res;
    }

    public static void main(String[] args) throws IOException {
//        Analyzer analyzer = new MorfologikAnalyzer();
        Analyzer analyzer = new LowercasingMorfologicWithBaseFormAnalyzer();
        System.out.println(parseTextByAnalyzer(analyzer, "Annapurna to nie dziewczyna! Nie ma Annapurnej, to nie są dziewczyny, nie ma dziewczyn, Anny, Annie, annie", "name"));

//        System.out.println(BaseUtils.splitBySep("ann*pur", "*", false));
//        System.out.println(BaseUtils.splitBySep("*nn*pur", "*", false));
//        System.out.println(BaseUtils.splitBySep("*nn*pu*", "*", false));
//        System.out.println(BaseUtils.splitBySep("anna*", "*", false));
//        System.out.println(BaseUtils.splitBySep("anna**", "*", false));
//        System.out.println(BaseUtils.splitBySep("anna**na", "*", false));
//        System.out.println(BaseUtils.splitBySep("anna*na", "*", false));
//        System.out.println(BaseUtils.splitBySep("*purna", "*", false));
//        System.out.println(BaseUtils.splitBySep("*", "*", false));
//        System.out.println(BaseUtils.splitBySep("annapurna", "*", false));
//        System.out.println(matches("anna*", "annapurna"));
//        System.out.println(matches("anna*a", "annapurna"));
//        System.out.println(matches("an*a*a", "annapurna"));
//        System.out.println(matches("*ann*pur*a*", "annapurna"));
//        System.out.println(matches("anna*na", "annana"));
//        System.out.println(matches("anna*ana", "annana"));
//        System.out.println(matches("*na*", "annapurna"));
//        System.out.println(matches("annapurna", "annapurna pies"));
//        System.out.println(matches("kot", "annapurna pies"));
//        System.out.println(matches("pie* pies", "annapurna pies"));
//        System.out.println(matches("pie* pies *s ann*a", "annapurna pies"));
    }
}
