/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.erwin;

/**
 *
 * @author pgajda
 */
public interface IErwinXMLElement<V> {

    public String getElementName();

    public void setPropertieValue(String key, String value);

    public V getPropertieValue(String key);

    public boolean checkIfReadPropertie(String propertyName);
}
