/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import commonlib.*;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import simplelib.*;
import simplelib.logging.ILameLogger;

/**
 *
 * @author wezyr
 */
public class FoxyUrlRewriter implements IOutboundUrlRewriter {

    private static final ILameLogger logger = LameUtils.getMyLogger() //
            //.setLevelForDebugOnly(ILameLogger.LameLogLevel.Debug)
            ;

    public static class RuleConfig {

        public String from;
        public String to;
        public boolean isLast;
        public Pattern compiled;

        public RuleConfig(String from, String to, boolean isLast) {
            this.from = from;
            this.to = to;
            this.isLast = isLast;
        }

        @Override
        public String toString() {
            return "<from=\"" + from + "\" to=\"" + to + "\" isLast=\"" + isLast + "\">";
        }
    }
    private IWatchedResourceFromProvider<InputStream> resource;
    private String fileName;
    private List<RuleConfig> outboundRules;
    private List<RuleConfig> rules;

    public FoxyUrlRewriter(IWatchedResourceFromProvider<InputStream> resource,
            String fileName) {
        this.fileName = fileName;
        this.resource = resource;
    }

    protected synchronized void reinitIfNeeded() {
        boolean hbmr = resource.hasBeenModifiedRecently();
        if (outboundRules != null && !hbmr) {
            if (logger.isDebugEnabled()) {
                logger.debug("no need to reinit - rules are set and no modifications recently");
            }
            return;
        }

        if (logger.isInfoEnabled()) {
            logger.info("needs to reinit, rules are " + (outboundRules != null ? "not " : "")
                    + "null, has been modified recently: " + hbmr);
        }

        outboundRules = new ArrayList<RuleConfig>();
        rules = new ArrayList<RuleConfig>();

        InputStream is = resource.gainResource();
        Map<String, Object> m = XMLMapper.readMap(is, fileName);

        List subNodes = (List) m.get("#subNodes");

        for (Object subNode : subNodes) {
            if (subNode instanceof Map) {
                Map sbm = (Map) subNode;
                if (logger.isDebugEnabled()) {
                    logger.debug("sbm=" + sbm);
                }
                String name = (String) sbm.get("#name");
                if ("outbound-rule".equals(name) || "rule".equals(name)) {
                    String to = null;
                    String from = null;
                    boolean isLast = false;
                    List subSubNodes = (List) sbm.get("#subNodes");
                    for (Object subSubNode : subSubNodes) {
                        if (subSubNode instanceof Map) {
                            Map ssnm = (Map) subSubNode;
                            String nn = (String) ssnm.get("#name");
                            if ("from".equals(nn)) {
                                from = ((List) ssnm.get("#subNodes")).get(0).toString();
                            } else if ("to".equals(nn)) {
                                to = ((List) ssnm.get("#subNodes")).get(0).toString();
                                String isLastStr = (String) ssnm.get("last");
                                isLast = isLastStr != null && isLastStr.trim().equals("true");
                            } else if ("name".equals(nn)) {
                                // no-op
                            } else {
                                if (logger.isDebugEnabled()) {
                                    logger.debug("#### error - unknown node with name: " + nn);
                                }
                            }
                        }
                    }
                    RuleConfig r = new RuleConfig(from, to, isLast);
                    boolean isOutbound = name.equals("outbound-rule");
                    if (isOutbound) {
                        outboundRules.add(r);
                    } else {
                        rules.add(r);
                    }
                    if (logger.isDebugEnabled()) {
                        logger.debug("recognized " + (isOutbound ? "outbound-" : "")
                                + "rule, from=" + from + ", to=" + to + ", isLast=" + isLast);
                    }
                }
            }
        }

        if (logger.isInfoEnabled()) {
            logger.info("read rules count: " + rules.size()
                    + ", outbound-rules count: " + outboundRules.size() + ", now compiling...");
        }

        compileRulesOneKind(false);
        compileRulesOneKind(true);

        if (logger.isInfoEnabled()) {
            logger.info("done reiniting rules");
        }
    }

    protected void compileRulesOneKind(boolean forOutbound) {
        int i = 0;
        for (RuleConfig r : forOutbound ? outboundRules : rules) {
            if (logger.isDebugEnabled()) {
                logger.debug("starting to compile "
                        + (forOutbound ? "outbound-" : "") + "rule #" + i + ": " + r);
            }
            r.compiled = Pattern.compile(r.from);
            i++;
        }
    }

    public String rewriteOutgoingUrl(String uglyUrl) {
        return rewriteUrl(uglyUrl, true);
    }

    public String rewriteIncomingUrl(String uglyUrl) {
        return rewriteUrl(uglyUrl, false);
    }

    protected String rewriteUrl(String uglyUrl, boolean forOutbound) {
        reinitIfNeeded();

        Iterable<RuleConfig> ruleSet = forOutbound ? outboundRules : rules;

        if (logger.isDebugEnabled()) {
            logger.debug("starting rewrite of " + (forOutbound ? "outgoing" : "incoming") + " ugly url=" + uglyUrl);
        }

        String prettyUrl = uglyUrl;
        int i = 0;
        for (RuleConfig r : ruleSet) {
            if (logger.isDebugEnabled()) {
                logger.debug("before rewrite with rule #" + i + ": " + r);
            }
            String oldUrl = prettyUrl;
            prettyUrl = r.compiled.matcher(prettyUrl).replaceAll(r.to);

            boolean wasLast = r.isLast && r.compiled.matcher(oldUrl).find();

            if (logger.isDebugEnabled()) {
                if (prettyUrl.equals(oldUrl)) {
                    logger.debug("rule #" + i + " had no effect, "
                            + "isLast=" + r.isLast + (r.isLast && wasLast ? ", wasLast!" : ""));
                } else {
                    logger.debug("rewriten with rule #" + i + ", ugly=" + oldUrl
                            + ", pretty=" + prettyUrl + ", isLast=" + r.isLast + (r.isLast && wasLast ? ", wasLast!" : ""));
                }
            }

            if (wasLast) {
                break;
            }

            i++;
        }

        if (logger.isDebugEnabled()) {
            logger.debug("finally rewriten " + (forOutbound ? "outgoing" : "incoming") + " URL: uglyUrl=" + uglyUrl + ", prettyUrl=" + prettyUrl);
        }

        return prettyUrl;
    }

    private static void assertEqual(Object expected, Object actual) {
        if (!BaseUtils.safeEquals(expected, actual)) {
            System.err.println("assertEqual failed: expected=[" + expected + "], actual=[" + actual + "]");
        }
    }

    public static void main(String[] args) throws Exception {

        String fileName = "C:/3zero/QnaApp/web/WEB-INF/urlrewrite.xml";
        ILameResourceProvider resProvider = new LameFileResourceProvider();
        IResourceFromProvider<InputStream> res = new ResourceFromProvider<InputStream>(resProvider, fileName);
        IWatchedResourceFromProvider<InputStream> wr = new WatchedResourceFromProvider<InputStream>(res);

        FoxyUrlRewriter fur = new FoxyUrlRewriter(wr, fileName);

        assertEqual("/qna?lang=pl&page=oneQuestion&id=27167&cM=3", fur.rewriteIncomingUrl("/pytanie_27167/cM_3"));
        assertEqual("/qna?page=oneQuestion&id=27167&cM=3", fur.rewriteIncomingUrl("/oneQuestion/id_27167/cM_3"));
        assertEqual("/qna?get=file_2754364715327622684.gif", fur.rewriteIncomingUrl("/qna?get=file_2754364715327622684.gif"));
        assertEqual("", fur.rewriteOutgoingUrl("http://wezyr.fando1.pl/qna?page=oneQuestion&id=27167&seotxt=uda.sie.zadac.pytanie.czy.nie.test.zadawania.pytania.od.czapy"));

//        System.out.println("decoded url="
//                + URLDecoder.decode(
//                "%3Cp%3E%3Ca+href%3D%22http%3A%2F%2Fpl.wikipedia.org%2Fwiki%2FPara_uporz%25C4%2585dkowana%22+target%3D%22_blank%22%3EPara+uporz%C4%85dkowana%3C%2Fa%3E+to+w+uproszczeniu+konstrukcja+spe%C5%82niaj%C4%85ca%3A%3C%2Fp%3E%0D%0A%3Cblockquote%3E%3C%2Fblockquote%3E%0D%0A%3Cp+style%3D%22padding-left%3A+30px%3B%22%3E%3Cem%3E%28a1%2C+b1%29+%3D+%28a2%2C+b2%29+%26lt%3B%3D%3D%3D%26gt%3B+a1+%3D+a2+i+b1+%3D+b2%3C%2Fem%3E%26nbsp%3B%3C%2Fp%3E%0D%0A%3Cblockquote%3E%3C%2Fblockquote%3E%0D%0A%3Cp%3EZnana+definicja+%28konstrukcja%29%26nbsp%3Bpary+uporz%C4%85dkowanej%26nbsp%3Bautorstwa+%3Ca+href%3D%22http%3A%2F%2Fpl.wikipedia.org%2Fwiki%2FKazimierz_Kuratowski%22+target%3D%22_blank%22%3EKazimierza+Kuratowskiego%3C%2Fa%3E%26nbsp%3Bjest+taka%3A%3C%2Fp%3E%0D%0A%3Cblockquote%3E%3C%2Fblockquote%3E%0D%0A%3Cp+style%3D%22padding-left%3A+30px%3B%22%3E%3Cem%3E%28a%2C+b%29+%3A%3D+%7B+%7B+a+%7D%2C+%7B+a%2C+b+%7D+%7D%3C%2Fem%3E%3C%2Fp%3E%0D%0A%3Cblockquote%3E%3C%2Fblockquote%3E%0D%0A%3Cp%3EPozornie+wydaje+si%C4%99%2C+%C5%BCe+wystarczy%C5%82aby+prostsza+konstrukcja%3A%3C%2Fp%3E%0D%0A%3Cblockquote%3E%3C%2Fblockquote%3E%0D%0A%3Cp+style%3D%22padding-left%3A+30px%3B%22%3E%3Cem%3E%28a%2C+b%29+%3A%3D+%7B+a%2C+%7B+b+%7D+%7D%3C%2Fem%3E%3C%2Fp%3E%0D%0A%3Cblockquote%3E%3C%2Fblockquote%3E%0D%0A%3Cp%3EZagadka+polega+na+podaniu+przyk%C5%82adu+%3Cem%3Ea1%3C%2Fem%3E%2C+%3Cem%3Eb1%3C%2Fem%3E+i+%3Cem%3Ea2%3C%2Fem%3E%2C+%3Cem%3Eb2%3C%2Fem%3E+dla+kt%26oacute%3Brych+%3Cem%3Ea1+%26lt%3B%26gt%3B+a2%3C%2Fem%3E+i%2Flub+%3Cem%3Eb1+%26lt%3B%26gt%3B+b2%3C%2Fem%3E%2C+ale+%3Cem%3E%28a1%2C+b1%29+%3D+%28a2%2C+b2%29%3C%2Fem%3E+wg+b%C5%82%C4%99dnej+konstrukcji.+Czyli+b%C4%99dzie+to+kontrprzyk%C5%82ad+uzasadniaj%C4%85cy+b%C5%82%C4%99dno%C5%9B%C4%87+tej+konstrukcji.%3C%2Fp%3E",
//                "utf-8"));


//        String uglyUrl = //"/qna?lang=pl&page=oneQuestion&id=716";
//                //"/QnaApp/qna?page=oneQuestion&id=27167&cId=231";
//                "/pytanie_27167/cM_3";
//        String prettyUrl = fur.rewriteIncomingUrl(uglyUrl);
//        //String prettyUrl = our.rewriteOutgoingUrl(uglyUrl);
//
//        System.out.println("uglyUrl=" + uglyUrl + ", prettyUrl=" + prettyUrl);


//        InputStream is = new FileInputStream(fileName);
//        Map<String, Object> m = XMLMapper.readMap(is, fileName);
//
//        //System.out.println("m=" + m);
//        List<Object> subNodes = (List) m.get("#subNodes");
//        //System.out.println("subnodes=" + subNodes);
//
//        List<RuleConfig> rules = new ArrayList<RuleConfig>();
//
//        for (Object subNode : subNodes) {
//            if (subNode instanceof Map) {
//                Map sbm = (Map) subNode;
//                System.out.println("sbm=" + sbm);
//                String name = (String) sbm.get("#name");
//                if ("outbound-rule".equals(name)) {
//                    String to = null;
//                    String from = null;
//                    List<Object> subSubNodes = (List) sbm.get("#subNodes");
//                    for (Object subSubNode : subSubNodes) {
//                        if (subSubNode instanceof Map) {
//                            Map ssnm = (Map) subSubNode;
//                            String nn = (String) ssnm.get("#name");
//                            if ("from".equals(nn)) {
//                                from = ((List) ssnm.get("#subNodes")).get(0).toString();
//                            } else if ("to".equals(nn)) {
//                                to = ((List) ssnm.get("#subNodes")).get(0).toString();
//                            } else {
//                                System.out.println("#### error - unknown node with name: " + nn);
//                            }
//                        }
//                    }
//                    RuleConfig r = new RuleConfig(from, to);
//                    rules.add(r);
//                    System.out.println("recognized outbound-rule, from=" + from + ", to=" + to);
//                }
//            }
//        }
//        System.out.println("rules:" + rules);
//
//        for (RuleConfig r : rules) {
//            r.compiled = Pattern.compile(r.from);
//        }
//
//        String uglyUrl = "/qna?lang=pl&page=oneQuestion&id=716";
//
//        System.out.println("test----: " + "ala ma kota".replaceAll("ala ([a-z]*)", "hela $1"));
//
//        Pattern p = Pattern.compile("ala ([a-z]*)");
//        System.out.println("test2-------: " + p.matcher("ala ma kota").replaceAll("hela $1"));
//
//        System.out.println("------- rewriting starts");
//
//        String prettyUrl = uglyUrl;
//        int i = 0;
//        for (RuleConfig r : rules) {
//            System.out.println("before rewrite with rule #" + i + ", from=" + r.from + ", to=" + r.to);
//            String oldUrl = prettyUrl;
//            prettyUrl = r.compiled.matcher(prettyUrl).replaceAll(r.to);
//            System.out.println("rewriten with rule #" + i + ", ugly=" + oldUrl + ", pretty=" + prettyUrl);
//            i++;
//        }
//
//        System.out.println("uglyUrl=" + uglyUrl + ", prettyUrl=" + prettyUrl);
    }
}
