/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.lisa;

import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;

/**
 *
 * @author ctran
 */
public class CheckboxHeaderCell extends CheckboxCell {

    private final SafeHtml INPUT_CHECKED = SafeHtmlUtils.fromSafeConstant("<input type=\"checkbox\" tabindex=\"-1\" checked/>");
    private final SafeHtml INPUT_UNCHECKED = SafeHtmlUtils.fromSafeConstant("<input type=\"checkbox\" tabindex=\"-1\"/>");
    private Boolean checked;
    private Element parent;

    public CheckboxHeaderCell() {
        super(true, false);
        checked = false;
    }

    public void setValue(Boolean value) {
        checked = value;
        if (parent != null) {
            if (checked) {
                parent.setInnerSafeHtml(INPUT_CHECKED);
            } else {
                parent.setInnerSafeHtml(INPUT_UNCHECKED);
            }
        }
    }

    public Boolean getValue() {
        return checked;
    }

    @Override
    public void onBrowserEvent(Context context, Element parent, Boolean value, NativeEvent event, ValueUpdater<Boolean> valueUpdater) {
        this.parent = parent;
        super.onBrowserEvent(context, parent, value, event, valueUpdater);
    }
}
