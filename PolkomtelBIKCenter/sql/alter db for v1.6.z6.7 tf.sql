﻿-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
-----------------   Dodanie konektora do PostgreSQL  --------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
exec sp_check_version '1.6.z6.7';
go


if not exists(select * from sys.objects where object_id = object_id(N'[amg_node]') and type in (N'U'))
begin
	create table amg_node (
		id int not null identity(1,1) primary key,
		obj_id varchar(900) not null,
		parent_obj_id varchar(900) null,
		node_kind_code varchar(255) not null,
		name varchar(900) not null,
		descr varchar(max) null,
		visual_order int not null default(0),
		branch_ids varchar(900) null
	);
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[amg_node]') and name = N'uq_amg_node_obj_id')
begin
	create unique index uq_amg_node_obj_id on amg_node (obj_id);
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[amg_node]') and name = N'idx_amg_node_node_kind_code')
begin
	create index idx_amg_node_node_kind_code on amg_node (node_kind_code);
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[amg_node]') and name = N'idx_amg_node_parent_obj_id')
begin
	create index idx_amg_node_parent_obj_id on amg_node (parent_obj_id);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'aaa_postgres_column_extradata') and type in (N'U'))
begin
	create table aaa_postgres_column_extradata (
		id int not null identity(1,1) primary key,
		name varchar(500) not null,
		owner varchar(500) not null,
		table_name varchar(500) not null,
		database_name varchar(500) not null,
		server_name varchar(500) not null,
		column_id int not null,
		description varchar(max) null,
		is_nullable int not null,
		type varchar(100) not null,
		default_field varchar(512) null
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'aaa_postgres_foreign_key') and type in (N'U'))
begin
	create table aaa_postgres_foreign_key (
		id int not null identity(1,1) primary key,
		name varchar(500) not null,
		description varchar(max) null,
		server varchar(500) null,
		database_name varchar(500) not null,
		fk_table varchar(500) not null,
		fk_schema varchar(500) not null,
		fk_column varchar(500) not null,
		pk_table varchar(500) not null,
		pk_schema varchar(500) not null,
		pk_column varchar(500) not null
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'aaa_postgres_index') and type in (N'U'))
begin
	create table aaa_postgres_index (
		id int not null identity(1,1) primary key,
		name varchar(500) null,
		server varchar(500) null,
		owner varchar(500) not null,
		database_name varchar(500) not null,
		table_name varchar(500) not null,
		column_name varchar(500) not null,
		column_position int null,
		is_unique int null,
		type varchar(50) null,
		is_primary_key int not null default(0)
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'aaa_postgres_dependency') and type in (N'U'))
begin
	create table aaa_postgres_dependency (
		id int not null identity(1,1) primary key,
		server varchar(500) null,
		database_name varchar(500) not null,
		ref_name varchar(500) not null,
		ref_owner varchar(500) not null,
		dep_name varchar(500) not null,
		dep_owner varchar(500) not null
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'aaa_postgres_definition') and type in (N'U'))
begin
	create table aaa_postgres_definition (
		id int not null identity(1,1) primary key,
		server varchar(500) null,
		database_name varchar(500) not null,
		owner varchar(500) not null,
		name varchar(500) not null,
		definition varchar(max) not null
	);
end;
go

if not exists (select 1 from bik_tree where code = 'PostgreSQL')
begin
	insert into bik_tree(name, code, tree_kind, is_hidden, is_in_ranking, branch_level_for_statistics, is_built_in)
	values('PostgreSQL', 'PostgreSQL', 'Metadata', 1, 0, 1, 0);
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic, children_kinds)
	values ('PostgresServer', 'Serwer PostgreSQL', 'postgresServer', 'Metadata', 0, 8, 0, 'PostgresDatabase');
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic, children_kinds)
	values ('PostgresDatabase', 'Baza danych PostgreSQL', 'postgresDatabase', 'Metadata', 0, 10, 0, 'PostgresSchema');
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic, children_kinds)
	values ('PostgresSchema', 'Schemat PostgreSQL', 'postgresSchema', 'Metadata', 0, 9, 0, 'PostgresFolder');
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values ('PostgresTable', 'Tabela PostgreSQL', 'postgresTable', 'Metadata', 0, 8, 0);
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values ('PostgresView', 'Widok PostgreSQL', 'postgresView', 'Metadata', 0, 8, 0);
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values ('PostgresFunction', 'Funkcja PostgreSQL', 'postgresFunction', 'Metadata', 0, 8, 0);
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values ('PostgresColumn', 'Kolumna PostgreSQL', 'postgresColumn', 'Metadata', 0, 7, 0);
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values ('PostgresColumnPK', 'Kolumna PostgreSQL PK', 'postgresColumnPK', 'Metadata', 0, 7, 0);
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values ('PostgresColumnFK', 'Kolumna PostgreSQL FK', 'postgresColumnFK', 'Metadata', 0, 7, 0);
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic, children_kinds)
	values ('PostgresFolder', 'Folder PostgreSQL', 'postgresFolder', 'Metadata', 0, 0, 0, 'PostgresTable|PostgresView|PostgresFunction');
	

	
	exec sp_add_menu_node 'databases', '@', '$PostgreSQL'
	exec sp_add_menu_node '$PostgreSQL', '@', '&PostgresServer'
	exec sp_add_menu_node '&PostgresServer', '@', '&PostgresDatabase'
	exec sp_add_menu_node '&PostgresDatabase', '@', '&PostgresSchema'
	exec sp_add_menu_node '&PostgresSchema', '@', '&PostgresTable'
	exec sp_add_menu_node '&PostgresSchema', '@', '&PostgresView'
	exec sp_add_menu_node '&PostgresSchema', '@', '&PostgresFunction'
	exec sp_add_menu_node '&PostgresView', '@', '&PostgresColumn'
	exec sp_add_menu_node '&PostgresTable', '@', '&PostgresColumn'
	exec sp_add_menu_node '&PostgresTable', '@', '&PostgresColumnPK'
	exec sp_add_menu_node '&PostgresTable', '@', '&PostgresColumnFK'
	
	insert into bik_translation(code, txt, lang, kind)
	values('PostgreSQL', 'PostgreSQL', 'en', 'tree')

	insert into bik_icon(name)
	values  ('postgresServer'),
			('postgresDatabase'),
			('postgresSchema'),
			('postgresTable'),
			('postgresView'),
			('postgresFunction'),
			('postgresColumn'),
			('postgresColumnPK'),
			('postgresColumnFK'),
			('postgresFolder'),
			('postgresIndex')
			
	insert into bik_translation(code, txt, lang, kind)
	values  ('PostgresServer', 'PostgreSQL server', 'en', 'kind'),
			('PostgresDatabase', 'PostgreSQL database', 'en', 'kind'),
			('PostgresSchema', 'PostgreSQL schema', 'en', 'kind'),
			('PostgresTable', 'PostgreSQL table', 'en', 'kind'),
			('PostgresView', 'PostgreSQL view', 'en', 'kind'),
			('PostgresFunction', 'PostgreSQL function', 'en', 'kind'),
			('PostgresColumn', 'PostgreSQL column', 'en', 'kind'),
			('PostgresColumnFK', 'PostgreSQL column foreign key', 'en', 'kind'),
			('PostgresColumnPK', 'PostgreSQL column primary key', 'en', 'kind'),
			('PostgresFolder', 'PostgreSQL folder', 'en', 'kind')
			
end;
go

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
exec sp_node_init_branch_id @tree_id, null
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('[bik_db_server]') and name = 'tree_id')
begin
  alter table bik_db_server add tree_id int null references bik_tree(id);
end
go

update bik_db_server set tree_id = (select id from bik_tree where code = 'PostgreSQL')
from bik_db_server where source_id = (select id from bik_data_source_def where description = 'PostgreSQL')

update bik_db_server set tree_id = (select id from bik_tree where code = 'Oracle')
from bik_db_server where source_id = (select id from bik_data_source_def where description = 'Oracle')

update bik_db_server set tree_id = (select id from bik_tree where code = 'MSSQL')
from bik_db_server where source_id = (select id from bik_data_source_def where description = 'MS SQL')

if not exists (select 1 from bik_data_source_def where description = 'PostgreSQL')
begin
	insert into bik_data_source_def(description) values ('PostgreSQL')
/*
	insert into bik_schedule(source_id, hour, minute, interval, is_schedule, priority)
	values((select id from bik_data_source_def where description = 'PostgreSQL'), 0, 0, 1, 0, 4)

	insert into bik_db_server(source_id, name, server, instance, db_user, db_password, is_active, port)
	values ((select id from bik_data_source_def where description='PostgreSQL'), 'My First Server', '', '', '', '', 0, null)
*/
end;
go

-- fix na schedule dla Oracle
declare @oracleSourceId int = (select id from bik_data_source_def where description = 'Oracle');
if exists(select 1 from bik_schedule where source_id = @oracleSourceId and instance_name is null)
begin
	update bik_schedule set instance_name = (select top 1 name from bik_db_server where source_id = @oracleSourceId)
	where source_id = @oracleSourceId
	
	insert into bik_schedule(source_id, hour, minute, interval, is_schedule, priority, instance_name)
	select @oracleSourceId, 0, 0, 1, 0, 2, name 
	from bik_db_server db
	where source_id = @oracleSourceId
	and not exists(select 1 from bik_schedule where source_id = @oracleSourceId and instance_name = db.name)
end;
go

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_generic_insert_into_bik_node]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_generic_insert_into_bik_node]
go

create procedure [dbo].[sp_generic_insert_into_bik_node](@treeCode varchar(max), @objId varchar(900))
as
begin
	-- założenia:
	-- branch_ids są uzupełnione w bik_node i amg_node (@objId is not null)
	-- jeśli @objId is not null wtedy musi istniec ten obiekt w bik_node (inny mechanizm go wstawil wczesniej)
	-- 
	declare @treeId int = (select id from bik_tree where code = @treeCode);
	declare @parentNodeId int = (select id from bik_node where tree_id = @treeId and obj_id = @objId);
	declare @branchIds varchar(900) = (select branch_ids from bik_node where tree_id = @treeId and obj_id = @objId);
	declare @amgBranchIds varchar(900) = (select branch_ids from amg_node where obj_id = @objId);

	-- update
	update bik_node 
	set is_deleted = 0, descr = gen.descr, visual_order = gen.visual_order
	from amg_node as gen
	where bik_node.tree_id = @treeId
	and (@branchIds is null or bik_node.branch_ids like @branchIds + '%')
	and (@amgBranchIds is null or gen.branch_ids like @amgBranchIds + '%')
	and bik_node.obj_id = gen.obj_id
	and bik_node.linked_node_id is null

	-- insert nowych
	insert into bik_node(parent_node_id, node_kind_id, name, descr, tree_id, obj_id, visual_order)
	select @parentNodeId, bnk.id, gen.name, gen.descr, @treeId, gen.obj_id, gen.visual_order
	from amg_node as gen
	inner join bik_node_kind as bnk on gen.node_kind_code = bnk.code
	left join (bik_node as bn inner join bik_tree as bt on bn.tree_id = bt.id and bn.tree_id = @treeId and (@branchIds is null or bn.branch_ids like @branchIds + '%')) on bn.obj_id = gen.obj_id
	where bn.id is null
	and (@amgBranchIds is null or gen.branch_ids like @amgBranchIds + '%')
	order by gen.obj_id
	
	exec sp_node_init_branch_id @treeId, @parentNodeId;

	-- update parentów
	update bik_node 
	set parent_node_id = bk.id
	from bik_node 
	inner join amg_node as gen on bik_node.obj_id = gen.obj_id
	inner join bik_node as bk on bk.obj_id = gen.parent_obj_id
	where bik_node.tree_id = @treeId
	and (@branchIds is null or bik_node.branch_ids like @branchIds + '%')
	and (@amgBranchIds is null or gen.branch_ids like @amgBranchIds + '%')
	and bik_node.is_deleted = 0
	and bik_node.linked_node_id is null
	and bk.tree_id = @treeId
	and bk.is_deleted = 0
	and bk.linked_node_id is null

	-- usunięcie zbędnych
	update bik_node
	set is_deleted = 1
	from bik_node 
	left join amg_node as gen on bik_node.obj_id = gen.obj_id 
		and (@amgBranchIds is null or gen.branch_ids like @amgBranchIds + '%')
	where gen.name is null
	and bik_node.tree_id = @treeId
	and (@branchIds is null or bik_node.branch_ids like @branchIds + '%')
	and bik_node.is_deleted = 0
	and bik_node.linked_node_id is null
	
	-- update zbędnych podlinkowanych
	update bik_node
	set is_deleted = 1
	from bik_node
	inner join bik_node as bn2 on bik_node.linked_node_id = bn2.id
	where bn2.tree_id = @treeId
	and (@branchIds is null or bn2.branch_ids like @branchIds + '%')
	and bn2.is_deleted = 1

	exec sp_node_init_branch_id @treeId, null
end;
go


if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_amg_node_init_branch_id]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_amg_node_init_branch_id]
go

create procedure [dbo].[sp_amg_node_init_branch_id]
as
begin
	set nocount on
	declare @diags_level int = 0;


	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #1'

  create table #ids (id int not null primary key);
  
  insert into #ids (id) 
  select id from amg_node where parent_obj_id is null

	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #2'
    
  create table #child_ids (id int not null primary key);
    
  while exists (select 1 from #ids)
  begin
	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #3'
    
    update amg_node
    set branch_ids = coalesce(parent.branch_ids, '') + CAST(amg_node.id as varchar(10)) + '|'
    from amg_node inner join #ids on amg_node.id = #ids.id
      left join amg_node as parent on parent.obj_id = amg_node.parent_obj_id;    
    
	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #4'

    truncate table #child_ids;
    
    insert into #child_ids (id)
    select id from amg_node where parent_obj_id in (select nd.obj_id from #ids as tm inner join amg_node as nd on nd.id = tm.id);
    
	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #5'

    truncate table #ids;
    
    insert into #ids (id) select id from #child_ids;

	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #6'    
  end
    
	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #7'

  drop table #ids;
  drop table #child_ids;
  
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_db_column_extradata') and type in (N'U'))
begin
	create table bik_db_column_extradata (
		id int not null identity(1,1) primary key,
		source_id int not null references bik_data_source_def(id),
		name varchar(512) not null,
		server_name varchar(512) not null,
		database_name varchar(512) not null,
		table_node_id int not null references bik_node(id),
		column_node_id int not null references bik_node(id),
		column_id int not null,
		description varchar(max) null,
		is_nullable int not null,
		type varchar(100) not null,
		default_field varchar(512) null,
		seed_value bigint null,
		increment_value bigint null
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_db_foreign_key') and type in (N'U'))
begin
	create table bik_db_foreign_key (
		id int not null identity(1,1) primary key,
		source_id int not null references bik_data_source_def(id),
		name varchar(512) not null,
		description varchar(max) null,
		server_name varchar(512) not null,
		database_name varchar(512) not null,
		fk_table varchar(512) not null,
		fk_table_node_id int not null references bik_node(id),
		fk_schema varchar(512) not null,
		fk_column varchar(512) not null,
		fk_column_node_id int not null references bik_node(id),
		pk_table varchar(512) not null,
		pk_table_node_id int not null references bik_node(id),
		pk_schema varchar(512) not null,
		pk_column varchar(512) not null,
		pk_column_node_id int not null references bik_node(id)
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_db_index') and type in (N'U'))
begin
	create table bik_db_index (
		id int not null identity(1,1) primary key,
		source_id int not null references bik_data_source_def(id),
		name varchar(512) null,
		server_name varchar(512) not null,
		database_name varchar(512) not null,
		table_node_id int not null references bik_node(id),
		column_node_id int not null references bik_node(id),
		column_name varchar(512) not null,
		column_position int not null,
		is_unique int not null,
		type varchar(512) not null,
		is_primary_key int not null default(0),
		partition_ordinal int not null default(0)
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_db_dependency') and type in (N'U'))
begin
	create table bik_db_dependency (
		id int not null identity(1,1) primary key,
		source_id int not null references bik_data_source_def(id),
		server_name varchar(512) not null,
		database_name varchar(512) not null,
		ref_node_id int not null references bik_node (id),
		dep_node_id int not null references bik_node (id),
		ref_name varchar(512) not null,
		ref_owner varchar(512) not null,
		dep_name varchar(512) not null,
		dep_owner varchar(512) not null
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_db_definition') and type in (N'U'))
begin
	create table bik_db_definition (
		id int not null identity(1,1) primary key,
		source_id int not null references bik_data_source_def(id),
		server_name varchar(512) not null,
		database_name varchar(512) not null,
		object_node_id int not null references bik_node (id),
		definition varchar(max) not null
	);
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_db_definition]') and name = N'uq_bik_db_definition_object_node_id')
begin
	create unique index uq_bik_db_definition_object_node_id on bik_db_definition (object_node_id);
end;
go


-- poprzednia wersja w pliku: alter db for v1.6.z4 tf.sql
-- dodanie konektrora do PostgreSQL
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs_metadata]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs_metadata
go

create procedure [dbo].[sp_verticalize_node_attrs_metadata](@optNodeFilter varchar(max))
as
begin
	declare @fixedOptNodeFilter varchar(max), @baseFixedOptNodeFilter varchar(max);
	if @optNodeFilter is null
	begin
		set @baseFixedOptNodeFilter = '(1 = 1)';
	end
	else
	begin
		set @baseFixedOptNodeFilter = '(' + @optNodeFilter + ')';
	end;

	-- SAP BO
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select report_tree_id from bik_sapbo_server union all select universe_tree_id from bik_sapbo_server union all select connection_tree_id from bik_sapbo_server))';
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_query', 'node_id', 'sql_text, filtr_text', null, @fixedOptNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_connection', 'node_id', 'database_engine, database_source, connetion_networklayer_name, user_name, server', null, @fixedOptNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_object', 'node_id', 'text_of_select, text_of_where', null, @fixedOptNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_table', 'node_id', 'sql_of_derived_table', null, @fixedOptNodeFilter
	
	declare @bo_extradata_sql varchar(max) = '(select node_id, author, owner, cuid, guid, ruid, convert(varchar(24),created,120) as created_time, convert(varchar(24),modified,120) as modified_time, convert(varchar(24),last_run_time,120) as last_run_time from bik_sapbo_extradata)';
	exec sp_verticalize_node_attrs_one_metadata_table @bo_extradata_sql, 'node_id', 'author, owner, cuid, guid, ruid, created_time, modified_time, last_run_time', null, @fixedOptNodeFilter
	
	declare @schedule_sql varchar(max) = '(select node_id, owner as schedule_owner, convert(varchar(24),creation_time,120) as creation_time, convert(varchar(24),nextruntime,120) as nextruntime, convert(varchar(24),expire,120) as expire, destination, format, parameters, destination_email_to from bik_sapbo_schedule)';
	exec sp_verticalize_node_attrs_one_metadata_table @schedule_sql, 'node_id', 'schedule_owner, destination, creation_time, nextruntime, expire, format, parameters, destination_email_to', null, @fixedOptNodeFilter
	
	declare @olap_sql varchar(max) = '(select node_id, type, provider_caption, cube_caption, cuid as olap_cuid, convert(varchar(24),created,120) as created, owner as olap_owner, convert(varchar(24),modified,120) as modified from bik_sapbo_olap_connection)';
	exec sp_verticalize_node_attrs_one_metadata_table @olap_sql, 'node_id', 'type, provider_caption, cube_caption, olap_cuid, created, olap_owner, modified', null, @fixedOptNodeFilter
	
	declare @obj_id_sql varchar(max) = '(select bn.id, bn.obj_id as si_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''DataConnection'', ''Webi'', ''Flash'', ''CrystalReport'', ''Universe'', ''Excel'', ''FullClient'', ''Pdf'', ''Hyperlink'', ''Powerpoint'',
		''ReportFolder'', ''UniversesFolder'', ''ConnectionFolder'', ''ReportSchedule'', ''DSL.MetaDataFile'', ''CommonConnection'', ''CCIS.DataConnection''))'
	exec sp_verticalize_node_attrs_one_metadata_table @obj_id_sql, 'id', 'si_id', null, @fixedOptNodeFilter
	
	-- Bazy danych (Oracle, Postgres, ...)
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select distinct tree_id from bik_db_server))';
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_db_definition', 'object_node_id', 'definition', 'definition_sql', @fixedOptNodeFilter
	
	-- MS SQL
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code = ''MSSQL''))';
	declare @mssql_tree_id int = (select id from bik_tree where code = 'MSSQL')
	declare @mssql_sql varchar(max) = '(select bn.id, ms.definition_text from bik_mssql ms 
		inner join bik_node bn on bn.obj_id = ms.branch_names
		where bn.tree_id = ' +  cast(@mssql_tree_id as varchar(20)) + '
		and bn.is_deleted = 0
		and linked_node_id is null
		and ms.definition_text is not null)'
	exec sp_verticalize_node_attrs_one_metadata_table @mssql_sql, 'id', 'definition_text', null, @fixedOptNodeFilter
	-- Usuwanie starych ex propsów
	delete from bik_searchable_attr_val where attr_id in 
	(select att.id from bik_searchable_attr att 
	left join (select distinct name from bik_mssql_extended_properties prop) x on att.name = '$exprop_' + x.name
	where att.name like '$exprop_%' and x.name is null)

	delete from bik_searchable_attr from bik_searchable_attr att 
	left join (select distinct name from bik_mssql_extended_properties prop) x on att.name = '$exprop_' + x.name
	where att.name like '$exprop_%' and x.name is null
	--
	exec sp_verticalize_node_attrs_ex_props @fixedOptNodeFilter
	  
	-- DQC
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code = ''DQC''))';
	declare @tree_id int = (select id from bik_tree where code = 'DQC')
	declare @inact_nk_id int = (select id from bik_node_kind where code = 'DQCTestInactive'),
    @succ_nk_id int = (select id from bik_node_kind where code = 'DQCTestSuccess'),
    @fail_nk_id int = (select id from bik_node_kind where code = 'DQCTestFailed')

	declare @dqc_src_sql varchar(max) = '(select cast(dt.long_name as varchar(max)) as long_name, cast(dt.sampling_method as varchar(max)) as sampling_method, cast(dt.verified_attributes as varchar(max)) as verified_attributes,cast(dt.logging_details as varchar(max)) as logging_details, cast(dt.benchmark_definition as varchar(max)) as benchmark_definition, cast(dt.results_object as varchar(max)) as results_object, cast(dt.additional_information as varchar(max)) as additional_information, n.id as node_id
		from bik_dqc_test dt inner join bik_node n on dt.__obj_id = n.obj_id and n.node_kind_id in (' +
		cast(@inact_nk_id as varchar(20)) + ',' + cast(@succ_nk_id as varchar(20)) + ',' + cast(@fail_nk_id as varchar(20)) + ') and tree_id = ' + cast(@tree_id as varchar(20)) + '
		where n.linked_node_id is null and n.is_deleted = 0)'
	exec sp_verticalize_node_attrs_one_metadata_table @dqc_src_sql, 'node_id', 'long_name, sampling_method, verified_attributes,logging_details, benchmark_definition, results_object, additional_information', null, @fixedOptNodeFilter
	
	-- AD
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code = ''UserRoles''))';
	declare @ad_src_sql varchar(max) = '(select bu.email, coalesce(bu.phone_num, ad.telephone_number) as phone_num,
		ad.physical_delivery_office_name,ad.postal_code, ad.manager, 
		ad.description, ad.department, ad.title, ad.mobile, 
		ad.display_name, bu.node_id
		from bik_user bu
		join bik_node bn on bn.id = bu.node_id and bn.is_deleted = 0
		left join bik_system_user bsu on bsu.user_id = bu.id 
		left join bik_active_directory ad 
		on (bu.login_name_for_ad = ad.s_a_m_account_name or bsu.login_name = ad.s_a_m_account_name))'
	exec sp_verticalize_node_attrs_one_metadata_table @ad_src_sql, 'node_id', 'email, phone_num, physical_delivery_office_name,postal_code,manager, description, department, title, mobile, display_name', null, @fixedOptNodeFilter
	
	-- SAP BW query extradata
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code in (''BWProviders'', ''BWReports'')))';
	declare @bwreports_id int = (select id from bik_tree where code = 'BWReports')
	declare @query_nk_id int = (select id from bik_node_kind where code = 'BWBEx')
	declare @sapbw_query_src_sql varchar(max) = '(select q.update_time, q.owner as sapbw_owner, q.last_edit, bn.id as node_id
		from bik_sapbw_query q inner join bik_node bn on q.obj_name = bn.obj_id and bn.node_kind_id = ' +
		cast(@query_nk_id as varchar(20)) + ' and bn.tree_id = ' + cast(@bwreports_id as varchar(20)) + '
		where bn.linked_node_id is null and bn.is_deleted = 0)'
	exec sp_verticalize_node_attrs_one_metadata_table @sapbw_query_src_sql, 'node_id', 'update_time, sapbw_owner, last_edit', null, @fixedOptNodeFilter
	
	-- SAP BW providers, query and areas - technical id
	declare @bw_obj_id_sql varchar(max) = '(select bn.id, bn.obj_id as technical_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''BWBEx'', ''BWArea'', ''BWMPRO'', ''BWCUBE'', ''BWISET'', ''BWODSO''))'
	exec sp_verticalize_node_attrs_one_metadata_table @bw_obj_id_sql, 'id', 'technical_id', null, @fixedOptNodeFilter
	
	-- SAP BW objects - technical id
	declare @bw_objs_id_sql varchar(max) = '(select bn.id, obj.obj_name as obj_technical_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
		inner join bik_sapbw_object obj on obj.provider + ''|'' + obj.obj_name = bn.obj_id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''BWUni'',''BWKyf'', ''BWTim'', ''BWDpa'', ''BWCha'')
		and obj.provider_parent is null)'
	exec sp_verticalize_node_attrs_one_metadata_table @bw_objs_id_sql, 'id', 'obj_technical_id', null, @fixedOptNodeFilter
	
end;
go

update bik_node_kind set children_kinds = 'MSSQLDatabase' where code = 'MSSQLServer'
update bik_node_kind set children_kinds = 'MSSQLFolder' where code = 'MSSQLDatabase'
update bik_node_kind set children_kinds = 'MSSQLTable|MSSQLView|MSSQLProcedure|MSSQLFunction|MSSQLTableFunction' where code = 'MSSQLFolder'

alter table aaa_oracle_index alter column name varchar(500) not null;
go

alter table aaa_oracle_index alter column owner varchar(500) not null;
go

alter table aaa_oracle_index alter column table_name varchar(500) not null;
go

alter table aaa_oracle_index alter column column_name varchar(500) not null;
go

if not exists (select * from bik_app_prop where name = 'disableRankingPage')
begin
	insert into bik_app_prop(name, val, is_editable)
	values ('disableRankingPage', 'false', 1)
end;
go

-- obiekty is_built_in na końcu
update bik_node set visual_order = 1000
where id in (select id from bik_node where is_built_in = 1 and is_deleted = 0 and node_kind_id in (select id from bik_node_kind where code in ('DocumentsFolder','AllUsersFolder','DQCAllTestsFolder')))
go

-- fix na nazwy definicji
update bik_translation set txt = 'Definicja sql' where code = 'definition_sql' and kind = 'adef' and lang = 'pl'
update bik_translation set txt = 'Definition sql' where code = 'definition_sql' and kind = 'adef' and lang = 'en'
go

exec sp_update_version '1.6.z6.7', '1.6.z6.8';
go