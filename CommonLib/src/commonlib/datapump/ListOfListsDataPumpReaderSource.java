/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib.datapump;

import java.util.List;

/**
 *
 * @author pmielanczuk
 */
public class ListOfListsDataPumpReaderSource<V> implements IDataPumpSource {

    protected List<String> colNames;
    protected List<List<V>> rows;
    protected final int colCnt;
    protected final int rowCnt;
    protected int rowNum = 0;

    public ListOfListsDataPumpReaderSource(List<String> colNames, List<List<V>> rows) {
        this.colNames = colNames;
        this.rows = rows;
        colCnt = colNames.size();
        rowCnt = rows.size();
    }

    @Override
    public ColumnMetadata[] openSource() throws Exception {
        ColumnMetadata[] res = new ColumnMetadata[colCnt];

        for (int i = 0; i < colCnt; i++) {
            ColumnMetadata c = new ColumnMetadata(colNames.get(i), "varchar", Integer.MAX_VALUE, Integer.MAX_VALUE, -1, true, i);
            res[i] = c;
        }

        return res;
    }

    @Override
    public void closeSource() throws Exception {
        // możliwe szybsze odśmiecenie
        colNames = null;
        rows = null;
    }

    @Override
    public boolean hasSourceData() throws Exception {
        return rowNum < rowCnt;
    }

    @Override
    public Object[] readDataRow() throws Exception {
        Object[] res = new Object[colCnt];

        final List row = rows.get(rowNum);

        for (int i = 0; i < colCnt; i++) {
            res[i] = row.get(i);
        }

        rowNum++;

        return res;
    }
}
