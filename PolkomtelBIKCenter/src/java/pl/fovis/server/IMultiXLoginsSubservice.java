/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import java.util.Set;

/**
 *
 * @author pmielanczuk
 */
public interface IMultiXLoginsSubservice {

    public void addMultiXLoginToExistingDb(String login);

    public boolean multiXLoginExistsForCurrentDatabase(String login);

    public void replaceMultiXLogin(String login, String oldLogin);

    public String getServerSupportedLocaleNamesStr();

    public boolean multiXLoginExistsForDatabase(String loginName, String dbName);

    public void addMultiXLoginToExistingDb(String loginName, String dbName);

    public void changeAccessToDatabase(boolean disabled, String loginName, String get);

    public String getDefaultDbNameForMultiXLogin(final String login);

    public Set<String> loadDbNames(final String login);
}
