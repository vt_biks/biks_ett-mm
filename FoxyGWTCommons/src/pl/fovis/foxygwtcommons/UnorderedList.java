/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

/**
 *
 * @author wezyr
 */
@SuppressWarnings("deprecation")
public class UnorderedList extends GenericHtmlElementContainerWidget {

    public static UnorderedList wrap(com.google.gwt.user.client.Element ele) {
        return wrap(ele, new UnorderedList(null));
    }

    protected UnorderedList(com.google.gwt.user.client.Element ele) {
        super(ele);
    }

    public UnorderedList() {
        super();
    }

    @Override
    protected String getElementName() {
        return "UL" /* i18n: no */;
    }
}
