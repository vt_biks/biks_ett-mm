/*
 * XMLMapper.java
 *
 * Created on 28 grudzie� 2006, 14:03
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package commonlib;

import simplelib.LameRuntimeException;
import simplelib.BaseUtils.MapProjector;
import simplelib.BaseUtils.Projector;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.kxml2.io.KXmlParser;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public class XMLMapper {

    static ILameLogger logger = LameLoggerFactory.getLogger(XMLMapper.class);
    //LameLogger.getClassLogger();
    private KXmlParser parser;
    private boolean skipMode;
    private boolean ignoreEmptyText;

    public void initializeXml(InputStream is) {
        try {
            parser.setInput(is, null);
            //if (logger.isInfoEnabled())
            //    logger.info("parser encoding 1: " + parser.getInputEncoding());

            parser.require(XmlPullParser.START_DOCUMENT, null, null);
            //if (logger.isInfoEnabled())
            //    logger.info("parser encoding 1.1: " + parser.getInputEncoding());
            parser.nextTag();
            //if (logger.isInfoEnabled())
            //    logger.info("parser encoding 2: " + parser.getInputEncoding());
        } catch (Exception ex) {
            //ex.printStackTrace();
            throw new LameRuntimeException("exception while initializing parser / xml", ex);
        }
    }

    public XMLMapper() {
        this.parser = new KXmlParser();
    }

    public XMLMapper(InputStream is) {
        this();
        initializeXml(is);
    }

    public static Map<String, Object> readMap(byte[] bytes, String location) {
        return readMap(new ByteArrayInputStream(bytes), location);
    }

    public void finalizeXml() {
        try {
            //if (logger.isInfoEnabled())
            //    logger.info("parser encoding 3: " + parser.getInputEncoding());
            parser.require(XmlPullParser.END_DOCUMENT, null, null);
        } catch (Exception ex) {
            throw new LameRuntimeException("exception while finalizing xml", ex);
        }
    }

    public static Map<String, Object> readMap(String fileName, boolean ignoreEmptyText) {
        try {
            return readMap(new FileInputStream(fileName), fileName, ignoreEmptyText);
        } catch (Exception ex) {
            throw new RuntimeException("error reading file: " + fileName, ex);
        }
    }

    public static Map<String, Object> readMap(String fileName) {
        return readMap(fileName, true);
    }

    public static Map<String, Object> readMap(InputStream is, String location, boolean ignoreEmptyText) {
        try {
            XMLMapper mapper = new XMLMapper();
            mapper.setIgnoreEmptyText(ignoreEmptyText);
            mapper.initializeXml(is);
            Map<String, Object> res = mapper.internalReadMap();
            mapper.finalizeXml();

            return res;
        } catch (Exception ex) {
            throw new LameRuntimeException("error converting content (location: " + location + ") to map: " + ex.getMessage(), ex);
        }
    }

    public static Map<String, Object> readMap(InputStream is, String location) {
        return readMap(is, location, false);
    }

    public String getNodeName() throws XmlPullParserException, IOException {
        return parser.getEventType() == XmlPullParser.TEXT ? null : parser.getName();
    }

    public Map<String, Object> getNodeNameAndAttrs() throws XmlPullParserException, IOException {
        if (getSkipMode()) {
            return null;
        }

        Map<String, Object> res = new LinkedHashMap<String, Object>();

        res.put("#name", parser.getName());

        int attrCount = parser.getAttributeCount();
        for (int i = 0; i < attrCount; i++) {
            res.put(parser.getAttributeName(i), parser.getAttributeValue(i));
        }

        return res;
    }

    public int getNodeType() throws XmlPullParserException {
        return parser.getEventType();
    }

    public void printNodeType() throws XmlPullParserException {
        System.out.println("node type: " + getNodeType());
    }

    public void skipEmptyTextNodes() throws XmlPullParserException, IOException {
        boolean isEmpty = true;

        while (isEmpty && parser.getEventType() == XmlPullParser.TEXT) {
            String txt = parser.getText();
            if (txt == null || txt.trim().length() == 0) {
                parser.next();
            } else {
                isEmpty = false;
            }
        }
    }

    public Object readSubNode() throws XmlPullParserException, IOException {
        Object childNode;

        if (ignoreEmptyText) {
            skipEmptyTextNodes();
        }

        if (parser.getEventType() == XmlPullParser.TEXT) {
            String txt = parser.getText();
            childNode = txt;
            parser.next();
        } else if (parser.getEventType() == XmlPullParser.START_TAG) {
            childNode = internalReadMap();
        } else {
            throw new LameRuntimeException("unexpected event type: " + parser.getEventType());
        }

        if (ignoreEmptyText) {
            skipEmptyTextNodes();
        }

        return childNode;
    }

    public void startWithSubNodes() throws XmlPullParserException, IOException {
        parser.next();
        if (ignoreEmptyText) {
            skipEmptyTextNodes();
        }
    }

    public void endWithSubNodes() throws XmlPullParserException, IOException {
        parser.next();
    }

    public boolean hasMoreSubNodes() throws XmlPullParserException, IOException {
        if (ignoreEmptyText) {
            skipEmptyTextNodes();
        }
        return parser.getEventType() != XmlPullParser.END_TAG;
    }

    public List<Object> readSubNodes() throws XmlPullParserException, IOException {
        List<Object> subNodes = null;

        startWithSubNodes();

        while (hasMoreSubNodes()) {
            Object childNode = readSubNode();

            if (!getSkipMode()) {
                if (subNodes == null) {
                    subNodes = new ArrayList<Object>();
                }
                subNodes.add(childNode);
            }
        }

        endWithSubNodes();

        return subNodes;
    }

    public Map<String, Object> internalReadMap() throws XmlPullParserException, IOException {
        Map<String, Object> res = getNodeNameAndAttrs();

        List subNodes = readSubNodes();
        if (subNodes != null) {
            res.put("#subNodes", subNodes);
        }

        return res;
    }

//    public static Map<String, Object> readMapOld(Reader content) {
//        try {
//            KXmlParser parser = new KXmlParser();
//            parser.setInput(content);
//            if (logger.isInfoEnabled())
//                logger.info("parser encoding 1: " + parser.getInputEncoding());
//
//            Map<String, Object> res;
//
//            parser.require(XmlPullParser.START_DOCUMENT, null, null);
//            if (logger.isInfoEnabled())
//                logger.info("parser encoding 1.1: " + parser.getInputEncoding());
//            parser.nextTag();
//            if (logger.isInfoEnabled())
//                logger.info("parser encoding 2: " + parser.getInputEncoding());
//            res = internalReadMap(parser);
//            if (logger.isInfoEnabled())
//                logger.info("parser encoding 3: " + parser.getInputEncoding());
//            parser.require(XmlPullParser.END_DOCUMENT, null, null);
//
//            return res;
//        } catch (Exception ex) {
//            throw new LameRuntimeException("error converting content to map", ex);
//        }
//    }
//    public static Map<String, Object> readMapXXX(Reader content) {
//        try {
//            return readMap(new ByteArrayInputStream(LameUtils.readerToString(content).getBytes()));
//        /*
//            KXmlParser parser = new KXmlParser();
//            parser.setInput(content);
//            if (logger.isInfoEnabled())
//                logger.info("parser encoding 1: " + parser.getInputEncoding());
//
//            Map<String, Object> res;
//
//            parser.require(XmlPullParser.START_DOCUMENT, null, null);
//            if (logger.isInfoEnabled())
//                logger.info("parser encoding 1.1: " + parser.getInputEncoding());
//            parser.nextTag();
//            if (logger.isInfoEnabled())
//                logger.info("parser encoding 2: " + parser.getInputEncoding());
//            res = internalReadMap(parser);
//            if (logger.isInfoEnabled())
//                logger.info("parser encoding 3: " + parser.getInputEncoding());
//            parser.require(XmlPullParser.END_DOCUMENT, null, null);
//
//            return res;
//         */
//        } catch (Exception ex) {
//            throw new LameRuntimeException("error converting content to map", ex);
//        }
//    }
    public boolean getSkipMode() {
        return skipMode;
    }

    public void setSkipMode(boolean skipMode) {
        this.skipMode = skipMode;
    }

    public boolean getIgnoreEmptyText() {
        return ignoreEmptyText;
    }

    public void setIgnoreEmptyText(boolean ignoreEmptyText) {
        this.ignoreEmptyText = ignoreEmptyText;
    }
    public static final Projector<Map<String, String>, String> NodeNameProjector = new MapProjector<String, String>("#name");
}
