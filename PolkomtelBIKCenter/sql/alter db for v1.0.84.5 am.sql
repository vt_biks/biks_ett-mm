﻿exec sp_check_version '1.0.84.5';
go

drop table bik_sapbo_universe_column
go

drop table bik_sapbo_universe_table;
go

create table bik_sapbo_universe_table(
	id int identity(1,1) not null primary key,
	name varchar(255) not null,
	is_alias int not null check (is_alias in (0,1)),
	is_derived int not null check (is_derived in (0,1)),
	original_table int, 
	sql_of_derived_table varchar(max) null,
	sql_of_derived_table_with_alias varchar(max) null,
	their_id int not null,
	universe_node_id int not null references bik_node(id)
)
go
create table bik_sapbo_universe_column(
	id int identity(1,1) not null primary key,
	name varchar(255) not null,
	type varchar(255) not null,
	table_id int not null references bik_sapbo_universe_table(id)
)
go

alter table bik_sapbo_universe_column 
	add constraint UNIQ_unique_name_id_bik_sapbo_universe_column unique(name, table_id)
go

alter table bik_sapbo_universe_table
	add constraint UNIQ_unique_their_id_bik_sapbo_universe_table unique(their_id, universe_node_id)
go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_objects_from_Designer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_into_bik_node_objects_from_Designer]
GO

create procedure [dbo].[sp_insert_into_bik_node_objects_from_Designer]
as
	declare @tree_id int;
	declare @kind_id int;
	declare @tabele_node_kind_id int;
	declare @column_node_kind_id int;
	declare @dimension_nki int, @measure_nki int, @detail_nki int;
begin--begin procedure

	----wybranie id odpowiedniego drzewa
	select @tree_id = id
	from bik_tree
	where code = 'ObjectUniverses';	
	
	----wybranie id odpowiedniego kind'a
	select @kind_id = id
	from bik_node_kind
	where code = 'UniverseClass';
		
	create table #tmpNodes (si_id varchar(1000) , si_parentid varchar(1000), si_kind_id int, si_name varchar(max), si_description varchar(max));

	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Class
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id), 
		case
			when uc.parent_id is null then convert(varchar, u.global_props_si_id)
			else convert(varchar, uc.parent_id) + '|' + convert(varchar, u.global_props_si_id) end as parent_id, 
		@kind_id, uc.name, uc.description
	from aaa_universe_class uc join aaa_universe u on u.id = uc.universe_id 
	
	----wybranie id odpowiedniego kind'a
	--detail
	select @detail_nki = id
	from bik_node_kind
	where code = 'Detail';
	--demension
	select @dimension_nki = id
	from bik_node_kind
	where code = 'Dimension';
	--measure
	select @measure_nki = id
	from bik_node_kind
	where code = 'Measure';
	
	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Obj
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select convert(varchar, uo.si_id) + '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id), 
			case
				when uo.parent_id is null then convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id)
				else convert(varchar, uo.parent_id)+ '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id)
			end as parent_id, 
			case
				when uo.qualification = 'dsDetailObject' then @detail_nki
				when uo.qualification = 'dsMeasureObject' then @measure_nki
				when uo.qualification = 'dsDimensionObject' then @dimension_nki
				end as node_kind_id,
		 uo.name, uo.description
	from aaa_universe_obj uo join aaa_universe_class uc on uo.universe_class_id = uc.id
							 join aaa_universe u on u.id = uc.universe_id 

	--aktualizacja istniejących węzłów w drzewie							
	update bik_node 
	set parent_node_id = null, node_kind_id = #tmpNodes.si_kind_id, name = #tmpNodes.si_name,
		is_deleted = 0, descr = #tmpNodes.si_description, tree_id = @tree_id
	from #tmpNodes 
	where bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id and bik_node.linked_node_id is null;
	
	--dorzucanie nowych węzłów w drzewie
	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select #tmpNodes.si_kind_id, #tmpNodes.si_name, @tree_id, #tmpNodes.si_id, #tmpNodes.si_description
	from #tmpNodes left join bik_node on bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id
		and bik_node.tree_id = @tree_id
	where bik_node.id is null;	
	
	--uaktualnianie parentów w nodach
	declare @universe_kind_id int;
	select @universe_kind_id = id
	from bik_node_kind
	where code = 'Universe';
	
	update bik_node 
	set parent_node_id= bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id and bik_node.tree_id = @tree_id and bik_node.node_kind_id = pt.si_kind_id
		inner join bik_node bk on bk.obj_id = pt.si_parentid and bk.tree_id = @tree_id 
		and  ((pt.si_kind_id=@column_node_kind_id and bk.node_kind_id = @tabele_node_kind_id) or
		 (pt.si_kind_id=@detail_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @kind_id)) or
		 (pt.si_kind_id=@dimension_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @kind_id)) or
		 (pt.si_kind_id=@measure_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @kind_id)) or
		 (pt.si_kind_id=@tabele_node_kind_id and bk.node_kind_id = @universe_kind_id)or
		 (pt.si_kind_id=@kind_id and (bk.node_kind_id = @kind_id or bk.node_kind_id = @universe_kind_id)))
	where bik_node.linked_node_id is null;
		
	--usuwanie nodów
	update bik_node
	set is_deleted = 1
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and bik_node.node_kind_id in (@kind_id, @tabele_node_kind_id, @detail_nki, @dimension_nki, @measure_nki,
	 @column_node_kind_id);
	
	drop table #tmpNodes;
	
	create table #tmp(table_node_id int, obj_node_id int)
	
	
	insert into #tmp(table_node_id, obj_node_id)
	select bn.id, bn1.id
	from bik_node bn join (		
					select  convert(varchar, ut.their_id) + '|' + convert(varchar, u.global_props_si_id) as table_id, 
							convert(varchar, uo.si_id) + '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u1.global_props_si_id) as obj_id
					from aaa_universe_obj_tables uob join aaa_universe_table ut on uob.universe_table_id = ut.id
 						join aaa_universe u on ut.universe_id = u.id
 						join aaa_universe_obj  uo on uo.id = uob.universe_obj_id
 						join aaa_universe_class uc on uo.universe_class_id = uc.id
						join aaa_universe u1 on u1.id = uc.universe_id ) ot on bn.obj_id = ot.table_id and bn.node_kind_id = @tabele_node_kind_id
						join bik_node bn1 on bn1.obj_id = ot.obj_id and bn1.node_kind_id in (@measure_nki, @detail_nki, @dimension_nki) 

	delete from bik_joined_objs
	from bik_joined_objs join bik_node n on bik_joined_objs.dst_node_id = n.id and n.node_kind_id in (@tabele_node_kind_id, @measure_nki, @detail_nki, @dimension_nki)
	
	----teraz insert
	----
	insert into bik_joined_objs(src_node_id, dst_node_id, type)
	select #tmp.table_node_id, #tmp.obj_node_id, 1
	from #tmp 
		
	insert into bik_joined_objs(src_node_id, dst_node_id, type)
	select #tmp.obj_node_id, #tmp.table_node_id, 1
	from #tmp  	
	
	drop table #tmp
	
	--uzupełnianie danych w tabelach extra 
	--dla object
	create table #tmpObjExtra(text_of_select varchar(max),text_of_where varchar(max), type varchar(155), aggregate_function varchar(50), node_id int)
	
	insert into #tmpObjExtra(text_of_select, text_of_where, type, aggregate_function, node_id)
	select univ_obj.text_of_select, univ_obj.text_of_where, univ_obj.type, univ_obj.aggregate_function, bik_node.id
	from (
	select convert(varchar, uo.si_id) + '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id) as obj_id, 
					uo.text_of_select, uo.text_of_where, uo.type, uo.aggregate_function
			from aaa_universe_obj uo join aaa_universe_class uc on uo.universe_class_id = uc.id
							 join aaa_universe u on u.id = uc.universe_id ) univ_obj 
			join bik_node on bik_node.obj_id = univ_obj.obj_id and bik_node.node_kind_id in (@measure_nki, @detail_nki, @dimension_nki)-- = @obj_node_kind_id;
			
	update bik_sapbo_universe_object
	set text_of_select = #tmpObjExtra.text_of_select, text_of_where = #tmpObjExtra.text_of_where, type = #tmpObjExtra.type,  
		           aggregate_function = #tmpObjExtra.aggregate_function 
	from #tmpObjExtra join bik_sapbo_universe_object on bik_sapbo_universe_object.node_id = #tmpObjExtra.node_id;
	
	insert into bik_sapbo_universe_object(text_of_select, text_of_where, type, aggregate_function, node_id)
	select #tmpObjExtra.text_of_select,#tmpObjExtra.text_of_where, #tmpObjExtra.type, #tmpObjExtra.aggregate_function, #tmpObjExtra.node_id 
	from #tmpObjExtra left join bik_sapbo_universe_object on bik_sapbo_universe_object.node_id = #tmpObjExtra.node_id
	where bik_sapbo_universe_object.id is null;

	drop table #tmpObjExtra;
	
	--dla tabeli
	
	update bik_sapbo_universe_table
	set name = ut.name, is_alias = ut.is_alias, is_derived = ut.is_derived, original_table = ut.original_table, sql_of_derived_table = ut.sql_of_derived_table, 
		sql_of_derived_table_with_alias = ut.sql_of_derived_table_with_alias, their_id = ut.their_id, universe_node_id = bik_node.id
	from aaa_universe_table ut join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
	where linked_node_id is null and bik_node.node_kind_id = @universe_kind_id
	
	insert into bik_sapbo_universe_table(name, is_alias, is_derived, original_table, sql_of_derived_table, sql_of_derived_table_with_alias, their_id, universe_node_id)
	select ut.name, ut.is_alias, ut.is_derived, ut.original_table, ut.sql_of_derived_table, ut.sql_of_derived_table_with_alias, ut.their_id, 
					bik_node.id as universe_node_id
	from aaa_universe_table ut join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			left join bik_sapbo_universe_table bsut 
		on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
	where  bik_node.linked_node_id is null and bsut.name is null and bik_node.node_kind_id = @universe_kind_id

	delete 
	from bik_sapbo_universe_table
	from aaa_universe_table ut join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			left join bik_sapbo_universe_table bsut 
		on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
	where  bik_node.linked_node_id is null and ut.name is null and bik_node.node_kind_id = @universe_kind_id
	
	--dla kolumn
	update bik_sapbo_universe_column
	set name = uc.name, type = uc.type, table_id = bsut.id
	from aaa_universe_column uc join aaa_universe_table ut on uc.universe_table_id = ut.id
			join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
			join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bsut.id
	where linked_node_id is null and bik_node.node_kind_id = @universe_kind_id 
	
	insert into bik_sapbo_universe_column(name, type, table_id)
	select uc.name, uc.type, bsut.id
	from aaa_universe_column uc join aaa_universe_table ut on uc.universe_table_id = ut.id
			join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
			left join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bsut.id
	where linked_node_id is null and bsuc.id is null and bik_node.node_kind_id = @universe_kind_id 
	
	delete 
	from bik_sapbo_universe_table
	from aaa_universe_column uc join aaa_universe_table ut on uc.universe_table_id = ut.id
			join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
			left join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bsut.id
	where linked_node_id is null and uc.id is null and bik_node.node_kind_id = @universe_kind_id
	
	update bik_sapbo_universe_connection 
	set bik_sapbo_universe_connection.server = aaa_universe_connetion.server, bik_sapbo_universe_connection.user_name = aaa_universe_connetion.user_name,
		bik_sapbo_universe_connection.password = aaa_universe_connetion.password, bik_sapbo_universe_connection.database_source = aaa_universe_connetion.database_source, 
		bik_sapbo_universe_connection.connetion_networklayer_name = aaa_universe_connetion_networklayer.name, bik_sapbo_universe_connection.node_id = bik_node.id
	from aaa_universe_connetion join aaa_universe_connetion_networklayer on aaa_universe_connetion.connetion_networklayer_id = aaa_universe_connetion_networklayer.id
		join bik_node on aaa_universe_connetion.connetion_name = bik_node.name
		join bik_tree on bik_node.tree_id = bik_tree.id
		join bik_sapbo_universe_connection on  bik_sapbo_universe_connection.node_id = bik_node.id
	where bik_tree.code = 'Connections'
	
	insert into bik_sapbo_universe_connection (server, user_name, password, database_source, connetion_networklayer_name, node_id)
	select aaa_universe_connetion.server, aaa_universe_connetion.user_name, aaa_universe_connetion.password, aaa_universe_connetion.database_source, 
		aaa_universe_connetion_networklayer.name, bik_node.id
	from aaa_universe_connetion join aaa_universe_connetion_networklayer on aaa_universe_connetion.connetion_networklayer_id = aaa_universe_connetion_networklayer.id
		join bik_node on aaa_universe_connetion.connetion_name = bik_node.name
		join bik_tree on bik_node.tree_id = bik_tree.id
		left join bik_sapbo_universe_connection on  bik_sapbo_universe_connection.node_id = bik_node.id
	where bik_tree.code = 'Connections' and bik_sapbo_universe_connection.id is null;
				
	exec sp_delete_linked_nodes_where_orignal_is_deleted;
end--end procedure
GO

---exec sp_insert_into_bik_node_objects_from_Designer
delete from bik_node
where node_kind_id = (select id from bik_node_kind
where code = 'UniverseColumn')


delete from bik_joined_objs
from bik_joined_objs join bik_node on bik_joined_objs.src_node_id = bik_node.id 
	join bik_node_kind on bik_node.node_kind_id = bik_node_kind.id
where bik_node_kind.code = 'UniverseTable'

delete from bik_joined_objs
from bik_joined_objs join bik_node on bik_joined_objs.dst_node_id = bik_node.id 
	join bik_node_kind on bik_node.node_kind_id = bik_node_kind.id
where bik_node_kind.code = 'UniverseTable'

delete from bik_node
where node_kind_id = (select id from bik_node_kind
where code = 'UniverseTable')

delete from bik_node_kind
where code = 'UniverseColumn' or code = 'UniverseTable'
go


exec sp_update_version '1.0.84.5', '1.0.85';
go

