/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.Iterator;

/**
 *
 * @author wezyr
 */
public class SimpleArrayIterator<T> implements Iterator<T> {

    private T[] array;
    private int length;
    private int idx = 0;

    public SimpleArrayIterator(T... array) {
        this.array = array;
        this.length = array.length;
    }

    public boolean hasNext() {
        return idx < length;
    }

    @SuppressWarnings("unchecked")
    public T next() {
        if (hasNext()) {
            return array[idx++];
        }
        throw new IllegalStateException("no more elements");
    }

    public void remove() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
