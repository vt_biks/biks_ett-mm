/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.http.client.Request;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.entitydetailswidgets.AdminConfigADWidget;
import pl.fovis.client.entitydetailswidgets.AdminConfigConfluenceWidget;
import pl.fovis.client.entitydetailswidgets.AdminConfigDQCWidget;
import pl.fovis.client.entitydetailswidgets.AdminConfigDQMWidget;
import pl.fovis.client.entitydetailswidgets.AdminConfigDynamicAxWidget;
import pl.fovis.client.entitydetailswidgets.AdminConfigErwinWidget;
import pl.fovis.client.entitydetailswidgets.AdminConfigFileSystemWidget;
import pl.fovis.client.entitydetailswidgets.AdminConfigGeneralJdbcWidget;
import pl.fovis.client.entitydetailswidgets.AdminConfigMsSqlExWidget;
import pl.fovis.client.entitydetailswidgets.AdminConfigOracleWidget;
import pl.fovis.client.entitydetailswidgets.AdminConfigAnySqlWidget;
import pl.fovis.client.entitydetailswidgets.AdminConfigPlainFileWidget;
import pl.fovis.client.entitydetailswidgets.AdminConfigPostgreSQLWidget;
import pl.fovis.client.entitydetailswidgets.AdminConfigProfileWidget;
import pl.fovis.client.entitydetailswidgets.AdminConfigSASWidget;
import pl.fovis.client.entitydetailswidgets.AdminConfigSapBWWidget;
import pl.fovis.client.entitydetailswidgets.AdminConfigSapBo4ExWidget;
import pl.fovis.client.entitydetailswidgets.AdminConfigSapBoExWidget;
import pl.fovis.client.entitydetailswidgets.AdminConfigTeradataWidget;
import pl.fovis.common.BISystemsConnectionsParametersBean;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author tflorczak
 */
public class DataPumpPage extends BikPageStandardBase<BISystemsConnectionsParametersBean> {

    protected AdminConfigSapBoExWidget sapboWidget;
    protected AdminConfigSapBo4ExWidget sapbo4Widget;
    protected AdminConfigTeradataWidget teradataWidget;
    protected AdminConfigDQCWidget dqcWidget;
    protected AdminConfigOracleWidget oracleWidget;
    protected AdminConfigPostgreSQLWidget postgresWidget;
    protected AdminConfigMsSqlExWidget mssqlWidget;
    protected AdminConfigADWidget adWidget;
    protected AdminConfigSASWidget sasWidget;
    protected AdminConfigSapBWWidget sapbwWidget;
    protected AdminConfigProfileWidget profileWidget;
    protected AdminConfigErwinWidget erwinWidget;
    protected AdminConfigConfluenceWidget confluenceWidget;
    protected AdminConfigDQMWidget dqmWidget;
    protected AdminConfigGeneralJdbcWidget jdbcWidget;
    protected AdminConfigFileSystemWidget fileSystemWidget;
    protected AdminConfigPlainFileWidget plainFileWidget;
    protected AdminConfigDynamicAxWidget dynamicAxWidget;
    protected AdminConfigAnySqlWidget permissionsWidget;
    protected TabPanel main;

    @Override
    protected Widget buildWidgets() {
        /*TabPanel*/ main = new TabPanel();
        main.setAnimationEnabled(true);
        main.setHeight("100%");
        main.setWidth("100%");
        main.addStyleName("mainConnConf");

        sapboWidget = new AdminConfigSapBoExWidget();
        sapbo4Widget = new AdminConfigSapBo4ExWidget();
        teradataWidget = new AdminConfigTeradataWidget();
        dqcWidget = new AdminConfigDQCWidget();
        oracleWidget = new AdminConfigOracleWidget();
        postgresWidget = new AdminConfigPostgreSQLWidget();
        mssqlWidget = new AdminConfigMsSqlExWidget();
        adWidget = new AdminConfigADWidget();
        sasWidget = new AdminConfigSASWidget();
        sapbwWidget = new AdminConfigSapBWWidget();
        profileWidget = new AdminConfigProfileWidget();
        erwinWidget = new AdminConfigErwinWidget();
        confluenceWidget = new AdminConfigConfluenceWidget();
        dqmWidget = new AdminConfigDQMWidget();
        jdbcWidget = new AdminConfigGeneralJdbcWidget();
        fileSystemWidget = new AdminConfigFileSystemWidget();
        plainFileWidget = new AdminConfigPlainFileWidget();
        dynamicAxWidget = new AdminConfigDynamicAxWidget();
        permissionsWidget=new AdminConfigAnySqlWidget();

        addTab(sapboWidget.buildWidget(), sapboWidget.getSourceName());
        addTab(sapbo4Widget.buildWidget(), sapbo4Widget.getSourceName());
        addTab(teradataWidget.buildWidget(), teradataWidget.getSourceName());
        addTab(dqcWidget.buildWidget(), dqcWidget.getSourceName());
        addTab(oracleWidget.buildWidget(), oracleWidget.getSourceName());
        addTab(postgresWidget.buildWidget(), postgresWidget.getSourceName());
        addTab(mssqlWidget.buildWidget(), mssqlWidget.getSourceName());
        addTab(adWidget.buildWidget(), adWidget.getSourceName());
        addTab(sasWidget.buildWidget(), sasWidget.getSourceName());
        addTab(sapbwWidget.buildWidget(), sapbwWidget.getSourceName());
        addTab(profileWidget.buildWidget(), profileWidget.getSourceName());
        addTab(erwinWidget.buildWidget(), erwinWidget.getSourceName());
        addTab(confluenceWidget.buildWidget(), confluenceWidget.getSourceName());
        addTab(jdbcWidget.buildWidget(), jdbcWidget.getSourceName());
        addTab(fileSystemWidget.buildWidget(), fileSystemWidget.getSourceName());
        addTab(plainFileWidget.buildWidget(), plainFileWidget.getSourceName());
        addTab(dynamicAxWidget.buildWidget(), dynamicAxWidget.getSourceName());
        addTab(permissionsWidget.buildWidget(), permissionsWidget.getSourceName());
        addTab(dqmWidget.buildWidget(), dqmWidget.getSourceName());
        if (main.getTabBar().getTab(0) == null) {
            return new Label(I18n.brakKonektorow.get() /* I18N:  */ + "!");
        }
        main.selectTab(0);
        return main;
    }

    protected void addTab(Widget w, String sourceName) {
//        if (BIKClientSingletons.availableConnectors().contains(sorceName)) {
        if (BIKClientSingletons.isConnectorAvailable(sourceName)) {
            main.add(createMainScrollPanel(w, 35), sourceName);
        }
    }

    @Override
    public String getId() {
        return "admin:load:cfg";
    }

//    public String getCaptionDeprecated() {
//        return I18n.konfiguracjaPolaczen.get() /* I18N:  */;
//    }
    @Override
    protected Request callServiceToGetData(AsyncCallback<BISystemsConnectionsParametersBean> callback) {
        return BIKClientSingletons.getService().getBISystemsConnectionsParameters(callback);
    }

    @Override
    protected void populateWidgetsWithData(BISystemsConnectionsParametersBean data/*, boolean hasIdToSelect*/) {
        sapboWidget.populateWidgetWithData(data.sapboCPB);
        sapbo4Widget.populateWidgetWithData(data.sapbo4CPB);
        teradataWidget.populateWidgetWithData(data.teradataCPB);
        dqcWidget.populateWidgetWithData(data.dqcCPB);
        oracleWidget.populateWidgetWithData(data.oracleCPB);
        postgresWidget.populateWidgetWithData(data.postgresCPB);
        mssqlWidget.populateWidgetWithData(data.mssqlCPB);
        jdbcWidget.populateWidgetWithData(data.jdbcCPB);
        fileSystemWidget.populateWidgetWithData(data.fileSystemCPB);
        plainFileWidget.populateWidgetWithData(data.plainFileCPB);
        dynamicAxWidget.populateWidgetWithData(data.dynamicAxCPB);
        adWidget.populateWidgetWithData(data.adCPB);
        sasWidget.populateWidgetWithData(data.sasCPB);
        sapbwWidget.populateWidgetWithData(data.sapbwCPB);
        profileWidget.populateWidgetWithData(data.profileCPB);
        erwinWidget.populateWidgetWithData(data.erwinCPB);
        confluenceWidget.populateWidgetWithData(data.confluenceCPB);
        dqmWidget.populateWidgetWithData(data.dqmCPB);
        permissionsWidget.populateWidgetWithData(data.anySqlsPB);
    }
}
