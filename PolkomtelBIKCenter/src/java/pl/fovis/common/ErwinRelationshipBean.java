/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author pgajda
 */
public class ErwinRelationshipBean implements Serializable{
    public String relationshipName;
    public String parentTableLongId;
    public String childTableLongId;
    public String parentColLongId;
    public String childColLongId;
}
