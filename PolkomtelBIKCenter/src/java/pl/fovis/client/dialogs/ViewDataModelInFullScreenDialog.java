/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.datamodel.DataModel;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.foxygwtcommons.ViewSomethingInFullScreenDialog;

/**
 *
 * @author pgajda
 */
public class ViewDataModelInFullScreenDialog extends ViewSomethingInFullScreenDialog {

    protected DataModel dataModelWidget;

    public ViewDataModelInFullScreenDialog(EntityDetailsDataBean eddb) {
        dataModelWidget = new DataModel(eddb, true, null);
    }

    public ViewDataModelInFullScreenDialog(EntityDetailsDataBean eddb, String preferredSubjectAreaLongIdToFirstShow) {
        dataModelWidget = new DataModel(eddb, true, preferredSubjectAreaLongIdToFirstShow);
    }

    @Override
    protected Widget getContentWidget() {
        main.add(dataModelWidget.buildSimleContlolWidgets());
        Widget widget = dataModelWidget.buildWidgets();
        dataModelWidget.displayData();

        return widget;
    }

    @Override
    protected String getDialogCaption() {
        return "Data Model";
    }
}
