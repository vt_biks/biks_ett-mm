/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Set;
import simplelib.LameRuntimeException;

/**
 *
 * @author wezyr
 */
public class LameFileResourceProvider implements ILameResourceProvider {

    public Set<String> getResourceNames(String base, Set<String> fileExts, boolean recurseSubDirs) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public InputStream gainResource(String resourceName) {
        InputStream is;
        try {
            is = new FileInputStream(resourceName);
        } catch (Exception ex) {
            throw new LameRuntimeException("error while gaining resource: " + resourceName, ex);
        }
        return is;
    }

    public long getLastModifiedOfResource(String resourceName) {
        File f = new File(resourceName);
        return f.lastModified();
    }
}
