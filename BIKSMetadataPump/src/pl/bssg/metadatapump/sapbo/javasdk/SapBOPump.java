/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.sapbo.javasdk;

import com.crystaldecisions.sdk.framework.CrystalEnterprise;
import com.crystaldecisions.sdk.framework.IEnterpriseSession;
import com.crystaldecisions.sdk.framework.ISessionMgr;
import com.crystaldecisions.sdk.occa.infostore.CePropertyID;
import com.crystaldecisions.sdk.occa.infostore.IInfoObject;
import com.crystaldecisions.sdk.occa.infostore.IInfoObjects;
import com.crystaldecisions.sdk.occa.infostore.IInfoStore;
import com.crystaldecisions.sdk.properties.IProperties;
import com.crystaldecisions.sdk.properties.IProperty;
import commonlib.LameUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.bssg.metadatapump.MetadataPumpBase;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.bssg.metadatapump.common.SAPBOScheduleBean;
import pl.bssg.metadatapump.sapbo.biadmin.ReportScheduleInfo;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import pl.trzy0.foxy.serverlogic.db.SqlValueFormatterCurlyTimeStamp;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class SapBOPump extends MetadataPumpBase {

    protected static final Set<String> USEFUL_TABLE_NAMES = BaseUtils.splitUniqueBySep(
            "INFO_FOLDER, INFO_FULLCLIENT, INFO_WEBI, APP_UNIVERSE, APP_FOLDER, APP_COMMONCONNECTION, APP_METADATA__DATACONNECTION, ", ",", true);
    protected static final boolean ONLY_USEFUL_TABLES = true;
    //---
    private static final ILameLogger logger = LameUtils.getMyLogger();
    public final static String AUTHENTICATION_METHOD = "secEnterprise";
    public final static String FOLDER_CUID_ROOT = "ASHnC0S_Pw5LhKFbZ.iA_j4";
    public final static String FOLDER_CUID_USER = "AWigQI18AAZJoXfRHLzWJ2c";
    public final static String FOLDER_CUID_UNIVERSES = "AWcPjwbDdBxPoXPBOUCsKkk";
    public final static String FOLDER_CUID_CONNECTIONS = "AZVXOgKIBEdOkiXzUuybja4";
    protected static final String DONT_GENERATE_FIELD_TYPE = "-";
    protected static final Map<String, String> classNameToSqlType = new LinkedHashMap<String, String>() {

        {
            put("SDKPropertyBag", DONT_GENERATE_FIELD_TYPE);
            put("Boolean", "int");
            put("String", "varchar(max)");
            put("Integer", "int");
            put("Long", "int");
            put("Date", "datetime");
            put("Double", "float");
        }
    };
    protected static final Set<String> fixedGlobalPropsColumns = BaseUtils.splitUniqueBySep(
            "SI_ID, SI_KIND, SI_NAME, SI_PARENTID, SI_AUTHOR, SI_SIZE, SI_HAS_CHILDREN, SI_CONTENT_LOCALE, SI_WEBI_DOC_PROPERTIES, SI_READ_ONLY, SI_LAST_SUCCESSFUL_INSTANCE_ID, SI_LAST_RUN_TIME, SI_ENDTIME, SI_STARTTIME, SI_SCHEDULE_STATUS, SI_RECURRING, SI_NEXTRUNTIME, SI_DOC_SENDER, SI_OBJECT_IS_CONTAINER", ",", true);
    protected static final Set<String> interestingSiKinds
            = BaseUtils.splitUniqueBySep(
                    "Folder, CrystalReport, Webi, Universe, DSL.MetaDataFile, CCIS.DataConnection, CommonConnection, Connection, "
                    //+ "Pdf, FullClient, Rtf, Txt, User, Flash, Usergroup, Hyperlink, Excel, "
                    + "Pdf, FullClient, Rtf, Txt, Flash, Hyperlink, Excel, "
                    + "Powerpoint, Word, MetaData.DataConnection".toUpperCase(), ",", true);
    protected SqlValueFormatterCurlyTimeStamp mssqlValueFormatter = new SqlValueFormatterCurlyTimeStamp();
    protected String where = createWhere();
    // kind -> fieldName -> [type]
    protected Map<String, Map<String, Set<String>>> propNames;
    // fieldName -> type -> [tabCode.kind]
    protected Map<String, Map<String, Set<String>>> globalPropNames;
    //public static Map<String, Map<String, Set<String>>> propNames;
    protected Map<String, Set<String>> allTablesColNames = new LinkedHashMap<String, Set<String>>();
    protected Set<Pair<String, String>> multiTypeProps;
    protected Set<String> typeNames;
    protected String globalTableCode;
    protected int allCount = 0;
    protected int currentCount = 0;
    protected int insertedLogId = 0;
    protected Map<String, Integer> counts = new HashMap<String, Integer>();
    protected int longColumnNamesCounter = 0;

    public void createBOXITables(IInfoStore iStore, String tableName,
            String tableCode) throws Exception {
        propNames = new LinkedHashMap<String, Map<String, Set<String>>>();
        multiTypeProps = new LinkedHashSet<Pair<String, String>>();
        typeNames = new LinkedHashSet<String>();
        globalTableCode = tableCode;

        Integer siID = null;
        int resultsSize = 0;
        while (true) {
            IInfoObjects results = iStore.query("SELECT top 100 * FROM " + tableName
                    + (siID == null ? " WHERE " + where : " WHERE (si_id > " + siID + ") AND (" + where + ")") + " order by SI_ID"); /*
             * WHERE SI_KIND = 'CrystalReport'
             */

            resultsSize += results.size();
            if (logger.isDebugEnabled()) {
                logger.debug("\n\n---<<" + tableName + ">>--- results: " + results.size() + ", resultSize: " + results.getResultSize());
            }
            currentCount += results.size();
            pumpLogger.pumpLog("Insert into temporary tables - createBOXITables - table: " + tableName + "(" + resultsSize + " / " + counts.get(tableCode) + ")", (currentCount * 100 / allCount));
//            updateMainLog("Pobieranie do tabel tymczasowych: " + (currentCount * 100 / allCount) + "%");
//            insertLogToDB("Pobieranie do tabel tymczasowych - createBOXITables - tabela: " + tableName + "(" + resultsSize + " / " + counts.get(tableCode) + ") - " + (currentCount * 100 / allCount) + "% z calosci");
            for (int i = 0; i < results.size(); i++) {
                IInfoObject obj = (IInfoObject) results.get(i);
                String v = infoObjectToString(obj);
//            System.out.println("#" + i + ": " + //props
//                    v);
                siID = obj.getID(); //properties().getInt("SI_ID");
            }
            if (results.size() < 100) {
                break;
            }
        }

        generateCreateTables(tableCode);
    }

    public void collectValsFromOneBOXIRow(Iterator iter, Set<String> customCols, Map<String, Object> customVals, Set<String> genericCols, Map<String, Object> genericVals,
            String propNamePrefix) {
        if (isPrefixLevelTooHigh(propNamePrefix)) {
            return;
        }
        while (iter.hasNext()) {
            Map.Entry p = (Map.Entry) iter.next();
            Integer id = (Integer) p.getKey();
            String propName = propNamePrefix + CePropertyID.idToName(id).toUpperCase();
            IProperty prop = (IProperty) p.getValue();
            Object val = prop.getValue();
            int isGlobalV = -1;
            String properFldName = fixIdent(propName.toUpperCase().replace(".", "__"));
            if (customCols.contains(properFldName)) {
                customVals.put(properFldName, val);
                isGlobalV = 0;
            }
            if (genericCols.contains(properFldName)) {
                genericVals.put(properFldName, val);
                isGlobalV = 1;
            }

            if (prop.isContainer()) {
                IProperties props = (IProperties) val;
                String newPrefix = propNamePrefix + CePropertyID.idToName(id) + ".";

                collectValsFromOneBOXIRow(props.entrySet().iterator(), customCols, customVals, genericCols, genericVals, newPrefix);
            }
        } // while cols
        // while cols
    }

    public boolean isInterestingKind(String siKind) {
        return interestingSiKinds.isEmpty()
                || siKind != null && interestingSiKinds.contains(siKind.toUpperCase());
    }

    public void storePropMapAsCustomProps(
            String rootSiKind,
            IProperties propMap,
            //Map<String, Object> fixedProps,
            Integer propParentId, String propNamePrefix, int propNestLevel,
            Integer parentSiId) throws IOException {
        if (!isInterestingKind(rootSiKind)) {
            return;
        }

        Iterator iter = propMap.entrySet().iterator();

        //StringBuilder customPropsBuffer = new StringBuilder();
        while (iter.hasNext()) {
            Map.Entry p = (Map.Entry) iter.next();
            Integer id = (Integer) p.getKey();
            String propName = CePropertyID.idToName(id).toUpperCase();
            IProperty prop = (IProperty) p.getValue();
            Object val = prop.getValue();
            int isGlobalV = -2;

            Map<String, Object> customPropVals = new LinkedHashMap<String, Object>();

            Object siIdObj = propMap.getProperty("SI_ID");
//            if (siIdObj != null && !(siIdObj instanceof Number)) {
//                System.out.println("siIdObj is of class: " + BaseUtils.safeGetClassName(siIdObj) + ", val: " + siIdObj);
//            }

            int siId;

            if (parentSiId == null) {
                if (siIdObj != null) {
                    try {
                        siId = (Integer) siIdObj;//propMap.getInt("SI_ID");
                    } catch (Exception ex) {
                        siId = propNestLevel == 0 ? -2 : -3;
                        if (logger.isInfoEnabled()) {
                            logger.info("error: " + ex);
                        }
                    }
                } else {
                    siId = -1;
                }
            } else {
                siId = parentSiId;
            }

            //int siId = siIdObj != null ? propMap.getInt("SI_ID") : -1;
            customPropVals.put("SI_ID", siId);
            customPropVals.put("PROP_NAME", propName);
            String fullPropName = propNamePrefix + propName;
            customPropVals.put("FULL_PROP_NAME", fullPropName);
            customPropVals.put("FIELD_NAME", fixIdent(fullPropName));
            customPropVals.put("PROP_VAL", BaseUtils.safeToString(val));
            customPropVals.put("IS_GLOBAL", isGlobalV);
            customPropVals.put("PROP_PARENT_ID", propParentId);
            customPropVals.put("PROP_CLASS", BaseUtils.safeGetSimpleClassName(val));
            customPropVals.put("PROP_NEST_LEVEL", propNestLevel);

            int propId;
            execCommand(BeanConnectionUtils.generateInsertInto(getMc(),
                    "aaa_custom_prop", customPropVals));
            propId = getMc().execQrySingleValInteger("select @@identity", false);
//            mc.finalizeTran(true);

            if (val instanceof IProperties) {
                IProperties subProps = (IProperties) val;
                storePropMapAsCustomProps(rootSiKind, subProps, propId, fullPropName + ".", propNestLevel + 1, siId);
            }
        } // while cols
    }

    public void importDataToBOXITables(IInfoStore iStore, String tableName,
            String tableCode) throws Exception {
        globalTableCode = tableCode;

        Integer siID = null;
        int resultsSize = 0;
        while (true) {
            IInfoObjects results = iStore.query("SELECT top 100 * FROM " + tableName
                    + (siID == null ? " WHERE " + where : " WHERE (si_id > " + siID + ") AND (" + where + ")") + " order by SI_ID");

//            IInfoObjects results = iStore.query("SELECT top 100 * FROM " + tableName
//                    + (siID == null ? "" : " where si_id > " + siID) + " order by SI_ID"); /* WHERE SI_KIND = 'CrystalReport' */
            if (logger.isDebugEnabled()) {
                logger.debug("\n\n---<<" + tableName + ">>--- results: " + results.size() + ", resultSize: " + results.getResultSize());
            }
            resultsSize += results.size();
            StringBuilder insertsBuffer = new StringBuilder();
            currentCount += results.size();
            pumpLogger.pumpLog("Insert into temporary tables - importDataToBOXITables - table: " + tableName + "(" + resultsSize + " / " + counts.get(tableCode) + ")", (currentCount * 100 / allCount));
//            updateMainLog("Pobieranie do tabel tymczasowych: " + (currentCount * 100 / allCount) + "%");
//            insertLogToDB("Pobieranie do tabel tymczasowych - importDataToBOXITables - tabela: " + tableName + "(" + resultsSize + " / " + counts.get(tableCode) + ") - " + (currentCount * 100 / allCount) + "% z calosci");
            for (int i = 0; i < results.size(); i++) {
                IInfoObject obj = (IInfoObject) results.get(i);
                //IProperties props = obj.properties();
                //String v = infoObjectToString(obj);
//            System.out.println("#" + i + ": " + //props
//                    v);

                IProperties propMap = obj.properties();
                Object kindObj = BaseUtils.safeToString(propMap.getProperty("SI_KIND"));
                String kind = BaseUtils.safeToString(kindObj);

                if (kind != null) {
                    String sqlTableName = getSqlTableName(tableCode, kind);

                    Map<String, Object> customVals = new LinkedHashMap<String, Object>();
                    Map<String, Object> genericVals = new LinkedHashMap<String, Object>();

                    Set<String> customCols = allTablesColNames.get(sqlTableName);
                    Set<String> genericCols = allTablesColNames.get("aaa_global_props".toUpperCase());

                    Iterator iter = propMap.entrySet().iterator();

                    collectValsFromOneBOXIRow(iter, customCols, customVals, genericCols, genericVals,
                            "");

                    if (!ONLY_USEFUL_TABLES) {
                        storePropMapAsCustomProps(kind, propMap, null, "", 0, null);
                    }

                    genericVals.put("table_code".toUpperCase(), tableCode);

//                    if ("APP_UNIVERSE".equals(sqlTableName)) {
//                        System.out.println("app_universe: customCols=" + customCols
//                                + ", genericCols=" + genericCols + ", customVals=" + customVals
//                                + ", genericVals=" + genericVals);
//                    }
                    if (!ONLY_USEFUL_TABLES || USEFUL_TABLE_NAMES.contains(sqlTableName)) {
                        insertsBuffer.append(BeanConnectionUtils.generateInsertInto(getMc(), sqlTableName, customVals)).append(";\n");
                    }
                    insertsBuffer.append(BeanConnectionUtils.generateInsertInto(getMc(), "aaa_global_props", genericVals)).append(";\n");
//                    insertsBuffer.append(customPropsBuffer);
                }

                siID = obj.getID(); //properties().getInt("SI_ID");
            } // for rows

            execCommand(insertsBuffer.toString());

            if (results.size() < 100) {
                break;
            }

            //generateCreateTables(tableCode);
//        System.out.println("table name: " + tableName + ", kinds: " + propNames.keySet()
//                + ", props: " + propNames);
//        System.out.println("types: " + typeNames);
//        System.out.println("multiTypeProps: " + multiTypeProps);
        } // while true
    }

    public String infoObjectToString(IInfoObject obj) {
        return propertyBagToString(obj.properties());
    }

    public String propertyBagToString(IProperties propMap) {
        return propertyBagToString(propMap, "", null);
    }

    public boolean isPrefixLevelTooHigh(String prefix) {
        int level = BaseUtils.countSubstrings(prefix, ".");
        return level > 2;
    }

    public String propertyBagToString(IProperties propMap, String prefix, String parentKind) {
//        int level = BaseUtils.countSubstrings(prefix, ".");
//
//        if (level > 2) {
//            return "";
//        }
        if (isPrefixLevelTooHigh(prefix)) {
            return "";
        }

        StringBuilder buff = new StringBuilder();
        //buff.setLength(0);

        String kind;
        if (parentKind == null) {
            Object kindObj = BaseUtils.safeToString(propMap.getProperty("SI_KIND"));
            kind = BaseUtils.safeToString(kindObj);
        } else {
            kind = parentKind;
        }

        if ((propMap != null) && (propMap.size() > 0)) {
            Iterator iter = propMap.entrySet().iterator();

            while (iter.hasNext()) {
                Map.Entry p = (Map.Entry) iter.next();
                Integer id = (Integer) p.getKey();
                String propName = prefix + CePropertyID.idToName(id);
                buff.append(propName);
                IProperty prop = (IProperty) p.getValue();
                Object val = prop.getValue();

                if (true || prefix.isEmpty()) {
                    if (kind != null) {
                        Map<String, Set<String>> flds = propNames.get(kind);

                        if (flds == null) {
                            flds = new LinkedHashMap<String, Set<String>>();
                            propNames.put(kind, flds);
                        }

                        Set<String> types = flds.get(propName);

                        if (types == null) {
                            types = new LinkedHashSet<String>();
                            flds.put(propName, types);
                        }

                        String typeName = BaseUtils.safeGetSimpleClassName(val);
                        if (types.isEmpty() || val != null) {
                            types.add(typeName);
                        }

                        if (types.size() > 1 && types.contains(BaseUtils.NULL_CLASS_NAME)) {
                            types.remove(BaseUtils.NULL_CLASS_NAME);
                        }

                        if (types.size() > 1) {
                            multiTypeProps.add(new Pair<String, String>(kind, propName));
                        }

                        typeNames.add(typeName);

                        if (val != null) {
                            Map<String, Set<String>> typeOccs = globalPropNames.get(propName);
                            if (typeOccs == null) {
                                typeOccs = new LinkedHashMap<String, Set<String>>();
                                globalPropNames.put(propName, typeOccs);
                            }

                            //typeOccs.put(new Pair<String, String>(typeName, globalTableCode + "." + kind));
                            Set<String> oneTypeOcc = typeOccs.get(typeName);
                            if (oneTypeOcc == null) {
                                oneTypeOcc = new LinkedHashSet<String>();
                                typeOccs.put(typeName, oneTypeOcc);
                            }
                            oneTypeOcc.add(globalTableCode + ":" + kind);
                        }
                    }
                }

                if (prop.isContainer()) {
                    IProperties props = (IProperties) val;
                    buff.append("\n");
                    String newPrefix = prefix + CePropertyID.idToName(id) + ".";
//                    System.out.println("*@*@*@* subproperty with name=" + propName + ", calling with prefix: " + newPrefix);
                    buff.append(propertyBagToString(props, newPrefix, kind));
                } else {
                    buff.append("=").append(val).append("\n");
                }
            }
        }
        return buff.toString();
    }

    // nazwa tabeli będzie uppercasem
    public String getSqlTableName(String tabNamePrefix, String kind) {
        return new StringBuilder().append(tabNamePrefix).append("_").append(kind.replace(".", "__")).toString().toUpperCase();
    }

    public void generateCreateTables(String tabNamePrefix) throws IOException {
        StringBuilder sb = new StringBuilder();
        for (String kind : propNames.keySet()) {
            Set<String> tabColNames = new LinkedHashSet<String>();

            String tableName = getSqlTableName(tabNamePrefix, kind);

            boolean generateThisTable = !ONLY_USEFUL_TABLES || USEFUL_TABLE_NAMES.contains(tableName);

            if (generateThisTable) {
                dropTableIfExists(tableName);
                sb.append("create table ").append(tableName).append(" (\n");
            }

            int colCnt = 0;

            for (Map.Entry<String, Set<String>> e : propNames.get(kind).entrySet()) {
                String fldName = e.getKey();

                Set<String> classes = e.getValue();

                if (classes.size() > 1 || (classes.size() == 1 && classes.contains(BaseUtils.NULL_CLASS_NAME))) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("-- fld: " + kind + "." + fldName + " has inproper classes: " + classes);
                    }
                } else {
                    String className = classes.iterator().next();
                    String sqlType = classNameToSqlType.get(className);
                    if (sqlType == null) {
                        if (logger.isDebugEnabled()) {
                            logger.debug("-- fld: " + kind + "." + fldName + " has inproper classes: " + classes + ", no sql datatype!");
                        }
                    } else {
                        String properFldName = fixIdent(fldName.toUpperCase().replace(".", "__"));
                        if (properFldName.length() > 128) {
                            properFldName = properFldName.substring(0, 120) + "_LONG" + longColumnNamesCounter++;
                        }
                        if (!fldName.equalsIgnoreCase("SI_ID") && !BaseUtils.safeEquals(sqlType, DONT_GENERATE_FIELD_TYPE)) {
                            if (colCnt < 1000) {
                                if (generateThisTable) {
                                    sb.append("  ").append(properFldName).append(" ").append(sqlType).append(" null,\n");
                                }
                                tabColNames.add(//fldName.toUpperCase()
                                        properFldName);
                                colCnt++;
                            } else {
                                if (logger.isDebugEnabled()) {
                                    logger.debug("-- fld: " + kind + "." + fldName + " exceeds colCnt = 1000");
                                }
                            }
                        }
                    }
                }
            }

            if (generateThisTable) {
                sb.append("  si_id int not null primary key\n);\n\n");
            }
            tabColNames.add("si_id".toUpperCase());

            allTablesColNames.put(tableName, tabColNames);
        }

        if (sb.length() > 0) {
            execCommand(sb.toString()); // here leci except
        }
    }

    public void dropTableIfExists(String tabName) throws IOException {
        execCommand("IF OBJECT_ID (N'" + tabName + "', N'U') IS NOT NULL drop table " + tabName);
    }

    private void generateGlobalTable() throws IOException {
        StringBuilder tabSB = new StringBuilder();
        //mc.execCommand("\n\nIF OBJECT_ID (N'aaa_global_props', N'U') IS NOT NULL drop table aaa_global_props");
        dropTableIfExists("aaa_global_props");
        if (!ONLY_USEFUL_TABLES) {
            dropTableIfExists("aaa_custom_prop");

            tabSB.append("\n\n-----------------------------\n\ncreate table aaa_custom_prop (\n"
                    + "id int not null identity primary key, "
                    + "si_id int not null, "
                    + "prop_name varchar(255) not null, "
                    + "full_prop_name varchar(2550) not null, "
                    + "field_name varchar(2550) not null, "
                    + "prop_val varchar(max) null, "
                    + "prop_parent_id int null, "
                    + "prop_nest_level int not null, "
                    + "is_global int not null, "
                    + "prop_class varchar(255) not null);\n");
        }

        tabSB.append("\n\n-----------------------------\n\ncreate table aaa_global_props (\n");

        Set<String> tabColNames = new LinkedHashSet<String>();

        for (String fixedGlobalPropsColumn : fixedGlobalPropsColumns) {
            tabColNames.add(fixedGlobalPropsColumn.toUpperCase());
        }

        tabSB.append("  si_id int not null primary key,\n");
        tabSB.append("  si_kind varchar(max) not null,\n");
        tabSB.append("  si_name varchar(max) null,\n");
        tabSB.append("  si_parentid int null,\n");
        tabSB.append("  SI_AUTHOR varchar(max) null,\n");
        tabSB.append("  SI_SIZE int null,\n");
        tabSB.append("  SI_HAS_CHILDREN int null,\n");
        tabSB.append("  SI_CONTENT_LOCALE varchar(max) null,\n");
        tabSB.append("  SI_WEBI_DOC_PROPERTIES varchar(max) null,\n");
        tabSB.append("  SI_READ_ONLY int null,\n");
        tabSB.append("  SI_LAST_SUCCESSFUL_INSTANCE_ID int null,\n");
        tabSB.append("  SI_LAST_RUN_TIME datetime null,\n");
        tabSB.append("  SI_ENDTIME datetime null,\n");
        tabSB.append("  SI_STARTTIME datetime null,\n");
        tabSB.append("  SI_SCHEDULE_STATUS int null,\n");
        tabSB.append("  SI_RECURRING int null,\n");
        tabSB.append("  SI_NEXTRUNTIME datetime null,\n");
        tabSB.append("  SI_DOC_SENDER varchar(max) null,\n");
        tabSB.append("  SI_OBJECT_IS_CONTAINER int null,\n"); // for sapbo4 connections

        for (Map.Entry<String, Map<String, Set<String>>> e : globalPropNames.entrySet()) {
            StringBuilder sb = new StringBuilder();
            sb.append("prop: ").append(e.getKey()).append(" has ").append(e.getValue().size() > 1 ? "many " : "").append("types: ").append(e.getValue().size()).append(", [ ");
            boolean first = true;
            int cnt = 0;
            for (Map.Entry<String, Set<String>> ee : e.getValue().entrySet()) {
                if (first) {
                    first = false;
                } else {
                    sb.append(", ");
                }

                sb.append(ee.getKey()).append(": ").append(ee.getValue().size());
                cnt += ee.getValue().size();
            }
            sb.append(" ]");
            //System.out.println(sb);

            boolean isPopularProp = cnt >= propNames.keySet().size() * 10 / 8;

            if (isPopularProp && e.getValue().size() > 1) {
                if (logger.isDebugEnabled()) {
                    logger.debug("-- ERROR: fld " + e.getKey() + " is popular but multikey");
                }
            }
            if (isPopularProp) {
                String fldName = e.getKey();
                String properFldName = fixIdent(fldName.toUpperCase().replace(".", "__"));
                if (properFldName.length() > 128) {
                    properFldName = properFldName.substring(0, 120) + "_LONG" + longColumnNamesCounter++;
                }
                String sqlType = classNameToSqlType.get(e.getValue().keySet().iterator().next());

                String fldNameUpper = fldName.toUpperCase();

                if (!fixedGlobalPropsColumns.contains(fldNameUpper) && !BaseUtils.safeEquals(sqlType, DONT_GENERATE_FIELD_TYPE)) {
                    tabSB.append("  ").append(//fldName
                            properFldName).append(" ").append(sqlType).append(" null,\n");
                    tabColNames.add(//fldName.toUpperCase()
                            properFldName);
                }
            }
        }

        tabSB.append("  table_code varchar(100) not null\n);\n\n");
        tabColNames.add("table_code".toUpperCase());
        allTablesColNames.put("aaa_global_props".toUpperCase(), tabColNames);

        //System.out.println(tabSB);
        execCommand(tabSB.toString());
    }

    protected String fixIdent(String ident) {
        return BaseUtils.fixIdentName(ident, "__").toUpperCase();
    }

    protected String createWhere() {
        if (interestingSiKinds.isEmpty()) {
            return "1=1";
        }

        return "SI_KIND in (" + mssqlValueFormatter.valToString(interestingSiKinds) + ")";
    }

    protected Integer getCount(IInfoStore infoStore, String text) {
        IInfoObjects objects = null;
        try {
            objects = infoStore.query("SELECT COUNT(SI_ID) FROM " + text + " WHERE " + where);
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in getCount: " + ex.getMessage(), ex);
            }
        }
        IInfoObject user = (IInfoObject) objects.get(0);
        IProperty property = user.properties().getProperty("SI_AGGREGATE_COUNT");
        IProperties properties = (IProperties) property.getValue();
        return (Integer) properties.getProperty("SI_ID").getValue();
    }

    protected void importUsers(IInfoStore infoStore) {
        List<Pair<Integer, String>> boUsers = new ArrayList<Pair<Integer, String>>();
        try {
            Integer siID = null;
            while (true) {
                IInfoObjects results = infoStore.query("select top 100 si_id, si_name from ci_systemobjects"
                        + (siID == null ? " where si_kind='User'" : (" WHERE (si_id > " + siID + ") AND ( si_kind='User' )")) + " order by SI_ID");

                for (int i = 0; i < results.size(); i++) {
                    IInfoObject obj = (IInfoObject) results.get(i);
                    boUsers.add(new Pair<Integer, String>(obj.getID(), obj.properties().getString("si_name")));
                    siID = obj.getID();
                }
                if (results.size() < 100) {
                    break;
                }
            }
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in importUsers: " + ex.getMessage(), ex);
            }
            boUsers.clear();
        }
        if (!boUsers.isEmpty()) {
            execNamedCommandWithCommit("deleteBOUsers");
            insertListToDBInPackages(boUsers, "insertBOUsers");
        }
    }

    protected void importSchedules(IInfoStore infoStore) {
        List<SAPBOScheduleBean> schedules = ReportScheduleInfo.getSchedules(infoStore);
        if (schedules != null && !schedules.isEmpty()) {
            execNamedCommandWithCommit("deleteBOSchedules");
            insertListToDBInPackages(schedules, "insertBOSchedules");
        }
    }

    public String getPumpGroup() {
        return PumpConstants.PUMP_GROUP_BO;
    }

    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_SAPBO;
    }

    @Override
    protected Pair<Integer, String> startPump() {
        execNamedCommandWithCommit("createBOInstanceFunctions", getSourceName(), instanceName);

        SapBOBean configBean = getAdhocDao().createBeanFromNamedQry("getBOConnectionParamatersForSource", SapBOBean.class, true, getSourceName(), instanceName);
        if (configBean == null) {
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "Error in reading BO Config!");
        }
        IEnterpriseSession enterpriseSession = null;

        try {
            ISessionMgr sessionMgr = CrystalEnterprise.getSessionMgr();
            try {
                enterpriseSession = sessionMgr.logon(configBean.userName, configBean.password, configBean.server, AUTHENTICATION_METHOD);
                if (enterpriseSession == null) {
                    return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "No connection with BO!");
                }
            } catch (Exception ex) {
                throw new LameRuntimeException(ex.getMessage(), ex);
            }
            IInfoStore iStore = (IInfoStore) enterpriseSession.getService("InfoStore");

            globalPropNames = new LinkedHashMap<String, Map<String, Set<String>>>();

            Integer countInfo = getCount(iStore, "CI_INFOOBJECTS");
            Integer countApp = getCount(iStore, "CI_APPOBJECTS");
//            Integer countSystem = getCount(iStore, "CI_SYSTEMOBJECTS");

            counts.put("info", countInfo);
            counts.put("app", countApp);
//            counts.put("sys", countSystem);

            allCount = (int) (2 * (countInfo + countApp/* + countSystem*/) * 1.2);
            if (logger.isInfoEnabled()) {
                logger.info("Data count: " + allCount);
            }

            createBOXITables(iStore, "CI_INFOOBJECTS", "info");
            createBOXITables(iStore, "CI_APPOBJECTS", "app");
//            createBOXITables(iStore, "CI_SYSTEMOBJECTS", "sys"); // niewykorzystywane
            generateGlobalTable();
            if (logger.isInfoEnabled()) {
                logger.info("Import data to tables.");
            }
            importDataToBOXITables(iStore, "CI_INFOOBJECTS", "info");
            importDataToBOXITables(iStore, "CI_APPOBJECTS", "app");
//            importDataToBOXITables(iStore, "CI_SYSTEMOBJECTS", "sys"); // niewykorzystywane

            pumpLogger.pumpLog("Import users", 90);
            importUsers(iStore);
            pumpLogger.pumpLog("Import schedules", 91);
            importSchedules(iStore);

            pumpLogger.pumpLog("Update BIK structure", 92);
            if (logger.isInfoEnabled()) {
                logger.info("Update BIK structure!");
            }
//            execNamedCommandWithCommit("deleteJoinedMetadata"); // to jest złe i niemłodzieżowe, więc nie robimy tak!!!
            pumpLogger.pumpLog("Update tree: Reports", 95);
            execNamedCommandWithCommit("insertIntoBikNode", "Reports");
            pumpLogger.pumpLog("Update tree: Universes", 96);
            execNamedCommandWithCommit("insertIntoBikNode", "ObjectUniverses");
            pumpLogger.pumpLog("Update tree: Connections", 97);
            execNamedCommandWithCommit("insertIntoBikNode", "Connections");
            pumpLogger.pumpLog("Update SAP BO: Extradata", 98);
            execNamedCommandWithCommit("insertIntoBikNodeBOExtradata");
            if (!configBean.withUsers) {
                execNamedCommandWithCommit("deleteUsers");
            }
            execNamedCommandWithCommit("insertConnections");
//            execNamedCommandWithCommit("insertDeskiConnections"); // niewykorzystywane w 4.0+ - brak tabeli INFO_FULLCLIENT

            // przygotowanie tabel z multiinstancjami do dalszych faz zasilania z BO
            execNamedCommandWithCommit("finishMultiInstanceTablesForJavaSDK");
            if (logger.isInfoEnabled()) {
                logger.info("All right!!");
            }
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_DONE, "Java SDK: Success");
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("Error: " + ex.getMessage(), ex);
            }
            throw new LameRuntimeException(ex.getMessage(), ex);
        } finally {
            if (enterpriseSession != null) {
                enterpriseSession.logoff();
            }
        }
    }
}
