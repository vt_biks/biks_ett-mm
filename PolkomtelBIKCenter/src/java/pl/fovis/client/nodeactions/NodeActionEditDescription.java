/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.EditDescriptionDialog;
import pl.fovis.common.NameDescBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public class NodeActionEditDescription extends NodeActionUpdatingBase {

    @Override
    public String getCaption() {
        return I18n.edytujOpis.get() /* I18N:  */;
    }

    @Override
    public void execute() {
        NameDescBean nameDesc = new NameDescBean();
        nameDesc.content = data.metadataEditedDescription != null ? data.metadataEditedDescription : data.node.descr;
        nameDesc.subject = data.node.name;
        new EditDescriptionDialog().buildAndShowDialog(nameDesc, new IParametrizedContinuation<NameDescBean>() {
            @Override
            public void doIt(final NameDescBean param) {
                if (data.node.treeKind.equals(BIKGWTConstants.TREE_KIND_METADATA)) {
                    BIKClientSingletons.getService().updateDescriptionForMetadata(param, data.node, new StandardAsyncCallback<Void>("Failure on" /* I18N: no */ + " updateDescriptionForMetadata") {
                                @Override
                                public void onSuccess(Void result) {
                                    setNotificationAndRefresh();
                                }
                            });
                } else {
                    BIKClientSingletons.getService().updateDescription(param, data.node, new StandardAsyncCallback<Void>("Failure on" /* I18N: no */ + " updateDescription") {
                                @Override
                                public void onSuccess(Void result) {
                                    setNotificationAndRefresh();
                                }
                            });
                }
            }
        });
    }

    protected void setNotificationAndRefresh() {
        BIKClientSingletons.showInfo(I18n.zedytowanoOpis.get() /* I18N:  */);
        refreshRedisplayDataAndSelectRecord(node.id/*, false*/);
    }

    @Override
    public boolean isEnabledByDesign() {
        if (isNodeOneOfTheKind(true, /*"User", */
                BIKGWTConstants.NODE_KIND_BLOG_ENTRY,
                BIKGWTConstants.NODE_KIND_GLOSSARY,
                BIKGWTConstants.NODE_KIND_GLOSSARY_VERSION,
                BIKGWTConstants.NODE_KIND_GLOSSARY_NOT_CORPO,
                BIKGWTConstants.NODE_KIND_KEYWORD //"BlogEntry", I18n.glossary.get() /* I18N:  */, "GlossaryVersion", "GlossaryNotCorpo", I18n.keyword.get() /* I18N:  */
        ) || isConfluenceObject() || isDQMTest()) {
            return false;
        }

//        if (isNodeOneOfTheKind(false, "Blog")) {
//            // 1. dla niewbudowanych i niepodlinkowanych oraz
//            // 2. musi być adminem bloga
//            return isNodeNotLinkedAnyhow()
//                    && isLoggedUserInBlogAuthors(true);
//        } else {
//            return isNodeNotLinkedAnyhow();
//        }
        if (BIKClientSingletons.isLoggedUserAuthorOfTree(node.treeId) || BIKClientSingletons.isLoggedUserCreatorOfTree(node.treeId)) {
            return true;
        }
        return isNodeNotLinkedAnyhow();
    }
}
