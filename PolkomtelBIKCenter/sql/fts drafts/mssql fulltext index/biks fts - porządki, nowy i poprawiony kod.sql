/*

1. node_kind_id i tree_id w attr_val
2. usuwa� attr_vals dla usuni�tych node'�w (is_deleted <> 0 i linked i co� tam - warunki z wyszukiwarki)
3. usuwa� attr_vals dla usuni�tych attrs - czyli ju� nie ma takich atrybut�w a nadal warto�� sobie figuruje
4. indeks fts tylko na polu fixed_val
5. zmiany w kodzie BIKS aby aktualizowa� attr_vals po zmianach w bik_node - wsz�dzie gdzie zmienia si� co� co b�dzie
   trzymane w attr_vals
5. szybsze wersje sqli do wyszukiwania - uproszczone, bo wi�cej informacji b�dzie siedzia�o bezpo�rednio w attr_vals
*/

/*
na podstawie pliku: alter db for v1.0.88.13 tf.sql

--  exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_extradata', 'node_id', 'author, owner, cuid, guid, ruid', null, @optNodeFilter
select node_id, author, owner, cuid, guid, ruid from bik_sapbo_extradata 

--  exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_query', 'node_id', 'sql_text, filtr_text', null, @optNodeFilter
select node_id, sql_text, filtr_text from bik_sapbo_query

--  exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_column', 'table_id', 'name', null, @optNodeFilter
select table_id, name, * from bik_sapbo_universe_column

--  exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_connection', 'node_id', 'database_enigme, database_source, connetion_networklayer_name, user_name, server', null, @optNodeFilter
select node_id, database_enigme, database_source, connetion_networklayer_name, user_name, server from bik_sapbo_universe_connection

--    exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_object', 'node_id', 'text_of_select, text_of_where', null, @optNodeFilter
select node_id, text_of_select, text_of_where from bik_sapbo_universe_object


--  exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_table', 'node_id', 'sql_of_derived_table, sql_of_derived_table_with_alias', null, @optNodeFilter
select node_id, sql_of_derived_table, sql_of_derived_table_with_alias from bik_sapbo_universe_table

*/

-- print cast(sysdatetime() as varchar(23)) + ': koniec'

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

/*

zawsze po zasilaniu wywo�uje si� procedura:
 exec sp_verticalize_node_attrs null

natomiast ta metoda wywo�uje:
  1. zdejmij indeks FTS z bik_searchable_attr_val
  2. exec sp_verticalize_node_attrs_one_table 'bik_node', 'id', 'name, descr', null, 0, @optNodeFilter
  3. exec sp_verticalize_node_attrs_metadata @optNodeFilter
  4. za�� indeks FTS na bik_searchable_attr_val

w "alter db for v1.0.88.13 tf.sql" dzia�anie sp_verticalize_node_attrs_metadata wygl�da�o tak (poni�ej jest te� nowa tre�� dla tej procedury):
  exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_extradata', 'node_id', 'author, owner, cuid, guid, ruid', null, @optNodeFilter
  exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_query', 'node_id', 'sql_text, filtr_text', null, @optNodeFilter
  exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_column', 'table_id', 'name', null, @optNodeFilter
  exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_connection', 'node_id', 'database_enigme, database_source, connetion_networklayer_name, user_name, server', null, @optNodeFilter
  exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_object', 'node_id', 'text_of_select, text_of_where', null, @optNodeFilter
  exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_table', 'node_id', 'sql_of_derived_table, sql_of_derived_table_with_alias', null, @optNodeFilter

natomiast sp_verticalize_node_attrs_one_metadata_table to po prostu:
  exec sp_verticalize_node_attrs_one_table @source, @nodeIdCol, @cols, @properNames, 0, @optNodeFilter
czyli dajemy 0 w parametrze @valueImportance

procedura sp_verticalize_node_attrs_one_table jest jeszcze wywo�ywana z:
create procedure [dbo].sp_update_searchable_attr_val(@nodeId int, @properAttrName varchar(255), @valueImportance int, @value varchar(max)) as
begin
  declare @source varchar(max) = '(select ' + cast(@nodeId as varchar(20)) + ' as node_id, 
    ''' + replace(@value, '''', '''''') + ''' as val)'
  
  declare @nodeFilter varchar(max) = 'n.id = ' + cast(@nodeId as varchar(20))
    
  exec sp_verticalize_node_attrs_one_table @source, 'node_id', 'val', @properAttrName, @valueImportance, 
    @nodeFilter
end
go


*/



-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
-- poprzednia wersja pochodzi z pliku: alter db for v1.0.88.12 ww.sql

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs_one_table]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs_one_table
go

create procedure [dbo].[sp_verticalize_node_attrs_one_table](@source varchar(max), @nodeIdCol sysname, @cols varchar(max),
  @properNames varchar(max), @valueImportance int, @optNodeFilter varchar(max))
as
begin
  set nocount on
  
  declare @diags_level int = 1 -- 0 oznacza brak logowania

  -- diag
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': start'

  --------------
  -- 1. rozbijamy nazwy atrybut�w - nazwy kolumn w �r�dle i nazwy w�a�ciwe (opcjonalne)
  --------------

  declare @attrPropNamesTab table (idx int primary key, name sysname not null)
  
  insert into @attrPropNamesTab (idx, name)
  select idx, str from dbo.fn_split_by_sep(@properNames, ',', 7)
  
  declare @attrNamesTab table (name sysname not null primary key, 
    proper_name varchar(255) not null, search_weight int not null,
    attr_id int null
  )
  
  insert into @attrNamesTab (name, proper_name, search_weight,
    attr_id)
  select aaa.str, coalesce(apnt.name, aaa.str), coalesce(a.search_weight, 1),
    a.id
  from dbo.fn_split_by_sep(@cols, ',', 7) aaa left join @attrPropNamesTab apnt on aaa.idx = apnt.idx
    left join 
      bik_searchable_attr a on a.name = coalesce(apnt.name, aaa.str)

  --------------
  -- 2. znamy nazwy atrybut�w i node_kindy, teraz uzupe�niamy bik_attribute, ew. poprawiamy tam typy
  --------------
  
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed uzupe�nieniem searchable attrs'
  
  insert into bik_searchable_attr (name, caption, search_weight)
  select ant.proper_name, ant.proper_name, 1
  from @attrNamesTab ant
  where ant.attr_id is null  
  
  update @attrNamesTab set attr_id = a.id
  from @attrNamesTab aaa inner join bik_searchable_attr a on aaa.attr_id is null and a.name = aaa.name
  
  --------------
  -- 3. wrzucamy warto�ci atrybut�w do tabeli pomocniczej
  --------------

  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przygotowanie do wrzucania do @attrValsTab'
  
  declare @attrValsTab table (attr_id int not null, node_id int not null, value varchar(max) null, 
    --type int not null, 
    search_weight int not null, tree_id int not null, node_kind_id int not null)
  
  declare attrCur cursor for select name, --attr_type, 
  proper_name, search_weight, attr_id from @attrNamesTab
  
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed open attrCur'

  open attrCur
  
  declare @attrName sysname, @attrType int, @attrProperName varchar(255), @searchWeight int, @attr_id int
  declare @attrValSql varchar(max)
  
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed pierwszym fetch next from attrCur'

  fetch next from attrCur into @attrName, --@attrType, 
    @attrProperName, @searchWeight, @attr_id
  
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed p�tl�'

  while @@fetch_status = 0
  begin
    if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': start p�tli dla pola: ' + @attrName + ' (' + @attrProperName + ')'
    
    set @attrValSql = 
    'select attr_id, node_id, case when is_deleted = 1 or value is null or ltrim(rtrim(value)) = '''' then null else value end, search_weight,' +
    ' tree_id, node_kind_id from (' +
    'select ' + cast(@attr_id as varchar(20)) + ' as attr_id, x.' + @nodeIdCol + ' as node_id, x.' + @attrName + ' as value, ' + --', a.type, ' + 
    cast(@searchWeight as varchar(20)) + ' * (n.search_rank + nk.search_rank) * 100 + n.vote_sum as search_weight, ' +
    + 'case when n.is_deleted = 0 and (n.linked_node_id is null or n.disable_linked_subtree <> 0) then 0 else 1 end is_deleted,' + 
      ' n.tree_id, n.node_kind_id' +
      ' from ' + @source + ' as x inner join bik_node n on n.id = x.' + @nodeIdCol +
      ' inner join bik_node_kind nk on n.node_kind_id = nk.id' +
      --' inner join bik_searchable_attr a on n.node_kind_id = a.node_kind_id and a.name = ''' + @attrProperName + '''' +
      --' where x.' + @attrName + ' is not null' +
      case when @optNodeFilter is null then '' else ' where ' + @optNodeFilter end +
      ' ) x'
    print 'sql for attr=' + @attrName + ' is: 
    ' + @attrValSql
  
    insert into @attrValsTab (attr_id, node_id, value, --type, 
    search_weight, tree_id, node_kind_id)
    exec (@attrValSql)
  
    if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed ko�cem iteracji p�tli dla pola: ' + @attrName
    
    fetch next from attrCur into @attrName, --@attrType, 
      @attrProperName, @searchWeight, @attr_id
  end
    
  close attrCur
  
  deallocate attrCur
  
  --------------
  -- 5. aktualizacja bik_searchable_attr_val - na podstawie wyci�gni�tych warto�ci
  --------------
  
  if @diags_level > 0 begin
    declare @avtCnt int = (select count(*) from @attrValsTab)
    print cast(sysdatetime() as varchar(23)) + ': za p�tl�, kursor zamkni�ty, liczba warto�ci razem: ' +
      cast(@avtCnt as varchar(20)) + ', przed delete'
  end
  
  declare @delCnt int
  delete from bik_searchable_attr_val
  from @attrValsTab avt
  where avt.value is null and bik_searchable_attr_val.node_id = avt.node_id and bik_searchable_attr_val.attr_id = avt.attr_id
  set @delCnt = @@rowcount
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': po delete, rowcnt=' + cast(@delCnt as varchar(20)) + ', przed update'
  end

  update bik_searchable_attr_val
  set value = avt.value, 
    fixed_value = case when bik_searchable_attr_val.value <> avt.value then dbo.fn_normalize_text_for_fts(avt.value) else fixed_value end,
    search_weight = avt.search_weight
  from @attrValsTab avt
  where avt.value is not null and bik_searchable_attr_val.node_id = avt.node_id and bik_searchable_attr_val.attr_id = avt.attr_id and
     (bik_searchable_attr_val.value <> avt.value or bik_searchable_attr_val.search_weight <> avt.search_weight)
  
  declare @updRowCnt int = @@rowcount
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': po update, row count: ' +
      cast(@updRowCnt as varchar(20)) + ', przed insert'
  end
  
  --------------
  -- 6. dorzucenie nowych warto�ci
  --------------
  
  insert into bik_searchable_attr_val (node_id, attr_id, value, fixed_value, search_weight, tree_id, node_kind_id)
  select avt.node_id, avt.attr_id, avt.value, dbo.fn_normalize_text_for_fts(avt.value), avt.search_weight, avt.tree_id, avt.node_kind_id
  from @attrValsTab avt left join bik_searchable_attr_val av on
  avt.attr_id = av.attr_id and avt.node_id = av.node_id
  where av.id is null and avt.value is not null

  declare @insRowCnt int = @@rowcount

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': po insert, row count: ' +
      cast(@insRowCnt as varchar(20)) + ', KONIEC!'
  end
  
  -- select * from @attrValsTab
  
  --declare @sql varchar(max) = 'select ' + @nodeIdCol + ', ' + @normalCols + ', ' + @identCols + ' from ' + @source
  --print @sql
  
  --select * from @attrNamesTab
  --select * from @attrNodeKindsTab ankt inner join bik_node_kind nk on ankt.node_kind_id = nk.id
end
go


---------------------------------------------------------------
---------------------------------------------------------------
---------------------------------------------------------------

/*

exec sp_verticalize_node_attrs_one_table '(select id, name, coalesce(descr_plain, descr) as descr from bik_node)', 'id', 'name, descr', null, 0, null


declare @tree_id int = (select id from bik_tree where code = 'Teradata' --'Reports' --'Connections' --'Glossary'
)
declare @filter varchar(max) = 'n.tree_id = ' + cast(@tree_id as varchar(20))
exec sp_verticalize_node_attrs_one_table 'bik_node', 'id', 'name, descr', null, 0, null --@filter

declare @tree_id int = (select id from bik_tree where code = 'Glossary')
select * from bik_searchable_attr_val where tree_id = @tree_id
order by node_id, attr_id

insert into bik_searchable_attr_val (attr_id, node_id, value, fixed_value, value_importance, search_weight, tree_id, node_kind_id)
values (1, 841783, 'sss', 'x', 0, 1, 8, 16)

update 
bik_searchable_attr_val set value = 'xxxxxx'
where id = 242427

select *, (select count(*) from bik_node where tree_id = t.id and is_deleted = 0) as node_cnt from bik_tree t

-- czas: 5:31
k

drop table #xxx

select id, name, descr, case when n.is_deleted = 0 and (n.linked_node_id is null or n.disable_linked_subtree <> 0) then 0 else 1 end is_deleted_fix
into #xxx
from bik_node n


drop table #xxx

select attr_id, node_id, case when is_deleted = 1 or value is null or ltrim(rtrim(value)) = '' then null else value end as is_deleted_fix, search_weight, tree_id, node_kind_id 
into #xxx
from (
select 1 as attr_id, n.id as node_id, n.descr as value, 100 * (n.search_rank + nk.search_rank) * 100 + n.vote_sum as search_weight, case when n.is_deleted = 0 and (n.linked_node_id is null or n.disable_linked_subtree <> 0) then 0 else 1 end is_deleted, n.tree_id, n.node_kind_id 
from bik_node as x inner join 
bik_node n on n.id = x.id 
inner join bik_node_kind nk on n.node_kind_id = nk.id 
) x

*/


-- select case when ltrim(rtrim('  ')) = '' then 1 else 0 end

-- print cast(sysdatetime() as varchar(23)) + ': koniec'

---------------------------------------------------------------
---------------------------------------------------------------
---------------------------------------------------------------
