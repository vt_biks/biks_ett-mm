/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.junit.Test;
import pl.fovis.common.SearchWizardHitResult;
import pl.fovis.server.NodesDataFetcherForLucene;
import pl.fovis.server.fts.luceneUtils.LuceneLameUtils;
import pl.fovis.server.fts.luceneUtils.SearchWizardBucketManagerJoinedObjsTabKindEx;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import pl.trzy0.foxy.serverlogic.FoxyAppUtils;
import pl.trzy0.foxy.serverlogic.db.MssqlConnection;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.BaseUtils;
import simplelib.FieldNameConversion;
import simplelib.ILameCollector;
import simplelib.Pair;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author pmielanczuk
 */
public class LuceneSimilarNodesWizardTestBiks {

    private static final String ADHOC_SQLS_DIR = "./web/WEB-INF/FoxyMill/";

    static {
        Map<String, String> loggersCfg = new HashMap<String, String>();
        loggersCfg.put("/level", "Stage");
        loggersCfg.put("LuceneSimilarNodesWizard", "Info");
//        loggersCfg.put("LowercasingMorfologicWithBaseFormFilter", "Debug");
//        loggersCfg.put("StandardFilterWithTerm", "Debug");
        LameLoggerFactory.applyLameLoggersConfig(loggersCfg);
    }
    LuceneSimilarNodesWizard wizard = new LuceneSimilarNodesWizard("C:/Temp/bikcenter-upload/lucene/BIKS_Polkomtel_2015");
    IBeanConnectionEx<Object> conn = new MssqlConnection(
            new MssqlConnectionConfig("localhost", null, "BIKS_Polkomtel_2015", "sa", "sasasa"));
    INamedSqlsDAO<Object> adhocDao = FoxyAppUtils.createNamedSqlsDAOFromFile(conn, ADHOC_SQLS_DIR + "adhocSqls.html");

    INodesDataFetcher dummyFetcher = new INodesDataFetcher() {

        @Override
        public void fetchFullData(ILameCollector<Map<String, Pair<String, Float>>> collector) {
        }

        @Override
        public void fetchChangedData(ILameCollector<Map<String, Pair<String, Float>>> collector) {
        }
    };

    INodesDataFetcher nodesFetcher = new NodesDataFetcherForLucene(adhocDao, null, null, null, 0, 0.0f);

    public LuceneSimilarNodesWizardTestBiks() {
//        wizard.setUp(nodesFetcher, true);
        wizard.setUpReadOnly();
        //LameUtils.threadSleep(10);
        wizard.waitForFinalizeIndexingIfInProgress();
    }

//    @Test
    public void testSimilarToSzokArchiwum() {
        //System.out.println("Current dir=" + Paths.get(".").toAbsolutePath().normalize().toString());

        wizard.explainForNodeIds = BaseUtils.paramsAsSet(368597, 2975914, 1643279);

        // Szok Archiwum
        int nodeId = 1107503;

        Map<Integer, Float> res = wizard.findSimilarNodeIds(nodeId, 0.01, 1000, null);

        showResults(res, nodeId);
    }

//    @Test
    public void testSimilarToAnnapurna() {
        //System.out.println("Current dir=" + Paths.get(".").toAbsolutePath().normalize().toString());

        wizard.explainForNodeIds = BaseUtils.paramsAsSet(2419728, 2975919, 2410230, 2422948);
        // AnnaPurna
//        int nodeId = 2420199;
        int nodeId = 2975933;
//        int nodeId = 2975920;
//        int nodeId = 2975919;
//        int nodeId = 2971352;

        Map<Integer, Float> res = wizard.findSimilarNodeIds(nodeId, 0.0, 50, new SearchWizardBucketManagerJoinedObjsTabKindEx(Collections.<String>emptyList()));

        showResults(res, nodeId);
    }

    protected void showResults(Map<Integer, Float> res) {
        showResults(res, null);
    }

    protected void showResults(Map<Integer, Float> res, Integer nodeId) {
        showResultsInner(null, res, nodeId);
    }

    protected void showResults(List<SearchWizardHitResult> hits) {
        showResultsInner(hits, LuceneLameUtils.convertSearchHitsToMap(hits), null);
    }

    protected void showResultsInner(List<SearchWizardHitResult> hits, Map<Integer, Float> res, Integer nodeId) {
        System.out.println("Found results: " + res.size());

        if (nodeId != null) {
            res.put(nodeId, 1000f);
        }

        List<Map<String, Object>> rows
                = //conn.execQry("select id, name, coalesce(descr_plain, descr) as descr from bik_node where id in (" + conn.toSqlString(res.keySet()) + ") --and name like '%szok%'");
                res.isEmpty() ? Collections.<Map<String, Object>>emptyList()
                        : adhocDao.execNamedQueryCFN("foundNodesWithSimilarityScore", FieldNameConversion.ToLowerCase, res);

        Set<Integer> ids = new HashSet<Integer>();
        int num = 0;

        for (Map<String, Object> row : rows) {
//            String name = (String) row.get("name");
//            if (!BaseUtils.safeEqualsStr(name, "szok", true)) {
//                continue;
//            }
            String descr = (String) row.get("descr");
            if (descr != null) {
                descr = BaseUtils.safeSubstring(BaseUtils.replaceNewLinesToRowSep(descr, " "), 0, 200);
                row.put("descr", descr);
            }
            Integer id = conn.sqlNumberToInt(row.get("id"));
            ids.add(id);

            String occTxt;

            if (hits != null) {
                Set<String> occFlds = hits.get(num).occurenceFields;
                occTxt = occFlds == null ? "" : " (occurence: " + BaseUtils.mergeWithSepEx(occFlds, ",") + ")";
            } else {
                occTxt = "";
            }

            System.out.println("#" + (num++) + occTxt + ": " + row);
        }

        System.out.println("\nids=" + conn.toSqlString(ids));
    }

    @Test
    public void testSearchText() throws Exception {
//        Map<Integer, Float> res = wizard.findNodeIds(FtsByLucene.COLLECTIVE_FIELD_NAME + ":" + "annapurna", 0.0001, 1000);
//        showResults(res);
//        System.out.println("--------------------");
//        showResults(wizard.findNodeIds("annaPurna szczyt", 0, 1000));
//        wizard.explainForNodeIds = BaseUtils.paramsAsSet(2971338);

//        showResults(wizard.findNodeIds("*nn*pur*a szczyt", 0, 1000));
//        showResults(wizard.findNodeIds("*nn*pur*a psy", 0, 1000));
//        showResults(wizard.findNodeIds("s*bie szczyrku", 0, 1000));
//        showResults(wizard.findNodeIds("jak* s*bie szczyrku", 0, 1000));
//        showResults(wizard.findNodeIds("ann*", 0, 1000, "sz*yt", true));
//        showResults(wizard.findNodeIds("for-biks-file-system", 0, 1000, null, true));
        showResults(wizard.findNodeIds("Jolanta ma chomiczka", 0, 1000, null, true));
//        showResults(wizard.findNodeIds("*nn*pur*a ann*purna szczyt pies kot", 0, 1000));
//        showResults(wizard.findNodeIds("*nn*pur*a ann*purna szczyty psy koty", 0, 1000));
//        showResults(wizard.findNodeIds("annapurna.raport", 0, 1000));
//        Query q = new QueryParser("name", wizard.analyzer).parse("annapurna");
//        System.out.println("qry=" + q);
//        TopDocs td = wizard.searcher.search(q, 100);
//        System.out.println("total hits: " + td.totalHits);
//        Highlighter h = new Highlighter(new QueryScorer((Query) null));
//        Analyzer analyzer = new LowercasingMorfologicWithBaseFormAnalyzer();
//        System.out.println(LuceneLameUtils.parseTextByAnalyzer(analyzer, "Annapurna Ania ania purna psów i kotów", "aqq"));
    }
}
