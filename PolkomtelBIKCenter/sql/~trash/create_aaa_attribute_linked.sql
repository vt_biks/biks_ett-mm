

/****** Object:  Table [dbo].[aaa_attribute_linked]    Script Date: 03/08/2011 09:56:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[aaa_attribute_linked](
	[atr_lin_id] [int] IDENTITY(1,1) NOT NULL,
	[atr_lin_atr_id] [int] NOT NULL,
	[atr_lin_value] [varchar](max) NULL,
	[atr_lin_si_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[atr_lin_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[aaa_attribute_linked]  WITH CHECK ADD FOREIGN KEY([atr_lin_atr_id])
REFERENCES [dbo].[aaa_attribute] ([atr_id])
GO

ALTER TABLE [dbo].[aaa_attribute_linked]  WITH CHECK ADD FOREIGN KEY([atr_lin_atr_id])
REFERENCES [dbo].[aaa_attribute] ([atr_id])
GO


