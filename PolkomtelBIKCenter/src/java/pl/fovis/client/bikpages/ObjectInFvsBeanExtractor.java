/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.ui.HTML;
import java.util.ArrayList;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.entitydetailswidgets.IGridBeanAction;
import pl.fovis.client.entitydetailswidgets.NodeAwareBeanExtractorBase;
import pl.fovis.client.entitydetailswidgets.SimpleGridBeanAction;
import pl.fovis.common.FvsChangeExBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author beata
 */
public class ObjectInFvsBeanExtractor extends NodeAwareBeanExtractorBase<FvsChangeExBean> {

    public ObjectInFvsBeanExtractor() {
        super(false);
    }

//    {
//        iconHintByIdx.put(2, "Usuń z ulubionych");
//    }
    public Integer getNodeId(FvsChangeExBean b) {
        return b.nodeId;
    }

    public String getTreeCode(FvsChangeExBean b) {
        return b.tabId;
    }

    protected String getName(FvsChangeExBean b) {
        return b.name;
    }

    protected String getNodeKindCode(FvsChangeExBean b) {
        return b.code;
    }

    public String getBranchIds(FvsChangeExBean b) {
        return null;
    }

    @Override
    protected void execActDelete(final FvsChangeExBean b) {
        BIKClientSingletons.getService().removeFromFvs(getNodeId(b),
                new StandardAsyncCallback<Boolean>() {
                    public void onSuccess(Boolean result) {
                        refreshView(I18n.usunietoZUlubionych.get() /* I18N:  */, true, false);
                    }
                });
    }
    protected IGridBeanAction<FvsChangeExBean> actDeleteFromFvs = new SimpleGridBeanAction<FvsChangeExBean>() {
        public String getActionIconUrl(FvsChangeExBean b) {
            Integer nodeId = getNodeId(b);
            if (nodeId == null) {
                return null;
            }
            return getIconUrlByIconName("trash_gray");
        }

        public String getActionIconHint(FvsChangeExBean b) {
            return I18n.usunZUlubionych.get() /* I18N:  */;
        }

        public void executeAction(final FvsChangeExBean b) {

            HTML element = new HTML(
                    I18n.czyNaPewnoUsunacZUlubionyElement2.get() /* I18N:  */ + ": <div class='Delete-Element'><img src" /* I18N: no */ + "='" + getIconUrl(b) + "'/> <b>" + getNameAsHtml(b) + "</b></div>");
            element.setStyleName("komunikat" /* I18N: no:css-class */);

            new ConfirmDialog().buildAndShowDialog(element, I18n.usun.get() /* I18N:  */, new IContinuation() {
                        public void doIt() {

                            execActDelete(b);
                        }
                    });

        }

        public boolean isVisible(FvsChangeExBean b) {
            return isDeleteBtnVisible2Wtf(b);
        }

        public boolean isEnabled(FvsChangeExBean b) {
            return BIKClientSingletons.isUserLoggedIn() && isDeleteBtnEnabled(b);
        }

        public IGridBeanAction.TypeOfAction getActionType() {
            return IGridBeanAction.TypeOfAction.Delete;
        }
    };

    @Override
    public List<IGridBeanAction<FvsChangeExBean>> createActionList() {
        List<IGridBeanAction<FvsChangeExBean>> res = new ArrayList<IGridBeanAction<FvsChangeExBean>>();
        res.add(actView);
        res.add(actFollow);
        res.add(actDeleteFromFvs);
        return res;
    }

    public List<IGridBeanAction<FvsChangeExBean>> createActionList2() {
        List<IGridBeanAction<FvsChangeExBean>> res = new ArrayList<IGridBeanAction<FvsChangeExBean>>();
        res.add(actView);
        res.add(actFollow);
//        res.add(actDeleteFromFvs);
        return res;
    }

    protected void refreshView(String msg, boolean refetch, boolean resetPosition) {
        if (dataChangedHandler != null) {
            dataChangedHandler.doIt(new Pair<Boolean, Boolean>(refetch, resetPosition));
            BIKClientSingletons.showInfo(msg);
        }
    }
    public IParametrizedContinuation<Pair<Boolean, Boolean>> dataChangedHandler;

    public void setDataChangedHandler(IParametrizedContinuation<Pair<Boolean, Boolean>> handler) {
        this.dataChangedHandler = handler;
    }

    @Override
    protected void execActFollow(final FvsChangeExBean b) {
        BIKClientSingletons.getService().deleteChangedObjectInFvs(b.nodeId, new StandardAsyncCallback<Void>() {
            public void onSuccess(Void result) {
                BIKClientSingletons.getTabSelector().submitNewTabSelection(getTreeCode(b), getNodeId(b), true);
            }
        });
    }
}
