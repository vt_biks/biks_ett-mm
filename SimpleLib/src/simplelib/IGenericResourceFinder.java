/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package simplelib;

import java.util.Set;

/**
 *
 * @author wezyr
 */
public interface IGenericResourceFinder {
    public Set<String> getResourceNames(String base, Set<String> fileExts, boolean recurseSubDirs);
}
